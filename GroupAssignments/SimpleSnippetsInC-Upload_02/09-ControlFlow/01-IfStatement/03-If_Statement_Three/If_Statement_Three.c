#include<stdio.h>

int main(void)
{
	//variable declarations
	int num;

	// code
	printf("\nEnter value of 'num' : ");
	scanf("%d", &num);

	if (num < 0)
		printf("\nnum= %d is less then 0 (negative)", num);
	if (num >=0 && num <= 100)
		printf("\nnum= %d lies from 0 to 100", num);
	if (num > 100 && num <= 200)
		printf("\nnum= %d lies from 100 to 200", num);
	if (num > 200 && num <= 300)
		printf("\nnum= %d lies from 200 to 300", num);
	if (num > 300 && num <= 400)
		printf("\nnum= %d lies from 300 to 400", num);
	if (num > 400 && num <= 500)
		printf("\nnum= %d lies from 400 to 500", num);
	if (num > 500)
		printf("\nnum= %d is greater than 500", num);

	printf("\n");
	return(0);
}
/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\01-IfStatement\03-If_Statement_Three>if_Statement_Three.exe

Enter value of 'num' : 67

num= 67 lies from 0 to 100

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\01-IfStatement\03-If_Statement_Three>if_Statement_Three.exe

Enter value of 'num' : 278

num= 278 lies from 200 to 300

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\01-IfStatement\03-If_Statement_Three>if_Statement_Three.exe

Enter value of 'num' : 900

num= 900 is greater than 500
*/