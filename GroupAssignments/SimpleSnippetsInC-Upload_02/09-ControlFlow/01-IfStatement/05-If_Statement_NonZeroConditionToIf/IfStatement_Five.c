#include<stdio.h>

int main(void)
{
	// variable declarations
	int x;

	// code
	x = 8;
	if (x)
		printf("\nif-block 1: 'x' exists and has value %d", x);

	x = -8;
	if (x)
		printf("\nif-block 2: 'x' exists and has value %d", x);

	x = 0;
	if (x)
		printf("\nif-block 3: 'x' exists and has value %d", x);

	printf("\nAll 3 if statements done");
	printf("\n");
	return(0);
}
/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\01-IfStatement\05-If_Statement_NonZeroConditionToIf>ifStatement_Five.exe

if-block 1: 'x' exists and has value 8
if-block 2: 'x' exists and has value -8
All 3 if statements done
*/