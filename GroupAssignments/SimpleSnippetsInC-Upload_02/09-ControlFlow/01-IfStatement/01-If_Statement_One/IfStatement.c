#include<stdio.h>

int main(void)
{
	// variable declarations
	int x, y, z;

	// code
	x = 8;
	y = 70;
	z = 70;

	if (x < y)
		printf("\nx is less than y");
	if (y != z)
		printf("\ny is not equal to z");

	printf("\nBoth comparisions done");

	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\01-IfStatement\If_Statement_One>ifStatement.exe

x is less than y
Both comparisions done
*/