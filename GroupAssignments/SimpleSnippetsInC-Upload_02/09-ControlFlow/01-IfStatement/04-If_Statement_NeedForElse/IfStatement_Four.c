#include<stdio.h>

int main(void)
{
	// variable declarations
	int age;

	// code
	printf("\nEnter age: ");
	scanf("%d", &age);

	if (age >= 18)
	{
		printf("\nEligible to vote");
	}
	else
	{
		printf("\nNot eligible to vote");
	}
	printf("\n");
	return(0);
}
/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\01-IfStatement\04-If_Statement_NeedForElse>ifStatement_Four.exe

Enter age: 89

Eligible to vote

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\01-IfStatement\04-If_Statement_NeedForElse>ifStatement_Four.exe

Enter age: 5

Not eligible to vote
*/