#include<stdio.h>

int main(void)
{
	// variable declarations
	int age;

	// code
	printf("\nEnter age: ");
	scanf("%d", &age);

	if (age >= 18)
	{
		printf("\nEligible to vote");
	}

	return(0);
}

/*Outout-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\01-IfStatement\If_Statement_Two>ifStatement_Two.exe

Enter age: 8

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\01-IfStatement\If_Statement_Two>ifStatement_Two.exe

Enter age: 47

Eligible to vote
*/