#include<stdio.h>

int main(void)
{
	//variable declarations
	int num;

	// code
	printf("\nEnter value of 'num' : ");
	scanf("%d", &num);

	if (num < 0)
		printf("\nnum= %d is less then 0 (negative)", num);
	else if (num >= 0 && num <= 100)
		printf("\nnum= %d lies from 0 to 100", num);
	else if (num > 100 && num <= 200)
		printf("\nnum= %d lies from 100 to 200", num);
	else if (num > 200 && num <= 300)
		printf("\nnum= %d lies from 200 to 300", num);
	else if (num > 300 && num <= 400)
		printf("\nnum= %d lies from 300 to 400", num);
	else if (num > 400 && num <= 500)
		printf("\nnum= %d lies from 400 to 500", num);
	else if (num > 500)
		printf("\nnum= %d is greater than 500", num);
	else
		printf("\nInvalid value entered");

	printf("\n");
	return(0);
}
/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\03-IfElsefLadder\02-IfElseIfLadderWithOptionalEndingElse>IfElseIfLadder.exe

Enter value of 'num' : 67

num= 67 lies from 0 to 100

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\03-IfElsefLadder\02-IfElseIfLadderWithOptionalEndingElse>IfElseIfLadder.exe

Enter value of 'num' : 677

num= 677 is greater than 500

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\03-IfElsefLadder\02-IfElseIfLadderWithOptionalEndingElse>IfElseIfLadder.exe

Enter value of 'num' : f

num= 0 lies from 0 to 100
*/