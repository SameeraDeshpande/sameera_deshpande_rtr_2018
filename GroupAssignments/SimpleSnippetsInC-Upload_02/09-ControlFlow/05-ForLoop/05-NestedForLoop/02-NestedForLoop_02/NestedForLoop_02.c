#include<stdio.h>

int main(void)
{
	// variable declarations
	int i, j,k;

	// code
	for (i = 1;i <= 6;i++)
	{
		printf("\ni= %d\n", i);
		for (j = 1;j <= 5;j++)
		{
			printf("\n\tj= %d\n", j);
			for (k = 1;k <= 3;k++)
			{
				printf("\n\t\tk= %d\n", k);
			}
		}
	}

	printf("\n");
	return(0);
}

/*
Output-
i= 1

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3

i= 2

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3

i= 3

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3

i= 4

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3

i= 5

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3

i= 6

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3
*/