#include<stdio.h>

int main(void)
{
	// variable declarations
	int i, j;

	// code
	for (i = 1;i <= 6;i++)
	{
		printf("\n\ni= %d", i);
		for (j = 1;j <= 5;j++)
			printf("\n\tj= %d", j);
	}

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\05-ForLoop\05-NestedForLoop\01-NestedForLoop_01>NestedForLoop_01.exe


i= 1
j= 1
j= 2
j= 3
j= 4
j= 5

i= 2
j= 1
j= 2
j= 3
j= 4
j= 5

i= 3
j= 1
j= 2
j= 3
j= 4
j= 5

i= 4
j= 1
j= 2
j= 3
j= 4
j= 5

i= 5
j= 1
j= 2
j= 3
j= 4
j= 5

i= 6
j= 1
j= 2
j= 3
j= 4
j= 5

*/