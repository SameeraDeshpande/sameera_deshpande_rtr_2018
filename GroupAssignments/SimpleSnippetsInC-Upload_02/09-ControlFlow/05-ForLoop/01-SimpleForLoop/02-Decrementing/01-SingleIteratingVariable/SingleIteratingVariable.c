#include<stdio.h>

int main(void)
{
	// variable declarations
	int i;

	// code
	printf("\nPrinting digits 10 to 1 :\n\n");

	for (i = 10;i >= 1;i--)
		printf("\t%d", i);

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\05-ForLoop\01-SimpleForLoop\02-Decrementing\01-SingleIteratingVariable>SingleIteratingVariable.exe

Printing digits 10 to 1 :

10      9       8       7       6       5       4       3       2       1
*/