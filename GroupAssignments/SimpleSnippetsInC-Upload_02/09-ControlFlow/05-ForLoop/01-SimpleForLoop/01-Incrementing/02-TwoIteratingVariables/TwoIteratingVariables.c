#include<stdio.h>

int main(void)
{
	// variable declarations
	int i, j;

	// code
	printf("\nPrinting digits from 1 to 10 and 10 to 100 :\n\n");

	for (i = 1, j = 10;i <= 10, j <= 100;i++, j = j + 10)
	{
		printf("\n\t%d\t%d", i, j);
	}

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\05-ForLoop\01-SimpleForLoop\01-Incrementing\02-TwoIteratingVariables>TwoIteratingVariables.exe

Printing digits from 1 to 10 and 10 to 100 :


1       10
2       20
3       30
4       40
5       50
6       60
7       70
8       80
9       90
10      100

*/