#include<stdio.h>

int main(void)
{
	// variable declarations
	int i;

	// code
	printf("\nPrinting digits 1 to 10 :\n\n");

	for (i = 1;i <= 10;i++)
		printf("\t%d", i);

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\05-ForLoop\01-SimpleForLoop\01-Incrementing\01-SingleIteratingVariable>SingleIteratingVariable.exe

Printing digits 1 to 10 :

1       2       3       4       5       6       7       8       9       10
*/