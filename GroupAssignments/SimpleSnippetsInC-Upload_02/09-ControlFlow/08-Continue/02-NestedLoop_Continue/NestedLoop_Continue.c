#include<stdio.h>

int main(void)
{
	// variable declarations
	int i, j;

	// code
	printf("\nOuter loop prints odd numbers between 1 and 10");
	printf("\nInner loop prints even numbers between 1 and 10 for every odd number printed by outer loop");

	for (i = 0;i <= 10;i++)
	{
		if (i % 2 != 0)	// i is odd 
		{
			printf("\ni=%d", i);
			printf("\n");
			for (j = 0;j <= 10;j++)
			{
				if (j % 2 == 0)	// j is even
				{
					printf("\tj=%d", j);
				}
				else 	// j is odd
				{
					continue;
				}
			}
		}
		else // i is even 
		{
			continue;
		}
	}
	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\08-Continue\02-NestedLoop_Continue>NestedLoop_Continue.exe

Outer loop prints odd numbers between 1 and 10
Inner loop prints even numbers between 1 and 10 for every odd number printed by outer loop
i=1
j=0     j=2     j=4     j=6     j=8     j=10
i=3
j=0     j=2     j=4     j=6     j=8     j=10
i=5
j=0     j=2     j=4     j=6     j=8     j=10
i=7
j=0     j=2     j=4     j=6     j=8     j=10
i=9
j=0     j=2     j=4     j=6     j=8     j=10

*/