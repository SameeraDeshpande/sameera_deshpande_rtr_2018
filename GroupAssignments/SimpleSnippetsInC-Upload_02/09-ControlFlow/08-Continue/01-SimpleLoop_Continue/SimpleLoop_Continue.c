#include<stdio.h>

int main(void)
{
	// variable declarations
	int i;

	// code
	printf("\nPrinting even nos. from 0 to 100 :\n\n");

	for (i = 0;i <= 100;i++)
	{
		if (i % 2 != 0)
			continue;
		else
		printf("\n\t%d", i);
	}

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\08-Continue\01-SimpleLoop_Continue>SimpleLoop_Continue.exe

Printing even nos. from 0 to 100 :


0
2
4
6
8
10
12
14
16
18
20
22
24
26
28
30
32
34
36
38
40
42
44
46
48
50
52
54
56
58
60
62
64
66
68
70
72
74
76
78
80
82
84
86
88
90
92
94
96
98
100

*/