// problem
#include<stdio.h>
#include<conio.h>

int main(void)
{
	// variable declarations
	int i;
	char ch;

	// code
	printf("\nPrinting numbers from 0 to 100 for every user input. Exiting the loop when user enters 'q' or 'Q'");

	for (i = 0;i <= 100;i++)	// getting executed twice consecutively
	{
		printf("\ni=%d\n", i);

		printf("\nEnter a character : ");	
		ch = getch();	// problem

		if (ch == 'q' || ch == 'Q')
			break;
	}
	printf("\nExiting loop");
	printf("\n");
	return(0);
}

/*
Output-
Printing numbers from 0 to 100 for every user input. Exiting the loop when user enters 'q' or 'Q'
i=0

Enter a character :
i=1

Enter a character :
i=2

Enter a character :
i=3

Enter a character :
i=4

Enter a character :
i=5

Enter a character :
i=6

Enter a character :
Exiting loop

*/