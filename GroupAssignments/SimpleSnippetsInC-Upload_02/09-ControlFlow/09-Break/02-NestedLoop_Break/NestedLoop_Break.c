#include<stdio.h>
#include<conio.h>

int main(void)
{
// variable declarations
	int i, j;

	// code
	for (i = 0;i < 20;i++)
	{
		for (j = 0;j < 20;j++)
		{
			if (j > i)
				break;
			else
				printf("* ");
		}
		printf("\n");
	}

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\09-Break\02-NestedLoop_Break>NestedLoop_Break.exe
*
* *
* * *
* * * *
* * * * *
* * * * * *
* * * * * * *
* * * * * * * *
* * * * * * * * *
* * * * * * * * * *
* * * * * * * * * * *
* * * * * * * * * * * *
* * * * * * * * * * * * *
* * * * * * * * * * * * * *
* * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * *
* * * * * * * * * * * * * * * * * * * *
*/