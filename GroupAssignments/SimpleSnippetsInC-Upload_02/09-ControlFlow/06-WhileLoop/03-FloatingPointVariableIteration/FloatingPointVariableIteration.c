#include<stdio.h>

int main(void)
{
	// variable declarations
	float f, f_num = 1.7f;

	// code
	printf("\nPrinting numbers %f to %f : \n\n", f_num, f_num*10.0f);
	f = f_num;
	while (f <= f_num * 10.0f)
	{
		printf("\n\t%f", f);
		f = f + f_num;
	}
	printf("\n");
	return(0);
}
/*
Output-

Printing numbers 1.700000 to 17.000000 :


1.700000
3.400000
5.100000
6.800000
8.500000
10.200000
11.900000
13.599999
15.299999
17.000000

*/