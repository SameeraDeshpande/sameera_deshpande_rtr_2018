#include<stdio.h>

int main(void)
{
	// variable declarations
	int i;

	// code
	printf("\nPrinting digits from 10 to 1:\n");
	i = 10;
	
	while (i >= 1)
	{
		printf("\n\t%d", i);
		i--;
	}
	printf("\n");
	return(0);
}
/*
Output-
Printing digits from 10 to 1:

10
9
8
7
6
5
4
3
2
1

*/