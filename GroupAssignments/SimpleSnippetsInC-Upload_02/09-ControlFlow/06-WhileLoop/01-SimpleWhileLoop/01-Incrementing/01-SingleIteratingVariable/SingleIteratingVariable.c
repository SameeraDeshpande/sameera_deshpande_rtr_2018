#include<stdio.h>

int main(void)
{
	// variable declarations
	int i;

	// code
	printf("\nPrinting digits from 1 to 10 :\n");
	i = 1;
	while (i <= 10)
	{
		printf("\n\t%d", i);
		i++;
	}
	printf("\n");
	return(0);
}
/*
Output-
Printing digits from 1 to 10 :

1
2
3
4
5
6
7
8
9
10

*/