#include<stdio.h>

int main(void)
{
	// variable declarations
	int i, j,k;

	// code
	i = 1;
	while (i <= 6)
	{
		printf("\n\ni= %d", i);
		j = 1;
		while (j <= 5)
		{
			printf("\n\tj= %d", j);
			k = 1;
			while (k <= 3)
			{
				printf("\n\t\tk= %d", k);
				k++;
			}
			j++;
		}
		i++;
	}

	printf("\n");
	return(0);
}

/*
Output-
i= 1

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3

i= 2

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3

i= 3

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3

i= 4

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3

i= 5

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3

i= 6

j= 1

k= 1

k= 2

k= 3

j= 2

k= 1

k= 2

k= 3

j= 3

k= 1

k= 2

k= 3

j= 4

k= 1

k= 2

k= 3

j= 5

k= 1

k= 2

k= 3
*/