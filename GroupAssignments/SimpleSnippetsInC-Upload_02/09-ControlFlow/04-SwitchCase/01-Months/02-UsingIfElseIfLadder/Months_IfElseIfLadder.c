#include<stdio.h>

int main(void)
{
	// variable declarations
	int num_month;

	//code
	printf("\nEnter number of month (0-12) : ");
	scanf("%d", &num_month);

	if(num_month==1)	// like 'case 1'
		printf("\nMonth no. %d is January", num_month);
	else if (num_month == 2)	// like 'case 2'
		printf("\nMonth no. %d is February", num_month);
	else if (num_month == 3)	// like 'case 3'
		printf("\nMonth no. %d is March", num_month);
	else if (num_month == 4)	// like 'case 4'
		printf("\nMonth no. %d is april", num_month);
	else if (num_month == 5)	// like 'case 5'
		printf("\nMonth no. %d is May", num_month);
	else if (num_month == 6)	// like 'case 6'
		printf("\nMonth no. %d is June", num_month);
	else if (num_month == 7)	// like 'case 7'
		printf("\nMonth no. %d is July", num_month);
	else if (num_month == 8)	// like 'case 8'
		printf("\nMonth no. %d is August", num_month);
	else if (num_month == 9)	// like 'case 9'
		printf("\nMonth no. %d is September", num_month);
	else if (num_month == 10)	// like 'case 10'
		printf("\nMonth no. %d is October", num_month);
	else if (num_month == 11)	// like 'case 11'
		printf("\nMonth no. %d is November", num_month);
	else if (num_month == 12)	// like 'case 12'
		printf("\nMonth no. %d is December", num_month);
	else // like 'default'; else of if-else if ladder is also optional
		printf("\nInvalid month %d entered. Pl. try again", num_month);

	printf("\nif-else if ladder complete");
	printf("\n");
	return(0);
}
/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\04-SwitchCase\01-Months\02-UsingIfElseIfLadder>Months_IfElseIfLadder.exe

Enter number of month (0-12) : 7

Month no. 7 is July
if-else if ladder complete

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\04-SwitchCase\01-Months\02-UsingIfElseIfLadder>Months_IfElseIfLadder.exe

Enter number of month (0-12) : 44

Invalid month 44 entered. Pl. try again
if-else if ladder complete
*/