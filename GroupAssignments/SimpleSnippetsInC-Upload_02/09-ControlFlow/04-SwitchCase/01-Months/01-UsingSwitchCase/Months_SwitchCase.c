#include<stdio.h>

int main(void)
{
	// variable declarations
	int num_month;

	//code
	printf("\nEnter number of month (0-12) : ");
	scanf("%d", &num_month);

	switch (num_month)
	{
	case 1:	// like 'if'
		printf("\nMonth no. %d is January", num_month);
		break;
	case 2:	// like 'else if'
		printf("\nMonth no. %d is February", num_month);
		break;
	case 3:	// like 'else if'
		printf("\nMonth no. %d is March", num_month);
		break;
	case 4:	// like 'else if'
		printf("\nMonth no. %d is April", num_month);
		break;
	case 5:	// like 'else if'
		printf("\nMonth no. %d is May", num_month);
		break;
	case 6:	// like 'else if'
		printf("\nMonth no. %d is June", num_month);
		break;
	case 7:	// like 'else if'
		printf("\nMonth no. %d is July", num_month);
		break;
	case 8:	// like 'else if'
		printf("\nMonth no. %d is August", num_month);
		break;
	case 9:	// like 'else if'
		printf("\nMonth no. %d is September", num_month);
		break;
	case 10:	// like 'else if'
		printf("\nMonth no. %d is October", num_month);
		break;
	case 11:	// like 'else if'
		printf("\nMonth no. %d is November", num_month);
		break;
	case 12:	// like 'else if'
		printf("\nMonth no. %d is December", num_month);
		break;
	default:	// like optional 'else'; default is also optional
		printf("\nInvalid month %d entered. Pl. try again", num_month);
		break;
	}
	printf("\nswitch-case complete");
	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\04-SwitchCase\01-Months\01-UsingSwitchCase>Months_SwitchCase.exe

Enter number of month (0-12) : 6

Month no. 6 is June
switch-case complete

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\04-SwitchCase\01-Months\01-UsingSwitchCase>Months_SwitchCase.exe

Enter number of month (0-12) : 79

Invalid month 79 entered. Pl. try again
switch-case complete
*/