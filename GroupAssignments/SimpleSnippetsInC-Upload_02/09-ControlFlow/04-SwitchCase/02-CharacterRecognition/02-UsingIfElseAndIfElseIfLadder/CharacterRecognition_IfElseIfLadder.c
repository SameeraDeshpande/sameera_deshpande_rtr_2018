#include<stdio.h>	// for printf()
#include<conio.h>	// for getch()

// ASCII Values For 'A' to 'Z' => 65 to 90
#define CHAR_ALPHABET_UPPERCASE_BEGINNING 65
#define CHAR_ALPHABET_UPPERCASE_ENDING 90

// ASCII Values For 'a' to 'z' => 97 to 122
#define CHAR_ALPHABET_LOWERCASE_BEGINNING 97
#define CHAR_ALPHABET_LOWERCASE_ENDING 122

// ASCII Values For '0' to '9' => 48 to 57
#define CHAR_DIGIT_BEGINNING 48
#define CHAR_DIGIT_ENDING 57

int main(void)
{
	// variable declarations
	char ch;
	int ch_value;

	// code
	printf("\nEnter a character : ");
	ch = getch();

	if ((ch == 'A' || ch == 'a') || (ch == 'E' || ch == 'e') || (ch == 'I' || ch == 'i') || (ch == 'O' || ch == 'o') || (ch == 'U' || ch == 'u'))
	{
		printf("\nCharacter \'%c\' is a vowel character in the English alphabet", ch);
	}
	else
	{
		ch_value = (int)ch;

		//If The Character Has ASCII Value Between 65 AND 90 OR Between 97 AND 122, It Is Still A Letter Of The Alphabet, But It Is A 'CONSONANT', and NOT a 'VOWEL'...
		if ((ch_value >= CHAR_ALPHABET_UPPERCASE_BEGINNING && ch_value <= CHAR_ALPHABET_UPPERCASE_ENDING) || (ch_value >= CHAR_ALPHABET_LOWERCASE_BEGINNING && ch_value <= CHAR_ALPHABET_LOWERCASE_ENDING))
		{
			printf("\nCharacter \'%c\' is a constant character in the English alphabet", ch);
		}
		else if (ch_value >= CHAR_DIGIT_BEGINNING && ch_value <= CHAR_DIGIT_ENDING)
		{
			printf("\nCharacter \'%c\' is a digit character", ch);
		}
		else
		{
			printf("\nCharacter \'%c\' is a special character", ch);
		}
	}
	return(0);
}
/*Output-
Enter a character :
Character 'u' is a vowel character in the English alphabet
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\04-SwitchCase\02-CharacterRecognition\02-UsingIfElseAndIfElseIfLadder>CharacterRecognition_IfElseIfLadder.exe

Enter a character :
Character '7' is a digit character
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\04-SwitchCase\02-CharacterRecognition\02-UsingIfElseAndIfElseIfLadder>CharacterRecognition_IfElseIfLadder.exe

Enter a character :
Character '/' is a special character
*/