// not working for division

#include<stdio.h>	// for printf(),scanf()
#include<conio.h>	// for getch()

int main()
{
	// variabledeclarations
	int x, y, result;
	char choice, choice_division;

	// code
	printf("\nEnter value of X : ");
	scanf("%d", &x);

	printf("\nEnter value of Y : ");
	scanf("%d", &y);

	printf("\nEnter option in character : ");
	printf("\n'A' or 'a' for addition : ");
	printf("\n'S' or 's' for subtraction : ");
	printf("\n'M' or 'm' for multiplication : ");
	printf("\n'D' or 'd' for division : ");

	printf("\n\nEnter your choice : ");
	choice = getch();

	printf("\n\n");

	if (choice == 'A' || choice == 'a')
	{
		result = x + y;
		printf("\nAddition of X=%d and Y=%d is %d", x, y, result);
	}

	else if (choice == 'S' || choice == 's')
	{
		if (x >= y)
		{
			result = x - y;
			printf("\nSubtracting Y=%d from X=%d gives %d", y, x, result);
		}
		else
		{
			result = y - x;
			printf("\nSubtracting X=%d from Y=%d gives %d", x, y, result);
		}
	}

	else if (choice == 'M' || choice == 'm')
	{
		result = x * y;
		printf("\nMultiplication of X=%d and Y=%d is %d", x, y, result);
	}

	else if (choice == 'D' || choice == 'd')
	{

		printf("\nEnter choice in character : ");
		printf("'Q' or 'q' or'/' for quotient on division");
		printf("\t|\t'R' or 'r' or '%%' for remainder on division");

		printf("\nEnter choice : ");
		choice_division = getch();	// not working :(
		printf("\n\n");

		if (choice_division == 'Q' || choice_division == 'q' || choice_division == '/')
		{
			if (x >= y)
			{
				result = x / y;
				printf("\nDivision of X=%d and Y=%d gives quotient %d", x, y, result);
			}
			else
			{
				result = y / x;
				printf("\nDivision of Y=%d and X=%d gives quotient %d", y, x, result);
			}
		}
		else if (choice_division == 'R' || choice_division == 'r' || choice_division == '%')
		{
			if (x >= y)
			{
				result = x % y;
				printf("\nDivision of X=%d and Y=%d gives remainder %d", x, y, result);
			}
			else
			{
				result = y % x;
				printf("\nDivision of Y=%d and X=%d gives remainder %d", y, x, result);
			}
		}
		else
		{
			printf("\nInvalid character %c entered for division", choice_division);
		}
	}
	else
	{
		printf("\nInvalid character %c entered", choice);
	}
	printf("\nSwitch-block complete");
	printf("\n");
	return(0);
}
