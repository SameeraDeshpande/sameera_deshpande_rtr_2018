// not working for division

#include<stdio.h>	// for printf()
#include<conio.h>	// for getch()

int main()
{
	// variabledeclarations
	int x, y, result;
	char choice, choice_division;

	// code
	printf("\nEnter value of X : ");
	scanf("%d", &x);

	printf("\nEnter value of Y : ");
	scanf("%d", &y);

	printf("\nEnter option in character : ");
	printf("\n'A' or 'a' for addition : ");
	printf("\n'S' or 's' for subtraction : ");
	printf("\n'M' or 'm' for multiplication : ");
	printf("\n'D' or 'd' for division : ");

	printf("\n\nEnter your choice : ");
	choice = getch();

	switch (choice)
	{
	// fall through for 'A' and 'a'
	case 'A':
	case 'a':
		result = x + y;
		printf("\nAddition of X=%d and Y=%d is %d", x, y, result);
		break;

	// fall through for 'S' and 's'
	case 'S':
	case 's':
		if (x >= y)
		{
			result = x - y;
			printf("\nSubtracting Y=%d from X=%d gives %d", y, x, result);
		}
		else
		{
			result = y - x;
			printf("\nSubtracting X=%d from Y=%d gives %d", x, y, result);
		}
		break;

	// fall through for 'M' and 'm'
	case 'M':
	case 'm':
		result = x * y;
		printf("\nMultiplication of X=%d and Y=%d is %d", x, y, result);
		break;

	// fall through for 'D' and 'd'
	case 'D':
	case 'd':
		printf("\nEnter choice in character : ");
		printf("'Q' or 'q' or'/' for quotient on division");
		printf("\t|\t'R' or 'r' or '%%' for remainder on division");

		printf("\nEnter choice : ");
		choice_division = getch();	// not working :(

		switch (choice_division)
		{
		case 'Q':
		case 'q':
		case '/':
			if (x >= y)
			{
				result = x / y;
				printf("\nDivision of X=%d and Y=%d gives quotient %d", x, y, result);
			}
			else
			{
				result = y / x;
				printf("\nDivision of Y=%d and X=%d gives quotient %d", y, x, result);
			}
			break;

		case 'R':
		case 'r':
	//	case '%%':
			if (x >= y)
			{
				result = x % y;
				printf("\nDivision of X=%d and Y=%d gives remainder %d", x, y, result);
			}
			else
			{
				result = y % x;
				printf("\nDivision of Y=%d and X=%d gives remainder %d", x, y, result);
			}
			break;
		
		default:
			printf("\nInvalid character %c entered for division", choice_division);
			break;
		}
		break;

	default:
		printf("\nInvalid character %c entered", choice);
		break;

	}
	printf("\nSwitch-block complete");
	printf("\n");
	return(0);
}
