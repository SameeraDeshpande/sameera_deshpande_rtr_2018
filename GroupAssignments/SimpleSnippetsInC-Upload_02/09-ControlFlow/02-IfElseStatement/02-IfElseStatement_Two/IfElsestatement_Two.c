#include<stdio.h>

int main(void)
{
	// variable declarations
	int age;

	// code
	printf("\nEnter age: ");
	scanf("%d", &age);

	if (age >= 18)
	{
		printf("\nIn if block");
		printf("\nEligible to vote");
	}
	else
	{
		printf("\nIn else block");
		printf("\nNot eligible to vote");
	}
	printf("\n");
	return(0);
}
/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\02-IfElseStatement\02-IfElseStatement_Two>IfElseStatement_two.exe

Enter age: 7

In else block
Not eligible to vote

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\02-IfElseStatement\02-IfElseStatement_Two>IfElseStatement_two.exe

Enter age: 55

In if block
Eligible to vote
*/