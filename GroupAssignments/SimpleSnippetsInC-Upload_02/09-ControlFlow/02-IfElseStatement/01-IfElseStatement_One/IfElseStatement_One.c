#include<stdio.h>

int main(void)
{
	// variable declarations
	int x = 9;
	int y = 40;
	int z = 40;

	// first if-else pair
	if (x < y)
	{
		printf("\nIn first if block");
		printf("\nx is less than y");
	}
	else
	{
		printf("\nIn first else block");
		printf("\nx is not less than y");
	}
	printf("\nFirst if-else block done");

	// second if-else pair
	if (y!=z)
	{
		printf("\nIn second if block");
		printf("\ny is not equal to z");
	}
	else
	{
		printf("\nIn second else block");
		printf("\ny is equal to z");
	}
	printf("\nSecond if-else block done");

	printf("\n");
	return(0);
}
/*Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\02-IfElseStatement\01-IfElseStatement_One>IfElseStatement_one.exe

In first if block
x is less than y
First if-else block done
In second else block
y is equal to z
Second if-else block done
*/