#include<stdio.h>

int main(void)
{
	//variable declarations
	int num;

	// code
	printf("\nEnter value of 'num' : ");
	scanf("%d", &num);

	if (num < 0)	// 'if' no. 1
		printf("\nnum= %d is less then 0 (negative)", num);
	else // 'else' no. 1
	{

		if (num >= 0 && num <= 100)	// 'if' no. 2
			printf("\nnum= %d lies from 0 to 100", num);
		else // 'else' no. 2
		{
			if (num > 100 && num <= 200)	// 'if' no. 3
				printf("\nnum= %d lies from 100 to 200", num);
			else // 'else' no. 3
			{
				if (num > 200 && num <= 300)	// 'if' no. 4
					printf("\nnum= %d lies from 200 to 300", num);
				else // 'else' no. 4
				{
					if (num > 300 && num <= 400)	// 'if' no. 5
						printf("\nnum= %d lies from 300 to 400", num);
					else // 'else' no. 5
					{
						if (num > 400 && num <= 500)	// 'if' no. 6
							printf("\nnum= %d lies from 400 to 500", num);
						else // 'else' no. 6
							printf("\nnum= %d is greater than 500", num);

					} // closing bracket of 'else' no. 5
				} // closing bracket of 'else' no. 4
			} // closing bracket of 'else' no. 3
		} // closing bracket of 'else' no. 2
	} // closing bracket of 'else' no. 1
	printf("\n");
	return(0);
}
	/*
	Output-
	C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\02-IfElseStatement\03-NeedFor_IfElseIfLadder>IfElseStatement_Three.exe

	Enter value of 'num' : 593

	num= 593 is greater than 500

	C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\02-IfElseStatement\03-NeedFor_IfElseIfLadder>IfElseStatement_Three.exe

	Enter value of 'num' : 277

	num= 277 lies from 200 to 300
	*/