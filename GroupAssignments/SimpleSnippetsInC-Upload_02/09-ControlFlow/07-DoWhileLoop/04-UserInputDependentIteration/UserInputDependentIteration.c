#include<stdio.h>

int main(void)
{
	// variable declarations
	int i_num, count, i;

	// code
	printf("\nEnter an integer from which iteration must begin : ");
	scanf("%d", &i_num);

	printf("\nHow many numbers do you want to print from %d onwards? : ", i_num);
	scanf("%d", &count);

	printf("\nPrinting numbers %d to %d : ", i_num, i_num + count);

	i = i_num;
	do
	{
			printf("\n\t%d", i);
			i++;
		}	while (i <= i_num + count);

		printf("\n");
		return(0);
	}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\05-ForLoop\04-UserInputDependentIteration>UserInputDependentIteration.exe

Enter an integer from which iteration must begin : 5

How many numbers do you want to print from 5 onwards? : 6

Printing numbers 5 to 11 :
5
6
7
8
9
10
11

*/