#include<stdio.h>

int main(void)
{
	// variable declarations
	int i;

	// code
	printf("\nPrinting digits from 10 to 1:\n");
	i = 10;
	
	do
	{
		printf("\n\t%d", i);
		i--;
	}	while (i >= 1);

	printf("\n");
	return(0);
}
/*
Output-
Printing digits from 10 to 1:

10
9
8
7
6
5
4
3
2
1

*/