#include<stdio.h>

int main(void)
{
	// variable declarations
	int i,j;

	// code
	printf("\nPrinting digits from 10 to 1 and 100 to 10 :\n");
	i = 10;
	j = 100;

	do
	{
		printf("\n\t%d\t%d", i, j);
		i--;
		j -= 10;
	}	while (i >= 1 , j > 10);	// ',' works like '&&'

	printf("\n");
	return(0);
}
/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\06-WhileLoop\01-SimpleWhileLoop\02-Decrementing\02-TwoIteratingvariables>TwoIteratingVariables.exe

Printing digits from 10 to 1 and 100 to 10 :

10      100
9       90
8       80
7       70
6       60
5       50
4       40
3       30
2       20
1       10

*/