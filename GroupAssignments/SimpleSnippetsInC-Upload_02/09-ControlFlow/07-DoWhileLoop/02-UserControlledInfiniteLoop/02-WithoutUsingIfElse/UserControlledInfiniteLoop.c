//problem

#include<stdio.h>

int main(void)
{
	// variable declarations
	char choice, ch;

	// code
	printf("\nWhen the infinite loop begins, enter 'Q' or 'q' to exit the infinite 'for' loop");
	printf("\n\n");

	do
	{
		do
		{
				printf("In loop\n");	// ? getting printed twice 
				ch = getch();

		}while (ch!='Q'&&ch!='q');	// infinite loop

		printf("\nExiting user controlled infinite loop");
		printf("\nDo you want to begin the user-controlled loop again? (y/n) : ");
		choice = getch();	// ? not working

	}while (choice == 'Y' || choice == 'y');
	
	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\07-DoWhileLoop\02-UserControlledInfiniteLoop\02-WithoutUsingIfElse>UserControlledInfiniteLoop.exe

When the infinite loop begins, enter 'Q' or 'q' to exit the infinite 'for' loop

In loop
In loop
In loop
In loop
In loop

Exiting user controlled 'for' loop
Do you want to begin the user-controlled loop again? (y/n) :

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\07-DoWhileLoop\02-UserControlledInfiniteLoop\02-WithoutUsingIfElse>
*/