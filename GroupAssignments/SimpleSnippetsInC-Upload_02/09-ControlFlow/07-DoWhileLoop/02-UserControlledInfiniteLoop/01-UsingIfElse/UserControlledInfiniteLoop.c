//problem

#include<stdio.h>

int main(void)
{
	// variable declarations
	char choice, ch = '\0';

	// code
	printf("\nWhen the infinite loop begins, enter 'Q' or 'q' to exit the infinite 'for' loop");
	printf("\n\nEnter 'Y' or'y' to start the user controlled infinite loop : ");
	printf("\n\n");
	choice = getch();

	if (choice == 'Y' || choice == 'y')
	{
		do
		{
			printf("In loop\n");	// getting printed twice ?
			ch = getch();

			if (ch == 'Q' || ch == 'q')
			{
				printf("\nExiting user controlled 'for' loop");
				break;	// user controlled exit from 'for' loop
			}
		}while (1);	// infinite loop
	}
	else
	{
		printf("\nYou must press 'Y' or 'y' to initiate the loop");
	}
	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\06-WhileLoop\02-UserControlledInfiniteLoop>UserControlledInfiniteLoop.exe

When the infinite loop begins, enter 'Q' or 'q' to exit the infinite 'for' loop

Enter 'Y' or'y' to start the user controlled infinite loop :


You must press 'Y' or 'y' to initiate the loop

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\06-WhileLoop\02-UserControlledInfiniteLoop>UserControlledInfiniteLoop.exe

When the infinite loop begins, enter 'Q' or 'q' to exit the infinite 'for' loop

Enter 'Y' or'y' to start the user controlled infinite loop :

In loop
In loop
In loop
In loop
In loop
In loop

Exiting user controlled 'for' loop
*/