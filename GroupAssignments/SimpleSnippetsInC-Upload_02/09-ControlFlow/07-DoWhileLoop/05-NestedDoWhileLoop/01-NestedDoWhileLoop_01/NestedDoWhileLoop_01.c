#include<stdio.h>

int main(void)
{
	// variable declarations
	int i, j;

	// code
	i = 1;
	do	
	{
		printf("\n\ni= %d", i);
		j = 1;
		do
		{
			printf("\n\tj= %d", j);
			j++;
		}while (j <= 5);

		i++;
	}while (i <= 6);

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_02\09-ControlFlow\05-ForLoop\05-NestedForLoop\01-NestedForLoop_01>NestedForLoop_01.exe


i= 1
j= 1
j= 2
j= 3
j= 4
j= 5

i= 2
j= 1
j= 2
j= 3
j= 4
j= 5

i= 3
j= 1
j= 2
j= 3
j= 4
j= 5

i= 4
j= 1
j= 2
j= 3
j= 4
j= 5

i= 5
j= 1
j= 2
j= 3
j= 4
j= 5

i= 6
j= 1
j= 2
j= 3
j= 4
j= 5

*/