#include<stdio.h>	// stdio.hcontains declaration of printf()

int main(int argc)	// entry point function => main(): valid return type (int), one parameter(int argc) 
{
	// code
	printf("\nHello World !!!");	// library function
	printf("\nNo. of command line arguments= %d", argc);
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\01-EntryPointFunctions\03-Type_03>EntryPointFunction.exe

Hello World !!!
No. of command line arguments= 1
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\01-EntryPointFunctions\03-Type_03>EntryPointFunction.exe Sameera

Hello World !!!
No. of command line arguments= 2
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\01-EntryPointFunctions\03-Type_03>EntryPointFunction.exe Sameera Deshpande

Hello World !!!
No. of command line arguments= 3
*/