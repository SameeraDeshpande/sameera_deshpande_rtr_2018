#include<stdio.h>	// stdio.hcontains declaration of printf()

void main(void)	// entry point function => main() Has no return type (void), no parameters(void) hence no CLA
{
// code
	printf("\nHello World !!!");	// library function
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\01-EntryPointFunctions\01-Type_01>EntryPointFunction.exe

Hello World !!!
*/