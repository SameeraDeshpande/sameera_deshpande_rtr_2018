#include<stdio.h>	// contains declaration of 'printf()'
#include<ctype.h>	// contains declaration of 'atoi()'

int main(int argc, char *argv[], char *envp[])
{
	// variable declarations
	int i, num, sum = 0;

	// code
	// this program adds all CLAs given in integer form only & outputs the sum
	// *** due to atoi(), all CLAs of types other than 'int' are ignored ***

	printf("\nSum of all integer command line arguments is : ");
	for (i = 1;i < argc;i++)	// argv[0] is program name
	{
		num = atoi(argv[i]);
		sum = sum + num;
	}
	printf("%d", sum);
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\01-EntryPointFunctions\06-CLA_Application>CLA_Application.exe Sameera 40 30

Sum of all integer command line arguments is : 70
*/