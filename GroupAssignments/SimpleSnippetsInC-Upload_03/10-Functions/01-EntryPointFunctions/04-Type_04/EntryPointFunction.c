#include<stdio.h>	// stdio.h contains declaration of printf()

int main(int argc,char *argv[])	// entry point function => main(): valid return type (int), 2 parameters(int argc,char *argv[]) 
{
	// variable declarations
	int i;

	// code
	printf("\nHello World !!!");	// library function
	printf("\nNo. of command line arguments= %d", argc);
	printf("\nCommand Line Arguments passed to this program are : ");
	for (i = 0;i < argc;i++)
		printf("\nCommand Line Argument no. %d = %s", i, argv[i]);
	
	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\01-EntryPointFunctions\04-Type_04>EntryPointFunction.exe

Hello World !!!
No. of command line arguments= 1
Command Line Arguments passed to this program are :
Command Line Argument no. 0 = EntryPointFunction.exe

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\01-EntryPointFunctions\04-Type_04>EntryPointFunction.exe Sameera

Hello World !!!
No. of command line arguments= 2
Command Line Arguments passed to this program are :
Command Line Argument no. 0 = EntryPointFunction.exe
Command Line Argument no. 1 = Sameera

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\01-EntryPointFunctions\04-Type_04>EntryPointFunction.exe Sameera Deshpande

Hello World !!!
No. of command line arguments= 3
Command Line Arguments passed to this program are :
Command Line Argument no. 0 = EntryPointFunction.exe
Command Line Argument no. 1 = Sameera
Command Line Argument no. 2 = Deshpande
*/