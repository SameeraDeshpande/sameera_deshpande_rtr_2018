#include<stdio.h>	// stdio.hcontains declaration of printf()

int main(void)	// entry point function => main(): valid return type (int), no parameters(void) hence no CLA
{
	// code
	printf("\nHello World !!!");	// library function
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\01-EntryPointFunctions\01-Type_01>EntryPointFunction.exe

Hello World !!!
*/