#include<stdio.h>	// stdio.h contains declaration of printf()

int main(int argc, char *argv[],char *envp[])	// entry point function => main(): valid return type (int), 3 parameters(int argc,char *argv[],char *envp[]) 
{
	// variable declarations
	int i;

	// code
	printf("\nHello World !!!");	// library function
	printf("\n\nNo. of command line arguments= %d", argc);

	printf("\n\nCommand Line Arguments passed to this program are : ");
	for (i = 0;i < argc;i++)
		printf("\nCommand Line Argument no. %d = %s", i, argv[i]);

	printf("\n\nFirst 20 Command Line Arguments passed to this program are : ");
	for (i = 0;i < 20;i++)
		printf("\nCommand Line Argument no. %d = %s", i, envp[i]);

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\01-EntryPointFunctions\05-FullFledgedEntryPointFunctionWithAllCLA>EntryPointFunction.exe Sameera Deshpande

Hello World !!!

No. of command line arguments= 3

Command Line Arguments passed to this program are :
Command Line Argument no. 0 = EntryPointFunction.exe
Command Line Argument no. 1 = Sameera
Command Line Argument no. 2 = Deshpande

First 20 Command Line Arguments passed to this program are :
Command Line Argument no. 0 = ALLUSERSPROFILE=C:\ProgramData
Command Line Argument no. 1 = APPDATA=C:\Users\SAMEERA\AppData\Roaming
Command Line Argument no. 2 = CommandPromptType=Native
Command Line Argument no. 3 = CommonProgramFiles=C:\Program Files (x86)\Common Files
Command Line Argument no. 4 = CommonProgramFiles(x86)=C:\Program Files (x86)\Common Files
Command Line Argument no. 5 = CommonProgramW6432=C:\Program Files\Common Files
Command Line Argument no. 6 = COMPUTERNAME=LAPTOP-FKCDACNM
Command Line Argument no. 7 = ComSpec=C:\WINDOWS\system32\cmd.exe
Command Line Argument no. 8 = DevEnvDir=C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\
Command Line Argument no. 9 = DriverData=C:\Windows\System32\Drivers\DriverData
Command Line Argument no. 10 = ExtensionSdkDir=C:\Program Files (x86)\Microsoft SDKs\Windows Kits\10\ExtensionSDKs
Command Line Argument no. 11 = FPS_BROWSER_APP_PROFILE_STRING=Internet Explorer
Command Line Argument no. 12 = FPS_BROWSER_USER_PROFILE_STRING=Default
Command Line Argument no. 13 = Framework40Version=v4.0
Command Line Argument no. 14 = FrameworkDir=C:\Windows\Microsoft.NET\Framework\
Command Line Argument no. 15 = FrameworkDir32=C:\Windows\Microsoft.NET\Framework\
Command Line Argument no. 16 = FrameworkVersion=v4.0.30319
Command Line Argument no. 17 = FrameworkVersion32=v4.0.30319
Command Line Argument no. 18 = HOMEDRIVE=C:
Command Line Argument no. 19 = HOMEPATH=\Users\SAMEERA
*/