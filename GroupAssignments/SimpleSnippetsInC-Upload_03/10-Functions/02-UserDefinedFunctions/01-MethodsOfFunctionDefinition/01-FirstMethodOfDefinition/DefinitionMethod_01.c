#include<stdio.h>

// entry point function
int main(int args, char *argv[], char *envp[])
{
	// function prototype/declaration/signature
	void MyAddition(void);

	// code
	MyAddition();	// function call
	return(0);
}

// user defined function
// method 1 of definition- no return value no parameters

void MyAddition(void)	// function definition
{
	// variable declarations
	int a, b, sum;

	// code
	printf("\nEnter integer value of A : ");
	scanf("%d", &a);
	printf("\nEnter integer value of B : ");
	scanf("%d", &b);

	sum = a + b;

	printf("\n\nSum of A= %d and B= %d is %d\n",a, b, sum);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\02-UserDefinedFunctions\01-MethodsOfFunctionDefinition\01-FirstMethodOfDefinition>DefinitionMethod_01.exe

Enter integer value of A : 7

Enter integer value of B : 8


Sum of A= 7 and B= 8 is 15

*/