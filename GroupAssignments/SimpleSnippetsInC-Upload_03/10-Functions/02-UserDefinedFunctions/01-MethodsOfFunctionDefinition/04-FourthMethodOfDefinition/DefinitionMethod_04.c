#include<stdio.h>

// entry point function
int main(int args, char *argv[], char *envp[])
{
	// function prototype/declaration/signature
	int MyAddition(int,int);
	
	// variable declarations
	int a, b,result;

	// code
	printf("\nEnter integer value of A : ");
	scanf("%d", &a);
	printf("\nEnter integer value of B : ");
	scanf("%d", &b);

	result=MyAddition(a,b);	// function call

	printf("\n\nSum of A=%d and B=%d is %d", a, b, result);

	printf("\n");
	return(0);
}

// user defined function
// method 3 of definition- no return value, valid parameters(int,int)

int MyAddition(int a, int b)	// function definition
{
	// variable declarations
	int sum;

	sum = a + b;

	return(sum);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\02-UserDefinedFunctions\01-MethodsOfFunctionDefinition\04-FourthMethodOfDefinition>DefinitionMethod_04.exe

Enter integer value of A : 7

Enter integer value of B : 0


Sum of A=7 and B=0 is 7


*/