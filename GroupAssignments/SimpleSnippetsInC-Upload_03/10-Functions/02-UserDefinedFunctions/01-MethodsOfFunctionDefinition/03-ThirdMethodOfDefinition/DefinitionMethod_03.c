#include<stdio.h>

// entry point function
int main(int args, char *argv[], char *envp[])
{
	// function prototype/declaration/signature
	void MyAddition(int,int);
	
	// variable declarations
	int a, b;

	// code
	printf("\nEnter integer value of A : ");
	scanf("%d", &a);
	printf("\nEnter integer value of B : ");
	scanf("%d", &b);

	MyAddition(a,b);	// function call

	printf("\n");
	return(0);
}

// user defined function
// method 3 of definition- no return value, valid parameters(int,int)

void MyAddition(int a, int b)	// function definition
{
	// variable declarations
	int sum;

	sum = a + b;

	printf("\n\nSum of A=%d and B=%d is %d",a,b, sum);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\02-UserDefinedFunctions\01-MethodsOfFunctionDefinition\03-ThirdMethodOfDefinition>DefinitionMethod_03.exe

Enter integer value of A : 6

Enter integer value of B : 90


Sum of A=6 and B=90 is 96

*/