#include<stdio.h>

// entry point function
int main(int args, char *argv[], char *envp[])
{
	// function prototype/declaration/signature
	int MyAddition(void);

	// variable declarations
	int result;

	// code
	result=MyAddition();	// function call

	printf("\n\nSum is %d",result);

	printf("\n");
	return(0);
}

// user defined function
// method 2 of definition- valid(int) return value, no parameters

int MyAddition(void)	// function definition
{
	// variable declarations
	int a, b, sum;

	// code
	printf("\nEnter integer value of A : ");
	scanf("%d", &a);
	printf("\nEnter integer value of B : ");
	scanf("%d", &b);

	sum = a + b;

	return(sum);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\02-UserDefinedFunctions\01-MethodsOfFunctionDefinition\02-SecondMethodOfDefinition>DefinitionMethod_02.exe

Enter integer value of A : 7

Enter integer value of B : 9


Sum is 16

*/