#include<stdio.h>

// user defined function
// method 2 of calling- calling only two methods in main(), rst of the functions trace their call indirectly to main()

int main(int argc, char *argv[], char *envp[])
{
	// function declarations
	void display_info(void);
	void func_country(void);

	// code
	display_info();	 // function call
	func_country();	// function call

	return(0);
}

// definitions of UDFs
void display_info(void)	// function definition
{
	// function prototypes
	void func_my(void);
	void func_name(void);
	void func_is(void);
	void func_first_name(void);
	void func_last_name(void);
	void func_of_amc(void);

	// code
	// function calls
	func_my();	// 'void' as parameter not allowed
	func_name();
	func_is();
	func_first_name();
	func_last_name();
	func_of_amc();
}

// function definitions
void func_my(void)
{
	// code
	printf("\nMy");
}

void func_name(void)
{
	// code
	printf("\nName");
}

void func_is(void)
{
	// code
	printf("\nIs");
}

void func_first_name(void)
{
	// code
	printf("\nSameera");
}

void func_last_name(void)
{
	// code
	printf("\nDeshpande");
}

void func_of_amc(void)
{
	// code
	printf("\nOf ASTROMEDICOMP");
}

void func_country(void)
{
	// code
	printf("\nI live in India");
}

/*

Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\02-UserDefinedFunctions\02-MethodsOfFunctionCall\02-SecondMethodOfCall>CallMethod_02.exe

My
Name
Is
Sameera
Deshpande
Of ASTROMEDICOMP
I live in India
*/