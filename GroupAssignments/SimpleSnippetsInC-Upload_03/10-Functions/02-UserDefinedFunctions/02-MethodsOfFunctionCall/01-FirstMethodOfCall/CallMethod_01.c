#include<stdio.h>

// user defined functions
// method 1 of calling function- calling all functions in main() directly

// entry point function
int main(int argc, char* argv[], char *envp[])
{
	// function prototypes/declarations/signatures
	void MyAddition(void);
	int MySubtraction(void);
	void MyMultiplication(int, int);
	int MyDivision(int, int);

	// variable declarations
	int result_sub;
	int a_mult, b_mult;
	int a_div, b_div, result_div;

	// code

	// addition
	MyAddition();	// function call

	// subtraction
	result_sub = MySubtraction();	// function call

	printf("\nSubtraction yeilds result %d", result_sub);

	// multiplication
	printf("\n\nEnter integer value of A for multiplication : ");
	scanf("%d", &a_mult);
	printf("\nEnter integer value of B for multiplication : ");
	scanf("%d", &b_mult);
	MyMultiplication(a_mult, b_mult);

	//division
	printf("\n\nEnter integer value of A for division : ");
	scanf("%d", &a_div);
	printf("\nEnter integer value of B for division : ");
	scanf("%d", &b_div);

	result_div = MyDivision(a_div, b_div);

	printf("\nDivision of A= %d and B= %d yeilds result %d", a_div,b_div,result_div);

	printf("\n");
	return(0);
}

// function definition of MyAddition()
void MyAddition()
{
	// variable declarations
	int a,b,sum;

	// code
	printf("\nEnter integer value of A for addition : ");
	scanf("%d", &a);
	printf("\nEnter integer value of B for addition : ");
	scanf("%d", &b);

	sum = a + b;

	printf("\nAddition of A= %d and B= %d yeilds result %d", a,b,sum);

}

// function definition of MySubtraction()
int MySubtraction()
{
	// variable declarations
	int a, b, sub;

	// code
	printf("\n\nEnter integer value of A for subtraction : ");
	scanf("%d", &a);
	printf("\nEnter integer value of B for subtraction : ");
	scanf("%d", &b);

	sub = a - b;

	return(sub);
}

// function definition of MyMultiplication()
void MyMultiplication(int a,int b)
{
	// variable declarations
	int mult;

	// code
	mult = a * b;

	printf("\nMultiplication of A= %d and B= %d yeilds result %d", a, b, mult);

}

// function definition of MyDivision()
int MyDivision(int a,int b)
{
	// variable declarations
	int div;

	// code
	div = a / b;

	return(div);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_03\10-Functions\02-UserDefinedFunctions\02-MethodsOfFunctionCall\01-FirstMethodOfCall>CallMethod_01.exe

Enter integer value of A for addition : 6

Enter integer value of B for addition : 9

Addition of A= 6 and B= 9 yeilds result 15

Enter integer value of A for subtraction : 7

Enter integer value of B for subtraction : 9

Subtraction yeilds result -2

Enter integer value of A for multiplication : 4

Enter integer value of B for multiplication : 5

Multiplication of A= 4 and B= 5 yeilds result 20

Enter integer value of A for division : 9

Enter integer value of B for division : 4

Division of A= 9 and B= 4 yeilds result 2
*/