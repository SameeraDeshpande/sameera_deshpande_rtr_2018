// Headers
#include<stdio.h>
#include<memory.h>	// memset()

int main(void)
{
	// function declarations
	void PrintCUDADeviceProperties(void);

	// code
	PrintCUDADeviceProperties();
}

void PrintCUDADeviceProperties(void)
{
	// function declarations
	int ConvertSMVersionNumberToCores(int, int);

	// code
	printf("\nCUDA INFORMATION :\n");

	cudaError_t ret_cuda_rt;
	int dev_count;
	ret_cuda_rt = cudaGetDeviceCount(&dev_count);
	if (ret_cuda_rt != cudaSuccess)
	{
		printf("\nCUDA Runtime API Error - cudaGetDeviceCount() Failed Due To %s. Exiting Now...", cudaGetErrorString(ret_cuda_rt));
	}
	else if (dev_count == 0)
	{
		printf("\nThere Is No CUDA Supporting GPU Device On This System : %d", dev_count);
		return;
	}
	else
	{
		printf("\nTotal Number Of CUDA Supporting GPU Device/Devices On This System : %d", dev_count);
		for (int i = 0;i < dev_count;i++)
		{
			cudaDeviceProp dev_prop;
			memset(&dev_prop, 0, sizeof(dev_prop));
			int driverVersion = 0, runtimeVersion = 0;

			ret_cuda_rt = cudaGetDeviceProperties(&dev_prop, i);
			if (ret_cuda_rt != cudaSuccess)
			{
				printf("\n%s in %s at line %d", cudaGetErrorString(ret_cuda_rt), __FILE__, __LINE__);
				return;
			}
			printf("\n");
			cudaDriverGetVersion(&driverVersion);
			cudaRuntimeGetVersion(&runtimeVersion);

			printf("\n================================================================================");
			printf("\n\n\t****** CUDA DRIVER AND RUNTIME INFORMATION *****\n");
			printf("\n================================================================================");
			printf("\nCUDA Driver Version		: %d.%d", driverVersion / 1000, (driverVersion % 100) / 10);
			printf("\nCUDA Runtime Version		: %d.%d", runtimeVersion / 1000, (runtimeVersion % 100) / 10);

			printf("\n================================================================================");
			printf("\n\n\t ***** GPU DEVICE GENERAL INFORMATION *****\n");
			printf("\n================================================================================");
			printf("\nGPU Device Number		: %d", i);
			printf("\nGPU Device Name		: %s", dev_prop.name);
			printf("\nGPU Device Compute Capability		: %d.%d", dev_prop.major, dev_prop.minor);
			printf("\nGPU Device Clock Rate		: ",dev_prop.clockRate);
			printf("\nGPU Device Type		: ");
			if (dev_prop.integrated)
				printf("Integrated (On-Board)");
			else
				printf("Discrete (card)");
			printf("\n");

			printf("\n================================================================================");
			printf("\n\n\t ***** GPU DEVICE MEMORY INFORMATION *****\n");
			printf("\n================================================================================");
			printf("\nGPU Device Total Memory		: %.0f GB = %.0f MB = %llu Bytes", ((float)dev_prop.totalGlobalMem / 1048576.0f) / 1024.0f, (float)dev_prop.totalGlobalMem / 1048576.0f, (unsigned long long)dev_prop.totalGlobalMem);
			printf("\nGPU Device Available Memory		: %lu Bytes", (unsigned long)dev_prop.totalConstMem);
			printf("\nGPU Device Host Memory Mapping Capability		: ");
			if (dev_prop.canMapHostMemory)
				printf("Yes");
			else
				printf("No");
			printf("\n");

			printf("\n================================================================================");
			printf("\n\n\t ***** GPU DEVICE MULTIPROCESSOR INFORMATION *****\n");
			printf("\n================================================================================");
			printf("\nGPU Device Number Of SMProcessors		: %d", dev_prop.multiProcessorCount);
			printf("\nGPU Device Number Of Cores Per SMProcessors		: %d", ConvertSMVersionNumberToCores(dev_prop.major, dev_prop.minor));
			printf("\nGPU Device Total Number Of Cores		: %d", ConvertSMVersionNumberToCores(dev_prop.major, dev_prop.minor) * dev_prop.multiProcessorCount);
			printf("\nGPU Device Shared Memory Per SMProcessor		: %lu", (unsigned long)dev_prop.sharedMemPerBlock);
			printf("\nGPU Device Number Of Registers Per SMProcessor		: %d", dev_prop.regsPerBlock);
			printf("\n");

			printf("\n================================================================================");
			printf("\n\n\t ***** GPU DEVICE THREAD INFORMATION *****\n");
			printf("\n================================================================================");
			printf("\nGPU Device Maximum Number Of Threads Per SMProcessors		: %d", dev_prop.maxThreadsPerMultiProcessor);
			printf("\nGPU Device Maximum Number Of Threads Per Block		: %d", dev_prop.maxThreadsPerBlock);
			printf("\nGPU Device Threads In Warp		: %d", dev_prop.warpSize);
			printf("\nGPU Device Maximum Thread Dimensions		: (%d, %d, %d)", dev_prop.maxThreadsDim[0], dev_prop.maxThreadsDim[1], dev_prop.maxThreadsDim[2]);
			printf("\nGPU Device Maximum Grid Dimensions		: (%d, %d, %d)", dev_prop.maxGridSize[0], dev_prop.maxGridSize[1], dev_prop.maxGridSize[2]);
			printf("\n");


			printf("\n================================================================================");
			printf("\n\n\t ***** GPU DEVICE DRIVER INFORMATION *****\n");
			printf("\n================================================================================");
			printf("\nGPU Device has ECC support		: %s",dev_prop.ECCEnabled? "Enabled":"Disabled");
#if defined(WIN32)|| defined(_WIN32)||defined(WIN64)||defined(_WIN64)
			printf("\nGPU Device CUDA Driver Mode (TCC or WDDM)	: %s", dev_prop.tccDriver ? "TCC(Tesla Compute Cluster Driver)" : "WDDM (Windows DisplayDriver Model)");
#endif

			printf("\n\n**********************************************************************************\n");
		}
	}
}

int ConvertSMVersionNumberToCores(int major, int minor)
{
	// Defines for GPU Architecture types (using the SM version to determine the number of cores per SM
	typedef struct
	{
		int SM;		// 0xMm (hexadecimal notation), M = SM major version and m = SM minor version
		int Cores;
	}sSMtoCores;

	sSMtoCores nGpuArchitectureCoresPerSM[]=	// array of struct ; array can have datatype as object name of struct?
	{
		{ 0x20, 32 },	// Fermi Generation (SM 2.0) GF100 class
		{ 0x21, 48 },	// Fermi Generation (SM 2.1) GF10x class
		{ 0x30, 192 },	// Keplar Generation (SM 3.0) GK10x class
		{ 0x32, 192 },	// Keplar Generation (SM 3.2) GK10x class
		{ 0x35, 192 },	// Keplar Generation (SM 3.5) GK11x class
		{ 0x37, 192 },	// Keplar Generation (SM 3.7) GK21x class
		{ 0x50, 128 },	// Maxwell Generation (SM 5.0) GM10x class
		{ 0x52, 128 },	// Maxwell Generation (SM 5.2) GM20x class
		{ 0x53, 128 },	// Maxwell Generation (SM 5.3) GM20x class
		{ 0x60, 64 },	// Pascal Generation (SM 6.0) GP100 class
		{ 0x61, 128 },	// Pascal Generation (SM 6.1) GP10x class
		{ 0x62, 128 },	// Pascal Generation (SM 6.2) GP10x class
		{ -1,-1 }
	};

	int index = 0;

	while (nGpuArchitectureCoresPerSM[index].SM != -1)
	{
		if (nGpuArchitectureCoresPerSM[index].SM == ((major << 4) + minor))
		{
			return nGpuArchitectureCoresPerSM[index].Cores;
		}

		index++;
	}

	// If we don't find the values, we default use the previous one to return properly
	printf("MapSMtoCores for SM %d.%d is undefined. Default to use %d Cores/SM", major, minor, nGpuArchitectureCoresPerSM[index - 1].Cores);
	return(nGpuArchitectureCoresPerSM[index - 1].Cores);
}
