#include<stdio.h>

int main(void)
{
	int choice,ch,i;
	char ans;
	//char *directors[] = { "Steven Speilberg","James Cameron","Wolfgang Peterson" };
	char directors[][20] = { "1.Steven Speilberg","2.James Cameron","3.Wolfgang Peterson","4.Quentin Tarantino" };

	char Speilberg_movies[][25] = { "Jurrasic Park","Schindler's List","Saving Private Ryan" };

	char JurrasicPark_cast[][20] = { "Sam Neil","Laura Dern","Jeff Goldblum" };
	char JurrasicPark_release_date[] = { "June 9,1993" };
	char JurrasicPark_music[] = { "John Williams" };

	char SchindlersList_cast[][20] = { "Liam Neeson","Ben Kingsley","Ralph Fiennes"};
	char SchindlersList_release_date[] = {"November 30,1993"};
	char SchindlersList_music[] = { "John Williams" };

	char SavingPrivateRyan_cast[][20] = { "Tom Hanks","Edward Burns","Matt Damon" };
	char SavingPrivateRyan_release_date[] = { "July 24,1998" };
	char SavingPrivateRyan_music[] = { "John Williams" };

	char* Cameron_movies[] = { "Titanic","Avatar","The Terminator" };

	struct Cameron_Movies_Struct
	{
		char cast[2][22];
		char date_of_release[20];
		char music[20];
	}titanic,avatar,terminator;

	strcpy(&titanic.cast[0],&"Leonardo DiCaprio");
	strcpy(&titanic.cast[1],&"Kate Winslet");
	strcpy(&titanic.date_of_release[0],&"November 1,1997");
	strcpy(&titanic.music,"James Horner");

	strcpy(&avatar.cast[0], &"Sam Worthington");
	strcpy(&avatar.cast[1], &"Zoe Saldana");
	strcpy(&avatar.date_of_release[0], &"December 18, 2009");
	strcpy(&avatar.music, "James Horner");

	strcpy(&terminator.cast[0],&"Arnold Schwarzenegger");
	strcpy(&terminator.cast[1],&"Michael Biehn");
	strcpy(&terminator.date_of_release[0],&"October 26, 1984");
	strcpy(&terminator.music, "Brad Fiedel");

	char* Wolfgang_movies[][4] = { { "In the Line of Fire","Clint Eastwood\n\tJohn Malkovich","July 9, 1993","Ennio Morricone" },
								   { "Troy","Brad Pitt\n\tEric Bana","May 14, 2004","James Horner" },
								   { "The Perfect Storm","George Clooney\n\tMark Wahlberg","June 30, 2000","James Horner" } };

	struct Quentin_Movie
	{
		char* movie_name[1];
		char* cast[2];
		char* date_of_release[20];
		char* music[2];
	};

	struct Quentin_Movie arr_Quentin[3];

	arr_Quentin[0].movie_name[0] = "Kill Bill- Vol 1";
	arr_Quentin[0].cast[0] = "Uma Thurman";
	arr_Quentin[0].cast[1] = "Lucy Liu";
	arr_Quentin[0].date_of_release[0] = "October 10, 2003";
	arr_Quentin[0].music[0] = "The RZA";
	arr_Quentin[0].music[1] = NULL;

	arr_Quentin[1].movie_name[0] = "Kill Bill- Vol 2";
	arr_Quentin[1].cast[0] = "Uma Thurman";
	arr_Quentin[1].cast[1] = "David Carradine";
	arr_Quentin[1].date_of_release[0] = "April 16, 2004 ";
	arr_Quentin[1].music[0] = "The RZA";
	arr_Quentin[1].music[1] = "Robert Rodriguez";

	arr_Quentin[2].movie_name[0] = "Pulp Fiction";
	arr_Quentin[2].cast[0] = "John Travolta";
	arr_Quentin[2].cast[1] = "Samuel L.Jackson";
	arr_Quentin[2].date_of_release[0] = "October 14, 1994";
	arr_Quentin[2].music[0] = "Various";
	//arr_Quentin[0].music[1] = NULL;

	do
	{
	for(i=0;i<4;i++)
		printf("\n%s",&directors[i][0]);

	printf("\n\nEnter your choice : ");
	scanf("%d", &choice);

	switch (choice)
	{
	case 1:
		printf("\nMovies by Steven Speilberg :");
		for (i = 0;i < 3;i++)
			printf("\n%d. %s",i+1, &Speilberg_movies[i][0]);

		printf("\n\nEnter your choice : ");
		scanf("%d", &ch);
		printf("------------------------------------------------------------------------------");

		switch (ch)
		{
		case 1:
			printf("\nSome details about Jurrasic Park :\n");

			printf("\nCast : ");
			for (i = 0;i < 3;i++)
				printf("\t%s\n",&JurrasicPark_cast[i][0]);

			printf("\nRelease year : ");
			printf("%s\n",&JurrasicPark_release_date);

			printf("\nMusic by : ");
			printf("%s\n", &JurrasicPark_music);

			break;

		case 2:

			printf("\nSome details about Schindler's List :\n");

			printf("\nCast : ");
			for (i = 0;i < 3;i++)
				printf("\t%s\n", &SchindlersList_cast[i][0]);

			printf("\nRelease year : ");
			printf("%s\n", &SchindlersList_release_date);

			printf("\nMusic by : ");
			printf("%s\n", &SchindlersList_music);

			break;

		case 3:

			printf("\nSome details about Saving Private Ryan :\n");

			printf("\nCast : ");
			for (i = 0;i < 3;i++)
				printf("\t%s\n", &SavingPrivateRyan_cast[i][0]);

			printf("\nRelease year : ");
			printf("%s\n", &SavingPrivateRyan_release_date);

			printf("\nMusic by : ");
			printf("%s\n", &SavingPrivateRyan_music);

			break;

		}
		break;
//===========================================================================
	case 2:
		printf("\nMovies by James Cameron :");
		for (i = 0;i < 3;i++)
			printf("\n%d. %s",i+1, &Cameron_movies[i][0]);

		printf("\n\nEnter your choice : ");
		scanf("%d", &ch);
		printf("------------------------------------------------------------------------------");

		switch (ch)
		{
		case 1:
			printf("\nSome details about Titanic :\n");

			printf("\nCast : ");
			for (i = 0;i < 2;i++)
				printf("\t%s\n", &titanic.cast[i][0]);

			printf("\nRelease year : ");
			printf("%s", &titanic.date_of_release);

			printf("\n\nMusic by : ");
			printf("%s\n", &titanic.music);

			break;

		case 2:
			printf("\nSome details about Avatar :\n");

			printf("\nCast : ");
			for (i = 0;i < 2;i++)
				printf("\t%s\n", &avatar.cast[i][0]);

			printf("\nRelease year : ");
			printf("%s", &avatar.date_of_release);

			printf("\n\nMusic by : ");
			printf("%s\n", &avatar.music);

			break;

		case 3:
			printf("\nSome details about The Terminator :\n");

			printf("\nCast : ");
			for (i = 0;i < 2;i++)
				printf("\t%s\n", &terminator.cast[i][0]);

			printf("\nRelease year : ");
			printf("%s", &terminator.date_of_release);

			printf("\n\nMusic by : ");
			printf("%s\n", &terminator.music);

			break;
		}
		break;

//===========================================================================
	case 3:
		printf("\nMovies by Wolfgang Peterson :");
		for (i = 0;i < 3;i++)
			printf("\n%d. %s", i+1,Wolfgang_movies[i][0]);

		printf("\n\nEnter your choice : ");
		scanf("%d", &ch);
		printf("------------------------------------------------------------------------------");

		switch (ch)
		{
		case 1:
			printf("\nSome details about In The Line of Fire :\n");

			printf("\nCast : ");
			//for (i = 0;i < 2;i++)
			printf("\t%s\n", Wolfgang_movies[0][1]);

			printf("\nRelease year : ");
			printf("%s", Wolfgang_movies[0][2]);

			printf("\n\nMusic by : ");
			printf("%s\n", Wolfgang_movies[0][3]);

			break;

		case 2:
			printf("\nSome details about Troy :\n");

			printf("\nCast : ");
			//for (i = 0;i < 2;i++)
			printf("\t%s\n", Wolfgang_movies[1][1]);

			printf("\nRelease year : ");
			printf("%s", Wolfgang_movies[1][2]);

			printf("\n\nMusic by : ");
			printf("%s\n", Wolfgang_movies[1][3]);

			break;

		case 3:
			printf("\nSome details about The Perfect Storm :\n");

			printf("\nCast : ");
			//for (i = 0;i < 2;i++)
			printf("\t%s\n", Wolfgang_movies[2][1]);

			printf("\nRelease year : ");
			printf("%s", Wolfgang_movies[2][2]);

			printf("\n\nMusic by : ");
			printf("%s\n", Wolfgang_movies[2][3]);

			break;
		}
		break;
		//===========================================================================
	case 4:
		printf("\nMovies by Quentin Tarantino :");
		for (i = 0;i < 3;i++)
			printf("\n%d. %s", i+1,arr_Quentin[i].movie_name[0]);

		printf("\n\nEnter your choice : ");
		scanf("%d", &ch);
		printf("------------------------------------------------------------------------------");

		switch (ch)
		{
		case 1:
			printf("\nSome details about Kill Bill- Vol 1 :\n");

			printf("\nCast : ");
			for (i = 0;i < 2;i++)
				printf("\t%s\n", arr_Quentin[0].cast[i]);

			printf("\nRelease year : ");
			printf("%s", arr_Quentin[0].date_of_release[0]);

			printf("\n\nMusic by : ");
			//for (i = 0;i < 2;i++)
				printf("%s\n", arr_Quentin[0].music[0]);

			break;

		case 2:
			printf("\nSome details about Kill Bill- Vol 2 :\n");

			printf("\nCast : ");
			for (i = 0;i < 2;i++)
				printf("\t%s\n", arr_Quentin[1].cast[i]);

			printf("\nRelease year : ");
			printf("%s", arr_Quentin[1].date_of_release[0]);

			printf("\n\nMusic by : ");
			for (i = 0;i < 2;i++)
				printf("\n%s", arr_Quentin[1].music[i]);

			break;

		case 3:
			printf("\nSome details about Pulp Fiction :\n");

			printf("\nCast : ");
			for (i = 0;i < 2;i++)
				printf("\t%s\n", arr_Quentin[2].cast[i]);

			printf("\nRelease year : ");
			printf("%s", arr_Quentin[2].date_of_release[0]);

			printf("\n\nMusic by : ");
			//for (i = 0;i < 2;i++)
			printf("%s\n", arr_Quentin[2].music[0]);

			break;
		}

	}
	printf("\n==============================================================================");
	printf("\nDo you want to continue? (y/n) : ");
	scanf("%1s",&ans);	//why not %c ?
	} while (ans == 'y');

	return(0);
}
