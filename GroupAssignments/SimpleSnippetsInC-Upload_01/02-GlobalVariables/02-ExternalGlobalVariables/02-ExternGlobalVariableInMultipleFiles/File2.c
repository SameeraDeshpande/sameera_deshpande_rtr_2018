#include<stdio.h>

// *** GLOBAL SCOPE ***
// count is a global variable declared in source code file Global_MultiFiles.c
// To access it in this file, it must first be re-declared as an external variable in the global scope of this file along with the 'extern' keyword and its proper data type
// Then, it can be used as any ordinary global variable throughout this file as well

void change_count_by_two()
{
	// Here, Re-declaring 'count' as a local variable using 'extern' keyword within change_count_two()
	extern int count;	// treats count as a global var. only

	count=count+2;	
	printf("From change_count_by_two() :\ncount : %d\n", count);
}
