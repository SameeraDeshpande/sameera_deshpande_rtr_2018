/* Problem Statement -
3. Extern Global Variable in Multiple File

split the two functions -> 
"change_count_by_one" & "change_count_buy_two" in two different files say File_01.c and File_02.c respectively

NOTE: here "count" variable is declared globally in main file -> eg.main.c
		To access it in other two files, 
		it must first be re-declared as an external variable in the global scope of the files 
		along with the 'extern' keyword and its proper data type
		for eg. extern int global_count;
*/

#include<stdio.h>

// global variable declaration
int count;

int main(void)
{

	// function declarations
	void change_count();
	void change_count_by_one();	// defined in File1.c
	void change_count_by_two();	// defined in File2.c

	printf("From main() : \ncount : %d\n", count);

	change_count();	
	printf("From main() : \ncount : %d\n", count);

	change_count_by_one();	//?
	printf("From main() : \ncount : %d\n", count);

	change_count_by_two();	//?
	printf("From main() : \ncount : %d\n", count);

	return(0);
}

void change_count()
{
	count++;
	printf("From change_count() :\ncount : %d\n", count);
}

/*
Compilation cmd-
>cl.exe Global_Multifiles.c File1.c File2.c

Output-
>Global_Multifiles.exe
From main() :
count : 0
From change_count() :
count : 1
From main() :
count : 1
From change_count_by_one() :
count : 2
From main() :
count : 2
From change_count_by_two() :
count : 4
From main() :
count : 4
*/
