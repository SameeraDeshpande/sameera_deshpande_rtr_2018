#include<stdio.h>

// global variable declaration
extern int count;

// *** GLOBAL SCOPE ***
// count is a global variable declared in source code file Global_MultiFiles.c
// To access it in this file, it must first be re-declared as an external variable in the global scope of this file along with the 'extern' keyword and its proper data type
// Then, it can be used as any ordinary global variable throughout this file as well

void change_count_by_one()
{
	count++;
	printf("From change_count_by_one() :\ncount : %d\n", count);
}
