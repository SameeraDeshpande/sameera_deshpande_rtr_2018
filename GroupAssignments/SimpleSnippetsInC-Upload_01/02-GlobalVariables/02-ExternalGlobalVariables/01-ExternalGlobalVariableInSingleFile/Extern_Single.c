/* Problem Statement -
2. Extern Global Variable in Single File

initialize global variable eg. "global_count" (integer type) below main function
declare "global_count" in main
eg. extern int global_count;->Since, it is declared after main(), it must be first re - declared in main() as an external global variable

call below function from main,
a.change_count ->

i)increase count variable by one.
ii)print variable count in it.

print count variable before and after this function call from main. */

#include<stdio.h>

// *** GLOBAL SCOPE ***
// global_count is a global variable. 
// Since it is declared before change_count(), it can be accssed and used as any ordinary global variable in change_count()
// Since it is declared after main(), it must be firest re-declared in main() as an external global variable by means of the 'extern' keyword and the type of the variable.
// Once this is done, it can be used as an ordinary global variable in main as well.

int main(void)
{
	// variable declarations
	extern int global_count;

	// function declarations
	void change_count();

	printf("\nFrom main() : ");
	printf("\nglobal_count : %d", global_count);	// prints 3 because of the keyword "extern" above (even though initialization is after main())
	change_count();
	printf("\nFrom main() : ");
	printf("\nglobal_count : %d", global_count);
}

int global_count = 3;

void change_count()
{
	global_count++;
	printf("\nFrom change_count() : ");
	printf("\nglobal_count : %d", global_count);
}

/*
Output -
From main() :
global_count : 3
From change_count() :
global_count : 4
From main() :
global_count : 4
*/