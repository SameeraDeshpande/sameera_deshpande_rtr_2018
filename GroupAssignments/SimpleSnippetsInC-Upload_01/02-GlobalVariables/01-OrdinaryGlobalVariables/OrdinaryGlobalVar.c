/* Problem Statement -

1. GLOBAL VARIABLE

take one global "count" variable.
call two functions from main,
a.change_count_by_one ->

i)increase count variable by one.
ii)print variable count in it.

b.change_count_buy_two ->
i)increase count variable by two.
ii)print variable count in it.

print count variable before and after these two function call from main.*/

#include<stdio.h>

//If not initialized by us, global variables are initialized to their zero values (with respect to their data types i.e: 0 for int, 0.0 for float and double, etc.) by default.
//But still, for good programming discipline, we shall explicitly initialize our global variable with 0.

// global variable declaration
int count;

// global function declarations
void change_count_by_one();
void change_count_by_two();

int main(void)
{
	printf("From main() : \ncount : %d\n", count);

	change_count_by_one();
	printf("From main() : \ncount : %d\n", count);

	change_count_by_two();
	printf("From main() : \ncount : %d\n", count);

	return(0);
}

void change_count_by_one()
{
	count++;
	printf("From change_count_by_one() :\ncount : %d\n", count);
}

void change_count_by_two()
{
	count = count + 2;
	printf("From change_count_by_two() :\ncount : %d\n", count);
}

/*
Output :
From main() :
count : 0
From change_count_by_one() :
count : 1
From main() :
count : 1
From change_count_by_two() :
count : 3
From main() :
count : 3 */
