#include<stdio.h>

int main(void)
{
	// function declarations
	void ConvertToBinary();

	// variable declarations
	unsigned int x, y, result;

	// code
	printf("\nEnter an integer : ");
	scanf("%u", &x);
	printf("\nEnter another integer : ");
	scanf("%u", &y);

	result = x ^ y;
	printf("\nBitwise XORing of x= %d(Decimal), %o(Octal), %x(Hexadecimal) and y= %d(Decimal), %o(Octal), %x(Hexadecimal) gives result %d(Decimal), %o(Octal), %x(Hexadecimal)", x, x, x, y, y, y, result, result, result);

	printf("\n");
	ConvertToBinary(x);
	ConvertToBinary(y);
	ConvertToBinary(result);

	printf("\n");
	return(0);
}

void ConvertToBinary(unsigned int decimal_no)
{
	// variable declarations
	unsigned int quotient, remainder, num;
	unsigned int binary_array[8];
	int i;

	// code
	for (i = 0;i < 8;i++)
		binary_array[i] = 0;
	num = decimal_no;

	printf("\nThe binary form of the decimal integer %d is : ", decimal_no);
	i = 7;
	while (num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}

	for (i = 0;i < 8;i++)
		printf("%u", binary_array[i]);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\07-Operators\04-BitwiseOperators\03-BitwiseXOR>BitwiseXOR.exe

Enter an integer : 9

Enter another integer : 12

Bitwise XORing of x= 9(Decimal), 11(Octal), 9(Hexadecimal) and y= 12(Decimal), 14(Octal), c(Hexadecimal) gives result 5(Decimal), 5(Octal), 5(Hexadecimal)

The binary form of the decimal integer 9 is : 00001001
The binary form of the decimal integer 12 is : 00001100
The binary form of the decimal integer 5 is : 00000101

*/