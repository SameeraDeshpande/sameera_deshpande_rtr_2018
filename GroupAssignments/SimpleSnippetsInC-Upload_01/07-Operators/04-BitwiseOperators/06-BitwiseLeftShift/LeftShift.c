#include<stdio.h>

int main(void)
{
	// function declarations
	void ConvertToBinary();

	// variable declarations
	unsigned int x, y, result;
	unsigned int left_shift_x, left_shift_y;

	// code
	printf("\nEnter an integer : ");
	scanf("%u", &x);
	printf("\nEnter another integer : ");
	scanf("%u", &y);

	printf("\nBy how many bits do you want to shift x=%d to the left? : ", x);
	scanf("%u", &left_shift_x);	
	printf("\nBy how many bits do you want to shift y=%d to the left? : ", y);
	scanf("%u", &left_shift_y);

	result = x << left_shift_x;
	printf("\nBitwise left shift of x= %u(decimal), %o(octal), %x(hexadecimal) by %u bits gives result %u(decimal), %o(octal), %x(hexadecimal)", x, x, x, left_shift_x, result, result, result);
	ConvertToBinary(x);
	ConvertToBinary(result);

	printf("\n");
	result = y << left_shift_y;
	printf("\nBitwise left shift of y= %u(decimal), %o(octal), %x(hexadecimal) by %d bits gives result %u(decimal), %o(octal), %x(hexadecimal)", y, y, y, left_shift_y, result, result, result);
	ConvertToBinary(y);
	ConvertToBinary(result);

	printf("\n");
	return(0);
}

void ConvertToBinary(unsigned int decimal_no)
{
	// variable declarations
	unsigned int quotient, remainder, num;
	unsigned int binary_array[8];
	int i;

	// code
	for (i = 0;i < 8;i++)
		binary_array[i] = 0;

	num = decimal_no;

	printf("\nThe binary form of the decimal integer %d is : ", decimal_no);
	i = 7;
	while (num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}

	for (i = 0;i < 8;i++)
		printf("%u", binary_array[i]);
}

/*
Output-

C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\07-Operators\04-BitwiseOperators\06-BitwiseLeftShift>LeftShift.exe

Enter an integer : 10

Enter another integer : 12

By how many bits do you want to shift x=10 to the left? : 2

By how many bits do you want to shift y=12 to the left? : 1

Bitwise left shift of x= 10(decimal), 12(octal), a(hexadecimal) by 2 bits gives result 40(decimal), 50(octal), 28(hexadecimal)
The binary form of the decimal integer 10 is : 00001010
The binary form of the decimal integer 40 is : 00101000

Bitwise left shift of y= 12(decimal), 14(octal), c(hexadecimal) by 1 bits gives result 24(decimal), 30(octal), 18(hexadecimal)
The binary form of the decimal integer 12 is : 00001100
The binary form of the decimal integer 24 is : 00011000


*/