#include<stdio.h>

int main(void)
{
	// function declarations
	void ConvertToBinary();

	// variable declarations
	unsigned int x, y, result;
	unsigned int right_shift_x, right_shift_y;
	unsigned int left_shift_x, left_shift_y;

	// code
	printf("\nEnter an integer : ");
	scanf("%u", &x);
	printf("\nEnter another integer : ");
	scanf("%u", &y);

	result = x & y;
	printf("\nBitwise ANDing of x= %d(Decimal), %o(Octal), %x(Hexadecimal) and y= %d(Decimal), %o(Octal), %x(Hexadecimal) gives result %d(Decimal), %o(Octal), %x(Hexadecimal)", x, x, x, y, y, y, result, result, result);

	printf("\n");
	ConvertToBinary(x);
	ConvertToBinary(y);
	ConvertToBinary(result);

	printf("\n");
	result = x | y;
	printf("\nBitwise ORing of x= %d(Decimal), %o(Octal), %x(Hexadecimal) and y= %d(Decimal), %o(Octal), %x(Hexadecimal) gives result %d(Decimal), %o(Octal), %x(Hexadecimal)", x, x, x, y, y, y, result, result, result);

	printf("\n");
	ConvertToBinary(x);
	ConvertToBinary(y);
	ConvertToBinary(result);

	printf("\n");
	result = x ^ y;
	printf("\nBitwise XORing of x= %d(Decimal), %o(Octal), %x(Hexadecimal) and y= %d(Decimal), %o(Octal), %x(Hexadecimal) gives result %d(Decimal), %o(Octal), %x(Hexadecimal)", x, x, x, y, y, y, result, result, result);

	printf("\n");
	ConvertToBinary(x);
	ConvertToBinary(y);
	ConvertToBinary(result);

	printf("\n");
	result = ~x;
	printf("\nBitwise complement of x= %d(Decimal), %o(Octal), %x(Hexadecimal) gives result %d(Decimal), %o(Octal), %x(Hexadecimal)", x, x, x, result, result, result);

	printf("\n");
	ConvertToBinary(x);
	ConvertToBinary(result);

	printf("\n");
	result = ~y;
	printf("\nBitwise complement of y= %d(Decimal), %o(Octal), %x(Hexadecimal) gives result %d(Decimal), %o(Octal), %x(Hexadecimal)", y, y, y, result, result, result);

	printf("\n");
	ConvertToBinary(y);
	ConvertToBinary(result);

	printf("\n");
	printf("\nBy how many bits do you want to shift x=%d to the right? : ", x);
	scanf("%u", &right_shift_x);
	printf("\nBy how many bits do you want to shift y=%d to the right? : ", y);
	scanf("%u", &right_shift_y);

	result = x >> right_shift_x;
	printf("\nBitwise right shift of x= %u(decimal), %o(octal), %x(hexadecimal) by %u bits gives result %u(decimal), %o(octal), %x(hexadecimal)", x, x, x, right_shift_x, result, result, result);
	ConvertToBinary(x);
	ConvertToBinary(result);

	printf("\n");
	result = y >> right_shift_y;
	printf("\nBitwise right shift of y= %u(decimal), %o(octal), %x(hexadecimal) by %d bits gives result %u(decimal), %o(octal), %x(hexadecimal)", y, y, y, right_shift_y, result, result, result);
	ConvertToBinary(y);
	ConvertToBinary(result);

	printf("\n");
	printf("\nBy how many bits do you want to shift x=%d to the left? : ", x);
	scanf("%u", &left_shift_x);
	printf("\nBy how many bits do you want to shift y=%d to the left? : ", y);
	scanf("%u", &left_shift_y);

	result = x << left_shift_x;
	printf("\nBitwise left shift of x= %u(decimal), %o(octal), %x(hexadecimal) by %u bits gives result %u(decimal), %o(octal), %x(hexadecimal)", x, x, x, left_shift_x, result, result, result);
	ConvertToBinary(x);
	ConvertToBinary(result);

	printf("\n");
	result = y << left_shift_y;
	printf("\nBitwise left shift of y= %u(decimal), %o(octal), %x(hexadecimal) by %d bits gives result %u(decimal), %o(octal), %x(hexadecimal)", y, y, y, left_shift_y, result, result, result);
	ConvertToBinary(y);
	ConvertToBinary(result);

	printf("\n");
	return(0);
}

void ConvertToBinary(unsigned int decimal_no)
{
	// variable declarations
	unsigned int quotient, remainder, num;
	unsigned int binary_array[8];
	int i;

	// code
	for (i = 0;i < 8;i++)
		binary_array[i] = 0;
	num = decimal_no;

	printf("\nThe binary form of the decimal integer %d is : ", decimal_no);
	i = 7;
	while (num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}

	for (i = 0;i < 8;i++)
		printf("%u", binary_array[i]);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\07-Operators\04-BitwiseOperators\07-AllBitwiseOperators>AllBitwiseOperators.exe

Enter an integer : 9

Enter another integer : 11

Bitwise ANDing of x= 9(Decimal), 11(Octal), 9(Hexadecimal) and y= 11(Decimal), 13(Octal), b(Hexadecimal) gives result 9(Decimal), 11(Octal), 9(Hexadecimal)

The binary form of the decimal integer 9 is : 00001001
The binary form of the decimal integer 11 is : 00001011
The binary form of the decimal integer 9 is : 00001001

Bitwise ORing of x= 9(Decimal), 11(Octal), 9(Hexadecimal) and y= 11(Decimal), 13(Octal), b(Hexadecimal) gives result 11(Decimal), 13(Octal), b(Hexadecimal)

The binary form of the decimal integer 9 is : 00001001
The binary form of the decimal integer 11 is : 00001011
The binary form of the decimal integer 11 is : 00001011

Bitwise XORing of x= 9(Decimal), 11(Octal), 9(Hexadecimal) and y= 11(Decimal), 13(Octal), b(Hexadecimal) gives result 2(Decimal), 2(Octal), 2(Hexadecimal)

The binary form of the decimal integer 9 is : 00001001
The binary form of the decimal integer 11 is : 00001011
The binary form of the decimal integer 2 is : 00000010

Bitwise complement of x= 9(Decimal), 11(Octal), 9(Hexadecimal) gives result -10(Decimal), 37777777766(Octal), fffffff6(Hexadecimal)

The binary form of the decimal integer 9 is : 00001001
The binary form of the decimal integer -10 is : 11110110

Bitwise complement of y= 11(Decimal), 13(Octal), b(Hexadecimal) gives result -12(Decimal), 37777777764(Octal), fffffff4(Hexadecimal)

The binary form of the decimal integer 11 is : 00001011
The binary form of the decimal integer -12 is : 11110100

By how many bits do you want to shift x=9 to the right? : 3

By how many bits do you want to shift y=11 to the right? : 2

Bitwise right shift of x= 9(decimal), 11(octal), 9(hexadecimal) by 3 bits gives result 1(decimal), 1(octal), 1(hexadecimal)
The binary form of the decimal integer 9 is : 00001001
The binary form of the decimal integer 1 is : 00000001

Bitwise right shift of y= 11(decimal), 13(octal), b(hexadecimal) by 2 bits gives result 2(decimal), 2(octal), 2(hexadecimal)
The binary form of the decimal integer 11 is : 00001011
The binary form of the decimal integer 2 is : 00000010

By how many bits do you want to shift x=9 to the left? : 4

By how many bits do you want to shift y=11 to the left? : 0

Bitwise left shift of x= 9(decimal), 11(octal), 9(hexadecimal) by 4 bits gives result 144(decimal), 220(octal), 90(hexadecimal)
The binary form of the decimal integer 9 is : 00001001
The binary form of the decimal integer 144 is : 10010000

Bitwise left shift of y= 11(decimal), 13(octal), b(hexadecimal) by 0 bits gives result 11(decimal), 13(octal), b(hexadecimal)
The binary form of the decimal integer 11 is : 00001011
The binary form of the decimal integer 11 is : 00001011

*/
