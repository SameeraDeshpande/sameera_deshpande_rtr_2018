#include<stdio.h>

int main(void)
{
	// function declarations
	void ConvertToBinary();

	// variable declarations
	unsigned int x, y, result;

	// code
	printf("\nEnter an integer : ");
	scanf("%u", &x);
	printf("\nEnter another integer : ");
	scanf("%u", &y);

	result = ~x;
	printf("\nBitwise complement of x= %d(Decimal), %o(Octal), %x(Hexadecimal) gives result %d(Decimal), %o(Octal), %x(Hexadecimal)", x, x, x,result, result, result);
	ConvertToBinary(x);
	ConvertToBinary(result);
	printf("\n");

	result = ~y;
	printf("\nBitwise complement of y= %d(Decimal), %o(Octal), %x(Hexadecimal) gives result %d(Decimal), %o(Octal), %x(Hexadecimal)",y, y, y, result, result, result);
	ConvertToBinary(y);
	ConvertToBinary(result);

	printf("\n");
	return(0);
}

void ConvertToBinary(unsigned int decimal_no)
{
	// variable declarations
	unsigned int quotient, remainder, num;
	unsigned int binary_array[8];
	int i;

	// code
	for (i = 0;i < 8;i++)
		binary_array[i] = 0;
	num = decimal_no;

	printf("\nThe binary form of the decimal integer %d is : ", decimal_no);
	i = 7;
	while (num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}

	for (i = 0;i < 8;i++)
		printf("%u", binary_array[i]);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\07-Operators\04-BitwiseOperators\04-BitwiseOnesComplement>OnesComplement.exe

Enter an integer : 5

Enter another integer : 11

Bitwise complement of x= 5(Decimal), 5(Octal), 5(Hexadecimal) gives result -6(Decimal), 37777777772(Octal), fffffffa(Hexadecimal)
The binary form of the decimal integer 5 is : 00000101
The binary form of the decimal integer -6 is : 11111010

Bitwise complement of y= 11(Decimal), 13(Octal), b(Hexadecimal) gives result -12(Decimal), 37777777764(Octal), fffffff4(Hexadecimal)
The binary form of the decimal integer 11 is : 00001011
The binary form of the decimal integer -12 is : 11110100
*/