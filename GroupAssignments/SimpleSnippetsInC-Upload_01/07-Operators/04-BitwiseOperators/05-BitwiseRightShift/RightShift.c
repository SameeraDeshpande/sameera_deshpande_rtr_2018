#include<stdio.h>

int main(void)
{
	// function declarations
	void ConvertToBinary();

	// variable declarations
	unsigned int x, y, result;
	unsigned int right_shift_x, right_shift_y;

	// code
	printf("\nEnter an integer : ");
	scanf("%u", &x);
	printf("\nEnter another integer : ");
	scanf("%u", &y);

	printf("\nBy how many bits do you want to shift x=%d to the right?", x);
	scanf("%d", &right_shift_x);	// gives no error on %d
	printf("\nBy how many bits do you want to shift y=%d to the right?", y);
	scanf("%d", &right_shift_y);

	result = x >> right_shift_x;
	printf("\nBitwise right shift of x= %d(decimal), %o(octal), %x(hexadecimal) by %d bits gives result %d(decimal), %o(octal), %x(hexadecimal)", x, x, x, right_shift_x, result, result, result);
	// gives no error on %d
	ConvertToBinary(x);
	ConvertToBinary(result);

	printf("\n");
	result = y >> right_shift_y;
	printf("\nBitwise right shift of y= %d(decimal), %o(octal), %x(hexadecimal) by %d bits gives result %d(decimal), %o(octal), %x(hexadecimal)", y, y, y, right_shift_y, result, result, result);
	ConvertToBinary(y);
	ConvertToBinary(result);

	printf("\n");
	return(0);
}

void ConvertToBinary(unsigned int decimal_no)
{
	// variable declarations
	unsigned int quotient, remainder, num;
	unsigned int binary_array[8];
	int i;

	// code
	for (i = 0;i < 8;i++)
		binary_array[i] = 0;

	num = decimal_no;

	printf("\nThe binary form of the decimal integer %d is : ", decimal_no);
	i = 7;
	while (num != 0)
	{
		quotient = num / 2;
		remainder = num % 2;
		binary_array[i] = remainder;
		num = quotient;
		i--;
	}

	for (i = 0;i < 8;i++)
		printf("%u", binary_array[i]);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\07-Operators\04-BitwiseOperators\05-BitwiseRightShift>RightShift.exe

Enter an integer : 5

Enter another integer : 6

By how many bits do you want to shift x=5 to the right?3

By how many bits do you want to shift y=6 to the right?2

Bitwise right shift of x= 5(decimal), 5(octal), 5(hexadecimal) by 3 bits gives result 0(decimal), 0(octal), 0(hexadecimal)
The binary form of the decimal integer 5 is : 00000101
The binary form of the decimal integer 0 is : 00000000

Bitwise right shift of y= 6(decimal), 6(octal), 6(hexadecimal) by 2 bits gives result 1(decimal), 1(octal), 1(hexadecimal)
The binary form of the decimal integer 6 is : 00000110
The binary form of the decimal integer 1 is : 00000001


*/