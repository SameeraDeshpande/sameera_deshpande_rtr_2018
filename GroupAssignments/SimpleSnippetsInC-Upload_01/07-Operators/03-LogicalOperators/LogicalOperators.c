#include<stdio.h>

int main(void)
{
	// variable declarations
	int x, y, z,result;

	// code
	printf("\nEnter first integer : ");
	scanf("%d", &x);
	printf("\nEnter second integer : ");
	scanf("%d", &y);
	printf("\nEnter third integer : ");
	scanf("%d", &z);

	printf("\nIf answer=1, it is TRUE");
	printf("\nIf answer=0, it is FALSE");

	result = (x <= y) && (y != z);
	printf("\n\nLOGICAL AND (&&) : Answer is TRUE (1) If And Only If BOTH Conditions Are True. The Answer is FALSE (0), If Any One Or Both Conditions Are False.");
	printf("\nx=%d is less than or equal to y=%d AND y=%d is not equal to z=%d,\tanswer=%d", x, y, y, z, result);

	result = (y >= x) || (x == z);
	printf("\n\nLOGICAL OR (||) : Answer is FALSE (0) If And Only If BOTH Conditions Are False. The Answer is TRUE (1), If Any One Or Both Conditions Are True.");
	printf("\ny=%d is greater than or equal to x=%d OR x=%d is equal to z=%d,\tanswer=%d", y, x, x, z, result);

	result = !x;
	printf("\n\nx=%d and using logical NOT operator, !x gives answer=%d", x, result);

	result = !y;
	printf("\ny=%d and using logical NOT operator, !y gives answer=%d", y, result);

	result = !x;
	printf("\nz=%d and using logical NOT operator, !z gives answer=%d", z, result);

	result = (!(x <= y) && !(y != z));
	printf("\n\nx=%d\ty=%d\tz=%d", x, y, z);
	printf("\nUsing logical NOT on (x<=y) and also on (y!=z) and ANDing them afterwards, answer=%d", result);

	result = !((y >= x) || (x == z));
	printf("\n\nx=%d\ty=%d\tz=%d", x, y, z);
	printf("\nUsing logical NOT on entire logical expression (y >= x) || (x == z), answer=%d", result);

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\07-Operators\03-LogicalOperators>LogicalOperators.exe

Enter first integer : 5

Enter second integer : 8

Enter third integer : 3

If answer=1, it is TRUE
If answer=0, it is FALSE

LOGICAL AND (&&) : Answer is TRUE (1) If And Only If BOTH Conditions Are True. The Answer is FALSE (0), If Any One Or Both Conditions Are False.
x=5 is less than or equal to y=8 AND y=8 is not equal to z=3,   answer=1

LOGICAL OR (||) : Answer is FALSE (0) If And Only If BOTH Conditions Are False. The Answer is TRUE (1), If Any One Or Both Conditions Are True.
y=8 is greater than or equal to x=5 OR x=5 is equal to z=3,     answer=1

x=5 and using logical NOT operator, !x gives answer=0
y=8 and using logical NOT operator, !y gives answer=0
z=3 and using logical NOT operator, !z gives answer=0

x=5     y=8     z=3
Using logical NOT on (x<=y) and also on (y!=z) and ANDing them afterwards, answer=0

x=5     y=8     z=3
Using logical NOT on entire logical expression (y >= x) || (x == z), answer=0
*/