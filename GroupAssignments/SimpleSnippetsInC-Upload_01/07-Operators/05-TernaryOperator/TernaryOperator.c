#include<stdio.h>

int main(void)
{
	// variable declarations
	int j, k, l, m;
	char ch_result_1, ch_result_2;
	int i_result_1, i_result_2;

	// code
	j = 5;
	k = 10;

	ch_result_1 = (j > k) ? 'j' : 'k';
	i_result_1 = (j > k) ? j : k;

	printf("\nTernary Operator Answer 1 : %c, %d", ch_result_1, i_result_1);

	l = 7;
	m = 10;

	ch_result_2 = (l != m) ? 'l' : 'm';
	i_result_2 = (l != m) ? l : m;

	printf("\nTernary Operator Answer 2 : %c, %d", ch_result_2, i_result_2);

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\07-Operators\05-TernaryOperator>TernaryOperator.exe

Ternary Operator Answer 1 : k, 10
Ternary Operator Answer 2 : l, 7
*/