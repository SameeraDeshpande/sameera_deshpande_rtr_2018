#include<stdio.h>

int main(void)
{
	// variable declarations
	int x, y,result;

	// code
	printf("\nEnter one integer : ");
	scanf("%d", &x);
	printf("\nEnter another integer : ");
	scanf("%d", &y);
	printf("\nIf answer=1,it is TRUE");
	printf("\nIf answer=0,it is FALSE");

	result = x < y;
	printf("\n\nFor x<y, x=%d is less than y=%d,\tanswer is %d", x, y, result);

	result = x > y;
	printf("\nFor x>y, x=%d is greater than y=%d,\tanswer is %d", x, y, result);

	result = x <= y;
	printf("\nFor x<=y, x=%d is lesser than equal to y=%d,\tanswer is %d", x, y, result);

	result = x >= y;
	printf("\nFor x>=y, x=%d is greater than equal to y=%d,\tanswer is %d", x, y, result);

	result = x == y;
	printf("\nFor x==y, x=%d is equal to y=%d,\tanswer is %d", x, y, result);

	result = x != y;
	printf("\nFor x!=y, x=%d is not equal to y=%d,\tanswer is %d", x, y, result);
	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\07-Operators\02-RelationalOperators>RelationalOperators.exe

Enter one integer : 5

Enter another integer : 7

If answer=1,it is TRUE
If answer=0,it is FALSE

For x<y, x=5 is less than y=7,  answer is 1
For x>y, x=5 is greater than y=7,       answer is 0
For x<=y, x=5 is lesser than equal to y=7,      answer is 1
For x>=y, x=5 is greater than equal to y=7,     answer is 0
For x==y, x=5 is equal to y=7,  answer is 0
For x!=y, x=5 is not equal to y=7,      answer is 1
*/