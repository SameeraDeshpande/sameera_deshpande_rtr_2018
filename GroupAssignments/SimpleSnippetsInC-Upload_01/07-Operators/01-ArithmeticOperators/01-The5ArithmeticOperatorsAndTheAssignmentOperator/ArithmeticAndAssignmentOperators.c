#include<stdio.h>

int main(void)
{
	// variable declarations
	int num1, num2,result;

	// code
	printf("\n\n");
	printf("Enter a number : ");
	scanf("%d", &num1);
	printf("\n\n");
	printf("Enter another number : ");
	scanf("%d", &num2);
	printf("\n\n");

	// The 5 arithmetic operators are +,-,*,/,%
	// Result assigned to variable 'result' using assignment operator

	result = num1 + num2;
	printf("Addition of %d and %d is %d\n", num1, num2, result);

	result = num1 - num2;
	printf("Subtraction of %d and %d is %d\n", num1, num2, result);

	result = num1 * num2;
	printf("Multiplication of %d and %d is %d\n", num1, num2, result);

	result = num1 / num2;
	printf("Division of %d and %d gives quotient %d\n", num1, num2, result);

	result = num1 % num2;
	printf("Division of %d and %d gives remainder %d\n", num1, num2, result);

	printf("\n\n");
	return(0);
}

/*
Output-


Enter a number : 5


Enter another number : 7


Addition of 5 and 7 is 12
Subtraction of 5 and 7 is -2
Multiplication of 5 and 7 is 35
Division of 5 and 7 gives quotient 0
Division of 5 and 7 gives remainder 5
*/