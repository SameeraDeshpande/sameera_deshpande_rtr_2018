#include<stdio.h>

int main(void)
{
	// variable declarations
	int a=7, b=5;

	// code
	printf("\na = %d", a);
	printf("\na = %d", a++);
	printf("\na = %d", a);
	printf("\na = %d", ++a);

	printf("\nb = %d", b);
	printf("\nb = %d", b--);
	printf("\nb = %d", b);
	printf("\nb = %d", --b);

	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\07-Operators\01-ArithmeticOperators\03-IncrementDecrementOperators>IncrementDecrementOperators.exe

a = 7
a = 7
a = 8
a = 9
b = 5
b = 5
b = 4
b = 3
*/