#include<stdio.h>

int main(void)
{
	// variable declarations
	int num1, num2, x;

	// code
	printf("\nEnter a number : ");
	scanf("%d", &num1);
	printf("\nEnter another number : ");
	scanf("%d", &num2);

	// Since, In All The Following 5 Cases, The Operand on The Left 'num1' Is Getting Repeated Immediately On The Right (e.g : num1 = num1 + num2),
	// We Are Using Compound Assignment Operators +=, -=, *=, /= and %=

	// Since, 'num1' Will Be Assigned The Value Of (num1 + num2) At The Expression (num1 += num2), We Must Save The Original Value Of 'num1' To Another Variable (x)

	x = num1;
	num1 += num2;	//num1=num1+num2
	printf("\nAddition of %d and %d is %d", x, num2, num1);	

	// new value of num1 will be used further
	// save current value of num1
	x = num1;
	num1 -= num2;	//num1=num1-num2
	printf("\n\nSubtraction of %d and %d is %d", x, num2, num1);

	// new value of num1 will be used further
	// save current value of num1
	x = num1;
	num1 *= num2;	//num1=num1*num2
	printf("\n\nMultiplication of %d and %d is %d", x, num2, num1);

	// new value of num1 will be used further
	// save current value of num1
	x = num1;
	num1 /= num2;	//num1=num1/num2
	printf("\n\nDivision of %d and %d gives quotient %d", x, num2, num1);

	// new value of num1 will be used further
	// save current value of num1
	x = num1;
	num1 %= num2;	//num1=num1%num2
	printf("\n\nDivision of %d and %d gives remainder %d", x, num2, num1);

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\07-Operators\01-ArithmeticOperators\02-CompoundAssignmentOperators>CompoundAssignmentOperators.exe

Enter a number : 5

Enter another number : 3

Addition of 5 and 3 is 8

Subtraction of 8 and 3 is 5

Multiplication of 5 and 3 is 15

Division of 15 and 3 gives quotient 5

Division of 5 and 3 gives remainder 2
*/