/* Problem stmt-
5.Local Static Variable

Repeat Previous Assignment 
the only change is in change_count function
a. initialize variable as "local static variable" instead of "local variable".

*/

#include<stdio.h>

int main(void)
{
	// variable declarations
	// 'a' is a Local Variable. It is local to main() only. 
	int a = 5;

	// function declaration
	void change_a();

	printf("\na in main(): %d", a);

	// a is initialized to 0.
	// a = a + 1 = 0 + 1 = 1
	change_a();

	// Since, 'a' is a local static variable of change_a(), it WILL retain its value from previous call to change_a().
	// So a is 1
	// a = a + 1 = 1 + 1 = 2
	change_a();

	return(0);
}

void change_a()
{
	// variable declarations
	// 'a' is a Local Static Variable. It is local to change_a() only.
	// It will retain its value between calls to change_a()
	static int a = 0;

	a++;
	printf("\na in change_a(): %d", a);
}

/*
Output-
a in main(): 5
a in change_a(): 1
a in change_a(): 2
*/