/* Problem stmt-
4. Ordinary Local Variable

initialize one local varible in main say "a" variable with 5.
print variable a.
call function -> change_count() from main function 2 times.

a.change_count ->
i)initialize local variable with same name "local_count" with 0.
i)increase local_count variable by one.
ii)print variable local_count in it.
*/

#include<stdio.h>

// global function declaration
void change_a();

int main(void)
{
	int a = 5;
	printf("\na in main(): %d", a);

	// local_count is initialized to 0 in change_a().
	// a = a + 1 = 0 + 1 = 1
	change_a();

	// Since, 'a' is an ordinary local variable of change_count(), it will NOT retain its value from previous call to change_count().
	// So a is AGAIN initialized to 0
	// a = a + 1 = 0 + 1 = 1
	change_a();

	return(0);
}

void change_a()
{
	int a = 0;
	a++;
	printf("\na in change_a(): %d", a);
}

/*
Output-
a in main(): 5
a in change_a(): 1
a in change_a(): 1
*/