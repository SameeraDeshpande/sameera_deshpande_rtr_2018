#include<stdio.h>

int main(void)
{
	// code
	printf("\n");
	printf("Going to next line using \\n escape sequence!!! \n\n");
	printf("Demonstrating \t horizontal \t tab \t using \t \\t escape \t sequence!!!\n\n");
	printf("\"This is a double quoted output\" done using \\\" \\\" escape sequence\n\n");
	printf("\'This is a single quoted output\' done using \\\' \\\' escape sequence\n\n");
	printf("BACKSPACE turned to BACKSPACE\b using \\b escape sequence\n\n");

	printf("\r Demonstrating carriage return using \\r escape sequence\n\n");
	printf("Demonstrating \r carriage return using \\r escape sequence\n\n");
	printf("Demonstrating carriage \r return using \\r escape sequence\n\n");

	printf("Demonstrating \x41 using \\xhh escape sequence\n\n");	//0x41 is the hexadecimal representation of A. Syntax: x followed by two digits
	printf("Demonstrating \102 using \\ooo escape sequence\n\n");	//102 is the octal representation of B. Syntax: three digits forming an octal no.
	return(0);
}

/*Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\05-EscapeSequences>EscapeSeq.exe

Going to next line using \n escape sequence!!!

Demonstrating    horizontal      tab     using   \t escape       sequence!!!

"This is a double quoted output" done using \" \" escape sequence

'This is a single quoted output' done using \' \' escape sequence

BACKSPACE turned to BACKSPAC using \b escape sequence

Demonstrating carriage return using \r escape sequence

carriage return using \r escape sequence

return using \r escape sequence

Demonstrating A using \xhh escape sequence

Demonstrating B using \ooo escape sequence
*/