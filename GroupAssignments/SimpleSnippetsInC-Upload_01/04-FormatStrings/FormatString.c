/*
Problem stmt-
int number = 13;
i)print number in Decimal value with %d
ii)print number in octal format by %o
iii)print number in Hexadecimal format by
%x -> (Hexadecimal Letters In Lower Case)
%X -> (Hexadecimal Letters In Upper Case)

char ch = 'P';
iv)print number in character format by %c
char str[] = "Astromedicomp";
v)print number in string format by %s

long num = 30121995L;
vi)print num in long integer format by %ld

unsigned int b = 7;
vii)print Unsigned Integer by using %u

float f_num = 3012.1995f;
viii) print f_num using
a)%f
b)%4.2f
c)%2.5f

double d_pi = 3.14159265358979323846;

ix) print d_pi using
a)%g (Double Precision Floating Point Number Without Exponential)
b)%e (Double Precision Floating Point Number With Exponential (Lower Case))
c)%E (Double Precision Floating Point Number With Exponential (Upper Case))
d)%a (Double Hexadecimal Value Of 'd_pi' (Hexadecimal Letters In Lower Case))
e)%A (Double Hexadecimal Value Of 'd_pi' (Hexadecimal Letters In Upper Case))
*/

#include<stdio.h>

int main(void)
{
	int num = 13;

	printf("\nnum in decimal: %d", num);
	printf("\nnum in octal: %o", num);
	printf("\nnum in hexadecimal lower case: %x", num);
	printf("\nnum in hexadecimal upper case: %X", num);

	char ch = 'p';
	printf("\nch in character format: %c", ch);

	char str[] = "Astromedicomp";
	printf("\nstr in string format: %s", str);

	long num2 = 30121995L;
	printf("\nnum2 in long integer format: %ld", num2);

	unsigned int b = 7;
	printf("\nunsigned int b: %ld", b);

	float f_num1 = 3012.1995f;
	printf("\nf_num1: %f", f_num1);
	printf("\nf_num1 in 4.2f: %4.2f", f_num1);
	printf("\nf_num1 in 2.5f: %2.5f", f_num1);

	double d_pi = 3.14159265358979323846;
	printf("\nd_pi without exponential: %g", d_pi);
	printf("\nd_pi With Exponential (Lower Case): %e", d_pi);
	printf("\nd_pi With Exponential (Upper Case): %E", d_pi);
	printf("\nHexadecimal Value Of 'd_pi' (Hexadecimal Letters In Lower Case): %a", d_pi);
	printf("\nDouble Hexadecimal Value Of 'd_pi' (Hexadecimal Letters In Upper Case): %A", d_pi);

	return(0);
}

/*Output-
num in decimal: 13
num in octal: 15
num in hexadecimal lower case: d
num in hexadecimal upper case: D
ch in character format: p
str in string format: Astromedicomp
num2 in long integer format: 30121995
unsigned int b: 7
f_num1: 3012.199463
f_num1 in 4.2f: 3012.20
f_num1 in 2.5f: 3012.19946
d_pi without exponential: 3.14159
d_pi With Exponential (Lower Case): 3.141593e+00
d_pi With Exponential (Upper Case): 3.141593E+00
Hexadecimal Value Of 'd_pi' (Hexadecimal Letters In Lower Case): 0x1.921fb54442d18p+1
Double Hexadecimal Value Of 'd_pi' (Hexadecimal Letters In Upper Case): 0X1.921FB54442D18P+1
*/