#include<stdio.h>

int main(void)
{
	// variable declarations
	int a, b;
	char ch_1, ch_2;

	int x, result_int;
	float y, result_float;

	int i_explicit;
	float f_explicit;

	// inter-conversion and implicit type-casting between char and int types
	a = 67;
	ch_1 = a;

	printf("\na=%d", a);
	printf("\nch_1 after 'ch_1=a' = %c", ch_1);

	ch_2 = 'q';
	b = ch_2;

	printf("\nch_2=%c", ch_2);
	printf("\nb after 'b=ch_2' = %d", b);

	// implicit conversion of int to float
	x = 6;
	y = 5.5;

	result_float = x + y;
	printf("\nInteger x=%d added to Float y=%f gives Float result_float=%f", x, y, result_float);

	result_int = x + y;
	printf("\nInteger x=%d added to Float y=%f gives Integer result_int=%d", x, y, result_int);

	// explicit type-casting using cast operator
	f_explicit = 7.4567;
	i_explicit = (int)f_explicit;

	printf("\nFloating point no. type-casted explicitly is %f", f_explicit);
	printf("\nResultant integer after typecasting %f to int=%d", f_explicit, i_explicit);

	printf("\n");
	return(0);
}

/*
Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\SimpleSnippetsInC-Upload_01\08-TypeConversion>TypeConversion.exe

a=67
ch_1 after 'ch_1=a' = C
ch_2=q
b after 'b=ch_2' = 113
Integer x=6 added to Float y=5.500000 gives Float result_float=11.500000
Integer x=6 added to Float y=5.500000 gives Integer result_int=11
Floating point no. type-casted explicitly is 7.456700
Resultant integer after typecasting 7.456700 to int=7
*/