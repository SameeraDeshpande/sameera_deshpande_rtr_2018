#include<stdio.h>

#define MY_PI 3.1415926535897932

// Un-named enums
enum
{
	SUNDAY,
	MONDAY,
	TUESDAY,
	WEDNESDAY,
	THURSDAY,
	FRIDAY,
	SATURDAY
};	//If First Constant Is Not Assigned A Value, It Is Assumed To Be 0 i.e: 'SUNDAY' Will Be 0
	//And The Rest Of The Constants Are Assigned Consecutive Integer Values i.e: 'MONDAY' Will Be 1, 'TUESDAY' Will Be 2, and so on...

enum 
{
	JANUARY=1,
	FEBRUARY,
	MARCH,
	APRIL,
	MAY,
	JUNE,
	JULY,
	AUGUST,
	SEPTEMBER,
	OCTOBER,
	NOVEMBER,
	DECEMBER
};

// named enums
enum Numbers
{
	ONE,
	TWO,
	THREE,
	FOUR,
	FIVE=5,
	SIX,
	SEVEN,
	EIGHT,
	NINE,
	TEN
};

enum boolean
{
	TRUE=1,
	FALSE=0
};

int main(void)
{
	// local constant declarations
	const double epsilon = 0.000001;

	// code
	printf("\n\n");
	printf("Local constant Epsilon=%lf\n\n", epsilon);	// lf=double

	printf("Sunday is day number %d\n", SUNDAY);
	printf("Monday is day number %d\n", MONDAY);
	printf("Tuesday is day number %d\n", TUESDAY);
	printf("Wednesday is day number %d\n", WEDNESDAY);
	printf("Thursday is day number %d\n", THURSDAY);
	printf("Friday is day number %d\n", FRIDAY);
	printf("Saturday is day number %d\n\n", SATURDAY);

	printf("One is enum number %d\n", ONE);
	printf("Two is enum number %d\n", TWO);
	printf("Three is enum number %d\n", THREE);
	printf("Four is enum number %d\n", FOUR);
	printf("Five is enum number %d\n", FIVE);
	printf("Six is enum number %d\n", SIX);
	printf("Seven is enum number %d\n", SEVEN);
	printf("Eight is enum number %d\n", EIGHT);
	printf("Nine is enum number %d\n", NINE);
	printf("Ten is enum number %d\n\n", TEN);

	printf("January is month number %d\n", JANUARY);
	printf("February is month number %d\n", FEBRUARY);
	printf("March is month number %d\n", MARCH);
	printf("April is month number %d\n", APRIL);
	printf("May is month number %d\n", MAY);
	printf("June is month number %d\n", JUNE);
	printf("July is month number %d\n", JULY);
	printf("August is month number %d\n", AUGUST);
	printf("September is month number %d\n", SEPTEMBER);
	printf("October is month number %d\n", OCTOBER);
	printf("November is month number %d\n", NOVEMBER);
	printf("December is month number %d\n\n", DECEMBER);

	printf("Value of TRUE is %d\n", TRUE);
	printf("Value of FALSE is %d\n", FALSE);

	printf("MY_PI macro value is %.10lf\n", MY_PI);
	printf("Area of circle with radius 3 is %f\n\n", MY_PI * 3 * 3);

	return(0);
}

/*Output-
Local constant Epsilon=0.000001

Sunday is day number 0
Monday is day number 1
Tuesday is day number 2
Wednesday is day number 3
Thursday is day number 4
Friday is day number 5
Saturday is day number 6

One is enum number 0
Two is enum number 1
Three is enum number 2
Four is enum number 3
Five is enum number 5
Six is enum number 6
Seven is enum number 7
Eight is enum number 8
Nine is enum number 9
Ten is enum number 10

January is month number 1
February is month number 2
March is month number 3
April is month number 4
May is month number 5
June is month number 6
July is month number 7
August is month number 8
September is month number 9
October is month number 10
November is month number 11
December is month number 12

Value of TRUE is 1
Value of FALSE is 0
MY_PI macro value is 3.1415926536
Area of circle with radius 3 is 28.274334
*/