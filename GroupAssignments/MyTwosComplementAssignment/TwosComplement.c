#include<stdio.h>
#include<math.h>

// global function declarations
//void show_other_representations(int);
void convert_into_octal(int,int*);
void convert_into_hex(int, char*);
void convert_into_binary(int, int*);
void display_octal(int*);
void display_hex(char*);
void display_binary(int*);
void ones_complement(int *);
void twos_complement(int *);
void print_decimal(int*);

int main(int argc,char* argv[])
{
	int binary[8] = { 0 },i;
	int octal[3] = { 0 };
	char hex[2] = { 0 };

	int num = atoi(argv[1]);
	printf("\nThe given decimal number is : %d", num);

	//show_other_representations(num);

	convert_into_octal(num, octal);
	printf("\n\nThe octal representation of the given decimal number is : ");
	display_octal(octal);

	convert_into_hex(num, hex);
	printf("\n\nThe hexadecimal representation of the given decimal number is : ");
	display_hex(hex);

	convert_into_binary(num, binary);
	printf("\n\nThe binary representation of the given decimal number is : ");
	display_binary(binary);

	ones_complement(binary);
	printf("\n\nThe one's complement of the given decimal number is : ");
	display_binary(binary);
	printf("\n\nThe decimal representation of this one's complement is : ");
	print_decimal(binary);

	twos_complement(binary);
	printf("\n\nThe two's complement of the given decimal number is : ");
	display_binary(binary);
	printf("\n\nThe decimal representation of this two's complement is : ");
	print_decimal(binary);
	printf("\n");

	return(0);
}

//void show_other_representations(num)
//{
//	printf("\n\nGiven number in octal format is : %o", num);
//	printf("\n\nGiven number in hex format is : %x", num);
//}

void convert_into_octal(int num, int*rem)
{
	int i = 2;

	while (num != 0 && i >= 0)
	{
		rem[i] = num % 8;
		num = num / 8;
		i--;
	}
}

void convert_into_hex(int num,char* hex)
{
	int i = 1,rem;
	
	while (num != 0)
	{
		rem = num % 16;
		num = num / 16;

		switch (rem)
		{
		case 0:
			hex[i] = '0';
			break;
		case 1:
			hex[i] = '1';
			break;
		case 2:
			hex[i] = '2';
			break;
		case 3:
			hex[i] = '3';
			break;
		case 4:
			hex[i] = '4';
			break;
		case 5:
			hex[i] = '5';
			break;
		case 6:
			hex[i] = '6';
			break;
		case 7:
			hex[i] = '7';
			break;
		case 8:
			hex[i] = '8';
			break;
		case 9:
			hex[i] = '9';
			break;

		case 10:
			hex[i] = 'A';
			break;
		case 11:
			hex[i] = 'B';
			break;
		case 12:
			hex[i] = 'C';
			break;
		case 13:
			hex[i] = 'D';
			break;
		case 14:
			hex[i] = 'E';
			break;
		case 15:
			hex[i] = 'F';
			break;
		}
		i--;
	}
}

void convert_into_binary(int num,int* rem)
{
	int i = 7;

	while (num != 0 && i>=0)
	{
		rem[i] = num % 2;
		num = num / 2;
		i--;
	}
}

void display_octal(int *num)
{
	int i;

	for (i = 0;i < 3;i++)
		printf("%d", num[i]);
}

void display_hex(char *num)
{
	int i;

	for (i = 0;i < 2;i++)
		printf("%c", num[i]);
}

void display_binary(int *num)
{
	int i;

	for (i = 0;i < 8;i++)
		printf("%d", num[i]);
}

void ones_complement(int *num)
{
	int i;

	for (i = 0;i < 8;i++)
	{
		//num[i] = ~num[i];
		if (num[i] == 0)
			num[i] = 1;
		else  // 1
			num[i] = 0;
	}
}

void twos_complement(int *ones_c)
{
	int i = 7,flag=0;

	while (ones_c[i] != 0)
	{
		// 1
		ones_c[i] = 0;
		i--;
		if (i == 0)	// reached MSB
		{
			flag = 1;
			break;
		}
	}
		
	if (flag == 0)	// not yet at MSB
	{
		ones_c[i] = 1;	// making the first 0 from LSB 1
	}
	
}

void print_decimal(int *binary)
{
	int dec = 0,i, j = -1;

	for (i = 7;i >= 0;i--)
	{
		j = j + 1;
		if (binary[i] == 1)
		{
			if (j == 0)
				dec = dec + 1;
			else
				dec = dec + pow(2, j);
		}
	}
	printf("%d", dec);
}

/*Output-
C:\MyVisualStudio2017Projects\02-OpenGL\Other\MyTwosComplementAssignment>TwosComplement.exe 255

The given decimal number is : 255

The octal representation of the given decimal number is : 377

The hexadecimal representation of the given decimal number is : FF

The binary representation of the given decimal number is : 11111111

The one's complement of the given decimal number is : 00000000

The decimal representation of this one's complement is : 0

The two's complement of the given decimal number is : 00000001

The decimal representation of this two's complement is : 1

C:\MyVisualStudio2017Projects\02-OpenGL\Other\MyTwosComplementAssignment>TwosComplement.exe 17

The given decimal number is : 17

The octal representation of the given decimal number is : 021

The hexadecimal representation of the given decimal number is : 11

The binary representation of the given decimal number is : 00010001

The one's complement of the given decimal number is : 11101110

The decimal representation of this one's complement is : 238

The two's complement of the given decimal number is : 11101111

The decimal representation of this two's complement is : 239

*/