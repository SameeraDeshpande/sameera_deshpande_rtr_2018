// standard header files
#include<iostream>	// namespace
#include<stdio.h>	// printf()
#include<stdlib.h>	// exit()
#include<memory.h>	// memset()
//X11 header files
#include<X11/Xlib.h>	// similar to windows.h; for all XServer APIs
#include<X11/Xutil.h> 	// XVisualInfo
#include<X11/XKBlib.h>	// keyboard utilization header
#include<X11/keysym.h>	// KeySym;	defines certain preprocessor symbols; mapping between keycode and symbol (eg. XK_F to ASCII 70)
// OpenGL related header files
#include<GL/glew.h>
#include<GL/gl.h>	// has OpenGL related functions and data types
#include<GL/glx.h>	// glx: OpenGL for XWindows (bridging APIs)	//~WGL
#include "vmath.h"
#include"Sphere.h"

// namespaces
using namespace std;	// to avoid using 'std::' everywhere

// global variable declarations
bool bFullscreen=false;		// cpp feature (false=0)
Display *gpDisplay=NULL;
XVisualInfo* gpXVisualInfo=NULL;	// ~pfd (s/w representation of graphics card)
Colormap gColormap;	// struct; The colormap is a small table with entries specifying the RGB values of the currently available colors. The hardware imposes limits on the number of significant bits in these values.
				    // In X, the colormap entries are called color cells. The pixel values are interpreted as indices to the color cells. 
					// Thus, to refresh the screen, the display hardware uses the RGB values in the color cells indexed by the pixel values. 
					// The size of the colormap is less than or equal to 2^N color cells, where N is the number of bits in a pixel value. A device with 8 bits per pixel value, for example, will have a colormap with up to 256 color cells, indexed though pixel values ranging from 0 through 255. 
Window gWindow;	// struct; ~wndclass
int giWindowWidth=800;
int giWindowHeight=600;
static GLXContext gGlxContext;		// ~ghrc; static is optional. Do global static if you don't want it to be accessible outside this file

typedef GLXContext (*glXCreateContextAttribsARBProc) (Display*,GLXFBConfig,GLXContext,Bool,const int*);	// fn ptr (not possible to make call w/o this)
																										// glxCreateContextAttribsARBProc: our type
																										// GLXContext: ret type
																										// (Display*,GLXFBConfig,GLXContext,Bool,const int*: parameters
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;
GLXFBConfig gGlxFbConfig;

bool gbLighting = false;

GLenum result;
GLuint gShaderProgramObject;

GLuint gVao_sphere;		// vertex array object
GLuint gVbo_sphere_position, gVbo_sphere_normal, gVbo_sphere_element;		// vertex buffer object
GLuint mUniform;		// model matrix
GLuint vUniform;		// view matrix	// taken seperate for freedom
GLuint pUniform;	// projection; seperate since calculation of mormal matrix needs mv matrix only
GLuint laUniform;	// intensity of ambient light
GLuint ldUniform;	// intensity of diffused light
GLuint lsUniform;	// intensity of specular light
GLuint lightPositionUniform;
GLuint kaUniform;	// coefficient of material's ambient reflectivity
GLuint kdUniform;	// coefficient of material's diffused reflectivity
GLuint ksUniform;	// coefficient of material's specular reflectivity 
GLuint materialShininessUniform; // material shininess
GLuint isLKeyPressedUniform;

vmath::mat4 perspectiveProjectionMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array
vmath::mat4 modelMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array
vmath::mat4 viewMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array

GLint keypress = 0;	// no key pressed
GLfloat r = 100.0f;
GLfloat angleOfXRotation = 0.0f;
GLfloat angleOfYRotation = 0.0f;
GLfloat angleOfZRotation = 0.0f;
int window_width;
int window_height;

GLfloat light_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_diffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[4] = { 0.0f,0.0f,0.0f,1.0f };

float sphere_vertices[1146];
float sphere_normals[1146];
float sphere_textures[764];
unsigned short sphere_elements[2280];
GLsizei gNumVertices;
GLsizei gNumElements;

enum	// nameless since we are just concerned with its "named" indices
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,		// 1
	AMC_ATTRIBUTE_NORMAL,		// 2
	AMC_ATTRIBUTE_TEXCOORD0,	// 3 (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// entry-point function
int main(void)	// no WinMain() here
{
	// function declarations
	void CreateWindow(void);
	void InitializeOGL(void);
	void ResizeOGL(int,int);
	void UpdateOGL(void);
	void DisplayOGL(void);
	void UninitializeOGL(void);
	void ToggleFullscreen(void);

	// variable declarations
	window_width=giWindowWidth;
	window_height=giWindowHeight;
	char keys[26];
	bool bDone=false;

	// code
	// create window
	CreateWindow();	// gpDisplay, gWindow, gColormap, gXVisualInfo are defined in this function

	// call to Initialize()
	InitializeOGL();

	// game loop
	XEvent event;	//  In the X protocol, X clients send requests to the X server. The server sends replies, errors, and events to its clients. 
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))		// returns the number of events that have been received from the X server but have not been removed from the event queue (asynchronous)	// ~PeekMessage()
		{
			XNextEvent(gpDisplay,&event);	// ~GetMessage(); 
											// The XNextEvent() function copies the first event from the event queue into the specified XEvent structure and then removes it from the queue. 
											// If the event queue is empty, XNextEvent() flushes the output buffer and blocks until an event is received
											// gpDisplay : Specifies the connection to the X server
											// &event ~ &msg
			switch(event.type)
			{
				case MapNotify:	// ~WM_CREATE (occurs only once) but creates as well as shows window
								// XServer maps its default window given to the client with the window with the client provided attributes
				break;

				case KeyPress:	// ~WM_KEYDOWN
				keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);	// convert keycode to key symbol
																				// event.xkey.keycode: keycode; 
																				// 0: page no. of UNICODE (0=ANSI i.e. English) (default)
																				// 0: is shift key pressed? (no)
				switch(keysym)	// ~SWITCH(wParam)
				{
					case XK_Escape:
					bDone=true;
					break;

					default:
					break;
				}

				XLookupString(&event.xkey,	// XKeyEvent*; string is in xkey
					keys,					// char*; the translated string comes into this buffer
					sizeof(keys),			// size of buffer
					NULL,					// KeySym*; returns the KeySym computed from the event if this argument is not NULL. 
					NULL 					// XComposeStatus*;	saves old state
					);	// translates a key event to a string and a KeySym
				
				switch(keys[0])
				{
					case 'F':
					case 'f':
						if(bFullscreen==false)
						{
							ToggleFullscreen();
							bFullscreen=true;
						}
						else
						{
							ToggleFullscreen();
							bFullscreen=false;
						}
					break;

					case 'L':
					case 'l':
						if (gbLighting == false)
							gbLighting = true;
						else
							gbLighting = false;
					break;

					case 'X':
					case'x':
						if (gbLighting)
						{
							keypress = 1;
							angleOfXRotation = 0.0f;	// resetting angleOfXRotation
						}
						break;

					case 'Y':
					case'y':
						if (gbLighting)
						{
							keypress = 2;
							angleOfYRotation = 0.0f;	// resetting angleOfYRotation
						}
						break;

					case 'Z':
					case'z':
						if (gbLighting)
						{
							keypress = 3;
							angleOfZRotation = 0.0f;	// resetting angleOfZRotation
						}
						break;
				}
				break;	// case keypress

				case ButtonPress:	// mouse click
				switch(event.xbutton.button)	// xbutton: XButtonEvent; button: int(detail)
				{
					case 1: // ~WM_LBUTTONDOWN
						printf("Left mouse button is pressed\n");
					break;

					case 2:	// middle button
						printf("Middle mouse button is pressed\n");
					break;

					case 3:	// ~WM_RBUTTONDOWN
						printf("Right mouse button is pressed\n");
					break;

					case 4:	// mouse wheel up
						printf("Mouse wheel is released\n");
					break;

					case 5:	// mouse wheel down
						printf("Mouse wheel is pressed\n");
					break;

					default:
					break;
				}
				break;

				case MotionNotify:	// ~WM_MOUSEMOVE
				break;

				case ConfigureNotify:	// ~WM_SIZE (lParam gives size of window in Windows)
				window_width=event.xconfigure.width;	// xconfigure: XConfigureEvent
				window_height=event.xconfigure.height;	// The width and height members are set to the inside size of the window, not including the border
				ResizeOGL(window_width,window_height);
				break;

				case Expose:	// ~WM_PAINT	// nothing written since we want def black color
				break;

				case DestroyNotify:	// ~WM_CLOSE (option of whether to exit or not is given to user)
				break;

				case 33:	// const; When close button is clicked or close is chosen from menu, 33 is sent to window manager
							// this is just a provision for exit
				bDone=true;
				break;	// discipline; no use here


				default:
				break;
			}	// switch(event.type)
		}	// while(XPending())

		UpdateOGL();
		DisplayOGL();
		
	}	// while(bDone==false)

	// clean-up
	UninitializeOGL();

	return(0);
}

void CreateWindow(void)
{
	// function declarations
	void UninitializeOGL(void);

	// variable declarations
	XSetWindowAttributes winAttribs;	// attributes capable of being set
	int defaultScreen;
	//int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[]={	// static not necessary in our OpenGL practices since we do not call this function again so no need to retain its value (but can be needed in case of multiple displays)
		GLX_X_RENDERABLE, True,			// True is value of GLX_X_RENDERABLE (kvc)
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE,8,		// Must be followed by a nonnegative minimum size specification
							// allocate 8 bit for red(since single buffer) (hence, 32-bit frame buffer); GLX_RED_SIZE is an enumerated type attribute
							// key-value pairs; Integer attributes and enumerated type attributes are followed immediately by the corresponding desired or minimum value
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,	// for depth
		GLX_STENCIL_SIZE,8,	// stencil buff reqd for shadow and reflection
		GLX_DOUBLEBUFFER,True,	// If present, only double-buffered visuals are considered
		None 				// the last attribute must be None.
							// in case of a const array, when you don't wish to assign values to all its members but you need to pass that array to a function, do end with 0/None (#define None 0)
							// None means telling it to give values to other attributes
	};	// best FBA for Linux PP
	Screen *screen;

	GLXFBConfig *pGlxFbConfigs=NULL;
	GLXFBConfig bestGlxFbConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNumberOfFbConfigs=0;
	int bestFrameBufferConfig=-1;	// index
	int bestNumberOfSamples=-1;		// how many samples it can give
	int worstFrameBufferConfig=-1;	// index
	int worstNumberOfSamples=999;

	// code
	// open the XServer connection and get Display
	gpDisplay=XOpenDisplay(NULL);	// To open a connection to the X server that controls a display;	NULL: get default Display (#define NULL 0)
									// returns a Display structure that serves as the connection to the X server and that contains all the information about that X server (representative of client)
									// will be sent as first parameter of further functions for the session manager to authencicate the client
	if(gpDisplay==NULL)	// error checking; to check if mem is assigned or not
	{
		printf("ERROR: Unable To Open X Display.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	 // get default screen using gpDisplay obtained in previous step
	defaultScreen=XDefaultScreen(gpDisplay);	// ~MONITORINFOF_PRIMARY	// no need of error checking
												// return the default screen number referenced by the XOpenDisplay() function.
												// use defaultScreen just for obtaining a screen of matching VisualInfo

	// retrieve all FBConfigs the driver has
	pGlxFbConfigs=glXChooseFBConfig(gpDisplay,
		defaultScreen,
		frameBufferAttributes,
		&iNumberOfFbConfigs);	// tells XServer to take FBA for this defaultScreen and return all matching FBConfigs in LHS variable and no. of them in last para

	printf("\nThere are %d matching FBConfigs.\n",iNumberOfFbConfigs);

	// In FFP, you used to take the best matching VisualInfo(not exactly what you want) and based on that adjustment you would further take device context. But in PP, you demand the exact VisualInfo you want.
	// Get VisualInfo from FBConfig and then get context from FBConfig

	// from each obtained FBConfig, get a temporary VisualInfo
	for(int i=0;i<iNumberOfFbConfigs;i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGlxFbConfigs[i]);	// temp since in loop

		if(pTempXVisualInfo)
		{
			int sampleBuffers,samples;	// fine parts of an object are called samples and they are stored in sample buffers

			// tell glx to tell XServer to give sample buff for this FBConfig in last para
			glXGetFBConfigAttrib(gpDisplay,
				pGlxFbConfigs[i],		// for which FBConfig
				GLX_SAMPLE_BUFFERS,		// what is needed
				&sampleBuffers);

			// get no. of samples from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay,
				pGlxFbConfigs[i],
				GLX_SAMPLES,
				&samples);

			// FBConfig with the most no. of sampleBuffers,samples is the best. Therefore, do the comparision
			// get the best from all (like sorting logic)
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)	// first >,< then && then ||
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples=samples;
			}

			// get the worst from all (not needed but good practice)
			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNumberOfSamples)	// first >,< then && then ||
			{
				worstFrameBufferConfig=i;
				worstNumberOfSamples=samples;
			}
		}

		// free pTempXVisualInfo
		XFree(pTempXVisualInfo);
	}

	// now assign the best found one
	bestGlxFbConfig=pGlxFbConfigs[bestFrameBufferConfig];

	// assign the same best to global one
	gGlxFbConfig=bestGlxFbConfig;

	// free the obtained pGlxFbConfigs array
	XFree(pGlxFbConfigs);

	// accordingly, get the best VisualInfo
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGlxFbConfig);

	if(gpXVisualInfo==NULL)	// error checking; to check if mem is assigned or not
	{
		printf("ERROR : Unable To Allocate Memory For Visual Info.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	// fill window attributes;	~wndclass
	winAttribs.border_pixel=0;	// border color; pixel=color; 	0= don't care
	winAttribs.border_pixmap=0;	// border image; 0= don't create; pixmap~ bitmap of Windows
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);	// background color 	// ~hBrush
																		// returns the black pixel value for the specified screen.
																		// macro of XBlackPixel() 
	winAttribs.background_pixmap=0;		// background image

	winAttribs.colormap=XCreateColormap(
		gpDisplay,		// the connection to the X server
		RootWindow(gpDisplay,gpXVisualInfo->screen),	// the window on whose screen you want to create a colormap; give matching screen to h/w 
		gpXVisualInfo->visual,						// a visual type supported on the screen; h/w's representation
		AllocNone									// Specifies the colormap entries to be allocated; AllocNone: do not allocate mem for colormap(the colormap initially has no allocated entries, and clients can allocate them)
		);	//~cbClsExtra and cbWndExtra

	gColormap=winAttribs.colormap;	// saving the colormap(since we have done AllocNone)	// why (not used further)?

	// specify the events(messages) that you want the X server to return to a client application. 
	winAttribs.event_mask = VisibilityChangeMask|KeyPressMask|ButtonPressMask|PointerMotionMask|StructureNotifyMask|ExposureMask; // VisibilityChangeMask: for case MapNotify
																																 // KeyPressMask: for case KeyPress
																																 // ButtonPressMask: for case ButtonPress (mouse button down events)
																																 // PointerMotionMask: for case MotionNotify
																																 // StructureNotifyMask: for case ConfigureNotify (resize)
																																 // ExposureMask: for case Expose
																																 // no mask for case DestroyNotify since it is always there; also there exists no mask for case 33
	// fill window styles
	// like for creating event mask you refer events, for creating style mask you refer attribs
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;	

	// create actual window
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay,gpXVisualInfo->screen),// parent; telling that it is root window's sub-window with h/w matching screen (gXVisualInfo.screen)
		0,		// x (origin at left-top)
		0,		// y (origin at left-top)
		giWindowWidth,								// width
		giWindowHeight,								// height
		0,											// border width
		gpXVisualInfo->depth,						// depth
		InputOutput,								// class; your window is going to take inputs and also give outputs
		gpXVisualInfo->visual,						// visual
		styleMask,									// specifies which window attributes are defined in the attributes(last) argument
		&winAttribs 								// the structure from which the attributes (as specified by the style mask) are to be taken; address given since it fills the remaining attribs
		);	// creates an unmapped subwindow for a specified parent window, returns the window ID of the created window, and causes the X server to generate a CreateNotify event
			// The created window is not yet displayed (mapped) on the user's display;
			// ~CreateWindow()
			// To display the window, call XMapWindow()

	if(!gWindow)
	{
		printf("ERROR: Failed To Create Main Window.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	// give name to the window
	XStoreName(gpDisplay,gWindow,"Linux Window");	// gWindow: the window to which the name is to assigned

	// steps to close the window  on close btn and close menu (to actually exit after case 33; case 33 just brings the msg of closing the window)
	// specify the atom identifier of the protocol for deleting window
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);	// returns the atom identifier associated with the specified atom_name string (WM_DELETE_WINDOW)
																				// XInternAtom: X Server's internal immutable protocols(protocols cannot be changed by us, they are immutable, hence Atom)
																				// WM_DELETE_WINDOW: WM= Window Manager; tell X Server to use this protocol on case 33
																				// last parameter: True means create protocol/atom irrespective of whether it is already present or not(False means create protocol/atom only if it is absent)
	// add above protocol in window manager's protocol list
	XSetWMProtocols(gpDisplay,
		gWindow,				// window for which the above protocol is to be attached
		&windowManagerDelete,	// the list(array) of protocols(here, only 1)
		1						// only one protocol(no array)
		);

	// map the default window given by XServer(10,10,200,200) to your window(0,0,800,600)
	XMapWindow(gpDisplay,gWindow);	// the window is mapped, and the X server generates a MapNotify event
									// When all its ancestors are mapped, the window becomes viewable and will be visible on the screen if it is not obscured by another window. 
									// ~ShowWindow()

	// window centering
	screen=XScreenOfDisplay(gpDisplay, gpXVisualInfo->screen);	// return a pointer to the indicated screen number

	XMoveWindow(gpDisplay,
		gWindow,
		XWidthOfScreen(screen)/2-giWindowWidth/2,	// x; XWidthOfScreen(screen): returns width of the specified screen in pixels
		XHeightOfScreen(screen)/2-giWindowHeight/2	// y; XHeightOfScreen(screen): returns height of the specified screen in pixels
		);	//  define the new location of the top-left pixel of the window's border or the window itself if it has no border
}

void InitializeOGL(void)
{
	// function declaration
	void UninitializeOGL(void);
	void ResizeOGL(int,int);

	// variable declarations
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	// code
	// library is loaded but fn addr is not known. Therefore get its addr using glx as fn is of OGL
	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB"); // now glxCreateContextAttribsARB on LHS is your fn, call it directly
																															 // typecast into (glxCreateContextAttribsARBProc) since originally void*
																															 // typecast into (GLubyte*) since in OGL, string is not char*, it is GLubyte*
																															 // glxCreateContextAttribsARB: name of fn whose addr is needed
																															 // fn ending with ARB means it is implementation dependent

	if(glXCreateContextAttribsARB==NULL)
	{
		printf("glXGetProcAddressARB() failed");
		UninitializeOGL();
		exit(0);
	}

	// declaration of context attributes array
	const int attribs[]={
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,5,	// latest OGL context is 4.5
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	// now get the context
	gGlxContext=glXCreateContextAttribsARB(
		gpDisplay,
		gGlxFbConfig,	// give context based on this (best FbConfig)
		0,				// Multi-monitor sharable context. We don't have, hence 0
		GL_TRUE,		// true gives you h/w RC, false gives you s/w RC
		attribs
		);

	if(!gGlxContext)
	{
		// if not obtained the highest one(i.e. for 4.5), specify the lowest one. It will give you the highest one known to it
		const int attribs[]={
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,	// min version by Linux: 1.0 to 1.3
			None
		}; // cannot ask for core now

		gGlxContext=glXCreateContextAttribsARB(
		gpDisplay,
		gGlxFbConfig,	// give context based on this (best FbConfig)
		0,				// Multi-monitor sharable context. We don't have, hence 0
		GL_TRUE,		// true gives you h/w RC, false gives you s/w RC
		attribs
		);
	}

	// check whether the obtained context is really h/w RC or not
	if(!glXIsDirect(gpDisplay,gGlxContext))
	{
		printf("The obtained context is not hardware rendering context.\n\n");
	}
	else
	{
		printf("The obtained context is hardware rendering context.\n\n");
	}

	// make the obtained context as current OGL context
	glXMakeCurrent(gpDisplay,
		gWindow,		// Specifies a GLX drawable. Must be either an X window ID or a GLX pixmap ID.
		gGlxContext);	//  makes gGlxContext the current GLX rendering context of the calling thread	// ~wglMakeCurrent()

	// put on the OpenGL extensions
	result = glewInit();	 
	if (result != GLEW_OK)
	{
		printf("\nglewInit() Failed");
		UninitializeOGL();	// good practice
		exit(0);
	}

	// vertex shader-
	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);	// creates a shader that is given as parameter	// 1st line of programmable pipeline !

	// write vertex shader source code
	const GLchar* vertexShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"out vec3 t_norm;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform vec4 u_light_position;" \
		"uniform int u_isLKeyPressed;" \
		"void main(void)" \
		"{" \
		"if(u_isLKeyPressed==1)" \
		"{" \
		"vec4 eye_coordinates =  u_v_matrix * u_m_matrix * vPosition;" \
		"t_norm = mat3(u_v_matrix * u_m_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position - eye_coordinates);" \
		"viewer_vector = vec3(-eye_coordinates.xyz);" \
		"}" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version statement
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
				// eye coordinates are not related to any projection, hence only mv
				// uniform mat4 u_mv_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// mat3 normal_matrix = mat3(u_mv_matrix): typecasting into mat3 gives you the normal matrix i.e. mat3(transpose(inverse(u_mv_matrix))); It gives first upper 3*3 matrix of the 4*4 matrix. transpose(), inverse() are shader functions
				// t_norm: transformed normal. normalize() is a shader function (our 'n')
				// vec3(u_light_position - eye_coordinates)= typecasting vec4(x,y,z,w) into vec3;
				// tn_dot_lightDirection (s.n): tn=transformed normal
				// reflect(-light_direction,t_norm): -light_direction since reflected ray is opposite to light direction
				// viewer_vector = normalize(vec3(-eye_coordinates.xyx)): -eye_coordinates since viewer's coordinates are opposite to object's eye coordinates
				// ambient = u_la * u_ka: Ia = La * Ka
				// diffuse = u_ld * u_kd * tn_dot_lightDirection: Id = Ld * Kd * (s.n)
				// specular = u_ls * u_ks * pow(max(dot(reflection_vector * viewer_vector),0.0),u_material_shininess): Is = Ls * Ks * (r.v)^f where r: reflection vector, v: viewer vector, f: material shininess (f= exponential factor)
				// phong_ads_light = ambient + diffuse + specular: I = Ia + Id + Is
				// gl_Position: in-built variable of shader				
				// u_p_matrix * u_v_matrix * u_m_matrix: opposite sequence of mvp				

	// give above source code to the vertex shader object
	glShaderSource(
		gVertexShaderObject,	// shader to which the source code is to be given
		1,	// the number of strings in the array
		(const GLchar **)&vertexShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);	// replace the source code in a shader object

	// compile the vertex shader
	glCompileShader(gVertexShaderObject);
	// error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gVertexShaderObject,	//  the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gVertexShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
					);						// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
											// When a shader object is created, its information log will be a string of length 0.
				printf("\nVertex Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				UninitializeOGL();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
			}
		}
	}

	// fragment shader-
	// define vertex shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);	// creates a shader that is given as parameter

	// write fragment shader source code
	const GLchar* fragmentShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"in vec3 t_norm;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_isLKeyPressed;" \
		"out vec4 frag_color;" \
		"vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_isLKeyPressed==1)" \
		"{" \
		"vec3 normalized_t_norm = normalize(t_norm);" \
		"vec3 normalized_light_direction = normalize(light_direction);" \
		"vec3 normalized_viewer_vector = normalize(viewer_vector);" \
		"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_t_norm);" \
		"float tn_dot_lightDirection = max(dot(normalized_light_direction,normalized_t_norm),0.0);" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 diffuse = u_ld * u_kd * tn_dot_lightDirection;" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_viewer_vector),0.0),u_material_shininess);" \
		"phong_ads_light = ambient + diffuse + specular;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"frag_color = vec4(phong_ads_light, 1.0);" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt
				// in vec4 out_color: VS's out is FS's in. Hence, same name. (gl_Position is in-built hence need not be passed)
				// out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
				// vec4: function/macro/constructor (of vmath?)
				// frag_color = vec4(diffuse_color,1.0): 1.0 just for converting vec3 to vec4
				// actually, checking the state of u_isLKeyPressed again is not a good code here. Instead, we should pass a variable from VS to FS for that state. But we are checking it here to explain the concept that uniforms(u_isLKeyPressed) are global to shaders.
				// frag_color = vec4(1.0,1.0,1.0,1.0): will give white color to fragment; don't write 'f' in shaders (if class)

	// give above source code to the fragment shader object
	glShaderSource(
		gFragmentShaderObject,	// shader to which the source code is to be given
		1,		// the number of strings in the array
		(const GLchar **)&fragmentShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);			// replace the source code in a shader object

	// compile the fragment shader
	glCompileShader(gFragmentShaderObject);
	// error checking
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gFragmentShaderObject,	//  the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gFragmentShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
					);						// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
											// When a shader object is created, its information log will be a string of length 0.
				printf("\nFragment Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				UninitializeOGL();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
			}
		}
	}

	// create shader program object 
	gShaderProgramObject = glCreateProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_POSITION,					// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
		// give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_NORMAL,					// the index of the generic vertex attribute to be bound.
		"vNormal"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	// link the shader program to your program
	glLinkProgram(gShaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
	// error checking
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(				// iv= integer vector
		gShaderProgramObject,	//  the program to be queried.
		GL_LINK_STATUS,			// what(object parameter) is to be queried	
		&iProgramLinkStatus		// empty	//  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
	);							// return a parameter from a program object
	if (iProgramLinkStatus == GL_FALSE)	// error present
	{
		// check if the linker has any info. about the error
		glGetProgramiv(gShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetProgramInfoLog(
					gShaderProgramObject,	// the program object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
											// When a program object is created, its information log will be a string of length 0.
				printf("\nShader Program Link Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				UninitializeOGL();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
			}
		}
	}

	// post-linking retrieving uniform locations (uniforms are global to shaders)
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");	// preparation of data transfer from CPU to GPU (binding)
																			// gShaderProgramObject: Specifies the program object to be queried; u_mv_matrix: of GPU; mvUniform: of CPU
																			// telling it to take location of uniform u_mv_matrix and give in mvUniform
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	isLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_isLKeyPressed");

	// fill sphere vertices in array (this was in Display() in FFP)
	getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
	gNumVertices = getNumberOfSphereVertices();
	gNumElements = getNumberOfSphereElements();

	// 9 lines
	// create vao(id) (vao is shape-wise)
	// vao
	glGenVertexArrays(1, &gVao_sphere);
	glBindVertexArray(gVao_sphere);

	// position vbo
	glGenBuffers(1, &gVbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// normal vbo
	glGenBuffers(1, &gVbo_sphere_normal);
	glBindBuffer(GL_ARRAY_BUFFER, gVbo_sphere_normal);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &gVbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);

	glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

	// usual OGL code
	glClearColor(0.25f,0.25f,0.25f,1.0f);	// clear the screen by OGL color

	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();
	perspectiveProjectionMatrix = vmath::mat4::identity();	// making perspectiveProjectionMatrix an identity matrix(diagonals 1)
															// mat4: of vmath
	// warmup call to ResizeOGL()
	ResizeOGL(giWindowWidth,giWindowHeight);
}

void ResizeOGL(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);// ? other values of near & far not working
																											// parameters:
																											// fovy- The field of view angle, in degrees, in the y - direction.
																											// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																											// zNear- The distance from the viewer to the near clipping plane(always positive).
																											// zFar- The distance from the viewer to the far clipping plane(always positive).*/
}

void DisplayOGL(void)
{
	// function declarations
	void Draw24Spheres(void);

	// code
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

	// One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	glUseProgram(gShaderProgramObject);	// binding your OpenGL code with shader program object
										// Specifies the handle of the program object whose executables are to be used as part of current rendering state.

	if (keypress == 1)	// x
	{
		light_position[0] = 0.0f;
		light_position[1] = r * sinf(angleOfXRotation);
		light_position[2] = r * cosf(angleOfXRotation);
	}
	else if (keypress == 2)	// y
	{
		light_position[0] = r * cosf(angleOfYRotation);
		light_position[1] = 0.0f;
		light_position[2] = r * sinf(angleOfYRotation);
	}
	else if (keypress == 3)	// z
	{
		light_position[0] = r * cosf(angleOfZRotation);
		light_position[1] = r * sinf(angleOfZRotation);
		light_position[2] = 0.0f;
	}
	else
	{
		light_position[0] = 0.0f;
		light_position[1] = 0.0f;
		light_position[2] = 0.0f;
	}

	// 4 CPU steps
	// declaration of matrices
	vmath::mat4 translationMatrix;

	// initialize above matrices to identity (not initialised in above step for better visibility and understanding)
	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();

	// do necessary transformation (T,S,R)
	translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
	GLint width = window_width / 6;
	GLint height = window_height / 4;
	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// for barrel distortion

	// do necessary matrix multiplication 
	modelMatrix = translationMatrix;

	// fill and send uniforms
	// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mUniform,		// uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelMatrix					// actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
	);

	glUniformMatrix4fv(vUniform,		// uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		viewMatrix					// actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
	);

	glUniformMatrix4fv(pUniform,		// uniform in which perspectiveProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		perspectiveProjectionMatrix		// actual matrix; this will bind to pUniform which is bound to u_p_matrix
	);

	if (gbLighting == true)
	{
		glUniform1i(isLKeyPressedUniform, 1);	// send 1 if 'L' key is pressed; 1i: to send 1 int

		glUniform3fv(laUniform, 1, light_ambient);	// to send vec3
		glUniform3fv(ldUniform, 1, light_diffuse);	// to send vec3
		glUniform3fv(lsUniform, 1, light_specular);	// to send vec3
		glUniform4fv(lightPositionUniform, 1, light_position);	// to send vec4; ~glLightfv(GL_LIGHT0, GL_POSITION, LightPosition) in FFP
	}
	else
	{
		glUniform1i(isLKeyPressedUniform, 0);	// send 0 if 'L' key is not pressed; 1i: to send 1 int
	}

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	// *** bind vao ***
	glBindVertexArray(gVao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gVbo_sphere_element);

	Draw24Spheres();

	// *** unbind vao ***
	glBindVertexArray(0);
	
	glUseProgram(0);	// unbinding your OpenGL code with shader program object
						// If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.

	glXSwapBuffers(gpDisplay,gWindow);	// exchange front and back buffers; gWindow: Specifies the drawable whose buffers are to be swapped
}

void Draw24Spheres(void)
{
	GLint width = window_width / 6;
	GLint height = window_height / 4;

	GLfloat material_ambient[4];
	GLfloat material_diffuse[4];
	GLfloat material_specular[4];
	GLfloat material_shininess;

	// =============================================================================================================================
	// first row - gems
	// emerald
	material_ambient[0] = 0.0215f;	// r
	material_ambient[1] = 0.1745f;	// g
	material_ambient[2] = 0.0215f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.07568f;	// r
	material_diffuse[1] = 0.61424f;	// g
	material_diffuse[2] = 0.07568f;	// b
	material_diffuse[3] = 1.0f;		// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.633f;		// r
	material_specular[1] = 0.727811f;	// g
	material_specular[2] = 0.633f;		// b
	material_specular[3] = 1.0f;		// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.6f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(0, 3*(GLint)height, (GLsizei)width, (GLsizei)height);	// for barrel distortion

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);	// When glDrawElements is called, it uses 'count' sequential indices from 'indices' to lookup elements in enabled arrays to construct a sequence of geometric primitives. 
		// 'mode' specifies what kind of primitives are constructed, and how the array elements construct these primitives. 
		// If GL_VERTEX_ARRAY is not enabled, no geometric primitives are constructed.

	//-----------------------------------------------------------------------------------------------------------------------------
	// jade
	material_ambient[0] = 0.135f;	// r
	material_ambient[1] = 0.2225f;	// g
	material_ambient[2] = 0.1575f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.54f;	// r
	material_diffuse[1] = 0.89f;	// g
	material_diffuse[2] = 0.63f;	// b
	material_diffuse[3] = 1.0f;		// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.316228f;	// r
	material_specular[1] = 0.316228f;	// g
	material_specular[2] = 0.316228f;	// b
	material_specular[3] = 1.0f;		// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.1f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(width, 3 * (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// obsidian
	material_ambient[0] = 0.05375f;	// r
	material_ambient[1] = 0.05f;		// g
	material_ambient[2] = 0.06625f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.18275f;	// r
	material_diffuse[1] = 0.17f;		// g
	material_diffuse[2] = 0.22525f;	// b
	material_diffuse[3] = 1.0f;		// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.332741f;	// r
	material_specular[1] = 0.328634f;	// g
	material_specular[2] = 0.346435f;	// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.3f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(2*width, 3 * (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// pearl
	material_ambient[0] = 0.25f;		// r
	material_ambient[1] = 0.20725f;	// g
	material_ambient[2] = 0.20725f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 1.0f;		// r
	material_diffuse[1] = 0.829f;	// g
	material_diffuse[2] = 0.829f;	// b
	material_diffuse[3] = 1.0f;		// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.296648f;		// r
	material_specular[1] = 0.296648f;		// g
	material_specular[2] = 0.296648f;		// b
	material_specular[3] = 1.0f;				// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.088f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(3 * width, 3 * (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// ruby
	material_ambient[0] = 0.1745f;		// r
	material_ambient[1] = 0.01175f;	// g
	material_ambient[2] = 0.01175f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.61424f;		// r
	material_diffuse[1] = 0.04136f;	// g
	material_diffuse[2] = 0.04136f;	// b
	material_diffuse[3] = 1.0f;		// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.727811f;		// r
	material_specular[1] = 0.626959f;		// g
	material_specular[2] = 0.626959f;		// b
	material_specular[3] = 1.0f;				// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.6f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(4 * width, 3 * (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// turquoise
	material_ambient[0] = 0.1f;		// r
	material_ambient[1] = 0.18725f;	// g
	material_ambient[2] = 0.1745f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.396f;		// r
	material_diffuse[1] = 0.74151f;	// g
	material_diffuse[2] = 0.69102f;	// b
	material_diffuse[3] = 1.0f;		// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.297254f;		// r
	material_specular[1] = 0.30829f;		// g
	material_specular[2] = 0.306678f;		// b
	material_specular[3] = 1.0f;				// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.1f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(5 * width, 3 * (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	// =============================================================================================================================
	// second row - metals
	// brass
	material_ambient[0] = 0.329412f;	// r
	material_ambient[1] = 0.223529f;	// g
	material_ambient[2] = 0.027451f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.780392f;	// r
	material_diffuse[1] = 0.568627f;	// g
	material_diffuse[2] = 0.113725f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.992157f;		// r
	material_specular[1] = 0.941176f;	// g
	material_specular[2] = 0.807843f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = (GLfloat)0.21794872 * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(0, 2 * (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// bronze
	material_ambient[0] = 0.2125f;	// r
	material_ambient[1] = 0.1275f;	// g
	material_ambient[2] = 0.054f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.714f;	// r
	material_diffuse[1] = 0.4284f;	// g
	material_diffuse[2] = 0.18144f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.393548f;		// r
	material_specular[1] = 0.271906f;	// g
	material_specular[2] = 0.166721f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.2f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(width, 2 * (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// chrome
	material_ambient[0] = 0.25f;	// r
	material_ambient[1] = 0.25f;	// g
	material_ambient[2] = 0.25f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.4f;	// r
	material_diffuse[1] = 0.4f;	// g
	material_diffuse[2] = 0.4f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.774597f;		// r
	material_specular[1] = 0.774597f;	// g
	material_specular[2] = 0.774597f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.6f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(2*width, 2 * (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// copper
	material_ambient[0] = 0.19125f;	// r
	material_ambient[1] = 0.0735f;	// g
	material_ambient[2] = 0.0225f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.7038f;	// r
	material_diffuse[1] = 0.27048f;	// g
	material_diffuse[2] = 0.0828f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.256777f;		// r
	material_specular[1] = 0.137622f;	// g
	material_specular[2] = 0.086014f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.1f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(3 * width, 2 * (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// gold
	material_ambient[0] = 0.24725f;	// r
	material_ambient[1] = 0.1995f;	// g
	material_ambient[2] = 0.0745f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.75164f;	// r
	material_diffuse[1] = 0.60648f;	// g
	material_diffuse[2] = 0.22648f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.628281f;		// r
	material_specular[1] = 0.555802f;	// g
	material_specular[2] = 0.366065f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.4f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(4 * width, 2 * (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// silver
	material_ambient[0] = 0.19225f;	// r
	material_ambient[1] = 0.19225f;	// g
	material_ambient[2] = 0.19225f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.50754f;	// r
	material_diffuse[1] = 0.50754f;	// g
	material_diffuse[2] = 0.50754f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.508273f;		// r
	material_specular[1] = 0.508273f;	// g
	material_specular[2] = 0.508273f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.4f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(5 * width, 2 * (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	// =============================================================================================================================
	// third row - plastic
	// black
	material_ambient[0] = 0.0f;	// r
	material_ambient[1] = 0.0f;	// g
	material_ambient[2] = 0.0f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.01f;	// r
	material_diffuse[1] = 0.01f;	// g
	material_diffuse[2] = 0.01f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.50f;		// r
	material_specular[1] = 0.50f;	// g
	material_specular[2] = 0.50f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.25f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(0, (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// cyan
	material_ambient[0] = 0.0f;	// r
	material_ambient[1] = 0.1f;	// g
	material_ambient[2] = 0.06f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.0f;	// r
	material_diffuse[1] = 0.50980392f;	// g
	material_diffuse[2] = 0.50980392f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.50196078f;		// r
	material_specular[1] = 0.50196078f;	// g
	material_specular[2] = 0.50196078f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.25f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(width, (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// green
	material_ambient[0] = 0.0f;	// r
	material_ambient[1] = 0.0f;	// g
	material_ambient[2] = 0.0f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.1f;	// r
	material_diffuse[1] = 0.35f;	// g
	material_diffuse[2] = 0.1f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.45f;		// r
	material_specular[1] = 0.55f;	// g
	material_specular[2] = 0.45f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.25f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(2*width, (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// red
	material_ambient[0] = 0.0f;	// r
	material_ambient[1] = 0.0f;	// g
	material_ambient[2] = 0.0f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.5f;	// r
	material_diffuse[1] = 0.0f;	// g
	material_diffuse[2] = 0.0f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.7f;		// r
	material_specular[1] = 0.6f;	// g
	material_specular[2] = 0.6f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.25f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(3 * width, (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// white
	material_ambient[0] = 0.0f;	// r
	material_ambient[1] = 0.0f;	// g
	material_ambient[2] = 0.0f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.55f;	// r
	material_diffuse[1] = 0.55f;	// g
	material_diffuse[2] = 0.55f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.70f;		// r
	material_specular[1] = 0.70f;	// g
	material_specular[2] = 0.70f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.25f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(4 * width, (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// yellow
	material_ambient[0] = 0.0f;	// r
	material_ambient[1] = 0.0f;	// g
	material_ambient[2] = 0.0f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.5f;	// r
	material_diffuse[1] = 0.5f;	// g
	material_diffuse[2] = 0.0f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.60f;		// r
	material_specular[1] = 0.60f;	// g
	material_specular[2] = 0.50f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.25f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(5 * width, (GLint)height, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	// =============================================================================================================================
	// fourth row - rubber
	// black
	material_ambient[0] = 0.02f;	// r
	material_ambient[1] = 0.02f;	// g
	material_ambient[2] = 0.02f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.01f;	// r
	material_diffuse[1] = 0.01f;	// g
	material_diffuse[2] = 0.01f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.4f;		// r
	material_specular[1] = 0.4f;	// g
	material_specular[2] = 0.4f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.078125f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// cyan
	material_ambient[0] = 0.0f;	// r
	material_ambient[1] = 0.05f;	// g
	material_ambient[2] = 0.05f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.4f;	// r
	material_diffuse[1] = 0.5f;	// g
	material_diffuse[2] = 0.5f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.04f;		// r
	material_specular[1] = 0.7f;	// g
	material_specular[2] = 0.7f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.078125f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(width, 0, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// green
	material_ambient[0] = 0.0f;	// r
	material_ambient[1] = 0.05f;	// g
	material_ambient[2] = 0.0f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.4f;	// r
	material_diffuse[1] = 0.5f;	// g
	material_diffuse[2] = 0.4f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.04f;		// r
	material_specular[1] = 0.7f;	// g
	material_specular[2] = 0.04f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.078125f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(2*width, 0, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// red
	material_ambient[0] = 0.05f;	// r
	material_ambient[1] = 0.0f;	// g
	material_ambient[2] = 0.0f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.5f;	// r
	material_diffuse[1] = 0.4f;	// g
	material_diffuse[2] = 0.4f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.7f;		// r
	material_specular[1] = 0.04f;	// g
	material_specular[2] = 0.04f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.078125f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(3 * width, 0, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// white
	material_ambient[0] = 0.05f;	// r
	material_ambient[1] = 0.05f;	// g
	material_ambient[2] = 0.05f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.5f;	// r
	material_diffuse[1] = 0.5f;	// g
	material_diffuse[2] = 0.5f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.7f;		// r
	material_specular[1] = 0.7f;	// g
	material_specular[2] = 0.7f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.078125f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(4 * width, 0, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);

	//-----------------------------------------------------------------------------------------------------------------------------
	// yellow
	material_ambient[0] = 0.05f;	// r
	material_ambient[1] = 0.05f;	// g
	material_ambient[2] = 0.0f;	// b
	material_ambient[3] = 1.0f;		// a
	glUniform3fv(kaUniform, 1, material_ambient);

	material_diffuse[0] = 0.5f;	// r
	material_diffuse[1] = 0.5f;	// g
	material_diffuse[2] = 0.4f;	// b
	material_diffuse[3] = 1.0f;	// a
	glUniform3fv(kdUniform, 1, material_diffuse);

	material_specular[0] = 0.7f;		// r
	material_specular[1] = 0.7f;	// g
	material_specular[2] = 0.04f;		// b
	material_specular[3] = 1.0f;			// a
	glUniform3fv(ksUniform, 1, material_specular);

	material_shininess = 0.078125f * 128.0f;
	glUniform1f(materialShininessUniform, material_shininess);

	glViewport(5 * width, 0, (GLsizei)width, (GLsizei)height);	// left top

	// sphere
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);
}

void UpdateOGL()
{
	angleOfXRotation = angleOfXRotation + (GLfloat)(M_PI / 180.0f)*1.0f;

	if (angleOfXRotation >= 360.0f)
		angleOfXRotation = 0.0f;

	angleOfYRotation = angleOfYRotation + (GLfloat)(M_PI / 180.0f)*1.0f;

	if (angleOfYRotation >= 360.0f)
		angleOfYRotation = 0.0f;

	angleOfZRotation = angleOfZRotation + (GLfloat)(M_PI / 180.0f)*1.0f;

	if (angleOfZRotation >= 360.0f)
		angleOfZRotation = 0.0f;
}

void UninitializeOGL(void)
{
	
	// opposite sequence of vao and vbo also ok
	if (gVbo_sphere_element)
	{
		glDeleteBuffers(1, &gVbo_sphere_element);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		gVbo_sphere_element = 0;
	}

	if (gVbo_sphere_normal)
	{
		glDeleteBuffers(1, &gVbo_sphere_normal);	// delete named buffer objects
													// 1: number of buffer objects to be deleted
													// &vbo: array of buffer objects to be deleted
		gVbo_sphere_normal = 0;
	}

	if (gVbo_sphere_position)
	{
		glDeleteBuffers(1, &gVbo_sphere_position);	// delete named buffer objects
													// 1: number of buffer objects to be deleted
													// &vbo: array of buffer objects to be deleted
		gVbo_sphere_position = 0;
	}

	if (gVao_sphere)
	{
		glDeleteVertexArrays(1, &gVao_sphere);		// delete vertex array objects
		gVao_sphere = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;	// typedef int
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);		// since unused in Display()
		
		// ask program that how many shaders are attached to it
		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
			&shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);		// dynamic array for shaders since we don't know how many present
		if (pShaders)	// mem allocated
		{
			// take attached shaders  into above array
			glGetAttachedShaders(gShaderProgramObject,		// the program object to be queried
				shaderCount,								// the size of the array for storing the returned object names
				&shaderCount,								// Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
				pShaders									// an array that is used to return the names of attached shader objects(empty now)
			);		// return the handles of the shader objects attached to a program object

			for (shaderNumber = 0;shaderNumber < shaderCount;shaderNumber++)
			{
				// detach each shader
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
																					// 2nd para: Specifies the shader object to be detached.

				// delete each detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		// delete the shader program
		glDeleteProgram(gShaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	GLXContext currentGlxContext=glXGetCurrentContext();	// return the current context; can be diff from gGlxContext in multi-monitor mode

	if(currentGlxContext!=NULL && currentGlxContext==gGlxContext)
	{
		// release the current context
		glXMakeCurrent(gpDisplay,0,0);	// ~wglMakeCurrent(NULL,NULL)
	}

	// reclaim
	if(gGlxContext)
	{
		glXDestroyContext(gpDisplay,gGlxContext);	// If the GLX rendering context ctx is not current to any thread, glXDestroyContext() destroys it immediately.
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);	// The XDestroyWindow() function destroys the specified window as well as all of its subwindows and causes the X server to generate a DestroyNotify event for each window.
											// ~ DestroyWindow() (sends WM_DESTROY msg)
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		// Close the connection to the X server for the display specified in the Display structure and destroys all windows, resource IDs (Window, Font, Pixmap, Colormap, Cursor, and GContext), or other resources that the client has created on this display.
		// Therefore, these windows, resource IDs, and other resources should never be referenced again or an error will be generated.
		// Before exiting, you should call XCloseDisplay() explicitly so that any pending errors are reported as XCloseDisplay() performs a final XSync() operation.
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};	// event; XEvent: union

	// code
	// take the current state
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);	// _NET is for network protocol

	memset(&xev,0,sizeof(xev));	// &xev: Pointer to the object to copy the 0
	xev.type=ClientMessage;	// type of event/msg (ClientMessage: client is going to send it; in Xlib.h)
							// X server generates ClientMessage events only when a client calls XSendEvent(). 
	xev.xclient.window=gWindow;	// the window related to the event; xclient: XClientMessageEvent structure;
	xev.xclient.message_type=wm_state;	// type of message; indicates how the data should be interpreted by the receiving client
	xev.xclient.format=32;	// format (no. of bits) of msg
	xev.xclient.data.l[0]=bFullscreen?0:1;	// The data member is a union that contains the members b(byte), s(short), and l(long). The b, s, and l members represent data of 20 8-bit values, 10 16-bit values, and 5 32-bit values.
											// If (bFullscreen) then l[0]=0 else l[0]=1
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;	// assign 1st member of l to new state(?)

	// if(bFullscreen==true)
	// 	XFreeCursor(gpDisplay,None);
	// else
	// 	XDefineCursor(gpDisplay,gWindow,None);

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay,gpXVisualInfo->screen),	// the window the event is to be sent to
		False,										// is the msg propagatable to sub-windows/other clients? (False since only 1 window is going to be fullscreen)
		StructureNotifyMask,						// event mask; fullscreen means changing size of the window and the event mask for resizing is StructureNotifyMask
		&xev 										// address of the event that is to be sent
		);
}
