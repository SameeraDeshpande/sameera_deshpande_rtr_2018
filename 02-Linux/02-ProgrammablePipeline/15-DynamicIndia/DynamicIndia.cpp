// standard header files
#include<iostream>	// namespace
#include<stdio.h>	// printf()
#include<stdlib.h>	// exit()
#include<memory.h>	// memset()
//X11 header files
#include<X11/Xlib.h>	// similar to windows.h; for all XServer APIs
#include<X11/Xutil.h> 	// XVisualInfo
#include<X11/XKBlib.h>	// keyboard utilization header
#include<X11/keysym.h>	// KeySym;	defines certain preprocessor symbols; mapping between keycode and symbol (eg. XK_F to ASCII 70)
// OpenGL related header files
#include<GL/glew.h>
#include<GL/gl.h>	// has OpenGL related functions and data types
#include<GL/glx.h>	// glx: OpenGL for XWindows (bridging APIs)	//~WGL
#include "vmath.h"

// namespaces
using namespace std;	// to avoid using 'std::' everywhere

// global variable declarations
bool bFullscreen=false;		// cpp feature (false=0)
Display *gpDisplay=NULL;
XVisualInfo* gpXVisualInfo=NULL;	// ~pfd (s/w representation of graphics card)
Colormap gColormap;	// struct; The colormap is a small table with entries specifying the RGB values of the currently available colors. The hardware imposes limits on the number of significant bits in these values.
				    // In X, the colormap entries are called color cells. The pixel values are interpreted as indices to the color cells. 
					// Thus, to refresh the screen, the display hardware uses the RGB values in the color cells indexed by the pixel values. 
					// The size of the colormap is less than or equal to 2^N color cells, where N is the number of bits in a pixel value. A device with 8 bits per pixel value, for example, will have a colormap with up to 256 color cells, indexed though pixel values ranging from 0 through 255. 
Window gWindow;	// struct; ~wndclass
int giWindowWidth=800;
int giWindowHeight=600;
static GLXContext gGlxContext;		// ~ghrc; static is optional. Do global static if you don't want it to be accessible outside this file

typedef GLXContext (*glXCreateContextAttribsARBProc) (Display*,GLXFBConfig,GLXContext,Bool,const int*);	// fn ptr (not possible to make call w/o this)
																										// glxCreateContextAttribsARBProc: our type
																										// GLXContext: ret type
																										// (Display*,GLXFBConfig,GLXContext,Bool,const int*: parameters
glXCreateContextAttribsARBProc glXCreateContextAttribsARB=NULL;
GLXFBConfig gGlxFbConfig;

GLenum result;
GLuint gVertexShaderObject;
GLuint gFragmentShaderObject;
GLuint gShaderProgramObject;

GLuint vao_I, vao_N, vao_D, vao_A, vao_plane, vao_smoke, vao_A_band;		// vertex array object	(1 also ok since we unbind)
GLuint vbo_position_I, vbo_color_I, vbo_position_N, vbo_color_N, vbo_position_D, vbo_color_D, vbo_position_A, vbo_color_A, vbo_position_plane, vbo_position_smoke, vbo_position_A_band, vbo_color_A_band;
GLuint mvpUniform;		// mvp: model-view projection

GLfloat letter_x = 0.46f;
GLfloat letter_start = -0.19f, letter_end = 0.19f;
GLfloat i1_x = -3.0f;
GLfloat a_x = 2.50f;
GLfloat n_y = 1.58f;
GLfloat i2_y = -1.58f;
GLfloat r_saffron = 0.0f, g_saffron = 0.0f, b_saffron = 0.0f;
GLfloat r_green = 0.0f, g_green = 0.0f, b_green = 0.0f;
GLfloat r = 1.42f;
GLfloat r_saffron_smoke = 255.0f, g_saffron_smoke = 153.0f, b_saffron_smoke = 51.0f;
GLfloat r_white_smoke = 1.0f;
GLfloat r_green_smoke = 18.0f, g_green_smoke = 136.0f, b_green_smoke = 7.0f;
GLfloat middle_plane_x = -letter_x * 5.4f;
GLfloat angle_upper_plane = -90.0f;
GLfloat angle_lower_plane = 90.0f;
GLfloat angle = (GLfloat)M_PI;
GLfloat angle_lower = (GLfloat)M_PI;
GLfloat angle_right_upper = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI;
GLfloat angle_right_lower = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI;

bool bDoneI1 = false;
bool bDoneA = false;
bool bDoneN = false;
bool bDoneI2 = false;
bool bDoneR_saffron = false, bDoneG_saffron = false, bDoneB_saffron = false;
bool bDoneR_green = false, bDoneG_green = false, bDoneB_green = false;
bool bDoneD = false;
bool bDoneMidddlePlane = false;
bool bDoneUpperPlane = false;
bool bDoneDetachPlanes = false;
bool bDoneFadingSmokes = false;

// declaration of matrices
vmath::mat4 modelViewMatrix;
vmath::mat4 modelViewProjectionMatrix;
vmath::mat4 translationMatrix;
vmath::mat4 rotationMatrix;
vmath::mat4 perspectiveProjectionMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array

enum	// nameless since we are just concerned with its "named" indices
{
	AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,		// 1
	AMC_ATTRIBUTE_NORMAL,		// 2
	AMC_ATTRIBUTE_TEXCOORD0,	// 3 (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// entry-point function
int main(void)	// no WinMain() here
{
	// function declarations
	void CreateWindow(void);
	void InitializeOGL(void);
	void ResizeOGL(int,int);
	void UpdateOGL(void);
	void DisplayOGL(void);
	void UninitializeOGL(void);
	void ToggleFullscreen(void);

	// variable declarations
	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;
	char keys[26];
	bool bDone=false;

	// code
	// create window
	CreateWindow();	// gpDisplay, gWindow, gColormap, gXVisualInfo are defined in this function

	// call to Initialize()
	InitializeOGL();

	// game loop
	XEvent event;	//  In the X protocol, X clients send requests to the X server. The server sends replies, errors, and events to its clients. 
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))		// returns the number of events that have been received from the X server but have not been removed from the event queue (asynchronous)	// ~PeekMessage()
		{
			XNextEvent(gpDisplay,&event);	// ~GetMessage(); 
											// The XNextEvent() function copies the first event from the event queue into the specified XEvent structure and then removes it from the queue. 
											// If the event queue is empty, XNextEvent() flushes the output buffer and blocks until an event is received
											// gpDisplay : Specifies the connection to the X server
											// &event ~ &msg
			switch(event.type)
			{
				case MapNotify:	// ~WM_CREATE (occurs only once) but creates as well as shows window
								// XServer maps its default window given to the client with the window with the client provided attributes
				break;

				case KeyPress:	// ~WM_KEYDOWN
				keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);	// convert keycode to key symbol
																				// event.xkey.keycode: keycode; 
																				// 0: page no. of UNICODE (0=ANSI i.e. English) (default)
																				// 0: is shift key pressed? (no)
				switch(keysym)	// ~SWITCH(wParam)
				{
					case XK_Escape:
					bDone=true;
					break;

					default:
					break;
				}

				// XLookupString(&event.xkey,	// XKeyEvent*; string is in xkey
				// 	keys,					// char*; the translated string comes into this buffer
				// 	sizeof(keys),			// size of buffer
				// 	NULL,					// KeySym*; returns the KeySym computed from the event if this argument is not NULL. 
				// 	NULL 					// XComposeStatus*;	saves old state
				// 	);	// translates a key event to a string and a KeySym
				
				// switch(keys[0])
				// {
				// 	case 'F':
				// 	case 'f':
				// 	if(bFullscreen==false)
				// 	{
				// 		ToggleFullscreen();
				// 		bFullscreen=true;
				// 	}
				// 	else
				// 	{
				// 		ToggleFullscreen();
				// 		bFullscreen=false;
				// 	}
				// 	break;
				// }
				break;	// case keypress

				case ButtonPress:	// mouse click
				switch(event.xbutton.button)	// xbutton: XButtonEvent; button: int(detail)
				{
					case 1: // ~WM_LBUTTONDOWN
						printf("Left mouse button is pressed\n");
					break;

					case 2:	// middle button
						printf("Middle mouse button is pressed\n");
					break;

					case 3:	// ~WM_RBUTTONDOWN
						printf("Right mouse button is pressed\n");
					break;

					case 4:	// mouse wheel up
						printf("Mouse wheel is released\n");
					break;

					case 5:	// mouse wheel down
						printf("Mouse wheel is pressed\n");
					break;

					default:
					break;
				}
				break;

				case MotionNotify:	// ~WM_MOUSEMOVE
				break;

				case ConfigureNotify:	// ~WM_SIZE (lParam gives size of window in Windows)
				winWidth=event.xconfigure.width;	// xconfigure: XConfigureEvent
				winHeight=event.xconfigure.height;	// The width and height members are set to the inside size of the window, not including the border
				ResizeOGL(winWidth,winHeight);
				break;

				case Expose:	// ~WM_PAINT	// nothing written since we want def black color
				break;

				case DestroyNotify:	// ~WM_CLOSE (option of whether to exit or not is given to user)
				break;

				case 33:	// const; When close button is clicked or close is chosen from menu, 33 is sent to window manager
							// this is just a provision for exit
				bDone=true;
				break;	// discipline; no use here


				default:
				break;
			}	// switch(event.type)
		}	// while(XPending())

		UpdateOGL();
		DisplayOGL();
		
	}	// while(bDone==false)

	// clean-up
	UninitializeOGL();

	return(0);
}

void CreateWindow(void)
{
	// function declarations
	void UninitializeOGL(void);
	void ToggleFullscreen(void);

	// variable declarations
	XSetWindowAttributes winAttribs;	// attributes capable of being set
	int defaultScreen;
	//int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[]={	// static not necessary in our OpenGL practices since we do not call this function again so no need to retain its value (but can be needed in case of multiple displays)
		GLX_X_RENDERABLE, True,			// True is value of GLX_X_RENDERABLE (kvc)
		GLX_DRAWABLE_TYPE,GLX_WINDOW_BIT,
		GLX_RENDER_TYPE,GLX_RGBA_BIT,
		GLX_X_VISUAL_TYPE,GLX_TRUE_COLOR,
		GLX_RED_SIZE,8,		// Must be followed by a nonnegative minimum size specification
							// allocate 8 bit for red(since single buffer) (hence, 32-bit frame buffer); GLX_RED_SIZE is an enumerated type attribute
							// key-value pairs; Integer attributes and enumerated type attributes are followed immediately by the corresponding desired or minimum value
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,	// for depth
		GLX_STENCIL_SIZE,8,	// stencil buff reqd for shadow and reflection
		GLX_DOUBLEBUFFER,True,	// If present, only double-buffered visuals are considered
		None 				// the last attribute must be None.
							// in case of a const array, when you don't wish to assign values to all its members but you need to pass that array to a function, do end with 0/None (#define None 0)
							// None means telling it to give values to other attributes
	};	// best FBA for Linux PP
	Screen *screen;

	GLXFBConfig *pGlxFbConfigs=NULL;
	GLXFBConfig bestGlxFbConfig;
	XVisualInfo *pTempXVisualInfo=NULL;
	int iNumberOfFbConfigs=0;
	int bestFrameBufferConfig=-1;	// index
	int bestNumberOfSamples=-1;		// how many samples it can give
	int worstFrameBufferConfig=-1;	// index
	int worstNumberOfSamples=999;

	// code
	// open the XServer connection and get Display
	gpDisplay=XOpenDisplay(NULL);	// To open a connection to the X server that controls a display;	NULL: get default Display (#define NULL 0)
									// returns a Display structure that serves as the connection to the X server and that contains all the information about that X server (representative of client)
									// will be sent as first parameter of further functions for the session manager to authencicate the client
	if(gpDisplay==NULL)	// error checking; to check if mem is assigned or not
	{
		printf("ERROR: Unable To Open X Display.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	 // get default screen using gpDisplay obtained in previous step
	defaultScreen=XDefaultScreen(gpDisplay);	// ~MONITORINFOF_PRIMARY	// no need of error checking
												// return the default screen number referenced by the XOpenDisplay() function.
												// use defaultScreen just for obtaining a screen of matching VisualInfo

	// retrieve all FBConfigs the driver has
	pGlxFbConfigs=glXChooseFBConfig(gpDisplay,
		defaultScreen,
		frameBufferAttributes,
		&iNumberOfFbConfigs);	// tells XServer to take FBA for this defaultScreen and return all matching FBConfigs in LHS variable and no. of them in last para

	printf("\nThere are %d matching FBConfigs.\n",iNumberOfFbConfigs);

	// In FFP, you used to take the best matching VisualInfo(not exactly what you want) and based on that adjustment you would further take device context. But in PP, you demand the exact VisualInfo you want.
	// Get VisualInfo from FBConfig and then get context from FBConfig

	// from each obtained FBConfig, get a temporary VisualInfo
	for(int i=0;i<iNumberOfFbConfigs;i++)
	{
		pTempXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,pGlxFbConfigs[i]);	// temp since in loop

		if(pTempXVisualInfo)
		{
			int sampleBuffers,samples;	// fine parts of an object are called samples and they are stored in sample buffers

			// tell glx to tell XServer to give sample buff for this FBConfig in last para
			glXGetFBConfigAttrib(gpDisplay,
				pGlxFbConfigs[i],		// for which FBConfig
				GLX_SAMPLE_BUFFERS,		// what is needed
				&sampleBuffers);

			// get no. of samples from respective FBConfig
			glXGetFBConfigAttrib(gpDisplay,
				pGlxFbConfigs[i],
				GLX_SAMPLES,
				&samples);

			// FBConfig with the most no. of sampleBuffers,samples is the best. Therefore, do the comparision
			// get the best from all (like sorting logic)
			if(bestFrameBufferConfig < 0 || sampleBuffers && samples > bestNumberOfSamples)	// first >,< then && then ||
			{
				bestFrameBufferConfig=i;
				bestNumberOfSamples=samples;
			}

			// get the worst from all (not needed but good practice)
			if(worstFrameBufferConfig < 0 || !sampleBuffers || samples < worstNumberOfSamples)	// first >,< then && then ||
			{
				worstFrameBufferConfig=i;
				worstNumberOfSamples=samples;
			}
		}

		// free pTempXVisualInfo
		XFree(pTempXVisualInfo);
	}

	// now assign the best found one
	bestGlxFbConfig=pGlxFbConfigs[bestFrameBufferConfig];

	// assign the same best to global one
	gGlxFbConfig=bestGlxFbConfig;

	// free the obtained pGlxFbConfigs array
	XFree(pGlxFbConfigs);

	// accordingly, get the best VisualInfo
	gpXVisualInfo=glXGetVisualFromFBConfig(gpDisplay,bestGlxFbConfig);

	if(gpXVisualInfo==NULL)	// error checking; to check if mem is assigned or not
	{
		printf("ERROR : Unable To Allocate Memory For Visual Info.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	// fill window attributes;	~wndclass
	winAttribs.border_pixel=0;	// border color; pixel=color; 	0= don't care
	winAttribs.border_pixmap=0;	// border image; 0= don't create; pixmap~ bitmap of Windows
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);	// background color 	// ~hBrush
																		// returns the black pixel value for the specified screen.
																		// macro of XBlackPixel() 
	winAttribs.background_pixmap=0;		// background image

	winAttribs.colormap=XCreateColormap(
		gpDisplay,		// the connection to the X server
		RootWindow(gpDisplay,gpXVisualInfo->screen),	// the window on whose screen you want to create a colormap; give matching screen to h/w 
		gpXVisualInfo->visual,						// a visual type supported on the screen; h/w's representation
		AllocNone									// Specifies the colormap entries to be allocated; AllocNone: do not allocate mem for colormap(the colormap initially has no allocated entries, and clients can allocate them)
		);	//~cbClsExtra and cbWndExtra

	gColormap=winAttribs.colormap;	// saving the colormap(since we have done AllocNone)	// why (not used further)?

	// specify the events(messages) that you want the X server to return to a client application. 
	winAttribs.event_mask = VisibilityChangeMask|KeyPressMask|ButtonPressMask|PointerMotionMask|StructureNotifyMask|ExposureMask; // VisibilityChangeMask: for case MapNotify
																																 // KeyPressMask: for case KeyPress
																																 // ButtonPressMask: for case ButtonPress (mouse button down events)
																																 // PointerMotionMask: for case MotionNotify
																																 // StructureNotifyMask: for case ConfigureNotify (resize)
																																 // ExposureMask: for case Expose
																																 // no mask for case DestroyNotify since it is always there; also there exists no mask for case 33
	// fill window styles
	// like for creating event mask you refer events, for creating style mask you refer attribs
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;	

	// create actual window
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay,gpXVisualInfo->screen),// parent; telling that it is root window's sub-window with h/w matching screen (gXVisualInfo.screen)
		0,		// x (origin at left-top)
		0,		// y (origin at left-top)
		giWindowWidth,								// width
		giWindowHeight,								// height
		0,											// border width
		gpXVisualInfo->depth,						// depth
		InputOutput,								// class; your window is going to take inputs and also give outputs
		gpXVisualInfo->visual,						// visual
		styleMask,									// specifies which window attributes are defined in the attributes(last) argument
		&winAttribs 								// the structure from which the attributes (as specified by the style mask) are to be taken; address given since it fills the remaining attribs
		);	// creates an unmapped subwindow for a specified parent window, returns the window ID of the created window, and causes the X server to generate a CreateNotify event
			// The created window is not yet displayed (mapped) on the user's display;
			// ~CreateWindow()
			// To display the window, call XMapWindow()

	if(!gWindow)
	{
		printf("ERROR: Failed To Create Main Window.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	// give name to the window
	XStoreName(gpDisplay,gWindow,"Linux Window");	// gWindow: the window to which the name is to assigned

	// steps to close the window  on close btn and close menu (to actually exit after case 33; case 33 just brings the msg of closing the window)
	// specify the atom identifier of the protocol for deleting window
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);	// returns the atom identifier associated with the specified atom_name string (WM_DELETE_WINDOW)
																				// XInternAtom: X Server's internal immutable protocols(protocols cannot be changed by us, they are immutable, hence Atom)
																				// WM_DELETE_WINDOW: WM= Window Manager; tell X Server to use this protocol on case 33
																				// last parameter: True means create protocol/atom irrespective of whether it is already present or not(False means create protocol/atom only if it is absent)
	// add above protocol in window manager's protocol list
	XSetWMProtocols(gpDisplay,
		gWindow,				// window for which the above protocol is to be attached
		&windowManagerDelete,	// the list(array) of protocols(here, only 1)
		1						// only one protocol(no array)
		);

	// map the default window given by XServer(10,10,200,200) to your window(0,0,800,600)
	XMapWindow(gpDisplay,gWindow);	// the window is mapped, and the X server generates a MapNotify event
									// When all its ancestors are mapped, the window becomes viewable and will be visible on the screen if it is not obscured by another window. 
									// ~ShowWindow()

	// window centering
	screen=XScreenOfDisplay(gpDisplay, gpXVisualInfo->screen);	// return a pointer to the indicated screen number

	XMoveWindow(gpDisplay,
		gWindow,
		XWidthOfScreen(screen)/2-giWindowWidth/2,	// x; XWidthOfScreen(screen): returns width of the specified screen in pixels
		XHeightOfScreen(screen)/2-giWindowHeight/2	// y; XHeightOfScreen(screen): returns height of the specified screen in pixels
		);	//  define the new location of the top-left pixel of the window's border or the window itself if it has no border

	ToggleFullscreen();
}

void InitializeOGL(void)
{
	// function declaration
	void UninitializeOGL(void);
	void ResizeOGL(int,int);

	// code
	// library is loaded but fn addr is not known. Therefore get its addr using glx as fn is of OGL
	glXCreateContextAttribsARB=(glXCreateContextAttribsARBProc)glXGetProcAddressARB((GLubyte*)"glXCreateContextAttribsARB"); // now glxCreateContextAttribsARB on LHS is your fn, call it directly
																															 // typecast into (glxCreateContextAttribsARBProc) since originally void*
																															 // typecast into (GLubyte*) since in OGL, string is not char*, it is GLubyte*
																															 // glxCreateContextAttribsARB: name of fn whose addr is needed
																															 // fn ending with ARB means it is implementation dependent

	if(glXCreateContextAttribsARB==NULL)
	{
		printf("glXGetProcAddressARB() failed");
		UninitializeOGL();
		exit(0);
	}

	// declaration of context attributes array
	const int attribs[]={
		GLX_CONTEXT_MAJOR_VERSION_ARB,4,
		GLX_CONTEXT_MINOR_VERSION_ARB,5,	// latest OGL context is 4.5
		GLX_CONTEXT_PROFILE_MASK_ARB,GLX_CONTEXT_CORE_PROFILE_BIT_ARB,
		None
	};

	// now get the context
	gGlxContext=glXCreateContextAttribsARB(
		gpDisplay,
		gGlxFbConfig,	// give context based on this (best FbConfig)
		0,				// Multi-monitor sharable context. We don't have, hence 0
		GL_TRUE,		// true gives you h/w RC, false gives you s/w RC
		attribs
		);

	if(!gGlxContext)
	{
		// if not obtained the highest one(i.e. for 4.5), specify the lowest one. It will give you the highest one known to it
		const int attribs[]={
			GLX_CONTEXT_MAJOR_VERSION_ARB,1,
			GLX_CONTEXT_MINOR_VERSION_ARB,0,	// min version by Linux: 1.0 to 1.3
			None
		}; // cannot ask for core now

		gGlxContext=glXCreateContextAttribsARB(
		gpDisplay,
		gGlxFbConfig,	// give context based on this (best FbConfig)
		0,				// Multi-monitor sharable context. We don't have, hence 0
		GL_TRUE,		// true gives you h/w RC, false gives you s/w RC
		attribs
		);
	}

	// check whether the obtained context is really h/w RC or not
	if(!glXIsDirect(gpDisplay,gGlxContext))
	{
		printf("The obtained context is not hardware rendering context.\n\n");
	}
	else
	{
		printf("The obtained context is hardware rendering context.\n\n");
	}

	// make the obtained context as current OGL context
	glXMakeCurrent(gpDisplay,
		gWindow,		// Specifies a GLX drawable. Must be either an X window ID or a GLX pixmap ID.
		gGlxContext);	//  makes gGlxContext the current GLX rendering context of the calling thread	// ~wglMakeCurrent()

	// put on the OpenGL extensions
	result = glewInit();	 
	if (result != GLEW_OK)
	{
		printf("\nglewInit() Failed");
		UninitializeOGL();	// good practice
		exit(0);
	}

	// vertex shader-
	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);	// creates a shader that is given as parameter	// 1st line of programmable pipeline !

	// write vertex shader source code
	const GLchar* vertexShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
				// uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// gl_Position: in-built variable of shader				
				// out_color = vColor: assigning in attribute to out attribute
				// here, color is going to be given to frag color directly				

	// give above source code to the vertex shader object
	glShaderSource(
		gVertexShaderObject,	// shader to which the source code is to be given
		1,	// the number of strings in the array
		(const GLchar **)&vertexShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);	// replace the source code in a shader object

	// compile the vertex shader
	glCompileShader(gVertexShaderObject);
	// error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gVertexShaderObject,	//  the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gVertexShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
					);						// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
											// When a shader object is created, its information log will be a string of length 0.
				printf("\nVertex Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				UninitializeOGL();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
			}
		}
	}

	// fragment shader-
	// define vertex shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);	// creates a shader that is given as parameter

	// write fragment shader source code
	const GLchar* fragmentShaderSourceCode = 
		"#version 450 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 FragColor;" \
		"void main(void)" \
		"{" \
		"FragColor = out_color;"
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt
				// in vec4 out_color: VS's out is FS's in. Hence, same name. (gl_Position is in-built hence need not be passed)
				// out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
				// vec4: function/macro/constructor (of vmath?)

	// give above source code to the fragment shader object
	glShaderSource(
		gFragmentShaderObject,	// shader to which the source code is to be given
		1,		// the number of strings in the array
		(const GLchar **)&fragmentShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);			// replace the source code in a shader object

	// compile the fragment shader
	glCompileShader(gFragmentShaderObject);
	// error checking
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gFragmentShaderObject,	//  the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gFragmentShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
					);						// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
											// When a shader object is created, its information log will be a string of length 0.
				printf("\nFragment Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				UninitializeOGL();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
			}
		}
	}

	// create shader program object 
	gShaderProgramObject = glCreateProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_POSITION,					// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
		// give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_COLOR,					// the index of the generic vertex attribute to be bound.
		"vColor"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	// link the shader program to your program
	glLinkProgram(gShaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
	// error checking
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(				// iv= integer vector
		gShaderProgramObject,	//  the program to be queried.
		GL_LINK_STATUS,			// what(object parameter) is to be queried	
		&iProgramLinkStatus		// empty	//  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
	);							// return a parameter from a program object
	if (iProgramLinkStatus == GL_FALSE)	// error present
	{
		// check if the linker has any info. about the error
		glGetProgramiv(gShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetProgramInfoLog(
					gShaderProgramObject,	// the program object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
											// When a program object is created, its information log will be a string of length 0.
				printf("\nShader Program Link Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				UninitializeOGL();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
			}
		}
	}

	// post-linking retrieving uniform locations (uniforms are global to shaders)
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");	// preparation of data transfer from CPU to GPU (binding)
																				// u_mvp_matrix: of GPU; mvpUniform: of CPU
																				// telling it to take location of uniform u_mvp_matrix and give in mvpUniform

	// variable declarations
	GLfloat half_letter_ht = 0.5f;

	// fill vertices in array (this was in Display() in FFP)
	const GLfloat I_position[] = {
		letter_start + 0.02f, half_letter_ht, 0.0f,
		letter_end - 0.02f, half_letter_ht, 0.0f,
		0.0f, half_letter_ht, 0.0f,
		0.0f, -half_letter_ht, 0.0f,
		letter_start + 0.02f, -half_letter_ht, 0.0f,
		letter_end - 0.02f, -half_letter_ht, 0.0f, };	// last ',' OK!; const since array of values
														// since written in Initialize() only, coords will not get set everytime in Display(). Hence, fast speed.

	const GLfloat I_color[] = { 
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375,	 // green
		0.0703125, 0.53125, 0.02734375,	 // green
		0.0703125, 0.53125, 0.02734375   // green
	};

	const GLfloat N_position[] = {
		letter_start, half_letter_ht + 0.015f, 0.0f,
		letter_start, -half_letter_ht - 0.015f, 0.0f,
		letter_start, half_letter_ht + 0.015f, 0.0f,
		letter_end, -half_letter_ht - 0.015f, 0.0f,
		letter_end, -half_letter_ht - 0.015f, 0.0f,
		letter_end, half_letter_ht + 0.015f, 0.0f
	};

	const GLfloat N_color[] = {
		1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375 ,  // green
		1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375,   // green
		0.0703125, 0.53125, 0.02734375,   // green
		1.0, 0.59765625, 0.19921875,	 // saffron
	};

	const GLfloat D_position[] = {
		letter_start + 0.05f, half_letter_ht, 0.0f,
		letter_start + 0.05f, -half_letter_ht, 0.0f,
		letter_start, half_letter_ht, 0.0f,
		letter_end + 0.0135f, half_letter_ht, 0.0f,
		letter_end, half_letter_ht, 0.0f,
		letter_end, -half_letter_ht, 0.0f,
		letter_end + 0.013f, -half_letter_ht, 0.0f,
		letter_start, -half_letter_ht, 0.0
	};

	const GLfloat A_position[] = {
		letter_start, -half_letter_ht - 0.015f, 0.0f,
		0.0f, half_letter_ht + 0.015f, 0.0f,
		0.0f, half_letter_ht + 0.015f, 0.0f,
		letter_end, -half_letter_ht - 0.015f, 0.0f
	};

	const GLfloat A_color[] = {
		0.0703125, 0.53125, 0.02734375,	 // green
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375	 // green
	};

	const GLfloat A_band_position[] = {
		letter_start / 2.0f, 0.0166f, 0.0f,
		letter_end / 2.0f, 0.0166f, 0.0f,
		letter_start / 2.0f, 0.0f, 0.0f,
		letter_end / 2.0f, 0.0f, 0.0f,
		letter_start / 2.0f, -0.0173f, 0.0f,
		letter_end / 2.0f, -0.0173f, 0.0f
	};

	const GLfloat A_band_color[] = {
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 1.0, 1.0,					// white
		1.0, 1.0, 1.0,					// white
		0.0703125, 0.53125, 0.02734375,	 // green
		0.0703125, 0.53125, 0.02734375,	 // green
	};

	const GLfloat plane_position[] = {
		// middle quad
		0.25f + 0.1f, 0.05f, 0.0f,
		0.0f + 0.1f, 0.05f, 0.0f,
		0.0f + 0.1f, -0.05f, 0.0f,
		0.25f + 0.1f, -0.05f, 0.0f,
		// upper wing
		0.0f + 0.1f, 0.05f, 0.0f,
		0.1f + 0.1f, 0.05f, 0.0f,
		0.05f + 0.1f, 0.15f, 0.0f,
		0.0f + 0.1f, 0.15f, 0.0f,
		// lower wing
		0.1f + 0.1f, -0.05f, 0.0f,
		0.0f + 0.1f, -0.05f, 0.0f,
		0.0f + 0.1f, -0.15f, 0.0f,
		0.05f + 0.1f, -0.15f, 0.0f,
		// back
		0.0f + 0.1f, 0.05f, 0.0f,
		-0.1f + 0.1f, 0.08f, 0.0f,
		-0.1f + 0.1f, -0.08f, 0.0f,
		0.0f + 0.1f, -0.05f, 0.0f,
		// front triangle
		0.25f + 0.1f, 0.05f, 0.0f,
		0.25f + 0.1f, -0.05f, 0.0f,
		0.30f + 0.1f, 0.00f, 0.0f,
		// I
		-0.01f + 0.1f, 0.03f, 0.0f,
		0.03f + 0.1f, 0.03f, 0.0f,
		0.01f + 0.1f, 0.03f, 0.0f,
		0.01f + 0.1f, -0.03f, 0.0f,
		-0.01f + 0.1f, -0.03f, 0.0f,
		0.03f + 0.1f, -0.03f, 0.0f,
		// A
		0.05f + 0.1f, -0.03f, 0.0f,
		0.07f + 0.1f, 0.03f, 0.0f,
		0.07f + 0.1f, 0.03f, 0.0f,
		0.09f + 0.1f, -0.03f, 0.0f,
		(0.05f + 0.07f) / 2.0f + 0.1f, 0.0f, 0.0f,
		(0.07f + 0.09f) / 2.0f + 0.1f, 0.0f, 0.0f,
		// F
		0.12f + 0.1f, -0.03f, 0.0f,
		0.12f + 0.1f, 0.03f, 0.0f,
		0.12f + 0.1f, 0.03f, 0.0f,
		0.16f + 0.1f, 0.03f, 0.0f,
		0.12f + 0.1f, 0.0f, 0.0f,
		0.15f + 0.1f, 0.0f, 0.0f
	};

	// 9 lines
	// I
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_I);		// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_I);		// bind a vertex array object

	// I position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_I);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_I);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

														// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

														// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(I_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		I_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	// same above steps for C,T,N if any

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer
	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// I color
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_color_I);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_I);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(I_color),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		I_color,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vColor

													// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// N
								// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_N);		// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_N);		// bind a vertex array object

	// N position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_N);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_N);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(N_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		N_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	// same above steps for C,T,N if any

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// N color
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_color_N);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_N);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(N_color),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		N_color,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vColor

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// D
								// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_D);		// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_D);		// bind a vertex array object

	// D position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_D);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_D);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(D_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		D_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	// same above steps for C,T,N if any

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// D color
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_color_D);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_D);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

													// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

													// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(8 * 3 * sizeof(GLfloat)),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			NULL,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vColor

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// A
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_A);		// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_A);		// bind a vertex array object

	// A position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_A);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_A);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

														// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

														// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(A_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		A_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

														// same above steps for C,T,N if any

														// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// A color
										// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_color_A);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_A);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(A_color),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		A_color,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vColor

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// A band
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_A_band);		// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_A_band);		// bind a vertex array object

	// A band position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_A_band);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_A_band);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

														// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

														// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(A_band_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		A_band_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

														// same above steps for C,T,N if any

														// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// A band color
										// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_color_A_band);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_A_band);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

													// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

													// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(A_band_color),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		A_band_color,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vColor

													// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// plane
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_plane);	// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_plane);		// bind a vertex array object

	// plane position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_plane);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_plane);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

														// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

														// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(plane_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		plane_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	// same above steps for C,T,N if any

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// plane color
	// ? not working here:
	//glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 186.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// smoke
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_smoke);		// generate vertex array object names
											// 1: Specifies the number of vertex array object names to generate
											// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
											// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_smoke);		// bind a vertex array object

	// smoke position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_smoke);		// generate buffer object names
												// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
												// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
															// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
															// vbo: bind this (the name of a buffer object.)

															// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

															// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		0,				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		NULL,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes(between 2 vertices); 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer								

	// same above steps for C,T,N if any

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

	// usual OGL code
	glClearColor(0.0f,0.0f,0.0f,1.0f);	// clear the screen by OGL color

	perspectiveProjectionMatrix = vmath::mat4::identity();	// making orthographicProjectionMatrix an identity matrix(diagonals 1)

	// warmup call to ResizeOGL()
	ResizeOGL(giWindowWidth,giWindowHeight);
}

void ResizeOGL(int width, int height)
{
	if (height == 0)
	{
		height = 1;
	}

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);// ? other values of near & far not working
																											// parameters:
																											// fovy- The field of view angle, in degrees, in the y - direction.
																											// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																											// zNear- The distance from the viewer to the near clipping plane(always positive).
																											// zFar- The distance from the viewer to the far clipping plane(always positive).*/
}

void DisplayOGL(void)
{
	// function declarations
	void DrawI1();
	void DrawA();
	void DrawN();
	void DrawI2();
	void DrawD();
	void DrawLeftUpperPlane();
	void DrawMiddlePlane();
	void DrawLeftLowerPlane();
	void DrawIAF(void);
	void DrawLeftUpperSmoke(void);
	void DrawSmokeMiddle(void);
	void DrawLeftLowerSmoke(void);
	void DrawABand();
	void DrawRightUpperPlane();
	void DrawRightLowerPlane();
	void DrawRightUpperSmoke();
	void DrawRightLowerSmoke();

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

	// One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	glUseProgram(gShaderProgramObject);	// binding your OpenGL code with shader program object
										// Specifies the handle of the program object whose executables are to be used as part of current rendering state.
	
	glLineWidth(15.0f);

	// I1
	DrawI1();

	// A
	if (bDoneI1 == true)
		DrawA();

	// N
	if (bDoneA == true)
		DrawN();

	// I2
	if (bDoneN == true)
		DrawI2();

	// D
	if (bDoneI2 == true)
		DrawD();

	// Attach planes
	if (bDoneD == true)
	{
		// upper
		if (bDoneUpperPlane == false)
		{
			DrawLeftUpperPlane();
		}
		DrawLeftUpperSmoke();

		// lower
		if (bDoneUpperPlane == false)
		{
			DrawLeftLowerPlane();
		}
		DrawLeftLowerSmoke();

		// middle
		DrawMiddlePlane();
		DrawSmokeMiddle();
	}

	// Detach planes
	if (bDoneMidddlePlane == true)
	{
		// upper
		if (bDoneDetachPlanes == false)
		{
			DrawRightUpperPlane();
		}
		DrawRightUpperSmoke();

		// lower
		if (bDoneDetachPlanes == false)
		{
			DrawRightLowerPlane();
		}
		DrawRightLowerSmoke();

		// middle
		DrawMiddlePlane();
		DrawSmokeMiddle();
	}

	// A's band
	if (bDoneDetachPlanes == true)
	{
		DrawABand();
	}

	glUseProgram(0);	// unbinding your OpenGL code with shader program object
						// If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.

	glXSwapBuffers(gpDisplay,gWindow);	// exchange front and back buffers; gWindow: Specifies the drawable whose buffers are to be swapped
}

void DrawI1(void)
{
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(i1_x, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_I);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	glBindVertexArray(0);
}

void DrawA()
{
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(a_x, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_A);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	glBindVertexArray(0);
}

void DrawN()
{
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(-letter_x, n_y, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_N);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	glBindVertexArray(0);
}

void DrawI2()
{
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(letter_x, i2_y, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_I);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	glBindVertexArray(0);
}

void DrawD()
{
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_D);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

	GLfloat DColor[] = {
		r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f,
		r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f
	};

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_D);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

													// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

													// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(DColor),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		DColor,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

			// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

										// draw the necessary scene!
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		8						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	glBindVertexArray(0);
}

void DrawLeftUpperPlane()
{
	// function declarations
	void DrawPlane(void);

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	translationMatrix = vmath::translate(-2.0f*letter_x + letter_end, 1.22f, -3.0f) * vmath::translate(r*(GLfloat)cos(angle), r*(GLfloat)sin(angle), 0.0f);
	rotationMatrix = vmath::rotate(angle_upper_plane, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

																								// draw the necessary scene!
	DrawPlane();

	// unbind vao
	glBindVertexArray(0);
}

void DrawMiddlePlane()
{
	// function declarations
	void DrawPlane(void);

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(middle_plane_x, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

																								// draw the necessary scene!
	DrawPlane();
	// unbind vao
	glBindVertexArray(0);
}

void DrawLeftLowerPlane()
{
	// function declarations
	void DrawPlane(void);

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	translationMatrix = vmath::translate(-2.0f*letter_x + letter_end, -1.22f, -3.0f) * vmath::translate(r*(GLfloat)cos(angle_lower), r*(GLfloat)sin(angle_lower), 0.0f);
	rotationMatrix = vmath::rotate(angle_lower_plane, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading
																					// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glBindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

																								// draw the necessary scene!
	DrawPlane();

	// unbind vao
	glBindVertexArray(0);
}

void DrawLeftUpperSmoke(void)
{
	// variable declarations
	static GLfloat angle_smoke;

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(-2.0f*letter_x + letter_end, 1.22f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	glPointSize(5.0f);

	// saffron
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_saffron[3] = { (r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_saffron),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_white[3] = { r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_white),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_green[3] = { (r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_green),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)


		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);
}

void DrawSmokeMiddle(void)
{
	// function declarations
	void DrawI1();
	void DrawN();
	void DrawD();
	void DrawI2();
	void DrawA();

	// code
	glLineWidth(5.0f);
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glPointSize(5.0f);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);

	// saffron
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	GLfloat line_saffron[6] = { -letter_x * 5.4f, 0.0166f, 0.0f,
								 middle_plane_x, 0.0166f, 0.0f };

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(line_saffron),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		line_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)


	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your array to start with (imp in inter-leaved)
		2						// how many vertices to draw
	);

	// white
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	GLfloat line_white[6] = { -letter_x * 5.4f, 0.0f, 0.0f,
								middle_plane_x, 0.0f, 0.0f };

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(line_white),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		line_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)


	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your array to start with (imp in inter-leaved)
		2						// how many vertices to draw
	);

	// green
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	GLfloat line_green[6] = { -letter_x * 5.4f, -0.0173f, 0.0f,
								middle_plane_x, -0.0173f, 0.0f };

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(line_green),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		line_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)


	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your array to start with (imp in inter-leaved)
		2						// how many vertices to draw
	);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);

	if (bDoneDetachPlanes == true)
	{
		glLineWidth(15.0f);

		DrawI1();
		DrawN();
		DrawD();
		DrawI2();
		DrawA();
	}
}

void DrawLeftLowerSmoke()
{
	// variable declarations
	static GLfloat angle_smoke;

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(-2.0f*letter_x + letter_end, -1.22f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glPointSize(5.0f);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);

	// saffron
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_saffron[3] = { (r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_saffron),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_white[3] = { r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_white),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_green[3] = { (r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_green),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)


		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);
}

void DrawABand()
{
	glLineWidth(5.0f);

	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(letter_x*2.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_A_band);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);

	// unbind vao
	glBindVertexArray(0);
}

void DrawRightUpperPlane()
{
	// function declarations
	void DrawPlane(void);

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	translationMatrix = vmath::translate(2.0f*letter_x - letter_end, 1.22f, -3.0f) * vmath::translate(r*(GLfloat)cos(angle_right_upper), r*(GLfloat)sin(angle_right_upper), 0.0f);
	rotationMatrix = vmath::rotate(angle_upper_plane, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

	// draw the necessary scene!
	DrawPlane();

	// unbind vao
	glBindVertexArray(0);
}

void DrawRightLowerPlane()
{
	// function declarations
	void DrawPlane(void);

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	translationMatrix = vmath::translate(2.0f*letter_x - letter_end, -1.22f, -3.0f) * vmath::translate(r*(GLfloat)cos(angle_right_lower), r*(GLfloat)sin(angle_right_lower), 0.0f);
	rotationMatrix = vmath::rotate(angle_lower_plane, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading
																					// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glBindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

										// draw the necessary scene!
	DrawPlane();

	// unbind vao
	glBindVertexArray(0);
}

void DrawRightUpperSmoke()
{
	// variable declarations
	static GLfloat angle_smoke;

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(2.0f*letter_x - letter_end, 1.22f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	glPointSize(5.0f);

	// saffron
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_saffron[3] = { (r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_saffron),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_white[3] = { r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_white),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_green[3] = { (r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_green),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)


		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);
}

void DrawRightLowerSmoke()
{
	// variable declarations
	static GLfloat angle_smoke;

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(2.0f*letter_x - letter_end, -1.22f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glPointSize(5.0f);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);

	// saffron
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_saffron[3] = { (r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_saffron),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_white[3] = { r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_white),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_green[3] = { (r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_green),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)


		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);
}

void DrawPlane(void)
{
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 186.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

	// middle quad
	glDrawArrays(GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// upper wing
	glDrawArrays(GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		4,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// lower wing
	glDrawArrays(GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		8,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// back
	glDrawArrays(GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		12,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// front triangle
	glDrawArrays(GL_TRIANGLES,	// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		16,						// array position of your array to start with (imp in inter-leaved)
		3						// how many vertices to draw
	);
	// IAF
	glLineWidth(3.0f);
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 99.0f / 255.0f, 99.0f / 255.0f, 99.0f / 255.0f);	// dark gray // specify the value of a generic vertex attribute(for single color to object)

																							// I
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		19,						// array position of your array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);
	// A
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		25,						// array position of your array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);
	// F
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		31,						// array position of your array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);
}

void UpdateOGL(void)
{
	if (bDoneI1 == false)
	{
		i1_x = i1_x + 0.0027f;

		if (i1_x >= -letter_x * 2)
		{
			i1_x = -letter_x * 2.0f;
			bDoneI1 = true;
		}
	}
	else if (bDoneA == false)
	{
		a_x = a_x - 0.0027f;

		if (a_x <= letter_x * 2)
		{
			a_x = letter_x * 2.0f;
			bDoneA = true;
		}
	}
	else if (bDoneN == false)
	{
		n_y = n_y - 0.0027f;

		if (n_y <= 0.0f)
		{
			bDoneN = true;
			n_y = 0.0f;
		}
	}
	else if (bDoneI2 == false)
	{
		i2_y = i2_y + 0.0027f;

		if (i2_y >= 0.0f)
		{
			i2_y = 0.0f;
			bDoneI2 = true;
		}
	}
	else if (bDoneD == false)
	{
		if (bDoneR_saffron == false)
		{
			r_saffron = r_saffron + 2.5f;
			if (r_saffron >= 255.0f)
			{
				r_saffron = 255.0f;
				bDoneR_saffron = true;
			}
		}
		if (bDoneG_saffron == false)
		{
			g_saffron = g_saffron + 1.0f;
			if (g_saffron >= 153.0f)
			{
				g_saffron = 153.0f;
				bDoneG_saffron = true;
			}
		}
		if (bDoneB_saffron == false)
		{
			b_saffron = b_saffron + 0.2f;
			if (b_saffron >= 51.0f)
			{
				b_saffron = 51.0f;
				bDoneB_saffron = true;
			}
		}

		// green
		if (bDoneR_green == false)
		{
			r_green = r_green + 0.1f;
			if (r_green >= 18.0f)
			{
				r_green = 18.0f;
				bDoneR_green = true;
			}
		}
		if (bDoneG_green == false)
		{
			g_green = g_green + 0.7f;
			if (g_green >= 136.0f)
			{
				g_green = 136.0f;
				bDoneG_green = true;
			}
		}
		if (bDoneB_green == false)
		{
			b_green = b_green + 0.1f;
			if (b_green >= 7.0f)
			{
				b_green = 7.0f;
				bDoneB_green = true;
			}
		}
		if (bDoneR_saffron == true && bDoneG_saffron == true && bDoneB_saffron == true && bDoneR_green == true && bDoneG_green == true && bDoneB_green == true)
			bDoneD = true;
	}
	else if (bDoneUpperPlane == false)
	{
		middle_plane_x = middle_plane_x + 0.00049f;


		if (angle_upper_plane <= 0.0f)
			angle_upper_plane = angle_upper_plane + 0.045f;

		if (angle_lower_plane >= 0.0f)
			angle_lower_plane = angle_lower_plane - 0.045f;

		if (r*(GLfloat)sin(angle) <= -1.22f)
		{
			bDoneUpperPlane = true;
			//fprintf_s(gpFile, "\nr*cos(angle)=%f", r*cos(angle));
			//fprintf_s(gpFile, "\nr*sin(angle)=%f", r*sin(angle));
			angle_upper_plane = 0.0f;
			angle_lower_plane = 0.0f;
		}

		angle = angle + 0.0005f;
		angle_lower = angle_lower - 0.0005f;
	}

	else if (bDoneMidddlePlane == false)
	{
		middle_plane_x = middle_plane_x + 0.00049f;
		if (middle_plane_x >= 2.0f*letter_x - letter_end+0.724918f)
			bDoneMidddlePlane = true;
	}

	else if (bDoneDetachPlanes == false)
	{
		middle_plane_x = middle_plane_x + 0.00049f;

		angle_right_upper = angle_right_upper + 0.0005f;
		angle_right_lower = angle_right_lower - 0.0005f;
		
		if (angle_upper_plane <= 90.0f)
				angle_upper_plane = angle_upper_plane + 0.047f;
		
		if (angle_lower_plane >= -90.0f)
				angle_lower_plane = angle_lower_plane - 0.047f;
		
		if (middle_plane_x >= letter_x * 6.0f)
			bDoneDetachPlanes = true;
	}
	else if(bDoneFadingSmokes==false)
	{
		if (r_saffron_smoke > 0.0f)
			r_saffron_smoke = r_saffron_smoke - 2.5f;
		if (g_saffron_smoke > 0.0f)
			g_saffron_smoke = g_saffron_smoke - 1.0f;
		if (b_saffron_smoke > 0.0f)
			b_saffron_smoke = b_saffron_smoke - 0.2f;

		if (r_white_smoke > 0.0f)
			r_white_smoke = r_white_smoke - 0.03f;

		if (r_green_smoke > 0.0f)
			r_green_smoke = r_green_smoke - 0.1f;
		if (g_green_smoke > 0.0f)
			g_green_smoke = g_green_smoke - 0.7f;
		if (b_green_smoke > 0.0f)
			b_green_smoke = b_green_smoke - 0.1f;

		if (r_saffron_smoke <= 0 && g_saffron_smoke <= 0 && b_saffron_smoke <= 0 && r_white_smoke && r_green_smoke <= 0 && g_green_smoke <= 0 && b_green_smoke <= 0)
			bDoneFadingSmokes = true;
	}
}

void UninitializeOGL(void)
{
	
	// opposite sequence of vao and vbo also ok
	if (vbo_position_plane)
	{
		glDeleteBuffers(1, &vbo_position_plane);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_position_plane = 0;
	}

	if (vao_plane)
	{
		glDeleteVertexArrays(1, &vao_plane);		// delete vertex array objects
		vao_plane = 0;
	}

	if (vbo_position_smoke)
	{
		glDeleteBuffers(1, &vbo_position_smoke);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_position_smoke = 0;
	}

	if (vao_smoke)
	{
		glDeleteVertexArrays(1, &vao_smoke);		// delete vertex array objects
		vao_smoke = 0;
	}

	if (vbo_position_I)
	{
		glDeleteBuffers(1, &vbo_position_I);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_position_I = 0;
	}

	if (vbo_color_I)
	{
		glDeleteBuffers(1, &vbo_color_I);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_color_I = 0;
	}

	if (vao_I)
	{
		glDeleteVertexArrays(1, &vao_I);		// delete vertex array objects
		vao_I = 0;
	}

	if (vbo_position_N)
	{
		glDeleteBuffers(1, &vbo_position_N);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_position_N = 0;
	}

	if (vbo_color_N)
	{
		glDeleteBuffers(1, &vbo_color_N);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_color_N = 0;
	}

	if (vao_N)
	{
		glDeleteVertexArrays(1, &vao_N);		// delete vertex array objects
		vao_N = 0;
	}

	if (vbo_position_D)
	{
		glDeleteBuffers(1, &vbo_position_D);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_position_D = 0;
	}

	if (vbo_color_D)
	{
		glDeleteBuffers(1, &vbo_color_D);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_color_D = 0;
	}

	if (vao_D)
	{
		glDeleteVertexArrays(1, &vao_D);		// delete vertex array objects
		vao_D = 0;
	}

	if (vbo_position_A)
	{
		glDeleteBuffers(1, &vbo_position_A);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_position_A = 0;
	}

	if (vbo_color_A)
	{
		glDeleteBuffers(1, &vbo_color_A);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_color_A = 0;
	}

	if (vao_A)
	{
		glDeleteVertexArrays(1, &vao_A);		// delete vertex array objects
		vao_A = 0;
	}

	if (vbo_position_A_band)
	{
		glDeleteBuffers(1, &vbo_position_A_band);	// delete named buffer objects
													// 1: number of buffer objects to be deleted
													// &vbo: array of buffer objects to be deleted
		vbo_position_A_band = 0;
	}

	if (vbo_color_A_band)
	{
		glDeleteBuffers(1, &vbo_color_A_band);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_color_A_band = 0;
	}

	if (vao_A_band)
	{
		glDeleteVertexArrays(1, &vao_A_band);		// delete vertex array objects
		vao_A_band = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;	// typedef int
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);		// since unused in Display()
		
		// ask program that how many shaders are attached to it
		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
			&shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);		// dynamic array for shaders since we don't know how many present
		if (pShaders)	// mem allocated
		{
			// take attached shaders  into above array
			glGetAttachedShaders(gShaderProgramObject,		// the program object to be queried
				shaderCount,								// the size of the array for storing the returned object names
				&shaderCount,								// Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
				pShaders									// an array that is used to return the names of attached shader objects(empty now)
			);		// return the handles of the shader objects attached to a program object

			for (shaderNumber = 0;shaderNumber < shaderCount;shaderNumber++)
			{
				// detach each shader
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
																					// 2nd para: Specifies the shader object to be detached.

				// delete each detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		// delete the shader program
		glDeleteProgram(gShaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	GLXContext currentGlxContext=glXGetCurrentContext();	// return the current context; can be diff from gGlxContext in multi-monitor mode

	if(currentGlxContext!=NULL && currentGlxContext==gGlxContext)
	{
		// release the current context
		glXMakeCurrent(gpDisplay,0,0);	// ~wglMakeCurrent(NULL,NULL)
	}

	// reclaim
	if(gGlxContext)
	{
		glXDestroyContext(gpDisplay,gGlxContext);	// If the GLX rendering context ctx is not current to any thread, glXDestroyContext() destroys it immediately.
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);	// The XDestroyWindow() function destroys the specified window as well as all of its subwindows and causes the X server to generate a DestroyNotify event for each window.
											// ~ DestroyWindow() (sends WM_DESTROY msg)
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		// Close the connection to the X server for the display specified in the Display structure and destroys all windows, resource IDs (Window, Font, Pixmap, Colormap, Cursor, and GContext), or other resources that the client has created on this display.
		// Therefore, these windows, resource IDs, and other resources should never be referenced again or an error will be generated.
		// Before exiting, you should call XCloseDisplay() explicitly so that any pending errors are reported as XCloseDisplay() performs a final XSync() operation.
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};	// event; XEvent: union

	// code
	// take the current state
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);	// _NET is for network protocol

	memset(&xev,0,sizeof(xev));	// &xev: Pointer to the object to copy the 0
	xev.type=ClientMessage;	// type of event/msg (ClientMessage: client is going to send it; in Xlib.h)
							// X server generates ClientMessage events only when a client calls XSendEvent(). 
	xev.xclient.window=gWindow;	// the window related to the event; xclient: XClientMessageEvent structure;
	xev.xclient.message_type=wm_state;	// type of message; indicates how the data should be interpreted by the receiving client
	xev.xclient.format=32;	// format (no. of bits) of msg
	//xev.xclient.data.l[0]=bFullscreen?0:1;	// The data member is a union that contains the members b(byte), s(short), and l(long). The b, s, and l members represent data of 20 8-bit values, 10 16-bit values, and 5 32-bit values.
											// If (bFullscreen) then l[0]=0 else l[0]=1
	xev.xclient.data.l[0]=1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;	// assign 1st member of l to new state(?)

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay,gpXVisualInfo->screen),	// the window the event is to be sent to
		False,										// is the msg propagatable to sub-windows/other clients? (False since only 1 window is going to be fullscreen)
		StructureNotifyMask,						// event mask; fullscreen means changing size of the window and the event mask for resizing is StructureNotifyMask
		&xev 										// address of the event that is to be sent
		);
}
