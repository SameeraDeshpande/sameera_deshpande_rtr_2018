// standard header files
#include<iostream>	// cin,cout; It is not a header, it's a namespace cache. Out of that we are using namespace 'std'
#include<stdio.h>	// printf()
#include<stdlib.h>	// exit()
#include<memory.h>	// memset()
// X11 header files
#include<X11/Xlib.h>	// similar to windows.h; for all XServer APIs
#include<X11/Xutil.h>	// XVisualInfo
#include<X11/XKBlib.h>	// keyboard utilization header
#include<X11/keysym.h>	// defines certain preprocessor symbols; mapping between keycode and symbol (eg. XK_F to ASCII 70)

// namespaces
using namespace std;	// to avoid using 'std::' everywhere

// global variable declarations
bool bFullscreen = false;	// cpp feature
Display *gpDisplay = NULL;	// g= global, p= pointer
// XVisualInfo *gpXVisualInfo = NULL;	// ~pfd (h/w representation of graphics card)
XVisualInfo gXVisualInfo;
Colormap gColormap;	// struct; The colormap is a small table with entries specifying the RGB values of the currently available colors. The hardware imposes limits on the number of significant bits in these values.
				    // In X, the colormap entries are called color cells. The pixel values are interpreted as indices to the color cells. 
					// Thus, to refresh the screen, the display hardware uses the RGB values in the color cells indexed by the pixel values. 
					// The size of the colormap is less than or equal to 2^N color cells, where N is the number of bits in a pixel value. A device with 8 bits per pixel value, for example, will have a colormap with up to 256 color cells, indexed though pixel values ranging from 0 through 255. 

Window gWindow;	// struct; ~wndclass
int giWindowWidth=800;	// not macro or const since it has to be changed
int giWindowHeight=600;

// entry-point function
int main(void)	// no WinMain() here
{
	// function prototypes
	void CreateWindow(void);	// UDF
	void ToggleFullscreen(void);
	void uninitialize();

	// variable declarations
	int winWidth=giWindowWidth;	// local
	int winHeight=giWindowHeight;

	// code
	CreateWindow();	 // gpDisplay, gWindow, gColormap, gpXVisualInfo are defined in this function (values are assigned)

	// message loop
	XEvent event;	//  In the X protocol, X clients send requests to the X server. The server sends replies, errors, and events to its clients. 
	KeySym keysym;

	while(1)	// infinite loop
	{
		XNextEvent(gpDisplay, &event);	// ~GetMessage(); 
										// The XNextEvent() function copies the first event from the event queue into the specified XEvent structure and then removes it from the queue. If the event queue is empty, XNextEvent() flushes the output buffer and blocks until an event is received. 
										// gpDisplay : Specifies the connection to the X server, &event ~ &msg
		switch(event.type)	// ~WM_ messages
		{
			case MapNotify:	// ~WM_CREATE (occurs only once) but creates as well as shows window
							// XServer maps its default window given to the client with the window with the client=provided attributes
							// The X server can report MapNotify events to clients wanting information about which windows are mapped.
			break;

			case KeyPress:	// ~WM_KEYDOWN
			keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);	// convert keycode to key symbol
																			// event.xkey.keycode: keycode; 
																			// 0: page no. of UNICODE (0=ANSI i.e. English) (default)
																			// 0: is shift key pressed? (no)
			switch(keysym)	// ~SWITCH(wParam)
			{
				case XK_Escape:	// X= X's keysym
				uninitialize();
				exit(0);	// ~return((int)msg.wParam)

				case XK_F:
				case XK_f:
				if(bFullscreen==false)
				{
					ToggleFullscreen();
					bFullscreen=true;
				}
				else
				{
					ToggleFullscreen();
					bFullscreen=false;
				}
				break;

				default:
				break;
			}
			break;

			case ButtonPress:	// mouse click
			switch(event.xbutton.button)
			{
				case 1:	// ~WM_LBUTTONDOWN
								printf("Left mouse button is pressed\n");
				break;
				case 2:	// middle button
								printf("Middle mouse button is pressed\n");

				break;
				case 3:	// ~WM_RBUTTONDOWN
								printf("Right mouse button is pressed\n");

				break;
				case 4:	// mouse wheel up
								printf("Mouse wheel is pressed\n");

				break;
				case 5:	// mouse wheel down
								printf("Mouse wheel is released\n");

				break;
				default:
				break;
			}
			break;

			case MotionNotify:	// ~WM_MOUSEMOVE
			break;

			case ConfigureNotify:	// ~WM_SIZE
			winWidth=event.xconfigure.width;	// lParam gives size of window in Windows
			winHeight=event.xconfigure.height;
			break;

			case Expose:	// ~WM_PAINT	// nothing written since we want def black color
			break;

			case DestroyNotify:	// ~WM_CLOSE (option of whether to exit or not is given to user)
			break;

			case 33:	// const; When close button is clicked or close is chosen from menu, 33 is sent to window manager
			uninitialize();
			exit(0);
			break;	// discipline; no use here

			default:
			break;
			
		}
	}

	uninitialize();		// if no case holds true for exiting and if aborted
	return(0);
}

void CreateWindow(void)	// UDF
{
	// function prototypes
	void uninitialize(void);

	// variable declarations
	XSetWindowAttributes winAttribs;	// attributes capable of being set
	int defaultScreen;
	int defaultDepth;
	int styleMask;

	// code
	// open the XServer connection and get Display
	gpDisplay=XOpenDisplay(NULL);	// NULL: get def Display
	if(gpDisplay==NULL)	// error checking; to check if mem is assigned or not
	{
		printf("ERROR : Unable To Open X Display.\nExiting Now\n");
		uninitialize();
		exit(0);
	}

	defaultScreen=XDefaultScreen(gpDisplay);	// get default screen; ~MONITORINFOF_PRIMARY	// no need of error checking

	defaultDepth=DefaultDepth(gpDisplay,defaultScreen);	// get def bit depth; Actually XDefaultDepth, DefaultDepth is macro

	// gpXVisualInfo=(XVisualInfo*)malloc(sizeof(XVisualInfo));	// get VisualInfo
	// if(gpXVisualInfo==NULL)	// error checking; to check if mem is assigned or not
	// {
	// 	printf("ERROR : Unable To Allocate Memory For Visual Info.\nExiting Now\n");
	// 	uninitialize();
	// 	exit(0);
	// }

	// XMatchVisualInfo(gpDisplay,defaultScreen,defaultDepth,TrueColor,gpXVisualInfo);	// get closest matching VisualInfo; TrueColor= 32 bit per pixel; gpXVisualInfo: empty now
	// if(gpXVisualInfo==NULL)	// error checking; to check if mem is assigned or not
	// {
	// 	printf("ERROR : Unable To Get A Visual.\nExiting Now\n");
	// 	uninitialize();
	// 	exit(0);
	// }

	Status status=XMatchVisualInfo(gpDisplay,
		defaultScreen,
		defaultDepth,
		TrueColor,
		&gXVisualInfo);

	// ~wndclass
	winAttribs.border_pixel=0;	// border color; pixel=color; 	0= don't care
	winAttribs.border_pixmap=0;	// border image; 0= don't create; pixmap= bitmap of Windows
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);	// background color
	winAttribs.background_pixmap=0;	 // backfround image

	// winAttribs.colormap=XCreateColormap(gpDisplay,
	// 	RootWindow(gpDisplay,gpXVisualInfo->screen),	// give matching screen to h/w
	// 	gpXVisualInfo->visual,
	// 	AllocNone);	// do not allocate mem for colormap
	// 	//~cbClsExtra and cbWndExtra;

	winAttribs.colormap=XCreateColormap(gpDisplay,
		RootWindow(gpDisplay,gXVisualInfo.screen),	// give matching screen to h/w
		gXVisualInfo.visual,
		AllocNone);	// do not allocate mem for colormap
		//~cbClsExtra and cbWndExtra;

	gColormap=winAttribs.colormap;	// saving the colormap

	winAttribs.event_mask=ExposureMask|VisibilityChangeMask|ButtonPressMask|KeyPressMask|PointerMotionMask|StructureNotifyMask;

	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;

	// gWindow=XCreateWindow(gpDisplay,
	// 		RootWindow(gpDisplay,gpXVisualInfo->screen),
	// 		0,
	// 		0,
	// 		giWindowWidth,
	// 		giWindowHeight,
	// 		0,
	// 		gpXVisualInfo->depth,
	// 		InputOutput,
	// 		gpXVisualInfo->visual,
	// 		styleMask,
	// 		&winAttribs);

gWindow=XCreateWindow(gpDisplay,
			RootWindow(gpDisplay,gXVisualInfo.screen),
			0,
			0,
			giWindowWidth,
			giWindowHeight,
			0,
			gXVisualInfo.depth,
			InputOutput,
			gXVisualInfo.visual,
			styleMask,
			&winAttribs);

	if(!gWindow)
	{
		printf("ERROR : Failed To Create Main Window.\nExiting Now\n");
		uninitialize();
		exit(0);
	}

	XStoreName(gpDisplay,gWindow,"First Window");

	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW", True);
	XSetWMProtocols(gpDisplay,gWindow,&windowManagerDelete,1);

	XMapWindow(gpDisplay,gWindow);
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};

	// code
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);
	memset(&xev,0,sizeof(xev));

	xev.type=ClientMessage;
	xev.xclient.window=gWindow;
	xev.xclient.message_type=wm_state;
	xev.xclient.format=32;
	xev.xclient.data.l[0]=bFullscreen?0:1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;

	// XSendEvent(gpDisplay,
	// 	RootWindow(gpDisplay,gpXVisualInfo->screen),
	// 	False,
	// 	StructureNotifyMask,
	// 	&xev);

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay,gXVisualInfo.screen),
		False,
		StructureNotifyMask,
		&xev);
}

void uninitialize(void)
{

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gWindow);
	}

	// if(gpXVisualInfo)
	// {
	// 	free(gpXVisualInfo);
	// 	gpXVisualInfo=NULL;
	// }

	// if(&gXVisualInfo)
	// {
	// 	free(&gXVisualInfo);
	// 	//&gXVisualInfo=NULL;
	// }

	if(gpDisplay)
	{
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}
