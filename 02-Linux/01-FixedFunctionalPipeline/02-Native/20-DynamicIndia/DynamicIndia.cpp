// standard header files
#include<iostream>	// namespace
#include<stdio.h>	// printf()
#include<stdlib.h>	// exit()
#include<memory.h>	// memset()
#define _USE_MATH_DEFINES 1
#include<math.h>
//X11 header files
#include<X11/Xlib.h>	// similar to windows.h; for all XServer APIs
#include<X11/Xutil.h> 	// XVisualInfo
#include<X11/XKBlib.h>	// keyboard utilization header
#include<X11/keysym.h>	// KeySym;	defines certain preprocessor symbols; mapping between keycode and symbol (eg. XK_F to ASCII 70)
// OpenGL related header files
#include<GL/gl.h>	// has OpenGL related functions and data types
#include<GL/glx.h>	// glx: OpenGL for XWindows (bridging APIs)	//~WGL
#include<GL/glu.h>

// namespaces
using namespace std;	// to avoid using 'std::' everywhere

// global variable declarations
//bool bFullscreen=false;		// cpp feature (false=0)
Display *gpDisplay=NULL;
XVisualInfo* gpXVisualInfo=NULL;	// ~pfd (s/w representation of graphics card)
Colormap gColormap;	// struct; The colormap is a small table with entries specifying the RGB values of the currently available colors. The hardware imposes limits on the number of significant bits in these values.
				    // In X, the colormap entries are called color cells. The pixel values are interpreted as indices to the color cells. 
					// Thus, to refresh the screen, the display hardware uses the RGB values in the color cells indexed by the pixel values. 
					// The size of the colormap is less than or equal to 2^N color cells, where N is the number of bits in a pixel value. A device with 8 bits per pixel value, for example, will have a colormap with up to 256 color cells, indexed though pixel values ranging from 0 through 255. 
Window gWindow;	// struct; ~wndclass
int giWindowWidth=800;
int giWindowHeight=600;
static GLXContext gGlxContext;		// ~ghrc; static is optional. Do global static if you don't want it to be accessible outside this file
GLfloat letter_start = -0.19f, letter_end = 0.19f;
GLfloat half_letter_ht = 0.5f;

// entry-point function
int main(void)	// no WinMain() here
{
	// function declarations
	void CreateWindow(void);
	void InitializeOGL(void);
	void ResizeOGL(int,int);
	void DisplayOGL(void);
	void UninitializeOGL(void);
	void ToggleFullscreen(void);

	// variable declarations
	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;
	char keys[26];
	bool bDone=false;

	// code
	// create window
	CreateWindow();	// gpDisplay, gWindow, gColormap, gXVisualInfo are defined in this function

	// call to Initialize()
	InitializeOGL();

	// game loop
	XEvent event;	//  In the X protocol, X clients send requests to the X server. The server sends replies, errors, and events to its clients. 
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))		// returns the number of events that have been received from the X server but have not been removed from the event queue (asynchronous)	// ~PeekMessage()
		{
			XNextEvent(gpDisplay,&event);	// ~GetMessage(); 
											// The XNextEvent() function copies the first event from the event queue into the specified XEvent structure and then removes it from the queue. 
											// If the event queue is empty, XNextEvent() flushes the output buffer and blocks until an event is received
											// gpDisplay : Specifies the connection to the X server
											// &event ~ &msg
			switch(event.type)
			{
				case MapNotify:	// ~WM_CREATE (occurs only once) but creates as well as shows window
								// XServer maps its default window given to the client with the window with the client provided attributes
				break;

				case KeyPress:	// ~WM_KEYDOWN
				keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);	// convert keycode to key symbol
																				// event.xkey.keycode: keycode; 
																				// 0: page no. of UNICODE (0=ANSI i.e. English) (default)
																				// 0: is shift key pressed? (no)
				switch(keysym)	// ~SWITCH(wParam)
				{
					case XK_Escape:
					bDone=true;

					default:
					break;
				}

				// XLookupString(&event.xkey,	// XKeyEvent*; string is in xkey
				// 	keys,					// char*; the translated string comes into this buffer
				// 	sizeof(keys),			// size of buffer
				// 	NULL,					// KeySym*; returns the KeySym computed from the event if this argument is not NULL. 
				// 	NULL 					// XComposeStatus*;	saves old state
				// 	);	// translates a key event to a string and a KeySym
				
				// switch(keys[0])
				// {
				// 	case 'F':
				// 	case 'f':
				// 	if(bFullscreen==false)
				// 	{
				// 		ToggleFullscreen();
				// 		bFullscreen=true;
				// 	}
				// 	else
				// 	{
				// 		ToggleFullscreen();
				// 		bFullscreen=false;
				// 	}
				// 	break;
				// }
				break;	// case keypress

				case ButtonPress:	// mouse click
				switch(event.xbutton.button)	// xbutton: XButtonEvent; button: int(detail)
				{
					case 1: // ~WM_LBUTTONDOWN
						printf("Left mouse button is pressed\n");
					break;

					case 2:	// middle button
						printf("Middle mouse button is pressed\n");
					break;

					case 3:	// ~WM_RBUTTONDOWN
						printf("Right mouse button is pressed\n");
					break;

					case 4:	// mouse wheel up
						printf("Mouse wheel is released\n");
					break;

					case 5:	// mouse wheel down
						printf("Mouse wheel is pressed\n");
					break;

					default:
					break;
				}
				break;

				case MotionNotify:	// ~WM_MOUSEMOVE
				break;

				case ConfigureNotify:	// ~WM_SIZE (lParam gives size of window in Windows)
				winWidth=event.xconfigure.width;	// xconfigure: XConfigureEvent
				winHeight=event.xconfigure.height;	// The width and height members are set to the inside size of the window, not including the border
				ResizeOGL(winWidth,winHeight);
				break;

				case Expose:	// ~WM_PAINT	// nothing written since we want def black color
				break;

				case DestroyNotify:	// ~WM_CLOSE (option of whether to exit or not is given to user)
				break;

				case 33:	// const; When close button is clicked or close is chosen from menu, 33 is sent to window manager
							// this is just a provision for exit
				bDone=true;
				break;	// discipline; no use here


				default:
				break;
			}	// switch(event.type)
		}	// while(XPending())

		// UpdateOGL();
		DisplayOGL();
		
	}	// while(bDone==false)

	UninitializeOGL();	
	return(0);
}

void CreateWindow(void)
{
	// function declarations
	void UninitializeOGL(void);
	void ToggleFullscreen(void);
	
	// variable declarations
	XSetWindowAttributes winAttribs;	// attributes capable of being set
	int defaultScreen;
	//int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[]={	// static not necessary in our OpenGL practices since we do not call this function again so no need to retain its value (but can be needed in case of multiple displays)
		GLX_RGBA,			// my palette is RGBA; frame buffer contents; If present, only TrueColor and DirectColor visuals are considered. Otherwise, only PseudoColor and StaticColor visuals are considered.                
		GLX_DOUBLEBUFFER,	// If present, only double-buffered visuals are considered
		GLX_RED_SIZE,8,		// Must be followed by a nonnegative minimum size specification
							// allocate 8 bit for red(since single buffer) (hence, 32-bit frame buffer); GLX_RED_SIZE is an enumerated type attribute
							// key-value pairs; Integer attributes and enumerated type attributes are followed immediately by the corresponding desired or minimum value
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		None 				// the last attribute must be None.
							// in case of a const array, when you don't wish to assign values to all its members but you need to pass that array to a function, do end with 0/None (#define None 0)
							// None means telling it to give values to other attributes
	};
	Screen *screen;

	// code
	// open the XServer connection and get Display
	gpDisplay=XOpenDisplay(NULL);	// To open a connection to the X server that controls a display;	NULL: get default Display (#define NULL 0)
									// returns a Display structure that serves as the connection to the X server and that contains all the information about that X server (representative of client)
									// will be sent as first parameter of further functions for the session manager to authencicate the client
	if(gpDisplay==NULL)	// error checking; to check if mem is assigned or not
	{
		printf("ERROR: Unable To Open X Display.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	 // get default screen using gpDisplay obtained in previous step
	defaultScreen=XDefaultScreen(gpDisplay);	// ~MONITORINFOF_PRIMARY	// no need of error checking
												// return the default screen number referenced by the XOpenDisplay() function.
												// use defaultScreen just for obtaining a screen of matching VisualInfo

	// // get default bit depth using gpDisplay and defaultScreen obtained in prev two steps
	// defaultDepth=DefaultDepth(gpDisplay,defaultScreen);	 // Actually XDefaultDepth, DefaultDepth is macro
	// 													 // return the depth (number of planes) of the default root window for the specified screen

	// get closest matching VisualInfo to frameBufferAttributes
	gpXVisualInfo=glXChooseVisual(gpDisplay,
		defaultScreen,			// Specifies the screen number (int)
		frameBufferAttributes	// Specifies a list of boolean attributes and integer attribute/value pairs
		);	// In OpenGL, bridging API itself gives you gpXVisualInfo

	if(gpXVisualInfo==NULL)	// error checking; to check if mem is assigned or not
	{
		printf("ERROR : Unable To Allocate Memory For Visual Info.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	// fill window attributes;	~wndclass
	winAttribs.border_pixel=0;	// border color; pixel=color; 	0= don't care
	winAttribs.border_pixmap=0;	// border image; 0= don't create; pixmap~ bitmap of Windows
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);	// background color 	// ~hBrush
																		// returns the black pixel value for the specified screen.
																		// macro of XBlackPixel() 
	winAttribs.background_pixmap=0;		// background image

	winAttribs.colormap=XCreateColormap(
		gpDisplay,		// the connection to the X server
		RootWindow(gpDisplay,gpXVisualInfo->screen),	// the window on whose screen you want to create a colormap; give matching screen to h/w 
		gpXVisualInfo->visual,						// a visual type supported on the screen; h/w's representation
		AllocNone									// Specifies the colormap entries to be allocated; AllocNone: do not allocate mem for colormap(the colormap initially has no allocated entries, and clients can allocate them)
		);	//~cbClsExtra and cbWndExtra

	gColormap=winAttribs.colormap;	// saving the colormap(since we have done AllocNone)	// why (not used further)?

	// specify the events(messages) that you want the X server to return to a client application. 
	winAttribs.event_mask = VisibilityChangeMask|KeyPressMask|ButtonPressMask|PointerMotionMask|StructureNotifyMask|ExposureMask; // VisibilityChangeMask: for case MapNotify
																																 // KeyPressMask: for case KeyPress
																																 // ButtonPressMask: for case ButtonPress (mouse button down events)
																																 // PointerMotionMask: for case MotionNotify
																																 // StructureNotifyMask: for case ConfigureNotify (resize)
																																 // ExposureMask: for case Expose
																																 // no mask for case DestroyNotify since it is always there; also there exists no mask for case 33
	// fill window styles
	// like for creating event mask you refer events, for creating style mask you refer attribs
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;	

	// create actual window
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay,gpXVisualInfo->screen),// parent; telling that it is root window's sub-window with h/w matching screen (gXVisualInfo.screen)
		0,		// x (origin at left-top)
		0,		// y (origin at left-top)
		giWindowWidth,								// width
		giWindowHeight,								// height
		0,											// border width
		gpXVisualInfo->depth,						// depth
		InputOutput,								// class; your window is going to take inputs and also give outputs
		gpXVisualInfo->visual,						// visual
		styleMask,									// specifies which window attributes are defined in the attributes(last) argument
		&winAttribs 								// the structure from which the attributes (as specified by the style mask) are to be taken; address given since it fills the remaining attribs
		);	// creates an unmapped subwindow for a specified parent window, returns the window ID of the created window, and causes the X server to generate a CreateNotify event
			// The created window is not yet displayed (mapped) on the user's display;
			// ~CreateWindow()
			// To display the window, call XMapWindow()

	if(!gWindow)
	{
		printf("ERROR: Failed To Create Main Window.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	// give name to the window
	XStoreName(gpDisplay,gWindow,"Linux Window");	// gWindow: the window to which the name is to assigned

	// steps to close the window  on close btn and close menu (to actually exit after case 33; case 33 just brings the msg of closing the window)
	// specify the atom identifier of the protocol for deleting window
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);	// returns the atom identifier associated with the specified atom_name string (WM_DELETE_WINDOW)
																				// XInternAtom: X Server's internal immutable protocols(protocols cannot be changed by us, they are immutable, hence Atom)
																				// WM_DELETE_WINDOW: WM= Window Manager; tell X Server to use this protocol on case 33
																				// last parameter: True means create protocol/atom irrespective of whether it is already present or not(False means create protocol/atom only if it is absent)
	// add above protocol in window manager's protocol list
	XSetWMProtocols(gpDisplay,
		gWindow,				// window for which the above protocol is to be attached
		&windowManagerDelete,	// the list(array) of protocols(here, only 1)
		1						// only one protocol(no array)
		);

	// map the default window given by XServer(10,10,200,200) to your window(0,0,800,600)
	XMapWindow(gpDisplay,gWindow);	// the window is mapped, and the X server generates a MapNotify event
									// When all its ancestors are mapped, the window becomes viewable and will be visible on the screen if it is not obscured by another window. 
									// ~ShowWindow()
	ToggleFullscreen();

	// // window centering
	// screen=XScreenOfDisplay(gpDisplay, gpXVisualInfo->screen);	// return a pointer to the indicated screen number

	// XMoveWindow(gpDisplay,
	// 	gWindow,
	// 	XWidthOfScreen(screen)/2-giWindowWidth/2,	// x; XWidthOfScreen(screen): returns width of the specified screen in pixels
	// 	XHeightOfScreen(screen)/2-giWindowHeight/2	// y; XHeightOfScreen(screen): returns height of the specified screen in pixels
	// 	);	//  define the new location of the top-left pixel of the window's border or the window itself if it has no border
}

void InitializeOGL(void)
{
	// function declaration
	void UninitializeOGL(void);
	void ResizeOGL(int,int);

	// code
	gGlxContext=glXCreateContext(gpDisplay,
		gpXVisualInfo,		// Specifies the visual that defines the frame buffer resources available to the rendering context
		NULL,				// Specifies the context with which to share display lists/monitor. NULL indicates that no sharing is to take place (we have only one monitor)
        GL_TRUE				// Specifies whether rendering is to be done with a direct connection to the graphics system if possible (True) or through the X server (False)(?)
    						// Telling it that you want h/w context
        					// 'True' also OK
		);	// creates a GLX rendering context and returns its handle

	if(gGlxContext==NULL)
	{
		printf("\nglXCreateContext() Failed\n");
		UninitializeOGL();
	}

	if(glXMakeCurrent(gpDisplay,
		gWindow,		// Specifies a GLX drawable. Must be either an X window ID or a GLX pixmap ID.
		gGlxContext)	//  makes gGlxContext the current GLX rendering context of the calling thread	// ~wglMakeCurrent()
		==False)	
	{
		printf("\nglXMakeCurrent() Failed\n");
		UninitializeOGL();
	}


	// usual OGL code
	glClearColor(0.0f,0.0f,0.0f,1.0f);	// clear the screen by OGL color

	// warmup call to ResizeOGL()
	ResizeOGL(giWindowWidth,giWindowHeight);
}

void ResizeOGL(int width, int height)
{
	// usual OGL code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	//gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 10.0f, 100.0f);	// ? other values of near & far not working
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// ? other values of near & far not working
																			// parameters:
																			// fovy- The field of view angle, in degrees, in the y - direction.
																			// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																			//	zNear- The distance from the viewer to the near clipping plane(always positive).
																			//	zFar- The distance from the viewer to the far clipping plane(always positive).
}

void DisplayOGL(void)
{
	// function declarations
	void DrawI();
	void DrawN();
	void DrawD(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void DrawA();
	void DrawABand();
	void DrawPlane();
	void DrawSmokeUpper(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void DrawSmokeMiddle(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat,bool);
	void DrawSmokeLower(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void DrawSmokeRightUpper(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void DrawSmokeRightLower(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);

	// variable declarations
	GLfloat letter_x = 0.46f;
	GLfloat r = 1.42f;

	static GLfloat i1_x = -3.0f;
	static GLfloat a_x = 2.50f;
	static GLfloat n_y = 1.58f;
	static GLfloat i2_y = -1.58f;
	static GLfloat r_saffron = 0.0f, g_saffron = 0.0f, b_saffron = 0.0f;
	static GLfloat r_green = 0.0f, g_green = 0.0f, b_green = 0.0f;
	static GLfloat r_saffron_smoke = 255.0f, g_saffron_smoke = 153.0f, b_saffron_smoke = 51.0f;
	static GLfloat r_white_smoke = 1.0f;
	static GLfloat r_green_smoke = 18.0f, g_green_smoke = 136.0f, b_green_smoke = 7.0f;
	static GLfloat middle_plane_x = -letter_x * 5.4f;
	static GLfloat angle_upper_plane = -90.0f;
	static GLfloat angle_lower_plane = 90.0f;
	static GLfloat angle = (GLfloat)M_PI;
	static GLfloat angle_lower = (GLfloat)M_PI;
	static GLfloat angle_right_upper = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI;
	static GLfloat angle_right_lower = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI;
	//static int cnt = 0;

	static bool bDoneI1 = false;
	static bool bDoneA = false;
	static bool bDoneN = false;
	static bool bDoneI2 = false;
	static bool bDoneR_saffron = false, bDoneG_saffron = false, bDoneB_saffron = false;
	static bool bDoneR_green = false, bDoneG_green = false, bDoneB_green = false;
	static bool bDoneD = false;
	static bool bDoneMidddlePlane = false;
	static bool bDoneUpperPlane = false;
	static bool bDoneDetachPlanes = false;
	static bool bDoneABand = false;
	static 	bool bDoneFadingSmokes = false;

	// code
	glClear(GL_COLOR_BUFFER_BIT);	// clears buffers to preset values.
									// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

									// I1
	glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(i1_x, 0.0f, -3.0f);
	glLineWidth(15.0f);
	DrawI();

	// A
	if (bDoneI1 == true)
	{
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(a_x, 0.0f, -3.0f);
		DrawA();
	}

	// N
	if (bDoneA == true)
	{
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(-letter_x, n_y, -3.0f);
		DrawN();
	}

	// I2
	if (bDoneN == true)
	{
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(letter_x, i2_y, -3.0f);
		DrawI();
	}

	// D
	if (bDoneI2 == true)
	{
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(0.0f, 0.0f, -3.0f);
		DrawD(r_saffron, g_saffron, b_saffron, r_green, g_green, b_green);
	}

	// Attach planes
	if (bDoneD == true)
	{
		// upper
		if (bDoneUpperPlane == false)
		{
			glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
			glLoadIdentity();	// replaces the current matrix with the identity matrix.
			//glTranslatef(-2.0f*letter_x - letter_start - 0.02f, 1.22f, -3.0f);
			glTranslatef(-2.0f*letter_x + letter_end, 1.22f, -3.0f);
			glTranslatef(r*(GLfloat)cos(angle), r*(GLfloat)sin(angle), 0.0f);
			glRotatef(angle_upper_plane, 0.0f, 0.0f, 1.0f);
			DrawPlane();
		}
		DrawSmokeUpper(r, angle, letter_x, r_saffron_smoke, g_saffron_smoke, b_saffron_smoke, r_white_smoke, r_green_smoke, g_green_smoke, b_green_smoke);

		// middle
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(middle_plane_x, 0.0f, -3.0f);
		DrawPlane();

		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(0.0f, 0.0f, -3.0f);
		DrawSmokeMiddle(letter_x, middle_plane_x, r_saffron_smoke, g_saffron_smoke, b_saffron_smoke, r_white_smoke, r_green_smoke, g_green_smoke, b_green_smoke,bDoneDetachPlanes);

		// lower
		if (bDoneUpperPlane == false)
		{
			glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
			glLoadIdentity();	// replaces the current matrix with the identity matrix.
			glTranslatef(-2.0f*letter_x + letter_end, -1.22f, -3.0f);
			glTranslatef(r*(GLfloat)cos(angle_lower), r*(GLfloat)sin(angle_lower), 0.0f);
			glRotatef(angle_lower_plane, 0.0f, 0.0f, 1.0f);
			DrawPlane();
		}
		DrawSmokeLower(r, angle_lower, letter_x, r_saffron_smoke, g_saffron_smoke, b_saffron_smoke, r_white_smoke, r_green_smoke, g_green_smoke, b_green_smoke);
	}

	// A's band
	if (bDoneMidddlePlane == true)
	{
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(letter_x*2.0f, 0.0f, -3.0f);

		DrawABand();
		bDoneABand = true;
	}

	// Detach planes
	if (bDoneABand == true)
	{
		// upper
		if (bDoneDetachPlanes == false)
		{
			glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
			glLoadIdentity();	// replaces the current matrix with the identity matrix.
			glTranslatef(2.0f*letter_x - letter_end, 1.22f, -3.0f);
			glTranslatef(r*(GLfloat)cos(angle_right_upper), r*(GLfloat)sin(angle_right_upper), 0.0f);
			glRotatef(angle_upper_plane, 0.0f, 0.0f, 1.0f);

			DrawPlane();
		}
		DrawSmokeRightUpper(r, angle_right_upper, letter_x, r_saffron_smoke, g_saffron_smoke, b_saffron_smoke, r_white_smoke, r_green_smoke, g_green_smoke, b_green_smoke);

		// middle
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(middle_plane_x, 0.0f, -3.0f);
		DrawPlane();

		// lower
		if (bDoneDetachPlanes == false)
		{
			glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
			glLoadIdentity();	// replaces the current matrix with the identity matrix.
			glTranslatef(2.0f*letter_x - letter_end, -1.22f, -3.0f);
			glTranslatef(r*(GLfloat)cos(angle_right_lower), r*(GLfloat)sin(angle_right_lower), 0.0f);
			glRotatef(angle_lower_plane, 0.0f, 0.0f, 1.0f);

			DrawPlane();
		}
		DrawSmokeRightLower(r, angle_right_lower, letter_x, r_saffron_smoke, g_saffron_smoke, b_saffron_smoke, r_white_smoke, r_green_smoke, g_green_smoke, b_green_smoke);
	}

	glXSwapBuffers(gpDisplay,gWindow);	// exchange front and back buffers; gWindow: Specifies the drawable whose buffers are to be swapped


	if (bDoneI1 == false)
	{
		i1_x = i1_x + 0.0027f;

		if (i1_x >= -letter_x * 2)
		{
			i1_x = -letter_x * 2.0f;
			bDoneI1 = true;
		}
	}
	else if (bDoneA == false)
	{
		a_x = a_x - 0.0027f;

		if (a_x <= letter_x * 2)
		{
			a_x = letter_x * 2.0f;
			bDoneA = true;
		}
	}
	else if (bDoneN == false)
	{
		n_y = n_y - 0.0027f;

		if (n_y <= 0.0f)
		{
			bDoneN = true;
			n_y = 0.0f;
		}
	}
	else if (bDoneI2 == false)
	{
		i2_y = i2_y + 0.0027f;

		if (i2_y >= 0.0f)
		{
			i2_y = 0.0f;
			bDoneI2 = true;
		}
	}
	else if (bDoneD == false)
	{
		if (bDoneR_saffron == false)
		{
			r_saffron = r_saffron + 2.5f;
			if (r_saffron >= 255.0f)
			{
				r_saffron = 255.0f;
				bDoneR_saffron = true;
			}
		}
		if (bDoneG_saffron == false)
		{
			g_saffron = g_saffron + 1.0f;
			if (g_saffron >= 153.0f)
			{
				g_saffron = 153.0f;
				bDoneG_saffron = true;
			}
		}
		if (bDoneB_saffron == false)
		{
			b_saffron = b_saffron + 0.2f;
			if (b_saffron >= 51.0f)
			{
				b_saffron = 51.0f;
				bDoneB_saffron = true;
			}
		}

		// green
		if (bDoneR_green == false)
		{
			r_green = r_green + 0.1f;
			if (r_green >= 18.0f)
			{
				r_green = 18.0f;
				bDoneR_green = true;
			}
		}
		if (bDoneG_green == false)
		{
			g_green = g_green + 0.7f;
			if (g_green >= 136.0f)
			{
				g_green = 136.0f;
				bDoneG_green = true;
			}
		}
		if (bDoneB_green == false)
		{
			b_green = b_green + 0.1f;
			if (b_green >= 7.0f)
			{
				b_green = 7.0f;
				bDoneB_green = true;
			}
		}
		if (bDoneR_saffron == true && bDoneG_saffron == true && bDoneB_saffron == true && bDoneR_green == true && bDoneG_green == true && bDoneB_green == true)
			bDoneD = true;
	}
	else if (bDoneUpperPlane == false)
	{
		middle_plane_x = middle_plane_x + 0.00049f;


		if (angle_upper_plane <= 0.0f)
			angle_upper_plane = angle_upper_plane + 0.045f;

		if (angle_lower_plane >= 0.0f)
			angle_lower_plane = angle_lower_plane - 0.045f;

		if (r*(GLfloat)sin(angle) <= -1.22f)
		{
			bDoneUpperPlane = true;
			//fprintf_s(gpFile, "\nr*cos(angle)=%f", r*cos(angle));
			//fprintf_s(gpFile, "\nr*sin(angle)=%f", r*sin(angle));
			angle_upper_plane = 0.0f;
			angle_lower_plane = 0.0f;
		}

		angle = angle + 0.0005f;
		angle_lower = angle_lower - 0.0005f;
	}

	else if (bDoneMidddlePlane == false)
	{
		middle_plane_x = middle_plane_x + 0.00049f;
		if (middle_plane_x >= 2.0f*letter_x - letter_end+0.724918f)
			bDoneMidddlePlane = true;
	}

	else if (bDoneDetachPlanes == false)
	{
		middle_plane_x = middle_plane_x + 0.00049f;

		angle_right_upper = angle_right_upper + 0.0005f;
		angle_right_lower = angle_right_lower - 0.0005f;
		
		if (angle_upper_plane <= 90.0f)
				angle_upper_plane = angle_upper_plane + 0.047f;
		
		if (angle_lower_plane >= -90.0f)
				angle_lower_plane = angle_lower_plane - 0.047f;
		
		if (middle_plane_x >= letter_x * 6.0f)
			bDoneDetachPlanes = true;
	}
	else if(bDoneFadingSmokes==false)
	{
		if (r_saffron_smoke > 0.0f)
			r_saffron_smoke = r_saffron_smoke - 2.5f;
		if (g_saffron_smoke > 0.0f)
			g_saffron_smoke = g_saffron_smoke - 1.0f;
		if (b_saffron_smoke > 0.0f)
			b_saffron_smoke = b_saffron_smoke - 0.2f;

		if (r_white_smoke > 0.0f)
			r_white_smoke = r_white_smoke - 0.03f;

		if (r_green_smoke > 0.0f)
			r_green_smoke = r_green_smoke - 0.1f;
		if (g_green_smoke > 0.0f)
			g_green_smoke = g_green_smoke - 0.7f;
		if (b_green_smoke > 0.0f)
			b_green_smoke = b_green_smoke - 0.1f;

		if (r_saffron_smoke <= 0 && g_saffron_smoke <= 0 && b_saffron_smoke <= 0 && r_white_smoke && r_green_smoke <= 0 && g_green_smoke <= 0 && b_green_smoke <= 0)
			bDoneFadingSmokes = true;
	}
}

void DrawI()
{
	// code
	glBegin(GL_LINES);

	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(letter_start + 0.02f, half_letter_ht, 0.0f);
	glVertex3f(letter_end - 0.02f, half_letter_ht, 0.0f);
	glVertex3f(0.0f, half_letter_ht, 0.0f);

	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(0.0f, -half_letter_ht, 0.0f);
	glVertex3f(letter_start + 0.02f, -half_letter_ht, 0.0f);
	glVertex3f(letter_end - 0.02f, -half_letter_ht, 0.0f);

	glEnd();
}

void DrawN()
{
	// code
	glBegin(GL_LINES);

	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(letter_start, half_letter_ht + 0.015f, 0.0f);
	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(letter_start, -half_letter_ht - 0.015f, 0.0f);

	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(letter_start, half_letter_ht + 0.015f, 0.0f);
	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(letter_end, -half_letter_ht - 0.015f, 0.0f);

	glVertex3f(letter_end, -half_letter_ht - 0.015f, 0.0f);	// green
	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(letter_end, half_letter_ht + 0.015f, 0.0f);

	glEnd();
}

void DrawD(GLfloat r_saffron, GLfloat g_saffron, GLfloat b_saffron, GLfloat r_green, GLfloat g_green, GLfloat b_green)
{
	// code
	glBegin(GL_LINES);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron
	glVertex3f(letter_start + 0.05f, half_letter_ht, 0.0f);
	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green
	glVertex3f(letter_start + 0.05f, -half_letter_ht, 0.0f);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron
	glVertex3f(letter_start, half_letter_ht, 0.0f);
	glVertex3f(letter_end+0.0135f, half_letter_ht, 0.0f);

	glVertex3f(letter_end, half_letter_ht, 0.0f);
	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green
	glVertex3f(letter_end, -half_letter_ht, 0.0f);

	glVertex3f(letter_end+0.013f, -half_letter_ht, 0.0f);
	glVertex3f(letter_start, -half_letter_ht, 0.0f);

	glEnd();
}

void DrawA()
{
	// code
	glBegin(GL_LINES);

	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(letter_start, -half_letter_ht - 0.015f, 0.0f);
	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(0.0f, half_letter_ht + 0.015f, 0.0f);

	glVertex3f(0.0f, half_letter_ht + 0.015f, 0.0f);
	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(letter_end, -half_letter_ht - 0.015f, 0.0f);

	glEnd();
}

void DrawABand()
{
	glLineWidth(5.0f);
	glBegin(GL_LINES);

	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(letter_start / 2.0f, 0.0166f, 0.0f);
	glVertex3f(letter_end / 2.0f, 0.0166f, 0.0f);

	glColor3d(1.0, 1.0, 1.0);	// white
	glVertex3f(letter_start / 2.0f, 0.0f, 0.0f);
	glVertex3f(letter_end / 2.0f, 0.0f, 0.0f);

	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(letter_start / 2.0f, -0.0173f, 0.0f);
	glVertex3f(letter_end / 2.0f, -0.0173f, 0.0f);

	glEnd();
}

void DrawPlane()
{
	GLfloat upper_plane_x = -0.19f * 5.4f;
	GLfloat upper_plane_y = 1.18f;

	glColor3d(186 / 255.0, 226 / 255.0, 238 / 255.0);

	glBegin(GL_QUADS);

	// middle quad
	glVertex3f(0.25f + 0.1f, 0.05f, 0.0f);
	glVertex3f(0.0f + 0.1f, 0.05f, 0.0f);
	glVertex3f(0.0f + 0.1f, -0.05f, 0.0f);
	glVertex3f(0.25f + 0.1f, -0.05f, 0.0f);

	// upper wing
	glVertex3f(0.1f + 0.1f, 0.05f, 0.0f);
	glVertex3f(0.05f + 0.1f, 0.15f, 0.0f);
	glVertex3f(0.0f + 0.1f, 0.15f, 0.0f);
	glVertex3f(0.0f + 0.1f, 0.05f, 0.0f);

	// lower wing
	glVertex3f(0.1f + 0.1f, -0.05f, 0.0f);
	glVertex3f(0.05f + 0.1f, -0.15f, 0.0f);
	glVertex3f(0.0f + 0.1f, -0.15f, 0.0f);
	glVertex3f(0.0f + 0.1f, -0.05f, 0.0f);

	// back
	glVertex3f(0.0f + 0.1f, 0.05f, 0.0f);
	glVertex3f(-0.1f + 0.1f, 0.08f, 0.0f);
	glVertex3f(-0.1f + 0.1f, -0.08f, 0.0f);
	glVertex3f(0.0f + 0.1f, -0.05f, 0.0f);

	glEnd();

	// front triangle
	glBegin(GL_TRIANGLES);

	glVertex3f(0.25f + 0.1f, 0.05f, 0.0f);
	glVertex3f(0.25f + 0.1f, -0.05f, 0.0f);
	glVertex3f(0.30f + 0.1f, 0.00f, 0.0f);

	glEnd();


	// IAF
	// I
	glColor3d(99/255.0, 1.0, 1.0);	// white	// not visible
	glColor3d(99 / 255.0, 99 / 255.0, 99 / 255.0);	// dark gray

	glLineWidth(3.0f);
	glBegin(GL_LINES);

	glVertex3f(-0.01f + 0.1f, 0.03f, 0.0f);
	glVertex3f(0.03f + 0.1f, 0.03f, 0.0f);

	glVertex3f(0.01f + 0.1f, 0.03f, 0.0f);
	glVertex3f(0.01f + 0.1f, -0.03f, 0.0f);

	glVertex3f(-0.01f + 0.1f, -0.03f, 0.0f);
	glVertex3f(0.03f + 0.1f, -0.03f, 0.0f);

	glEnd();

	// A
	glLineWidth(3.0f);
	glBegin(GL_LINES);

	glVertex3f(0.05f + 0.1f, -0.03f, 0.0f);
	glVertex3f(0.07f + 0.1f, 0.03f, 0.0f);

	glVertex3f(0.07f + 0.1f, 0.03f, 0.0f);
	glVertex3f(0.09f + 0.1f, -0.03f, 0.0f);

	glVertex3f((0.05f + 0.07f) / 2.0f + 0.1f, 0.0f, 0.0f);
	glVertex3f((0.07f + 0.09f) / 2.0f + 0.1f, 0.0f, 0.0f);

	glEnd();

	// F
	glLineWidth(3.0f);
	glBegin(GL_LINES);

	glVertex3f(0.12f + 0.1f, -0.03f, 0.0f);
	glVertex3f(0.12f + 0.1f, 0.03f, 0.0f);

	glVertex3f(0.12f + 0.1f, 0.03f, 0.0f);
	glVertex3f(0.16f + 0.1f, 0.03f, 0.0f);

	glVertex3f(0.12f + 0.1f, 0.0f, 0.0f);
	glVertex3f(0.15f + 0.1f, 0.0f, 0.0f);

	glEnd();
}

void DrawSmokeMiddle(GLfloat letter_x, GLfloat middle_plane_x, GLfloat r_saffron, GLfloat g_saffron,GLfloat b_saffron, GLfloat r_white, GLfloat r_green, GLfloat g_green, GLfloat b_green,bool bDoneDetachPlanes)
{
	void DrawI();
	void DrawN();
	void DrawD(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);

	glLineWidth(5.0f);
	glBegin(GL_LINES);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron
	glVertex3f(-letter_x * 5.4f, 0.0166f, 0.0f);
	glVertex3f(middle_plane_x, 0.0166f, 0.0f);

	glColor3d(r_white, r_white, r_white);	// white
	glVertex3f(-letter_x * 5.4f, 0.0f, 0.0f);
	glVertex3f(middle_plane_x, 0.0f, 0.0f);

	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green
	glVertex3f(-letter_x * 5.4f, -0.0173f, 0.0f);
	glVertex3f(middle_plane_x, -0.0173f, 0.0f);

	glEnd();

	if (bDoneDetachPlanes == true)
	{
		// I1
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(-2.0f*letter_x, 0.0f, -3.0f);
		glLineWidth(15.0f);
		DrawI();

		// N
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(-letter_x, 0.0f, -3.0f);
		glLineWidth(15.0f);
		DrawN();

		// D
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(0.0f, 0.0f, -3.0f);
		glLineWidth(15.0f);
		DrawD(255.0f, 153.0f, 51.0f, 18.0f, 136.0f, 7.0f);

		// I2
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(letter_x, 0.0f, -3.0f);
		glLineWidth(15.0f);
		DrawI();

		// A
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(2.0f*letter_x, 0.0f, -3.0f);
		glLineWidth(15.0f);
		DrawA();
	}
}

void DrawSmokeUpper(GLfloat r, GLfloat angle, GLfloat letter_x, GLfloat r_saffron, GLfloat g_saffron, GLfloat b_saffron, GLfloat r_white, GLfloat r_green, GLfloat g_green, GLfloat b_green)
{
	static GLfloat angle_smoke;

	glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(-2.0f*letter_x + letter_end, 1.22f, -3.0f);

	glPointSize(5.0f);
	glBegin(GL_POINTS);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f((r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_white, r_white, r_white);	// white

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f(r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f((r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glEnd();
}

void DrawSmokeLower(GLfloat r, GLfloat angle_lower, GLfloat letter_x, GLfloat r_saffron, GLfloat g_saffron, GLfloat b_saffron, GLfloat r_white, GLfloat r_green, GLfloat g_green, GLfloat b_green)
{
	static GLfloat angle_smoke;
	
	glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(-2.0f*letter_x + letter_end, -1.22f, -3.0f);

	glPointSize(5.0f);
	glBegin(GL_POINTS);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f((r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_white, r_white, r_white);	// white

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f(r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f((r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glEnd();
}

void DrawSmokeRightUpper(GLfloat r, GLfloat angle_right_upper, GLfloat letter_x, GLfloat r_saffron, GLfloat g_saffron, GLfloat b_saffron, GLfloat r_white, GLfloat r_green, GLfloat g_green, GLfloat b_green)
{
	static GLfloat angle_smoke;

	glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(2.0f*letter_x - letter_end, 1.22f, -3.0f);

	glPointSize(5.0f);
	glBegin(GL_POINTS);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f+0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f((r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_white, r_white, r_white);	// white

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f(r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f((r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glEnd();
}

void DrawSmokeRightLower(GLfloat r, GLfloat angle_right_lower, GLfloat letter_x, GLfloat r_saffron, GLfloat g_saffron, GLfloat b_saffron, GLfloat r_white, GLfloat r_green, GLfloat g_green, GLfloat b_green)
{
	static GLfloat angle_smoke;

	glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(2.0f*letter_x - letter_end, -1.22f, -3.0f);

	glPointSize(5.0f);
	glBegin(GL_POINTS);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f((r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_white, r_white, r_white);	// white

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f(r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f((r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glEnd();
}




void UninitializeOGL(void)
{
	// variable declarations
	GLXContext currentGlxContext=glXGetCurrentContext();	// return the current context; can be diff from gGlxContext in multi-monitor mode

	if(currentGlxContext!=NULL && currentGlxContext==gGlxContext)
	{
		// release the current context
		glXMakeCurrent(gpDisplay,0,0);	// ~wglMakeCurrent(NULL,NULL)
	}

	// reclaim
	if(gGlxContext)
	{
		glXDestroyContext(gpDisplay,gGlxContext);	// If the GLX rendering context ctx is not current to any thread, glXDestroyContext() destroys it immediately.
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);	// The XDestroyWindow() function destroys the specified window as well as all of its subwindows and causes the X server to generate a DestroyNotify event for each window.
											// ~ DestroyWindow() (sends WM_DESTROY msg)
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		// Close the connection to the X server for the display specified in the Display structure and destroys all windows, resource IDs (Window, Font, Pixmap, Colormap, Cursor, and GContext), or other resources that the client has created on this display.
		// Therefore, these windows, resource IDs, and other resources should never be referenced again or an error will be generated.
		// Before exiting, you should call XCloseDisplay() explicitly so that any pending errors are reported as XCloseDisplay() performs a final XSync() operation.
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};	// event; XEvent: union

	// code
	// take the current state
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);	// _NET is for network protocol

	memset(&xev,0,sizeof(xev));	// &xev: Pointer to the object to copy the 0
	xev.type=ClientMessage;	// type of event/msg (ClientMessage: client is going to send it; in Xlib.h)
							// X server generates ClientMessage events only when a client calls XSendEvent(). 
	xev.xclient.window=gWindow;	// the window related to the event; xclient: XClientMessageEvent structure;
	xev.xclient.message_type=wm_state;	// type of message; indicates how the data should be interpreted by the receiving client
	xev.xclient.format=32;	// format (no. of bits) of msg
	//xev.xclient.data.l[0]=bFullscreen?0:1;	// The data member is a union that contains the members b(byte), s(short), and l(long). The b, s, and l members represent data of 20 8-bit values, 10 16-bit values, and 5 32-bit values.
											// If (bFullscreen) then l[0]=0 else l[0]=1
	xev.xclient.data.l[0]=1;
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;	// assign 1st member of l to new state(?)

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay,gpXVisualInfo->screen),	// the window the event is to be sent to
		False,										// is the msg propagatable to sub-windows/other clients? (False since only 1 window is going to be fullscreen)
		StructureNotifyMask,						// event mask; fullscreen means changing size of the window and the event mask for resizing is StructureNotifyMask
		&xev 										// address of the event that is to be sent
		);
}
