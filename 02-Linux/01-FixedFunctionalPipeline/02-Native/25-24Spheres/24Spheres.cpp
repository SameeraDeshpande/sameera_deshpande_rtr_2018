// standard header files
#include<iostream>	// namespace
#include<stdio.h>	// printf()
#include<stdlib.h>	// exit()
#include<memory.h>	// memset()
//X11 header files
#include<X11/Xlib.h>	// similar to windows.h; for all XServer APIs
#include<X11/Xutil.h> 	// XVisualInfo
#include<X11/XKBlib.h>	// keyboard utilization header
#include<X11/keysym.h>	// KeySym;	defines certain preprocessor symbols; mapping between keycode and symbol (eg. XK_F to ASCII 70)
// OpenGL related header files
#include<GL/gl.h>	// has OpenGL related functions and data types
#include<GL/glx.h>	// glx: OpenGL for XWindows (bridging APIs)	//~WGL
#include<GL/glu.h>

// namespaces
using namespace std;	// to avoid using 'std::' everywhere

// global variable declarations
bool bFullscreen=false;		// cpp feature (false=0)
Display *gpDisplay=NULL;
XVisualInfo* gpXVisualInfo=NULL;	// ~pfd (s/w representation of graphics card)
Colormap gColormap;	// struct; The colormap is a small table with entries specifying the RGB values of the currently available colors. The hardware imposes limits on the number of significant bits in these values.
				    // In X, the colormap entries are called color cells. The pixel values are interpreted as indices to the color cells. 
					// Thus, to refresh the screen, the display hardware uses the RGB values in the color cells indexed by the pixel values. 
					// The size of the colormap is less than or equal to 2^N color cells, where N is the number of bits in a pixel value. A device with 8 bits per pixel value, for example, will have a colormap with up to 256 color cells, indexed though pixel values ranging from 0 through 255. 
Window gWindow;	// struct; ~wndclass
int giWindowWidth=800;
int giWindowHeight=600;
static GLXContext gGlxContext;		// ~ghrc; static is optional. Do global static if you don't want it to be accessible outside this file
GLfloat angle_pyramid = 0.0f;
bool bLight = false;
GLUquadric *quadric[24];	// array of 24 pointers
GLfloat angleOfXRotation = 0.0f;
GLfloat angleOfYRotation = 0.0f;
GLfloat angleOfZRotation = 0.0f;
GLint keypress = 0;	// no key pressed

GLfloat LightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };	// inline initialization; R,G,B,A
GLfloat LightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };	// R,G,B,A
GLfloat LightPosition[] = { 0.0f,0.0f,0.0f,1.0f };	// x,y,z,w

// lighting models
GLfloat light_model_ambient[] = { 0.2f,0.2f,0.2f,1.0f };	// also default
GLfloat light_model_local_viewer[] = { 0.0f };

// entry-point function
int main(void)	// no WinMain() here
{
	// function declarations
	void CreateWindow(void);
	void InitializeOGL(void);
	void ResizeOGL(int,int);
	void DisplayOGL(void);
	void UpdateOGL(void);
	void UninitializeOGL(void);
	void ToggleFullscreen(void);

	// variable declarations
	int winWidth=giWindowWidth;
	int winHeight=giWindowHeight;
	char keys[26];
	bool bDone=false;

	// code
	// create window
	CreateWindow();	// gpDisplay, gWindow, gColormap, gXVisualInfo are defined in this function

	// call to Initialize()
	InitializeOGL();

	// game loop
	XEvent event;	//  In the X protocol, X clients send requests to the X server. The server sends replies, errors, and events to its clients. 
	KeySym keysym;

	while(bDone == false)
	{
		while(XPending(gpDisplay))		// returns the number of events that have been received from the X server but have not been removed from the event queue (asynchronous)	// ~PeekMessage()
		{
			XNextEvent(gpDisplay,&event);	// ~GetMessage(); 
											// The XNextEvent() function copies the first event from the event queue into the specified XEvent structure and then removes it from the queue. 
											// If the event queue is empty, XNextEvent() flushes the output buffer and blocks until an event is received
											// gpDisplay : Specifies the connection to the X server
											// &event ~ &msg
			switch(event.type)
			{
				case MapNotify:	// ~WM_CREATE (occurs only once) but creates as well as shows window
								// XServer maps its default window given to the client with the window with the client provided attributes
				break;

				case KeyPress:	// ~WM_KEYDOWN
				keysym=XkbKeycodeToKeysym(gpDisplay,event.xkey.keycode,0,0);	// convert keycode to key symbol
																				// event.xkey.keycode: keycode; 
																				// 0: page no. of UNICODE (0=ANSI i.e. English) (default)
																				// 0: is shift key pressed? (no)
				switch(keysym)	// ~SWITCH(wParam)
				{
					case XK_Escape:
					bDone=true;

					default:
					break;
				}

				XLookupString(&event.xkey,	// XKeyEvent*; string is in xkey
					keys,					// char*; the translated string comes into this buffer
					sizeof(keys),			// size of buffer
					NULL,					// KeySym*; returns the KeySym computed from the event if this argument is not NULL. 
					NULL 					// XComposeStatus*;	saves old state
					);	// translates a key event to a string and a KeySym
				
				switch(keys[0])
				{
					case 'F':
					case 'f':
					if(bFullscreen==false)
					{
						ToggleFullscreen();
						bFullscreen=true;
					}
					else
					{
						ToggleFullscreen();
						bFullscreen=false;
					}
					break;

					case 'L':
					case 'l':
						if (bLight == false)
						{
							bLight = true;
							glEnable(GL_LIGHTING);	// to enable light sources and thus lighting calculations
							//glEnable(GL_LIGHT0);	// need to explicitly enable due to above call
						}
						else
						{
							bLight = false;
							glDisable(GL_LIGHTING);	// to enable light sources and thus lighting calculations
							//glDisable(GL_LIGHT0);	
						}
						break;

						case 'X':
						case'x':
							keypress = 1;
							angleOfXRotation = 0.0f;	// resetting angleOfXRotation
							break;

						case 'Y':
						case'y':
							keypress = 2;
							angleOfYRotation = 0.0f;	// resetting angleOfYRotation
							break;

						case 'Z':
						case'z':
							keypress = 3;
							angleOfZRotation = 0.0f;	// resetting angleOfZRotation
							break;
				}
				break;	// case keypress

				case ButtonPress:	// mouse click
				switch(event.xbutton.button)	// xbutton: XButtonEvent; button: int(detail)
				{
					case 1: // ~WM_LBUTTONDOWN
						printf("Left mouse button is pressed\n");
					break;

					case 2:	// middle button
						printf("Middle mouse button is pressed\n");
					break;

					case 3:	// ~WM_RBUTTONDOWN
						printf("Right mouse button is pressed\n");
					break;

					case 4:	// mouse wheel up
						printf("Mouse wheel is released\n");
					break;

					case 5:	// mouse wheel down
						printf("Mouse wheel is pressed\n");
					break;

					default:
					break;
				}
				break;

				case MotionNotify:	// ~WM_MOUSEMOVE
				break;

				case ConfigureNotify:	// ~WM_SIZE (lParam gives size of window in Windows)
				winWidth=event.xconfigure.width;	// xconfigure: XConfigureEvent
				winHeight=event.xconfigure.height;	// The width and height members are set to the inside size of the window, not including the border
				ResizeOGL(winWidth,winHeight);
				break;

				case Expose:	// ~WM_PAINT	// nothing written since we want def black color
				break;

				case DestroyNotify:	// ~WM_CLOSE (option of whether to exit or not is given to user)
				break;

				case 33:	// const; When close button is clicked or close is chosen from menu, 33 is sent to window manager
							// this is just a provision for exit
				bDone=true;
				break;	// discipline; no use here


				default:
				break;
			}	// switch(event.type)
		}	// while(XPending())

		UpdateOGL();
		DisplayOGL();
		
	}	// while(bDone==false)

	UninitializeOGL();	
	return(0);
}

void CreateWindow(void)
{
	// function declarations
	void UninitializeOGL(void);

	// variable declarations
	XSetWindowAttributes winAttribs;	// attributes capable of being set
	int defaultScreen;
	//int defaultDepth;
	int styleMask;
	static int frameBufferAttributes[]={	// static not necessary in our OpenGL practices since we do not call this function again so no need to retain its value (but can be needed in case of multiple displays)
		GLX_RGBA,			// my palette is RGBA; frame buffer contents; If present, only TrueColor and DirectColor visuals are considered. Otherwise, only PseudoColor and StaticColor visuals are considered.                
		GLX_DOUBLEBUFFER,	// If present, only double-buffered visuals are considered
		GLX_RED_SIZE,8,		// Must be followed by a nonnegative minimum size specification
							// allocate 8 bit for red(since single buffer) (hence, 32-bit frame buffer); GLX_RED_SIZE is an enumerated type attribute
							// key-value pairs; Integer attributes and enumerated type attributes are followed immediately by the corresponding desired or minimum value
		GLX_GREEN_SIZE,8,
		GLX_BLUE_SIZE,8,
		GLX_ALPHA_SIZE,8,
		GLX_DEPTH_SIZE,24,
		None 				// the last attribute must be None.
							// in case of a const array, when you don't wish to assign values to all its members but you need to pass that array to a function, do end with 0/None (#define None 0)
							// None means telling it to give values to other attributes
	};
	Screen *screen;

	// code
	// open the XServer connection and get Display
	gpDisplay=XOpenDisplay(NULL);	// To open a connection to the X server that controls a display;	NULL: get default Display (#define NULL 0)
									// returns a Display structure that serves as the connection to the X server and that contains all the information about that X server (representative of client)
									// will be sent as first parameter of further functions for the session manager to authencicate the client
	if(gpDisplay==NULL)	// error checking; to check if mem is assigned or not
	{
		printf("ERROR: Unable To Open X Display.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	 // get default screen using gpDisplay obtained in previous step
	defaultScreen=XDefaultScreen(gpDisplay);	// ~MONITORINFOF_PRIMARY	// no need of error checking
												// return the default screen number referenced by the XOpenDisplay() function.
												// use defaultScreen just for obtaining a screen of matching VisualInfo

	// // get default bit depth using gpDisplay and defaultScreen obtained in prev two steps
	// defaultDepth=DefaultDepth(gpDisplay,defaultScreen);	 // Actually XDefaultDepth, DefaultDepth is macro
	// 													 // return the depth (number of planes) of the default root window for the specified screen

	// get closest matching VisualInfo to frameBufferAttributes
	gpXVisualInfo=glXChooseVisual(gpDisplay,
		defaultScreen,			// Specifies the screen number (int)
		frameBufferAttributes	// Specifies a list of boolean attributes and integer attribute/value pairs
		);	// In OpenGL, bridging API itself gives you gpXVisualInfo

	if(gpXVisualInfo==NULL)	// error checking; to check if mem is assigned or not
	{
		printf("ERROR : Unable To Allocate Memory For Visual Info.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	// fill window attributes;	~wndclass
	winAttribs.border_pixel=0;	// border color; pixel=color; 	0= don't care
	winAttribs.border_pixmap=0;	// border image; 0= don't create; pixmap~ bitmap of Windows
	winAttribs.background_pixel=BlackPixel(gpDisplay,defaultScreen);	// background color 	// ~hBrush
																		// returns the black pixel value for the specified screen.
																		// macro of XBlackPixel() 
	winAttribs.background_pixmap=0;		// background image

	winAttribs.colormap=XCreateColormap(
		gpDisplay,		// the connection to the X server
		RootWindow(gpDisplay,gpXVisualInfo->screen),	// the window on whose screen you want to create a colormap; give matching screen to h/w 
		gpXVisualInfo->visual,						// a visual type supported on the screen; h/w's representation
		AllocNone									// Specifies the colormap entries to be allocated; AllocNone: do not allocate mem for colormap(the colormap initially has no allocated entries, and clients can allocate them)
		);	//~cbClsExtra and cbWndExtra

	gColormap=winAttribs.colormap;	// saving the colormap(since we have done AllocNone)	// why (not used further)?

	// specify the events(messages) that you want the X server to return to a client application. 
	winAttribs.event_mask = VisibilityChangeMask|KeyPressMask|ButtonPressMask|PointerMotionMask|StructureNotifyMask|ExposureMask; // VisibilityChangeMask: for case MapNotify
																																 // KeyPressMask: for case KeyPress
																																 // ButtonPressMask: for case ButtonPress (mouse button down events)
																																 // PointerMotionMask: for case MotionNotify
																																 // StructureNotifyMask: for case ConfigureNotify (resize)
																																 // ExposureMask: for case Expose
																																 // no mask for case DestroyNotify since it is always there; also there exists no mask for case 33
	// fill window styles
	// like for creating event mask you refer events, for creating style mask you refer attribs
	styleMask=CWBorderPixel|CWBackPixel|CWEventMask|CWColormap;	

	// create actual window
	gWindow=XCreateWindow(gpDisplay,
		RootWindow(gpDisplay,gpXVisualInfo->screen),// parent; telling that it is root window's sub-window with h/w matching screen (gXVisualInfo.screen)
		0,		// x (origin at left-top)
		0,		// y (origin at left-top)
		giWindowWidth,								// width
		giWindowHeight,								// height
		0,											// border width
		gpXVisualInfo->depth,						// depth
		InputOutput,								// class; your window is going to take inputs and also give outputs
		gpXVisualInfo->visual,						// visual
		styleMask,									// specifies which window attributes are defined in the attributes(last) argument
		&winAttribs 								// the structure from which the attributes (as specified by the style mask) are to be taken; address given since it fills the remaining attribs
		);	// creates an unmapped subwindow for a specified parent window, returns the window ID of the created window, and causes the X server to generate a CreateNotify event
			// The created window is not yet displayed (mapped) on the user's display;
			// ~CreateWindow()
			// To display the window, call XMapWindow()

	if(!gWindow)
	{
		printf("ERROR: Failed To Create Main Window.\nExiting Now\n");
		UninitializeOGL();
		exit(0);
	}

	// give name to the window
	XStoreName(gpDisplay,gWindow,"Linux Window");	// gWindow: the window to which the name is to assigned

	// steps to close the window  on close btn and close menu (to actually exit after case 33; case 33 just brings the msg of closing the window)
	// specify the atom identifier of the protocol for deleting window
	Atom windowManagerDelete=XInternAtom(gpDisplay,"WM_DELETE_WINDOW",True);	// returns the atom identifier associated with the specified atom_name string (WM_DELETE_WINDOW)
																				// XInternAtom: X Server's internal immutable protocols(protocols cannot be changed by us, they are immutable, hence Atom)
																				// WM_DELETE_WINDOW: WM= Window Manager; tell X Server to use this protocol on case 33
																				// last parameter: True means create protocol/atom irrespective of whether it is already present or not(False means create protocol/atom only if it is absent)
	// add above protocol in window manager's protocol list
	XSetWMProtocols(gpDisplay,
		gWindow,				// window for which the above protocol is to be attached
		&windowManagerDelete,	// the list(array) of protocols(here, only 1)
		1						// only one protocol(no array)
		);

	// map the default window given by XServer(10,10,200,200) to your window(0,0,800,600)
	XMapWindow(gpDisplay,gWindow);	// the window is mapped, and the X server generates a MapNotify event
									// When all its ancestors are mapped, the window becomes viewable and will be visible on the screen if it is not obscured by another window. 
									// ~ShowWindow()

	// window centering
	screen=XScreenOfDisplay(gpDisplay, gpXVisualInfo->screen);	// return a pointer to the indicated screen number

	XMoveWindow(gpDisplay,
		gWindow,
		XWidthOfScreen(screen)/2-giWindowWidth/2,	// x; XWidthOfScreen(screen): returns width of the specified screen in pixels
		XHeightOfScreen(screen)/2-giWindowHeight/2	// y; XHeightOfScreen(screen): returns height of the specified screen in pixels
		);	//  define the new location of the top-left pixel of the window's border or the window itself if it has no border
}

void InitializeOGL(void)
{
	// function declaration
	void UninitializeOGL(void);
	void ResizeOGL(int,int);

	// code
	gGlxContext=glXCreateContext(gpDisplay,
		gpXVisualInfo,		// Specifies the visual that defines the frame buffer resources available to the rendering context
		NULL,				// Specifies the context with which to share display lists/monitor. NULL indicates that no sharing is to take place (we have only one monitor)
        GL_TRUE				// Specifies whether rendering is to be done with a direct connection to the graphics system if possible (True) or through the X server (False)(?)
    						// Telling it that you want h/w context
        					// 'True' also OK
		);	// creates a GLX rendering context and returns its handle

	if(gGlxContext==NULL)
	{
		printf("\nglXCreateContext() Failed\n");
		UninitializeOGL();
	}

	if(glXMakeCurrent(gpDisplay,
		gWindow,		// Specifies a GLX drawable. Must be either an X window ID or a GLX pixmap ID.
		gGlxContext)	//  makes gGlxContext the current GLX rendering context of the calling thread	// ~wglMakeCurrent()
		==False)	
	{
		printf("\nglXMakeCurrent() Failed\n");
		UninitializeOGL();
	}

	// 5 imp. depth calls-
	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// lights
	glEnable(GL_AUTO_NORMAL);	// tell OpenGL to give normals

	glEnable(GL_NORMALIZE);		// If enabled, normal vectors specified with glNormal() are scaled to unit length after transformation
								// gives good quality; uses mathematical calculations, hence less performance

	// configuration of lights using configuration parameters (setting parameters)
	glLightfv(
		GL_LIGHT0,	// a light (OGL light no. 0 out of the 8 lights)
		GL_AMBIENT,	// parameter for light; to specify the ambient(sorrounding/encircling) RGBA intensity for light
		LightAmbient // pointer to the array whose values will be given to GL_AMBIENT; default : (0.0, 0.0, 0.0, 1.0)
	);	// f= float, v= vector

	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);	// to specify the diffuse(to be spread over) RGBA intensity of the light
													// The default diffuse intensity is (0.0, 0.0, 0.0, 1.0) for all lights other than light zero. 
													// The default diffuse intensity of light zero is (1.0, 1.0, 1.0, 1.0). 

	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);	// specifies the position of the light in homogeneous object coordinates
														// The position is transformed by the modelview matrix when glLight is called (just as if it were a point), and it is stored in eye coordinates.
														// The default position is (0,0,1,0); thus, the default light source is directional (since w=0), parallel to, and in the direction of the z axis. 

	// set the lighting model(overall lighting) parameters
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);	// GL_LIGHT_MODEL_AMBIENT: to specify the ambient RGBA intensity of the entire scene, 
																	// light_model_ambient: values

	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);	// GL_LIGHT_MODEL_LOCAL_VIEWER: specifies how specular reflection angles are computed
																			// light_model_local_viewer: a single integer or floating-point value.
																			// If 2nd parameter is 0 (or 0.0), specular reflection angles take the view direction to be parallel to and in the direction of the z axis, regardless of the location of the vertex in eye coordinates. 
																			// Otherwise specular reflections are computed from the origin of the eye coordinate system. The default is 0. 

	glEnable(GL_LIGHT0);	// need to explicitly enable due to explicit call to glEnable(GL_LIGHTING) in case 'l/L'

	// create 24 spheres
	for (int i = 0;i < 24;i++)
	{
		quadric[i] = gluNewQuadric();
	}

	glClearColor(0.0f,0.0f,0.0f,1.0f);	// clear the screen by OGL color

	// warmup call to ResizeOGL()
	ResizeOGL(giWindowWidth,giWindowHeight);
}

void ResizeOGL(int width, int height)
{
	// usual OGL code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (height > width)
	{
		glOrtho(0.0f, 15.5f, 0.0f, 15.5f*(GLfloat)height / (GLfloat)width, -10.0f, 10.0f);	// LRBTNF
	}
	else
	{
		glOrtho(0.0f, 15.5f*(GLfloat)width / (GLfloat)height, 0.0f, 15.5f, -10.0f, 10.0f);
	}
}

void DisplayOGL(void)
{
	// function declarations
	void Draw24Spheres(void);

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

	glMatrixMode(GL_MODELVIEW);	// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.

	//glTranslatef(0.0f, 0.0f, -0.70f);	// or -0.75f or -0.55f
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//gluSphere(quadric, 0.2f, 30, 30);		
	
	if (keypress == 1)
	{
		glRotatef(angleOfXRotation, 1.0f, 0.0f, 0.0f);	// change y
		LightPosition[1] = angleOfXRotation;
		//LightPosition[1] = angleOfXRotation;

	}
	else if (keypress == 2)
	{
		glRotatef(angleOfYRotation, 0.0f, 1.0f, 0.0f);	// change z
		LightPosition[2] = angleOfYRotation;
	}
	else if (keypress == 3)
	{
		glRotatef(angleOfZRotation, 0.0f, 0.0f, 1.0f);	// change x
		LightPosition[0] = angleOfZRotation;
	}
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);

	Draw24Spheres();

	glXSwapBuffers(gpDisplay,gWindow);	// exchange front and back buffers; gWindow: Specifies the drawable whose buffers are to be swapped
}

void Draw24Spheres(void)
{
	GLfloat MaterialAmbient[4];
	GLfloat MaterialDiffuse[4];
	GLfloat MaterialSpecular[4];
	GLfloat MaterialShininess;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// =============================================================================================================================
	// first column - gems
	// emerald
	MaterialAmbient[0] = 0.0215f;	// r
	MaterialAmbient[1] = 0.1745f;	// g
	MaterialAmbient[2] = 0.0215f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.07568f;	// r
	MaterialDiffuse[1] = 0.61424f;	// g
	MaterialDiffuse[2] = 0.07568f;	// b
	MaterialDiffuse[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.633f;		// r
	MaterialSpecular[1] = 0.727811f;	// g
	MaterialSpecular[2] = 0.633f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 13.285714f, 0.0f);
	gluSphere(quadric[0], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// jade
	MaterialAmbient[0] = 0.135f;	// r
	MaterialAmbient[1] = 0.2225f;	// g
	MaterialAmbient[2] = 0.1575f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.54f;	// r
	MaterialDiffuse[1] = 0.89f;	// g
	MaterialDiffuse[2] = 0.63f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.316228f;		// r
	MaterialSpecular[1] = 0.316228f;		// g
	MaterialSpecular[2] = 0.316228f;		// b
	MaterialSpecular[3] = 1.0f;				// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 11.071428f, 0.0f);
	gluSphere(quadric[1], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// obsidian
	MaterialAmbient[0] = 0.05375f;	// r
	MaterialAmbient[1] = 0.05f;		// g
	MaterialAmbient[2] = 0.06625f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.18275f;	// r
	MaterialDiffuse[1] = 0.17f;		// g
	MaterialDiffuse[2] = 0.22525f;	// b
	MaterialDiffuse[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.332741f;		// r
	MaterialSpecular[1] = 0.328634f;		// g
	MaterialSpecular[2] = 0.346435f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.3f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 8.857142f, 0.0f);
	gluSphere(quadric[2], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// pearl
	MaterialAmbient[0] = 0.25f;		// r
	MaterialAmbient[1] = 0.20725f;	// g
	MaterialAmbient[2] = 0.20725f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 1.0f;		// r
	MaterialDiffuse[1] = 0.829f;	// g
	MaterialDiffuse[2] = 0.829f;	// b
	MaterialDiffuse[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.296648f;		// r
	MaterialSpecular[1] = 0.296648f;		// g
	MaterialSpecular[2] = 0.296648f;		// b
	MaterialSpecular[3] = 1.0f;				// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.088f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 6.642857f, 0.0f);
	gluSphere(quadric[3], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// ruby
	MaterialAmbient[0] = 0.1745f;		// r
	MaterialAmbient[1] = 0.01175f;	// g
	MaterialAmbient[2] = 0.01175f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.61424f;		// r
	MaterialDiffuse[1] = 0.04136f;	// g
	MaterialDiffuse[2] = 0.04136f;	// b
	MaterialDiffuse[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.727811f;		// r
	MaterialSpecular[1] = 0.626959f;		// g
	MaterialSpecular[2] = 0.626959f;		// b
	MaterialSpecular[3] = 1.0f;				// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 4.428571f, 0.0f);
	gluSphere(quadric[4], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// turquoise
	MaterialAmbient[0] = 0.1f;		// r
	MaterialAmbient[1] = 0.18725f;	// g
	MaterialAmbient[2] = 0.1745f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.396f;		// r
	MaterialDiffuse[1] = 0.74151f;	// g
	MaterialDiffuse[2] = 0.69102f;	// b
	MaterialDiffuse[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.297254f;		// r
	MaterialSpecular[1] = 0.30829f;		// g
	MaterialSpecular[2] = 0.306678f;		// b
	MaterialSpecular[3] = 1.0f;				// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 2.214285f, 0.0f);
	gluSphere(quadric[4], 1.0f, 30, 30);

	// =============================================================================================================================
	// second column - metals
	// brass
	MaterialAmbient[0] = 0.329412f;	// r
	MaterialAmbient[1] = 0.223529f;	// g
	MaterialAmbient[2] = 0.027451f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.780392f;	// r
	MaterialDiffuse[1] = 0.568627f;	// g
	MaterialDiffuse[2] = 0.113725f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.992157f;		// r
	MaterialSpecular[1] = 0.941176f;	// g
	MaterialSpecular[2] = 0.807843f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = (GLfloat)0.21794872 * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 13.285714f, 0.0f);
	gluSphere(quadric[6], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// bronze
	MaterialAmbient[0] = 0.2125f;	// r
	MaterialAmbient[1] = 0.1275f;	// g
	MaterialAmbient[2] = 0.054f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.714f;	// r
	MaterialDiffuse[1] = 0.4284f;	// g
	MaterialDiffuse[2] = 0.18144f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.393548f;		// r
	MaterialSpecular[1] = 0.271906f;	// g
	MaterialSpecular[2] = 0.166721f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.2f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 11.071428f, 0.0f);
	gluSphere(quadric[7], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// chrome
	MaterialAmbient[0] = 0.25f;	// r
	MaterialAmbient[1] = 0.25f;	// g
	MaterialAmbient[2] = 0.25f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.4f;	// r
	MaterialDiffuse[1] = 0.4f;	// g
	MaterialDiffuse[2] = 0.4f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.774597f;		// r
	MaterialSpecular[1] = 0.774597f;	// g
	MaterialSpecular[2] = 0.774597f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 8.857142f, 0.0f);
	gluSphere(quadric[8], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// copper
	MaterialAmbient[0] = 0.19125f;	// r
	MaterialAmbient[1] = 0.0735f;	// g
	MaterialAmbient[2] = 0.0225f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.7038f;	// r
	MaterialDiffuse[1] = 0.27048f;	// g
	MaterialDiffuse[2] = 0.0828f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.256777f;		// r
	MaterialSpecular[1] = 0.137622f;	// g
	MaterialSpecular[2] = 0.086014f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 6.642857f, 0.0f);
	gluSphere(quadric[9], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// gold
	MaterialAmbient[0] = 0.24725f;	// r
	MaterialAmbient[1] = 0.1995f;	// g
	MaterialAmbient[2] = 0.0745f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.75164f;	// r
	MaterialDiffuse[1] = 0.60648f;	// g
	MaterialDiffuse[2] = 0.22648f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.628281f;		// r
	MaterialSpecular[1] = 0.555802f;	// g
	MaterialSpecular[2] = 0.366065f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.4f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 4.428571f, 0.0f);
	gluSphere(quadric[10], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// silver
	MaterialAmbient[0] = 0.19225f;	// r
	MaterialAmbient[1] = 0.19225f;	// g
	MaterialAmbient[2] = 0.19225f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.50754f;	// r
	MaterialDiffuse[1] = 0.50754f;	// g
	MaterialDiffuse[2] = 0.50754f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.508273f;		// r
	MaterialSpecular[1] = 0.508273f;	// g
	MaterialSpecular[2] = 0.508273f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.4f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 2.214285f, 0.0f);
	gluSphere(quadric[11], 1.0f, 30, 30);

	// =============================================================================================================================
	// third column - plastic
	// black
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.01f;	// r
	MaterialDiffuse[1] = 0.01f;	// g
	MaterialDiffuse[2] = 0.01f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.50f;		// r
	MaterialSpecular[1] = 0.50f;	// g
	MaterialSpecular[2] = 0.50f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 13.285714f, 0.0f);
	gluSphere(quadric[12], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// cyan
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.1f;	// g
	MaterialAmbient[2] = 0.06f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.0f;	// r
	MaterialDiffuse[1] = 0.50980392f;	// g
	MaterialDiffuse[2] = 0.50980392f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.50196078f;		// r
	MaterialSpecular[1] = 0.50196078f;	// g
	MaterialSpecular[2] = 0.50196078f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 11.071428f, 0.0f);
	gluSphere(quadric[13], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// green
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.1f;	// r
	MaterialDiffuse[1] = 0.35f;	// g
	MaterialDiffuse[2] = 0.1f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.45f;		// r
	MaterialSpecular[1] = 0.55f;	// g
	MaterialSpecular[2] = 0.45f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 8.857142f, 0.0f);
	gluSphere(quadric[14], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// red
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5f;	// r
	MaterialDiffuse[1] = 0.0f;	// g
	MaterialDiffuse[2] = 0.0f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;		// r
	MaterialSpecular[1] = 0.6f;	// g
	MaterialSpecular[2] = 0.6f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 6.642857f, 0.0f);
	gluSphere(quadric[15], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// white
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.55f;	// r
	MaterialDiffuse[1] = 0.55f;	// g
	MaterialDiffuse[2] = 0.55f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.70f;		// r
	MaterialSpecular[1] = 0.70f;	// g
	MaterialSpecular[2] = 0.70f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 4.428571f, 0.0f);
	gluSphere(quadric[16], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// yellow
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5f;	// r
	MaterialDiffuse[1] = 0.5f;	// g
	MaterialDiffuse[2] = 0.0f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.60f;		// r
	MaterialSpecular[1] = 0.60f;	// g
	MaterialSpecular[2] = 0.50f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 2.214285f, 0.0f);
	gluSphere(quadric[17], 1.0f, 30, 30);

	// =============================================================================================================================
	// fourth column - rubber
	// black
	MaterialAmbient[0] = 0.02f;	// r
	MaterialAmbient[1] = 0.02f;	// g
	MaterialAmbient[2] = 0.02f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.01f;	// r
	MaterialDiffuse[1] = 0.01f;	// g
	MaterialDiffuse[2] = 0.01f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.4f;		// r
	MaterialSpecular[1] = 0.4f;	// g
	MaterialSpecular[2] = 0.4f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 13.285714f, 0.0f);
	gluSphere(quadric[18], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// cyan
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.05f;	// g
	MaterialAmbient[2] = 0.05f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.4f;	// r
	MaterialDiffuse[1] = 0.5f;	// g
	MaterialDiffuse[2] = 0.5f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.04f;		// r
	MaterialSpecular[1] = 0.7f;	// g
	MaterialSpecular[2] = 0.7f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 11.071428f, 0.0f);
	gluSphere(quadric[19], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// green
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.05f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.4f;	// r
	MaterialDiffuse[1] = 0.5f;	// g
	MaterialDiffuse[2] = 0.4f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.04f;		// r
	MaterialSpecular[1] = 0.7f;	// g
	MaterialSpecular[2] = 0.04f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 8.857142f, 0.0f);
	gluSphere(quadric[20], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// red
	MaterialAmbient[0] = 0.05f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5f;	// r
	MaterialDiffuse[1] = 0.4f;	// g
	MaterialDiffuse[2] = 0.4f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;		// r
	MaterialSpecular[1] = 0.04f;	// g
	MaterialSpecular[2] = 0.04f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 6.642857f, 0.0f);
	gluSphere(quadric[21], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// white
	MaterialAmbient[0] = 0.05f;	// r
	MaterialAmbient[1] = 0.05f;	// g
	MaterialAmbient[2] = 0.05f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5f;	// r
	MaterialDiffuse[1] = 0.5f;	// g
	MaterialDiffuse[2] = 0.5f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;		// r
	MaterialSpecular[1] = 0.7f;	// g
	MaterialSpecular[2] = 0.7f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 4.428571f, 0.0f);
	gluSphere(quadric[22], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// yellow
	MaterialAmbient[0] = 0.05f;	// r
	MaterialAmbient[1] = 0.05f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5f;	// r
	MaterialDiffuse[1] = 0.5f;	// g
	MaterialDiffuse[2] = 0.4f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;		// r
	MaterialSpecular[1] = 0.7f;	// g
	MaterialSpecular[2] = 0.04f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 2.214285f, 0.0f);
	gluSphere(quadric[23], 1.0f, 30, 30);
}

void UpdateOGL()
{
	angleOfXRotation = angleOfXRotation + 0.05f;

	if (angleOfXRotation >= 360.0f)
		angleOfXRotation = 0.0f;

	angleOfYRotation = angleOfYRotation + 0.05f;

	if (angleOfYRotation >= 360.0f)
		angleOfYRotation = 0.0f;

	angleOfZRotation = angleOfZRotation + 0.05f;

	if (angleOfZRotation >= 360.0f)
		angleOfZRotation = 0.0f;
}

void UninitializeOGL(void)
{
	// code
	for (int i = 0;i < 24;i++)
	{
		if (quadric[i])
		{
			gluDeleteQuadric(quadric[i]);
			quadric[i] = NULL;
		}
	}

	GLXContext currentGlxContext=glXGetCurrentContext();	// return the current context; can be diff from gGlxContext in multi-monitor mode
	
	if(currentGlxContext!=NULL && currentGlxContext==gGlxContext)
	{
		// release the current context
		glXMakeCurrent(gpDisplay,0,0);	// ~wglMakeCurrent(NULL,NULL)
	}

	// reclaim
	if(gGlxContext)
	{
		glXDestroyContext(gpDisplay,gGlxContext);	// If the GLX rendering context ctx is not current to any thread, glXDestroyContext() destroys it immediately.
	}

	if(gWindow)
	{
		XDestroyWindow(gpDisplay,gWindow);	// The XDestroyWindow() function destroys the specified window as well as all of its subwindows and causes the X server to generate a DestroyNotify event for each window.
											// ~ DestroyWindow() (sends WM_DESTROY msg)
	}

	if(gColormap)
	{
		XFreeColormap(gpDisplay,gColormap);
	}

	if(gpXVisualInfo)
	{
		free(gpXVisualInfo);
		gpXVisualInfo=NULL;
	}

	if(gpDisplay)
	{
		// Close the connection to the X server for the display specified in the Display structure and destroys all windows, resource IDs (Window, Font, Pixmap, Colormap, Cursor, and GContext), or other resources that the client has created on this display.
		// Therefore, these windows, resource IDs, and other resources should never be referenced again or an error will be generated.
		// Before exiting, you should call XCloseDisplay() explicitly so that any pending errors are reported as XCloseDisplay() performs a final XSync() operation.
		XCloseDisplay(gpDisplay);
		gpDisplay=NULL;
	}
}

void ToggleFullscreen(void)
{
	// variable declarations
	Atom wm_state;
	Atom fullscreen;
	XEvent xev={0};	// event; XEvent: union

	// code
	// take the current state
	wm_state=XInternAtom(gpDisplay,"_NET_WM_STATE",False);	// _NET is for network protocol

	memset(&xev,0,sizeof(xev));	// &xev: Pointer to the object to copy the 0
	xev.type=ClientMessage;	// type of event/msg (ClientMessage: client is going to send it; in Xlib.h)
							// X server generates ClientMessage events only when a client calls XSendEvent(). 
	xev.xclient.window=gWindow;	// the window related to the event; xclient: XClientMessageEvent structure;
	xev.xclient.message_type=wm_state;	// type of message; indicates how the data should be interpreted by the receiving client
	xev.xclient.format=32;	// format (no. of bits) of msg
	xev.xclient.data.l[0]=bFullscreen?0:1;	// The data member is a union that contains the members b(byte), s(short), and l(long). The b, s, and l members represent data of 20 8-bit values, 10 16-bit values, and 5 32-bit values.
											// If (bFullscreen) then l[0]=0 else l[0]=1
	fullscreen=XInternAtom(gpDisplay,"_NET_WM_STATE_FULLSCREEN",False);
	xev.xclient.data.l[1]=fullscreen;	// assign 1st member of l to new state(?)

	XSendEvent(gpDisplay,
		RootWindow(gpDisplay,gpXVisualInfo->screen),	// the window the event is to be sent to
		False,										// is the msg propagatable to sub-windows/other clients? (False since only 1 window is going to be fullscreen)
		StructureNotifyMask,						// event mask; fullscreen means changing size of the window and the event mask for resizing is StructureNotifyMask
		&xev 										// address of the event that is to be sent
		);
}
