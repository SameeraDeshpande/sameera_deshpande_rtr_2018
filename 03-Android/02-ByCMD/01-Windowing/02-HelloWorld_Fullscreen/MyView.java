package com.rtr.win_hello;

// packages added by me
import androidx.appcompat.widget.AppCompatTextView; // for AppCompatTextView class (extended by MyView class)
import android.content.Context; // for Context class (drawing context to MyView constructor)
import android.view.Gravity;    // for Gravity class (text in center)
import android.graphics.Color;  // for Color class (text color)

// our class
public class MyView extends AppCompatTextView
{
    // constructor
    public MyView(Context drawingContext)
    {
        super(drawingContext);
        setTextColor(Color.rgb(0,255,0));   // green
        setTextSize(60);    // Set the default text size to the given value, interpreted as "scaled pixel" units. (para: float)
        setGravity(Gravity.CENTER); // Place the object in the center of its container in both the vertical and horizontal axis, not changing its size
        setText("Hello World !!!"); // Sets the text to be displayed
    }
}
