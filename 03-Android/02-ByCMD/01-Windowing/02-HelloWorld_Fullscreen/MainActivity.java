package com.rtr.win_hello;

// default given packages
import androidx.appcompat.app.AppCompatActivity;    // for AppCompatActivity class (extended by MainActivity class)
import android.os.Bundle;   // for Bundle class (parameter to onCreate())

// packages added by me
import android.view.Window; // for Window class (remove title bar)
import android.view.WindowManager;  // for WindowManager class (fullscreen)
import android.content.pm.ActivityInfo; // pm- packg manager    // for ActivityInfo class (landscape orientation)
import android.graphics.Color;  // for Color class (background color)
import android.view.View;   // for View class (remove navigation bar)   
/*This class represents the basic building block for user interface components. 
A View occupies a rectangular area on the screen and is responsible for drawing and event handling. 
View is the base class for widgets, which are used to create interactive UI components (buttons, text fields, etc.)*/

// An activity is a single, focused thing that the user can do. 
// Almost all activities interact with the user, so the Activity class takes care of creating a window for you in which you can place your UI with setContentView(View)
public class MainActivity extends AppCompatActivity // one activity in an app is specified as the main activity, which is the first screen to appear when the user launches the app. 
{
    private MyView myView;

    /*onCreate(Bundle) is where you initialize your activity. 
    Most importantly, here you will usually call setContentView(int) with a layout resource defining your UI, and using findViewById(int) 
    to retrieve the widgets in that UI that you need to interact with programmatically.*/
    @Override
    protected void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_main);

        // get rid of title bar
        this.supportRequestWindowFeature(Window.FEATURE_NO_TITLE); // request to window; this: current activity
                                                                   // Enable extended support library window features.
                                                                   // FEATURE_NO_TITLE: (int) Flag for the "no title" feature, turning off the title at the top of the screen.

        // get rid of navigation bar
        this.getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION); // Request that the visibility of the status bar or other screen/window decorations be changed.
                                                                                                    // getDecorView(): Retrieve the top-level window decor view (containing the standard window frame/decorations and the client's content inside of that)
        // make fullscreen
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);  // getWindow(): Retrieve the current Window for the activity. 
                                                                                                                           // setFlags(int flags, int mask): Set the flags of the window, as per the WindowManager.LayoutParams flags.
                                                                                                                           // FLAG_FULLSCREEN: Window flag: hide all screen decorations (such as the status bar) while this window is displayed.
                                                                                                                        
        // do forced landscape orientation
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // set background color
        this.getWindow().getDecorView().setBackgroundColor(Color.BLACK);

        // define our own view
        myView=new MyView(this);    // this: current activity

        // set this view as our main view
        setContentView(myView);
    }

    @Override
    protected void onPause()    // onPause() is where you deal with the user pausing active interaction with the activity. 
                                // Any changes made by the user should at this point be committed (usually to the ContentProvider holding the data). 
                                // In this state the activity is still visible on screen. For example when the device goes to sleep, when an activity result is delivered, when a new intent is delivered
    {
        super.onPause();
    } 

    @Override
    protected void onResume()   // for your activity to start interacting with the user. 
                                // This is an indicator that the activity became active and ready to receive input. It is on top of an activity stack and visible to user.
    {
        super.onResume();
    } 
}
