package com.rtr.BlueScreen;

import android.content.Context; // for Context class (drawing context to MyView constructor)
import android.view.Gravity;    // for Gravity class (text in center)
import android.graphics.Color;  // for Color class (text color)
import android.view.MotionEvent;    // for MotionEvent class
import android.view.GestureDetector;    // for GestureDetector class
import android.view.GestureDetector.OnGestureListener;    // for OnGestureListener class
import android.view.GestureDetector.OnDoubleTapListener;    // for OnDoubleTapListener class (specially for double tap)
// OpenGL packages
import android.opengl.GLSurfaceView;    // a view whose surface supports OpenGL
import android.opengl.GLES32;   // OpenGL ES whose version is 3.2 (Current)
import javax.microedition.khronos.opengles.GL10;    // for basic features of OpenGL ES
                                                    // javax: java extension
                                                    // microedition: supports J2ME(Java 2 Micro Edition)
                                                    // khronos: wrote OpenGL specification
                                                    // GL10: OpenGL's 10th extension (came from desktop version 1.0)
import javax.microedition.khronos.egl.EGLConfig;    // EGL(Embedded Graphics library) needed for conversion of above packages' functionality to native (C, since Android is Androdised "Linux")

// our class
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener // GLSurfaceView since we want to show OpenGL
                                                                                                                    // GLSurfaceView.Renderer: inner class
{
    private final Context context;  // final: like 'const' in C
    private GestureDetector gestureDetector;

    // constructor
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context=drawingContext;
        // call 3 functions of GLSurfaceView class
        setEGLContextClientVersion(3);  // tell egl that client wants version 3.x; it gives nearest
        setRenderer(this);  // who is your renderer; this since you are only writing onDrawFrame()
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // repaint when render mode is dirty. The rect that is to be repainted is called dirty.
                                                            // When renderMode is RENDERMODE_WHEN_DIRTY, the renderer only rendered when the surface is created, or when requestRender() is called

        gestureDetector=new GestureDetector(context,this,null,false);   // context: state
                                                                        // this: who is going to listen and handle events(handler)
                                                                        // null: no other class is going to handle events;
                                                                        // false: fixed 

        gestureDetector.setOnDoubleTapListener(this);   // this: handler; specially for double tap
    }

    // handling 'onTouchEvent' is the most important because it triggers all gesture and tap events
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // code
        int eventaction=event.getAction();  // not needed for OpenGL(needed for combination of NDK and SDK; eg.for keyboard touch)
        if(!gestureDetector.onTouchEvent(event))    // ==false; if user's touch detected by this class's GestureDetector cannot be handled by this class, send it to super class (like DefWindowProc())
            super.onTouchEvent(event);

        return(true);   // if touch event handled by this class, return true to GestureDetector class
    }

    // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)   // of double tap latency
    {
        return(true);
    }

    // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)  // can consist of multiple double taps(eg. swipe)
    {
        // do not write any code here because already written in 'OnDoubleTap'
        return(true);
    }

     // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)  // like WM_LBUTTONDOWN
    {
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)    // for single tap, double tap, scroll,etc. (all)
    {
        // do not write any code here because already written in 'onSingleTapConfirmed'
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)    // swipe
    {
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
    }

    // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY)  // takes two end points of scroll and finds direction and amount of scroll
    {
        System.exit(0);
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
        return(true); 
    }

    // implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) // prototype not decided by us
    {
        String version=gl.glGetString(GL10.GL_VERSION); // take GL version

        // print it
        System.out.println("RTR: "+version);

        Initialize();
    }

     @Override
    public void onSurfaceChanged(GL10 unused,int width, int height) // unused: the GL interface. 'unused' says that try not to use GL10 functions here
    {
        Resize(width,height);
    }

     @Override
    public void onDrawFrame(GL10 unused)
    {
        Display();
    }

    // our custom methods
    private void Initialize()   // private since our custom method; cannot write 'void' as parameter in Java 
    {
        GLES32.glClearColor(0.0f,0.0f,1.0f,1.0f);
    }

    private void Resize(int width,int height)
    {
        GLES32.glViewport(0,0,width,height);
    }

    private void Display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);

        requestRender();    // request to render; ~glSwapBuffers()
    }
}
