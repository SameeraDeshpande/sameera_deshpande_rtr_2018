package com.rtr.tweaked_smiley;

public class GLESMacros     // Java does not have enum
{
    public static final int AMC_ATTRIBUTE_POSITION=0;   // public: accessible to all classes; static: no need of object; final: will not change
    public static final int AMC_ATTRIBUTE_COLOR=1;
    public static final int AMC_ATTRIBUTE_NORMAL=2;
    public static final int AMC_ATTRIBUTE_TEXCOORD0=3;
}
