package com.rtr.roboticArm;

import android.content.Context; // for Context class (drawing context to MyView constructor)
import android.view.MotionEvent;    // for MotionEvent class
import android.view.GestureDetector;    // for GestureDetector class
import android.view.GestureDetector.OnGestureListener;    // for OnGestureListener class
import android.view.GestureDetector.OnDoubleTapListener;    // for OnDoubleTapListener class (specially for double tap)
// OpenGL classes
import android.opengl.GLSurfaceView;    // a view whose surface supports OpenGL
import android.opengl.GLES32;   // OpenGL ES whose version is 3.2 (Current)
import javax.microedition.khronos.opengles.GL10;    // for basic features of OpenGL ES
                                                    // javax: java extension
                                                    // microedition: supports J2ME(Java 2 Micro Edition)
                                                    // khronos: wrote OpenGL specification
                                                    // GL10: OpenGL's 10th extension (came from desktop version 1.0)
import javax.microedition.khronos.egl.EGLConfig;    // EGL(Embedded Graphics library) needed for conversion of above packages' functionality to native (C, since Android is Androdised "Linux")

// classes for OpenGL buffers (we will create them)
import java.nio.ByteBuffer;     // nio: non-blocking i/o or native i/o (since this library goes to native)
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;    // for elementsBuffer of sphere

import android.opengl.Matrix;   // class for matrix math

// our class
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener // GLSurfaceView since we want to show OpenGL
                                                                                                                    // GLSurfaceView.Renderer: inner class
{
    private final Context context;  // final: like 'const' in C
    private GestureDetector gestureDetector;

    // 7 more variables
    private int vertexShaderObject;     // there is no uint in java and hence no GLuint
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_sphere = new int[1];
    private int[] vbo_sphere_position = new int[1];
    private int[] vbo_sphere_normal = new int[1];
    private int[] vbo_sphere_element = new int[1];
    private int numVertices;
    private int numElements;
    
    private int mUniform;
    private int vUniform;
    private int pUniform;

    private float[] perspectiveProjectionMatrix=new float[16];     // 4*4 matrix

    private float shoulder = 0, elbow = 0;

    // constructor
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context=drawingContext;
        // call 3 functions of GLSurfaceView class
        setEGLContextClientVersion(3);  // tell egl that client wants version 3.x; it gives nearest
        setRenderer(this);  // who is your renderer; this since you are only writing onDrawFrame()
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // repaint when render mode is dirty. The rect that is to be repainted is called dirty.
                                                            // When renderMode is RENDERMODE_WHEN_DIRTY, the renderer only rendered when the surface is created, or when requestRender() is called

        gestureDetector=new GestureDetector(context,this,null,false);   // context: state
                                                                        // this: who is going to listen and handle events(handler)
                                                                        // null: no other class is going to handle events;
                                                                        // false: fixed 

        gestureDetector.setOnDoubleTapListener(this);   // this: handler; specially for double tap
    }

    // handling 'onTouchEvent' is the most important because it triggers all gesture and tap events
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // code
        int eventaction=event.getAction();  // not needed for OpenGL(needed for combination of NDK and SDK; eg.for keyboard touch)
        if(!gestureDetector.onTouchEvent(event))    // ==false; if user's touch detected by this class's GestureDetector cannot be handled by this class, send it to super class (like DefWindowProc())
            super.onTouchEvent(event);

        return(true);   // if touch event handled by this class, return true to GestureDetector class
    }

    // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)   // of double tap latency
    {
        return(true);
    }

    // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)  // can consist of multiple double taps(eg. swipe)
    {
        // do not write any code here because already written in 'OnDoubleTap'
        return(true);
    }

     // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)  // like WM_LBUTTONDOWN
    {
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)    // for single tap, double tap, scroll,etc. (all)
    {
        // do not write any code here because already written in 'onSingleTapConfirmed'
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)    // swipe
    {
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
    }

    // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY)  // takes two end points of scroll and finds direction and amount of scroll
    {
        Uninitialize();
        System.exit(0);	// successful exit status
        return(true); 
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
        return(true); 
    }

    // implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) // prototype not decided by us
    {
        String gl_version=gl.glGetString(GL10.GL_VERSION); // take GL version
        String gles32_version=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);   // OGL shading language version

        // print it
        System.out.println("\nRTR: GL Version: "+ gl_version);
        System.out.println("\nRTR: GLES32 Version: "+ gles32_version);

        Initialize();
    }

     @Override
    public void onSurfaceChanged(GL10 unused,int width, int height) // unused: the GL interface. 'unused' says that try not to use GL10 functions here
    {
        Resize(width,height);
    }

     @Override
    public void onDrawFrame(GL10 unused)
    {
        Update();
        Display();
    }

    // our custom methods
    private void Initialize()   // private since our custom method; cannot write 'void' as parameter in Java 
    {
        // code till wglMakeCurrent() already done due to GLSurfaceView's setRenderer(). (done natively in NDK using eglMakeCurrent())
        // for every OpenGL function/macro/enum, add 'GLES32.' before it

        // vertex shader
        vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        final String vertexShaderSourceCode=String.format(
        "#version 320 es" +
		"\n" +
		"in vec4 vPosition;" +
        "in vec4 vColor;" +
		"uniform mat4 u_m_matrix;" +
        "uniform mat4 u_v_matrix;" +
		"uniform mat4 u_p_matrix;" +
        "out vec4 out_color;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
        "out_color = vColor;" +
		"}"
        );      // written in Graphics Library Shading/Shader Language (GLSL)
				// 320: OpenGL version support*100 (3.2*100); es: embedded system
				// '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
                // ****VERY IMPORTANT****: uniform mediump int u_isSingleTapMade; program does not run if int not made mediump. Precision is imp. in ES. VS by def has highp for all. But int does not require highp(precision is related to battery life)
				// eye coordinates are not related to any projection, hence only mv
				// uniform mat4 u_mv_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// mat3 normal_matrix = mat3(u_mv_matrix): typecasting into mat3 gives you the normal matrix i.e. mat3(transpose(inverse(u_mv_matrix))); It gives first upper 3*3 matrix of the 4*4 matrix. transpose(), inverse() are shader functions
				// t_norm: transformed normal. normalize() is a shader function
				// vec3(u_light_position - eye_coordinates.xyz)= typecasting vec4 into vec3; eye_coordinates.xyz: taking x,y,z from x,y,z,w
				// actually, henceforth, do diffuse_color = u_ld * u_kd * max(dot(light_direction,t_norm),0.0): dot(): function for dot product; max(): to avoid sending -ve values to shader
				// gl_Position: in-built variable of shader	

	    // give above source code to the vertex shader object
        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);   // internally calls NDK's function which also has 4 para. like Windows' fn

        // compile the vertex shader
        GLES32.glCompileShader(vertexShaderObject);

        // compile time error checking
        int[] iShaderCompileStatus=new int[1];      // array of 1 since we will need addr of iShaderCompileStatus
        int[] iInfoLogLength=new int[1];            // array of 1 since we will need addr of iInfoLogLength
        String szInfoLog=null;

       GLES32.glGetShaderiv(	        // iv= integer vector
		vertexShaderObject,	            // the shader object to be queried
		GLES32.GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		iShaderCompileStatus,	        // empty; returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
        0                               // give address of 0th element; whenever passing address of array, pass the element no. in which you want the information filled
	    );	// returns a parameter from a shader object

        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)	// error present
        {
            // check if the compiler has any info. about the error
            GLES32.glGetShaderiv(vertexShaderObject,
                GLES32.GL_INFO_LOG_LENGTH,
                iInfoLogLength,     // returns the number of characters in the information log for shader
                0);

            if (iInfoLogLength[0] > 0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(vertexShaderObject);	// the shader object whose information log is to be queried.				
                                                                            // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
                                                                            // When a shader object is created, its information log will be a string of length 0.
                System.out.println("\nRTR: Vertex Shader Compilation Log : "+szInfoLog);
                Uninitialize();	
                System.exit(0);	// 0 since error not of OS
            }
        }

        // fragment shader
        fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        final String fragmentShaderSourceCode=String.format(
        "#version 320 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 out_color;" +
        "out vec4 fragColor;" +
        "void main(void)" +
        "{" +
        "fragColor = out_color;" +	
		"}"
        );      // written in Graphics Library Shading/Shader Language (GLSL)
                // 320: OpenGL version support*100 (3.2*100); core: core profile
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
                // precision highp float: give high precision to float.(error if not specified) Use mediump for int(def). VS by def has highp for all. Max processing happens in FS than VS as FS gives ultimate color.
                // precision needed only in ES as there are many math units
                // vec4: function/macro/constructor (of vmath?)
                // frag_color = vec4(diffuse_color,1.0): 1.0 just for converting vec3 to vec4
				// actually, checking the state of u_isLKeyPressed again is not a good code here. Instead, we should pass a variable from VS to FS for that state. But we are checking it here to explain the concept that uniforms(u_isLKeyPressed) are global to shaders.
				// frag_color = vec4(1.0,1.0,1.0,1.0): will give white color to fragment; don't write 'f' in shaders (if class)  

        // give above source code to the fragment shader object
        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);   // internally calls NDK's function which also has 4 para. like Windows' fn

        // compile the fragment shader
        GLES32.glCompileShader(fragmentShaderObject);

        // compile time error checking
        iShaderCompileStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        GLES32.glGetShaderiv(	        // iv= integer vector
            fragmentShaderObject,	        // the shader object to be queried
            GLES32.GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
            iShaderCompileStatus,	        // empty; returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
            0                               // give address of 0th element; whenever passing address of array, pass the element no. in which you want the information filled
            );	// returns a parameter from a shader object

            if (iShaderCompileStatus[0] == GLES32.GL_FALSE)	// error present
            {
                // check if the compiler has any info. about the error
                GLES32.glGetShaderiv(fragmentShaderObject,
                    GLES32.GL_INFO_LOG_LENGTH,
                    iInfoLogLength,     // returns the number of characters in the information log for shader
                    0);	

                if (iInfoLogLength[0] > 0)
                {
                    szInfoLog=GLES32.glGetShaderInfoLog(fragmentShaderObject);	// the shader object whose information log is to be queried.				
                                                                                // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
                                                                                // When a shader object is created, its information log will be a string of length 0.
                    System.out.println("\nRTR: Fragment Shader Compilation Log : "+szInfoLog);
                    Uninitialize();	
                    System.exit(0);	// 0 since error not of OS
                }
            }

        shaderProgramObject=GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);  // 1st para: the program object to which a shader object will be attached
                                                                        // 2nd para: the shader object that is to be attached.

        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);              

        // pre-linking binding to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
        GLES32.glBindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
            GLESMacros.AMC_ATTRIBUTE_POSITION,				// the index of the generic vertex attribute to be bound.
            "vPosition"								        // a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
        );	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable
            // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition 

        GLES32.glBindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
            GLESMacros.AMC_ATTRIBUTE_COLOR,				// the index of the generic vertex attribute to be bound.
            "vColor"								        // a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
        );

        // link the shader program to your program
        GLES32.glLinkProgram(shaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on                                                
        
        // link time error checking
        int iProgramLinkStatus[]=new int[1];      // array of 1 since we will need addr of iProgramLinkStatus
        iInfoLogLength[0]=0;          
        szInfoLog=null;

        GLES32.glGetProgramiv(	        // iv= integer vector
            shaderProgramObject,	    // the shader object to be queried
            GLES32.GL_LINK_STATUS,		// what(object parameter) is to be queried	
            iProgramLinkStatus,	        // empty; returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
            0                           // give address of 0th element; whenever passing address of array, pass the element no. in which you want the information filled
            );	// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
				// When a program object is created, its information log will be a string of length 0.

        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)	// error present
        {
            // check if the linker has any info. about the error
            GLES32.glGetShaderiv(shaderProgramObject,
                GLES32.GL_INFO_LOG_LENGTH,
                iInfoLogLength,     // returns the number of characters in the information log for shader
                0);	

            if (iInfoLogLength[0] > 0)
            {
                szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);	// the shader object whose information log is to be queried.				
                                                                            // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
                                                                            // When a shader object is created, its information log will be a string of length 0.
                System.out.println("\nRTR: Shader Program Link Log : "+szInfoLog);
                Uninitialize();	
                System.exit(0);	// 0 since error not of OS
            }
        }

        // post-linking retrieving uniform locations (uniforms are global to shaders)
	    mUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_m_matrix");	// preparation of data transfer from CPU to GPU (binding)
                                                                                        // u_mvp_matrix: of GPU; mvpUniform: of CPU
                                                                                        // telling it to take location of uniform u_mvp_matrix and give in mvpUniform

        vUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_v_matrix");
        pUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_p_matrix");

        // fill sphere data in arrays
        Sphere sphere=new Sphere();
        float sphere_vertices[]=new float[1146];
        float sphere_normals[]=new float[1146];
        float sphere_textures[]=new float[764];
        short sphere_elements[]=new short[2280];
        sphere.getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = sphere.getNumberOfSphereVertices();
        numElements = sphere.getNumberOfSphereElements();

	    // sphere
        // vao
        GLES32.glGenVertexArrays(1,vao_sphere,0);
        GLES32.glBindVertexArray(vao_sphere[0]);
        
        // position vbo
        GLES32.glGenBuffers(1,vbo_sphere_position,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_position[0]);
        
        ByteBuffer byteBuffer=ByteBuffer.allocateDirect(sphere_vertices.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        FloatBuffer verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_vertices);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_vertices.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // normal vbo
        GLES32.glGenBuffers(1,vbo_sphere_normal,0);
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,vbo_sphere_normal[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_normals.length * 4);
        byteBuffer.order(ByteOrder.nativeOrder());
        verticesBuffer=byteBuffer.asFloatBuffer();
        verticesBuffer.put(sphere_normals);
        verticesBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,
                            sphere_normals.length * 4,
                            verticesBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_NORMAL,
                                     3,
                                     GLES32.GL_FLOAT,
                                     false,0,0);
        
        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_NORMAL);
        
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER,0);
        
        // element vbo
        GLES32.glGenBuffers(1,vbo_sphere_element,0);
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,vbo_sphere_element[0]);
        
        byteBuffer=ByteBuffer.allocateDirect(sphere_elements.length * 2);
        byteBuffer.order(ByteOrder.nativeOrder());
        ShortBuffer elementsBuffer=byteBuffer.asShortBuffer();
        elementsBuffer.put(sphere_elements);
        elementsBuffer.position(0);
        
        GLES32.glBufferData(GLES32.GL_ELEMENT_ARRAY_BUFFER,
                            sphere_elements.length * 2,
                            elementsBuffer,
                            GLES32.GL_STATIC_DRAW);
        
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER,0);

        GLES32.glBindVertexArray(0);

        GLES32.glEnable(GLES32.GL_DEPTH_TEST);	// to compare depth values of objects

	    GLES32.glDepthFunc(GLES32.GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
                                                // Passes if the incoming z value is less than or equal to the stored z value. 
                                                // GL_LEQUAL : GLenum

	    GLES32.glDisable(GLES32.GL_CULL_FACE);		// If enabled, cull polygons based on their winding in window coordinates

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											            // specifies clear values[0,1] used by glClear() for the color buffers

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);   	// making perspectiveProjectionMatrix an identity matrix(diagonals 1); 0: fill from 0th index  

        // no warmup call to Resize() since our application is already fullscreen                                             
    }

    private void Resize(int width,int height)
    {
        if (height == 0)
            height = 1;

	    GLES32.glViewport(0, 0, width, height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);	// parameters:
                                                                                                                // perspectiveProjectionMatrix: fill in this
                                                                                                                // 0: from 0th index
                                                                                                                // fovy- The field of view angle, in degrees, in the y - direction.
                                                                                                                // aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
                                                                                                                // zNear- The distance from the viewer to the near clipping plane(always positive).
                                                                                                                // zFar- The distance from the viewer to the far clipping plane(always positive)
    }

    private void Display()
    {
        // variable declarations
        float[][] mv_stack=new float[3][];
        int ptr = -1;

        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);

        // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	    GLES32.glUseProgram(shaderProgramObject);	// binding your OpenGL code with shader program object
										            // Specifies the handle of the program object whose executables are to be used as part of current rendering state.

        // declaration of matrices
        float[] modelMatrix=new float[16];
        float[] viewMatrix=new float[16];
        float[] translationMatrix=new float[16];
        float[] rotationMatrix=new float[16];
        float[] scaleMatrix=new float[16];

        Matrix.setIdentityM(modelMatrix, 0);

        Matrix.setIdentityM(viewMatrix, 0);
        Matrix.setLookAtM(viewMatrix, 0, 0.0f, 0.0f, 10.0f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f);    // eyex, eyey, eyez : The position of the eye point. (camera position/coordinates/vector)
                                                                                                    // centerx, centery, centerz : The position of the reference point. (where is the camera looking)
                                                                                                    // upx, upy, upz : The direction of the up vector. (where is camera's y-axis/up position)

        Matrix.setIdentityM(translationMatrix, 0);
	    Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, 0.0f);   // translationMatrix: output matrix, 0: 0th index

        // do necessary matrix multiplication (done by gluOrtho2d() in FFP)
        Matrix.multiplyMM(
          modelMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

        Matrix.setIdentityM(rotationMatrix, 0);
        Matrix.setRotateM(rotationMatrix, 0, (float)shoulder, 0.0f, 0.0f, 1.0f);   // rotationMatrix: output matrix, 0: 0th index; rotate about y axis

        // do necessary matrix multiplication (done by gluOrtho2d() in FFP)
        Matrix.multiplyMM(
          modelMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelMatrix, 0,      
          rotationMatrix, 0           // same sequence
        );

        Matrix.setIdentityM(translationMatrix, 0);
	    Matrix.translateM(translationMatrix, 0, 1.0f, 0.0f, 0.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelMatrix, 0,      
          translationMatrix, 0        // same sequence
        );

        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.scaleM(scaleMatrix, 0, 2.0f, 0.5f, 1.0f);

        Matrix.multiplyMM(
          modelMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelMatrix, 0,      
          scaleMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelMatrix,		                    // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        GLES32.glUniformMatrix4fv(vUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            viewMatrix,		                    // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        GLES32.glUniformMatrix4fv(pUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            perspectiveProjectionMatrix,		                    // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR,  0.5f, 0.35f, 0.05f);	// specify the value of a generic vertex attribute(for single color to object)

        // bind vao
        GLES32.glBindVertexArray(vao_sphere[0]);

        // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
        GLES32.glBindBuffer(GLES32.GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element[0]);

        GLES32.glDrawElements(GLES32.GL_TRIANGLES,  // what kind of primitives to render (mode)
        numElements,                                // the number of elements to be rendered (count)
        GLES32.GL_UNSIGNED_SHORT,                   // type of the values in last parameter
        0                                           // Specifies a pointer to the location where the indices are stored (indices)
        );

        // elbow
        Matrix.setIdentityM(modelMatrix, 0);

        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.translateM(translationMatrix, 0, 0.0f, 0.0f, 0.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

        Matrix.setIdentityM(rotationMatrix, 0);
	    Matrix.setRotateM(rotationMatrix, 0, (float)shoulder, 0.0f, 0.0f, 1.0f);   // rotationMatrix: output matrix, 0: 0th index; rotate about x axis

        Matrix.multiplyMM(
          modelMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelMatrix, 0,      
          rotationMatrix, 0                    // same sequence
        );

        Matrix.setIdentityM(translationMatrix, 0);
	    Matrix.translateM(translationMatrix, 0, 2.0f, 0.0f, 0.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

        Matrix.setIdentityM(rotationMatrix, 0);
	    Matrix.setRotateM(rotationMatrix, 0, (float)elbow, 0.0f, 0.0f, 1.0f);   // rotationMatrix: output matrix, 0: 0th index; rotate about x axis

        Matrix.multiplyMM(
          modelMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelMatrix, 0,      
          rotationMatrix, 0                    // same sequence
        );

        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.translateM(translationMatrix, 0, 1.0f, 0.0f, 0.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

        Matrix.setIdentityM(scaleMatrix, 0);
        Matrix.scaleM(scaleMatrix, 0, 2.0f, 0.5f, 1.0f);

        Matrix.multiplyMM(
          modelMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelMatrix, 0,      
          scaleMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        GLES32.glDrawElements(GLES32.GL_TRIANGLES,  // what kind of primitives to render (mode)
        numElements,                                // the number of elements to be rendered (count)
        GLES32.GL_UNSIGNED_SHORT,                   // type of the values in last parameter
        0                                           // Specifies a pointer to the location where the indices are stored (indices)
        );

        // unbind vao
        GLES32.glBindVertexArray(0);

        // unuse program
        GLES32.glUseProgram(0);	// unbinding your OpenGL code with shader program object
                            // If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.
        
        requestRender();    // request to render; ~glSwapBuffers() // calls onDrawFrame() (?)
    }

    private void Update()
    {
        shoulder = (shoulder + 0.4f) % 360.0f;
        elbow = (elbow + 0.4f) % 360.0f;
    }

    private void Uninitialize()
    {
        // destroy vao
        if(vao_sphere[0] != 0)
        {
            GLES32.glDeleteVertexArrays(1, vao_sphere, 0);
            vao_sphere[0]=0;
        }
        
        // destroy position vbo
        if(vbo_sphere_position[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_position, 0);
            vbo_sphere_position[0]=0;
        }
        
        // destroy normal vbo
        if(vbo_sphere_normal[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_normal, 0);
            vbo_sphere_normal[0]=0;
        }
        
        // destroy element vbo
        if(vbo_sphere_element[0] != 0)
        {
            GLES32.glDeleteBuffers(1, vbo_sphere_element, 0);
            vbo_sphere_element[0]=0;
        }

        if (shaderProgramObject!=0)
        {
            int[] shaderCount=new int[1];	// since we want addr of shaderCount
            int shaderNumber;

            GLES32.glUseProgram(shaderProgramObject);		// since unused in Display()
            
            // ask program that how many shaders are attached to it
            GLES32.glGetProgramiv(shaderProgramObject,
                GLES32.GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
                shaderCount,
                0);

            int[] shaders = new int[4 * shaderCount[0]];		// dynamic array for shaders since we don't know how many present; 4: size of int in java

            if (shaders[0]!=0)	// mem allocated
            {
                // take attached shaders into above array
                GLES32.glGetAttachedShaders(shaderProgramObject,	// the program object to be queried
                    shaderCount[0],								    // the size of the array for storing the returned object names
                    shaderCount,								    // Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
                    0,
                    shaders,								        // an array that is used to return the names of attached shader objects(empty now),
                    0
                );		// return the handles of the shader objects attached to a program object

                for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                    // detach each shader
                    GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
                                                                                        // 2nd para: Specifies the shader object to be detached.

                    // delete each detached shader
                    GLES32.glDeleteShader(shaders[shaderNumber]);
                    shaders[shaderNumber] = 0;
                }
            }
            GLES32.glUseProgram(0);

            // delete the shader program
            GLES32.glDeleteProgram(shaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
            shaderProgramObject = 0;
        }
	}
}
