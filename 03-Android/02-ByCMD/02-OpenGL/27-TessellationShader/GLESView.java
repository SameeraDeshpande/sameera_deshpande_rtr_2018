package com.rtr.tessellationShader;

import android.content.Context; // for Context class (drawing context to MyView constructor)
import android.view.MotionEvent;    // for MotionEvent class
import android.view.GestureDetector;    // for GestureDetector class
import android.view.GestureDetector.OnGestureListener;    // for OnGestureListener class
import android.view.GestureDetector.OnDoubleTapListener;    // for OnDoubleTapListener class (specially for double tap)
// OpenGL classes
import android.opengl.GLSurfaceView;    // a view whose surface supports OpenGL
import android.opengl.GLES32;   // OpenGL ES whose version is 3.2 (Current)
import javax.microedition.khronos.opengles.GL10;    // for basic features of OpenGL ES
                                                    // javax: java extension
                                                    // microedition: supports J2ME(Java 2 Micro Edition)
                                                    // khronos: wrote OpenGL specification
                                                    // GL10: OpenGL's 10th extension (came from desktop version 1.0)
import javax.microedition.khronos.egl.EGLConfig;    // EGL(Embedded Graphics library) needed for conversion of above packages' functionality to native (C, since Android is Androdised "Linux")

// classes for OpenGL buffers (we will create them)
import java.nio.ByteBuffer;     // nio: non-blocking i/o or native i/o (since this library goes to native)
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;   // class for matrix math

// our class
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener // GLSurfaceView since we want to show OpenGL
                                                                                                                    // GLSurfaceView.Renderer: inner class
{
    private final Context context;  // final: like 'const' in C
    private GestureDetector gestureDetector;

    // 7 more variables
    private int vertexShaderObject;     // there is no uint in java and hence no GLuint   
    private int tessellationControlShaderObject;
    private int tessellationEvaluationShaderObject;
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int numberOfSegmentsUniform;		// segments of one line
    private int numberOfStripsUniform;		// no. of lines (we have only 1)
    private int lineColorUniform;			// color to line will be given at runtime
    private int gNumberOfLineSegments;		// segments of one line; CPU var. holding no. of line segments based on which uniform's no. of line segments on GPU will change
    private float[] lineColor = new float[] { 1.0f,1.0f,0.0f,1.0f };
    private boolean bDoneMaxTaps=false;

    private int[] vao=new int[1];   // array of 1: adjustment since there is no address in java hence we will send name of array instead of &vao as address
    private int[] vbo_position=new int[1];

    private int mvpUniform;
    private float[] perspectiveProjectionMatrix=new float[16];     // 4*4 matrix

    // constructor
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context=drawingContext;
        // call 3 functions of GLSurfaceView class
        setEGLContextClientVersion(3);  // tell egl that client wants version 3.x; it gives nearest
        setRenderer(this);  // who is your renderer; this since you are only writing onDrawFrame()
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // repaint when render mode is dirty. The rect that is to be repainted is called dirty.
                                                            // When renderMode is RENDERMODE_WHEN_DIRTY, the renderer only rendered when the surface is created, or when requestRender() is called

        gestureDetector=new GestureDetector(context,this,null,false);   // context: state
                                                                        // this: who is going to listen and handle events(handler)
                                                                        // null: no other class is going to handle events;
                                                                        // false: fixed 

        gestureDetector.setOnDoubleTapListener(this);   // this: handler; specially for double tap
    }

    // handling 'onTouchEvent' is the most important because it triggers all gesture and tap events
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // code
        int eventaction=event.getAction();  // not needed for OpenGL(needed for combination of NDK and SDK; eg.for keyboard touch)
        if(!gestureDetector.onTouchEvent(event))    // ==false; if user's touch detected by this class's GestureDetector cannot be handled by this class, send it to super class (like DefWindowProc())
            super.onTouchEvent(event);

        return(true);   // if touch event handled by this class, return true to GestureDetector class
    }

    // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)   // of double tap latency
    {
        return(true);
    }

    // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)  // can consist of multiple double taps(eg. swipe)
    {
        // do not write any code here because already written in 'OnDoubleTap'
        return(true);
    }

     // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)  // like WM_LBUTTONDOWN
    {
        if(bDoneMaxTaps==false)
        {
            gNumberOfLineSegments++;
            // make line yellow
            lineColor[0] = 1.0f;
            lineColor[1] = 1.0f;
            lineColor[2] = 0.0f;
            if (gNumberOfLineSegments >= 50)
            {
                bDoneMaxTaps=true;
                // make line green
                lineColor[0] = 0.0f;
                lineColor[1] = 1.0f;
                lineColor[2] = 0.0f;
            }
        }
        else
        {
            gNumberOfLineSegments--;
            // make line yellow
            lineColor[0] = 1.0f;
            lineColor[1] = 1.0f;
            lineColor[2] = 0.0f;
            if (gNumberOfLineSegments == 0)
            {
                gNumberOfLineSegments = 1;
                bDoneMaxTaps=false;
                // make line red
                lineColor[0] = 1.0f;
                lineColor[1] = 0.0f;
                lineColor[2] = 0.0f;
            }
        }
        return(true);
    }

    // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)    // for single tap, double tap, scroll,etc. (all)
    {
        // do not write any code here because already written in 'onSingleTapConfirmed'
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)    // swipe
    {
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
    }

    // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY)  // takes two end points of scroll and finds direction and amount of scroll
    {
        Uninitialize();
        System.exit(0);	// successful exit status
        return(true); 
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
        return(true); 
    }

    // implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) // prototype not decided by us
    {
        String gl_version=gl.glGetString(GL10.GL_VERSION); // take GL version
        String gles32_version=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);   // OGL shading language version

        // print it
        System.out.println("\nRTR: GL Version: "+ gl_version);
        System.out.println("\nRTR: GLES32 Version: "+ gles32_version);

        Initialize();
    }

     @Override
    public void onSurfaceChanged(GL10 unused,int width, int height) // unused: the GL interface. 'unused' says that try not to use GL10 functions here
    {
        Resize(width,height);
    }

     @Override
    public void onDrawFrame(GL10 unused)
    {
        Display();
    }

    // our custom methods
    private void Initialize()   // private since our custom method; cannot write 'void' as parameter in Java 
    {
        // code till wglMakeCurrent() already done due to GLSurfaceView's setRenderer(). (done natively in NDK using eglMakeCurrent())
        // for every OpenGL function/macro/enum, add 'GLES32.' before it

        // vertex shader
        vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        final String vertexShaderSourceCode=String.format(
        "#version 320 es" +
		"\n" +
		"in vec2 vPosition;" +
		"void main(void)" +
		"{" +
		"gl_Position = vec4(vPosition, 0.0, 1.0);" +
		"}"
        );      // written in Graphics Library Shading/Shader Language (GLSL)
				// 320: OpenGL version support*100 (3.2*100); es: embedded system
				// '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
				// uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// gl_Position: in-built variable of shader

	    // give above source code to the vertex shader object
        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);   // internally calls NDK's function which also has 4 para. like Windows' fn

        // compile the vertex shader
        GLES32.glCompileShader(vertexShaderObject);

        // compile time error checking
        int[] iShaderCompileStatus=new int[1];      // array of 1 since we will need addr of iShaderCompileStatus
        int[] iInfoLogLength=new int[1];            // array of 1 since we will need addr of iInfoLogLength
        String szInfoLog=null;

       GLES32.glGetShaderiv(	        // iv= integer vector
		vertexShaderObject,	            // the shader object to be queried
		GLES32.GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		iShaderCompileStatus,	        // empty; returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
        0                               // give address of 0th element; whenever passing address of array, pass the element no. in which you want the information filled
	    );	// returns a parameter from a shader object

        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)	// error present
        {
            // check if the compiler has any info. about the error
            GLES32.glGetShaderiv(vertexShaderObject,
                GLES32.GL_INFO_LOG_LENGTH,
                iInfoLogLength,     // returns the number of characters in the information log for shader
                0);

            if (iInfoLogLength[0] > 0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(vertexShaderObject);	// the shader object whose information log is to be queried.				
                                                                            // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
                                                                            // When a shader object is created, its information log will be a string of length 0.
                System.out.println("\nRTR: Vertex Shader Compilation Log : "+szInfoLog);
                Uninitialize();	
                System.exit(0);	// 0 since error not of OS
            }
        }

        // tessellation control shader
        tessellationControlShaderObject=GLES32.glCreateShader(GLES32.GL_TESS_CONTROL_SHADER);

        final String tessellationControlShaderSourceCode=String.format(
        "#version 320 es" +
		"\n" +
        "precision mediump int;" +
		"layout(vertices=4)out;" +
		"uniform int numberOfSegments;" +
		"uniform int numberOfStrips;" +
		"void main(void)" +
		"{" +
		"gl_out[gl_InvocationID].gl_Position = gl_in[gl_InvocationID].gl_Position;" +
		"gl_TessLevelOuter[0] = float(numberOfStrips);" +
		"gl_TessLevelOuter[1] = float(numberOfSegments);" +
		"}"
        );     // TS takes o/p from VS in a structure called gl_PerVertex. 
				// From this, it creates an array called gl_in[] of type gl_PerVertex.
				// length of array decided by 'layout' attribute
				// TCS creates gl_out[] from above array and passes further to tessellator
				// layout(vertices=4)out: this shader is going to out a patch of 4(we decide) vertices (TS works per patch and patch means an ordered group of vertices)
				// gl_InvocationID acts as counter variable and holds 0,1,2,3 (4 vertices)
				// gl_TessLevelOuter: segmentation of outer border of a mesh

	    // give above source code to the vertex shader object
        GLES32.glShaderSource(tessellationControlShaderObject,tessellationControlShaderSourceCode);   // internally calls NDK's function which also has 4 para. like Windows' fn

        // compile the vertex shader
        GLES32.glCompileShader(tessellationControlShaderObject);

        // compile time error checking
        iShaderCompileStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        GLES32.glGetShaderiv(	        // iv= integer vector
		tessellationControlShaderObject,	            // the shader object to be queried
		GLES32.GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		iShaderCompileStatus,	        // empty; returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
        0                               // give address of 0th element; whenever passing address of array, pass the element no. in which you want the information filled
	    );	// returns a parameter from a shader object

        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)	// error present
        {
            // check if the compiler has any info. about the error
            GLES32.glGetShaderiv(tessellationControlShaderObject,
                GLES32.GL_INFO_LOG_LENGTH,
                iInfoLogLength,     // returns the number of characters in the information log for shader
                0);

            if (iInfoLogLength[0] > 0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(tessellationControlShaderObject);	// the shader object whose information log is to be queried.				
                                                                            // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
                                                                            // When a shader object is created, its information log will be a string of length 0.
                System.out.println("\nRTR: Tessellation Control Shader Compilation Log : "+szInfoLog);
                Uninitialize();	
                System.exit(0);	// 0 since error not of OS
            }
        }

        // tessellation evaluation shader
        tessellationEvaluationShaderObject=GLES32.glCreateShader(GLES32.GL_TESS_EVALUATION_SHADER);

        final String tesselationEvaluationShaderSourceCode=String.format(
        "#version 320 es" +
		"\n" +
		"layout(isolines)in;" +
		"uniform mat4 u_mvp_matrix;" +
		"void main(void)" +
		"{" +
		"float u = gl_TessCoord.x;" +
		"vec3 p0 = gl_in[0].gl_Position.xyz;" +
		"vec3 p1 = gl_in[1].gl_Position.xyz;" +
		"vec3 p2 = gl_in[2].gl_Position.xyz;" +
		"vec3 p3 = gl_in[3].gl_Position.xyz;" +
		"float u1 = (1.0 - u);" +
		"float u2 = u * u;" +
		"float b3 = u2 * u;" +
		"float b2 = 3.0 * u2 * u1;" +
		"float b1 = 3.0 * u * u1 * u1;" +
		"float b0 = u1 * u1 * u1;" +
		"vec3 p = p0*b0 + p1*b1 + p2*b2 +p3*b3;" +
		"gl_Position = u_mvp_matrix * vec4(p,1.0);" +
		"}"
        );      // layout(isolines)in: create isolines from incoming primitives
				// gl_in given by tessellator
				// p0,p1,p2,p3: for primitives (4 since TCS had 4 vertices as out
				// u1,u2: for vertices (?)
				// b0,b1,b2,b3: for bezier (control points)

	    // give above source code to the vertex shader object
        GLES32.glShaderSource(tessellationEvaluationShaderObject,tesselationEvaluationShaderSourceCode);   // internally calls NDK's function which also has 4 para. like Windows' fn

        // compile the vertex shader
        GLES32.glCompileShader(tessellationEvaluationShaderObject);

        // compile time error checking
        iShaderCompileStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        GLES32.glGetShaderiv(	        // iv= integer vector
		tessellationEvaluationShaderObject,	            // the shader object to be queried
		GLES32.GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		iShaderCompileStatus,	        // empty; returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
        0                               // give address of 0th element; whenever passing address of array, pass the element no. in which you want the information filled
	    );	// returns a parameter from a shader object

        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)	// error present
        {
            // check if the compiler has any info. about the error
            GLES32.glGetShaderiv(tessellationEvaluationShaderObject,
                GLES32.GL_INFO_LOG_LENGTH,
                iInfoLogLength,     // returns the number of characters in the information log for shader
                0);

            if (iInfoLogLength[0] > 0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(tessellationEvaluationShaderObject);	// the shader object whose information log is to be queried.				
                                                                            // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
                                                                            // When a shader object is created, its information log will be a string of length 0.
                System.out.println("\nRTR: Tessellation Evaluation Shader Compilation Log : "+szInfoLog);
                Uninitialize();	
                System.exit(0);	// 0 since error not of OS
            }
        }

        // fragment shader
        fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        final String fragmentShaderSourceCode=String.format(
        "#version 320 es" +
        "\n" +
        "precision highp float;" +
        "uniform vec4 lineColor;" +
		"out vec4 FragColor;" +
		"void main(void)" +
		"{" +
		"FragColor = lineColor;" +
        "}"
        );      // written in Graphics Library Shading/Shader Language (GLSL)
                // 320: OpenGL version support*100 (3.2*100); core: core profile
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
                // precision highp float: give high precision to float. Use mediump for int(def). VS by def has highp for all. Max processing happens in FS than VS as it gives ultimate color.
                // precision needed only in ES as there are many math units
                // vec4: function/macro/constructor (of vmath?)
                // FragColor = vec4(1.0,1.0,0.0,0.0): will give yellow color to fragment; don't write 'f' in shaders (if class) 

        // give above source code to the fragment shader object
        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);   // internally calls NDK's function which also has 4 para. like Windows' fn

        // compile the fragment shader
        GLES32.glCompileShader(fragmentShaderObject);

        // compile time error checking
        iShaderCompileStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        GLES32.glGetShaderiv(	        // iv= integer vector
            fragmentShaderObject,	        // the shader object to be queried
            GLES32.GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
            iShaderCompileStatus,	        // empty; returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
            0                               // give address of 0th element; whenever passing address of array, pass the element no. in which you want the information filled
            );	// returns a parameter from a shader object

            if (iShaderCompileStatus[0] == GLES32.GL_FALSE)	// error present
            {
                // check if the compiler has any info. about the error
                GLES32.glGetShaderiv(fragmentShaderObject,
                    GLES32.GL_INFO_LOG_LENGTH,
                    iInfoLogLength,     // returns the number of characters in the information log for shader
                    0);	

                if (iInfoLogLength[0] > 0)
                {
                    szInfoLog=GLES32.glGetShaderInfoLog(fragmentShaderObject);	// the shader object whose information log is to be queried.				
                                                                                // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
                                                                                // When a shader object is created, its information log will be a string of length 0.
                    System.out.println("\nRTR: Fragment Shader Compilation Log : "+szInfoLog);
                    Uninitialize();	
                    System.exit(0);	// 0 since error not of OS
                }
            }

        shaderProgramObject=GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);  // 1st para: the program object to which a shader object will be attached
                                                                        // 2nd para: the shader object that is to be attached.

        GLES32.glAttachShader(shaderProgramObject,tessellationControlShaderObject);

        GLES32.glAttachShader(shaderProgramObject,tessellationEvaluationShaderObject);

        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);              

        // pre-linking binding to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
        GLES32.glBindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
            GLESMacros.AMC_ATTRIBUTE_POSITION,				// the index of the generic vertex attribute to be bound.
            "vPosition"								        // a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
        );	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable
            // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition 

        // link the shader program to your program
        GLES32.glLinkProgram(shaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on                                                
        
        // link time error checking
        int iProgramLinkStatus[]=new int[1];      // array of 1 since we will need addr of iProgramLinkStatus
        iInfoLogLength[0]=0;          
        szInfoLog=null;

        GLES32.glGetProgramiv(	        // iv= integer vector
            shaderProgramObject,	    // the shader object to be queried
            GLES32.GL_LINK_STATUS,		// what(object parameter) is to be queried	
            iProgramLinkStatus,	        // empty; returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
            0                           // give address of 0th element; whenever passing address of array, pass the element no. in which you want the information filled
            );	// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
				// When a program object is created, its information log will be a string of length 0.

        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)	// error present
        {
            // check if the linker has any info. about the error
            GLES32.glGetShaderiv(shaderProgramObject,
                GLES32.GL_INFO_LOG_LENGTH,
                iInfoLogLength,     // returns the number of characters in the information log for shader
                0);	

            if (iInfoLogLength[0] > 0)
            {
                szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);	// the shader object whose information log is to be queried.				
                                                                            // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
                                                                            // When a shader object is created, its information log will be a string of length 0.
                System.out.println("\nRTR: Shader Program Link Log : "+szInfoLog);
                Uninitialize();	
                System.exit(0);	// 0 since error not of OS
            }
        }

        // post-linking retrieving uniform locations (uniforms are global to shaders)
	    mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");	// preparation of data transfer from CPU to GPU (binding)
                                                                                        // u_mvp_matrix: of GPU; mvpUniform: of CPU
                                                                                        // telling it to take location of uniform u_mvp_matrix and give in mvpUniform
        numberOfSegmentsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "numberOfSegments");
        numberOfStripsUniform = GLES32.glGetUniformLocation(shaderProgramObject, "numberOfStrips");
        lineColorUniform = GLES32.glGetUniformLocation(shaderProgramObject, "lineColor");

        // fill line vertices in array (this was in Display() in FFP)
        final float lineVertices[] = new float[] { 
            -1.0f,-1.0f,	// x,y
            -0.5f,1.0f,		// x,y
            0.5f,-1.0f,	// x,y
            1.0f,1.0f		// x,y
        };	 // since written in Initialize() only, coords will not get set everytime in Display(). Hence, fast speed.

	    // 9 lines
        // create vao
        GLES32.glGenVertexArrays(1, vao, 0);    // generate vertex array object names
                                                      // 1: Specifies the number of vertex array object names to generate
                                                      // vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
                                                      // 0: want in 0th index of vao array

		// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()
        
        // bind vao
        GLES32.glBindVertexArray(vao[0]);

        // triangle position
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_position, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(lineVertices.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        positionBuffer.put(lineVertices);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        positionBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		lineVertices.length * 4,		            // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		positionBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		2,											                    // number of components per generic vertex attribute; x,y,z for position
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                        // enables vPosition

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

	    GLES32.glBindVertexArray(0);		// unbind vao

        // same above steps for C,T,N if any

	    GLES32.glLineWidth(3.0f);

	    gNumberOfLineSegments = 1;

        GLES32.glEnable(GLES32.GL_DEPTH_TEST);	// to compare depth values of objects

	    GLES32.glDepthFunc(GLES32.GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
                                                // Passes if the incoming z value is less than or equal to the stored z value. 
                                                // GL_LEQUAL : GLenum

	    GLES32.glDisable(GLES32.GL_CULL_FACE);		// If enabled, cull polygons based on their winding in window coordinates

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											            // specifies clear values[0,1] used by glClear() for the color buffers

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);   	// making orthographicProjectionMatrix an identity matrix(diagonals 1); 0: fill from 0th index  

        // no warmup call to Resize() since our application is already fullscreen                                             
    }

    private void Resize(int width,int height)
    {
        if (height == 0)
            height = 1;

	    GLES32.glViewport(0, 0, width, height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);	// parameters:
                                                                                                                // perspectiveProjectionMatrix: fill in this
                                                                                                                // 0: from 0th index
                                                                                                                // fovy- The field of view angle, in degrees, in the y - direction.
                                                                                                                // aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
                                                                                                                // zNear- The distance from the viewer to the near clipping plane(always positive).
                                                                                                                // zFar- The distance from the viewer to the far clipping plane(always positive)
    }

    private void Display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);

        // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	    GLES32.glUseProgram(shaderProgramObject);	// binding your OpenGL code with shader program object
										            // Specifies the handle of the program object whose executables are to be used as part of current rendering state.

        // 4 CPU steps
        // declaration of matrices
        float[] modelViewMatrix=new float[16];
        float[] modelViewProjectionMatrix=new float[16];

        // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        // do necessary transformation (T,S,R)
	    Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -3.0f);   // modelViewMatrix: output matrix, 0: 0th index

	    // do necessary matrix multiplication (done by gluOrtho2d() in FFP)
        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        GLES32.glUniform1i(numberOfSegmentsUniform, gNumberOfLineSegments);
        GLES32.glUniform1i(numberOfStripsUniform, 1);
        GLES32.glUniform4fv(lineColorUniform, 1, lineColor, 0);

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        // IMP: decide no. of vertices forming a patch
	    GLES32.glPatchParameteri(GLES32.GL_PATCH_VERTICES, 4);

        // draw the necessary scene!
        GLES32.glDrawArrays(GLES32.GL_PATCHES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            0,							// array position of your 9-member array to start with (imp in inter-leaved)
            4							// how many vertices to draw
        );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                // Arrays since multiple primitives(P,C,N,T) can be drawn

        // unbind vao
        GLES32.glBindVertexArray(0);

        // unuse program
        GLES32.glUseProgram(0);	// unbinding your OpenGL code with shader program object
                            // If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.
        
        requestRender();    // request to render; ~glSwapBuffers() // calls onDrawFrame() (?)
    }

    private void Uninitialize()
    {
        // opposite sequence of vao and vbo also ok
        if (vbo_position[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_position, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_position[0] = 0;
        }

        if (vao[0]!=0)
        {
            GLES32.glDeleteVertexArrays(1, vao, 0);		// delete vertex array objects
            vao[0] = 0;
        }

        if (shaderProgramObject!=0)
        {
            int[] shaderCount=new int[1];	// since we want addr of shaderCount
            int shaderNumber;

            GLES32.glUseProgram(shaderProgramObject);		// since unused in Display()
            
            // ask program that how many shaders are attached to it
            GLES32.glGetProgramiv(shaderProgramObject,
                GLES32.GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
                shaderCount,
                0);

            int[] shaders = new int[4 * shaderCount[0]];		// dynamic array for shaders since we don't know how many present; 4: size of int in java

            if (shaders[0]!=0)	// mem allocated
            {
                // take attached shaders into above array
                GLES32.glGetAttachedShaders(shaderProgramObject,	// the program object to be queried
                    shaderCount[0],								    // the size of the array for storing the returned object names
                    shaderCount,								    // Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
                    0,
                    shaders,								        // an array that is used to return the names of attached shader objects(empty now),
                    0
                );		// return the handles of the shader objects attached to a program object

                for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                    // detach each shader
                    GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
                                                                                        // 2nd para: Specifies the shader object to be detached.

                    // delete each detached shader
                    GLES32.glDeleteShader(shaders[shaderNumber]);
                    shaders[shaderNumber] = 0;
                }
            }
            GLES32.glUseProgram(0);

            // delete the shader program
            GLES32.glDeleteProgram(shaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
            shaderProgramObject = 0;
        }
	}
}
