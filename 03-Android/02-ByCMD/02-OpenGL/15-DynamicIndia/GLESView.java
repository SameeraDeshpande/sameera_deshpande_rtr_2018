package com.rtr.dynamicIndia;

import android.content.Context; // for Context class (drawing context to MyView constructor)
import android.view.MotionEvent;    // for MotionEvent class
import android.view.GestureDetector;    // for GestureDetector class
import android.view.GestureDetector.OnGestureListener;    // for OnGestureListener class
import android.view.GestureDetector.OnDoubleTapListener;    // for OnDoubleTapListener class (specially for double tap)
// OpenGL classes
import android.opengl.GLSurfaceView;    // a view whose surface supports OpenGL
import android.opengl.GLES32;   // OpenGL ES whose version is 3.2 (Current)
import javax.microedition.khronos.opengles.GL10;    // for basic features of OpenGL ES
                                                    // javax: java extension
                                                    // microedition: supports J2ME(Java 2 Micro Edition)
                                                    // khronos: wrote OpenGL specification
                                                    // GL10: OpenGL's 10th extension (came from desktop version 1.0f)
import javax.microedition.khronos.egl.EGLConfig;    // EGL(Embedded Graphics library) needed for conversion of above packages' functionality to native (C, since Android is Androdised "Linux")
import java.lang.Math;

// classes for OpenGL buffers (we will create them)
import java.nio.ByteBuffer;     // nio: non-blocking i/o or native i/o (since this library goes to native)
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

import android.opengl.Matrix;   // class for matrix math

// our class
public class GLESView extends GLSurfaceView implements GLSurfaceView.Renderer,OnGestureListener,OnDoubleTapListener // GLSurfaceView since we want to show OpenGL
                                                                                                                    // GLSurfaceView.Renderer: inner class
{
    private final Context context;  // final: like 'const' in C
    private GestureDetector gestureDetector;

    // 7 more variables
    private int vertexShaderObject;     // there is no uint in java and hence no GLuint
    private int fragmentShaderObject;
    private int shaderProgramObject;

    private int[] vao_I=new int[1];   // array of 1: adjustment since there is no address in java hence we will send name of array instead of &vao as address
    private int[] vao_N=new int[1];
    private int[] vao_D=new int[1];
    private int[] vao_A=new int[1];
    private int[] vao_A_band=new int[1];
    private int[] vao_plane=new int[1];
    private int[] vao_smoke=new int[1];

    private int[] vbo_position_I=new int[1];  private int[] vbo_color_I=new int[1];
    private int[] vbo_position_N=new int[1];  private int[] vbo_color_N=new int[1];
    private int[] vbo_position_D=new int[1];  private int[] vbo_color_D=new int[1];
    private int[] vbo_position_A=new int[1];  private int[] vbo_color_A=new int[1];
    private int[] vbo_position_A_band=new int[1];  private int[] vbo_color_A_band=new int[1];
    private int[] vbo_position_plane=new int[1];
    private int[] vbo_position_smoke=new int[1];

    private int mvpUniform;
    float[] modelViewMatrix=new float[16];
    float[] modelViewProjectionMatrix=new float[16];
    float[] translationMatrix=new float[16];
    float[] rotationMatrix=new float[16];
    private float[] perspectiveProjectionMatrix=new float[16];     // 4*4 matrix

    float letter_x = 0.46f;
    float letter_start = -0.19f, letter_end = 0.19f;
    float i1_x = -3.0f;
    float a_x = 2.50f;
    float n_y = 1.58f;
    float i2_y = -1.58f;
    float r_saffron = 0.0f, g_saffron = 0.0f, b_saffron = 0.0f;
    float r_green = 0.0f, g_green = 0.0f, b_green = 0.0f;
    float r = 1.42f;
    float r_saffron_smoke = 255.0f, g_saffron_smoke = 153.0f, b_saffron_smoke = 51.0f;
    float r_white_smoke = 1.0f;
    float r_green_smoke = 18.0f, g_green_smoke = 136.0f, b_green_smoke = 7.0f;
    float middle_plane_x = -letter_x * 5.4f;
    float angle_upper_plane = -90.0f;
    float angle_lower_plane = 90.0f;
    float angle = (float)Math.PI;
    float angle_lower = (float)Math.PI;
    float angle_right_upper = 3.0f*(float)Math.PI / 2.0f + 0.17f*(float)Math.PI;
    float angle_right_lower = (float)Math.PI / 2.0f - 0.17f*(float)Math.PI;
    float angle_smoke;

    boolean bDoneI1 = false;
    boolean bDoneA = false;
    boolean bDoneN = false;
    boolean bDoneI2 = false;
    boolean bDoneR_saffron = false, bDoneG_saffron = false, bDoneB_saffron = false;
    boolean bDoneR_green = false, bDoneG_green = false, bDoneB_green = false;
    boolean bDoneD = false;
    boolean bDoneMidddlePlane = false;
    boolean bDoneUpperPlane = false;
    boolean bDoneDetachPlanes = false;
    boolean bDoneFadingSmokes = false;

    // constructor
    public GLESView(Context drawingContext)
    {
        super(drawingContext);
        context=drawingContext;
        // call 3 functions of GLSurfaceView class
        setEGLContextClientVersion(3);  // tell egl that client wants version 3.x; it gives nearest
        setRenderer(this);  // who is your renderer; this since you are only writing onDrawFrame()
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY); // repaint when render mode is dirty. The rect that is to be repainted is called dirty.
                                                            // When renderMode is RENDERMODE_WHEN_DIRTY, the renderer only rendered when the surface is created, or when requestRender() is called

        gestureDetector=new GestureDetector(context,this,null,false);   // context: state
                                                                        // this: who is going to listen and handle events(handler)
                                                                        // null: no other class is going to handle events;
                                                                        // false: fixed 

        gestureDetector.setOnDoubleTapListener(this);   // this: handler; specially for double tap
    }

    // handling 'onTouchEvent' is the most important because it triggers all gesture and tap events
    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        // code
        int eventaction=event.getAction();  // not needed for OpenGL(needed for combination of NDK and SDK; eg.for keyboard touch)
        if(!gestureDetector.onTouchEvent(event))    // ==false; if user's touch detected by this class's GestureDetector cannot be handled by this class, send it to super class (like DefWindowProc())
            super.onTouchEvent(event);

        return(true);   // if touch event handled by this class, return true to GestureDetector class
    }

    // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTap(MotionEvent e)   // of double tap latency
    {
        return(true);
    }

    // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onDoubleTapEvent(MotionEvent e)  // can consist of multiple double taps(eg. swipe)
    {
        // do not write any code here because already written in 'OnDoubleTap'
        return(true);
    }

     // abstract methods from OnDoubleTapListener so must be implemented
    @Override
    public boolean onSingleTapConfirmed(MotionEvent e)  // like WM_LBUTTONDOWN
    {
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onDown(MotionEvent e)    // for single tap, double tap, scroll,etc. (all)
    {
        // do not write any code here because already written in 'onSingleTapConfirmed'
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onFling(MotionEvent e1,MotionEvent e2,float velocityX,float velocityY)    // swipe
    {
        return(true);
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public void onLongPress(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
    }

    // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onScroll(MotionEvent e1,MotionEvent e2,float distanceX,float distanceY)  // takes two end points of scroll and finds direction and amount of scroll
    {
        Uninitialize();
        System.exit(0);	// successful exit status
        return(true); 
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public void onShowPress(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
    }

     // abstract methods from OnGestureListener so must be implemented
    @Override
    public boolean onSingleTapUp(MotionEvent e)  // void: decided by OS (since it is a callback)
    {
        return(true); 
    }

    // implement GLSurfaceView.Renderer methods
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) // prototype not decided by us
    {
        String gl_version=gl.glGetString(GL10.GL_VERSION); // take GL version
        String gles32_version=gl.glGetString(GLES32.GL_SHADING_LANGUAGE_VERSION);   // OGL shading language version

        // print it
        System.out.println("\nRTR: GL Version: "+ gl_version);
        System.out.println("\nRTR: GLES32 Version: "+ gles32_version);

        Initialize();
    }

     @Override
    public void onSurfaceChanged(GL10 unused,int width, int height) // unused: the GL interface. 'unused' says that try not to use GL10 functions here
    {
        Resize(width,height);
    }

     @Override
    public void onDrawFrame(GL10 unused)
    {
        Update();
        Display();
    }

    // our custom methods
    private void Initialize()   // private since our custom method; cannot write 'void' as parameter in Java 
    {
        // code till wglMakeCurrent() already done due to GLSurfaceView's setRenderer(). (done natively in NDK using eglMakeCurrent())
        // for every OpenGL function/macro/enum, add 'GLES32.' before it

        // vertex shader
        vertexShaderObject=GLES32.glCreateShader(GLES32.GL_VERTEX_SHADER);

        final String vertexShaderSourceCode=String.format(
        "#version 320 es" +
		"\n" +
		"in vec4 vPosition;" +
        "in vec4 vColor;" +
		"uniform mat4 u_mvp_matrix;" +
        "out vec4 out_color;" +
		"void main(void)" +
		"{" +
		"gl_Position = u_mvp_matrix * vPosition;" +
        "gl_PointSize=5.0;" +
        "out_color = vColor;" +
		"}"
        );      // written in Graphics Library Shading/Shader Language (GLSL)
				// 320: OpenGL version support*100 (3.2*100); es: embedded system
				// '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
				// uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// gl_Position: in-built variable of shader

	    // give above source code to the vertex shader object
        GLES32.glShaderSource(vertexShaderObject,vertexShaderSourceCode);   // internally calls NDK's function which also has 4 para. like Windows' fn

        // compile the vertex shader
        GLES32.glCompileShader(vertexShaderObject);

        // compile time error checking
        int[] iShaderCompileStatus=new int[1];      // array of 1 since we will need addr of iShaderCompileStatus
        int[] iInfoLogLength=new int[1];            // array of 1 since we will need addr of iInfoLogLength
        String szInfoLog=null;

       GLES32.glGetShaderiv(	        // iv= integer vector
		vertexShaderObject,	            // the shader object to be queried
		GLES32.GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		iShaderCompileStatus,	        // empty; returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
        0                               // give address of 0th element; whenever passing address of array, pass the element no. in which you want the information filled
	    );	// returns a parameter from a shader object

        if (iShaderCompileStatus[0] == GLES32.GL_FALSE)	// error present
        {
            // check if the compiler has any info. about the error
            GLES32.glGetShaderiv(vertexShaderObject,
                GLES32.GL_INFO_LOG_LENGTH,
                iInfoLogLength,     // returns the number of characters in the information log for shader
                0);

            if (iInfoLogLength[0] > 0)
            {
                szInfoLog=GLES32.glGetShaderInfoLog(vertexShaderObject);	// the shader object whose information log is to be queried.				
                                                                            // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
                                                                            // When a shader object is created, its information log will be a string of length 0.
                System.out.println("\nRTR: Vertex Shader Compilation Log : "+szInfoLog);
                Uninitialize();	
                System.exit(0);	// 0 since error not of OS
            }
        }

        // fragment shader
        fragmentShaderObject=GLES32.glCreateShader(GLES32.GL_FRAGMENT_SHADER);

        final String fragmentShaderSourceCode=String.format(
        "#version 320 es" +
        "\n" +
        "precision highp float;" +
        "in vec4 out_color;" +
        "out vec4 fragColor;" +
        "void main(void)" +
        "{" +
        "fragColor = out_color;" +	
        "}"
        );      // written in Graphics Library Shading/Shader Language (GLSL)
                // 320: OpenGL version support*100 (3.2*100); core: core profile
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
                // precision highp float: give high precision to float. Use mediump for int(def). VS by def has highp for all. Max processing happens in FS than VS as it gives ultimate color.
                // precision needed only in ES as there are many math units
                // vec4: function/macro/constructor (of vmath?)
                // FragColor = vec4(1.0f,1.0f,0.0,0.0): will give yellow color to fragment; don't write 'f' in shaders (if class) 

        // give above source code to the fragment shader object
        GLES32.glShaderSource(fragmentShaderObject,fragmentShaderSourceCode);   // internally calls NDK's function which also has 4 para. like Windows' fn

        // compile the fragment shader
        GLES32.glCompileShader(fragmentShaderObject);

        // compile time error checking
        iShaderCompileStatus[0]=0;
        iInfoLogLength[0]=0;
        szInfoLog=null;

        GLES32.glGetShaderiv(	        // iv= integer vector
            fragmentShaderObject,	        // the shader object to be queried
            GLES32.GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
            iShaderCompileStatus,	        // empty; returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
            0                               // give address of 0th element; whenever passing address of array, pass the element no. in which you want the information filled
            );	// returns a parameter from a shader object

            if (iShaderCompileStatus[0] == GLES32.GL_FALSE)	// error present
            {
                // check if the compiler has any info. about the error
                GLES32.glGetShaderiv(fragmentShaderObject,
                    GLES32.GL_INFO_LOG_LENGTH,
                    iInfoLogLength,     // returns the number of characters in the information log for shader
                    0);	

                if (iInfoLogLength[0] > 0)
                {
                    szInfoLog=GLES32.glGetShaderInfoLog(fragmentShaderObject);	// the shader object whose information log is to be queried.				
                                                                                // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
                                                                                // When a shader object is created, its information log will be a string of length 0.
                    System.out.println("\nRTR: Fragment Shader Compilation Log : "+szInfoLog);
                    Uninitialize();	
                    System.exit(0);	// 0 since error not of OS
                }
            }

        shaderProgramObject=GLES32.glCreateProgram();

        GLES32.glAttachShader(shaderProgramObject,vertexShaderObject);  // 1st para: the program object to which a shader object will be attached
                                                                        // 2nd para: the shader object that is to be attached.

        GLES32.glAttachShader(shaderProgramObject,fragmentShaderObject);              

        // pre-linking binding to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
        GLES32.glBindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
            GLESMacros.AMC_ATTRIBUTE_POSITION,				// the index of the generic vertex attribute to be bound.
            "vPosition"								        // a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
        );	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable
            // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition 

        GLES32.glBindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
            GLESMacros.AMC_ATTRIBUTE_COLOR,				// the index of the generic vertex attribute to be bound.
            "vColor"								        // a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
        );

        // link the shader program to your program
        GLES32.glLinkProgram(shaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on                                                
        
        // link time error checking
        int iProgramLinkStatus[]=new int[1];      // array of 1 since we will need addr of iProgramLinkStatus
        iInfoLogLength[0]=0;          
        szInfoLog=null;

        GLES32.glGetProgramiv(	        // iv= integer vector
            shaderProgramObject,	    // the shader object to be queried
            GLES32.GL_LINK_STATUS,		// what(object parameter) is to be queried	
            iProgramLinkStatus,	        // empty; returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
            0                           // give address of 0th element; whenever passing address of array, pass the element no. in which you want the information filled
            );	// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
				// When a program object is created, its information log will be a string of length 0.

        if (iProgramLinkStatus[0] == GLES32.GL_FALSE)	// error present
        {
            // check if the linker has any info. about the error
            GLES32.glGetShaderiv(shaderProgramObject,
                GLES32.GL_INFO_LOG_LENGTH,
                iInfoLogLength,     // returns the number of characters in the information log for shader
                0);	

            if (iInfoLogLength[0] > 0)
            {
                szInfoLog=GLES32.glGetProgramInfoLog(shaderProgramObject);	// the shader object whose information log is to be queried.				
                                                                            // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
                                                                            // When a shader object is created, its information log will be a string of length 0.
                System.out.println("\nRTR: Shader Program Link Log : "+szInfoLog);
                Uninitialize();	
                System.exit(0);	// 0 since error not of OS
            }
        }

        // post-linking retrieving uniform locations (uniforms are global to shaders)
	    mvpUniform = GLES32.glGetUniformLocation(shaderProgramObject, "u_mvp_matrix");	// preparation of data transfer from CPU to GPU (binding)
                                                                                        // u_mvp_matrix: of GPU; mvpUniform: of CPU
                                                                                        // telling it to take location of uniform u_mvp_matrix and give in mvpUniform

        // variable declarations
	    float half_letter_ht = 0.5f;

        // fill triangle vertices in array (this was in Display() in FFP)
        final float I_position[] = new float[] { 
            letter_start + 0.02f, half_letter_ht, 0.0f,
            letter_end - 0.02f, half_letter_ht, 0.0f,
            0.0f, half_letter_ht, 0.0f,
            0.0f, -half_letter_ht, 0.0f,
            letter_start + 0.02f, -half_letter_ht, 0.0f,
            letter_end - 0.02f, -half_letter_ht, 0.0f
        };	 // since written in Initialize() only, coords will not get set everytime in Display(). Hence, fast speed.

        final float I_color[] = new float[] { 
           1.0f, 0.59765625f, 0.19921875f,	 // saffron
            1.0f, 0.59765625f, 0.19921875f,	 // saffron
            1.0f, 0.59765625f, 0.19921875f,	 // saffron
            0.0703125f, 0.53125f, 0.02734375f,	 // green
            0.0703125f, 0.53125f, 0.02734375f,	 // green
            0.0703125f, 0.53125f, 0.02734375f   // green
        };

        final float N_position[] = new float[] { 
            letter_start, half_letter_ht + 0.015f, 0.0f,
            letter_start, -half_letter_ht - 0.015f, 0.0f,
            letter_start, half_letter_ht + 0.015f, 0.0f,
            letter_end, -half_letter_ht - 0.015f, 0.0f,
            letter_end, -half_letter_ht - 0.015f, 0.0f,
            letter_end, half_letter_ht + 0.015f, 0.0f
        };

        final float N_color[] = new float[] { 
            1.0f, 0.59765625f, 0.19921875f,	 // saffron
            0.0703125f, 0.53125f, 0.02734375f ,  // green
            1.0f, 0.59765625f, 0.19921875f,	 // saffron
            0.0703125f, 0.53125f, 0.02734375f,   // green
            0.0703125f, 0.53125f, 0.02734375f,   // green
            1.0f, 0.59765625f, 0.19921875f,	 // saffron
        };

        final float D_position[] = new float[] { 
           letter_start + 0.05f, half_letter_ht, 0.0f,
            letter_start + 0.05f, -half_letter_ht, 0.0f,
            letter_start, half_letter_ht, 0.0f,
            letter_end + 0.0135f, half_letter_ht, 0.0f,
            letter_end, half_letter_ht, 0.0f,
            letter_end, -half_letter_ht, 0.0f,
            letter_end + 0.013f, -half_letter_ht, 0.0f,
            letter_start, -half_letter_ht, 0.0f
        };

        final float A_position[] = new float[] { 
            letter_start, -half_letter_ht - 0.015f, 0.0f,
            0.0f, half_letter_ht + 0.015f, 0.0f,
            0.0f, half_letter_ht + 0.015f, 0.0f,
            letter_end, -half_letter_ht - 0.015f, 0.0f
        };

        final float A_color[] = new float[] { 
           0.0703125f, 0.53125f, 0.02734375f,	 // green
            1.0f, 0.59765625f, 0.19921875f,	 // saffron
            1.0f, 0.59765625f, 0.19921875f,	 // saffron
            0.0703125f, 0.53125f, 0.02734375f	 // green
        };

        final float A_band_position[] = new float[] { 
            letter_start / 2.0f, 0.0166f, 0.0f,
            letter_end / 2.0f, 0.0166f, 0.0f,
            letter_start / 2.0f, 0.0f, 0.0f,
            letter_end / 2.0f, 0.0f, 0.0f,
            letter_start / 2.0f, -0.0173f, 0.0f,
            letter_end / 2.0f, -0.0173f, 0.0f
        };

        final float A_band_color[] = new float[] { 
            1.0f, 0.59765625f, 0.19921875f,	 // saffron
            1.0f, 0.59765625f, 0.19921875f,	 // saffron
            1.0f, 1.0f, 1.0f,					// white
            1.0f, 1.0f, 1.0f,					// white
            0.0703125f, 0.53125f, 0.02734375f,	 // green
            0.0703125f, 0.53125f, 0.02734375f,	 // green
        };

        final float plane_position[] = new float[] { 
        // middle quad
		0.25f + 0.1f, 0.05f, 0.0f,
		0.0f + 0.1f, 0.05f, 0.0f,
		0.0f + 0.1f, -0.05f, 0.0f,
		0.25f + 0.1f, -0.05f, 0.0f,
		// upper wing
		0.0f + 0.1f, 0.05f, 0.0f,
		0.1f + 0.1f, 0.05f, 0.0f,
		0.05f + 0.1f, 0.15f, 0.0f,
		0.0f + 0.1f, 0.15f, 0.0f,
		// lower wing
		0.1f + 0.1f, -0.05f, 0.0f,
		0.0f + 0.1f, -0.05f, 0.0f,
		0.0f + 0.1f, -0.15f, 0.0f,
		0.05f + 0.1f, -0.15f, 0.0f,
		// back
		0.0f + 0.1f, 0.05f, 0.0f,
		-0.1f + 0.1f, 0.08f, 0.0f,
		-0.1f + 0.1f, -0.08f, 0.0f,
		0.0f + 0.1f, -0.05f, 0.0f,
		// front triangle
		0.25f + 0.1f, 0.05f, 0.0f,
		0.25f + 0.1f, -0.05f, 0.0f,
		0.30f + 0.1f, 0.00f, 0.0f,
		// I
		-0.01f + 0.1f, 0.03f, 0.0f,
		0.03f + 0.1f, 0.03f, 0.0f,
		0.01f + 0.1f, 0.03f, 0.0f,
		0.01f + 0.1f, -0.03f, 0.0f,
		-0.01f + 0.1f, -0.03f, 0.0f,
		0.03f + 0.1f, -0.03f, 0.0f,
		// A
		0.05f + 0.1f, -0.03f, 0.0f,
		0.07f + 0.1f, 0.03f, 0.0f,
		0.07f + 0.1f, 0.03f, 0.0f,
		0.09f + 0.1f, -0.03f, 0.0f,
		(0.05f + 0.07f) / 2.0f + 0.1f, 0.0f, 0.0f,
		(0.07f + 0.09f) / 2.0f + 0.1f, 0.0f, 0.0f,
		// F
		0.12f + 0.1f, -0.03f, 0.0f,
		0.12f + 0.1f, 0.03f, 0.0f,
		0.12f + 0.1f, 0.03f, 0.0f,
		0.16f + 0.1f, 0.03f, 0.0f,
		0.12f + 0.1f, 0.0f, 0.0f,
		0.15f + 0.1f, 0.0f, 0.0f
        };

	    // 9 lines
        // I
        // create vao
        GLES32.glGenVertexArrays(1, vao_I, 0);    // generate vertex array object names
                                                      // 1: Specifies the number of vertex array object names to generate
                                                      // vao_I1: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
                                                      // 0: want in 0th index of vao array

		// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()
        
        // bind vao
        GLES32.glBindVertexArray(vao_I[0]);

        // I position
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_position_I, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_I[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(I_position.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        positionBuffer.put(I_position);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        positionBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		I_position.length * 4,		            // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		positionBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											                    // number of components per generic vertex attribute; x,y,z for position
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                        // enables vPosition

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

        // I1 color
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_color_I, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_I[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        byteBuffer = ByteBuffer.allocateDirect(I_color.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleColor.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        FloatBuffer colorBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        colorBuffer.put(I_color);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        colorBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		I_color.length * 4,		                // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		colorBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											                    // number of components per generic vertex attribute; r,g,b for color
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                    // enables vColor

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

	    GLES32.glBindVertexArray(0);		// unbind vao

	    //=====================================================================================================================================================

        // N
        // create vao
        GLES32.glGenVertexArrays(1, vao_N, 0);    // generate vertex array object names
                                                      // 1: Specifies the number of vertex array object names to generate
                                                      // vao_I1: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
                                                      // 0: want in 0th index of vao array

		// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()
        
        // bind vao
        GLES32.glBindVertexArray(vao_N[0]);

        // I1 position
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_position_N, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_N[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        byteBuffer = ByteBuffer.allocateDirect(N_position.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        positionBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        positionBuffer.put(N_position);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        positionBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		N_position.length * 4,		            // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		positionBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											                    // number of components per generic vertex attribute; x,y,z for position
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                        // enables vPosition

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

        // N color
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_color_N, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_N[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        byteBuffer = ByteBuffer.allocateDirect(N_color.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleColor.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        colorBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        colorBuffer.put(N_color);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        colorBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		N_color.length * 4,		                // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		colorBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											                    // number of components per generic vertex attribute; r,g,b for color
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                    // enables vColor

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

	    GLES32.glBindVertexArray(0);		// unbind vao

        //=====================================================================================================================================================

        // D
        // create vao
        GLES32.glGenVertexArrays(1, vao_D, 0);    // generate vertex array object names
                                                      // 1: Specifies the number of vertex array object names to generate
                                                      // vao_I1: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
                                                      // 0: want in 0th index of vao array

		// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()
        
        // bind vao
        GLES32.glBindVertexArray(vao_D[0]);

        // I1 position
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_position_D, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_D[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        byteBuffer = ByteBuffer.allocateDirect(D_position.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        positionBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        positionBuffer.put(D_position);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        positionBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		D_position.length * 4,		            // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		positionBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											                    // number of components per generic vertex attribute; x,y,z for position
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                        // enables vPosition

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

        // D color
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_color_D, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_D[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		0,		                // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		null,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_DYNAMIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											                    // number of components per generic vertex attribute; r,g,b for color
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                    // enables vColor

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

	    GLES32.glBindVertexArray(0);		// unbind vao

        //=====================================================================================================================================================

        // A
        // create vao
        GLES32.glGenVertexArrays(1, vao_A, 0);    // generate vertex array object names
                                                      // 1: Specifies the number of vertex array object names to generate
                                                      // vao_I1: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
                                                      // 0: want in 0th index of vao array

		// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()
        
        // bind vao
        GLES32.glBindVertexArray(vao_A[0]);

        // A position
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_position_A, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_A[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        byteBuffer = ByteBuffer.allocateDirect(A_position.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        positionBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        positionBuffer.put(A_position);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        positionBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		A_position.length * 4,		            // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		positionBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											                    // number of components per generic vertex attribute; x,y,z for position
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                        // enables vPosition

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

        // A color
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_color_A, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_A[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        byteBuffer = ByteBuffer.allocateDirect(A_color.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleColor.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        colorBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        colorBuffer.put(A_color);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        colorBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		A_color.length * 4,		                // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		colorBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											                    // number of components per generic vertex attribute; r,g,b for color
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                    // enables vColor

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

	    GLES32.glBindVertexArray(0);		// unbind vao

        //=====================================================================================================================================================

        // A band
        // create vao
        GLES32.glGenVertexArrays(1, vao_A_band, 0);    // generate vertex array object names
                                                      // 1: Specifies the number of vertex array object names to generate
                                                      // vao_I1: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
                                                      // 0: want in 0th index of vao array

		// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()
        
        // bind vao
        GLES32.glBindVertexArray(vao_A_band[0]);

        // A line position
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_position_A_band, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_A_band[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        byteBuffer = ByteBuffer.allocateDirect(A_band_position.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        positionBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        positionBuffer.put(A_band_position);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        positionBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		A_band_position.length * 4,		            // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		positionBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											                    // number of components per generic vertex attribute; x,y,z for position
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                        // enables vPosition

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

        // A line color
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_color_A_band, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_A_band[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        byteBuffer = ByteBuffer.allocateDirect(A_band_color.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleColor.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        colorBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        colorBuffer.put(A_band_color);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        colorBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		A_band_color.length * 4,		                // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		colorBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_COLOR,	// at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											                    // number of components per generic vertex attribute; r,g,b for color
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                    // enables vColor

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

	    GLES32.glBindVertexArray(0);		// unbind vao

        //=====================================================================================================================================================

        // plane
        // create vao
        GLES32.glGenVertexArrays(1, vao_plane, 0);    // generate vertex array object names
                                                      // 1: Specifies the number of vertex array object names to generate
                                                      // vao_I1: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
                                                      // 0: want in 0th index of vao array

		// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()
        
        // bind vao
        GLES32.glBindVertexArray(vao_plane[0]);

        // plane position
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_position_plane, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_plane[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        byteBuffer = ByteBuffer.allocateDirect(plane_position.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        positionBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        positionBuffer.put(plane_position);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        positionBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		plane_position.length * 4,		            // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		positionBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											                    // number of components per generic vertex attribute; x,y,z for position
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                        // enables vPosition

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

	    GLES32.glBindVertexArray(0);		// unbind vao

        //=====================================================================================================================================================

        // smoke
        // create vao
        GLES32.glGenVertexArrays(1, vao_smoke, 0);    // generate vertex array object names
                                                      // 1: Specifies the number of vertex array object names to generate
                                                      // vao_I1: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
                                                      // 0: want in 0th index of vao array

		// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()
        
        // bind vao
        GLES32.glBindVertexArray(vao_smoke[0]);

        // plane position
        // create vbo (vbo is attribute-wise)
	    GLES32.glGenBuffers(1, vbo_position_smoke, 0);		// generate buffer object names
                                            // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
                                            // vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
                                            // 0: want in 0th index of vbo array

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_smoke[0]);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                            // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                            // vbo[0]: bind this (the name of a buffer object.)   

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		0,		                                        // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		null,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_STATIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glVertexAttribPointer(GLESMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											                    // number of components per generic vertex attribute; x,y,z for position
		GLES32.GL_FLOAT,									            // data type of each component in the array
		false,									                        // is data normalised (Java's false)
		0,											                    // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                        // offset if V,C,T,N stored in single array (inter-leaved); 0: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                                                                        // don't write null here as in Win32, #define NULL is 0. But in Java, null is not mapped to 0.
        );		// define an array of generic vertex attribute data

        GLES32.glEnableVertexAttribArray(GLESMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														                        // enables vPosition

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

	    GLES32.glBindVertexArray(0);		// unbind vao

        //=====================================================================================================================================================

        GLES32.glEnable(GLES32.GL_DEPTH_TEST);	// to compare depth values of objects

	    GLES32.glDepthFunc(GLES32.GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
                                                // Passes if the incoming z value is less than or equal to the stored z value. 
                                                // GL_LEQUAL : GLenum

	    GLES32.glDisable(GLES32.GL_CULL_FACE);		// If enabled, cull polygons based on their winding in window coordinates

        GLES32.glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											            // specifies clear values[0,1] used by glClear() for the color buffers

        Matrix.setIdentityM(perspectiveProjectionMatrix, 0);   	// making orthographicProjectionMatrix an identity matrix(diagonals 1); 0: fill from 0th index  
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        // no warmup call to Resize() since our application is already fullscreen                                             
    }

    private void Resize(int width,int height)
    {
        if (height == 0)
            height = 1;

	    GLES32.glViewport(0, 0, width, height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

        Matrix.perspectiveM(perspectiveProjectionMatrix, 0, 45.0f, (float)width / (float)height, 0.1f, 100.0f);	// parameters:
                                                                                                                // perspectiveProjectionMatrix: fill in this
                                                                                                                // 0: from 0th index
                                                                                                                // fovy- The field of view angle, in degrees, in the y - direction.
                                                                                                                // aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
                                                                                                                // zNear- The distance from the viewer to the near clipping plane(always positive).
                                                                                                                // zFar- The distance from the viewer to the far clipping plane(always positive)
    }

    private void Display()
    {
        GLES32.glClear(GLES32.GL_COLOR_BUFFER_BIT|GLES32.GL_DEPTH_BUFFER_BIT);

        // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	    GLES32.glUseProgram(shaderProgramObject);	// binding your OpenGL code with shader program object
										            // Specifies the handle of the program object whose executables are to be used as part of current rendering state.

        GLES32.glLineWidth(15.0f);

        // I1
        DrawI1();

        // A
        if (bDoneI1 == true)
            DrawA();

        // N
        if (bDoneA == true)
            DrawN();

        // I2
        if (bDoneN == true)
            DrawI2();

        // D
        if (bDoneI2 == true)
            DrawD();

        // Attach planes
        if (bDoneD == true)
        {
            // upper
            if (bDoneUpperPlane == false)
            {
                DrawLeftUpperPlane();
            }
            DrawLeftUpperSmoke();

            // lower
            if (bDoneUpperPlane == false)
            {
                DrawLeftLowerPlane();
            }
            DrawLeftLowerSmoke();

            // middle
            DrawMiddlePlane();
            DrawSmokeMiddle();
        }

        // Detach planes
        if (bDoneMidddlePlane == true)
        {
            // upper
            if (bDoneDetachPlanes == false)
            {
                DrawRightUpperPlane();
            }
            DrawRightUpperSmoke();

            // lower
            if (bDoneDetachPlanes == false)
            {
                DrawRightLowerPlane();
            }
            DrawRightLowerSmoke();

            // middle
            DrawMiddlePlane();
            DrawSmokeMiddle();
        }

        // A's band
        if (bDoneDetachPlanes == true)
        {
            DrawABand();
        }

        // unuse program
        GLES32.glUseProgram(0);	// unbinding your OpenGL code with shader program object
                            // If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.
        
        requestRender();    // request to render; ~glSwapBuffers() // calls onDrawFrame() (?)
    }

    private void DrawI1()
    {
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, i1_x, 0.0f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_I[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        // draw the necessary scene!
        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            0,							// array position of your 9-member array to start with (imp in inter-leaved)
            6							// how many vertices to draw
        );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                // Arrays since multiple primitives(P,C,N,T) can be drawn

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void DrawA()
    {
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, a_x, 0.0f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_A[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        // draw the necessary scene!
        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            0,							// array position of your 9-member array to start with (imp in inter-leaved)
            4							// how many vertices to draw
        );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                // Arrays since multiple primitives(P,C,N,T) can be drawn

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void DrawN()
    {
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, -letter_x, n_y, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_N[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        // draw the necessary scene!
        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            0,							// array position of your 9-member array to start with (imp in inter-leaved)
            6							// how many vertices to draw
        );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                // Arrays since multiple primitives(P,C,N,T) can be drawn

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void DrawI2()
    {
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, letter_x, i2_y, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_I[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        // draw the necessary scene!
        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            0,							// array position of your 9-member array to start with (imp in inter-leaved)
            6							// how many vertices to draw
        );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                // Arrays since multiple primitives(P,C,N,T) can be drawn

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void DrawD()
    {
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_D[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_color_D[0]);

        float DColor[] = new float[] { 
        r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f,
		r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f
        };

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(DColor.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        FloatBuffer colorBuffer = byteBuffer.asFloatBuffer();

        // now put your array into this "cooked" buffer
        colorBuffer.put(DColor);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        colorBuffer.position(0);     // start from 0th index

        // now call glBufferData()
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		DColor.length * 4,		            // size of array in which data is to be provided; size in bytes of the buffer object's new data store
		colorBuffer,				                    // actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GLES32.GL_DYNAMIC_DRAW					        // when to give data (statically=now); static= The data store contents will be modified once and used many times.
	    );		// creates a new data store for the buffer object currently bound to target(1st para.)

        // draw the necessary scene!
        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            0,							// array position of your 9-member array to start with (imp in inter-leaved)
            8							// how many vertices to draw
        );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                // Arrays since multiple primitives(P,C,N,T) can be drawn

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer
        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void DrawLeftUpperPlane()
    {
        // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(translationMatrix, 0, -2.0f*letter_x + letter_end, 1.22f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

        Matrix.translateM(translationMatrix, 0, r*(float)Math.cos(angle), r*(float)Math.sin(angle), 0.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

	    Matrix.setRotateM(rotationMatrix, 0, angle_upper_plane, 0.0f, 0.0f, 1.0f);   // rotationMatrix: output matrix, 0: 0th index; rotate about y axis

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          rotationMatrix, 0                    // same sequence
        );

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_plane[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        DrawPlane();

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void DrawMiddlePlane()
    {
        // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, middle_plane_x, 0.0f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_plane[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        DrawPlane();

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void DrawLeftLowerPlane()
    {
        // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(translationMatrix, 0, -2.0f*letter_x + letter_end, -1.22f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

        Matrix.translateM(translationMatrix, 0, r*(float)Math.cos(angle_lower), r*(float)Math.sin(angle_lower), 0.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

	    Matrix.setRotateM(rotationMatrix, 0, angle_lower_plane, 0.0f, 0.0f, 1.0f);   // rotationMatrix: output matrix, 0: 0th index; rotate about y axis

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          rotationMatrix, 0                    // same sequence
        );

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_plane[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        DrawPlane();

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void DrawLeftUpperSmoke()
    {
        // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, -2.0f*letter_x + letter_end, 1.22f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_smoke[0]);

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_smoke[0]);

        float[] point = new float[] { 0.0f, 0.0f, 0.0f };

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(point.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        // saffron
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

        for (angle_smoke = (float)Math.PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
        {
            point[0] = (r - 0.018f)*(float)Math.cos(angle_smoke);
            point[1] = (r - 0.018f)*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // white
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// specify the value of a generic vertex attribute(for single color to object)
        
        for (angle_smoke = (float)Math.PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
        {
            point[0] = r*(float)Math.cos(angle_smoke);
            point[1] = r*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // green
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)
        
        for (angle_smoke = (float)Math.PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
        {
            point[0] = (r + 0.018f)*(float)Math.cos(angle_smoke);
            point[1] = (r + 0.018f)*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

        GLES32.glBindVertexArray(0);		// unbind vao                                                
    }

    private void DrawSmokeMiddle()
    {
        // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, 0.0f, 0.0f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_smoke[0]);

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_smoke[0]);

        float[] line_saffron = new float[] { -letter_x * 5.4f, 0.0166f, 0.0f,
								 middle_plane_x, 0.0166f, 0.0f };

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(line_saffron.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        // saffron
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

        // now put your array into this "cooked" buffer
        positionBuffer.put(line_saffron);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        positionBuffer.position(0);     // start from 0th index

        // fill attributes
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
            line_saffron.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
            positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
            GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
        );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            0,						// array position of your 9-member array to start with (imp in inter-leaved)
            2						// how many vertices to draw
        );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                // Arrays since multiple primitives(P,C,N,T) can be drawn}

        // white
        float[] line_white = new float[] { -letter_x * 5.4f, 0.0f, 0.0f,
								middle_plane_x, 0.0f, 0.0f };

        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// specify the value of a generic vertex attribute(for single color to object)

        // now put your array into this "cooked" buffer
        positionBuffer.put(line_white);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        positionBuffer.position(0);     // start from 0th index

        // fill attributes
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
            line_white.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
            positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
            GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
        );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            0,						// array position of your 9-member array to start with (imp in inter-leaved)
            2						// how many vertices to draw
        );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                // Arrays since multiple primitives(P,C,N,T) can be drawn}

         // green
        float[] line_green = new float[] { -letter_x * 5.4f, -0.0173f, 0.0f,
								middle_plane_x, -0.0173f, 0.0f };

        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

        // now put your array into this "cooked" buffer
        positionBuffer.put(line_green);

        // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
        positionBuffer.position(0);     // start from 0th index

        // fill attributes
        GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
            line_green.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
            positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
            GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
        );		// creates a new data store for the buffer object currently bound to target(1st para.)

        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            0,						// array position of your 9-member array to start with (imp in inter-leaved)
            2						// how many vertices to draw
        );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                // Arrays since multiple primitives(P,C,N,T) can be drawn}

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

        GLES32.glBindVertexArray(0);		// unbind vao 

        if (bDoneDetachPlanes == true)
        {
            GLES32.glLineWidth(15.0f);

            DrawI1();
            DrawN();
            DrawD();
            DrawI2();
            DrawA();
        }        
    }

    private void DrawLeftLowerSmoke()
    {
        // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, -2.0f*letter_x + letter_end, -1.22f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_smoke[0]);

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_smoke[0]);

        float[] point = new float[] { 0.0f, 0.0f, 0.0f };

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(point.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        // saffron
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

	    for (angle_smoke = (float)Math.PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
        {
            point[0] = (r + 0.018f)*(float)Math.cos(angle_smoke);
            point[1] = (r + 0.018f)*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // white
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// specify the value of a generic vertex attribute(for single color to object)
        
	    for (angle_smoke = (float)Math.PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
        {
            point[0] = r*(float)Math.cos(angle_smoke);
            point[1] = r*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // green
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)
        
	    for (angle_smoke = (float)Math.PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
        {
            point[0] = (r - 0.018f)*(float)Math.cos(angle_smoke);
            point[1] = (r - 0.018f)*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

        GLES32.glBindVertexArray(0);		// unbind vao                                                
    }

    private void DrawABand()
    {
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, letter_x*2.0f, 0.0f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_A_band[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; PP does not have GL_QUADS!
            0,							// array position of your 9-member array to start with (imp in inter-leaved)
            6							// how many vertices to draw
        );

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void DrawRightUpperPlane()
    {
        // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(translationMatrix, 0, 2.0f*letter_x - letter_end, 1.22f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

        Matrix.translateM(translationMatrix, 0, r*(float)Math.cos(angle_right_upper), r*(float)Math.sin(angle_right_upper), 0.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

	    Matrix.setRotateM(rotationMatrix, 0, angle_upper_plane, 0.0f, 0.0f, 1.0f);   // rotationMatrix: output matrix, 0: 0th index; rotate about y axis

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          rotationMatrix, 0                    // same sequence
        );

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_plane[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        DrawPlane();

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void DrawRightLowerPlane()
    {
        // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(translationMatrix, 0, 2.0f*letter_x - letter_end, -1.22f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

        Matrix.translateM(translationMatrix, 0, r*(float)Math.cos(angle_right_lower), r*(float)Math.sin(angle_right_lower), 0.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          translationMatrix, 0                    // same sequence
        );

	    Matrix.setRotateM(rotationMatrix, 0, angle_lower_plane, 0.0f, 0.0f, 1.0f);   // rotationMatrix: output matrix, 0: 0th index; rotate about y axis

        Matrix.multiplyMM(
          modelViewMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          modelViewMatrix, 0,      
          rotationMatrix, 0                    // same sequence
        );

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_plane[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        DrawPlane();

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void DrawRightUpperSmoke()
    {
        // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, 2.0f*letter_x - letter_end, 1.22f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_smoke[0]);

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_smoke[0]);

        float[] point = new float[] { 0.0f, 0.0f, 0.0f };

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(point.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        // saffron
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

	    for (angle_smoke = 3.0f*(float)Math.PI / 2.0f + 0.17f*(float)Math.PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
        {
            point[0] = (r - 0.018f)*(float)Math.cos(angle_smoke);
            point[1] = (r - 0.018f)*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // white
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// specify the value of a generic vertex attribute(for single color to object)
        
	    for (angle_smoke = 3.0f*(float)Math.PI / 2.0f + 0.17f*(float)Math.PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
        {
            point[0] = r*(float)Math.cos(angle_smoke);
            point[1] = r*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // green
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)
        
	    for (angle_smoke = 3.0f*(float)Math.PI / 2.0f + 0.17f*(float)Math.PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
        {
            point[0] = (r + 0.018f)*(float)Math.cos(angle_smoke);
            point[1] = (r + 0.018f)*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

        GLES32.glBindVertexArray(0);		// unbind vao                                                
    }

    private void DrawRightLowerSmoke()
    {
        // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
        Matrix.setIdentityM(modelViewMatrix, 0);
        Matrix.setIdentityM(modelViewProjectionMatrix, 0);
        Matrix.setIdentityM(translationMatrix, 0);
        Matrix.setIdentityM(rotationMatrix, 0);

        Matrix.translateM(modelViewMatrix, 0, -2.0f*letter_x + letter_end, -1.22f, -3.0f);   // translationMatrix: output matrix, 0: 0th index

        Matrix.multiplyMM(
          modelViewProjectionMatrix, 0,         // on LHS (write first); 0 since sending addresses of matrices
          perspectiveProjectionMatrix, 0,      
          modelViewMatrix, 0                    // same sequence
        );

        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        GLES32.glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            1,								        // how many matrices to send?
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
            0                                       // from 0th index
        );

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_smoke[0]);

        // bind vbo
        GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, vbo_position_smoke[0]);

        float[] point = new float[] { 0.0f, 0.0f, 0.0f };

        // Convert array into buffer which can be passed to glBufferData() since glBufferData() needs buffer. (In C/C++, array itself is a pointer. This is not the case in Java. Hence, convert array into buffer.)
        // 5 steps:
        // allocate the buffer directly from the native memory (not from VM memory). We are using nio
        ByteBuffer byteBuffer = ByteBuffer.allocateDirect(point.length * 4);   // telling OS to give you buffer of bytes for now (not int/float)
                                                                                          // Direct: from native
                                                                                          // triangleVertices.length * 4: size of buffer; java doesn't have sizeof operator; 4: size of float in java
    
        // arrange the buffer in native byte order (Little Endian or Big Endian)
        byteBuffer.order(ByteOrder.nativeOrder());  // ByteOrder.nativeOrder(): order of ByteOrder (static method)
                                                    // byteBuffer.order(): order of byteBuffer (instance method)

        // create a float type buffer and convert our byte type buffer into float type buffer
        FloatBuffer positionBuffer = byteBuffer.asFloatBuffer();

        // saffron
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

	    for (angle_smoke = (float)Math.PI / 2.0f - 0.17f*(float)Math.PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
        {
            point[0] = (r + 0.018f)*(float)Math.cos(angle_smoke);
            point[1] = (r + 0.018f)*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // white
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// specify the value of a generic vertex attribute(for single color to object)
        
	    for (angle_smoke = (float)Math.PI / 2.0f - 0.17f*(float)Math.PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
        {
            point[0] = r*(float)Math.cos(angle_smoke);
            point[1] = r*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // green
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)
        
	    for (angle_smoke = (float)Math.PI / 2.0f - 0.17f*(float)Math.PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
        {
            point[0] = (r - 0.018f)*(float)Math.cos(angle_smoke);
            point[1] = (r - 0.018f)*(float)Math.sin(angle_smoke);

            // now put your array into this "cooked" buffer
            positionBuffer.put(point);

            // set the array at the 0th position of the buffer (imp. in case of inter-leaved)
            positionBuffer.position(0);     // start from 0th index

            // fill attributes
            GLES32.glBufferData(GLES32.GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
                point.length*4,		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
                positionBuffer,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
                GLES32.GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
            );		// creates a new data store for the buffer object currently bound to target(1st para.)

            GLES32.glDrawArrays(GLES32.GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                0,						// array position of your 9-member array to start with (imp in inter-leaved)
                1						// how many vertices to draw
            );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
                    // Arrays since multiple primitives(P,C,N,T) can be drawn}
        }

        // unbind (LIFO)
	    GLES32.glBindBuffer(GLES32.GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										                // to bind with next buffer

        GLES32.glBindVertexArray(0);		// unbind vao                                                
    }

    private void DrawPlane()
    {
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, 186.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

        // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
        GLES32.glBindVertexArray(vao_plane[0]);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

        // middle quad
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            0,						// array position of your array to start with (imp in inter-leaved)
            4						// how many vertices to draw
        );
        // upper wing
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            4,						// array position of your array to start with (imp in inter-leaved)
            4						// how many vertices to draw
        );
        // lower wing
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            8,						// array position of your array to start with (imp in inter-leaved)
            4						// how many vertices to draw
        );
        // back
        GLES32.glDrawArrays(GLES32.GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            12,						// array position of your array to start with (imp in inter-leaved)
            4						// how many vertices to draw
        );
        // front triangle
        GLES32.glDrawArrays(GLES32.GL_TRIANGLES,	// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            16,						// array position of your array to start with (imp in inter-leaved)
            3						// how many vertices to draw
        );

        // IAF
	    GLES32.glLineWidth(3.0f);
        GLES32.glVertexAttrib3f(GLESMacros.AMC_ATTRIBUTE_COLOR, 99.0f / 255.0f, 99.0f / 255.0f, 99.0f / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

        // I
        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            19,						// array position of your array to start with (imp in inter-leaved)
            6						// how many vertices to draw
        );
        // A
        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            25,						// array position of your array to start with (imp in inter-leaved)
            6						// how many vertices to draw
        );
        // F
        GLES32.glDrawArrays(GLES32.GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
            31,						// array position of your array to start with (imp in inter-leaved)
            6						// how many vertices to draw
        );

        // unbind vao
        GLES32.glBindVertexArray(0);
    }

    private void Update()
    {
        if (bDoneI1 == false)
        {
            i1_x = i1_x + 0.0027f;

            if (i1_x >= -letter_x * 2)
            {
                i1_x = -letter_x * 2.0f;
                bDoneI1 = true;
            }
        }
        else if (bDoneA == false)
        {
            a_x = a_x - 0.0027f;

            if (a_x <= letter_x * 2)
            {
                a_x = letter_x * 2.0f;
                bDoneA = true;
            }
        }
        else if (bDoneN == false)
        {
            n_y = n_y - 0.0027f;

            if (n_y <= 0.0f)
            {
                bDoneN = true;
                n_y = 0.0f;
            }
        }
        else if (bDoneI2 == false)
        {
            i2_y = i2_y + 0.0027f;

            if (i2_y >= 0.0f)
            {
                i2_y = 0.0f;
                bDoneI2 = true;
            }
        }
        else if (bDoneD == false)
        {
            if (bDoneR_saffron == false)
            {
                r_saffron = r_saffron + 2.5f;
                if (r_saffron >= 255.0f)
                {
                    r_saffron = 255.0f;
                    bDoneR_saffron = true;
                }
            }
            if (bDoneG_saffron == false)
            {
                g_saffron = g_saffron + 1.0f;
                if (g_saffron >= 153.0f)
                {
                    g_saffron = 153.0f;
                    bDoneG_saffron = true;
                }
            }
            if (bDoneB_saffron == false)
            {
                b_saffron = b_saffron + 0.2f;
                if (b_saffron >= 51.0f)
                {
                    b_saffron = 51.0f;
                    bDoneB_saffron = true;
                }
            }

            // green
            if (bDoneR_green == false)
            {
                r_green = r_green + 0.1f;
                if (r_green >= 18.0f)
                {
                    r_green = 18.0f;
                    bDoneR_green = true;
                }
            }
            if (bDoneG_green == false)
            {
                g_green = g_green + 0.7f;
                if (g_green >= 136.0f)
                {
                    g_green = 136.0f;
                    bDoneG_green = true;
                }
            }
            if (bDoneB_green == false)
            {
                b_green = b_green + 0.1f;
                if (b_green >= 7.0f)
                {
                    b_green = 7.0f;
                    bDoneB_green = true;
                }
            }
            if (bDoneR_saffron == true && bDoneG_saffron == true && bDoneB_saffron == true && bDoneR_green == true && bDoneG_green == true && bDoneB_green == true)
                bDoneD = true;
        }
        else if (bDoneUpperPlane == false)
        {
            middle_plane_x = middle_plane_x + 0.0049f;


            if (angle_upper_plane <= 0.0f)
                angle_upper_plane = angle_upper_plane + 0.45f;

            if (angle_lower_plane >= 0.0f)
                angle_lower_plane = angle_lower_plane - 0.45f;

            if (r*(float)Math.sin(angle) <= -1.22f)
            {
                bDoneUpperPlane = true;
                //fprintf_s(gpFile, "\nr*cos(angle)=%f", r*cos(angle));
                //fprintf_s(gpFile, "\nr*sin(angle)=%f", r*sin(angle));
                angle_upper_plane = 0.0f;
                angle_lower_plane = 0.0f;
            }

            angle = angle + 0.005f;
            angle_lower = angle_lower - 0.005f;
        }

        else if (bDoneMidddlePlane == false)
        {
            middle_plane_x = middle_plane_x + 0.0049f;
            if (middle_plane_x >= 2.0f*letter_x - letter_end + 0.724918f)
                bDoneMidddlePlane = true;
        }

        else if (bDoneDetachPlanes == false)
        {
            middle_plane_x = middle_plane_x + 0.0049f;

            angle_right_upper = angle_right_upper + 0.005f;
            angle_right_lower = angle_right_lower - 0.005f;

            if (angle_upper_plane <= 90.0f)
                angle_upper_plane = angle_upper_plane + 0.47f;

            if (angle_lower_plane >= -90.0f)
                angle_lower_plane = angle_lower_plane - 0.47f;

            if (middle_plane_x >= letter_x * 6.0f)
                bDoneDetachPlanes = true;
        }
        else if (bDoneFadingSmokes == false)
        {
            if (r_saffron_smoke > 0.0f)
                r_saffron_smoke = r_saffron_smoke - 2.5f;
            if (g_saffron_smoke > 0.0f)
                g_saffron_smoke = g_saffron_smoke - 1.0f;
            if (b_saffron_smoke > 0.0f)
                b_saffron_smoke = b_saffron_smoke - 0.2f;

            if (r_white_smoke > 0.0f)
                r_white_smoke = r_white_smoke - 0.03f;

            if (r_green_smoke > 0.0f)
                r_green_smoke = r_green_smoke - 0.1f;
            if (g_green_smoke > 0.0f)
                g_green_smoke = g_green_smoke - 0.7f;
            if (b_green_smoke > 0.0f)
                b_green_smoke = b_green_smoke - 0.1f;

            if (r_saffron_smoke <= 0 && g_saffron_smoke <= 0 && b_saffron_smoke <= 0 && r_white_smoke<=0 && r_green_smoke <= 0 && g_green_smoke <= 0 && b_green_smoke <= 0)
                bDoneFadingSmokes = true;
        }
    }

    private void Uninitialize()
    {
        // opposite sequence of vao and vbo also ok
        if (vbo_color_I[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_color_I, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_color_I[0] = 0;
        }

        if (vbo_position_I[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_position_I, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_position_I[0] = 0;
        }

        if (vao_I[0]!=0)
        {
            GLES32.glDeleteVertexArrays(1, vao_I, 0);		// delete vertex array objects
            vao_I[0] = 0;
        }

        if (vbo_color_N[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_color_N, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_color_N[0] = 0;
        }

        if (vbo_position_N[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_position_N, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_position_N[0] = 0;
        }

        if (vao_N[0]!=0)
        {
            GLES32.glDeleteVertexArrays(1, vao_N, 0);		// delete vertex array objects
            vao_N[0] = 0;
        }

        if (vbo_color_D[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_color_D, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_color_D[0] = 0;
        }

        if (vbo_position_D[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_position_D, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_position_D[0] = 0;
        }

        if (vao_D[0]!=0)
        {
            GLES32.glDeleteVertexArrays(1, vao_D, 0);		// delete vertex array objects
            vao_D[0] = 0;
        }

        if (vbo_color_A[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_color_A, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_color_A[0] = 0;
        }

        if (vbo_position_A[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_position_A, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_position_A[0] = 0;
        }

        if (vao_A[0]!=0)
        {
            GLES32.glDeleteVertexArrays(1, vao_A, 0);		// delete vertex array objects
            vao_A[0] = 0;
        }

        if (vbo_color_A_band[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_color_A_band, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_color_A_band[0] = 0;
        }

        if (vbo_position_A_band[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_position_A_band, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_position_A_band[0] = 0;
        }

        if (vao_A_band[0]!=0)
        {
            GLES32.glDeleteVertexArrays(1, vao_A_band, 0);		// delete vertex array objects
            vao_A_band[0] = 0;
        }

        if (vbo_position_plane[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_position_plane, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_position_plane[0] = 0;
        }

        if (vao_plane[0]!=0)
        {
            GLES32.glDeleteVertexArrays(1, vao_plane, 0);		// delete vertex array objects
            vao_plane[0] = 0;
        }

        if (vbo_position_smoke[0]!=0)          // cannot write 'if(vao)' or 'if(vao[0])' in java
        {
            GLES32.glDeleteBuffers(1, vbo_position_smoke, 0);	// delete named buffer objects
                                                        // 1: number of buffer objects to be deleted
                                                        // &vbo: array of buffer objects to be deleted
            vbo_position_smoke[0] = 0;
        }

        if (vao_smoke[0]!=0)
        {
            GLES32.glDeleteVertexArrays(1, vao_smoke, 0);		// delete vertex array objects
            vao_smoke[0] = 0;
        }

        if (shaderProgramObject!=0)
        {
            int[] shaderCount=new int[1];	// since we want addr of shaderCount
            int shaderNumber;

            GLES32.glUseProgram(shaderProgramObject);		// since unused in Display()
            
            // ask program that how many shaders are attached to it
            GLES32.glGetProgramiv(shaderProgramObject,
                GLES32.GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
                shaderCount,
                0);

            int[] shaders = new int[4 * shaderCount[0]];		// dynamic array for shaders since we don't know how many present; 4: size of int in java

            if (shaders[0]!=0)	// mem allocated
            {
                // take attached shaders into above array
                GLES32.glGetAttachedShaders(shaderProgramObject,	// the program object to be queried
                    shaderCount[0],								    // the size of the array for storing the returned object names
                    shaderCount,								    // Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
                    0,
                    shaders,								        // an array that is used to return the names of attached shader objects(empty now),
                    0
                );		// return the handles of the shader objects attached to a program object

                for (shaderNumber = 0; shaderNumber < shaderCount[0]; shaderNumber++)
                {
                    // detach each shader
                    GLES32.glDetachShader(shaderProgramObject, shaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
                                                                                        // 2nd para: Specifies the shader object to be detached.

                    // delete each detached shader
                    GLES32.glDeleteShader(shaders[shaderNumber]);
                    shaders[shaderNumber] = 0;
                }
            }
            GLES32.glUseProgram(0);

            // delete the shader program
            GLES32.glDeleteProgram(shaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
            shaderProgramObject = 0;
        }
	}
}
