<<<<<<< HEAD
#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include "Teapot.h"
#include "ModelUsingHeader.h"
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// OGL's rendering context
bool gbIsFullScreen = false;
bool gbActiveWindow = false;
FILE *gpFile = NULL;
FILE *gpTraceLog = NULL;
GLfloat angle_teapot = 0.0f;
GLuint texture_marble;
bool bTexture = false;
bool bLight = false;
GLfloat LightAmbient[] = { 0.5f,0.5f,0.5f,1.0f };	// inline initialization; R,G,B,A
GLfloat LightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 0.0f,0.0f,0.0f,1.0f };	// x,y,z,w	
													
// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// trace log file creation code
	if (fopen_s(&gpTraceLog, "SD_TraceLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT(" TraceLog File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf_s(gpTraceLog, "In WinMain()\n");

	// function declarations
	int InitializeOGL(void);
	void DisplayOGL(void);
	void Update(void);

	// variable declarations
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// file creation code
	if (fopen_s(&gpFile, "SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully\n");
	}
	// code
	// initialization of wndclass
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// Centering The Window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,	// Forces a top-level window onto the taskbar when the window is visible.
		szAppName,
		TEXT("Sameera-OGL Window With Double Buffer With Triangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,	// clip the child windows and sibling windows of this window
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = InitializeOGL();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initialization Function Succeeded\n");
	}

	ShowWindow(hwnd, iCmdShow);
	//UpdateWindow(hwnd);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage.
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// call Update() here
				Update();
			}
			// call Display() here 
			DisplayOGL();
		}
	}
	fprintf_s(gpTraceLog, "Exiting WinMain()\n");

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	fprintf_s(gpTraceLog, "In WndProc()\n");

	// function declarations
	void ToggleFullScreen();
	void ResizeOGL(int, int);
	//void DisplayOGL(void);
	void UninitializeOGL(void);

	// code 
	switch (iMsg)
	{
	case WM_CHAR:
		fprintf_s(gpTraceLog, "In WM_CHAR message\n");

		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		case 'T':
		case 't':
			if (bTexture == false)
			{
				bTexture = true;
				glEnable(GL_TEXTURE_2D);	// to enable light sources and thus lighting calculations
										//glEnable(GL_LIGHT0);	// need to explicitly enable due to above call
			}
			else
			{
				bTexture = false;
				glDisable(GL_TEXTURE_2D);	// // to enable light sources and thus lighting calculations
										//glDisable(GL_LIGHT0);	
			}
			break;

		case 'L':
		case 'l':
			if (bLight == false)
			{
				bLight = true;
				glEnable(GL_LIGHTING);	// to enable light sources and thus lighting calculations
										//glEnable(GL_LIGHT0);	// need to explicitly enable due to above call
			}
			else
			{
				bLight = false;
				glDisable(GL_LIGHTING);	// // to enable light sources and thus lighting calculations
										//glDisable(GL_LIGHT0);	
			}
			break;
		}
		break;

	case WM_SETFOCUS:
		fprintf_s(gpTraceLog, "In WM_SETFOCUS message\n");
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		fprintf_s(gpTraceLog, "In WM_KILLFOCUS message\n");
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		fprintf_s(gpTraceLog, "In WM_SIZE message\n");
		ResizeOGL(LOWORD(lParam), HIWORD(lParam));
		break;

		/*case WM_PAINT:
		DisplayOGL();
		break;*/

	case WM_ERASEBKGND:
		fprintf_s(gpTraceLog, "In WM_ERASEBKGND message\n");
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.	// not invoked?
		fprintf_s(gpTraceLog, "In WM_CLOSE message\n");
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		fprintf_s(gpTraceLog, "In WM_KEYDOWN message\n");
		switch (wParam)
		{
		case VK_ESCAPE:
			fprintf_s(gpTraceLog, "In VK_ESCAPE key\n");
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		fprintf_s(gpTraceLog, "In WM_DESTROY message\n");
		UninitializeOGL();
		PostQuitMessage(0);
		break;
	}
	fprintf_s(gpTraceLog, "Exiting WndProc()\n");

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int InitializeOGL()
{
	fprintf_s(gpTraceLog, "In InitializeOGL()\n");

	// function declarations
	void ResizeOGL(int, int);
	BOOL loadTexture(GLuint*, TCHAR[]);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;	// paints onto window, supports OpenGL, double buffer
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32;	// R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														//ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
														// returns indices starting from 1. Hence, 0 means error
	if (iPixelFormatIndex == 0)
		return(-1);

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
		return(-2);

	// ask OS to give new RC
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL
	if (ghrc == NULL)
		return(-3);
	// now ghrc can do 3D rendering too

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context.
		return(-4);

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//glEnable(GL_TEXTURE_2D);	// If enabled, two-dimensional texturing is performed

	// load the texture in texture_marble
	loadTexture(&texture_marble, MAKEINTRESOURCE(ID_BITMAP_MARBLE));	// MAKEINTRESOURCE() macro takes int value(101) of your resource and converts into string(TCHAR)

	// configuration of lights using configuration parameters (setting parameters)
	glLightfv(
		GL_LIGHT0,	// a light (OGL light no. 0 out of the 8 lights)
		GL_AMBIENT,	// parameter for light; to specify the ambient(sorrounding/encircling) RGBA intensity for light
		LightAmbient // pointer to the array whose values will be given to GL_AMBIENT; default : (0.0, 0.0, 0.0, 1.0)
	);	// f= float, v= vector

	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);	// to specify the diffuse(to be spread over) RGBA intensity of the light
													// The default diffuse intensity is (0.0, 0.0, 0.0, 1.0) for all lights other than light zero. 
													// The default diffuse intensity of light zero is (1.0, 1.0, 1.0, 1.0). 

	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);	// specifies the position of the light in homogeneous object coordinates
														// The position is transformed by the modelview matrix when glLight is called (just as if it were a point), and it is stored in eye coordinates.
														// The default position is (0,0,1,0); thus, the default light source is directional (since w=0), parallel to, and in the direction of the z axis. 

	glEnable(GL_LIGHT0);	// need to explicitly enable due to glEnable(GL_LIGHTING);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// clear the screen by OGL color
											// specifies clear values[0,1] used by glClear() for the color buffers

											// warmup call to ResizeOGL()
	ResizeOGL(WIN_WIDTH, WIN_HEIGHT);

	fprintf_s(gpTraceLog, "Exiting InitializeOGL()\n");

	return(0);	// successful exit status
}

BOOL loadTexture(GLuint *texture, TCHAR imageResourceID[])	// TCHAR imageResourceID[] - string
{
	// variable declarations
	HBITMAP hBitmap = NULL;	// Handles refer to a resource that has been loaded into memory.
							// typedef HANDLE HBITMAP;	
							// typedef PVOID HANDLE;
							// typedef void* PVOID;

	BITMAP bmp;	/* typedef struct tagBITMAP {LONG bmType; LONG bmWidth; LONG bmHeight; LONG bmWidthBytes; WORD bmPlanes; WORD bmBitsPixel; LPVOID bmBits;}BITMAP; */

	BOOL bStatus = FALSE;

	// code
	hBitmap = (HBITMAP)LoadImage(
		GetModuleHandle(NULL),	// returns instance handle of the process which has the resource (hInstance of WinMain)
		imageResourceID,		// id of the resource we want to convert into Bitmap
		IMAGE_BITMAP,			// type of image (bitmap)
		0,						// width of the image (uses the actual resource width)
		0,						// height of the image (uses the actual resource height)
		LR_CREATEDIBSECTION		// how to load bitmap;	DIB = Device(GC) Independent Bitmap
	);	// Image can be icon, cursor or bitmap.

	if (hBitmap)
	{
		bStatus = TRUE;

		// get image data (into bmp)
		GetObject(hBitmap, sizeof(BITMAP), &bmp);	// retrieves information for the specified graphics object
													// hBitmap : handle to graphics object
													// sizeof(BITMAP) : number of bytes of information to be written to the buffer
													// &bmp : Pointer to a buffer that receives the information about the specified graphics object

													// OpenGL functions now

													// set pixel storage mode
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// GL_UNPACK_ALIGNMENT : Specifies the alignment requirements for the start of each pixel row in memory
												// 4 : word alignment (R,G,B,A)

												// allocate memory for textures on GPU and obtain its id in 'texture'
		glGenTextures(1, texture);	// 1 : how many textures to generate
									// texture : starting addr of the array of textures returned as an id

									// bind every element of the array to a target texture (type of texture)
		glBindTexture(GL_TEXTURE_2D, *texture);	// binding the first texture(value) to a 2D array type usage
												// *Texture targets become aliases for textures currently bound to them*

												// set the state/parameters of the texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// when model is near the viewer; pixel being textured maps to an area lesser than one texture element
																			// sets the texture magnification function to either GL_NEAREST(better performance) or GL_LINEAR(better quality due to interpolation)
																			// GL_TEXTURE_MAG_FILTER : parameter to be tweaked
																			// GL_LINEAR : value of the parameter(this is int : i)

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);	// when model is away from the viewer; pixel being textured maps to an area greater than one texture element
																						// GL_LINEAR_MIPMAP_LINEAR : Maintain GL_LINEAR at all mipmap levels; A mipmap is an ordered set of arrays representing the same image at progressively lower resolutions

																						// fill the data
		gluBuild2DMipmaps(
			GL_TEXTURE_2D,		// where to fill data; the target texture
			3,					// the number of color components in the texture (3 = R,G,B)
			bmp.bmWidth,		// width of the texture image to be filled
			bmp.bmHeight,		// height of the texture image to be filled
			GL_BGR_EXT,			// format of the pixel data; BGR = reverse of RGB; BGR_EXT = extended form of GL_BGR
			GL_UNSIGNED_BYTE,	// the data type for data (last parameter)
			bmp.bmBits			// actual data : Pointer to the location of the bit values for the bitmap
		);	// builds a series of prefiltered two-dimensional texture maps of decreasing resolutions called a mipmap

			// delete hBitmap
		DeleteObject(hBitmap);	// deletes a resource
	}
	return(bStatus);
}

void ResizeOGL(int width, int height)
{
	fprintf_s(gpTraceLog, "In ResizeOGL()\n");

	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	fprintf_s(gpTraceLog, "Exiting ResizeOGL()\n");
}

void DisplayOGL(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

	glMatrixMode(GL_MODELVIEW);	// specifies which matrix is the current matrix.

	glLoadIdentity();	// replaces the current matrix with the identity matrix.

	glTranslatef(0.0f, 0.0f, -1.5f);	// pushing the object 4 units on -ve z-axis	// why moving model first?

	//glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);	// to make the model straight

	//glRotatef(angle_teapot, 0.0f, 0.0f, 1.0f);	// axes are now rotated

	glRotatef(angle_teapot, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	for (int i = 0;i < sizeof(face_indicies) / sizeof(face_indicies[0]);i++)	// to get number of elements of face_indicies array
	{
		for (int j = 0;j < 3;j++)
		{
			int vi = face_indicies[i][j];	// vertex index
			int ni = face_indicies[i][j+3];	// normal index
			int ti = face_indicies[i][j+6];	// texture index

			glTexCoord2f(textures[ti][0], textures[ti][1]);	// no effect if no texture
			glNormal3f(normals[ni][0], normals[ni][1],normals[ni][2]);
			glVertex3f(vertices[vi][0], vertices[vi][1], vertices[vi][2]);
		}
	}

	glEnd();

	SwapBuffers(ghdc);	// exchanges the front and back buffers if the current pixel format for the window referenced by the specified device context includes a back buffer(double buffer)
}


void Update()
{
	angle_teapot = angle_teapot + 1.0f;

	if (angle_teapot >= 360.0f)
		angle_teapot = 0.0f;
}

void UninitializeOGL()
{
	fprintf_s(gpTraceLog, "In UninitializeOGL()\n");

	glDeleteTextures(1, &texture_marble);

	// check if fullscreen. If yes, restore to normal size and proceed for uninitialization
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);	// sets the placement of the window

		SetWindowPos(ghwnd,
			HWND_TOP,	// insert after
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);	// sets the z-order of the window
																							// SWP_NOZORDER: Retains the current Z order 
																							// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
																							// SWP_NOMOVE: Retains the current position
																							// SWP_NOSIZE: Retains the current size
																							// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.

		ShowCursor(TRUE);
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	//Parameters: HDC, HGLRC; If hglrc is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. In this case, hdc is ignored.

		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpFile, "Log File Closed Successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
	fprintf_s(gpTraceLog, "Exiting UninitializeOGL()\n");
}

void ToggleFullScreen()
{
	fprintf_s(gpTraceLog, "In ToggleFullScreen()\n");

	MONITORINFO mi;

	if (gbIsFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };	// Setting cbSize field of MONITORINFO. Doing so lets GetMonitorInfo() determine the type of structure you are passing to it.

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))	// MonitorFromWindow() gives HMONITORINFO
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,
					HWND_TOP,	// A handle to the window to precede the positioned window in the Z order; HWND_TOP places the window at the top of the z-order
					mi.rcMonitor.left,	// x;	rcMonitor is a RECT struct
					mi.rcMonitor.top,	//y
					mi.rcMonitor.right - mi.rcMonitor.left,	//width
					mi.rcMonitor.bottom - mi.rcMonitor.top,	//ht
					SWP_NOZORDER | SWP_FRAMECHANGED);	//flags
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);	// sets the placement of the window

		SetWindowPos(ghwnd,
			HWND_TOP,	// has no effect although written since SWP_NOZORDER flag(which uses last z-order) used below
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);	// sets the z-order of the window

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}
	fprintf_s(gpTraceLog, "Exiting ToggleFullScreen()\n");
}
=======
#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#include "Teapot.h"
#include "ModelUsingHeader.h"
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// OGL's rendering context
bool gbIsFullScreen = false;
bool gbActiveWindow = false;
FILE *gpFile = NULL;
FILE *gpTraceLog = NULL;
GLfloat angle_teapot = 0.0f;
GLuint texture_marble;
bool bTexture = false;
bool bLight = false;
GLfloat LightAmbient[] = { 0.5f,0.5f,0.5f,1.0f };	// inline initialization; R,G,B,A
GLfloat LightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat LightPosition[] = { 0.0f,0.0f,0.0f,1.0f };	// x,y,z,w	
													
// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// trace log file creation code
	if (fopen_s(&gpTraceLog, "SD_TraceLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT(" TraceLog File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf_s(gpTraceLog, "In WinMain()\n");

	// function declarations
	int InitializeOGL(void);
	void DisplayOGL(void);
	void Update(void);

	// variable declarations
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// file creation code
	if (fopen_s(&gpFile, "SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully\n");
	}
	// code
	// initialization of wndclass
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// Centering The Window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,	// Forces a top-level window onto the taskbar when the window is visible.
		szAppName,
		TEXT("Sameera-OGL Window With Double Buffer With Triangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,	// clip the child windows and sibling windows of this window
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = InitializeOGL();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initialization Function Succeeded\n");
	}

	ShowWindow(hwnd, iCmdShow);
	//UpdateWindow(hwnd);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage.
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// call Update() here
				Update();
			}
			// call Display() here 
			DisplayOGL();
		}
	}
	fprintf_s(gpTraceLog, "Exiting WinMain()\n");

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	fprintf_s(gpTraceLog, "In WndProc()\n");

	// function declarations
	void ToggleFullScreen();
	void ResizeOGL(int, int);
	//void DisplayOGL(void);
	void UninitializeOGL(void);

	// code 
	switch (iMsg)
	{
	case WM_CHAR:
		fprintf_s(gpTraceLog, "In WM_CHAR message\n");

		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		case 'T':
		case 't':
			if (bTexture == false)
			{
				bTexture = true;
				glEnable(GL_TEXTURE_2D);	// to enable light sources and thus lighting calculations
										//glEnable(GL_LIGHT0);	// need to explicitly enable due to above call
			}
			else
			{
				bTexture = false;
				glDisable(GL_TEXTURE_2D);	// // to enable light sources and thus lighting calculations
										//glDisable(GL_LIGHT0);	
			}
			break;

		case 'L':
		case 'l':
			if (bLight == false)
			{
				bLight = true;
				glEnable(GL_LIGHTING);	// to enable light sources and thus lighting calculations
										//glEnable(GL_LIGHT0);	// need to explicitly enable due to above call
			}
			else
			{
				bLight = false;
				glDisable(GL_LIGHTING);	// // to enable light sources and thus lighting calculations
										//glDisable(GL_LIGHT0);	
			}
			break;
		}
		break;

	case WM_SETFOCUS:
		fprintf_s(gpTraceLog, "In WM_SETFOCUS message\n");
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		fprintf_s(gpTraceLog, "In WM_KILLFOCUS message\n");
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		fprintf_s(gpTraceLog, "In WM_SIZE message\n");
		ResizeOGL(LOWORD(lParam), HIWORD(lParam));
		break;

		/*case WM_PAINT:
		DisplayOGL();
		break;*/

	case WM_ERASEBKGND:
		fprintf_s(gpTraceLog, "In WM_ERASEBKGND message\n");
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.	// not invoked?
		fprintf_s(gpTraceLog, "In WM_CLOSE message\n");
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		fprintf_s(gpTraceLog, "In WM_KEYDOWN message\n");
		switch (wParam)
		{
		case VK_ESCAPE:
			fprintf_s(gpTraceLog, "In VK_ESCAPE key\n");
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		fprintf_s(gpTraceLog, "In WM_DESTROY message\n");
		UninitializeOGL();
		PostQuitMessage(0);
		break;
	}
	fprintf_s(gpTraceLog, "Exiting WndProc()\n");

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int InitializeOGL()
{
	fprintf_s(gpTraceLog, "In InitializeOGL()\n");

	// function declarations
	void ResizeOGL(int, int);
	BOOL loadTexture(GLuint*, TCHAR[]);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;	// paints onto window, supports OpenGL, double buffer
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32;	// R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														//ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
														// returns indices starting from 1. Hence, 0 means error
	if (iPixelFormatIndex == 0)
		return(-1);

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
		return(-2);

	// ask OS to give new RC
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL
	if (ghrc == NULL)
		return(-3);
	// now ghrc can do 3D rendering too

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context.
		return(-4);

	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	//glEnable(GL_TEXTURE_2D);	// If enabled, two-dimensional texturing is performed

	// load the texture in texture_marble
	loadTexture(&texture_marble, MAKEINTRESOURCE(ID_BITMAP_MARBLE));	// MAKEINTRESOURCE() macro takes int value(101) of your resource and converts into string(TCHAR)

	// configuration of lights using configuration parameters (setting parameters)
	glLightfv(
		GL_LIGHT0,	// a light (OGL light no. 0 out of the 8 lights)
		GL_AMBIENT,	// parameter for light; to specify the ambient(sorrounding/encircling) RGBA intensity for light
		LightAmbient // pointer to the array whose values will be given to GL_AMBIENT; default : (0.0, 0.0, 0.0, 1.0)
	);	// f= float, v= vector

	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);	// to specify the diffuse(to be spread over) RGBA intensity of the light
													// The default diffuse intensity is (0.0, 0.0, 0.0, 1.0) for all lights other than light zero. 
													// The default diffuse intensity of light zero is (1.0, 1.0, 1.0, 1.0). 

	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);	// specifies the position of the light in homogeneous object coordinates
														// The position is transformed by the modelview matrix when glLight is called (just as if it were a point), and it is stored in eye coordinates.
														// The default position is (0,0,1,0); thus, the default light source is directional (since w=0), parallel to, and in the direction of the z axis. 

	glEnable(GL_LIGHT0);	// need to explicitly enable due to glEnable(GL_LIGHTING);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// clear the screen by OGL color
											// specifies clear values[0,1] used by glClear() for the color buffers

											// warmup call to ResizeOGL()
	ResizeOGL(WIN_WIDTH, WIN_HEIGHT);

	fprintf_s(gpTraceLog, "Exiting InitializeOGL()\n");

	return(0);	// successful exit status
}

BOOL loadTexture(GLuint *texture, TCHAR imageResourceID[])	// TCHAR imageResourceID[] - string
{
	// variable declarations
	HBITMAP hBitmap = NULL;	// Handles refer to a resource that has been loaded into memory.
							// typedef HANDLE HBITMAP;	
							// typedef PVOID HANDLE;
							// typedef void* PVOID;

	BITMAP bmp;	/* typedef struct tagBITMAP {LONG bmType; LONG bmWidth; LONG bmHeight; LONG bmWidthBytes; WORD bmPlanes; WORD bmBitsPixel; LPVOID bmBits;}BITMAP; */

	BOOL bStatus = FALSE;

	// code
	hBitmap = (HBITMAP)LoadImage(
		GetModuleHandle(NULL),	// returns instance handle of the process which has the resource (hInstance of WinMain)
		imageResourceID,		// id of the resource we want to convert into Bitmap
		IMAGE_BITMAP,			// type of image (bitmap)
		0,						// width of the image (uses the actual resource width)
		0,						// height of the image (uses the actual resource height)
		LR_CREATEDIBSECTION		// how to load bitmap;	DIB = Device(GC) Independent Bitmap
	);	// Image can be icon, cursor or bitmap.

	if (hBitmap)
	{
		bStatus = TRUE;

		// get image data (into bmp)
		GetObject(hBitmap, sizeof(BITMAP), &bmp);	// retrieves information for the specified graphics object
													// hBitmap : handle to graphics object
													// sizeof(BITMAP) : number of bytes of information to be written to the buffer
													// &bmp : Pointer to a buffer that receives the information about the specified graphics object

													// OpenGL functions now

													// set pixel storage mode
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// GL_UNPACK_ALIGNMENT : Specifies the alignment requirements for the start of each pixel row in memory
												// 4 : word alignment (R,G,B,A)

												// allocate memory for textures on GPU and obtain its id in 'texture'
		glGenTextures(1, texture);	// 1 : how many textures to generate
									// texture : starting addr of the array of textures returned as an id

									// bind every element of the array to a target texture (type of texture)
		glBindTexture(GL_TEXTURE_2D, *texture);	// binding the first texture(value) to a 2D array type usage
												// *Texture targets become aliases for textures currently bound to them*

												// set the state/parameters of the texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// when model is near the viewer; pixel being textured maps to an area lesser than one texture element
																			// sets the texture magnification function to either GL_NEAREST(better performance) or GL_LINEAR(better quality due to interpolation)
																			// GL_TEXTURE_MAG_FILTER : parameter to be tweaked
																			// GL_LINEAR : value of the parameter(this is int : i)

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);	// when model is away from the viewer; pixel being textured maps to an area greater than one texture element
																						// GL_LINEAR_MIPMAP_LINEAR : Maintain GL_LINEAR at all mipmap levels; A mipmap is an ordered set of arrays representing the same image at progressively lower resolutions

																						// fill the data
		gluBuild2DMipmaps(
			GL_TEXTURE_2D,		// where to fill data; the target texture
			3,					// the number of color components in the texture (3 = R,G,B)
			bmp.bmWidth,		// width of the texture image to be filled
			bmp.bmHeight,		// height of the texture image to be filled
			GL_BGR_EXT,			// format of the pixel data; BGR = reverse of RGB; BGR_EXT = extended form of GL_BGR
			GL_UNSIGNED_BYTE,	// the data type for data (last parameter)
			bmp.bmBits			// actual data : Pointer to the location of the bit values for the bitmap
		);	// builds a series of prefiltered two-dimensional texture maps of decreasing resolutions called a mipmap

			// delete hBitmap
		DeleteObject(hBitmap);	// deletes a resource
	}
	return(bStatus);
}

void ResizeOGL(int width, int height)
{
	fprintf_s(gpTraceLog, "In ResizeOGL()\n");

	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);

	fprintf_s(gpTraceLog, "Exiting ResizeOGL()\n");
}

void DisplayOGL(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

	glMatrixMode(GL_MODELVIEW);	// specifies which matrix is the current matrix.

	glLoadIdentity();	// replaces the current matrix with the identity matrix.

	glTranslatef(0.0f, 0.0f, -1.5f);	// pushing the object 4 units on -ve z-axis	// why moving model first?

	//glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);	// to make the model straight

	//glRotatef(angle_teapot, 0.0f, 0.0f, 1.0f);	// axes are now rotated

	glRotatef(angle_teapot, 0.0f, 1.0f, 0.0f);

	glBegin(GL_TRIANGLES);

	for (int i = 0;i < sizeof(face_indicies) / sizeof(face_indicies[0]);i++)	// to get number of elements of face_indicies array
	{
		for (int j = 0;j < 3;j++)
		{
			int vi = face_indicies[i][j];	// vertex index
			int ni = face_indicies[i][j+3];	// normal index
			int ti = face_indicies[i][j+6];	// texture index

			glTexCoord2f(textures[ti][0], textures[ti][1]);	// no effect if no texture
			glNormal3f(normals[ni][0], normals[ni][1],normals[ni][2]);
			glVertex3f(vertices[vi][0], vertices[vi][1], vertices[vi][2]);
		}
	}

	glEnd();

	SwapBuffers(ghdc);	// exchanges the front and back buffers if the current pixel format for the window referenced by the specified device context includes a back buffer(double buffer)
}


void Update()
{
	angle_teapot = angle_teapot + 1.0f;

	if (angle_teapot >= 360.0f)
		angle_teapot = 0.0f;
}

void UninitializeOGL()
{
	fprintf_s(gpTraceLog, "In UninitializeOGL()\n");

	glDeleteTextures(1, &texture_marble);

	// check if fullscreen. If yes, restore to normal size and proceed for uninitialization
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);	// sets the placement of the window

		SetWindowPos(ghwnd,
			HWND_TOP,	// insert after
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);	// sets the z-order of the window
																							// SWP_NOZORDER: Retains the current Z order 
																							// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
																							// SWP_NOMOVE: Retains the current position
																							// SWP_NOSIZE: Retains the current size
																							// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.

		ShowCursor(TRUE);
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	//Parameters: HDC, HGLRC; If hglrc is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. In this case, hdc is ignored.

		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpFile, "Log File Closed Successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
	fprintf_s(gpTraceLog, "Exiting UninitializeOGL()\n");
}

void ToggleFullScreen()
{
	fprintf_s(gpTraceLog, "In ToggleFullScreen()\n");

	MONITORINFO mi;

	if (gbIsFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };	// Setting cbSize field of MONITORINFO. Doing so lets GetMonitorInfo() determine the type of structure you are passing to it.

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))	// MonitorFromWindow() gives HMONITORINFO
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,
					HWND_TOP,	// A handle to the window to precede the positioned window in the Z order; HWND_TOP places the window at the top of the z-order
					mi.rcMonitor.left,	// x;	rcMonitor is a RECT struct
					mi.rcMonitor.top,	//y
					mi.rcMonitor.right - mi.rcMonitor.left,	//width
					mi.rcMonitor.bottom - mi.rcMonitor.top,	//ht
					SWP_NOZORDER | SWP_FRAMECHANGED);	//flags
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);	// sets the placement of the window

		SetWindowPos(ghwnd,
			HWND_TOP,	// has no effect although written since SWP_NOZORDER flag(which uses last z-order) used below
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);	// sets the z-order of the window

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}
	fprintf_s(gpTraceLog, "Exiting ToggleFullScreen()\n");
}
>>>>>>> 10725a07e625d8141639f717ef836b5bcf74d0ad
