#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc= NULL;	// OGL's rendering context
bool gbIsFullScreen = false;
bool gbIsActiveWindow = false;
FILE *gpFile = NULL;
FILE *gpTraceLog = NULL;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int Initialize(void);
	void Display(void);

	// variable declarations
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");

	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// code
	// trace log file creation code
	if (fopen_s(&gpTraceLog, "sd_TraceLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("TraceLog File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf_s(gpTraceLog, "\nIn WinMain()");

	// log file creation code
	if (fopen_s(&gpFile,"SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf_s(gpFile, "\nLog File Created Successfully");

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// CS_OWNDC: To avoid retrieving a device context each time it needs to paint inside a window
															// Directs the system to create a private/unique device context for each window in the class
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)(GetStockObject(BLACK_BRUSH));
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm= LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// centering the window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,	// Forces a top-level window onto the taskbar when the window is visible.
		szAppName,
		TEXT("Sameera_Application"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,	// clip the child windows and sibling windows of this window
																				// window is initially visible
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf_s(gpFile, "\nChoosePixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "\nSetPixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "\nwglCreateContext() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "\nwglMakeCurrent() Failed");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "\nInitialization Function Succeeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);	// gives a slightly higher priority to the thread creating this window
	SetFocus(hwnd);

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow == true)
			{
				// call Update() here
			}
			// call Display() here
			Display();
		}
	}
	fprintf_s(gpTraceLog, "\nExiting WinMain()");

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	fprintf_s(gpTraceLog, "\nIn WndProc()");

	// function declarations
	void ToggleFullscreen();
	void Resize(int, int);
	void Uninitialize();

	// code
	switch (iMsg)
	{
	case WM_CHAR:
		fprintf_s(gpTraceLog, "\nIn WM_CHAR message");

		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullscreen();
			break;
		}
		break;

	case WM_SETFOCUS:
		fprintf_s(gpTraceLog, "\nIn WM_SETFOCUS message");
		gbIsActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		fprintf_s(gpTraceLog, "\nIn WM_KILLFOCUS message");
		gbIsActiveWindow = false;
		break;

	case WM_SIZE:
		fprintf_s(gpTraceLog, "\nIn WM_SIZE message");
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:	//  sent when the window background must be erased (for example, when a window is resized). 
						// we are handling this msg since we don't want DefWindowProc() to post WM_PAINT message
		
		fprintf_s(gpTraceLog, "\nIn WM_ERASEBKGND message");
		return(0);	// we have an external painter

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.	// not invoked?
		fprintf_s(gpTraceLog, "\nIn WM_CLOSE message");
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		fprintf_s(gpTraceLog, "\nIn WM_KEYDOWN message");
		switch (wParam)
		{
		case VK_ESCAPE:
			fprintf_s(gpTraceLog, "\nIn VK_ESCAPE key");
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		fprintf_s(gpTraceLog, "\nIn WM_DESTROY message");
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	fprintf_s(gpTraceLog,"\nExiting WndProc()");

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize()
{
	fprintf_s(gpTraceLog, "\nIn Initialize()");

	// function declarations
	void Resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32;	// R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														//ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
	if (iPixelFormatIndex == 0)
		return(-1);

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
		return(-2);

	// ask OS to give new Rendering Context
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL
	if (ghrc == NULL)
		return(-3);
	// now ghrc can do 3D rendering too
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context.
		return(-4);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// clear the screen by OGL color
											// specifies clear values[0,1] used by glClear() for the color buffers
	Resize(WIN_WIDTH, WIN_HEIGHT);

	fprintf_s(gpTraceLog, "\nExiting Initialize()");

	return(0);	// successful exit status
}

void Resize(int width, int height)
{
	fprintf_s(gpTraceLog, "\nIn Resize()");

	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height,0.1f,100.0f);	// parameters:
																// fovy- The field of view angle, in degrees, in the y - direction.
																// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																//	zNear- The distance from the viewer to the near clipping plane(always positive).
																//	zFar- The distance from the viewer to the far clipping plane(always positive).*/
	fprintf_s(gpTraceLog,"\nExiting Resize()");
}

void Display()
{
	glClear(GL_COLOR_BUFFER_BIT);	// clears buffers to preset values.
									// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

	glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.

	glLoadIdentity();	// replaces the current matrix with the identity matrix.

	glTranslatef(0.0f, 0.0f, -3.0f);

	glColor3f(1.0f, 1.0f, 1.0f);

	glLineWidth(3.0f);

	glBegin(GL_LINES);

	glVertex3f(-1.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);

	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glVertex3f(1.0f, -1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	glVertex3f(-1.0f, -1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 0.0f);

	//--------------------------------------------------------------------------------------------------

	glVertex3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, 0.0f, 0.0f);

	glVertex3f(1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	glVertex3f(0.0f, -1.0f, 0.0f);
	glVertex3f(-1.0f, 0.0f, 0.0f);

	glVertex3f(-1.0f, 0.0f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//--------------------------------------------------------------------------------------------------

	glVertex3f(-1.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);

	glVertex3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 0.0f);

	glEnd();

	SwapBuffers(ghdc);
}

void Uninitialize()
{
	fprintf_s(gpTraceLog,"\nIn Uninitialize()");

	// check if fullscreen. If yes, restore to normal size and proceed for uninitialization
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);	// sets the placement of the window

		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);	// sets the z-order of the window
																							// SWP_NOZORDER: Retains the current Z order 
																							// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
																							// SWP_NOMOVE: Retains the current position
																							// SWP_NOSIZE: Retains the current size
																							// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.
		ShowCursor(TRUE);
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	// Parameters: HDC, HGLRC; 
									// If hglrc is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. 
									// In this case, hdc is ignored.
		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpTraceLog,"\nLog File Closed successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
	fprintf_s(gpTraceLog, "\nExiting Uninitialize()");
}

void ToggleFullscreen()
{
	fprintf_s(gpTraceLog, "\nIn ToggleFullscreen()");

	// variable declarations
	MONITORINFO mi;

	// code
	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };	// Setting cbSize field of MONITORINFO. Doing so lets GetMonitorInfo() determine the type of structure you are passing to it.

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))	// MonitorFromWindow() gives HMONITORINFO
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,
					HWND_TOP,
					mi.rcMonitor.left,	// x
					mi.rcMonitor.top,	// y
					mi.rcMonitor.right - mi.rcMonitor.left,	// width
					mi.rcMonitor.bottom - mi.rcMonitor.top,	// height
					SWP_NOZORDER | SWP_FRAMECHANGED);	// flags
			}
		}
		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);

		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE |SWP_NOSIZE| SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbIsFullScreen = false;

		fprintf_s(gpTraceLog, "\nExiting ToggleFullScreen()");
	}
}
