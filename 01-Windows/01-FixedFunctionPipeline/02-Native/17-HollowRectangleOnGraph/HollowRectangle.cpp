#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// OGL's rendering context
bool gbIsFullScreen = false;
bool gbActiveWindow = false;
FILE *gpFile = NULL;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int InitializeOGL(void);
	void DisplayOGL(void);

	// variable declarations
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// file creation code
	if (fopen_s(&gpFile, "SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully\n");
	}
	// code
	// initialization of wndclass
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// Centering The Window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,	// Forces a top-level window onto the taskbar when the window is visible.
		szAppName,
		TEXT("Sameera-OGL Window With Double Buffer Dot"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,	// clip the child windows and sibling windows of this window
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = InitializeOGL();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initialization Function Succeeded\n");
	}

	ShowWindow(hwnd, iCmdShow);
	//UpdateWindow(hwnd);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage.
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// call Update() here
			}
			// call Display() here 
			DisplayOGL();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(void);
	void ResizeOGL(int, int);
	//void DisplayOGL(void);
	void UninitializeOGL(void);

	// code 
	switch (iMsg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;
		}
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		ResizeOGL(LOWORD(lParam), HIWORD(lParam));
		break;

		/*case WM_PAINT:
		DisplayOGL();
		break;*/

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		UninitializeOGL();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int InitializeOGL()
{
	// function declarations
	void ResizeOGL(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;	// paints onto window, supports OpenGL, double buffer
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32;	// R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														//ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
														// returns indices starting from 1. Hence, 0 means error
	if (iPixelFormatIndex == 0)
		return(-1);

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
		return(-2);

	// ask OS to give new RC
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL
	if (ghrc == NULL)
		return(-3);
	// now ghrc can do 3D rendering too

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context.
		return(-4);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// clear the screen by OGL color
											// specifies clear values[0,1] used by glClear() for the color buffers

											// warmup call to ResizeOGL()
	ResizeOGL(WIN_WIDTH, WIN_HEIGHT);

	return(0);	// successful exit status
}

void ResizeOGL(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// ? other values of near & far not working
}

void DisplayOGL(void)
{
	// function declarations
	void DrawGraph();
	void DrawHollowRectangle();

	// code
	glClear(GL_COLOR_BUFFER_BIT);	// clears buffers to preset values.
									// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

	glMatrixMode(GL_MODELVIEW);	// specifies which matrix is the current matrix; Applies subsequent matrix operations to the modelview matrix stack. 

	glLoadIdentity();	// replaces the current matrix with the identity matrix.

						// graph paper

	glTranslatef(0.0f, 0.0f, -3.0f);	// pushing the object 3 units on -ve z-axis	// why moving model first?

	DrawGraph();

	// triangle

	glLineWidth(3.0);	// specifies the width of rasterized lines.

	DrawHollowRectangle();

	SwapBuffers(ghdc);	// exchanges the front and back buffers if the current pixel format for the window referenced by the specified device context includes a back buffer(double buffer)
}

void DrawGraph()
{
	// variable declarations
	int i;
	float j = -1.0f;

	// code
	// x-axis :

	glLineWidth(3.0f);	// specifies the width of rasterized lines.

	glBegin(GL_LINES);	// parameters: The primitive or primitives that will be created from vertices presented between glBegin() and the subsequent glend()

	glColor3f(1.0f, 0.0f, 0.0f);	// sets the RGB color
	glVertex3f(-1.0f, 0.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glVertex3f(1.0f, 0.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.

									//-----------------------------------------------------------------------------------------------------------------------------------

									// y-axis :

	glColor3f(0.0f, 1.0f, 0.0f);	// sets the RGB color
	glVertex3f(0.0f, -1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glVertex3f(0.0f, 1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.

	glEnd();

	//-----------------------------------------------------------------------------------------------------------------------------------

	// horizontal lines	:

	glLineWidth(1.0);	// specifies the width of rasterized lines.

	glBegin(GL_LINES);	// parameters: The primitive or primitives that will be created from vertices presented between glBegin() and the subsequent glend()

	glColor3f(0.0f, 0.0f, 1.0f);	// sets the RGB color

	for (i = 0;i < 41;i++)	// 41  lines 
	{

		if (i == 20)	// ignore if x-axis
		{
			j = (j + (float)1 / 20);	// 20 blue lines = 21 parts
			continue;
		}

		glVertex3f(-1.0f, j, 0.0f);	// Specifies the x and y coordinates of a vertex.
		glVertex3f(1.0f, j, 0.0f);	// Specifies the x and y coordinates of a vertex.

		j = (j + (float)1 / 20);	// 20 blue lines = 21 parts
	}

	//-----------------------------------------------------------------------------------------------------------------------------------

	// vertical lines :
	j = -1;

	for (i = 0;i < 41;i++)	// 41  lines 
	{
		if (i == 20)	// ignore if y-axis
		{
			j = (j + (float)1 / 20);	// 20 blue lines = 21 parts
			continue;
		}

		glVertex3f(j, -1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.
		glVertex3f(j, 1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.

		j = (j + (float)1 / 20);	// 20 blue lines = 21 parts
	}

	glEnd();

	//-----------------------------------------------------------------------------------------------------------------------------------

	// origin dot

	glPointSize(7.0f);	// specifies the diameter of rasterized points  (?)

	glBegin(GL_POINTS);	// parameters: The primitive or primitives that will be created from vertices presented between glBegin() and the subsequent glend()

	glColor3f(1.0f, 1.0f, 1.0f);	// sets the RGB color
	glVertex3f(0.0f, 0.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.

	glEnd();

}

void DrawHollowRectangle()
{
	glBegin(GL_LINES);	// parameters: The primitive or primitives that will be created from vertices presented between glBegin() and the subsequent glend()

	glColor3f(1.0f, 1.0f, 0.0f);	// sets the RGB color

	glVertex3f(0.5f, 0.5f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glVertex3f(-0.5f, 0.5f, 0.0f);	// Specifies the x and y coordinates of a vertex.

	glVertex3f(-0.5f, 0.5f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glVertex3f(-0.5f, -0.5f, 0.0f);	// Specifies the x and y coordinates of a vertex.

	glVertex3f(-0.5f, -0.5f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glVertex3f(0.5f, -0.5f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	
	glVertex3f(0.5f, -0.5f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glVertex3f(0.5f, 0.5f, 0.0f);	// Specifies the x and y coordinates of a vertex.

	glEnd();
}

void UninitializeOGL()
{
	// check if fullscreen. If yes, restore to normal size and proceed for uninitialization
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);	// sets the placement of the window

		SetWindowPos(ghwnd,
			HWND_TOP,	// insert after
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);	// sets the z-order of the window
																							// SWP_NOZORDER: Retains the current Z order 
																							// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
																							// SWP_NOMOVE: Retains the current position
																							// SWP_NOSIZE: Retains the current size
																							// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.

		ShowCursor(TRUE);
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	//If hglrc is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. In this case, hdc is ignored.

		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpFile, "Log File Closed Successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbIsFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };	// Setting cbSize field of MONITORINFO. Doing so lets GetMonitorInfo() determine the type of structure you are passing to it.

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))	// MonitorFromWindow() gives HMONITORINFO
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,
					HWND_TOP,	// A handle to the window to precede the positioned window in the Z order; HWND_TOP places the window at the top of the z-order
					mi.rcMonitor.left,	// x;	rcMonitor is a RECT struct
					mi.rcMonitor.top,	//y
					mi.rcMonitor.right - mi.rcMonitor.left,	//width
					mi.rcMonitor.bottom - mi.rcMonitor.top,	//ht
					SWP_NOZORDER | SWP_FRAMECHANGED);	//flags
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);	// sets the placement of the window

		SetWindowPos(ghwnd,
			HWND_TOP,	// has no effect although written since SWP_NOZORDER flag(which uses last z-order) used below
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);	// sets the z-order of the window

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}
}

