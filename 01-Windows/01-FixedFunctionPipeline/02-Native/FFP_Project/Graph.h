#ifndef _GRAPH_H
#define _GRAPH_H

#include<stdio.h>
#include<stdlib.h>

#define TRUE 1
#define FALSE 0
#define SUCCESS 1
#define FAILURE 0
#define G_INVALID_VERTEX 2
#define G_INVALID_EDGE 3

struct hnode;
struct vnode;
struct edge;
struct graph;

typedef struct hnode hnode_t;
typedef struct vnode vnode_t;
typedef hnode_t hlist_t;
typedef vnode_t vlist_t;
typedef struct edge edge_t;
typedef struct graph graph_t;
//typedef int bool;
typedef int vertex_t;
typedef int ret_t;
typedef int len_t;
typedef char color_t;
//typedef enum{WHITE=0,GRAY,BLACK}color_t;

struct hnode
{
	vertex_t vertex;
	double w;
	struct hnode *prev;
	struct hnode *next;
};

struct vnode
{
	vertex_t vertex;
	vertex_t pred_vertex;
	hnode_t *ph_head_node;
	color_t color;
	vnode_t *prev;
	vnode_t *next;
};

struct edge
{
	vertex_t v_start;
	vertex_t v_end;
};

struct graph
{
	vlist_t *pv_head_node;
	unsigned int nr_vertices;
	unsigned int nr_edges;
};

// graph interface routines
graph_t *create_graph(void);
ret_t add_vertex(graph_t *g);
ret_t add_edge(graph_t *g,vertex_t v_start,vertex_t v_end);
ret_t remove_edge(graph_t *g, vertex_t v_start, vertex_t v_end);
ret_t remove_vertex(graph_t *g, vertex_t vertex);
ret_t degree(graph_t *g, vertex_t vertex, int *p_degree);
void print_graph(graph_t *g);
void color_graph(graph_t *g);
ret_t destroy_graph(graph_t **p_g);
color_t get_color(graph_t *g,int);
ret_t get_next_number(void);

// graph auxilliary routines
static int get_next_vertex_number(void);
static void find_and_color_adjacent_vertices(graph_t*, color_t *, int, int);

// horizontal list management
// interface
static hlist_t *h_create_list();
static ret_t h_insert_end(hlist_t *ph_lst, vertex_t vertex);
static len_t h_len(hlist_t *ph_lst);
static ret_t h_destroy_list(hlist_t *ph_lst);

// auxilliary 
static hnode_t *h_get_node(vertex_t vertex);
static void h_generic_insert(hnode_t *p_beg, hnode_t *p_mid, hnode_t *p_end);
static void h_generic_delete(hnode_t *p_node);
static hnode_t *h_search_node(hlist_t *ph_lst, vertex_t vertex);

// vertical list management
// interface
static vlist_t *v_create_list();
static ret_t v_insert_end(vlist_t *pv_lst, vertex_t vertex);
static len_t v_len(vlist_t *pv_lst);
static ret_t v_destroy_list(vlist_t *pv_lst);

// auxilliary 
static vnode_t *v_get_node(vertex_t vertex);
static void v_generic_insert(vnode_t *p_beg, vnode_t *p_mid, vnode_t *p_end);
static void v_generic_delete(vnode_t *p_node);
static vnode_t *v_search_node(vlist_t *pv_lst, vertex_t vertex);

// auxilliary functions
static void *xcalloc(size_t n, size_t s);

#endif
