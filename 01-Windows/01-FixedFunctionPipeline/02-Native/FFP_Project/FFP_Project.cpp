#include<stdio.h>
#include<stdlib.h>
#include<assert.h>	
#include "Graph.h"
#define _USE_MATH_DEFINES 1
#include<math.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc;
HGLRC ghrc = NULL;	// OGL's rendering context
bool gbIsFullScreen = false;
bool gbActiveWindow = false;
FILE *gpFile = NULL;
FILE *gpTraceLog = NULL;
extern FILE *gpGraph = NULL;
GLUquadric *quadric = NULL;
graph_t *gr = NULL;
bool bPressedG = false,bPressedC=false,bDoneColoringAllNodes=false, bSplashScreen=true;
bool bDrawFirstNode = false, bDrawSecondNode = false, bDrawThirdNode = false, bDrawFourthNode = false, bDrawFifthNode = false, bDrawSixthNode = false, bDrawSeventhNode = false, bDrawEighthNode = false, bDrawNinthNode = false, bDrawTenthNode = false;
bool bDrawFirstEdge = false, bDrawSecondEdge = false, bDrawThirdEdge = false, bDrawFourthEdge = false, bDrawFifthEdge = false, bDrawSixthEdge = false, bDrawSeventhEdge = false, bDrawEighthEdge = false, bDrawNinthEdge = false, bDrawTenthEdge = false, bDrawEleventhEdge = false, bDrawTwelthEdge = false, bDrawThirteenthEdge = false, bDrawFourteenthEdge = false;
extern bool bColorFirstNode = false, bColorSecondNode = false, bColorThirdNode = false, bColorFourthNode = false, bColorFifthNode = false, bColorSixthNode = false, bColorSeventhNode = false, bColorEighthNode = false, bColorNinthNode = false, bColorTenthNode = false;
bool bLight = false;
GLYPHMETRICSFLOAT *gmf;
unsigned int base;
int counter;
GLfloat LightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };	// inline initialization; R,G,B,A
GLfloat LightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };	// R,G,B,A
GLfloat LightPosition[] = { 0.0f,0.0f,0.0f,1.0f };	// x,y,z,w
//GLfloat LightSpecular[] = { 1.0f,1.0f,1.0f,1.0f };	// R,G,B,A

// lighting models
GLfloat light_model_ambient[] = { 0.2f,0.2f,0.2f,1.0f };	// also default
GLfloat light_model_local_viewer[] = { 0.0f };

// material
GLfloat MaterialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };	// inline initialization; R,G,B,A
GLfloat MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };	// R,G,B,A
GLfloat MaterialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };	// R,G,B,A
GLfloat MaterialShininess[] = { 50.0f };	// or 128.0f

// for fonts
GLfloat MaterialDiffuse1[] = { 1.0f,1.0f,0.0f,1.0f };	// R,G,B,A
GLfloat MaterialDiffuse2[] = { 0.0f,1.0f,1.0f,1.0f };	// R,G,B,A

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// trace log file creation code
	if (fopen_s(&gpTraceLog, "SD_TraceLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("TraceLog File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf_s(gpTraceLog, "In WinMain()\n");

	// function declarations
	int Initialize(void);
	void Display(void);
	void Update(void);

	// variable declarations
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = "MyApp";

	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// code
	// file creation code
	if (fopen_s(&gpFile, "SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully\n");
	}

	if (fopen_s(&gpGraph, "SD_GraphLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Graph Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpGraph, "Graph Log File Created Successfully\n");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// centering the window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,	// Forces a top-level window onto the taskbar when the window is visible.
		szAppName,
		TEXT("Sameera - FFP Project"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,	// clip the child windows and sibling windows of this window
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = Initialize();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "\nChoosePixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "\nSetPixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "\nwglCreateContext() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "\nwglMakecurrent() Failed");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "\nInitialization Function Succeeded");
	}
	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);	// window should be at top
	SetFocus(hwnd);	// window's title bar,etc. should be highlighted

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage()
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				Update();
			}
			Display();
		}
	}
	fprintf_s(gpTraceLog, "Exiting WinMain()\n");

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	fprintf_s(gpTraceLog, "In WndProc()\n");

	// function declarations
	void Resize(int, int);
	void Uninitialize(void);
	void CheckNextNode(void);

	// variable declarations:
	static bool bDoneColoringAllNodes = false;
	static int c_count = 0;

	// code
	switch (iMsg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 'G':
		case 'g':
			if (bPressedG == false)
				bPressedG = true;
			break;

		case 'C':
		case 'c':
			if (bDrawFourteenthEdge==true && bPressedC == false)
				bPressedC = true;
			break;
		}
		break;

	case WM_SETFOCUS:
		gbActiveWindow = true;
		break;

	case  WM_KILLFOCUS:
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_SPACE:
			bSplashScreen = false;
			break;

		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	fprintf_s(gpTraceLog, "Exiting WndProc()\n");

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize()
{
	fprintf_s(gpTraceLog, "In Initialize()\n");

	// function declarations
	void Resize(int, int);
	void MakeFullScreen();
	unsigned int Create_Font();

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;	// paints onto window, supports OpenGL, double buffer
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32; // R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														//ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
														// returns indices starting from 1. Hence, 0 means error
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
	{
		return(-2);
	}
	// ask OS to give new RC
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL

	if (ghrc == NULL)
	{
		return(-3);
	}
	// now ghrc can do 3D rendering too

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context. 
	{
		return(-4);
	}

	// depth related statements
	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	glShadeModel(GL_SMOOTH);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glClearDepth(1.0f);

	// lights
	glEnable(GL_AUTO_NORMAL);	// tell OpenGL to give normals

	glEnable(GL_NORMALIZE);		// If enabled, normal vectors specified with glNormal() are scaled to unit length after transformation
								// gives good quality; uses mathematical calculations, hence less performance

	// configuration of lights using configuration parameters (setting parameters)
	glLightfv(
		GL_LIGHT0,	// a light (OGL light no. 0 out of the 8 lights)
		GL_AMBIENT,	// parameter for light; to specify the ambient(sorrounding/encircling) RGBA intensity for light
		LightAmbient // pointer to the array whose values will be given to GL_AMBIENT; default : (0.0, 0.0, 0.0, 1.0)
	);	// f= float, v= vector

	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);	// to specify the diffuse(to be spread over) RGBA intensity of the light
													// The default diffuse intensity is (0.0, 0.0, 0.0, 1.0) for all lights other than light zero. 
													// The default diffuse intensity of light zero is (1.0, 1.0, 1.0, 1.0). 

	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);	// specifies the position of the light in homogeneous object coordinates
														// The position is transformed by the modelview matrix when glLight is called (just as if it were a point), and it is stored in eye coordinates.
														// The default position is (0,0,1,0); thus, the default light source is directional (since w=0), parallel to, and in the direction of the z axis. 

	// set the lighting model(overall lighting) parameters
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);	// GL_LIGHT_MODEL_AMBIENT: to specify the ambient RGBA intensity of the entire scene, 
																	// light_model_ambient: values

	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);	// GL_LIGHT_MODEL_LOCAL_VIEWER: specifies how specular reflection angles are computed
																			// light_model_local_viewer: a single integer or floating-point value.
																			// If 2nd parameter is 0 (or 0.0), specular reflection angles take the view direction to be parallel to and in the direction of the z axis, regardless of the location of the vertex in eye coordinates. 
																			// Otherwise specular reflections are computed from the origin of the eye coordinate system. The default is 0. 
																			//		glEnable(GL_LIGHTING);	// to enable light sources and thus lighting calculations
	//glEnable(GL_LIGHTING);	// to enable light sources and thus lighting calculations

	//glEnable(GL_LIGHT0);	// need to explicitly enable due to explicit call to glEnable(GL_LIGHTING) in case 'l/L'

	// specify material parameters for the lighting model
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);	// GL_FRONT : The face or faces that are being updated (since ours is steady sphere)
															// GL_AMBIENT : The material parameter of the face or faces being updated and their interpretations by the lighting equation
															// MaterialAmbient : specify the ambient RGBA reflectance of the material.
															// The default ambient reflectance for both front- and back-facing materials is (0.2, 0.2, 0.2, 1.0). 

	//glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);	// specify the diffuse RGBA reflectance of the material. 
															//  The default diffuse reflectance for both front- and back-facing materials is (0.8, 0.8, 0.8, 1.0). 

	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);	// specify the diffuse RGBA reflectance of the material. 
															// The default diffuse reflectance for both front- and back-facing materials is (0.0, 0.0, 0.0, 1.0). 

	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);	// a single value that specifies the RGBA specular exponent of the material
																// Only values in the range[0, 128] are accepted.
																// The default specular exponent for both front - and back - facing materials is 0.
	Create_Font();

	//glClearColor(253.0f / 255.0f, 207.0f / 255.0f, 130.0f / 255.0f, 1.0f);	// clear the screen by OGL color	
	glClearColor(48.0f / 255.0f, 188.0f / 255.0f, 250.0f / 255.0f, 1.0f);	// clear the screen by OGL color	

	//Resize(WIN_WIDTH, WIN_HEIGHT);
	MakeFullScreen();

	gr = create_graph();

	fprintf_s(gpTraceLog, "Exiting Initialize()\n");

	return(0);
}

void Resize(int width, int height)
{
	fprintf_s(gpTraceLog, "In Resize()\n");
	
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).
	
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// ? other values of near & far not working
																			// parameters:
																			// fovy- The field of view angle, in degrees, in the y - direction.
																			// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																			//	zNear- The distance from the viewer to the near clipping plane(always positive).
																			//	zFar- The distance from the viewer to the far clipping plane(always positive).*/
	fprintf_s(gpTraceLog, "Exiting Resize()\n");
}

void Display()
{
	// function declarations
	void AddNode(void);
	void DrawNode(GLfloat, GLfloat);
	void AddEdge(GLint,GLint);
	void DrawEdge(GLfloat, GLfloat, GLfloat, GLfloat);
	void ColorNode(int);
	void GiveDefaultColor(void);
	void RenderFont(GLfloat, GLfloat, char*);

	// variable declarations 
	char str[] = "GRAPH  COLORING  PROBLEM :";
	char str1[] = "1.Press G/g to create graph";
	char str2[] = "2.Press C/c to see colors as they get assigned to nodes";
	char str3[] = "Thus, the nodes have been colored with minimum colors such that no two connected nodes have the same color !";
	char str4[] = "Problem :";
	char str5[] = "To color the nodes of a graph such that -";
	char str6[] = "1.Minimum number of colors should be used.";
	char str7[] = "2.Connected (adjacent) nodes should not have the same color.";
	char str8[] = "Strategy :";
	char str9[] = "1.Color a node and all its non-adjacent nodes.";
	char str10[] = "2.Repeat above step with the next non-colored node and with a new color.";
	char str11[] = "Press space bar for visualization !";

	static bool bNode1 = false, bNode2 = false, bNode3 = false, bNode4 = false, bNode5 = false, bNode6 = false, bNode7 = false, bNode8 = false, bNode9 = false, bNode10 = false;
	static bool bEdge1_2 = false, bEdge2_3 = false, bEdge3_4 = false, bEdge1_5 = false, bEdge2_6 = false, bEdge5_6 = false, bEdge4_6 = false, bEdge2_5 = false, bEdge3_6 = false, bEdge5_7 = false, bEdge2_8 = false, bEdge8_4 = false, bEdge3_9 = false, bEdge9_10 = false;
	
	// code
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(0.0f, 0.0f, -3.0f);

	if (bSplashScreen == true)
	{
		//glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse3);
		glColor3f(213.0f/255.0f, 4.0f / 255.0f, 83.0f / 255.0f);
		RenderFont(-2.9f, 4.2f, str);
		glColor3f(2.0f/255.0f, 172.0f/255.0f, 2.0f / 255.0f);
		RenderFont(-4.9f, 2.4f, str4);
		glColor3f(1.0f, 1.0f, 1.0f);
		RenderFont(-4.9f, 1.6f, str5);
		RenderFont(-4.9f, 1.0f, str6);
		RenderFont(-4.9f, 0.4f, str7);
		glColor3f(2.0f / 255.0f, 172.0f / 255.0f, 2.0f / 255.0f);
		RenderFont(-4.9f, -1.2f, str8);
		glColor3f(1.0f, 1.0f, 1.0f);
		RenderFont(-4.9f, -2.0f, str9);
		RenderFont(-4.9f, -2.6f, str10);
		glColor3f(0.0f, 0.0f, 1.0f);
		RenderFont(-2.6f, -5.0f, str11);
	}
	else
	{
		glEnable(GL_LIGHTING);	// to enable light sources and thus lighting calculations

		glEnable(GL_LIGHT0);	// need to explicitly enable due to explicit call to glEnable(GL_LIGHTING) in case 'l/L'
		glClearColor(0.25f, 0.25f, 0.25f, 1.0f);	// clear the screen by OGL color
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

		// render fonts
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse1);
		RenderFont(-10.0f, 5.0f, str);
		if (bDrawFourteenthEdge == false)
		{
			glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse2);
			RenderFont(-9.5f, -4.5f, str1);
		}
		else if (bDoneColoringAllNodes == false && bDrawFourteenthEdge == true)
		{
			glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse2);
			RenderFont(-9.5f, -4.5f, str2);
		}
		else if (bDoneColoringAllNodes == true)
		{
			glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse2);
			RenderFont(-9.5f, -5.5f, str3);
		}
		// nodes
		if (bDrawFirstNode == true)
		{
			if (bNode1 == false)
			{
				AddNode();
				bNode1 = true;
			}
			if (bColorFirstNode == true)
				ColorNode(1);
			else
				GiveDefaultColor();
			DrawNode(-1.32f, 0.406666f);
		}
		if (bDrawSecondNode == true)
		{
			if (bNode2 == false)
			{
				AddNode();
				bNode2 = true;
			}
			if (bColorSecondNode == true)
				ColorNode(2);
			else
				GiveDefaultColor();
			DrawNode(-0.44f, 0.406666f);
		}
		if (bDrawThirdNode == true)
		{
			if (bNode3 == false)
			{
				AddNode();
				bNode3 = true;
			}
			if (bColorThirdNode == true)
				ColorNode(3);
			else
				GiveDefaultColor();
			DrawNode(0.44f, 0.406666f);
		}
		if (bDrawFourthNode == true)
		{
			if (bNode4 == false)
			{
				AddNode();
				bNode4 = true;
			}
			if (bColorFourthNode == true)
				ColorNode(4);
			else
				GiveDefaultColor();
			DrawNode(1.32f, 0.406666f);
		}
		if (bDrawFifthNode == true)
		{
			if (bNode5 == false)
			{
				AddNode();
				bNode5 = true;
			}
			if (bColorFifthNode == true)
				ColorNode(5);
			else
				GiveDefaultColor();
			DrawNode(-0.44f, -0.406666f);
		}
		if (bDrawSixthNode == true)
		{
			if (bNode6 == false)
			{
				AddNode();
				bNode6 = true;
			}
			if (bColorSixthNode == true)
				ColorNode(6);
			else
				GiveDefaultColor();
			DrawNode(0.44f, -0.406666f);
		}
		if (bDrawSeventhNode == true)
		{
			if (bNode7 == false)
			{
				AddNode();
				bNode7 = true;
			}
			if (bColorSeventhNode == true)
				ColorNode(7);
			else
				GiveDefaultColor();
			DrawNode(-1.7f, 0.0f);
		}
		if (bDrawEighthNode == true)
		{
			if (bNode8 == false)
			{
				AddNode();
				bNode8 = true;
			}
			if (bColorEighthNode == true)
				ColorNode(8);
			else
				GiveDefaultColor();
			DrawNode(-0.44f, 1.0f);
		}
		if (bDrawNinthNode == true)
		{
			if (bNode9 == false)
			{
				AddNode();
				bNode9 = true;
			}
			if (bColorNinthNode == true)
				ColorNode(9);
			else
				GiveDefaultColor();
			DrawNode(0.8f, 0.9f);
		}
		if (bDrawTenthNode == true)
		{
			if (bNode10 == false)
			{
				AddNode();
				bNode10 = true;
			}
			if (bColorTenthNode == true)
				ColorNode(10);
			else
				GiveDefaultColor();
			DrawNode(1.2f, -0.7f);
		}

		// edges
		glLineWidth(8.0f);

		if (bDrawFirstEdge == true)
		{
			if (bEdge1_2 == false)
			{
				AddEdge(1, 2);
				bEdge1_2 = true;
			}
			glColor3f(1.0f, 1.0f, 1.0f);
			DrawEdge(-1.32f, 0.406666f, -0.44f, 0.406666f);
		}
		if (bDrawSecondEdge == true)
		{
			if (bEdge2_3 == false)
			{
				AddEdge(2, 3);
				bEdge2_3 = true;
			}
			DrawEdge(-0.44f, 0.406666f, 0.44f, 0.406666f);
		}
		if (bDrawThirdEdge == true)
		{
			if (bEdge3_4 == false)
			{
				AddEdge(3, 4);
				bEdge3_4 = true;
			}
			DrawEdge(0.44f, 0.406666f, 1.32f, 0.406666f);
		}
		if (bDrawFourthEdge == true)
		{
			if (bEdge1_5 == false)
			{
				AddEdge(1, 5);
				bEdge1_5 = true;
			}
			DrawEdge(-1.32f, 0.406666f, -0.44f, -0.406666f);
		}
		if (bDrawFifthEdge == true)
		{
			if (bEdge2_6 == false)
			{
				AddEdge(2, 6);
				bEdge2_6 = true;
			}
			DrawEdge(-0.44f, 0.406666f, 0.44f, -0.406666f);
		}
		if (bDrawSixthEdge == true)
		{
			if (bEdge5_6 == false)
			{
				AddEdge(5, 6);
				bEdge5_6 = true;
			}
			DrawEdge(-0.44f, -0.406666f, 0.44f, -0.406666f);
		}
		if (bDrawSeventhEdge == true)
		{
			if (bEdge4_6 == false)
			{
				AddEdge(4, 6);
				bEdge4_6 = true;
			}
			DrawEdge(1.32f, 0.406666f, 0.44f, -0.406666f);
		}
		if (bDrawEighthEdge == true)
		{
			if (bEdge2_5 == false)
			{
				AddEdge(2, 5);
				bEdge2_5 = true;
			}
			DrawEdge(-0.44f, 0.406666f, -0.44f, -0.406666f);
		}
		if (bDrawNinthEdge == true)
		{
			if (bEdge3_6 == false)
			{
				AddEdge(3, 6);
				bEdge3_6 = true;
			}
			DrawEdge(0.44f, 0.406666f, 0.44f, -0.406666f);
		}
		if (bDrawTenthEdge == true)
		{
			if (bEdge5_7 == false)
			{
				AddEdge(5, 7);
				bEdge5_7 = true;
			}
			DrawEdge(-0.44f, -0.406666f, -1.7f, 0.0f);
		}
		if (bDrawEleventhEdge == true)
		{
			if (bEdge2_8 == false)
			{
				AddEdge(2, 8);
				bEdge2_8 = true;
			}
			DrawEdge(-0.44f, 0.406666f, -0.44f, 1.0f);
		}
		if (bDrawTwelthEdge == true)
		{
			if (bEdge8_4 == false)
			{
				AddEdge(8, 4);
				bEdge8_4 = true;
			}
			DrawEdge(-0.44f, 1.0f, 1.32f, 0.406666f);
		}
		if (bDrawThirteenthEdge == true)
		{
			if (bEdge3_9 == false)
			{
				AddEdge(3, 9);
				bEdge3_9 = true;
			}
			DrawEdge(0.44f, 0.406666f, 0.8f, 0.9f);
		}
		if (bDrawFourteenthEdge == true)
		{
			if (bEdge9_10 == false)
			{
				AddEdge(9, 10);
				bEdge9_10 = true;
				color_graph(gr);
			}
			DrawEdge(0.8f, 0.9f, 1.2f, -0.7f);
		}
	}
	SwapBuffers(ghdc);	// exchanges the front and back buffers if the current pixel format for the window referenced by the specified device context includes a back buffer(double buffer)
}

void AddNode()
{
	assert(add_vertex(gr) == SUCCESS);
	fprintf_s(gpGraph, "\nAfter adding a node : ");
	print_graph(gr);
}

void DrawNode(GLfloat x, GLfloat y)
{
	// function declarations
	void DrawSphere(double, int, int);

	// code
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(x, y, -3.0f);
	quadric = gluNewQuadric();
	gluSphere(quadric, 0.15f, 30, 30);
	//DrawSphere(0.0000000001, 30, 30);
}

void AddEdge(GLint node1, GLint node2)
{
	assert(add_edge(gr, node1, node2) == SUCCESS);
	fprintf_s(gpGraph, "\nAfter adding an edge : ");
	print_graph(gr);
}

void DrawEdge(GLfloat node1_x, GLfloat node1_y, GLfloat node2_x, GLfloat node2_y)
{
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	/*glTranslatef(0.0f, 0.0f, -3.0f);
	glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
	gluCylinder(quadric, 0.15f, 0.15f, 0.88f, 30, 30);*/
	glBegin(GL_LINES);
	glVertex3f(node1_x, node1_y, -3.0f);
	glVertex3f(node2_x, node2_y, -3.0f);
	glEnd();
}

/*void DrawSphere(double r, int lats, int longs) 
{
	int i, j;
	for (i = 0; i <= lats; i++) 
	{
		double lat0 = M_PI * (-0.5 + (double)(i - 1) / lats);
		double z0 = sin(lat0);
		double zr0 = cos(lat0);

		double lat1 = M_PI * (-0.5 + (double)i / lats);
		double z1 = sin(lat1);
		double zr1 = cos(lat1);

		glBegin(GL_QUAD_STRIP);

		for (j = 0; j <= longs; j++)
		{
			double lng = 2 * M_PI * (double)(j - 1) / longs;
			double x = cos(lng);
			double y = sin(lng);

			glNormal3d(x * zr0, y * zr0, z0);
			glVertex3d(x * zr0, y * zr0, z0);
			glNormal3d(x * zr1, y * zr1, z1);
			glVertex3d(x * zr1, y * zr1, z1);
		}

		glEnd();
	}
}*/

void CheckNextNode()
{
	int n=0;

	if (counter % 50 == 0)
	{
		n = get_next_number();

		if (n == 1)
			bColorFirstNode = true;
		else if (n == 2)
			bColorSecondNode = true;
		else if (n == 3)
			bColorThirdNode = true;
		else if (n == 4)
			bColorFourthNode = true;
		else if (n == 5)
			bColorFifthNode = true;
		else if (n == 6)
			bColorSixthNode = true;
		else if (n == 7)
			bColorSeventhNode = true;
		else if (n == 8)
			bColorEighthNode = true;
		else if (n == 9)
			bColorNinthNode = true;
		else if (n == 10)
			bColorTenthNode = true;
	}
}

void ColorNode(int vertex)
{
	color_t color1;
	
	color1 = get_color(gr, vertex);

	if (color1 == 'R')
	{
		MaterialDiffuse[0] = 1.0f;	// r
		MaterialDiffuse[1] = 0.0f;	// g
		MaterialDiffuse[2] = 0.0f;	// b
		MaterialDiffuse[3] = 1.0f;	// a
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	}
	else if (color1 == 'G')
	{
		MaterialDiffuse[0] = 0.0f;	// r
		MaterialDiffuse[1] = 1.0f;	// g
		MaterialDiffuse[2] = 0.0f;	// b
		MaterialDiffuse[3] = 1.0f;	// a
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	}
	else if (color1 == 'B')
	{
		MaterialDiffuse[0] = 0.0f;	// r
		MaterialDiffuse[1] = 0.0f;	// g
		MaterialDiffuse[2] = 1.0f;	// b
		MaterialDiffuse[3] = 1.0f;	// a
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	}
	else if (color1 == 'Y')
	{
		MaterialDiffuse[0] = 1.0f;	// r
		MaterialDiffuse[1] = 1.0f;	// g
		MaterialDiffuse[2] = 0.0f;	// b
		MaterialDiffuse[3] = 1.0f;	// a
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	}
	else if (color1 == 'C')
	{
		MaterialDiffuse[0] = 0.0f;	// r
		MaterialDiffuse[1] = 1.0f;	// g
		MaterialDiffuse[2] = 1.0f;	// b
		MaterialDiffuse[3] = 1.0f;	// a
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	}
	else if (color1 == 'M')
	{
		MaterialDiffuse[0] = 1.0f;	// r
		MaterialDiffuse[1] = 0.0f;	// g
		MaterialDiffuse[2] = 1.0f;	// b
		MaterialDiffuse[3] = 1.0f;	// a
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	}
	else if (color1 == 'O')
	{
		MaterialDiffuse[0] = 1.0f;	// r
		MaterialDiffuse[1] = 0.5f;	// g
		MaterialDiffuse[2] = 39.0f/255.0f;	// b
		MaterialDiffuse[3] = 1.0f;	// a
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	}
	else if (color1 == 'P')
	{
		MaterialDiffuse[0] = 1.0f;	// r
		MaterialDiffuse[1] = 174.0f/255.0f;	// g
		MaterialDiffuse[2] = 201.0f/255.0f;	// b
		MaterialDiffuse[3] = 1.0f;	// a
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	}
	else if (color1 == 'X')
	{
		MaterialDiffuse[0] = 185.0f/255.0f;	// r
		MaterialDiffuse[1] = 122.0f/255.0f;	// g
		MaterialDiffuse[2] = 87.0f/255.0f;	// b
		MaterialDiffuse[3] = 1.0f;	// a
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	}
	else if (color1 == 'Z')
	{
		MaterialDiffuse[0] = 163.0f/255.0f;	// r
		MaterialDiffuse[1] = 73.0f/255.0f;	// g
		MaterialDiffuse[2] = 164.0f/255.0f;	// b
		MaterialDiffuse[3] = 1.0f;	// a
		glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
	}
}

void GiveDefaultColor(void)
{
	//for (int i = 0;i < 10000;i++);

	MaterialDiffuse[0] = 1.0f;	// r
	MaterialDiffuse[1] = 1.0f;	// g
	MaterialDiffuse[2] = 1.0f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);
}

void Update()
{
	static int count = 0;

	if (bPressedG == true)
		counter = counter + 1;

		if (counter == 100)
		{
			counter = 0;
			if (bDrawFirstNode == false)
				bDrawFirstNode = true;
			else if (bDrawSecondNode == false)
				bDrawSecondNode = true;
			else if (bDrawThirdNode == false)
				bDrawThirdNode = true;
			else if (bDrawFourthNode == false)
				bDrawFourthNode = true;
			else if (bDrawFifthNode == false)
				bDrawFifthNode = true;
			else if (bDrawSixthNode == false)
				bDrawSixthNode = true;
			else if (bDrawSeventhNode == false)
				bDrawSeventhNode = true;
			else if (bDrawEighthNode == false)
				bDrawEighthNode = true;
			else if (bDrawNinthNode == false)
				bDrawNinthNode = true;
			else if (bDrawTenthNode == false)
				bDrawTenthNode = true;

			else if (bDrawFirstEdge == false)
				bDrawFirstEdge = true;
			else if (bDrawSecondEdge == false)
				bDrawSecondEdge = true;
			else if (bDrawThirdEdge == false)
				bDrawThirdEdge = true;
			else if (bDrawFourthEdge == false)
				bDrawFourthEdge = true;
			else if (bDrawFifthEdge == false)
				bDrawFifthEdge = true;
			else if (bDrawSixthEdge == false)
				bDrawSixthEdge = true;
			else if (bDrawSeventhEdge == false)
				bDrawSeventhEdge = true;
			else if (bDrawEighthEdge == false)
				bDrawEighthEdge = true;
			else if (bDrawNinthEdge == false)
				bDrawNinthEdge = true;
			else if (bDrawTenthEdge == false)
				bDrawTenthEdge = true;
			else if (bDrawEleventhEdge == false)
				bDrawEleventhEdge = true;
			else if (bDrawThirteenthEdge == false)
				bDrawThirteenthEdge = true;
			else if (bDrawFourteenthEdge == false)
				bDrawFourteenthEdge = true;

			else if (bPressedC == true)
			{
				if (bDoneColoringAllNodes==false)
				{
					int n = get_next_number();
					if (n == 1)
						bColorFirstNode = true;
					else if (n == 2)
						bColorSecondNode = true;
					else if (n == 3)
						bColorThirdNode = true;
					else if (n == 4)
						bColorFourthNode = true;
					else if (n == 5)
						bColorFifthNode = true;
					else if (n == 6)
						bColorSixthNode = true;
					else if (n == 7)
						bColorSeventhNode = true;
					else if (n == 8)
						bColorEighthNode = true;
					else if (n == 9)
						bColorNinthNode = true;
					else if (n == 10)
						bColorTenthNode = true;

					count++;
					if (count >= 10)
						bDoneColoringAllNodes = true;
				}
			}
		}
}

unsigned int Create_Font()
{
	//variable declaration
	HFONT hFont;

	//code
	base = glGenLists(256);

	hFont = CreateFont(30, 1, 1, 0, FW_BOLD, FALSE, 0, FALSE, EASTEUROPE_CHARSET, OUT_DEFAULT_PRECIS, CLIP_CHARACTER_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DECORATIVE, NULL);

	if (!hFont)
		return(-1);

	SelectObject(ghdc, hFont);
	wglUseFontOutlines(ghdc, 0, 256, base, 0.1f, 0, WGL_FONT_POLYGONS, gmf);

	return(base);
}

void RenderFont(GLfloat x_position, GLfloat y_position, char *str)
{
	//code
	if ((base == 0) || (!str))
	{
		return;
	}

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(x_position, y_position, -15.0f);
	glPushAttrib(GL_LIST_BIT);

	glListBase(base);
	glCallLists((int)strlen(str), GL_UNSIGNED_BYTE, str);

	glPopAttrib();
}

void Uninitialize()
{
	fprintf_s(gpTraceLog, "In Uninitialize()\n");

	assert(destroy_graph(&gr) == SUCCESS && gr == NULL);
	fprintf_s(gpGraph,"\nGraph destroyed successfully");

	gluDeleteQuadric(quadric);

	// check if fullscreen. If yes, restore to normal size and proceed for uninitialization
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		// SWP_NOZORDER: Retains the current Z order 
		// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
		// SWP_NOMOVE: Retains the current position
		// SWP_NOSIZE: Retains the current size
		// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.

		ShowCursor(TRUE);
	}
	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	// If hglrc(2nd parameter) is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. In this case, hdc is ignored.

		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpFile, "\nLog File Closed Successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
	fprintf_s(gpTraceLog, "Exiting Uninitialize()\n");
}

void MakeFullScreen()
{
	fprintf_s(gpTraceLog, "\nIn MakeFullscreen()");

	// variable declarations
	MONITORINFO mi;

	dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
	if (dwStyle & WS_OVERLAPPEDWINDOW)
	{
		mi = { sizeof(MONITORINFO) };	// Setting cbSize field of MONITORINFO. Doing so lets GetMonitorInfo() determine the type of structure you are passing to it.

		if (GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))	// MonitorFromWindow() gives HMONITORINFO
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

			SetWindowPos(ghwnd,
				HWND_TOP,
				mi.rcMonitor.left,	// x
				mi.rcMonitor.top,	// y
				mi.rcMonitor.right - mi.rcMonitor.left,	// width
				mi.rcMonitor.bottom - mi.rcMonitor.top,	// height
				SWP_NOZORDER | SWP_FRAMECHANGED);	// flags
		}
	}
	ShowCursor(FALSE);
}
