#include<stdio.h>
#include<stdlib.h>
#include "Graph.h"

// global variable declarations
int colored_vertices[10] = { 0 };

// Graph Interface Routines
graph_t* create_graph(void)
{
	graph_t *g = NULL;

	g = (graph_t*)xcalloc(1, sizeof(graph_t));

	g->pv_head_node = v_create_list();
	g->nr_vertices = 0;
	g->nr_edges = 0;

	return(g);
}

ret_t add_vertex(graph_t *g)
{
	vertex_t curr_vertex_number = 0;
	curr_vertex_number = get_next_vertex_number();	// returns int
	v_insert_end(g->pv_head_node, curr_vertex_number);	
	g->nr_vertices = g->nr_vertices + 1;
	return(SUCCESS);
}

ret_t remove_vertex(graph_t *g, vertex_t vertex)
{
	vnode_t *pv_remove_node = NULL;
	vnode_t *pv_adj_node = NULL;
	hnode_t *ph_remove_in_adj_node = NULL;
	hnode_t *ph_run = NULL;
	hnode_t *ph_run_next = NULL;

	pv_remove_node = v_search_node(g->pv_head_node, vertex);
	if (pv_remove_node == NULL)
		return(G_INVALID_VERTEX);

	ph_run = pv_remove_node->ph_head_node->next;
	while (ph_run != pv_remove_node->ph_head_node)
	{
		ph_run_next = ph_run->next;

		pv_adj_node = v_search_node(g->pv_head_node, ph_run->vertex);
		if (pv_adj_node == NULL)
		{
			fprintf(stderr, "remove_vertex:unexpected error\n");
			exit(EXIT_FAILURE);
		}

		ph_remove_in_adj_node = h_search_node(pv_adj_node->ph_head_node,vertex);
		if (ph_remove_in_adj_node == NULL)
		{
			fprintf(stderr, "remove_vertex:unexpected error\n");
			exit(EXIT_FAILURE);
		}

		h_generic_delete(ph_remove_in_adj_node);
		h_generic_delete(ph_run);

		ph_run = ph_run_next;
	}
	v_generic_delete(pv_remove_node);
	return(SUCCESS);
}

ret_t add_edge(graph_t *g, vertex_t v_start, vertex_t v_end)
{
	vnode_t *pv_start_node = NULL;
	vnode_t *pv_end_node = NULL;

	ret_t ret;

	pv_start_node = v_search_node(g->pv_head_node, v_start);
	if (pv_start_node == NULL)
		return(G_INVALID_VERTEX);

	pv_end_node = v_search_node(g->pv_head_node, v_end);
	if (pv_end_node == NULL)
		return(G_INVALID_VERTEX);

	ret = h_insert_end(pv_start_node->ph_head_node, v_end);
	if (ret != SUCCESS)
	{
		fprintf(stderr, "add_edge:unexpected error\n");
		exit(EXIT_FAILURE);
	}

	ret = h_insert_end(pv_end_node->ph_head_node, v_start);
	if (ret != SUCCESS)
	{
		fprintf(stderr, "add_edge:unexpected error\n");
		exit(EXIT_FAILURE);
	}

	return(SUCCESS);
}

ret_t remove_edge(graph_t *g, vertex_t v_start, vertex_t v_end)
{
	vnode_t *pv_start_node = NULL;
	vnode_t *pv_end_node = NULL;
	hnode_t *ph_start_in_end_node = NULL;
	hnode_t *ph_end_in_start_node = NULL;

	pv_start_node = v_search_node(g->pv_head_node, v_start);
	if (pv_start_node == NULL)
		return(G_INVALID_VERTEX);

	pv_end_node = v_search_node(g->pv_head_node, v_end);
	if (pv_end_node == NULL)
		return(G_INVALID_VERTEX);

	ph_start_in_end_node = h_search_node(pv_end_node->ph_head_node, v_start);
	if (ph_start_in_end_node == NULL)
		return(G_INVALID_EDGE);

	ph_end_in_start_node = h_search_node(pv_start_node->ph_head_node, v_end);
	if (ph_end_in_start_node == NULL)
		return(G_INVALID_EDGE);

	h_generic_delete(ph_start_in_end_node);
	h_generic_delete(ph_end_in_start_node);

	return(SUCCESS);
}

ret_t degree(graph_t *g, vertex_t vertex, int *p_degree)
{
	vnode_t *pv_node = NULL;
	hnode_t *ph_run = NULL;
	len_t h_len = -1;

	pv_node = v_search_node(g->pv_head_node, vertex);
	if (pv_node == NULL)
		return(G_INVALID_VERTEX);

	ph_run = pv_node->ph_head_node->next;

	h_len = 0;

	while (ph_run != pv_node->ph_head_node)
	{
		h_len = h_len + 1;
		ph_run = ph_run->next;
	}
	*p_degree = h_len;
	return(SUCCESS);
}

void print_graph(graph_t *g)
{
	extern FILE *gpGraph;

	vnode_t *pv_run = NULL;
	hnode_t *ph_run = NULL;

	for (pv_run = g->pv_head_node->next;pv_run != g->pv_head_node;pv_run = pv_run->next)
	{
		fprintf_s(gpGraph,"\n[%d]:\t\t", pv_run->vertex);

		for (ph_run = pv_run->ph_head_node->next;ph_run != pv_run->ph_head_node;ph_run = ph_run->next)
		{
			fprintf_s(gpGraph, "[%d]:<->", ph_run->vertex);
		}
		fprintf_s(gpGraph, "[end]");
	}
}

void color_graph(graph_t *g)
{
	// variable declarations
	vnode_t *pv_run = NULL;
	color_t colors[] = { 'R','G','B','Y','C','M','O','P','X','Z' };
	int index_colors = 0;
	bool bDoneColoringAllNodes = false;
	extern FILE *gpGraph;

	// code
	pv_run = g->pv_head_node->next;
	while (bDoneColoringAllNodes == false)
	{
		if (pv_run->color == 'n')
		{
			pv_run->color = colors[index_colors];

			if (index_colors == 0)	// if R is assigned
				find_and_color_adjacent_vertices(g, colors, index_colors, pv_run->vertex);
			else if (index_colors == 1)	// if G is assigned
				find_and_color_adjacent_vertices(g, colors, index_colors, pv_run->vertex);
			else if (index_colors == 2)	// if B is assigned
				find_and_color_adjacent_vertices(g, colors, index_colors, pv_run->vertex);
			else if (index_colors == 3)	// if Y is assigned
				find_and_color_adjacent_vertices(g, colors, index_colors, pv_run->vertex);
			else if (index_colors == 4)	// if C is assigned
				find_and_color_adjacent_vertices(g, colors, index_colors, pv_run->vertex);
			else if (index_colors == 5)	// if M is assigned
				find_and_color_adjacent_vertices(g, colors, index_colors, pv_run->vertex);
			else if (index_colors == 6)	// if M is assigned
				find_and_color_adjacent_vertices(g, colors, index_colors, pv_run->vertex);
			else if (index_colors == 7)	// if M is assigned
				find_and_color_adjacent_vertices(g, colors, index_colors, pv_run->vertex);
			else if (index_colors == 8)	// if M is assigned
				find_and_color_adjacent_vertices(g, colors, index_colors, pv_run->vertex);
			else if (index_colors == 9)	// if M is assigned
				find_and_color_adjacent_vertices(g, colors, index_colors, pv_run->vertex);

			index_colors++;

		}// if pv_run->color==n

		pv_run = pv_run->next;
		if (pv_run == g->pv_head_node)
		{
			bDoneColoringAllNodes = true;
			break;
		}
	}// while

	fprintf_s(gpGraph, "\nColors assigned are :");
	for (pv_run = g->pv_head_node->next;pv_run != g->pv_head_node;pv_run = pv_run->next)
	{
		fprintf_s(gpGraph, "\n[%d]:\t", pv_run->vertex);
		fprintf_s(gpGraph, "%c", pv_run->color);
	}
}

color_t get_color(graph_t *g, int no)
{
	vnode_t *pv_vertex = NULL;
	color_t c;

	pv_vertex = v_search_node(g->pv_head_node, no);
	c = pv_vertex->color;
	
	return(c);
}

ret_t get_next_number()
{
	static int i = 0;
	int no;
	extern FILE *gpGraph;

	no = colored_vertices[i];
	i++;
	return(no);
}

ret_t destroy_graph(graph_t **pp_g)
{
	graph_t *g = NULL;
	vnode_t *pv_run = NULL;
	vnode_t *pv_run_next = NULL;
	hnode_t *ph_run = NULL;

	g = *pp_g;

	pv_run = g->pv_head_node->next;

	while (pv_run != g->pv_head_node)
	{
		if (h_destroy_list(pv_run->ph_head_node) != SUCCESS)
		{
			fprintf(stderr, "destroy_graph : unexpected error\n");
			exit(EXIT_FAILURE);
		}
		pv_run_next = pv_run->next;
		free(pv_run);
		pv_run = pv_run_next;
	}

	free(g->pv_head_node);
	free(g);
	g = NULL;
	*pp_g = NULL;
	return(SUCCESS);
}

// graph auxilliary routines
static int get_next_vertex_number(void)
{
	static int next_vertex_number = 0;
	return(++next_vertex_number);
}

static void find_and_color_adjacent_vertices(graph_t *g, color_t *colors, int index_colors, int pv_run_vertex)
{
	int colored_vertices_color[10] = { 0 }, index_coloredvertices_color = 0, currentindex_coloredvertices_color = 0;
	bool bVertexAlreadyColored;
	bool bAdjacent;
	int index_coloredvertices;
	static int currentindex_coloredvertices = 0;
	static int j = 0;
	vnode_t *pv_next_node = NULL;
	vnode_t *pv_coloredvertex = NULL;
	hnode_t *ph_adj_node = NULL;
	extern FILE *gpGraph;
	//extern bool bColorFirstNode, bColorSecondNode, bColorThirdNode, bColorFourthNode, bColorFifthNode, bColorSixthNode;

	colored_vertices[currentindex_coloredvertices] = pv_run_vertex;	// add newly colored vertex to colored array
	currentindex_coloredvertices++;

	fprintf_s(gpGraph, "\nVertex colored : %d", pv_run_vertex);

	//colored_vertices_sequence[j] = pv_next_node->vertex;
	//j++;

	colored_vertices_color[currentindex_coloredvertices_color] = pv_run_vertex;	// add newly colored vertex to that color's array
	currentindex_coloredvertices_color++;

	// find and color non-colored, non-adjacent nodes to 'that color' with 'that color'
	for (int i = 1;i <= 10;i++)
	{
		bVertexAlreadyColored = false;
		for (index_coloredvertices = 0;index_coloredvertices < currentindex_coloredvertices;index_coloredvertices++)
		{
			if (colored_vertices[index_coloredvertices] == i)
			{
				bVertexAlreadyColored = true;
				break;
			}
		}
		if (bVertexAlreadyColored == false)
		{
			bAdjacent = false;
			for (index_coloredvertices_color = 0; index_coloredvertices_color < currentindex_coloredvertices_color; index_coloredvertices_color++)
			{
				pv_coloredvertex = v_search_node(g->pv_head_node, colored_vertices_color[index_coloredvertices_color]);
				ph_adj_node = h_search_node(pv_coloredvertex->ph_head_node, i);
				if (ph_adj_node != NULL)
				{
					bAdjacent = true;
					break;
				}
			}
			if (bAdjacent == false)
			{
				pv_next_node = v_search_node(g->pv_head_node, i);
				pv_next_node->color = colors[index_colors];
				fprintf_s(gpGraph, "\nVertex colored : %d", pv_next_node->vertex);
				//colored_vertices_sequence[j] = pv_next_node->vertex;
				//j++;

				colored_vertices[currentindex_coloredvertices] = pv_next_node->vertex;	// add newly colored vertex to colored vertices array
				currentindex_coloredvertices++;

				colored_vertices_color[currentindex_coloredvertices_color] = pv_next_node->vertex;	// or i	// add newly colored vertex to that color's vertices array
				currentindex_coloredvertices_color++;
			}
		}// if already colored with that color==false
	}// for i
}

// horizontal list management
// interface
static hlist_t *h_create_list()
{
	hnode_t *ph_head_node = NULL;

	ph_head_node = (hnode_t*)h_get_node(-1);
	ph_head_node->prev = ph_head_node;
	ph_head_node->next = ph_head_node;

	return(ph_head_node);
}

static ret_t h_insert_end(hlist_t *ph_lst, vertex_t vertex)
{
	hnode_t *ph_new_node = NULL;

	ph_new_node = h_get_node(vertex);
	h_generic_insert(ph_lst->prev, ph_new_node, ph_lst);

	return(SUCCESS);
}

static len_t h_len(hlist_t *ph_lst)
{
	hnode_t *ph_run = NULL;
	len_t h_len = -1;

	ph_run = ph_lst->next;
	while (ph_run != ph_lst)
	{
		h_len = h_len + 1;
		ph_run = ph_run->next;
	}
	return(h_len);
}

static ret_t h_destroy_list(hlist_t *ph_lst)
{
	hnode_t *ph_run = NULL;
	hnode_t *ph_run_next = NULL;
	ph_run = ph_lst->next;

	while (ph_run != ph_lst)
	{
		ph_run_next = ph_run->next;
		free(ph_run);

		ph_run = ph_run_next;
	}
	free(ph_lst);
	ph_lst = NULL;
	return(SUCCESS);
}

// auxilliary
static hnode_t* h_get_node(vertex_t vertex)
{
	hnode_t *ph_node = NULL;

	ph_node = (hnode_t*)xcalloc(1, sizeof(hnode_t));
	ph_node->vertex = vertex;

	return(ph_node);
}

static void h_generic_insert(hnode_t *p_beg, hnode_t *p_mid, hnode_t *p_end)
{
	p_mid->next = p_end;
	p_mid->prev = p_beg;
	p_beg->next = p_mid;
	p_end->prev = p_mid;
}

static void h_generic_delete(hnode_t *p_node)
{
	p_node->next->prev = p_node->prev;
	p_node->prev->next = p_node->next;

	free(p_node);
	p_node = NULL;
}

static hnode_t *h_search_node(hlist_t *ph_lst, vertex_t vertex)
{
	hnode_t *ph_run = NULL;

	ph_run = ph_lst->next;
	while (ph_run != ph_lst)
	{
		if (ph_run->vertex == vertex)
		{
			return(ph_run);
		}
		ph_run = ph_run->next;
	}
	return((hnode_t*)NULL);
}

// vertical list management
// interface
static vlist_t *v_create_list()
{
	vnode_t *v_head_node = NULL;

	v_head_node = v_get_node(-1);
	v_head_node->prev = v_head_node;
	v_head_node->next = v_head_node;

	return(v_head_node);
}

static ret_t v_insert_end(vlist_t *pv_lst, vertex_t vertex)
{
	vnode_t *pv_new_node = NULL;

	pv_new_node = v_get_node(vertex);
	v_generic_insert(pv_lst->prev, pv_new_node, pv_lst);

	return(SUCCESS);
}

static len_t v_len(vlist_t *pv_lst)
{
	vnode_t *pv_run = NULL;
	len_t v_len = -1;

	pv_run = pv_lst->next;
	while (pv_run != pv_lst)
	{
		v_len = v_len + 1;
		pv_run = pv_run->next;
	}
	return(v_len);
}

static ret_t v_destroy_list(vlist_t *pv_lst)
{
	vnode_t *pv_run = NULL;
	vnode_t *pv_run_next = NULL;

	pv_run = pv_lst->next;
	while (pv_run != pv_lst)
	{
		pv_run_next = pv_run->next;
		free(pv_run);

		pv_run = pv_run_next;
	}
	free(pv_lst);
	pv_lst = NULL;
	return(SUCCESS);
}

// auxilliary
static vnode_t* v_get_node(vertex_t vertex)
{
	vnode_t *pv_node = NULL;

	pv_node = (vnode_t*)xcalloc(1, sizeof(vnode_t));
	pv_node->vertex = vertex;
	pv_node->pred_vertex = -1;
	pv_node->ph_head_node = h_create_list();
	pv_node->color = 'n';
	return(pv_node);
}

static void v_generic_insert(vnode_t *p_beg, vnode_t *p_mid, vnode_t *p_end)
{
	p_mid->next = p_end;
	p_mid->prev = p_beg;
	p_beg->next = p_mid;
	p_end->prev = p_mid;
}

static void v_generic_delete(vnode_t *p_node)
{
	p_node->next->prev = p_node->prev;
	p_node->prev->next = p_node->next;

	free(p_node);
	p_node = NULL;
}

static vnode_t *v_search_node(vlist_t *pv_lst, vertex_t vertex)
{
	vnode_t *pv_run = NULL;

	pv_run = pv_lst->next;
	while (pv_run != pv_lst)
	{
		if (pv_run->vertex == vertex)
			return(pv_run);

			pv_run = pv_run->next;
	}
	return((vnode_t*)NULL);
}

// auxilliary functions
static void *xcalloc(size_t n, size_t s)
{
	void *p = NULL;

	p = calloc(n, s);
	if (p == NULL)
	{
		fprintf(stderr, "calloc : fatal : out of memory\n");
		exit(EXIT_FAILURE);
	}
	return(p);
}
