#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc;
HGLRC ghrc = NULL;	// OpenGL's rendering context
bool gbIsFullScreen = false;
bool gbIsActiveWindow = false;
bool bLight = false;
FILE* gpFile = NULL;
GLfloat angle_pyramid = 0.0f;

struct Light
{
	GLfloat Ambient[4];	
	GLfloat Diffuse[4];
	GLfloat Specular[4];
	GLfloat Position[4];
};

Light lights[2] = { {{ 0.0f,0.0f,0.0f,1.0f },{ 1.0f,0.0f,0.0f,1.0f },{ 1.0f,0.0f,0.0f,1.0f },{ -2.0f,0.0f,0.0f,1.0f }},
					{{ 0.0f,0.0f,0.0f,1.0f },{ 0.0f,0.0f,1.0f,1.0f },{ 0.0f,0.0f,1.0f,1.0f },{ 2.0f,0.0f,0.0f,1.0f }} };

// material properties' values
GLfloat MaterialAmbient[] = { 0.0f,0.0f,0.0f,1.0f };	// inline initialization; R,G,B,A
GLfloat MaterialDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };	// R,G,B,A
GLfloat MaterialSpecular[] = { 1.0f,1.0f,1.0f,1.0f };	// R,G,B,A
GLfloat MaterialShininess[] = { 128.0f };	// or 50.0f

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int Initialize(void);
	void Display(void);
	void Update(void);

	// variable declarations
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");
	MSG msg;
	WNDCLASSEX wndclass;
	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// code
	// file creation code
	if (fopen_s(&gpFile, "SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// centering the window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Sameera_Application"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = Initialize();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "\nChoosePixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "\nSetPixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "\nwglCreateContext() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "\nwglMakecurrent() Failed");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "\nInitialization Function Succeeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);	// window should be at top
	SetFocus(hwnd);	// window's title bar,etc. should be highlighted

					// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage()
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow == true)
			{
				Update();
			}
			Display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(HWND);
	void Resize(int, int);
	void Uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen(hwnd);
			break;

		case 'L':
		case 'l':
			if (bLight == false)
			{
				bLight = true;
				glEnable(GL_LIGHTING);	// to enable light sources and thus lighting calculations
										//glEnable(GL_LIGHT0);	// need to explicitly enable due to above call
			}
			else
			{
				bLight = false;
				glDisable(GL_LIGHTING);	// to enable light sources and thus lighting calculations
										//glDisable(GL_LIGHT0);	
			}
			break;
		}
		break;

	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;

	case  WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize()
{
	// function declarations
	void Resize(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;	// paints onto window, supports OpenGL, double buffer
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32; // R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														//ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
														// returns indices starting from 1. Hence, 0 means error
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
	{
		return(-2);
	}
	// ask OS to give new RC
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL

	if (ghrc == NULL)
	{
		return(-3);
	}
	// now ghrc can do 3D rendering too

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context. 
	{
		return(-4);
	}

	glShadeModel(GL_SMOOTH);	// selects flat or smooth shading (beautification function)

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											// specifies clear values[0,1] used by glClear() for the color buffers

	glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// specifies implementation-specific hints.
														// Parameters : A symbolic constant indicating the behavior to be controlled, A symbolic constant indicating the desired behavior
														// GL_PERSPECTIVE_CORRECTION_HINT : Indicates the quality of color and texture coordinate interpolation 
														// Beautification call

	/*lights[0].Ambient[0] = 0.0f;
	lights[0].Ambient[1] = 0.0f;
	lights[0].Ambient[2] = 0.0f;
	lights[0].Ambient[3] = 1.0f;*/

	// configuration of lights using configuration parameters (setting parameters)
	glLightfv(
		GL_LIGHT0,	// a light (OGL light no. 0 out of the 8 lights)
		GL_AMBIENT,	// parameter for light; to specify the ambient(sorrounding/encircling) RGBA intensity for light
		lights[0].Ambient	// pointer to the array whose values will be given to GL_AMBIENT; default : (0.0, 0.0, 0.0, 1.0)
	);	// f= float, v= vector

	glLightfv(GL_LIGHT0, GL_DIFFUSE, lights[0].Diffuse);	// to specify the diffuse(to be spread over) RGBA intensity of the light
														// The default diffuse intensity is (0.0, 0.0, 0.0, 1.0) for all lights other than light zero. 
														// The default diffuse intensity of light zero is (1.0, 1.0, 1.0, 1.0). 

	glLightfv(GL_LIGHT0, GL_SPECULAR, lights[0].Specular);	// to specify the specular(reflective) RGBA intensity of the light
															// The default specular intensity is (0.0, 0.0, 0.0, 1.0) for all lights other than light zero. 
															// The default specular intensity of light zero is (1.0, 1.0, 1.0, 1.0). 

	glLightfv(GL_LIGHT0, GL_POSITION, lights[0].Position);	// specifies the position of the light in homogeneous object coordinates
															// The position is transformed by the modelview matrix when glLight is called (just as if it were a point), and it is stored in eye coordinates.
															// The default position is (0,0,1,0); thus, the default light source is directional (since w=0), parallel to, and in the direction of the z axis. 

	glEnable(GL_LIGHT0);	// need to explicitly enable due to explicit call to glEnable(GL_LIGHTING) in case 'l/L'

	glLightfv(GL_LIGHT1, GL_AMBIENT, lights[1].Ambient);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lights[1].Diffuse);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lights[1].Specular);
	glLightfv(GL_LIGHT1, GL_POSITION, lights[1].Position);
	glEnable(GL_LIGHT1);

	// specify material parameters for the lighting model
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);	// GL_FRONT : The face or faces that are being updated (since ours is steady sphere)
															// GL_AMBIENT : The material parameter of the face or faces being updated and their interpretations by the lighting equation
															// MaterialAmbient : specify the ambient RGBA reflectance of the material.
															// The default ambient reflectance for both front- and back-facing materials is (0.2, 0.2, 0.2, 1.0). 

	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);	// specify the diffuse RGBA reflectance of the material. 
															//  The default diffuse reflectance for both front- and back-facing materials is (0.8, 0.8, 0.8, 1.0). 

	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);	// specify the specular RGBA reflectance of the material. 
															// The default specular reflectance for both front- and back-facing materials is (0.0, 0.0, 0.0, 1.0). 

	glMaterialfv(GL_FRONT, GL_SHININESS, MaterialShininess);	// a single value that specifies the RGBA specular exponent of the material
																// Only values in the range[0, 128] are accepted.
																// The default specular exponent for both front - and back - facing materials is 0.
	Resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void Display()
{
	// function declarations
	void DrawPyramid();

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

														// pyramid-
	glMatrixMode(GL_MODELVIEW);	// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(0.0f, 0.25f, -4.0f);
	glRotatef(angle_pyramid, 0.0f, 1.0f, 0.0f);
	DrawPyramid();

	SwapBuffers(ghdc);	// exchanges the front and back buffers if the current pixel format for the window referenced by the specified device context includes a back buffer(double buffer)
}

void DrawPyramid()
{
	glBegin(GL_TRIANGLES);	// parameters: The primitive or primitives that will be created from vertices presented between glBegin() and the subsequent glend()

							// front
	glNormal3f(0.0f, 0.447214f, 0.894427f);	// he current normalis set to the given coordinates // normal coming outside the surface
	glVertex3f(0.0f, 1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// right
	glNormal3f(0.89427f, 0.447214f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// back
	glNormal3f(0.0f, 0.447214f, -0.89427f);
	glVertex3f(0.0f, 1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	// left
	glNormal3f(-0.89427f, 0.447214f, 0.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glEnd();

}

void Update()
{
	angle_pyramid = angle_pyramid + 1.0f;	// greater increment means greater speed of rotation
	if (angle_pyramid >= 360.0f)	// discipline; angle is never greater than 360 degrees
		angle_pyramid = 0.0f;
}

void Uninitialize()
{
	// check if fullscreen. If yes, restore to normal size and proceed for uninitialization
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		// SWP_NOZORDER: Retains the current Z order 
		// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
		// SWP_NOMOVE: Retains the current position
		// SWP_NOSIZE: Retains the current size
		// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.

		ShowCursor(TRUE);
	}
	// break th ecurrent context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	// Parameters: HDC, HGLRC; If hglrc(2nd parameter) is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. In this case, hdc is ignored.

		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpFile, "|nLog File Closed Successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
}

void ToggleFullScreen(HWND hwnd)
{
	MONITORINFO mi;

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(hwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(hwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(hwnd, &wpPrev);

		SetWindowPos(hwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}
}
