#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// OGL's rendering context
bool gbIsFullScreen = false;
bool gbActiveWindow = false;
FILE *gpFile = NULL;
FILE *gpTraceLog = NULL;
bool bLight = false;
GLUquadric *quadric[24];	// array of 24 pointers
GLfloat angleOfXRotation = 0.0f;
GLfloat angleOfYRotation = 0.0f;
GLfloat angleOfZRotation = 0.0f;
GLint keypress = 0;	// no key pressed

GLfloat LightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };	// inline initialization; R,G,B,A
GLfloat LightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };	// R,G,B,A
GLfloat LightPosition[] = { 0.0f,0.0f,0.0f,1.0f };	// x,y,z,w

// lighting models
GLfloat light_model_ambient[] = { 0.2f,0.2f,0.2f,1.0f };	// also default
GLfloat light_model_local_viewer[] = { 0.0f };

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// trace log file creation code
	if (fopen_s(&gpTraceLog, "SD_TraceLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT(" TraceLog File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf_s(gpTraceLog, "In WinMain()\n");

	// function declarations
	int InitializeOGL(void);
	void DisplayOGL(void);
	void Update(void);

	// variable declarations
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// file creation code
	if (fopen_s(&gpFile, "SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully\n");
	}
	// code
	// initialization of wndclass
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// Centering The Window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,	// Forces a top-level window onto the taskbar when the window is visible.
		szAppName,
		TEXT("Sameera-OGL Window With Double Buffer With Triangle"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,	// clip the child windows and sibling windows of this window
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = InitializeOGL();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "Initialization Function Succeeded\n");
	}

	ShowWindow(hwnd, iCmdShow);
	//UpdateWindow(hwnd);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage.
		{
			if (msg.message == WM_QUIT)
				bDone = true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == true)
			{
				// call Update() here
				Update();
			}
			// call Display() here 
			DisplayOGL();
		}
	}
	fprintf_s(gpTraceLog, "Exiting WinMain()\n");

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	fprintf_s(gpTraceLog, "In WndProc()\n");

	// function declarations
	void ToggleFullScreen();
	void ResizeOGL(int, int);
	//void DisplayOGL(void);
	void UninitializeOGL(void);

	// code 
	switch (iMsg)
	{
	case WM_CHAR:
		fprintf_s(gpTraceLog, "In WM_CHAR message\n");

		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen();
			break;

		case 'L':
		case 'l':
			if (bLight == false)
			{
				bLight = true;
				glEnable(GL_LIGHTING);	// to enable light sources and thus lighting calculations
										//glEnable(GL_LIGHT0);	// need to explicitly enable due to above call
			}
			else
			{
				bLight = false;
				glDisable(GL_LIGHTING);	// to enable light sources and thus lighting calculations
										//glDisable(GL_LIGHT0);	
			}
			break;

		case 'X':
		case'x':
			keypress = 1;
			angleOfXRotation = 0.0f;	// resetting angleOfXRotation
			break;

		case 'Y':
		case'y':
			keypress = 2;
			angleOfYRotation = 0.0f;	// resetting angleOfYRotation
			break;

		case 'Z':
		case'z':
			keypress = 3;
			angleOfZRotation = 0.0f;	// resetting angleOfZRotation
			break;
		}
		break;

	case WM_SETFOCUS:
		fprintf_s(gpTraceLog, "In WM_SETFOCUS message\n");
		gbActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		fprintf_s(gpTraceLog, "In WM_KILLFOCUS message\n");
		gbActiveWindow = false;
		break;

	case WM_SIZE:
		fprintf_s(gpTraceLog, "In WM_SIZE message\n");
		ResizeOGL(LOWORD(lParam), HIWORD(lParam));
		break;

		/*case WM_PAINT:
		DisplayOGL();
		break;*/

	case WM_ERASEBKGND:
		fprintf_s(gpTraceLog, "In WM_ERASEBKGND message\n");
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.	// not invoked?
		fprintf_s(gpTraceLog, "In WM_CLOSE message\n");
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		fprintf_s(gpTraceLog, "In WM_KEYDOWN message\n");
		switch (wParam)
		{
		case VK_ESCAPE:
			fprintf_s(gpTraceLog, "In VK_ESCAPE key\n");
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		fprintf_s(gpTraceLog, "In WM_DESTROY message\n");
		UninitializeOGL();
		PostQuitMessage(0);
		break;
	}
	fprintf_s(gpTraceLog, "Exiting WndProc()\n");

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int InitializeOGL()
{
	fprintf_s(gpTraceLog, "In InitializeOGL()\n");

	// function declarations
	void ResizeOGL(int, int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;	// paints onto window, supports OpenGL, double buffer
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32;	// R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														//ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
														// returns indices starting from 1. Hence, 0 means error
	if (iPixelFormatIndex == 0)
		return(-1);

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
		return(-2);

	// ask OS to give new RC
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL
	if (ghrc == NULL)
		return(-3);
	// now ghrc can do 3D rendering too

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context.
		return(-4);

	// 5 imp. depth calls-
	glShadeModel(GL_SMOOTH);

	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);

	glDepthFunc(GL_LEQUAL);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// lights
	glEnable(GL_AUTO_NORMAL);	// tell OpenGL to give normals

	glEnable(GL_NORMALIZE);		// If enabled, normal vectors specified with glNormal() are scaled to unit length after transformation
								// gives good quality; uses mathematical calculations, hence less performance

	// configuration of lights using configuration parameters (setting parameters)
	glLightfv(
		GL_LIGHT0,	// a light (OGL light no. 0 out of the 8 lights)
		GL_AMBIENT,	// parameter for light; to specify the ambient(sorrounding/encircling) RGBA intensity for light
		LightAmbient // pointer to the array whose values will be given to GL_AMBIENT; default : (0.0, 0.0, 0.0, 1.0)
	);	// f= float, v= vector

	glLightfv(GL_LIGHT0, GL_DIFFUSE, LightDiffuse);	// to specify the diffuse(to be spread over) RGBA intensity of the light
													// The default diffuse intensity is (0.0, 0.0, 0.0, 1.0) for all lights other than light zero. 
													// The default diffuse intensity of light zero is (1.0, 1.0, 1.0, 1.0). 

	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);	// specifies the position of the light in homogeneous object coordinates
														// The position is transformed by the modelview matrix when glLight is called (just as if it were a point), and it is stored in eye coordinates.
														// The default position is (0,0,1,0); thus, the default light source is directional (since w=0), parallel to, and in the direction of the z axis. 

	// set the lighting model(overall lighting) parameters
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, light_model_ambient);	// GL_LIGHT_MODEL_AMBIENT: to specify the ambient RGBA intensity of the entire scene, 
																	// light_model_ambient: values

	glLightModelfv(GL_LIGHT_MODEL_LOCAL_VIEWER, light_model_local_viewer);	// GL_LIGHT_MODEL_LOCAL_VIEWER: specifies how specular reflection angles are computed
																			// light_model_local_viewer: a single integer or floating-point value.
																			// If 2nd parameter is 0 (or 0.0), specular reflection angles take the view direction to be parallel to and in the direction of the z axis, regardless of the location of the vertex in eye coordinates. 
																			// Otherwise specular reflections are computed from the origin of the eye coordinate system. The default is 0. 

	glEnable(GL_LIGHT0);	// need to explicitly enable due to explicit call to glEnable(GL_LIGHTING) in case 'l/L'

	// create 24 spheres
	for (int i = 0;i < 24;i++)
	{
		quadric[i] = gluNewQuadric();
	}

	//glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glClearColor(0.25f, 0.25f, 0.25f, 1.0f);	// clear the screen by OGL color
											// specifies clear values[0,1] used by glClear() for the color buffers

											// warmup call to ResizeOGL()
	ResizeOGL(WIN_WIDTH, WIN_HEIGHT);

	fprintf_s(gpTraceLog, "Exiting InitializeOGL()\n");

	return(0);	// successful exit status
}

void ResizeOGL(int width, int height)
{
	fprintf_s(gpTraceLog, "In ResizeOGL()\n");
	//fprintf_s(gpTraceLog, "width= %d\n",width);
	//fprintf_s(gpTraceLog, "height= %d\n", height);

	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	if (height > width)
	{
		glOrtho(0.0f, 15.5f, 0.0f, 15.5f*(GLfloat)height / (GLfloat)width, -10.0f, 10.0f);	// LRBTNF
	}
	else
	{
		glOrtho(0.0f, 15.5f*(GLfloat)width / (GLfloat)height, 0.0f, 15.5f, -10.0f, 10.0f);
	}

	fprintf_s(gpTraceLog, "Exiting ResizeOGL()\n");
}

void DisplayOGL(void)
{
	// function declarations
	void Draw24Spheres(void);

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

	glMatrixMode(GL_MODELVIEW);	// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.

	//glTranslatef(0.0f, 0.0f, -0.70f);	// or -0.75f or -0.55f
	//glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	//gluSphere(quadric, 0.2f, 30, 30);		
	
	if (keypress == 1)
	{
		glRotatef(angleOfXRotation, 1.0f, 0.0f, 0.0f);	// change y
		LightPosition[1] = angleOfXRotation;
	}
	else if (keypress == 2)
	{
		glRotatef(angleOfYRotation, 0.0f, 1.0f, 0.0f);	// change z
		LightPosition[2] = angleOfYRotation;
	}
	else if (keypress == 3)
	{
		glRotatef(angleOfZRotation, 0.0f, 0.0f, 1.0f);	// change x
		LightPosition[0] = angleOfZRotation;
	}
	glLightfv(GL_LIGHT0, GL_POSITION, LightPosition);

	Draw24Spheres();

	SwapBuffers(ghdc);	// exchanges the front and back buffers if the current pixel format for the window referenced by the specified device context includes a back buffer(double buffer)
}

void Draw24Spheres(void)
{
	GLfloat MaterialAmbient[4];
	GLfloat MaterialDiffuse[4];
	GLfloat MaterialSpecular[4];
	GLfloat MaterialShininess;

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	// =============================================================================================================================
	// first column - gems
	// emerald
	MaterialAmbient[0] = 0.0215f;	// r
	MaterialAmbient[1] = 0.1745f;	// g
	MaterialAmbient[2] = 0.0215f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.07568f;	// r
	MaterialDiffuse[1] = 0.61424f;	// g
	MaterialDiffuse[2] = 0.07568f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.633f;		// r
	MaterialSpecular[1] = 0.727811f;	// g
	MaterialSpecular[2] = 0.633f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 13.285714f, 0.0f);
	gluSphere(quadric[0], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// jade
	MaterialAmbient[0] = 0.135f;	// r
	MaterialAmbient[1] = 0.2225f;	// g
	MaterialAmbient[2] = 0.1575f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.54f;	// r
	MaterialDiffuse[1] = 0.89f;	// g
	MaterialDiffuse[2] = 0.63f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.316228f;		// r
	MaterialSpecular[1] = 0.316228f;		// g
	MaterialSpecular[2] = 0.316228f;		// b
	MaterialSpecular[3] = 1.0f;				// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 11.071428f, 0.0f);
	gluSphere(quadric[1], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// obsidian
	MaterialAmbient[0] = 0.05375f;	// r
	MaterialAmbient[1] = 0.05f;		// g
	MaterialAmbient[2] = 0.06625f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.18275f;	// r
	MaterialDiffuse[1] = 0.17f;		// g
	MaterialDiffuse[2] = 0.22525f;	// b
	MaterialDiffuse[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.332741f;		// r
	MaterialSpecular[1] = 0.328634f;		// g
	MaterialSpecular[2] = 0.346435f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.3f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 8.857142f, 0.0f);
	gluSphere(quadric[2], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// pearl
	MaterialAmbient[0] = 0.25f;		// r
	MaterialAmbient[1] = 0.20725f;	// g
	MaterialAmbient[2] = 0.20725f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 1.0f;		// r
	MaterialDiffuse[1] = 0.829f;	// g
	MaterialDiffuse[2] = 0.829f;	// b
	MaterialDiffuse[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.296648f;		// r
	MaterialSpecular[1] = 0.296648f;		// g
	MaterialSpecular[2] = 0.296648f;		// b
	MaterialSpecular[3] = 1.0f;				// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.088f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 6.642857f, 0.0f);
	gluSphere(quadric[3], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// ruby
	MaterialAmbient[0] = 0.1745f;		// r
	MaterialAmbient[1] = 0.01175f;	// g
	MaterialAmbient[2] = 0.01175f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.61424f;		// r
	MaterialDiffuse[1] = 0.04136f;	// g
	MaterialDiffuse[2] = 0.04136f;	// b
	MaterialDiffuse[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.727811f;		// r
	MaterialSpecular[1] = 0.626959f;		// g
	MaterialSpecular[2] = 0.626959f;		// b
	MaterialSpecular[3] = 1.0f;				// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 4.428571f, 0.0f);
	gluSphere(quadric[4], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// turquoise
	MaterialAmbient[0] = 0.1f;		// r
	MaterialAmbient[1] = 0.18725f;	// g
	MaterialAmbient[2] = 0.1745f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.396f;		// r
	MaterialDiffuse[1] = 0.74151f;	// g
	MaterialDiffuse[2] = 0.69102f;	// b
	MaterialDiffuse[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.297254f;		// r
	MaterialSpecular[1] = 0.30829f;		// g
	MaterialSpecular[2] = 0.306678f;		// b
	MaterialSpecular[3] = 1.0f;				// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(5.511111f, 2.214285f, 0.0f);
	gluSphere(quadric[4], 1.0f, 30, 30);

	// =============================================================================================================================
	// second column - metals
	// brass
	MaterialAmbient[0] = 0.329412f;	// r
	MaterialAmbient[1] = 0.223529f;	// g
	MaterialAmbient[2] = 0.027451f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.780392f;	// r
	MaterialDiffuse[1] = 0.568627f;	// g
	MaterialDiffuse[2] = 0.113725f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.992157f;		// r
	MaterialSpecular[1] = 0.941176f;	// g
	MaterialSpecular[2] = 0.807843f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = (GLfloat)0.21794872 * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 13.285714f, 0.0f);
	gluSphere(quadric[6], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// bronze
	MaterialAmbient[0] = 0.2125f;	// r
	MaterialAmbient[1] = 0.1275f;	// g
	MaterialAmbient[2] = 0.054f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.714f;	// r
	MaterialDiffuse[1] = 0.4284f;	// g
	MaterialDiffuse[2] = 0.18144f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.393548f;		// r
	MaterialSpecular[1] = 0.271906f;	// g
	MaterialSpecular[2] = 0.166721f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.2f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 11.071428f, 0.0f);
	gluSphere(quadric[7], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// chrome
	MaterialAmbient[0] = 0.25f;	// r
	MaterialAmbient[1] = 0.25f;	// g
	MaterialAmbient[2] = 0.25f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.4f;	// r
	MaterialDiffuse[1] = 0.4f;	// g
	MaterialDiffuse[2] = 0.4f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.774597f;		// r
	MaterialSpecular[1] = 0.774597f;	// g
	MaterialSpecular[2] = 0.774597f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.6f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 8.857142f, 0.0f);
	gluSphere(quadric[8], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// copper
	MaterialAmbient[0] = 0.19125f;	// r
	MaterialAmbient[1] = 0.0735f;	// g
	MaterialAmbient[2] = 0.0225f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.7038f;	// r
	MaterialDiffuse[1] = 0.27048f;	// g
	MaterialDiffuse[2] = 0.0828f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.256777f;		// r
	MaterialSpecular[1] = 0.137622f;	// g
	MaterialSpecular[2] = 0.086014f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.1f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 6.642857f, 0.0f);
	gluSphere(quadric[9], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// gold
	MaterialAmbient[0] = 0.24725f;	// r
	MaterialAmbient[1] = 0.1995f;	// g
	MaterialAmbient[2] = 0.0745f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.75164f;	// r
	MaterialDiffuse[1] = 0.60648f;	// g
	MaterialDiffuse[2] = 0.22648f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.628281f;		// r
	MaterialSpecular[1] = 0.555802f;	// g
	MaterialSpecular[2] = 0.366065f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.4f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 4.428571f, 0.0f);
	gluSphere(quadric[10], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// silver
	MaterialAmbient[0] = 0.19225f;	// r
	MaterialAmbient[1] = 0.19225f;	// g
	MaterialAmbient[2] = 0.19225f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.50754f;	// r
	MaterialDiffuse[1] = 0.50754f;	// g
	MaterialDiffuse[2] = 0.50754f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.508273f;		// r
	MaterialSpecular[1] = 0.508273f;	// g
	MaterialSpecular[2] = 0.508273f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.4f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(11.022222f, 2.214285f, 0.0f);
	gluSphere(quadric[11], 1.0f, 30, 30);

	// =============================================================================================================================
	// third column - plastic
	// black
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.01f;	// r
	MaterialDiffuse[1] = 0.01f;	// g
	MaterialDiffuse[2] = 0.01f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.50f;		// r
	MaterialSpecular[1] = 0.50f;	// g
	MaterialSpecular[2] = 0.50f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 13.285714f, 0.0f);
	gluSphere(quadric[12], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// cyan
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.1f;	// g
	MaterialAmbient[2] = 0.06f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.0f;	// r
	MaterialDiffuse[1] = 0.50980392f;	// g
	MaterialDiffuse[2] = 0.50980392f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.50196078f;		// r
	MaterialSpecular[1] = 0.50196078f;	// g
	MaterialSpecular[2] = 0.50196078f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 11.071428f, 0.0f);
	gluSphere(quadric[13], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// green
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.1f;	// r
	MaterialDiffuse[1] = 0.35f;	// g
	MaterialDiffuse[2] = 0.1f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.45f;		// r
	MaterialSpecular[1] = 0.55f;	// g
	MaterialSpecular[2] = 0.45f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 8.857142f, 0.0f);
	gluSphere(quadric[14], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// red
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5f;	// r
	MaterialDiffuse[1] = 0.0f;	// g
	MaterialDiffuse[2] = 0.0f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;		// r
	MaterialSpecular[1] = 0.6f;	// g
	MaterialSpecular[2] = 0.6f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 6.642857f, 0.0f);
	gluSphere(quadric[15], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// white
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.55f;	// r
	MaterialDiffuse[1] = 0.55f;	// g
	MaterialDiffuse[2] = 0.55f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.70f;		// r
	MaterialSpecular[1] = 0.70f;	// g
	MaterialSpecular[2] = 0.70f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 4.428571f, 0.0f);
	gluSphere(quadric[16], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// yellow
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5f;	// r
	MaterialDiffuse[1] = 0.5f;	// g
	MaterialDiffuse[2] = 0.0f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.60f;		// r
	MaterialSpecular[1] = 0.60f;	// g
	MaterialSpecular[2] = 0.50f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.25f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(16.533333f, 2.214285f, 0.0f);
	gluSphere(quadric[17], 1.0f, 30, 30);

	// =============================================================================================================================
	// fourth column - rubber
	// black
	MaterialAmbient[0] = 0.02f;	// r
	MaterialAmbient[1] = 0.02f;	// g
	MaterialAmbient[2] = 0.02f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.01f;	// r
	MaterialDiffuse[1] = 0.01f;	// g
	MaterialDiffuse[2] = 0.01f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.4f;		// r
	MaterialSpecular[1] = 0.4f;	// g
	MaterialSpecular[2] = 0.4f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 13.285714f, 0.0f);
	gluSphere(quadric[18], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// cyan
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.05f;	// g
	MaterialAmbient[2] = 0.05f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.4f;	// r
	MaterialDiffuse[1] = 0.5f;	// g
	MaterialDiffuse[2] = 0.5f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.04f;		// r
	MaterialSpecular[1] = 0.7f;	// g
	MaterialSpecular[2] = 0.7f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 11.071428f, 0.0f);
	gluSphere(quadric[19], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// green
	MaterialAmbient[0] = 0.0f;	// r
	MaterialAmbient[1] = 0.05f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.4f;	// r
	MaterialDiffuse[1] = 0.5f;	// g
	MaterialDiffuse[2] = 0.4f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.04f;		// r
	MaterialSpecular[1] = 0.7f;	// g
	MaterialSpecular[2] = 0.04f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 8.857142f, 0.0f);
	gluSphere(quadric[20], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// red
	MaterialAmbient[0] = 0.05f;	// r
	MaterialAmbient[1] = 0.0f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5f;	// r
	MaterialDiffuse[1] = 0.4f;	// g
	MaterialDiffuse[2] = 0.4f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;		// r
	MaterialSpecular[1] = 0.04f;	// g
	MaterialSpecular[2] = 0.04f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 6.642857f, 0.0f);
	gluSphere(quadric[21], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// white
	MaterialAmbient[0] = 0.05f;	// r
	MaterialAmbient[1] = 0.05f;	// g
	MaterialAmbient[2] = 0.05f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5f;	// r
	MaterialDiffuse[1] = 0.5f;	// g
	MaterialDiffuse[2] = 0.5f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;		// r
	MaterialSpecular[1] = 0.7f;	// g
	MaterialSpecular[2] = 0.7f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 4.428571f, 0.0f);
	gluSphere(quadric[22], 1.0f, 30, 30);

	//-----------------------------------------------------------------------------------------------------------------------------
	// yellow
	MaterialAmbient[0] = 0.05f;	// r
	MaterialAmbient[1] = 0.05f;	// g
	MaterialAmbient[2] = 0.0f;	// b
	MaterialAmbient[3] = 1.0f;		// a
	glMaterialfv(GL_FRONT, GL_AMBIENT, MaterialAmbient);

	MaterialDiffuse[0] = 0.5f;	// r
	MaterialDiffuse[1] = 0.5f;	// g
	MaterialDiffuse[2] = 0.4f;	// b
	MaterialDiffuse[3] = 1.0f;	// a
	glMaterialfv(GL_FRONT, GL_DIFFUSE, MaterialDiffuse);

	MaterialSpecular[0] = 0.7f;		// r
	MaterialSpecular[1] = 0.7f;	// g
	MaterialSpecular[2] = 0.04f;		// b
	MaterialSpecular[3] = 1.0f;			// a
	glMaterialfv(GL_FRONT, GL_SPECULAR, MaterialSpecular);

	MaterialShininess = 0.078125f * 128.0f;
	glMaterialf(GL_FRONT, GL_SHININESS, MaterialShininess);

	// two lines for discipline
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glTranslatef(22.044444f, 2.214285f, 0.0f);
	gluSphere(quadric[23], 1.0f, 30, 30);
}

void Update()
{
	angleOfXRotation = angleOfXRotation + 0.5f;

	if (angleOfXRotation >= 360.0f)
		angleOfXRotation = 0.0f;

	angleOfYRotation = angleOfYRotation + 0.5f;

	if (angleOfYRotation >= 360.0f)
		angleOfYRotation = 0.0f;

	angleOfZRotation = angleOfZRotation + 0.5f;

	if (angleOfZRotation >= 360.0f)
		angleOfZRotation = 0.0f;
}

void UninitializeOGL()
{
	fprintf_s(gpTraceLog, "In UninitializeOGL()\n");

	for (int i = 0;i < 24;i++)
	{
		if (quadric[i])
		{
			gluDeleteQuadric(quadric[i]);
			quadric[i] = NULL;
		}
	}

	// check if fullscreen. If yes, restore to normal size and proceed for uninitialization
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);	// sets the placement of the window

		SetWindowPos(ghwnd,
			HWND_TOP,	// insert after
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);	// sets the z-order of the window
																							// SWP_NOZORDER: Retains the current Z order 
																							// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
																							// SWP_NOMOVE: Retains the current position
																							// SWP_NOSIZE: Retains the current size
																							// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.

		ShowCursor(TRUE);
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	//Parameters: HDC, HGLRC; If hglrc is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. In this case, hdc is ignored.

		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpFile, "Log File Closed Successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
	fprintf_s(gpTraceLog, "Exiting UninitializeOGL()\n");
}

void ToggleFullScreen()
{
	fprintf_s(gpTraceLog, "In ToggleFullScreen()\n");

	MONITORINFO mi;

	if (gbIsFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };	// Setting cbSize field of MONITORINFO. Doing so lets GetMonitorInfo() determine the type of structure you are passing to it.

			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))	// MonitorFromWindow() gives HMONITORINFO
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghwnd,
					HWND_TOP,	// A handle to the window to precede the positioned window in the Z order; HWND_TOP places the window at the top of the z-order
					mi.rcMonitor.left,	// x;	rcMonitor is a RECT struct
					mi.rcMonitor.top,	//y
					mi.rcMonitor.right - mi.rcMonitor.left,	//width
					mi.rcMonitor.bottom - mi.rcMonitor.top,	//ht
					SWP_NOZORDER | SWP_FRAMECHANGED);	//flags
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);	// sets the placement of the window

		SetWindowPos(ghwnd,
			HWND_TOP,	// has no effect although written since SWP_NOZORDER flag(which uses last z-order) used below
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);	// sets the z-order of the window

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}
	fprintf_s(gpTraceLog, "Exiting ToggleFullScreen()\n");
}
