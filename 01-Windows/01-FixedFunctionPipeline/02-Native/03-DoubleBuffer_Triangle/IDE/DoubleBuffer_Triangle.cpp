#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#pragma comment(lib,"opengl32.lib")
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd=NULL;
HDC ghdc=NULL;
HGLRC ghrc=NULL;	// OGL's rendering context
bool gbIsFullScreen = false;
bool gbActiveWindow=false;
FILE *gpFile=NULL;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int InitializeOGL(void);
	void DisplayOGL(void);

	// variable declarations
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");
	bool bDone=false;
	int iRet=0;
	int iWindow_x,iWindow_y;

	// file creation code
	if(fopen_s(&gpFile,"SD_Log.txt","w")!=0)
	{
		MessageBox(NULL,TEXT("Log File Cannot Be Created"),TEXT("Error"),MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile,"Log File Created Successfully\n");
	}
	// code
	// initialization of wndclass
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW|CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wndclass.hCursor = LoadCursor(NULL, IDC_HAND);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_WINLOGO);

	// register above class
	RegisterClassEx(&wndclass);

	// Centering The Window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y= GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Sameera-OGL Window With Double Buffer With Triangle"),
		WS_OVERLAPPEDWINDOW|WS_CLIPCHILDREN|WS_CLIPSIBLINGS|WS_VISIBLE,
		100,
		100,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet=InitializeOGL();
	if(iRet==-1)
	{
		fprintf_s(gpFile,"ChoosePixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if(iRet==-2)
	{
		fprintf_s(gpFile,"SetPixelFormat() Failed\n");
		DestroyWindow(hwnd);
	}
	else if(iRet==-3)
	{
		fprintf_s(gpFile,"wglCreateContext() Failed\n");
		DestroyWindow(hwnd);
	}
	else if(iRet==-4)
	{
		fprintf_s(gpFile,"wglMakeCurrent() Failed\n");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile,"Initialization Function Succeeded\n");
	}

	ShowWindow(hwnd, iCmdShow);
	//UpdateWindow(hwnd);
	SetForegroundWindow(hwnd);
	SetFocus(hwnd);

	// game loop
	while(bDone==false)
	{
		if(PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage.
		{
			if(msg.message==WM_QUIT)
				bDone=true;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if(gbActiveWindow==true)
			{
				// call Update() here
			}
			// call Display() here 
			DisplayOGL();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(HWND);
	void ResizeOGL(int,int);
	//void DisplayOGL(void);
	void UninitializeOGL(void);

	// code 
	switch (iMsg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen(hwnd);
			break;
		}
		break;

	case WM_SETFOCUS:
		gbActiveWindow=true;
		break;

	case WM_KILLFOCUS:
		gbActiveWindow=false;
		break;

	case WM_SIZE:
		ResizeOGL(LOWORD(lParam),HIWORD(lParam));
		break;

	/*case WM_PAINT:
		DisplayOGL();
		break;*/

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
		}
		break;

	case WM_DESTROY:
		UninitializeOGL();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int InitializeOGL()
{
	// function declarations
	void ResizeOGL(int,int);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd,sizeof(PIXELFORMATDESCRIPTOR));
	// initialie pfd structure
	pfd.nSize=sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion=1;
	pfd.dwFlags=PFD_DRAW_TO_WINDOW|PFD_SUPPORT_OPENGL|PFD_DOUBLEBUFFER;
	pfd.iPixelType=PFD_TYPE_RGBA;
	pfd.cColorBits=32;
	pfd.cRedBits=8;
	pfd.cGreenBits=8;
	pfd.cBlueBits=8;
	pfd.cAlphaBits=8;

	ghdc=GetDC(ghwnd);

	iPixelFormatIndex=ChoosePixelFormat(ghdc,&pfd);
	if(iPixelFormatIndex==0)
		return(-1);

	if(SetPixelFormat(ghdc,iPixelFormatIndex,&pfd)==FALSE)
		return(-2);

	// ask OS to give new RC
	ghrc=wglCreateContext(ghdc);
	if(ghrc==NULL)
		return(-3);

	if(wglMakeCurrent(ghdc,ghrc)==FALSE)
		return(-4);

	glClearColor(0.0f,0.0f,0.0f,1.0f);	// clear the screen by OGL color

	// warmup call to ResizeOGL()
	//ResizeOGL(WIN_WIDTH,WIN_HEIGHT);

	return(0);	// successful exit status
}

void ResizeOGL(int width,int height)
{
	glViewport(0,0,(GLsizei)width,(GLsizei)height);
}

void DisplayOGL(void)
{
	glClear(GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);

	glLoadIdentity();

	glBegin(GL_TRIANGLES);

	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex2f(0.0f, 1.0f);

	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex2f(-1.0f, -1.0f);

	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex2f(1.0f, -1.0f);

	glEnd();
	//glFlush();
	SwapBuffers(ghdc);
}

void UninitializeOGL()
{
	if(gbIsFullScreen==true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghwnd, &wpPrev);	// sets the placement of the window

		SetWindowPos(ghwnd,
			HWND_TOP,	// has no effect although written since SWP_NOZORDER flag(which uses last z-order) used below
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);	// sets the z-order of the window
			
		ShowCursor(TRUE);
	}

	// break the current context
	if(wglGetCurrentContext()==ghrc)
	{
		wglMakeCurrent(NULL,NULL);

		if(ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc=NULL;
		}
		if(ghdc)
		{
			ReleaseDC(ghwnd,ghdc);
			ghdc=NULL;
		}
		if(gpFile)
		{
			fprintf_s(gpFile,"Log File Closed Successfully");
			fclose(gpFile);
			gpFile=NULL;
		}
	}
}

void ToggleFullScreen(HWND hwnd)
{
	MONITORINFO mi;
	
	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(hwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(hwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(hwnd, &wpPrev);

		SetWindowPos(hwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}	
}
