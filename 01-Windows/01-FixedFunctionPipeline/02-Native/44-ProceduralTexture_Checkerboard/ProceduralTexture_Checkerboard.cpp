#include<stdio.h>
#include<windows.h>
#include<gl/GL.h>
#include<gl/GLU.h>
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64	// ?

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc;
HGLRC ghrc = NULL;	// OpenGL's rendering context
bool gbIsFullScreen = false;
bool gbIsActiveWindow = false;
FILE* gpFile = NULL;
GLubyte checkImage[CHECK_IMAGE_WIDTH][CHECK_IMAGE_HEIGHT][4];
GLuint texImage;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int Initialize(void);
	void Display(void);

	// variable declarations
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");
	MSG msg;
	WNDCLASSEX wndclass;
	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// code
	// file creation code
	if (fopen_s(&gpFile, "SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// centering the window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Sameera_Application"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = Initialize();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "\nChoosePixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "\nSetPixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "\nwglCreateContext() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "\nwglMakecurrent() Failed");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "\nInitialization Function Succeeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);	// window should be at top
	SetFocus(hwnd);	// window's title bar,etc. should be highlighted

					// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage()
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow == true)
			{
				//Update();
			}
			Display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(HWND);
	void Resize(int, int);
	void Uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen(hwnd);
			break;
		}
		break;

	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;

	case  WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize()
{
	// function declarations
	void Resize(int, int);
	void loadTexture(void);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL; //| PFD_DOUBLEBUFFER;	// paints onto window, supports OpenGL, double buffer
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32; // R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														//ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
														// returns indices starting from 1. Hence, 0 means error
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
	{
		return(-2);
	}
	// ask OS to give new RC
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL

	if (ghrc == NULL)
	{
		return(-3);
	}
	// now ghrc can do 3D rendering too

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context. 
	{
		return(-4);
	}

	glShadeModel(GL_FLAT);	// selects flat or smooth shading (beautification function)

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											// specifies clear values[0,1] used by glClear() for the color buffers

	glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// specifies implementation-specific hints.
														// Parameters : A symbolic constant indicating the behavior to be controlled, A symbolic constant indicating the desired behavior
														// GL_PERSPECTIVE_CORRECTION_HINT : Indicates the quality of color and texture coordinate interpolation 
														// Beautification call

	glEnable(GL_TEXTURE_2D);	// If enabled, two-dimensional texturing is performed

	// load the texture
	loadTexture();

	Resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void loadTexture(void)
{
	// function declaration
	void makeCheckImage(void);

	// code
	makeCheckImage();

	// OpenGL functions now
	// set pixel storage mode
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);	// GL_UNPACK_ALIGNMENT : Specifies the alignment requirements for the start of each pixel row in memory
											// 1 : 1 byte alignment between rows of image

	// allocate memory for textures on GPU and obtain its id in 'texture'
	glGenTextures(1, &texImage);	// 1 : how many textures to generate
									// &texImage : starting addr of the array of textures returned as an id

	// bind every element of the array to a target texture (type of texture)
	glBindTexture(GL_TEXTURE_2D, texImage);	// binding the first texture of array(value) to a 2D array type usage
											// Texture targets become aliases for textures currently bound to them

	// set the state/parameters of the texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// GL_TEXTURE_WRAP_S : wrap the tex horizontally. Sets the wrap parameter for texture coordinate s to either GL_CLAMP or GL_REPEAT
																	// GL_TEXTURE_WRAP_S : parameter to be tweaked
																	// GL_REPEAT : tell OGL to repeat pattern

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// GL_TEXTURE_WRAP_T : wrap the tex vertically

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);	// when model is near the viewer; pixel being textured maps to an area lesser than one texture element
																		// sets the texture magnification function to either GL_NEAREST(better performance) or GL_LINEAR(better quality due to interpolation)
																		// GL_TEXTURE_MAG_FILTER : parameter to be tweaked
																		// GL_NEAREST : since we have a pattern (no need of fine quality)

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);	// when model is away from the viewer; pixel being textured maps to an area greater than one texture element

	// fill the data
	glTexImage2D(GL_TEXTURE_2D,	// target
		0,						// levels of mipmaps
		GL_RGBA,				// the number of color components in the texture (called internal image format)
		CHECK_IMAGE_WIDTH,		// width of the texture image
		CHECK_IMAGE_HEIGHT,		// height of the texture image
		0,						// border width (no border)
		GL_RGBA,				// format of the pixel data (last parameter)
		GL_UNSIGNED_BYTE,		// data type of the pixel data (last parameter)
		checkImage				// address of the image data
	);	// specifies a two-dimensional texture image.	// ?

	// set texture environment parameters i.e. how to color a pixel through the given tex (used for procedural textures)
	glTexEnvf(GL_TEXTURE_ENV,	// compulsorily; target : A texture environment
		GL_TEXTURE_ENV_MODE,	// The symbolic name of a texture environment parameter (setting the mode)
		GL_REPLACE				// A single symbolic constant; Replace pixel's color by texture's color
	);	// f : float

}

// creating the texture through Maths
void makeCheckImage(void)
{
	// variable declarations
	int i, j, c;

	// code
	for (i = 0;i < CHECK_IMAGE_HEIGHT;i++)
	{
		for (j = 0;j < CHECK_IMAGE_WIDTH;j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;	// gives color	// ?

			checkImage[i][j][0] = (GLubyte)c;	// R
			checkImage[i][j][1] = (GLubyte)c;	// G
			checkImage[i][j][2] = (GLubyte)c;	// B
			checkImage[i][j][3] = 255;			// A
		}
	}
}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0f, (GLfloat)width / (GLfloat)height, 1.0f, 30.0f);
}

void Display()
{
	// function declarations
	void drawCheckerboards(void);

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

														// pyramid-
	glMatrixMode(GL_MODELVIEW);	// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(0.0f, 0.0f, -3.6f);
	// glBindTexture(GL_TEXTURE_2D, texImage);	// OK if not written since tex is already bound due glBindTexture() callin loadTexture() 

	drawCheckerboards();

	SwapBuffers(ghdc);
	//glFlush();
}

void drawCheckerboards()
{
	glBegin(GL_QUADS);	// parameters: The primitive or primitives that will be created from vertices presented between glBegin() and the subsequent glend()

	// clockwise starting from left bottom
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-2.0f, -1.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-2.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(0.0f, -1.0f, 0.0f);

	// clockwise starting from left bottom
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, 0.0f);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(2.41421f, 1.0f, -1.41421f);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(2.41421f, -1.0f, -1.41421f);

	glEnd();
}

void Uninitialize()
{
	glBindTexture(GL_TEXTURE_2D, 0);	// unbinds tex from target
	glDeleteTextures(1, &texImage);

	// check if fullscreen. If yes, restore to normal size and proceed for uninitialization
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		// SWP_NOZORDER: Retains the current Z order 
		// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
		// SWP_NOMOVE: Retains the current position
		// SWP_NOSIZE: Retains the current size
		// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.

		ShowCursor(TRUE);
	}
	// break th ecurrent context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	// Parameters: HDC, HGLRC; If hglrc(2nd parameter) is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. In this case, hdc is ignored.

		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpFile, "\nLog File Closed Successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
}

void ToggleFullScreen(HWND hwnd)
{
	MONITORINFO mi;

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(hwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(hwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(hwnd, &wpPrev);

		SetWindowPos(hwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}
}
