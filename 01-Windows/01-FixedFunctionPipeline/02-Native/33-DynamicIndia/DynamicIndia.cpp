#include<stdio.h>
#include<windows.h>
#include "DynamicIndia.h"
#include<gl/GL.h>
#include<gl/GLU.h>
#define _USE_MATH_DEFINES 1
#include<math.h>
#pragma comment(lib,"opengl32.lib")
#pragma comment(lib,"glu32.lib")
#pragma comment(lib,"Winmm.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variable declarations
DWORD dwStyle;
GLfloat letter_start = -0.19f, letter_end = 0.19f;
GLfloat half_letter_ht = 0.5f;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc = NULL;
HGLRC ghrc = NULL;	// OGL's rendering context
bool gbIsActiveWindow = false;
FILE *gpFile = NULL;
FILE *gpTraceLog = NULL;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int Initialize(void);
	void Display(void);

	// variable declarations
	HWND hwnd;
	MSG msg;
	WNDCLASSEX wndclass;
	TCHAR szAppName[] = TEXT("MyApp");

	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// code
	// trace log file creation code
	if (fopen_s(&gpTraceLog, "sd_TraceLog.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("TraceLog File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf_s(gpTraceLog, "\nIn WinMain()");

	// log file creation code
	if (fopen_s(&gpFile, "SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	fprintf_s(gpFile, "\nLog File Created Successfully");

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// CS_OWNDC: To avoid retrieving a device context each time it needs to paint inside a window
															// Directs the system to create a private/unique device context for each window in the class
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)(GetStockObject(BLACK_BRUSH));
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// centering the window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,	// Forces a top-level window onto the taskbar when the window is visible.
		szAppName,
		TEXT("Sameera_Application"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,	// clip the child windows and sibling windows of this window
																				// window is initially visible
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = Initialize();

	if (iRet == -1)
	{
		fprintf_s(gpFile, "\nChoosePixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "\nSetPixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "\nwglCreateContext() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "\nwglMakeCurrent() Failed");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "\nInitialization Function Succeeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);	// gives a slightly higher priority to the thread creating this window
	SetFocus(hwnd);

	// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow == true)
			{
				// call Update() here
			}
			// call Display() here
			Display();
		}
	}
	fprintf_s(gpTraceLog, "\nExiting WinMain()");

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	fprintf_s(gpTraceLog, "\nIn WndProc()");

	// function declarations
	void Resize(int, int);
	void Uninitialize();
	void MakeFullscreen();

	// code
	switch (iMsg)
	{
		//case WM_CREATE:	// not working
		//	MakeFullscreen();
		//	break;

	case WM_SETFOCUS:
		fprintf_s(gpTraceLog, "\nIn WM_SETFOCUS message");
		gbIsActiveWindow = true;
		break;

	case WM_KILLFOCUS:
		fprintf_s(gpTraceLog, "\nIn WM_KILLFOCUS message");
		gbIsActiveWindow = false;
		break;

	case WM_SIZE:
		fprintf_s(gpTraceLog, "\nIn WM_SIZE message");
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:	//  sent when the window background must be erased (for example, when a window is resized). 
						// we are handling this msg since we don't want DefWindowProc() to post WM_PAINT message

		fprintf_s(gpTraceLog, "\nIn WM_ERASEBKGND message");
		return(0);	// we have an external painter

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.	// not invoked?
		fprintf_s(gpTraceLog, "\nIn WM_CLOSE message");
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		fprintf_s(gpTraceLog, "\nIn WM_KEYDOWN message");
		switch (wParam)
		{
		case VK_ESCAPE:
			fprintf_s(gpTraceLog, "\nIn VK_ESCAPE key");
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		fprintf_s(gpTraceLog, "\nIn WM_DESTROY message");
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	fprintf_s(gpTraceLog, "\nExiting WndProc()");

	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize()
{
	fprintf_s(gpTraceLog, "\nIn Initialize()");

	// function declarations
	void Resize(int, int);
	void MakeFullscreen();

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32;	// R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														//ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
	if (iPixelFormatIndex == 0)
		return(-1);

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
		return(-2);

	// ask OS to give new Rendering Context
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL
	if (ghrc == NULL)
		return(-3);
	// now ghrc can do 3D rendering too
	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context.
		return(-4);

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// clear the screen by OGL color
											// specifies clear values[0,1] used by glClear() for the color buffers

	MakeFullscreen();

	PlaySound(MAKEINTRESOURCE(MY_AUDIO), NULL, SND_ASYNC | SND_NODEFAULT | SND_RESOURCE);

	//Resize(WIN_WIDTH, WIN_HEIGHT);

	fprintf_s(gpTraceLog, "\nExiting Initialize()");

	return(0);	// successful exit status
}

void Resize(int width, int height)
{
	fprintf_s(gpTraceLog, "\nIn Resize()");

	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// parameters:
																			// fovy- The field of view angle, in degrees, in the y - direction.
																			// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																			// zNear- The distance from the viewer to the near clipping plane(always positive).
																			// zFar- The distance from the viewer to the far clipping plane(always positive).*/
	fprintf_s(gpTraceLog, "\nExiting Resize()");
}

void Display()
{
	// function declarations
	void DrawI();
	void DrawN();
	void DrawD(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void DrawA();
	void DrawABand();
	void DrawPlane();
	void DrawSmokeUpper(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void DrawSmokeMiddle(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat,bool);
	void DrawSmokeLower(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void DrawSmokeRightUpper(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);
	void DrawSmokeRightLower(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);

	// variable declarations
	GLfloat letter_x = 0.46f;
	GLfloat r = 1.42f;

	static GLfloat i1_x = -3.0f;
	static GLfloat a_x = 2.50f;
	static GLfloat n_y = 1.58f;
	static GLfloat i2_y = -1.58f;
	static GLfloat r_saffron = 0.0f, g_saffron = 0.0f, b_saffron = 0.0f;
	static GLfloat r_green = 0.0f, g_green = 0.0f, b_green = 0.0f;
	static GLfloat r_saffron_smoke = 255.0f, g_saffron_smoke = 153.0f, b_saffron_smoke = 51.0f;
	static GLfloat r_white_smoke = 1.0f;
	static GLfloat r_green_smoke = 18.0f, g_green_smoke = 136.0f, b_green_smoke = 7.0f;
	static GLfloat middle_plane_x = -letter_x * 5.4f;
	static GLfloat angle_upper_plane = -90.0f;
	static GLfloat angle_lower_plane = 90.0f;
	static GLfloat angle = (GLfloat)M_PI;
	static GLfloat angle_lower = (GLfloat)M_PI;
	static GLfloat angle_right_upper = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI;
	static GLfloat angle_right_lower = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI;
	static int cnt = 0;

	static bool bDoneI1 = false;
	static bool bDoneA = false;
	static bool bDoneN = false;
	static bool bDoneI2 = false;
	static bool bDoneR_saffron = false, bDoneG_saffron = false, bDoneB_saffron = false;
	static bool bDoneR_green = false, bDoneG_green = false, bDoneB_green = false;
	static bool bDoneD = false;
	static bool bDoneMidddlePlane = false;
	static bool bDoneUpperPlane = false;
	static bool bDoneDetachPlanes = false;
	static bool bDoneABand = false;
	static 	bool bDoneFadingSmokes = false;

	// code
	glClear(GL_COLOR_BUFFER_BIT);	// clears buffers to preset values.
									// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

									// I1
	glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(i1_x, 0.0f, -3.0f);
	glLineWidth(15.0f);
	DrawI();

	// A
	if (bDoneI1 == true)
	{
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(a_x, 0.0f, -3.0f);
		DrawA();
	}

	// N
	if (bDoneA == true)
	{
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(-letter_x, n_y, -3.0f);
		DrawN();
	}

	// I2
	if (bDoneN == true)
	{
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(letter_x, i2_y, -3.0f);
		DrawI();
	}

	// D
	if (bDoneI2 == true)
	{
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(0.0f, 0.0f, -3.0f);
		DrawD(r_saffron, g_saffron, b_saffron, r_green, g_green, b_green);
	}

	// Attach planes
	if (bDoneD == true)
	{
		// upper
		if (bDoneUpperPlane == false)
		{
			glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
			glLoadIdentity();	// replaces the current matrix with the identity matrix.
			//glTranslatef(-2.0f*letter_x - letter_start - 0.02f, 1.22f, -3.0f);
			glTranslatef(-2.0f*letter_x + letter_end, 1.22f, -3.0f);
			glTranslatef(r*(GLfloat)cos(angle), r*(GLfloat)sin(angle), 0.0f);
			glRotatef(angle_upper_plane, 0.0f, 0.0f, 1.0f);
			DrawPlane();
		}
		DrawSmokeUpper(r, angle, letter_x, r_saffron_smoke, g_saffron_smoke, b_saffron_smoke, r_white_smoke, r_green_smoke, g_green_smoke, b_green_smoke);

		// middle
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(middle_plane_x, 0.0f, -3.0f);
		DrawPlane();

		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(0.0f, 0.0f, -3.0f);
		DrawSmokeMiddle(letter_x, middle_plane_x, r_saffron_smoke, g_saffron_smoke, b_saffron_smoke, r_white_smoke, r_green_smoke, g_green_smoke, b_green_smoke,bDoneDetachPlanes);

		// lower
		if (bDoneUpperPlane == false)
		{
			glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
			glLoadIdentity();	// replaces the current matrix with the identity matrix.
			glTranslatef(-2.0f*letter_x + letter_end, -1.22f, -3.0f);
			glTranslatef(r*(GLfloat)cos(angle_lower), r*(GLfloat)sin(angle_lower), 0.0f);
			glRotatef(angle_lower_plane, 0.0f, 0.0f, 1.0f);
			DrawPlane();
		}
		DrawSmokeLower(r, angle_lower, letter_x, r_saffron_smoke, g_saffron_smoke, b_saffron_smoke, r_white_smoke, r_green_smoke, g_green_smoke, b_green_smoke);
	}

	// A's band
	if (bDoneMidddlePlane == true)
	{
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(letter_x*2.0f, 0.0f, -3.0f);

		DrawABand();
		bDoneABand = true;
	}

	// Detach planes
	if (bDoneABand == true)
	{
		// upper
		if (bDoneDetachPlanes == false)
		{
			glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
			glLoadIdentity();	// replaces the current matrix with the identity matrix.
			glTranslatef(2.0f*letter_x - letter_end, 1.22f, -3.0f);
			glTranslatef(r*(GLfloat)cos(angle_right_upper), r*(GLfloat)sin(angle_right_upper), 0.0f);
			glRotatef(angle_upper_plane, 0.0f, 0.0f, 1.0f);

			DrawPlane();
		}
		DrawSmokeRightUpper(r, angle_right_upper, letter_x, r_saffron_smoke, g_saffron_smoke, b_saffron_smoke, r_white_smoke, r_green_smoke, g_green_smoke, b_green_smoke);

		// middle
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(middle_plane_x, 0.0f, -3.0f);
		DrawPlane();

		// lower
		if (bDoneDetachPlanes == false)
		{
			glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
			glLoadIdentity();	// replaces the current matrix with the identity matrix.
			glTranslatef(2.0f*letter_x - letter_end, -1.22f, -3.0f);
			glTranslatef(r*(GLfloat)cos(angle_right_lower), r*(GLfloat)sin(angle_right_lower), 0.0f);
			glRotatef(angle_lower_plane, 0.0f, 0.0f, 1.0f);

			DrawPlane();
		}
		DrawSmokeRightLower(r, angle_right_lower, letter_x, r_saffron_smoke, g_saffron_smoke, b_saffron_smoke, r_white_smoke, r_green_smoke, g_green_smoke, b_green_smoke);
	}

	if (bDoneFadingSmokes == true && cnt==0)
	{
		PlaySound(MAKEINTRESOURCE(MY_REC), NULL, SND_ASYNC | SND_NODEFAULT | SND_RESOURCE);
		cnt = 1;
	}
	SwapBuffers(ghdc);

	if (bDoneI1 == false)
	{
		i1_x = i1_x + 0.0027f;

		if (i1_x >= -letter_x * 2)
		{
			i1_x = -letter_x * 2.0f;
			bDoneI1 = true;
		}
	}
	else if (bDoneA == false)
	{
		a_x = a_x - 0.0027f;

		if (a_x <= letter_x * 2)
		{
			a_x = letter_x * 2.0f;
			bDoneA = true;
		}
	}
	else if (bDoneN == false)
	{
		n_y = n_y - 0.0027f;

		if (n_y <= 0.0f)
		{
			bDoneN = true;
			n_y = 0.0f;
		}
	}
	else if (bDoneI2 == false)
	{
		i2_y = i2_y + 0.0027f;

		if (i2_y >= 0.0f)
		{
			i2_y = 0.0f;
			bDoneI2 = true;
		}
	}
	else if (bDoneD == false)
	{
		if (bDoneR_saffron == false)
		{
			r_saffron = r_saffron + 2.5f;
			if (r_saffron >= 255.0f)
			{
				r_saffron = 255.0f;
				bDoneR_saffron = true;
			}
		}
		if (bDoneG_saffron == false)
		{
			g_saffron = g_saffron + 1.0f;
			if (g_saffron >= 153.0f)
			{
				g_saffron = 153.0f;
				bDoneG_saffron = true;
			}
		}
		if (bDoneB_saffron == false)
		{
			b_saffron = b_saffron + 0.2f;
			if (b_saffron >= 51.0f)
			{
				b_saffron = 51.0f;
				bDoneB_saffron = true;
			}
		}

		// green
		if (bDoneR_green == false)
		{
			r_green = r_green + 0.1f;
			if (r_green >= 18.0f)
			{
				r_green = 18.0f;
				bDoneR_green = true;
			}
		}
		if (bDoneG_green == false)
		{
			g_green = g_green + 0.7f;
			if (g_green >= 136.0f)
			{
				g_green = 136.0f;
				bDoneG_green = true;
			}
		}
		if (bDoneB_green == false)
		{
			b_green = b_green + 0.1f;
			if (b_green >= 7.0f)
			{
				b_green = 7.0f;
				bDoneB_green = true;
			}
		}
		if (bDoneR_saffron == true && bDoneG_saffron == true && bDoneB_saffron == true && bDoneR_green == true && bDoneG_green == true && bDoneB_green == true)
			bDoneD = true;
	}
	else if (bDoneUpperPlane == false)
	{
		middle_plane_x = middle_plane_x + 0.0049f;


		if (angle_upper_plane <= 0.0f)
			angle_upper_plane = angle_upper_plane + 0.45f;

		if (angle_lower_plane >= 0.0f)
			angle_lower_plane = angle_lower_plane - 0.45f;

		if (r*(GLfloat)sin(angle) <= -1.22f)
		{
			bDoneUpperPlane = true;
			//fprintf_s(gpFile, "\nr*cos(angle)=%f", r*cos(angle));
			//fprintf_s(gpFile, "\nr*sin(angle)=%f", r*sin(angle));
			angle_upper_plane = 0.0f;
			angle_lower_plane = 0.0f;
		}

		angle = angle + 0.005f;
		angle_lower = angle_lower - 0.005f;
	}

	else if (bDoneMidddlePlane == false)
	{
		middle_plane_x = middle_plane_x + 0.0049f;
		if (middle_plane_x >= 2.0f*letter_x - letter_end+0.724918f)
			bDoneMidddlePlane = true;
	}

	else if (bDoneDetachPlanes == false)
	{
		middle_plane_x = middle_plane_x + 0.0049f;

		angle_right_upper = angle_right_upper + 0.005f;
		angle_right_lower = angle_right_lower - 0.005f;
		
		if (angle_upper_plane <= 90.0f)
				angle_upper_plane = angle_upper_plane + 0.47f;
		
		if (angle_lower_plane >= -90.0f)
				angle_lower_plane = angle_lower_plane - 0.47f;
		
		if (middle_plane_x >= letter_x * 6.0f)
			bDoneDetachPlanes = true;
	}
	else if(bDoneFadingSmokes==false)
	{
		if (r_saffron_smoke > 0.0f)
			r_saffron_smoke = r_saffron_smoke - 2.5f;
		if (g_saffron_smoke > 0.0f)
			g_saffron_smoke = g_saffron_smoke - 1.0f;
		if (b_saffron_smoke > 0.0f)
			b_saffron_smoke = b_saffron_smoke - 0.2f;

		if (r_white_smoke > 0.0f)
			r_white_smoke = r_white_smoke - 0.03f;

		if (r_green_smoke > 0.0f)
			r_green_smoke = r_green_smoke - 0.1f;
		if (g_green_smoke > 0.0f)
			g_green_smoke = g_green_smoke - 0.7f;
		if (b_green_smoke > 0.0f)
			b_green_smoke = b_green_smoke - 0.1f;

		if (r_saffron_smoke <= 0 && g_saffron_smoke <= 0 && b_saffron_smoke <= 0 && r_white_smoke && r_green_smoke <= 0 && g_green_smoke <= 0 && b_green_smoke <= 0)
			bDoneFadingSmokes = true;
	}
}

void DrawI()
{
	// code
	glBegin(GL_LINES);

	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(letter_start + 0.02f, half_letter_ht, 0.0f);
	glVertex3f(letter_end - 0.02f, half_letter_ht, 0.0f);
	glVertex3f(0.0f, half_letter_ht, 0.0f);

	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(0.0f, -half_letter_ht, 0.0f);
	glVertex3f(letter_start + 0.02f, -half_letter_ht, 0.0f);
	glVertex3f(letter_end - 0.02f, -half_letter_ht, 0.0f);

	glEnd();
}

void DrawN()
{
	// code
	glBegin(GL_LINES);

	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(letter_start, half_letter_ht + 0.015f, 0.0f);
	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(letter_start, -half_letter_ht - 0.015f, 0.0f);

	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(letter_start, half_letter_ht + 0.015f, 0.0f);
	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(letter_end, -half_letter_ht - 0.015f, 0.0f);

	glVertex3f(letter_end, -half_letter_ht - 0.015f, 0.0f);	// green
	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(letter_end, half_letter_ht + 0.015f, 0.0f);

	glEnd();
}

void DrawD(GLfloat r_saffron, GLfloat g_saffron, GLfloat b_saffron, GLfloat r_green, GLfloat g_green, GLfloat b_green)
{
	// code
	glBegin(GL_LINES);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron
	glVertex3f(letter_start + 0.05f, half_letter_ht, 0.0f);
	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green
	glVertex3f(letter_start + 0.05f, -half_letter_ht, 0.0f);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron
	glVertex3f(letter_start, half_letter_ht, 0.0f);
	glVertex3f(letter_end+0.0135f, half_letter_ht, 0.0f);

	glVertex3f(letter_end, half_letter_ht, 0.0f);
	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green
	glVertex3f(letter_end, -half_letter_ht, 0.0f);

	glVertex3f(letter_end+0.013f, -half_letter_ht, 0.0f);
	glVertex3f(letter_start, -half_letter_ht, 0.0f);

	glEnd();
}

void DrawA()
{
	// code
	glBegin(GL_LINES);

	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(letter_start, -half_letter_ht - 0.015f, 0.0f);
	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(0.0f, half_letter_ht + 0.015f, 0.0f);

	glVertex3f(0.0f, half_letter_ht + 0.015f, 0.0f);
	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(letter_end, -half_letter_ht - 0.015f, 0.0f);

	glEnd();
}

void DrawABand()
{
	glLineWidth(5.0f);
	glBegin(GL_LINES);

	glColor3d(1.0, 0.59765625, 0.19921875);	// saffron
	glVertex3f(letter_start / 2.0f, 0.0166f, 0.0f);
	glVertex3f(letter_end / 2.0f, 0.0166f, 0.0f);

	glColor3d(1.0, 1.0, 1.0);	// white
	glVertex3f(letter_start / 2.0f, 0.0f, 0.0f);
	glVertex3f(letter_end / 2.0f, 0.0f, 0.0f);

	glColor3d(0.0703125, 0.53125, 0.02734375);	// green
	glVertex3f(letter_start / 2.0f, -0.0173f, 0.0f);
	glVertex3f(letter_end / 2.0f, -0.0173f, 0.0f);

	glEnd();
}

void DrawPlane()
{
	GLfloat upper_plane_x = -0.19f * 5.4f;
	GLfloat upper_plane_y = 1.18f;

	glColor3d(186 / 255.0, 226 / 255.0, 238 / 255.0);

	glBegin(GL_QUADS);

	// middle quad
	glVertex3f(0.25f + 0.1f, 0.05f, 0.0f);
	glVertex3f(0.0f + 0.1f, 0.05f, 0.0f);
	glVertex3f(0.0f + 0.1f, -0.05f, 0.0f);
	glVertex3f(0.25f + 0.1f, -0.05f, 0.0f);

	// upper wing
	glVertex3f(0.1f + 0.1f, 0.05f, 0.0f);
	glVertex3f(0.05f + 0.1f, 0.15f, 0.0f);
	glVertex3f(0.0f + 0.1f, 0.15f, 0.0f);
	glVertex3f(0.0f + 0.1f, 0.05f, 0.0f);

	// lower wing
	glVertex3f(0.1f + 0.1f, -0.05f, 0.0f);
	glVertex3f(0.05f + 0.1f, -0.15f, 0.0f);
	glVertex3f(0.0f + 0.1f, -0.15f, 0.0f);
	glVertex3f(0.0f + 0.1f, -0.05f, 0.0f);

	// back
	glVertex3f(0.0f + 0.1f, 0.05f, 0.0f);
	glVertex3f(-0.1f + 0.1f, 0.08f, 0.0f);
	glVertex3f(-0.1f + 0.1f, -0.08f, 0.0f);
	glVertex3f(0.0f + 0.1f, -0.05f, 0.0f);

	glEnd();

	// front triangle
	glBegin(GL_TRIANGLES);

	glVertex3f(0.25f + 0.1f, 0.05f, 0.0f);
	glVertex3f(0.25f + 0.1f, -0.05f, 0.0f);
	glVertex3f(0.30f + 0.1f, 0.00f, 0.0f);

	glEnd();


	// IAF
	// I
	glColor3d(99/255.0, 1.0, 1.0);	// white	// not visible
	glColor3d(99 / 255.0, 99 / 255.0, 99 / 255.0);	// dark gray

	glLineWidth(3.0f);
	glBegin(GL_LINES);

	glVertex3f(-0.01f + 0.1f, 0.03f, 0.0f);
	glVertex3f(0.03f + 0.1f, 0.03f, 0.0f);

	glVertex3f(0.01f + 0.1f, 0.03f, 0.0f);
	glVertex3f(0.01f + 0.1f, -0.03f, 0.0f);

	glVertex3f(-0.01f + 0.1f, -0.03f, 0.0f);
	glVertex3f(0.03f + 0.1f, -0.03f, 0.0f);

	glEnd();

	// A
	glLineWidth(3.0f);
	glBegin(GL_LINES);

	glVertex3f(0.05f + 0.1f, -0.03f, 0.0f);
	glVertex3f(0.07f + 0.1f, 0.03f, 0.0f);

	glVertex3f(0.07f + 0.1f, 0.03f, 0.0f);
	glVertex3f(0.09f + 0.1f, -0.03f, 0.0f);

	glVertex3f((0.05f + 0.07f) / 2.0f + 0.1f, 0.0f, 0.0f);
	glVertex3f((0.07f + 0.09f) / 2.0f + 0.1f, 0.0f, 0.0f);

	glEnd();

	// F
	glLineWidth(3.0f);
	glBegin(GL_LINES);

	glVertex3f(0.12f + 0.1f, -0.03f, 0.0f);
	glVertex3f(0.12f + 0.1f, 0.03f, 0.0f);

	glVertex3f(0.12f + 0.1f, 0.03f, 0.0f);
	glVertex3f(0.16f + 0.1f, 0.03f, 0.0f);

	glVertex3f(0.12f + 0.1f, 0.0f, 0.0f);
	glVertex3f(0.15f + 0.1f, 0.0f, 0.0f);

	glEnd();
}

void DrawSmokeMiddle(GLfloat letter_x, GLfloat middle_plane_x, GLfloat r_saffron, GLfloat g_saffron,GLfloat b_saffron, GLfloat r_white, GLfloat r_green, GLfloat g_green, GLfloat b_green,bool bDoneDetachPlanes)
{
	void DrawI();
	void DrawN();
	void DrawD(GLfloat, GLfloat, GLfloat, GLfloat, GLfloat, GLfloat);

	glLineWidth(5.0f);
	glBegin(GL_LINES);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron
	glVertex3f(-letter_x * 5.4f, 0.0166f, 0.0f);
	glVertex3f(middle_plane_x, 0.0166f, 0.0f);

	glColor3d(r_white, r_white, r_white);	// white
	glVertex3f(-letter_x * 5.4f, 0.0f, 0.0f);
	glVertex3f(middle_plane_x, 0.0f, 0.0f);

	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green
	glVertex3f(-letter_x * 5.4f, -0.0173f, 0.0f);
	glVertex3f(middle_plane_x, -0.0173f, 0.0f);

	glEnd();

	if (bDoneDetachPlanes == true)
	{
		// I1
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(-2.0f*letter_x, 0.0f, -3.0f);
		glLineWidth(15.0f);
		DrawI();

		// N
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(-letter_x, 0.0f, -3.0f);
		glLineWidth(15.0f);
		DrawN();

		// D
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(0.0f, 0.0f, -3.0f);
		glLineWidth(15.0f);
		DrawD(255.0f, 153.0f, 51.0f, 18.0f, 136.0f, 7.0f);

		// I2
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(letter_x, 0.0f, -3.0f);
		glLineWidth(15.0f);
		DrawI();

		// A
		glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
		glLoadIdentity();	// replaces the current matrix with the identity matrix.
		glTranslatef(2.0f*letter_x, 0.0f, -3.0f);
		glLineWidth(15.0f);
		DrawA();
	}
}

void DrawSmokeUpper(GLfloat r, GLfloat angle, GLfloat letter_x, GLfloat r_saffron, GLfloat g_saffron, GLfloat b_saffron, GLfloat r_white, GLfloat r_green, GLfloat g_green, GLfloat b_green)
{
	static GLfloat angle_smoke;

	glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(-2.0f*letter_x + letter_end, 1.22f, -3.0f);

	glPointSize(5.0f);
	glBegin(GL_POINTS);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f((r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_white, r_white, r_white);	// white

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f(r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f((r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glEnd();
}

void DrawSmokeLower(GLfloat r, GLfloat angle_lower, GLfloat letter_x, GLfloat r_saffron, GLfloat g_saffron, GLfloat b_saffron, GLfloat r_white, GLfloat r_green, GLfloat g_green, GLfloat b_green)
{
	static GLfloat angle_smoke;
	
	glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(-2.0f*letter_x + letter_end, -1.22f, -3.0f);

	glPointSize(5.0f);
	glBegin(GL_POINTS);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f((r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_white, r_white, r_white);	// white

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f(r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f((r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glEnd();
}

void DrawSmokeRightUpper(GLfloat r, GLfloat angle_right_upper, GLfloat letter_x, GLfloat r_saffron, GLfloat g_saffron, GLfloat b_saffron, GLfloat r_white, GLfloat r_green, GLfloat g_green, GLfloat b_green)
{
	static GLfloat angle_smoke;

	glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(2.0f*letter_x - letter_end, 1.22f, -3.0f);

	glPointSize(5.0f);
	glBegin(GL_POINTS);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f+0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f((r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_white, r_white, r_white);	// white

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f(r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		glVertex3f((r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glEnd();
}

void DrawSmokeRightLower(GLfloat r, GLfloat angle_right_lower, GLfloat letter_x, GLfloat r_saffron, GLfloat g_saffron, GLfloat b_saffron, GLfloat r_white, GLfloat r_green, GLfloat g_green, GLfloat b_green)
{
	static GLfloat angle_smoke;

	glMatrixMode(GL_MODELVIEW);		// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(2.0f*letter_x - letter_end, -1.22f, -3.0f);

	glPointSize(5.0f);
	glBegin(GL_POINTS);

	glColor3d(r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0);	// saffron

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f((r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_white, r_white, r_white);	// white

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f(r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glColor3d(r_green / 255.0, g_green / 255.0, b_green / 255.0);	// green

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		glVertex3f((r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f);
	}

	glEnd();
}

void Uninitialize()
{
	fprintf_s(gpTraceLog, "\nIn Uninitialize()");

	// Restore fullscreen to normal size and proceed for uninitialization

	SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

	SetWindowPlacement(ghwnd, &wpPrev);	// sets the placement of the window

	SetWindowPos(ghwnd,
		HWND_TOP,
		0,
		0,
		0,
		0,
		SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);	// sets the z-order of the window
																						// SWP_NOZORDER: Retains the current Z order 
																						// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
																						// SWP_NOMOVE: Retains the current position
																						// SWP_NOSIZE: Retains the current size
																						// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.
	ShowCursor(TRUE);

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	// Parameters: HDC, HGLRC; 
									// If hglrc is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. 
									// In this case, hdc is ignored.
		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpTraceLog, "\nLog File Closed successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
	fprintf_s(gpTraceLog, "\nExiting Uninitialize()");
}

void MakeFullscreen()
{
	fprintf_s(gpTraceLog, "\nIn MakeFullscreen()");

	// variable declarations
	MONITORINFO mi;

	dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
	if (dwStyle & WS_OVERLAPPEDWINDOW)
	{
		mi = { sizeof(MONITORINFO) };	// Setting cbSize field of MONITORINFO. Doing so lets GetMonitorInfo() determine the type of structure you are passing to it.

		if (GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))	// MonitorFromWindow() gives HMONITORINFO
		{
			SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

			SetWindowPos(ghwnd,
				HWND_TOP,
				mi.rcMonitor.left,	// x
				mi.rcMonitor.top,	// y
				mi.rcMonitor.right - mi.rcMonitor.left,	// width
				mi.rcMonitor.bottom - mi.rcMonitor.top,	// height
				SWP_NOZORDER | SWP_FRAMECHANGED);	// flags
		}
	}
	ShowCursor(FALSE);
}
