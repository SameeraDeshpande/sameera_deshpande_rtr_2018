#include<GL/freeglut.h>

bool bIsFullScreen = false;
int year = 0, day = 0;

int main(int argc, char* argv[])
{
	// function declarations
	void Initialize(void);
	void Uninitialize(void);
	void reshape(int, int);
	void display(void);
	void keyboard(unsigned char, int, int);
	void mouse(int, int, int, int);


	// code
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First OpenGL Program - Sameera");

	Initialize();

	// callbacks
	glutDisplayFunc(display);
	glutReshapeFunc(reshape);
	glutKeyboardFunc(keyboard);
	glutMouseFunc(mouse);

	glutCloseFunc(Uninitialize);

	glutMainLoop();

	return(0);
}

void Initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glShadeModel(GL_FLAT);
}

void Uninitialize(void)
{
	// code
}

void reshape(int width, int height)
{
	// code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(60.0f, (GLfloat)width / (GLfloat)height, 1.0f, 20.0f);	// Red book's parameters!
}

void display()
{
	// code
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(0.0f, 0.0f, 5.0f,
		0.0f, 0.0f, 0.0f,
		0.0f, 1.0f, 0.0f);

	glPushMatrix();
	
	glutWireSphere(1.0f, 20, 16);	// Sun
									// (GLdouble radius, GLint slices, GLint stacks); slices=no. of longitudes, stacks=no. of latitudes

	glRotatef((GLfloat)year, 0.0f, 1.0f, 0.0f);	// for revolution of Earth

	glTranslatef(2.0f, 0.0f, 0.0f);
	glRotatef((GLfloat)day, 0.0f, 1.0f, 0.0f);	// for rotation of Earth
	glutWireSphere(0.2f, 10, 8);	// Earth

	glPopMatrix();	// replacing the current matrix with the one below it on the stack
					// will give initial state of CTM (Current Transformation Matrix)

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 'Y':
		year = (year + 5) % 360;
		glutPostRedisplay();	// marks the current window as needing to be redisplayed. The next iteration through glutMainLoop, the window's display callback will be called to redisplay the window's normal plane
								// for repainting the window
		break;

	case 'y':
		year = (year - 5) % 360;
		glutPostRedisplay();
		break;

	case 'D':
		day = (day + 10) % 360;
		glutPostRedisplay();
		break;

	case 'd':
		day = (day - 10) % 360;
		glutPostRedisplay();
		break;

	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;

	case 27:	// ASCII for escape key
		glutLeaveMainLoop();
		break;
	}
}

void mouse(int button, int state, int x, int y)
{
	// code:
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;

	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	}
}
