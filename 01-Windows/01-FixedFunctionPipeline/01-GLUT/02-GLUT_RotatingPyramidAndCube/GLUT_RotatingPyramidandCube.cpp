//#include<C:\freeglut\include\GL\freeglut.h>
//#pragma comment(lib,"C:\freeglut\lib\freeglut.lib")

#include<GL/freeglut.h>

bool bIsFullScreen = false;
GLfloat angle_tri = 0.0f;
GLfloat angle_cube = 0.0f;

int main(int argc, char* argv[])
{
	// function declarations
	void Initialize(void);
	void Uninitialize(void);
	void Reshape(int, int);
	void Display(void);
	void Keyboard(unsigned char, int, int);
	void Mouse(int, int, int, int);
	void Update(void);

	// code
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGBA);
	glutInitWindowSize(800, 600);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("My First OpenGL Program - Sameera");

	Initialize();

	// callbacks
	glutDisplayFunc(Display);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Keyboard);
	glutMouseFunc(Mouse);
	glutIdleFunc(Update);
	glutCloseFunc(Uninitialize);

	glutMainLoop();

	return(0);
}

void Initialize(void)
{
	// code
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glShadeModel(GL_SMOOTH);	// selects flat or smooth shading (beautification function)

	glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);	// specifies implementation-specific hints.
														// Parameters : A symbolic constant indicating the behavior to be controlled, A symbolic constant indicating the desired behavior
														// GL_PERSPECTIVE_CORRECTION_HINT : Indicates the quality of color and texture coordinate interpolation 
														// Beautification call
}

void Uninitialize(void)
{
	// code
}

void Reshape(int width, int height)
{
	// code
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);
}

void Display()
{
	// function declarations
	void DrawPyramid();
	void DrawCube();

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

														// pyramid-
	glMatrixMode(GL_MODELVIEW);	// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(-1.5f, 0.0f, -6.0f);
	glRotatef(angle_tri, 0.0f, 1.0f, 0.0f);
	DrawPyramid();

	glMatrixMode(GL_MODELVIEW);	// specifies which matrix is the current matrix.
	glLoadIdentity();	// replaces the current matrix with the identity matrix.
	glTranslatef(1.5f, 0.0f, -6.0f);
	glScalef(0.75f, 0.75f, 0.75f);		// scale***
	glRotatef(angle_cube, 1.0f, 1.0f, 1.0f);
	DrawCube();

	glutSwapBuffers();	// exchanges the front and back buffers if the current pixel format for the window referenced by the specified device context includes a back buffer(double buffer)
}

void DrawPyramid()
{
	glBegin(GL_TRIANGLES);	// parameters: The primitive or primitives that will be created from vertices presented between glBegin() and the subsequent glend()

	// front
	glColor3f(1.0f, 0.0f, 0.0f);	// sets the current color
	glVertex3f(0.0f, 1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// right
	glColor3f(1.0f, 0.0f, 0.0f);	// sets the current color
	glVertex3f(0.0f, 1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// back
	glColor3f(1.0f, 0.0f, 0.0f);	// sets the current color
	glVertex3f(0.0f, 1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	// left
	glColor3f(1.0f, 0.0f, 0.0f);	// sets the current color
	glVertex3f(0.0f, 1.0f, 0.0f);	// Specifies the x and y coordinates of a vertex.
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glEnd();

}

void DrawCube()
{
	glBegin(GL_QUADS);	// parameters: The primitive or primitives that will be created from vertices presented between glBegin() and the subsequent glend()

						// top
	glColor3f(1.0f, 0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);

	// bottom
	glColor3f(0.0f, 1.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// front
	glColor3f(0.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);

	// back
	glColor3f(0.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	// right
	glColor3f(1.0f, 0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, -1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f);

	// left
	glColor3f(1.0f, 1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	glEnd();
}

void Update()
{
	angle_tri = angle_tri + 1.0f;	// greater increment means greater speed of rotation
	if (angle_tri >= 360.0f)	// discipline; angle is never greater than 360 degrees
		angle_tri = 0.0f;

	angle_cube = angle_cube - 1.0f;
	if (angle_cube <= -360.0f)
		angle_cube = 0.0f;

	glutPostRedisplay();
}

void Keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case 27:
		glutLeaveMainLoop();
		break;

	case 'F':
	case 'f':
		if (bIsFullScreen == false)
		{
			glutFullScreen();
			bIsFullScreen = true;
		}
		else
		{
			glutLeaveFullScreen();
			bIsFullScreen = false;
		}
		break;
	}
}

void Mouse(int button, int state, int x, int y)
{
	// code:
	switch (button)
	{
	case GLUT_LEFT_BUTTON:
		break;

	case GLUT_RIGHT_BUTTON:
		glutLeaveMainLoop();
		break;
	}
}
