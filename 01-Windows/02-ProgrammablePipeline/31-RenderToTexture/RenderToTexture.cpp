#include<stdio.h>	// for file i/o
#include<stdlib.h>	// for exit()
#include<windows.h>
#include "vmath.h"
#include "RenderToTexture.h"
#include<GL/glew.h>
#include<gl/GL.h>
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")
#pragma warning(disable : 4996)		// for security warnings

#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define BUFFER_SIZE 1024

//using namespace vmath;	// to avoid using :: operator everywhere

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc;
HGLRC ghrc = NULL;	// OpenGL's rendering context
bool gbIsFullScreen = false;
bool gbIsActiveWindow = false;
FILE* gpFile = NULL;
FILE *gpMeshFile = NULL;
GLuint texture;
GLuint texture_marble;
GLfloat angle = 0.0f;
int targetTextureWidth = 256;
int targetTextureHeight = 256;
int gWidth, gHeight;

GLenum result;
GLuint gShaderProgramObject;

GLuint vao_teapot;		// vertex array object	
GLuint vbo_position_teapot, vbo_element_teapot, vbo_texture_teapot;
GLuint vao_cube;		// vertex array object	(1 also ok since we unbind)
GLuint vbo_position_cube, vbo_texture_cube;		// vertex buffer object
GLuint mvpUniform;		// mvp: model-view projection
GLuint samplerUniform;
GLuint defaultFramebuffer;
GLuint depthRenderBuffer;
vmath::mat4 perspectiveProjectionMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array

enum	// nameless since we are just concerned with its "named" indices
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,		// 1
	AMC_ATTRIBUTE_NORMAL,		// 2
	AMC_ATTRIBUTE_TEXCOORD0,	// 3 (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

struct vec_int
{
	int *p;		// to store basebase address of our vector
	int size;	// to store current size of our dynamically expanding array
};

struct vec_float
{
	float *p;		// to store basebase address of our vector
	int size;	// to store current size of our dynamically expanding array
};

char buffer[BUFFER_SIZE];
struct vec_float *gp_vertex, *gp_texture, *gp_normal;
struct vec_float *gp_vertex_sorted, *gp_texture_sorted, *gp_normal_sorted;
struct vec_int *gp_vertex_indices, *gp_texture_indices, *gp_normal_indices;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int Initialize(void);
	void Display(void);
	void Update(void);

	// variable declarations
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");
	MSG msg;
	WNDCLASSEX wndclass;
	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// code
	// file creation code
	if (fopen_s(&gpFile, "SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// centering the window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Sameera_Application"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = Initialize();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "\nChoosePixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "\nSetPixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "\nwglCreateContext() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "\nwglMakecurrent() Failed");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "\nInitialization Function Succeeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);	// window should be at top
	SetFocus(hwnd);	// window's title bar,etc. should be highlighted

					// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage()
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow == true)
			{
				Update();
			}
			Display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(HWND);
	void Resize(int, int);
	void Uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen(hwnd);
			break;
		}
		break;

	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;

	case  WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		gWidth = LOWORD(lParam);
		gHeight=HIWORD(lParam);
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize()
{
	// function declarations
	void Resize(int, int);
	void Uninitialize(void);
	void loadCubeTexture();
	BOOL loadTeapotTexture(GLuint*, TCHAR[]);
	void LoadMesh(void);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;	// paints onto window, supports OpenGL, double buffer
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32; // R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														// ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
														// returns indices starting from 1. Hence, 0 means error
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
	{
		return(-2);
	}
	// ask OS to give new RC
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL

	if (ghrc == NULL)
	{
		return(-3);
	}
	// now ghrc can do 3D rendering too

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context. 
	{
		return(-4);
	}

	// put on the OpenGL extensions
	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf_s(gpFile, "glewInit() Failed");
		Uninitialize();	// good practice
		DestroyWindow(ghwnd);	// although DestroyWindow() sends WM_DESTROY and our WM_DESTROY calls Uninitialize(), no problem if Uninitialize() is called twice since our Uninitialize() follows the safe release method
	}

	// vertex shader-
	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);	// creates a shader that is given as parameter	// 1st line of programmable pipeline !

															// write vertex shader source code
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec2 out_texCoord;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_texCoord = vTexCoord;" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
				// in vec2 vTexCoord: texcoords are 2 in number
				// uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// gl_Position: in-built variable of shader				

				// give above source code to the vertex shader object
	glShaderSource(
		gVertexShaderObject,	// shader to which the source code is to be given
		1,	// the number of strings in the array
		(const GLchar **)&vertexShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);	// replace the source code in a shader object

		// compile the vertex shader
	glCompileShader(gVertexShaderObject);
	// error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gVertexShaderObject,	// the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gVertexShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
											// When a shader object is created, its information log will be a string of length 0.
				fprintf_s(gpFile, "\nVertex Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				Uninitialize();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
							//DestroyWindow(ghwnd);	// calls Uninitialize() but no problem since our Uninitialize() follows the safe release method
			}
		}
	}
	// vertex converted to fragment now

	// fragment shader-
	// define vertex shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);	// creates a shader that is given as parameter

																// write fragment shader source code
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec2 out_texCoord;" \
		"uniform sampler2D u_sampler;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"fragColor = texture(u_sampler,out_texCoord);"
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt
				// in vec2 out_texCoord: out of vertex shader
				// sampler2D: typedef in GLSL (internally GLuint)
				// out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
				// vec4: function/macro/constructor (of vmath?)
				// fragColor = texture(u_sampler,out_texCoord): this proves that texture is a type of color.
				// texture() is a function of GLSL. u_sampler: Specifies the sampler to which the texture is bound. Texture sampling is the act of retrieving data from a texture. out_texCoord: Specifies the texture coordinates at which texture will be sampled.
				// u_sampler colors out_texCoord and in-turn fragment gets colored										

				// give above source code to the fragment shader object
	glShaderSource(
		gFragmentShaderObject,	// shader to which the source code is to be given
		1,		// the number of strings in the array
		(const GLchar **)&fragmentShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);			// replace the source code in a shader object

				// compile the fragment shader
	glCompileShader(gFragmentShaderObject);
	// error checking
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gFragmentShaderObject,	//  the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gFragmentShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);						// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
										// When a shader object is created, its information log will be a string of length 0.
				fprintf_s(gpFile, "\nFragment Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				Uninitialize();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
							//DestroyWindow(ghwnd);	// calls Uninitialize() but no problem since our Uninitialize() follows the safe release method
			}
		}
	}

	// create shader program object 
	gShaderProgramObject = glCreateProgram();	// same program for all shaders

												// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

																// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

																 // pre-linking binding to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_POSITION,					// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
		// give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_TEXCOORD0,					// the index of the generic vertex attribute to be bound.
		"vTexCoord"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	// link the shader program to your program
	glLinkProgram(gShaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
											// error checking (eg. of link error- version incompatibility of shaders)
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(				// iv= integer vector
		gShaderProgramObject,	//  the program to be queried.
		GL_LINK_STATUS,			// what(object parameter) is to be queried	
		&iProgramLinkStatus		// empty	//  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
	);							// return a parameter from a program object
	if (iProgramLinkStatus == GL_FALSE)	// error present
	{
		// check if the linker has any info. about the error
		glGetProgramiv(gShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetProgramInfoLog(
					gShaderProgramObject,	// the program object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
											// When a program object is created, its information log will be a string of length 0.
				fprintf_s(gpFile, "\nShader Program Link Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				Uninitialize();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
							//DestroyWindow(ghwnd);	// calls Uninitialize() but no problem since our Uninitialize() follows the safe release method
			}
		}
	}

	// post-linking retrieving uniform locations (uniforms are global to shaders since u_mvp_matrix is a part of VS and u_sampler is a part of FS)
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");	// preparation of data transfer from CPU to GPU (binding)
																				// u_mvp_matrix: of GPU; mvpUniform: of CPU
																				// telling it to take location of uniform u_mvp_matrix and give in mvpUniform

	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");

	const GLfloat cubePosition[] = {
		// top
		1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 1.0f,
		// bottom
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		// front
		1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, 1.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		// back
		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, -1.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		// right
		1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		// left
		-1.0f, 1.0f, 1.0f,
		-1.0f, 1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
	};

	const GLfloat cubeTexCoords[] =
	{
		// top
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		// bottom
		0.0f, 0.0f,
		1.0f, 0.0f,
		1.0f, 1.0f,
		0.0f, 1.0f,
		// front
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		// back
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		// right
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f,
		// left
		1.0f, 1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f
	};

	// load and read mesh file
	LoadMesh();

	// 9 lines
	// teapot
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_teapot);		// generate vertex array object names
									// 1: Specifies the number of vertex array object names to generate
									// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
									// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_teapot);		// bind a vertex array object

								// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_teapot);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_teapot);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

														// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

														// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		gp_vertex_sorted->size * sizeof(float),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		gp_vertex_sorted->p,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

														// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

										// elements(indices)
										// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_element_teapot);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_teapot);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
															// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
															// vbo: bind this (the name of a buffer object.)

															// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

															// fill attributes
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		gp_vertex_indices->size * sizeof(int),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		gp_vertex_indices->p,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

			// unbind (LIFO)
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
												// to bind with next buffer

												// texture
	glGenBuffers(1, &vbo_texture_teapot);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_teapot);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

													// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

													// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		gp_texture_sorted->size * sizeof(float),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		gp_texture_sorted->p,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		2,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

														// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	// cube
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_cube);		// generate vertex array object names
											// 1: Specifies the number of vertex array object names to generate
											// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
											// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_cube);		// bind a vertex array object

										// rectangle position
										// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_cube);		// generate buffer object names
												// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
												// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_cube);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
															// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
															// vbo: bind this (the name of a buffer object.)

															// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

															// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(cubePosition),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		cubePosition,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// pointer to the first component of the first generic vertex attribute in the array; offset if V,C,T,N stored in single array (inter-leaved). NULL: no offset since no stride;
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

														// same above steps for C,T,N if any

														// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	// cube texture
	glGenBuffers(1, &vbo_texture_cube);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_texture_cube);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
															// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
															// vbo: bind this (the name of a buffer object.)

															// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

															// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(cubeTexCoords),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		cubeTexCoords,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		2,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

														// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

	glEnable(GL_TEXTURE_2D);	// If enabled, two-dimensional texturing is performed

	// load the texture in texture_smiley
	loadTeapotTexture(&texture_marble, MAKEINTRESOURCE(ID_BITMAP_MARBLE));	// MAKEINTRESOURCE() macro takes int value(101) of your resource and converts into string(TCHAR)
	
	// load the cube texture
	loadCubeTexture();	

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											// specifies clear values[0,1] used by glClear() for the color buffers

	perspectiveProjectionMatrix = vmath::mat4::identity();	// making orthographicProjectionMatrix an identity matrix(diagonals 1)
															// mat4: of vmath

	Resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

BOOL loadTeapotTexture(GLuint *texture, TCHAR imageResourceID[])	// TCHAR imageResourceID[] - string
{
	// variable declarations
	HBITMAP hBitmap = NULL;	// Handles refer to a resource that has been loaded into memory.
							// typedef HANDLE HBITMAP;	
							// typedef PVOID HANDLE;
							// typedef void* PVOID;

	BITMAP bmp;	/* typedef struct tagBITMAP {LONG bmType; LONG bmWidth; LONG bmHeight; LONG bmWidthBytes; WORD bmPlanes; WORD bmBitsPixel; LPVOID bmBits;}BITMAP; */

	BOOL bStatus = FALSE;

	// code
	hBitmap = (HBITMAP)LoadImage(
		GetModuleHandle(NULL),	// returns instance handle of the process which has the resource (hInstance of WinMain)
		imageResourceID,		// id of the resource we want to convert into Bitmap
		IMAGE_BITMAP,			// type of image (bitmap)
		0,						// width of the image (uses the actual resource width)
		0,						// height of the image (uses the actual resource height)
		LR_CREATEDIBSECTION		// how to load bitmap;	DIB = Device(GC) Independent Bitmap
	);	// Image can be icon, cursor or bitmap.

	if (hBitmap)
	{
		bStatus = TRUE;

		// get image data (into bmp)
		GetObject(hBitmap, sizeof(BITMAP), &bmp);	// retrieves information for the specified graphics object
													// hBitmap : handle to graphics object
													// sizeof(BITMAP) : number of bytes of information to be written to the buffer
													// &bmp : Pointer to a buffer that receives the information about the specified graphics object

													// OpenGL functions now

													// set pixel storage mode
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// GL_UNPACK_ALIGNMENT : Specifies the alignment requirements for the start of each pixel row in memory
												// 4 : word alignment (R,G,B,A)

												// allocate memory for textures on GPU and obtain its id in 'texture'
		glGenTextures(1, texture);	// 1 : how many textures to generate
									// texture : starting addr of the array of textures returned as an id

									// bind every element of the array to a target texture (type of texture)
		glBindTexture(GL_TEXTURE_2D, *texture);	// binding the first texture(value) to a 2D array type usage
												// Note: Texture targets become aliases for textures currently bound to them

												// set the state/parameters of the texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// when model is near the viewer; pixel being textured maps to an area lesser than one texture element
																			// sets the texture magnification function to either GL_NEAREST(better performance) or GL_LINEAR(better quality due to interpolation)
																			// GL_TEXTURE_MAG_FILTER : parameter to be tweaked
																			// GL_LINEAR : value of the parameter(this is int : i)

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);	// when model is away from the viewer; pixel being textured maps to an area greater than one texture element
																						// GL_LINEAR_MIPMAP_LINEAR : Maintain GL_LINEAR at all mipmap levels; A mipmap is an ordered set of arrays representing the same image at progressively lower resolutions

																						// fill the data (instead of gluBuild2DMipmaps())
		glTexImage2D(
			GL_TEXTURE_2D,		// where to fill data; the target texture; following 8 para. go in this
			0,					// mipmap level(level of detail). Level 0 is the base image level
			GL_RGB,				// internal image format
			bmp.bmWidth,		// width of the texture image
			bmp.bmHeight,		// height of the texture image
			0,					// border width. Must be 0
			GL_BGR,				// external image data format
			GL_UNSIGNED_BYTE,	// the data type for data (last parameter)
			bmp.bmBits			// actual data : Pointer to the location of the bit values for the bitmap
		);	// specify a two-dimensional texture image

		glGenerateMipmap(GL_TEXTURE_2D);	// texture data in this now

											// unbind texture explicitly
		glBindTexture(GL_TEXTURE_2D, 0);

		// delete hBitmap
		DeleteObject(hBitmap);	// deletes a resource
	}
	return(bStatus);
}

void loadCubeTexture()	// TCHAR imageResourceID[] - string
{
		// OpenGL functions
		// create to render to
		// set pixel storage mode
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// GL_UNPACK_ALIGNMENT : Specifies the alignment requirements for the start of each pixel row in memory
												// 4 : word alignment (R,G,B,A)

												// allocate memory for textures on GPU and obtain its id in 'texture'
		glGenTextures(1, &texture);	// 1 : how many textures to generate
									// texture : starting addr of the array of textures returned as an id

									// bind every element of the array to a target texture (type of texture)
		glBindTexture(GL_TEXTURE_2D, texture);	// binding the first texture(value) to a 2D array type usage
												// *Texture targets become aliases for textures currently bound to them*

		// set the state/parameters of the texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);	// when model is away from the viewer; pixel being textured maps to an area greater than one texture element
																			// GL_LINEAR : value of the parameter(this is int : i)

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);	// to avoid texture repetition

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);	// to avoid texture repetition

		// fill the data (instead of gluBuild2DMipmaps())
		glTexImage2D(
			GL_TEXTURE_2D,		// where to fill data; the target texture; following 8 para. go in this
			0,					// mipmap level(level of detail). Level 0 is the base image level
			GL_RGBA,			// internal image format
			targetTextureWidth,				// width of the texture image
			targetTextureHeight,				// height of the texture image
			0,					// border width. Must be 0
			GL_RGBA,			// external image data format
			GL_UNSIGNED_BYTE,	// the data type for data (last parameter)
			NULL				// actual data : Pointer to the location of the bit values for the bitmap
		);	// specify a two-dimensional texture image

		// create mipmaps for this texture for better image quality
		glGenerateMipmap(GL_TEXTURE_2D);	// texture data in this now

		// create framebuffer
		glGenFramebuffers(
			1,					// Specifies the number of framebuffer object names to generate.
			&defaultFramebuffer	// Specifies an array in which the generated framebuffer object names are stored.
		);

		// bind framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);

		// attach the texture as the a color attachment
		glFramebufferTexture2D(
			GL_FRAMEBUFFER,			// framebuffer target
			GL_COLOR_ATTACHMENT0,	// attachment point
			GL_TEXTURE_2D,			// texture target
			texture,				// texture object whose image is to be attached.
			0						// mipmap level of the texture image to be attached, which must be 0.
		);	// attach a texture image to a framebuffer object

		// depth buffer
		glGenRenderbuffers(1, &depthRenderBuffer);		// since teapot is 3D

		glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);

		glRenderbufferStorage(
			GL_RENDERBUFFER,			// target
			GL_DEPTH_COMPONENT16,		// 16 bit depth
			targetTextureWidth,			// width of DB
			targetTextureHeight			// ht of DB
		);

		// attach DB to FB 
		glFramebufferRenderbuffer(
			GL_FRAMEBUFFER,
			GL_DEPTH_ATTACHMENT,
			GL_RENDERBUFFER,
			depthRenderBuffer
		);

		// unbind framebuffer
		glBindFramebuffer(GL_FRAMEBUFFER, NULL);

		// unbind texture
		glBindTexture(GL_TEXTURE_2D, 0);
}

void LoadMesh(void)
{
	// function declarations
	struct vec_int* create_vec_int();
	struct vec_float* create_vec_float();
	int push_back_vec_int(struct vec_int*, int);
	int push_back_vec_float(struct vec_float*, float);
	void show_vec_int(struct vec_int*);
	void show_vec_float(struct vec_float*);
	int destroy_vec_int(struct vec_int*);
	int destroy_vec_float(struct vec_float*);

	// variable declarations
	const char *space = " ", *slash = "/";
	char *first_token = NULL, *token;
	char *f_entries[3] = { NULL, NULL, NULL };
	int nr_pos_cords = 0, nr_tex_cords = 0, nr_normal_cords = 0, nr_faces = 0;
	int i;

	fopen_s(&gpMeshFile, "teapot.obj", "r");
	if (gpMeshFile == NULL)
	{
		fprintf(stderr, "Error in opening teapot.obj file\n");
		exit(EXIT_FAILURE);
	}

	gp_vertex = create_vec_float();
	gp_texture = create_vec_float();
	gp_normal = create_vec_float();

	gp_vertex_indices = create_vec_int();
	gp_texture_indices = create_vec_int();
	gp_normal_indices = create_vec_int();

	while (fgets(buffer, BUFFER_SIZE, gpMeshFile) != NULL)	// stops when either 1024 bytes are read or newline is read or EOF is read, whichever come first. It maintains its state across calls
	{
		first_token = strtok(buffer, space);	// reads token by token

		if (strcmp(first_token, "v") == 0)
		{
			nr_pos_cords++;
			while ((token = strtok(NULL, space)) != NULL)	// now NULL as 1st para of strtok() ok as it now knows the str(i.e. buffer)
				push_back_vec_float(gp_vertex, (float)atof(token));	// push all components of position (i.e.x,y,z) into vector of vertices
		}
		else if (strcmp(first_token, "vt") == 0)
		{
			nr_tex_cords++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_vec_float(gp_texture, (float)atof(token));	// push all components of texture(s,t) into vector of texcoords
		}
		else if (strcmp(first_token, "vn") == 0)
		{
			nr_normal_cords++;
			while ((token = strtok(NULL, space)) != NULL)
				push_back_vec_float(gp_normal, (float)atof(token));	// push all components of normal into vector of normals
		}
		else if (strcmp(first_token, "f") == 0)
		{
			nr_faces++;
			for (i = 0; i < 3; i++)	// 3 vertices of  1 triangle
				f_entries[i] = strtok(NULL, space);

			for (i = 0; i < 3; i++)
			{
				token = strtok(f_entries[i], slash);	// get index of vertex position
				push_back_vec_int(gp_vertex_indices, atoi(token) - 1);	// data for each vertex
				token = strtok(NULL, slash);	// get index of vertex texcoords
				push_back_vec_int(gp_texture_indices, atoi(token) - 1);
				token = strtok(NULL, slash);	// get index of vertex normals
				push_back_vec_int(gp_normal_indices, atoi(token) - 1);
			}
		}
	}

	gp_vertex_sorted = create_vec_float();
	for (int i = 0; i < gp_vertex_indices->size; i++)
		push_back_vec_float(gp_vertex_sorted, gp_vertex->p[i]);		// how does this sort?

	gp_texture_sorted = create_vec_float();
	for (int i = 0; i < gp_texture_indices->size; i++)
		push_back_vec_float(gp_texture_sorted, gp_texture->p[i]);

	gp_normal_sorted = create_vec_float();
	for (int i = 0; i < gp_normal_indices->size; i++)
		push_back_vec_float(gp_normal_sorted, gp_normal->p[i]);

	fclose(gpMeshFile);
	gpMeshFile = NULL;
}

struct vec_int* create_vec_int()
{
	struct vec_int* p_new = NULL;
	p_new = (struct vec_int*)malloc(sizeof(struct vec_int));
	if (p_new == NULL)
	{
		puts("Error while allocating memory");
		exit(-1);
	}
	memset(p_new, 0, sizeof(struct vec_int));
	return(p_new);
}

struct vec_float* create_vec_float()
{
	struct vec_float* p_new = NULL;
	p_new = (struct vec_float*)malloc(sizeof(struct vec_float));
	if (p_new == NULL)
	{
		puts("Error while allocating memory");
		exit(-1);
	}
	memset(p_new, 0, sizeof(struct vec_float));
	return(p_new);
}

int push_back_vec_int(struct vec_int* p_vec_int, int new_data)
{
	p_vec_int->p = (int*)realloc(p_vec_int->p, (p_vec_int->size + 1) * sizeof(int));
	if (p_vec_int->p == NULL)
	{
		puts("Error while allocating memory");
		exit(-1);
	}
	p_vec_int->size = p_vec_int->size + 1;
	p_vec_int->p[p_vec_int->size - 1] = new_data;
	return(0);
}

int push_back_vec_float(struct vec_float* p_vec_float, float new_data)
{
	p_vec_float->p = (float*)realloc(p_vec_float->p, (p_vec_float->size + 1) * sizeof(int));
	if (p_vec_float->p == NULL)
	{
		puts("Error while allocating memory");
		exit(-1);
	}
	p_vec_float->size = p_vec_float->size + 1;
	p_vec_float->p[p_vec_float->size - 1] = new_data;
	return(0);
}

void show_vec_int(struct vec_int* p_vec_int)
{
	for (int i = 0;i < p_vec_int->size;i++)
	{
		fprintf(gpFile, "%d\n", p_vec_int->p[i]);
	}
}

void show_vec_float(struct vec_float* p_vec_float)
{
	for (int i = 0;i < p_vec_float->size;i++)
	{
		fprintf(gpFile, "%f\n", p_vec_float->p[i]);
	}
}

int destroy_vec_int(struct vec_int *p_vec_int)
{
	free(p_vec_int->p);
	free(p_vec_int);
	return (0);
}

int destroy_vec_float(struct vec_float *p_vec_float)
{
	free(p_vec_float->p);
	free(p_vec_float);
	return (0);
}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// ? other values of near & far not working
																												// parameters:
																												// fovy- The field of view angle, in degrees, in the y - direction.
																												// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																												// zNear- The distance from the viewer to the near clipping plane(always positive).
																												// zFar- The distance from the viewer to the far clipping plane(always positive).*/
}

void Display()
{
	// code
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

	// One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	glUseProgram(gShaderProgramObject);	// binding your OpenGL code with shader program object
										// Specifies the handle of the program object whose executables are to be used as part of current rendering state.
	
	// 4 CPU steps
	// declaration of matrices
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;

	// initialize above matrices to identity (not initialised in above step for better visibility and understanding)
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();

	// teapot
	// render to our targetTexture by binding the framebuffer
	glBindFramebuffer(GL_FRAMEBUFFER, defaultFramebuffer);

	// set viewport
	glViewport(0, 0, targetTextureWidth, targetTextureHeight);		// why 256,256?

	// Clear the attachment(s).
	glClearColor(0.25f, 0.25f, 0.25f, 1);   // clear to blue
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// draw teapot
	// do necessary transformation (T,S,R)
	translationMatrix = vmath::translate(0.0f, -1.5f, -10.0f);
	rotationMatrix = vmath::rotate(0.0f, angle, 0.0f);

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)targetTextureWidth / (GLfloat)targetTextureHeight, 0.1f, 100.0f);

	// do necessary matrix multiplication 
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// fill and send uniforms
	// 3 texture lines before vao (ABU)
	// A: select active texture unit (there are usually total 80 texture units for a single geometry)
	glActiveTexture(GL_TEXTURE0);	// GL_TEXTURE0: of OGL; matches with our AMC_ATTRIBUTE_TEXCOORD0

	// B: bind a named texture to a texturing target
	glBindTexture(GL_TEXTURE_2D, texture_marble);

	// U: specify the value of a uniform variable for the current program object(sending uniform); glUniformMatrix4fv() is used to send matrices as uniforms
	glUniform1i(samplerUniform, 0);		// tell OGL that you are sending uniform as one integer and give that to FS
										// samplerUniform: Specifies the location of the uniform variable to be modified.
										// 0: Specifies the new values to be used for the specified uniform variable; 0 means unit 0

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_teapot);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

								// bind vbo
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_element_teapot);

	// draw the necessary scene!
	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gp_vertex_indices->size, // the number of elements to be rendered (count)
		GL_UNSIGNED_INT,	//  type of the values in last parameter
		NULL	// Specifies a pointer to the location where the indices are stored (indices)
	);	// When glDrawElements is called, it uses 'count' sequential indices from 'indices' to lookup elements in enabled arrays to construct a sequence of geometric primitives. 
		// 'mode' specifies what kind of primitives are constructed, and how the array elements construct these primitives. 

	// unbind texture
	glBindTexture(GL_TEXTURE_2D, 0);

	// unbind vao
	glBindVertexArray(0);

	// unbind fbo
	glBindFramebuffer(GL_FRAMEBUFFER, NULL);

	// render to the screen
	// render the cube with the texture we just rendered to
	//glBindTexture(GL_TEXTURE_2D, texture);

	// set viewport
	glViewport(0, 0, gWidth, gHeight);

	// Clear the canvas AND the depth buffer.
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);  
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	translationMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = vmath::rotate(angle, angle, angle);
	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)gWidth / (GLfloat)gHeight, 0.1f, 100.0f);
	modelViewMatrix = translationMatrix * rotationMatrix;

	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

	// fill and send uniforms
	// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// texture
	// 3 texture lines before vao (ABU)
	// A: select active texture unit (there are usually total 80 texture units for a single geometry)
	glActiveTexture(GL_TEXTURE0);	// GL_TEXTURE0: of OGL; matches with our AMC_ATTRIBUTE_TEXCOORD0

	// B: bind a named texture to a texturing target
	glBindTexture(GL_TEXTURE_2D, texture);

	// U: specify the value of a uniform variable for the current program object(sending uniform); glUniformMatrix4fv() is used to send matrices as uniforms
	glUniform1i(samplerUniform, 0);		// tell OGL that you are sending uniform as one integer and give that to FS
										// samplerUniform: Specifies the location of the uniform variable to be modified.
										// 0: Specifies the new values to be used for the specified uniform variable; 0 means unit 0

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_cube);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

	// draw the necessary scene!
	glDrawArrays(GL_TRIANGLE_FAN,		// PP does not have GL_QUADS! ;	what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,								// array position of your array to start with (imp in inter-leaved)
		4								// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled, no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn
	glDrawArrays(GL_TRIANGLE_FAN,
		4,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		8,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		12,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		16,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		20,
		4
	);

	// unbind texture
	glBindTexture(GL_TEXTURE_2D, 0);

	// unbind vao
	glBindVertexArray(0);

	// unuse program
	glUseProgram(0);	// unbinding your OpenGL code with shader program object
						// If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.

	SwapBuffers(ghdc);	// exchanges the front and back buffers if the current pixel format for the window referenced by the specified device context includes a back buffer(double buffer)
}

void Update(void)
{
	angle = angle - 0.3f;
	if (angle <= -360.0f)
		angle = 0.0f;
}

void Uninitialize()
{
	glDeleteTextures(1, &texture);
	glDeleteTextures(1, &texture_marble);

	if (vbo_texture_teapot)
	{
		glDeleteBuffers(1, &vbo_texture_teapot);	// delete named buffer objects
													// 1: number of buffer objects to be deleted
													// &vbo: array of buffer objects to be deleted
		vbo_texture_teapot = 0;
	}

	if (vbo_element_teapot)
	{
		glDeleteBuffers(1, &vbo_element_teapot);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_element_teapot = 0;
	}

	if (vbo_position_teapot)
	{
		glDeleteBuffers(1, &vbo_position_teapot);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_position_teapot = 0;
	}

	if (vao_teapot)
	{
		glDeleteVertexArrays(1, &vao_teapot);		// delete vertex array objects
		vao_teapot = 0;
	}

	if (vbo_texture_cube)
	{
		glDeleteBuffers(1, &vbo_texture_cube);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_texture_cube = 0;
	}

	if (vbo_position_cube)
	{
		glDeleteBuffers(1, &vbo_position_cube);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_position_cube = 0;
	}

	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);		// delete vertex array objects
		vao_cube = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;	// typedef int
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);		// since unused in Display()

												// ask program that how many shaders are attached to it
		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
			&shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);		// dynamic array for shaders since we don't know how many present
		if (pShaders)	// mem allocated
		{
			// take attached shaders  into above array
			glGetAttachedShaders(gShaderProgramObject,		// the program object to be queried
				shaderCount,								// the size of the array for storing the returned object names
				&shaderCount,								// Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
				pShaders									// an array that is used to return the names of attached shader objects(empty now)
			);		// return the handles of the shader objects attached to a program object

			for (shaderNumber = 0;shaderNumber < shaderCount;shaderNumber++)
			{
				// detach each shader
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
																					// 2nd para: Specifies the shader object to be detached.

																					// delete each detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		// delete the shader program
		glDeleteProgram(gShaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	// check if fullscreen. If yes, restore to normal size and proceed for uninitialization
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		// SWP_NOZORDER: Retains the current Z order 
		// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
		// SWP_NOMOVE: Retains the current position
		// SWP_NOSIZE: Retains the current size
		// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.

		ShowCursor(TRUE);
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	// Parameters: HDC, HGLRC; If hglrc(2nd parameter) is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. In this case, hdc is ignored.

		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpFile, "\nLog File Closed Successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
}

void ToggleFullScreen(HWND hwnd)
{
	MONITORINFO mi;

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(hwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(hwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(hwnd, &wpPrev);

		SetWindowPos(hwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}
}
