#include<stdio.h>	// for file i/o
#include<stdlib.h>	// for exit()
#include<windows.h>
#include "vmath.h"
#include<GL/glew.h>
#include<gl/GL.h>
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//using namespace vmath;	// to avoid using :: operator everywhere

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc;
HGLRC ghrc = NULL;	// OpenGL's rendering context
bool gbIsFullScreen = false;
bool gbIsActiveWindow = false;
bool gbLighting = false;
FILE* gpFile = NULL;
GLfloat angle_pyramid = 0.0f;

GLenum result;
GLuint gShaderProgramObject;

GLuint vao_pyramid;		// vertex array object
GLuint vbo_position_pyramid, vbo_normal_pyramid;		// vertex buffer object
GLuint mUniform;		// model matrix
GLuint vUniform;		// view matrix	// taken seperate for freedom
GLuint pUniform;	// projection; seperate since calculation of mormal matrix needs mv matrix only

// red
GLuint laUniform_red;	// intensity of ambient light
GLuint ldUniform_red;	// intensity of diffused light
GLuint lsUniform_red;	// intensity of specular light
GLuint lightPositionUniform_red;
// blue
GLuint laUniform_blue;	// intensity of ambient light
GLuint ldUniform_blue;	// intensity of diffused light
GLuint lsUniform_blue;	// intensity of specular light
GLuint lightPositionUniform_blue;

GLuint kaUniform;	// coefficient of material's ambient reflectivity
GLuint kdUniform;	// coefficient of material's diffused reflectivity
GLuint ksUniform;	// coefficient of material's specular reflectivity 
GLuint materialShininessUniform; // material shininess
GLuint isLKeyPressedUniform;

vmath::mat4 perspectiveProjectionMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array
vmath::mat4 modelMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array
vmath::mat4 viewMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array

struct Light
{
	GLfloat Ambient[4];
	GLfloat Diffuse[4];
	GLfloat Specular[4];
	GLfloat Position[4];
};

Light lights[2] = { { { 0.0f,0.0f,0.0f,0.0f },{ 1.0f,0.0f,0.0f,1.0f },{ 1.0f,0.0f,0.0f,1.0f },{ -2.0f,0.0f,0.0f,1.0f } },
					{ { 0.0f,0.0f,0.0f,0.0f },{ 0.0f,0.0f,1.0f,1.0f },{ 0.0f,0.0f,1.0f,1.0f },{ 2.0f,0.0f,0.0f,1.0f } } };

// material properties' values
GLfloat material_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 128.0f;

enum	// nameless since we are just concerned with its "named" indices
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,		// 1
	AMC_ATTRIBUTE_NORMAL,		// 2
	AMC_ATTRIBUTE_TEXCOORD0,	// 3 (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int Initialize(void);
	void Display(void);
	void Update(void);

	// variable declarations
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");
	MSG msg;
	WNDCLASSEX wndclass;
	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// code
	// file creation code
	if (fopen_s(&gpFile, "SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// centering the window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Sameera_Application"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = Initialize();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "\nChoosePixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "\nSetPixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "\nwglCreateContext() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "\nwglMakecurrent() Failed");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "\nInitialization Function Succeeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);	// window should be at top
	SetFocus(hwnd);	// window's title bar,etc. should be highlighted

					// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage()
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow == true)
			{
			Update();
			}
			Display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(HWND);
	void Resize(int, int);
	void Uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen(hwnd);
			break;

		case 'L':
		case 'l':
			if (gbLighting == false)
				gbLighting = true;
			else
				gbLighting = false;
			break;
		}
		break;

	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;

	case  WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize()
{
	// function declarations
	void Resize(int, int);
	void Uninitialize(void);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;	// paints onto window, supports OpenGL, double buffer
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32; // R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														// ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
														// returns indices starting from 1. Hence, 0 means error
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
	{
		return(-2);
	}
	// ask OS to give new RC
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL

	if (ghrc == NULL)
	{
		return(-3);
	}
	// now ghrc can do 3D rendering too

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context. 
	{
		return(-4);
	}

	// put on the OpenGL extensions
	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf_s(gpFile, "glewInit() Failed");
		Uninitialize();	// good practice
		DestroyWindow(ghwnd);	// although DestroyWindow() sends WM_DESTROY and our WM_DESTROY calls Uninitialize(), no problem if Uninitialize() is called twice since our Uninitialize() follows the safe release method
	}

	// vertex shader-
	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);	// creates a shader that is given as parameter	// 1st line of programmable pipeline !

															// write vertex shader source code
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec3 vNormal;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform vec3 u_la_red;" \
		"uniform vec3 u_ld_red;" \
		"uniform vec3 u_ls_red;" \
		"uniform vec4 u_light_position_red;" \
		"uniform vec3 u_la_blue;" \
		"uniform vec3 u_ld_blue;" \
		"uniform vec3 u_ls_blue;" \
		"uniform vec4 u_light_position_blue;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shininess;" \
		"uniform int u_isLKeyPressed;" \
		"out vec3 phong_ads_light;" \
		"void main(void)" \
		"{" \
		"if(u_isLKeyPressed==1)" \
		"{" \
		"vec4 eye_coordinates =  u_v_matrix * u_m_matrix * vPosition;" \
		"vec3 t_norm = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);" \
		"																						\n" \		
		"vec3 light_direction_red = normalize(vec3(u_light_position_red - eye_coordinates));" \
		"float tn_dot_lightDirection_red = max(dot(light_direction_red,t_norm),0.0);" \
		"vec3 reflection_vector_red = reflect(-light_direction_red,t_norm);" \
		"vec3 viewer_vector = normalize(vec3(-eye_coordinates.xyz));" \
		"vec3 ambient_red = u_la_red * u_ka;" \
		"vec3 diffuse_red = u_ld_red * u_kd * tn_dot_lightDirection_red;" \
		"vec3 specular_red = u_ls_red * u_ks * pow(max(dot(reflection_vector_red, viewer_vector),0.0),u_material_shininess);" \
		"																						\n" \
		"vec3 light_direction_blue = normalize(vec3(u_light_position_blue - eye_coordinates));" \
		"float tn_dot_lightDirection_blue = max(dot(light_direction_blue,t_norm),0.0);" \
		"vec3 reflection_vector_blue = reflect(-light_direction_blue,t_norm);" \
		"vec3 ambient_blue = u_la_blue * u_ka;" \
		"vec3 diffuse_blue = u_ld_blue * u_kd * tn_dot_lightDirection_blue;" \
		"vec3 specular_blue = u_ls_blue * u_ks * pow(max(dot(reflection_vector_red, viewer_vector),0.0),u_material_shininess);" \
		"																						\n" \
		"phong_ads_light = ambient_red + diffuse_red + specular_red + ambient_blue + diffuse_blue + specular_blue;" \
		"}" \
		"else" \
		"{" \
		"phong_ads_light = vec3(1.0, 1.0, 1.0);" \
		"}" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version statement
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
				// eye coordinates are not related to any projection, hence only mv
				// uniform mat4 u_mv_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// mat3 normal_matrix = mat3(u_mv_matrix): typecasting into mat3 gives you the normal matrix i.e. mat3(transpose(inverse(u_mv_matrix))); It gives first upper 3*3 matrix of the 4*4 matrix. transpose(), inverse() are shader functions
				// t_norm: transformed normal. normalize() is a shader function (our 'n')
				// vec3(u_light_position - eye_coordinates)= typecasting vec4(x,y,z,w) into vec3;
				// tn_dot_lightDirection (s.n): tn=transformed normal
				// reflect(-light_direction,t_norm): -light_direction since reflected ray is opposite to light direction
				// viewer_vector = normalize(vec3(-eye_coordinates.xyx)): -eye_coordinates since viewer's coordinates are opposite to object's eye coordinates
				// ambient = u_la * u_ka: Ia = La * Ka
				// diffuse = u_ld * u_kd * tn_dot_lightDirection: Id = Ld * Kd * (s.n)
				// specular = u_ls * u_ks * pow(max(dot(reflection_vector * viewer_vector),0.0),u_material_shininess): Is = Ls * Ks * (r.v)^f where r: reflection vector, v: viewer vector, f: material shininess (f= exponential factor)
				// phong_ads_light = ambient + diffuse + specular: I = Ia + Id + Is
				// gl_Position: in-built variable of shader				
				// u_p_matrix * u_v_matrix * u_m_matrix: opposite sequence of mvp

				// give above source code to the vertex shader object
	glShaderSource(
		gVertexShaderObject,	// shader to which the source code is to be given
		1,	// the number of strings in the array
		(const GLchar **)&vertexShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);	// replace the source code in a shader object

		// compile the vertex shader
	glCompileShader(gVertexShaderObject);
	// error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	// check
	glGetShaderiv(	// iv= integer vector
		gVertexShaderObject,	// the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gVertexShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
											// When a shader object is created, its information log will be a string of length 0.
				fprintf_s(gpFile, "\nVertex Shader Compilation Log : \n%s", szInfoLog);
				free(szInfoLog);	// not needed now
				Uninitialize();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
							//DestroyWindow(ghwnd);	// calls Uninitialize() but no problem since our Uninitialize() follows the safe release method
			}
		}
	}
	// vertex converted to fragment now

	// fragment shader-
	// define vertex shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);	// creates a shader that is given as parameter

																// write fragment shader source code
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec3 phong_ads_light;" \
		"out vec4 frag_color;" \
		"void main(void)" \
		"{" \
		"frag_color = vec4(phong_ads_light,1.0);" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt
				// in vec4 out_color: VS's out is FS's in. Hence, same name. (gl_Position is in-built hence need not be passed)
				// out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
				// vec4: function/macro/constructor (of vmath?)
				// frag_color = vec4(diffuse_color,1.0): 1.0 just for converting vec3 to vec4
				// actually, checking the state of u_isLKeyPressed again is not a good code here. Instead, we should pass a variable from VS to FS for that state. But we are checking it here to explain the concept that uniforms(u_isLKeyPressed) are global to shaders.
				// frag_color = vec4(1.0,1.0,1.0,1.0): will give white color to fragment; don't write 'f' in shaders (if class) 

				// give above source code to the fragment shader object
	glShaderSource(
		gFragmentShaderObject,	// shader to which the source code is to be given
		1,		// the number of strings in the array
		(const GLchar **)&fragmentShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);			// replace the source code in a shader object

				// compile the fragment shader
	glCompileShader(gFragmentShaderObject);
	// error checking
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	// check
	glGetShaderiv(	// iv= integer vector
		gFragmentShaderObject,	//  the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gFragmentShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);						// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
										// When a shader object is created, its information log will be a string of length 0.
				fprintf_s(gpFile, "\nFragment Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				Uninitialize();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
							//DestroyWindow(ghwnd);	// calls Uninitialize() but no problem since our Uninitialize() follows the safe release method
			}
		}
	}

	// create shader program object 
	gShaderProgramObject = glCreateProgram();	// same program for all shaders

												// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

																// attach fragment shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

																 // pre-linking binding to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_POSITION,					// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
		// give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_NORMAL,					// the index of the generic vertex attribute to be bound.
		"vNormal"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	// link the shader program to your program
	glLinkProgram(gShaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
											// error checking (eg. of link error- version incompatibility of shaders)
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(				// iv= integer vector
		gShaderProgramObject,	// the program to be queried.
		GL_LINK_STATUS,			// what(object parameter) is to be queried	
		&iProgramLinkStatus		// empty	//  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
	);							// return a parameter from a program object
	if (iProgramLinkStatus == GL_FALSE)	// error present
	{
		// check if the linker has any info. about the error
		glGetProgramiv(gShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetProgramInfoLog(
					gShaderProgramObject,	// the program object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
											// When a program object is created, its information log will be a string of length 0.
				fprintf_s(gpFile, "\nShader Program Link Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				Uninitialize();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
							//DestroyWindow(ghwnd);	// calls Uninitialize() but no problem since our Uninitialize() follows the safe release method
			}
		}
	}

	// post-linking retrieving uniform locations (uniforms are global to shaders)
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");	// preparation of data transfer from CPU to GPU (binding)
																			// gShaderProgramObject: Specifies the program object to be queried; u_mv_matrix: of GPU; mvUniform: of CPU
																			// telling it to take location of uniform u_mv_matrix and give in mvUniform
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	laUniform_red = glGetUniformLocation(gShaderProgramObject, "u_la_red");
	ldUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ld_red");
	lsUniform_red = glGetUniformLocation(gShaderProgramObject, "u_ls_red");
	lightPositionUniform_red = glGetUniformLocation(gShaderProgramObject, "u_light_position_red");
	laUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_la_blue");
	ldUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_ld_blue");
	lsUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_ls_blue");
	lightPositionUniform_blue = glGetUniformLocation(gShaderProgramObject, "u_light_position_blue");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	isLKeyPressedUniform = glGetUniformLocation(gShaderProgramObject, "u_isLKeyPressed");

	const GLfloat pyramidPosition[] = {
		// front
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, 1.0f,
		// right
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, 1.0f,
		1.0f, -1.0f, -1.0f,
		// back
		0.0f, 1.0f, 0.0f,
		1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, -1.0f,
		// left
		0.0f, 1.0f, 0.0f,
		-1.0f, -1.0f, -1.0f,
		-1.0f, -1.0f, 1.0f
	};	// since written in Initialize() only, coords will not get set everytime in Display(). Hence, fast speed.

	const GLfloat pyramidNormal[] = {
		// front
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		0.0f, 0.447214f, 0.894427f,
		// right
		0.89427f, 0.447214f, 0.0f,
		0.89427f, 0.447214f, 0.0f,
		0.89427f, 0.447214f, 0.0f,
		// back
		0.0f, 0.447214f, -0.89427f,
		0.0f, 0.447214f, -0.89427f,
		0.0f, 0.447214f, -0.89427f,
		// left
		-0.89427f, 0.447214f, 0.0f,
		-0.89427f, 0.447214f, 0.0f,
		-0.89427f, 0.447214f, 0.0f
	};

	// 9 lines
	// pyramid
	// create vao(id) (vao is shape/object-wise)
	glGenVertexArrays(1, &vao_pyramid);		// generate vertex array object names
											// 1: Specifies the number of vertex array object names to generate
											// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
											// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_pyramid);		// bind a vertex array object

										// pyramid position
										// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_pyramid);		// generate buffer object names
												// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
												// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_pyramid);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
																// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
																// vbo: bind this (the name of a buffer object.)

																// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

																// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(pyramidPosition),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		pyramidPosition,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// pointer to the first component of the first generic vertex attribute in the array; offset if V,C,T,N stored in single array (inter-leaved). NULL: no offset since no stride;
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

														// same above steps for C,T,N if any

														// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	// pyramid normals
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_normal_pyramid);		// generate buffer object names
												// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
												// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_normal_pyramid);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
															// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
															// vbo: bind this (the name of a buffer object.)

															// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

															// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(pyramidNormal),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		pyramidNormal,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// pointer to the first component of the first generic vertex attribute in the array; offset if V,C,T,N stored in single array (inter-leaved). NULL: no offset since no stride;
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

														// same above steps for C,T,N if any

														// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	// unbind vao
	glBindVertexArray(0);

	glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

	glDisable(GL_CULL_FACE);		// If enabled, cull polygons based on their winding in window coordinates; this is def. in OGL

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											// specifies clear values[0,1] used by glClear() for the color buffers

	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();
	perspectiveProjectionMatrix = vmath::mat4::identity();	// making perspectiveProjectionMatrix an identity matrix(diagonals 1)
															// mat4: of vmath

	Resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// ? other values of near & far not working
																												// parameters:
																												// fovy- The field of view angle, in degrees, in the y - direction.
																												// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																												// zNear- The distance from the viewer to the near clipping plane(always positive).																											// zFar- The distance from the viewer to the far clipping plane(always positive).*/
}

void Display()
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

														// One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	glUseProgram(gShaderProgramObject);	// binding your OpenGL code with shader program object
										// Specifies the handle of the program object whose executables are to be used as part of current rendering state.

	// Pyramid
	// 4 CPU steps
	// declaration of matrices
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;

	// initialize above matrices to identity (not initialised in above step for better visibility and understanding)
	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();

	// do necessary transformation (T,S,R)
	translationMatrix = vmath::translate(0.0f, 0.25f, -4.0f);
	rotationMatrix = vmath::rotate(angle_pyramid, 0.0f, 1.0f, 0.0f);

	// do necessary matrix multiplication 
	modelMatrix = translationMatrix * rotationMatrix;

	// fill and send uniforms
	// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mUniform,		// uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelMatrix					// actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
	);

	glUniformMatrix4fv(vUniform,		// uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		viewMatrix					// actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
	);

	glUniformMatrix4fv(pUniform,		// uniform in which perspectiveProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		perspectiveProjectionMatrix		// actual matrix; this will bind to pUniform which is bound to u_p_matrix
	);

	if (gbLighting == true)
	{
		glUniform1i(isLKeyPressedUniform, 1);	// send 1 if 'L' key is pressed; 1i: to send 1 int

		glUniform3fv(laUniform_red, 1, lights[0].Ambient);	// to send vec3
		glUniform3fv(ldUniform_red, 1, lights[0].Diffuse);	// to send vec3
		glUniform3fv(lsUniform_red, 1, lights[0].Specular);	// to send vec3
		glUniform4fv(lightPositionUniform_red, 1, lights[0].Position);	// to send vec4; ~glLightfv(GL_LIGHT0, GL_POSITION, LightPosition) in FFP

		glUniform3fv(laUniform_blue, 1, lights[1].Ambient);	// to send vec3
		glUniform3fv(ldUniform_blue, 1, lights[1].Diffuse);	// to send vec3
		glUniform3fv(lsUniform_blue, 1, lights[1].Specular);	// to send vec3
		glUniform4fv(lightPositionUniform_blue, 1, lights[1].Position);	// to send vec4; ~glLightfv(GL_LIGHT0, GL_POSITION, LightPosition) in FFP

		glUniform3fv(kaUniform, 1, material_ambient);	// gray material; to send vec3
		glUniform3fv(kdUniform, 1, material_diffuse);	// gray material; to send vec3
		glUniform3fv(kdUniform, 1, material_specular);	// gray material; to send vec3
		glUniform1f(materialShininessUniform, material_shininess);	// gray material; to send vec3
	}
	else
	{
		glUniform1i(isLKeyPressedUniform, 0);	// send 0 if 'L' key is not pressed; 1i: to send 1 int
	}
																// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_pyramid);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

										// similarly, bind with textures if any (call glBindTexture() here)

										// draw the necessary scene!
	glDrawArrays(GL_TRIANGLES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,							// array position of your 9-member array to start with (imp in inter-leaved)
		12							// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	glBindVertexArray(0);

	// unuse program
	glUseProgram(0);	// unbinding your OpenGL code with shader program object
						// If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.

	SwapBuffers(ghdc);	// exchanges the front and back buffers if the current pixel format for the window referenced by the specified device context includes a back buffer(double buffer)
}

void Update(void)
{
	angle_pyramid = angle_pyramid + 1.0f;	// greater increment means greater speed of rotation
	if (angle_pyramid >= 360.0f)			// discipline; angle is never greater than 360 degrees
		angle_pyramid = 0.0f;
}

void Uninitialize()
{
	// opposite sequence of vao and vbo also ok
	if (vbo_normal_pyramid)
	{
		glDeleteBuffers(1, &vbo_normal_pyramid);	// delete named buffer objects
													// 1: number of buffer objects to be deleted
													// &vbo: array of buffer objects to be deleted
		vbo_normal_pyramid = 0;
	}

	if (vbo_position_pyramid)
	{
		glDeleteBuffers(1, &vbo_position_pyramid);	// delete named buffer objects
													// 1: number of buffer objects to be deleted
													// &vbo: array of buffer objects to be deleted
		vbo_position_pyramid = 0;
	}

	if (vao_pyramid)
	{
		glDeleteVertexArrays(1, &vao_pyramid);		// delete vertex array objects
		vao_pyramid = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;	// typedef int
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);		// since unused in Display()

												// ask program that how many shaders are attached to it
		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
			&shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);		// dynamic array for shaders since we don't know how many present
		if (pShaders)	// mem allocated
		{
			// take attached shaders  into above array
			glGetAttachedShaders(gShaderProgramObject,		// the program object to be queried
				shaderCount,								// the size of the array for storing the returned object names
				&shaderCount,								// Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
				pShaders									// an array that is used to return the names of attached shader objects(empty now)
			);		// return the handles of the shader objects attached to a program object

			for (shaderNumber = 0;shaderNumber < shaderCount;shaderNumber++)
			{
				// detach each shader
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
																					// 2nd para: Specifies the shader object to be detached.

																					// delete each detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		// delete the shader program
		glDeleteProgram(gShaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	// check if fullscreen. If yes, restore to normal size and proceed for uninitialization
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		// SWP_NOZORDER: Retains the current Z order 
		// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
		// SWP_NOMOVE: Retains the current position
		// SWP_NOSIZE: Retains the current size
		// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.

		ShowCursor(TRUE);
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	// Parameters: HDC, HGLRC; If hglrc(2nd parameter) is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. In this case, hdc is ignored.

		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpFile, "\nLog File Closed Successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
}

void ToggleFullScreen(HWND hwnd)
{
	MONITORINFO mi;

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(hwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(hwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(hwnd, &wpPrev);

		SetWindowPos(hwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}
}
