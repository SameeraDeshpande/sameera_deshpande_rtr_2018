#include<stdio.h>	// for file i/o
#include<stdlib.h>	// for exit()
#include<windows.h>
#include "vmath.h"
#include "Interleaved.h"
#include<GL/glew.h>
#include<gl/GL.h>
#pragma comment(lib,"glew32.lib")
#pragma comment(lib,"opengl32.lib")

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//using namespace vmath;	// to avoid using :: operator everywhere

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
HDC ghdc;
HGLRC ghrc = NULL;	// OpenGL's rendering context
bool gbIsFullScreen = false;
bool gbIsActiveWindow = false;
FILE* gpFile = NULL;
GLuint texture_marble;
GLfloat angle_cube = 0.0f;

GLenum result;
GLuint gShaderProgramObject;

GLuint vao_cube;		// vertex array object	(1 also ok since we unbind)
GLuint vbo_pctn_cube;		// vertex buffer object
GLuint mUniform;		// model matrix
GLuint vUniform;		// view matrix	// taken seperate for freedom
GLuint pUniform;	// projection; seperate since calculation of mormal matrix needs mv matrix onlyGLuint samplerUniform;
GLuint laUniform;	// intensity of ambient light
GLuint ldUniform;	// intensity of diffused light
GLuint lsUniform;	// intensity of specular light
GLuint lightPositionUniform;
GLuint kaUniform;	// coefficient of material's ambient reflectivity
GLuint kdUniform;	// coefficient of material's diffused reflectivity
GLuint ksUniform;	// coefficient of material's specular reflectivity 
GLuint materialShininessUniform; // material shininess
GLuint samplerUniform;

vmath::mat4 perspectiveProjectionMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array

GLfloat light_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_diffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[4] = { 0.0f,0.0f,0.0f,1.0f };

GLfloat material_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 128.0f;

enum	// nameless since we are just concerned with its "named" indices
{
	AMC_ATTRIBUTE_POSITION = 0,
	AMC_ATTRIBUTE_COLOR,		// 1
	AMC_ATTRIBUTE_NORMAL,		// 2
	AMC_ATTRIBUTE_TEXCOORD0,	// 3 (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int Initialize(void);
	void Display(void);
	void Update(void);

	// variable declarations
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");
	MSG msg;
	WNDCLASSEX wndclass;
	bool bDone = false;
	int iRet = 0;
	int iWindow_x, iWindow_y;

	// code
	// file creation code
	if (fopen_s(&gpFile, "SD_Log.txt", "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created"), TEXT("Error"), MB_OK);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully");
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// centering the window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Sameera_Application"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	iRet = Initialize();
	if (iRet == -1)
	{
		fprintf_s(gpFile, "\nChoosePixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -2)
	{
		fprintf_s(gpFile, "\nSetPixelFormat() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -3)
	{
		fprintf_s(gpFile, "\nwglCreateContext() Failed");
		DestroyWindow(hwnd);
	}
	else if (iRet == -4)
	{
		fprintf_s(gpFile, "\nwglMakecurrent() Failed");
		DestroyWindow(hwnd);
	}
	else
	{
		fprintf_s(gpFile, "\nInitialization Function Succeeded");
	}

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);	// window should be at top
	SetFocus(hwnd);	// window's title bar,etc. should be highlighted

					// game loop
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage()
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow == true)
			{
				Update();
			}
			Display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(HWND);
	void Resize(int, int);
	void Uninitialize(void);

	// code
	switch (iMsg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen(hwnd);
			break;
		}
		break;

	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;

	case  WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;

	case WM_SIZE:
		Resize(LOWORD(lParam), HIWORD(lParam));
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

int Initialize()
{
	// function declarations
	void Resize(int, int);
	void Uninitialize(void);
	BOOL loadTexture(GLuint*, TCHAR[]);

	// variable declarations
	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex;
	GLuint gVertexShaderObject;
	GLuint gFragmentShaderObject;

	// code
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR));

	// initialize pfd structure
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1;	// Windows max OGL support is for OGL 1.5 (1 since int)
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;	// paints onto window, supports OpenGL, double buffer
	pfd.iPixelType = PFD_TYPE_RGBA;	// tells type of pixel
	pfd.cColorBits = 32; // R:8+ G:8+ B:8+ A:8 ; c=count
	pfd.cRedBits = 8;	// no. of bits assigned to each color
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	pfd.cDepthBits = 32;

	ghdc = GetDC(ghwnd);

	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);	// iPixelFormatIndex- our var.
														// ChoosePixelFormat function attempts to match an appropriate pixel format supported by a device context to a given pixel format specification.
														// returns indices starting from 1. Hence, 0 means error
	if (iPixelFormatIndex == 0)
	{
		return(-1);
	}

	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)	// sets the pixel format of ghdc to iPixelFormatIndex's pixel format
																// pfd holds the new and actual pixel format set
	{
		return(-2);
	}
	// ask OS to give new RC
	ghrc = wglCreateContext(ghdc);	// The wglCreateContext function creates a new OpenGL rendering context, which is suitable for drawing on the device referenced by hdc. 
									//The rendering context has the same pixel format as the device context.
									// wgl: Graphics Library for Windows, has APIs for OGL

	if (ghrc == NULL)
	{
		return(-3);
	}
	// now ghrc can do 3D rendering too

	if (wglMakeCurrent(ghdc, ghrc) == FALSE)	// makes a specified OpenGL rendering context the calling thread's current rendering context. 
	{
		return(-4);
	}

	// put on the OpenGL extensions
	result = glewInit();
	if (result != GLEW_OK)
	{
		fprintf_s(gpFile, "glewInit() Failed");
		Uninitialize();	// good practice
		DestroyWindow(ghwnd);	// although DestroyWindow() sends WM_DESTROY and our WM_DESTROY calls Uninitialize(), no problem if Uninitialize() is called twice since our Uninitialize() follows the safe release method
	}

	// vertex shader-
	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);	// creates a shader that is given as parameter	// 1st line of programmable pipeline !

															// write vertex shader source code
	const GLchar* vertexShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"in vec2 vTexCoord;" \
		"in vec3 vNormal;" \
		"out vec4 out_color;" \
		"out vec2 out_texCoord;" \
		"out vec3 t_norm;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform vec4 u_light_position;" \
		"void main(void)" \
		"{" \
		"vec4 eye_coordinates =  u_v_matrix * u_m_matrix * vPosition;" \
		"t_norm = mat3(u_v_matrix * u_m_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position - eye_coordinates);" \
		"viewer_vector = vec3(-eye_coordinates.xyz);" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"out_color = vColor;" \
		"out_texCoord = vTexCoord;" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
				// in vec2 vTexCoord: texcoords are 2 in number
				// uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// gl_Position: in-built variable of shader				

				// give above source code to the vertex shader object
	glShaderSource(
		gVertexShaderObject,	// shader to which the source code is to be given
		1,	// the number of strings in the array
		(const GLchar **)&vertexShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);	// replace the source code in a shader object

		// compile the vertex shader
	glCompileShader(gVertexShaderObject);
	// error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gVertexShaderObject,	// the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gVertexShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
											// When a shader object is created, its information log will be a string of length 0.
				fprintf_s(gpFile, "\nVertex Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				Uninitialize();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
							//DestroyWindow(ghwnd);	// calls Uninitialize() but no problem since our Uninitialize() follows the safe release method
			}
		}
	}
	// vertex converted to fragment now

	// fragment shader-
	// define vertex shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);	// creates a shader that is given as parameter

																// write fragment shader source code
	const GLchar* fragmentShaderSourceCode =
		"#version 450 core" \
		"\n" \
		"in vec4 out_color;" \
		"in vec2 out_texCoord;" \
		"in vec3 t_norm;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 fragColor;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shininess;" \
		"uniform sampler2D u_sampler;" \
		"void main(void)" \
		"{" \
		"vec3 normalized_t_norm = normalize(t_norm);" \
		"vec3 normalized_light_direction = normalize(light_direction);" \
		"vec3 normalized_viewer_vector = normalize(viewer_vector);" \
		"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_t_norm);" \
		"float tn_dot_lightDirection = max(dot(normalized_light_direction,normalized_t_norm),0.0);" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 diffuse = u_ld * u_kd * tn_dot_lightDirection;" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_viewer_vector),0.0),u_material_shininess);" \
		"vec3 phong_ads_light = ambient + diffuse + specular;" \
		"vec4 tex = texture(u_sampler,out_texCoord);" \
		"fragColor = out_color * tex * vec4(phong_ads_light,1.0);"
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt
				// in vec2 out_texCoord: out of vertex shader
				// sampler2D: typedef in GLSL (internally GLuint)
				// out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
				// vec4: function/macro/constructor (of vmath?)
				// fragColor = texture(u_sampler,out_texCoord): this proves that texture is a type of color.
				// texture() is a function of GLSL. u_sampler: Specifies the sampler to which the texture is bound. Texture sampling is the act of retrieving data from a texture. out_texCoord: Specifies the texture coordinates at which texture will be sampled.
				// u_sampler colors out_texCoord and in-turn fragment gets colored										

				// give above source code to the fragment shader object
	glShaderSource(
		gFragmentShaderObject,	// shader to which the source code is to be given
		1,		// the number of strings in the array
		(const GLchar **)&fragmentShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);			// replace the source code in a shader object

				// compile the fragment shader
	glCompileShader(gFragmentShaderObject);
	// error checking
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gFragmentShaderObject,	//  the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gFragmentShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);						// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
										// When a shader object is created, its information log will be a string of length 0.
				fprintf_s(gpFile, "\nFragment Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				Uninitialize();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
							//DestroyWindow(ghwnd);	// calls Uninitialize() but no problem since our Uninitialize() follows the safe release method
			}
		}
	}

	// create shader program object 
	gShaderProgramObject = glCreateProgram();	// same program for all shaders

												// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

																// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

																 // pre-linking binding to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_POSITION,					// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
		// give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_COLOR,					// the index of the generic vertex attribute to be bound.
		"vColor"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_TEXCOORD0,					// the index of the generic vertex attribute to be bound.
		"vTexCoord"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_NORMAL,					// the index of the generic vertex attribute to be bound.
		"vNormal"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	// link the shader program to your program
	glLinkProgram(gShaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
											// error checking (eg. of link error- version incompatibility of shaders)
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(				// iv= integer vector
		gShaderProgramObject,	//  the program to be queried.
		GL_LINK_STATUS,			// what(object parameter) is to be queried	
		&iProgramLinkStatus		// empty	//  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
	);							// return a parameter from a program object
	if (iProgramLinkStatus == GL_FALSE)	// error present
	{
		// check if the linker has any info. about the error
		glGetProgramiv(gShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetProgramInfoLog(
					gShaderProgramObject,	// the program object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
											// When a program object is created, its information log will be a string of length 0.
				fprintf_s(gpFile, "\nShader Program Link Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				Uninitialize();	// ok if not written
				exit(0);	// 0 since error not of OS	// ok if not written
							//DestroyWindow(ghwnd);	// calls Uninitialize() but no problem since our Uninitialize() follows the safe release method
			}
		}
	}

	// post-linking retrieving uniform locations (uniforms are global to shaders since u_mvp_matrix is a part of VS and u_sampler is a part of FS)
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");	// preparation of data transfer from CPU to GPU (binding)
																				// u_mvp_matrix: of GPU; mvpUniform: of CPU
																				// telling it to take location of uniform u_mvp_matrix and give in mvpUniform
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");

	// actually it should be PCNT
	const GLfloat cubePCTN[] = {
		// top
		1.0f, 1.0f, -1.0f,		// P
		1.0f,0.0f,0.0f,			// C
		1.0f, 1.0f,				// T
		0.0f, 1.0f, 0.0f,		// N

		-1.0f, 1.0f, -1.0f,		// P
		1.0f,0.0f,0.0f,			// C
		0.0f, 1.0f,				// T
		0.0f, 1.0f, 0.0f,		// N

		-1.0f, 1.0f, 1.0f,
		1.0f,0.0f,0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		1.0f, 1.0f, 1.0f,
		1.0f,0.0f,0.0f,
		1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		// bottom
		1.0f, -1.0f, -1.0f,
		0.0f,1.0f,0.0f,
		0.0f, 0.0f,
		0.0f, -1.0f, 0.0f,

		-1.0f, -1.0f, -1.0f,
		0.0f,1.0f,0.0f,
		1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,

		-1.0f, -1.0f, 1.0f,
		0.0f,1.0f,0.0f,
		1.0f, 1.0f,
		0.0f, -1.0f, 0.0f,

		1.0f, -1.0f, 1.0f,
		0.0f,1.0f,0.0f,
		0.0f, 1.0f,
		0.0f, -1.0f, 0.0f,

		// front
		1.0f, 1.0f, 1.0f,
		0.0f,0.0f,1.0f,
		1.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		-1.0f, 1.0f, 1.0f,
		0.0f,0.0f,1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		-1.0f, -1.0f, 1.0f,
		0.0f,0.0f,1.0f,
		0.0f, 0.0f,
		0.0f, 0.0f, 1.0f,

		1.0f, -1.0f, 1.0f,
		0.0f,0.0f,1.0f,
		1.0f, 0.0f,
		0.0f, 0.0f, 1.0f,

		// back
		-1.0f, 1.0f, -1.0f,
		0.0f,1.0f,1.0f,
		1.0f, 1.0f,
		0.0f, 0.0f, -1.0f,

		1.0f, 1.0f, -1.0f,
		0.0f,1.0f,1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f, -1.0f,

		1.0f, -1.0f, -1.0f,
		0.0f,1.0f,1.0f,
		0.0f, 0.0f,
		0.0f, 0.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		0.0f,1.0f,1.0f,
		0.0f, 0.0f,
		0.0f, 0.0f, -1.0f,

		// right
		1.0f, 1.0f, -1.0f,
		1.0f,0.0f,1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f, 0.0f,

		1.0f, 1.0f, 1.0f,
		1.0f,0.0f,1.0f,
		0.0f, 1.0f,
		1.0f, 0.0f, 0.0f,

		1.0f, -1.0f, 1.0f,
		1.0f,0.0f,1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		1.0f, -1.0f, -1.0f,
		1.0f,0.0f,1.0f,
		1.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		// left
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f,
		-1.0f, 0.0f, 0.0f,

		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 0.0f,
		0.0f, 1.0f,
		-1.0f, 0.0f, 0.0f,

		-1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, 0.0f,
		0.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,

		-1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,
	};

	// fill cube vertices in array (this was in Display() in FFP)
	// cube
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_cube);		// generate vertex array object names
											// 1: Specifies the number of vertex array object names to generate
											// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
											// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_cube);		// bind a vertex array object

										// cube position
										// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_pctn_cube);		// generate buffer object names
												// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
												// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_pctn_cube);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
															// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
															// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)
	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(cubePCTN),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		cubePCTN,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		11 * sizeof(GLfloat),						// the byte offset between consecutive generic vertex attributes; 0=no stride
		0					// pointer to the first component of the first generic vertex attribute in the array; offset if V,C,T,N stored in single array (inter-leaved). NULL: no offset since no stride;
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		11*sizeof(GLfloat),							// the byte offset between consecutive generic vertex attributes; 0=no stride
		(void*)(3 * sizeof(GLfloat))				// pointer to the first component of the first generic vertex attribute in the array; offset if V,C,T,N stored in single array (inter-leaved). NULL: no offset since no stride;
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vPosition

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		2,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		11*sizeof(GLfloat),							// the byte offset between consecutive generic vertex attributes; 0=no stride
		(void*)(6 * sizeof(GLfloat))				// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		11 * sizeof(GLfloat),						// the byte offset between consecutive generic vertex attributes; 0=no stride
		(void*)(8 * sizeof(GLfloat))										// pointer to the first component of the first generic vertex attribute in the array; offset if V,C,T,N stored in single array (inter-leaved). NULL: no offset since no stride;
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vNormal

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

	glEnable(GL_TEXTURE_2D);	// If enabled, two-dimensional texturing is performed

	// load the texture
	loadTexture(&texture_marble, MAKEINTRESOURCE(ID_BITMAP_MARBLE));	// MAKEINTRESOURCE() macro takes int value(101) of your resource and converts into string(TCHAR)

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											// specifies clear values[0,1] used by glClear() for the color buffers

	perspectiveProjectionMatrix = vmath::mat4::identity();	// making orthographicProjectionMatrix an identity matrix(diagonals 1)
															// mat4: of vmath

	Resize(WIN_WIDTH, WIN_HEIGHT);

	return(0);
}

BOOL loadTexture(GLuint *texture, TCHAR imageResourceID[])	// TCHAR imageResourceID[] - string
{
	// variable declarations
	HBITMAP hBitmap = NULL;	// Handles refer to a resource that has been loaded into memory.
							// typedef HANDLE HBITMAP;	
							// typedef PVOID HANDLE;
							// typedef void* PVOID;

	BITMAP bmp;	/* typedef struct tagBITMAP {LONG bmType; LONG bmWidth; LONG bmHeight; LONG bmWidthBytes; WORD bmPlanes; WORD bmBitsPixel; LPVOID bmBits;}BITMAP; */

	BOOL bStatus = FALSE;

	// code
	hBitmap = (HBITMAP)LoadImage(
		GetModuleHandle(NULL),	// returns instance handle of the process which has the resource (hInstance of WinMain)
		imageResourceID,		// id of the resource we want to convert into Bitmap
		IMAGE_BITMAP,			// type of image (bitmap)
		0,						// width of the image (uses the actual resource width)
		0,						// height of the image (uses the actual resource height)
		LR_CREATEDIBSECTION		// how to load bitmap;	DIB = Device(GC) Independent Bitmap
	);	// Image can be icon, cursor or bitmap.

	if (hBitmap)
	{
		bStatus = TRUE;

		// get image data (into bmp)
		GetObject(hBitmap, sizeof(BITMAP), &bmp);	// retrieves information for the specified graphics object
													// hBitmap : handle to graphics object
													// sizeof(BITMAP) : number of bytes of information to be written to the buffer
													// &bmp : Pointer to a buffer that receives the information about the specified graphics object

													// OpenGL functions now

													// set pixel storage mode
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);	// GL_UNPACK_ALIGNMENT : Specifies the alignment requirements for the start of each pixel row in memory
												// 4 : word alignment (R,G,B,A)

												// allocate memory for textures on GPU and obtain its id in 'texture'
		glGenTextures(1, texture);	// 1 : how many textures to generate
									// texture : starting addr of the array of textures returned as an id

									// bind every element of the array to a target texture (type of texture)
		glBindTexture(GL_TEXTURE_2D, *texture);	// binding the first texture(value) to a 2D array type usage
												// *Texture targets become aliases for textures currently bound to them*

												// set the state/parameters of the texture
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// when model is near the viewer; pixel being textured maps to an area lesser than one texture element
																			// sets the texture magnification function to either GL_NEAREST(better performance) or GL_LINEAR(better quality due to interpolation)
																			// GL_TEXTURE_MAG_FILTER : parameter to be tweaked
																			// GL_LINEAR : value of the parameter(this is int : i)

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);	// when model is away from the viewer; pixel being textured maps to an area greater than one texture element
																						// GL_LINEAR_MIPMAP_LINEAR : Maintain GL_LINEAR at all mipmap levels; A mipmap is an ordered set of arrays representing the same image at progressively lower resolutions

																						// fill the data (instead of gluBuild2DMipmaps())
		glTexImage2D(
			GL_TEXTURE_2D,		// where to fill data; the target texture; following 8 para. go in this
			0,					// mipmap level(level of detail). Level 0 is the base image level
			GL_RGB,				// internal image format
			bmp.bmWidth,		// width of the texture image
			bmp.bmHeight,		// height of the texture image
			0,					// border width. Must be 0
			GL_BGR,				// external image data format
			GL_UNSIGNED_BYTE,	// the data type for data (last parameter)
			bmp.bmBits			// actual data : Pointer to the location of the bit values for the bitmap
		);	// specify a two-dimensional texture image

			// create mipmaps for this texture for better image quality
		glGenerateMipmap(GL_TEXTURE_2D);	// texture data in this now

											// unbind texture explicitly
		glBindTexture(GL_TEXTURE_2D, 0);

		// delete hBitmap
		DeleteObject(hBitmap);	// deletes a resource
	}
	return(bStatus);
}

void Resize(int width, int height)
{
	if (height == 0)
		height = 1;

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);	// 0,0: x,y->The lower-left corner of the viewport rectangle, in pixels. The default is (0,0).

	perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// ? other values of near & far not working
																												// parameters:
																												// fovy- The field of view angle, in degrees, in the y - direction.
																												// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																												// zNear- The distance from the viewer to the near clipping plane(always positive).
																												// zFar- The distance from the viewer to the far clipping plane(always positive).*/
}

void Display()
{
	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clears buffers to preset values.
														// Parameters: Bitwise OR operators of masks that indicate the buffers to be cleared

	// One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	glUseProgram(gShaderProgramObject);	// binding your OpenGL code with shader program object
										// Specifies the handle of the program object whose executables are to be used as part of current rendering state.
										
	// cube
	// 4 CPU steps
	// declaration of matrices
	vmath::mat4 modelMatrix;
	vmath::mat4 viewMatrix;
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;

	// initialize above matrices to identity (not initialised in above step for better visibility and understanding)
	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();

	// do necessary transformation (T,S,R)
	translationMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = vmath::rotate(angle_cube, angle_cube, angle_cube);

	// do necessary matrix multiplication 
	modelMatrix = translationMatrix * rotationMatrix;

	// fill and send uniforms
	// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glUniformMatrix4fv(vUniform,		// uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		viewMatrix					// actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
	);

	glUniformMatrix4fv(pUniform,		// uniform in which perspectiveProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		perspectiveProjectionMatrix		// actual matrix; this will bind to pUniform which is bound to u_p_matrix
	);

	// 3 texture lines before vao (ABU)
	// A: select active texture unit (there are usually total 80 texture units for a single geometry)
	glActiveTexture(GL_TEXTURE0);	// GL_TEXTURE0: of OGL; matches with our AMC_ATTRIBUTE_TEXCOORD0

	// B: bind a named texture to a texturing target
	glBindTexture(GL_TEXTURE_2D, texture_marble);

	// U: specify the value of a uniform variable for the current program object(sending uniform); glUniformMatrix4fv() is used to send matrices as uniforms
	glUniform1i(samplerUniform, 0);		// tell OGL that you are sending uniform as one integer and give that to FS
										// samplerUniform: Specifies the location of the uniform variable to be modified.
										// 0: Specifies the new values to be used for the specified uniform variable; 0 means unit 0

	glUniform3fv(laUniform, 1, light_ambient);	// to send vec4
	glUniform3fv(ldUniform, 1, light_diffuse);	// to send vec4
	glUniform3fv(lsUniform, 1, light_specular);	// to send vec4
	glUniform4fv(lightPositionUniform, 1, light_position);	// to send vec4; ~glLightfv(GL_LIGHT0, GL_POSITION, LightPosition) in FFP

	glUniform3fv(kaUniform, 1, material_ambient);	// gray material; to send vec3
	glUniform3fv(kdUniform, 1, material_diffuse);	// gray material; to send vec3
	glUniform3fv(ksUniform, 1, material_specular);	// gray material; to send vec3
	glUniform1f(materialShininessUniform, material_shininess);	// gray material; to send vec3

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_cube);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

										// similarly, bind with textures if any (call glBindTexture() here)

										// draw the necessary scene!
	glDrawArrays(GL_TRIANGLE_FAN,		// PP does not have GL_QUADS! ;	what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,								// array position of your array to start with (imp in inter-leaved)
		4								// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled, no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn
	glDrawArrays(GL_TRIANGLE_FAN,
		4,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		8,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		12,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		16,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		20,
		4
	);

	// unbind texture
	glBindTexture(GL_TEXTURE_2D, 0);

	// unbind vao
	glBindVertexArray(0);

	// unuse program
	glUseProgram(0);	// unbinding your OpenGL code with shader program object
						// If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.

	SwapBuffers(ghdc);	// exchanges the front and back buffers if the current pixel format for the window referenced by the specified device context includes a back buffer(double buffer)
}

void Update(void)
{
	angle_cube = angle_cube - 1.0f;
	if (angle_cube <= -360.0f)
		angle_cube = 0.0f;
}

void Uninitialize()
{
	glDeleteTextures(1, &texture_marble);

	if (vbo_pctn_cube)
	{
		glDeleteBuffers(1, &vbo_pctn_cube);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_pctn_cube = 0;
	}

	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);		// delete vertex array objects
		vao_cube = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;	// typedef int
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);		// since unused in Display()

												// ask program that how many shaders are attached to it
		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
			&shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);		// dynamic array for shaders since we don't know how many present
		if (pShaders)	// mem allocated
		{
			// take attached shaders  into above array
			glGetAttachedShaders(gShaderProgramObject,		// the program object to be queried
				shaderCount,								// the size of the array for storing the returned object names
				&shaderCount,								// Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
				pShaders									// an array that is used to return the names of attached shader objects(empty now)
			);		// return the handles of the shader objects attached to a program object

			for (shaderNumber = 0;shaderNumber < shaderCount;shaderNumber++)
			{
				// detach each shader
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
																					// 2nd para: Specifies the shader object to be detached.

																					// delete each detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		// delete the shader program
		glDeleteProgram(gShaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
		gShaderProgramObject = 0;
		glUseProgram(0);
	}

	// check if fullscreen. If yes, restore to normal size and proceed for uninitialization
	if (gbIsFullScreen == true)
	{
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowPos(ghwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);
		// SWP_NOZORDER: Retains the current Z order 
		// SWP_FRAMECHANGED: Applies new frame styles set using the SetWindowLong function
		// SWP_NOMOVE: Retains the current position
		// SWP_NOSIZE: Retains the current size
		// SWP_NOOWNERZORDER: Does not change the owner window's position in the Z order.

		ShowCursor(TRUE);
	}

	// break the current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);	// Parameters: HDC, HGLRC; If hglrc(2nd parameter) is NULL, the function makes the calling thread's current rendering context no longer current, and releases the device context that is used by the rendering context. In this case, hdc is ignored.

		if (ghrc)
		{
			wglDeleteContext(ghrc);
			ghrc = NULL;
		}
		if (ghdc)
		{
			ReleaseDC(ghwnd, ghdc);
			ghdc = NULL;
		}
		if (gpFile)
		{
			fprintf_s(gpFile, "\nLog File Closed Successfully");
			fclose(gpFile);
			gpFile = NULL;
		}
	}
}

void ToggleFullScreen(HWND hwnd)
{
	MONITORINFO mi;

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(hwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(hwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(hwnd, &wpPrev);

		SetWindowPos(hwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}
}
