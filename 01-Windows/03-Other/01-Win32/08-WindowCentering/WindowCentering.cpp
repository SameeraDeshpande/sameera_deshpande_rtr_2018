// Headers
#include<windows.h>
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variable declarations
bool gbIsFullScreen = false;
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghWnd;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");
	//int iWindowWidth = 800, iWindowHeight = 600;
	int iWindow_x, iWindow_y;

	// function declarations
	//void CenteringTheWindow(int, int, int*, int*);

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof wndclass;
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	//CenteringTheWindow(iWindowWidth,iWindowHeight,&iWindow_x,&iWindow_y);
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y= GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("My Application"),
		WS_OVERLAPPEDWINDOW,
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		HWND_DESKTOP,
		NULL,
		hInstance,
		NULL);

	ghWnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// ToggleFullScreen() declaration 
	void ToggleFullScreen(void);

	// code
	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	MONITORINFO mi;

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) }; // Setting cbSize field of MONITORINFO. Doing so lets GetMonitorInfo() determine the type of structure you are passing to it.

			if (GetWindowPlacement(ghWnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi))	// MonitorFromWindow() gives HMONITORINFO
			{
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(ghWnd,
					HWND_TOP,
					mi.rcMonitor.left,	// x;	rcMonitor is a RECT struct
					mi.rcMonitor.top,	//y
					mi.rcMonitor.right - mi.rcMonitor.left,	//width
					mi.rcMonitor.bottom - mi.rcMonitor.top,	//ht
					SWP_NOZORDER | SWP_FRAMECHANGED);	//flags
			}
		}
		ShowCursor(FALSE);
		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(ghWnd, &wpPrev);

		SetWindowPos(ghWnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);
		gbIsFullScreen = false;
	}
}

//void CenteringTheWindow(int iWindowWidth,int iWindowHeight,int *piWindow_x,int *piWindow_y)
//{
//	MONITORINFO mi;
//	TCHAR str[255];
//	int iMonitorWidth,iMonitorHeight,iHorizontalCenter,iVerticalCenter;
//	//int iWindowWidth = 800,iWindowHeight=600;
//	//int iWindow_x, iWindow_y;
//
//	mi = { sizeof(MONITORINFO) };
//
//	GetMonitorInfo(MonitorFromWindow(ghWnd, MONITORINFOF_PRIMARY), &mi);
//
//	iMonitorWidth = mi.rcMonitor.right - mi.rcMonitor.left;
//	iMonitorHeight = mi.rcMonitor.bottom - mi.rcMonitor.top;
//	iHorizontalCenter = iMonitorWidth / 2;
//	iVerticalCenter = iMonitorHeight / 2;
//	*piWindow_x = iHorizontalCenter - iWindowWidth / 2;
//	*piWindow_y = iVerticalCenter - iWindowHeight / 2;
//
//	wsprintf(str, TEXT("left space=%d\nright space==%d"), *piWindow_x - mi.rcMonitor.left, mi.rcMonitor.right - *piWindow_x - iWindowWidth);
//	MessageBox(ghWnd, str, TEXT("MSG"), MB_OK);
//}
