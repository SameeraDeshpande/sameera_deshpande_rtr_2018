// Headers
#include<windows.h>

// global variable declarations
//bool gbIsFullScreen = false;
BOOL gbIsFullScreen = FALSE;
DWORD dwStyle;
HWND ghWnd;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// variable declarations
	WNDCLASSEX wndclass;
	MSG msg;
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");

	// code
	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof wndclass;
	wndclass.style = CS_HREDRAW | CS_VREDRAW;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindow(szAppName,
		TEXT("My Application-Sameera"),
		WS_OVERLAPPEDWINDOW,
		100,
		100,
		800,
		600,
		HWND_DESKTOP,
		NULL,
		hInstance,
		NULL);

	ghWnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	UpdateWindow(hwnd);

	// message loop
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}

	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// ToggleFullScreen() declaration 
	void ToggleFullScreen(void);

	// code
	switch (iMsg)
	{
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;

		case 0x46:
			ToggleFullScreen();
			break;
		}
		break;

	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	static DEVMODE DevMode_old;
	DEVMODE DevMode_new;
	//POINTL p;
	RECT rc;

	GetClientRect(ghWnd, &rc);

	DevMode_new.dmSize = sizeof(DEVMODE);
	DevMode_old.dmSize = sizeof(DEVMODE);

	DevMode_new.dmFields = DM_PELSWIDTH | DM_PELSHEIGHT;	// | DM_POSITION;
	//DevMode_old.dmFields = DM_POSITION;
	/*p.x = 0;
	p.y = 0;*/

	if (gbIsFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghWnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

			//DevMode_new.dmPosition = p;

			//EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &DevMode_new);	// ok

			if (EnumDisplaySettings(NULL, ENUM_CURRENT_SETTINGS, &DevMode_old))
			{
				/*DevMode_old.dmPelsHeight = 600;
				DevMode_old.dmPelsWidth = 800;*/
				
				SetWindowLong(ghWnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				DevMode_new.dmPelsWidth = 800;
				DevMode_new.dmPelsHeight = 600;

				ChangeDisplaySettings(&DevMode_new, CDS_FULLSCREEN);

				ShowCursor(FALSE);
				gbIsFullScreen = TRUE;
			}
		}
	}
	else // fullscreen present
	{
		SetWindowLong(ghWnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		///*DevMode_old.dmPelsHeight = 1080;
		//DevMode_old.dmPelsWidth = 1920;*/
		/*DevMode_old.dmPosition.x = 100;
		DevMode_old.dmPosition.y = 100;*/

		//ChangeDisplaySettings(NULL, 0);	// also OK
		ChangeDisplaySettings(&DevMode_old, 0);
		
		ShowCursor(TRUE);
		gbIsFullScreen = FALSE;
	}
}
