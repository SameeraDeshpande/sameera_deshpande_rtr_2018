#include<windows.h>
#include<stdio.h>		// for file i/o
#include<d3d11.h>		// For DirectX's Direct3D (already present)
#include<d3dcompiler.h>	// shader compilation
#pragma warning(disable: 4838)	// 4838: warning no. in console
// to suppress warning of narrow typecasting(down-casting) between UINT and int (not good actually, but to avoid it, you have to not use XNAMath)
#include "XNAMath\xnamath.h"	// version 2.04; actually Math library for XBox
#pragma comment(lib,"d3d11.lib")		// corresponding to d3d11.dll (i.e. server)  (already present)
#pragma comment(lib,"D3dcompiler.lib")	// library of d3dcompiler.h

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
bool gbIsFullScreen = false;
bool gbIsActiveWindow = false;
FILE *gpFile = NULL;
char gszLogFileName[] = "SD_Log.txt";
float angle_pyramid = 0.0f;
float angle_cube = 0.0f;

float gClearColor[4];	// RGBA	// to hold color; ~glClearColor()
						// interfaces given to client by server (all interfaces are internally struct). All interfaces have 3 functions: QueryInterface(), AddRef(), Release()
IDXGISwapChain *gpIDXGISwapChain = NULL;	// for buffer swapping; DXGI= DirectX Graphics Infrastructure(the DX layer in GC) (there is also a layer for OGL)
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

ID3D11VertexShader *gpID3D11VertexShader = NULL;
ID3D11PixelShader *gpID3D11PixelShader = NULL;		// ~fragment shader
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position_Pyramid = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Color_Pyramid = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Position_Cube = NULL;
ID3D11Buffer *gpID3D11Buffer_VertexBuffer_Color_Cube = NULL;
ID3D11InputLayout *gpID3D11InputLayout = NULL;
ID3D11Buffer *gpID3D11Buffer_ConstantBuffer = NULL;

ID3D11RasterizerState *gpID3D11RasterizerState = NULL;		// to make culling off
ID3D11DepthStencilView *gpID3D11DepthStencilView = NULL;	// in DX, depth comes with stencil

struct CBUFFER	// our name; stores uniforms
{
	XMMATRIX WorldViewProjectionMatrix;		// XM= XNAMath; XMMATRIX=matrix(~vmath::mat4); WorldView ~ ModelView
};

XMMATRIX gPerspectiveProjectionMatrix;		// mapped with shader's float4x4. But that doesn't mean XMMATRIX is internally float4x4

// Entrypoint
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	HRESULT Initialize(void);
	void Display(void);
	void Update();

	// variable declarations
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");
	MSG msg;
	WNDCLASSEX wndclass;
	bool bDone = false;
	int iWindow_x, iWindow_y;

	// code
	// create log file
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created.\nExiting...\n"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully.\n");
		fclose(gpFile);
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// centering the window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Direct3D11 Window"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);	// window should be at top
	SetFocus(hwnd);	// window's title bar,etc. should be highlighted

					// initialize D3D
	HRESULT hr;
	hr = Initialize();			// since we are in COM
	if (FAILED(hr))	// macro
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Failed. Exitting Now...\n");
		fclose(gpFile);			// to avoid file becoming blank on error
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Succeeded.\n");
		fclose(gpFile);
	}

	// game loop			// same
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage()
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow == true)
			{
				Update();
			}
			Display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(HWND);
	HRESULT Resize(int, int);
	void Uninitialize(void);

	// variable declarations
	HRESULT hr;

	// code
	switch (iMsg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen(hwnd);
			break;
		}
		break;

	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;

	case  WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;

	case WM_SIZE:
		if (gpID3D11DeviceContext)	// if device context, then only go to resize
		{
			hr = Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "Resize() Failed.\n");
				fclose(gpFile);
				return(hr);	// no exit; up-casting of HRESULT TO LRESULT, hence no problem
			}
			else
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "Resize() Succeeded.\n");
				fclose(gpFile);
			}
		}
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

HRESULT Initialize()
{
	// function declarations
	HRESULT Resize(int, int);
	void Uninitialize(void);

	// variable declarations
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = {	// enum has 3 driver types; decreasing order of priority
		D3D_DRIVER_TYPE_HARDWARE,		// h/w rc (more quality)
		D3D_DRIVER_TYPE_WARP,			// Windows Accelerated Rasterization Platform
		D3D_DRIVER_TYPE_REFERENCE		// s/w rc
	};

	// ~glew in OpenGL
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;	// needed feature level=11.0 (DX11)
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;	// default,lowest; just initializing here, will be overwritten later

																			// initializing variables
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;	// can be more than 1; DX11 supports 8 FLs

								// code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);	// calculating no. of array elements

																			// ~PFD
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;		// descriptor of swap chain
												// struct (has 8 members: 2 structures(BufferDesc & SampleDesc), 2 enums(BufferUsage & DXGI_SWAP_CHAIN_FLAG(we don't use)), 4 individuals(BufferCount, OutputWindow, Windowed, DXGI_SWAP_EFFECT(we don't use)); we use 6);
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));	// zero out members of struct
	dxgiSwapChainDesc.BufferCount = 1;	// how many buffers to swap(one buffer already given by DX which is front buffer, so you ask for one more)
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;		// BufferDesc is a structure of type DXGI_MODE_DESC
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;  // want such a buffer of DXGI_FORMAT struct which is unsigned normalized
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;	// FPS
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;	// RefreshRate is struct of type DXGI_RATIONAL
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;	// want such a buffer which is a rendering target (o/p of rendering in it)
	dxgiSwapChainDesc.OutputWindow = ghwnd;		// output on which window
	dxgiSwapChainDesc.SampleDesc.Count = 1;		// single level sampling; SampleDesc is struct of type DXGI_SAMPLE_RATE
	dxgiSwapChainDesc.SampleDesc.Quality = 0;	// don't care
	dxgiSwapChainDesc.Windowed = TRUE;		// want windowed and fullscreen mode

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		// get hardware and buffers' chain
		hr = D3D11CreateDeviceAndSwapChain(		// will return 5 things
			NULL,						// which adapter; NULL=GC of primary monitor; DXGI identifies GC as adapter while D#D identifies GC as device
			d3dDriverType,				// driver type
			NULL,						// software; handle to your rasterizer library if you do s/w rendering; NULL=don't care
			createDeviceFlags,			// flags; 0=don't care
			&d3dFeatureLevel_required,	// give this FL(giving filled)
			numFeatureLevels,			// how many feature levels you have and you need
			D3D11_SDK_VERSION,			// use library of this SDK version (it's a macro(UINT))
			&dxgiSwapChainDesc,			// swap chain desc(giving filled)
			&gpIDXGISwapChain,			// swap chain(empty; ret. value)
			&gpID3D11Device,			// device(empty; ret. value); give DX's logical device
			&d3dFeatureLevel_acquired,	// feature level obtained
			&gpID3D11DeviceContext		// device context
		);	// 12 para.

		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");
		fprintf_s(gpFile, "The Chosen Driver Is Of ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type.\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type.\n");
		}

		fprintf_s(gpFile, "The Supported Highest Feature Level Is ");	// you decide how many to check(we want DX10+)
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpFile, "10.1\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown\n");
		}

		fclose(gpFile);

		// code similar to glewInit() done
	}

	// initialize shaders, input layouts, constant buffers, etc.
	// vertex shader-
	const char* vertexShaderSourceCode =
		"cbuffer ConstantBuffer" \
		"{" \
		"float4x4 worldViewProjectionMatrix;" \
		"}" \
		"struct vertex_output" \
		"{" \
		"float4 position : SV_POSITION;" \
		"float4 color : COLOR;" \
		"};" \
		"vertex_output main(float4 pos : POSITION, float4 color : COLOR)" \
		"{" \
		"vertex_output output;" \
		"output.position = mul(worldViewProjectionMatrix,pos);" \
		"output.color = color;" \
		"return(output);" \
		"}";	// written in High Level Shading/Shader Language (HLSL)
				// cbuffer: keyword; ConstantBuffer: our name; maps with CBUFFER in main program; gives uniforms to shader
				// float4x4 mapped with XMMATRIX in main program
				// you cannot have multiple 'out' variables like OGL in DX shaders, hence you create a struct to output them
				// position: bound to SV_POSITION (semantic name); will act as output only
				// SV_POSITION: Shader Variable; HLSL's, not ours; shader symantic; tells that position is returned
				// color: bound to COLOR (semantic name); will act as output only
				// vertex_output main: ret. type of main() is vertex_output
				// main(): EPF; can be any name!
				// pos : bound to POSITION; will act as input only (no need to write SV_POSITION here as it is already written in struct)
				// color : bound to COLOR; will act as input only
				// float4 pos: ~in; pos: our name
				// POSITION: shader symantic; parameter to main; our name; maps with AMC_ATTRIBUTE_POSITION
				// return(position): ~out

				// compile the vertex shader
	ID3DBlob *pID3DBlob_VertexShaderSourceCode = NULL;		// blob= binary large object(means data does not have any specific format)
	ID3DBlob *pID3DBlob_Error = NULL;

	hr = D3DCompile(
		vertexShaderSourceCode,					// buffer of VS source
		lstrlenA(vertexShaderSourceCode) + 1,	// length of 1st para; +1 for '\0'; long or literal version of strlen; A=ANSI(std) ASCII(code)
		"VS",									// code name; you can use this parameter for strings that specify error messages
		NULL,									// an array of NULL-terminated macro definitions (use if your shader has #define's)
		D3D_COMPILE_STANDARD_FILE_INCLUDE,		// use if your shader has #include's. D3D_COMPILE_STANDARD_FILE_INCLUDE: pointer to a default include handler
		"main",									// string; EPF name
		"vs_5_0",								// shader version/feature level
		0,										// how should the HLSL compiler compile your shader; 0= don't care (hence it takes ;debug' but does not actually debug since you didn't write'debug'
		0,										// no. of fx constants; 0= no effects (no seperate .fx file in which shader is written); In DX, all shaders are effects (fx)
		&pID3DBlob_VertexShaderSourceCode,		// give shader's compiled code in this; empty
		&pID3DBlob_Error						// give errors in this, if any
	);	// compiler: fxc.exe
	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)	// error present
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() Failed For Vertex Shader : %s\n", (char*)pID3DBlob_Error->GetBufferPointer());	// retrieves a pointer to the data in the buffer; returns void* string
			fclose(gpFile);
			pID3DBlob_Error->Release();		// not needed now; don't release pID3DBlob_VertexShaderSourceCode now since it is needed in input layout code
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() Succeeded For Vertex Shader.\n");
		fclose(gpFile);
	}

	// create the vertex shader
	hr = gpID3D11Device->CreateVertexShader(		// usually, 'create' functions are called on device
		pID3DBlob_VertexShaderSourceCode->GetBufferPointer(),	// pointer to the buffer of compiled shader source
		pID3DBlob_VertexShaderSourceCode->GetBufferSize(),		// size of the buffer
		NULL,													// class linkage parameter (if you have variables to be shared across shaders)
		&gpID3D11VertexShader									// give final vertex shader in this (empty)
	);	// ~glCreateShader()
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateVertexShader() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateVertexShader() Succeeded.\n");
		fclose(gpFile);
	}

	// tell pipeline to set vertex shader (in OGL, shader program object used to do this; here you do it directly)
	gpID3D11DeviceContext->VSSetShader(gpID3D11VertexShader,	// whom to set
		0,	// ID3D11ClassLinkage* or ID3D11ClassInstance* (array of sharable parameters); actually it should be NULL, but #define NULL is 0
		0	// count of array
	);

	// pixel shader-			~fragment shader
	const char* pixelShaderSourceCode =
		"float4 main(float4 pos : SV_POSITION, float4 color : COLOR) : SV_TARGET" \
		"{" \
		"return(color);" \
		"}";	// written in High Level Shading/Shader Language (HLSL)
				// float4 main: ret.type of main() is float4
				// SV_POSITION, COLOR: taken seperately from struct returned by VS (don't pick your para. from the struct but from the bound attributes of the struct)
				// main: EPF; can be any name!
				// void: ~in; no i/p
				// SV_TARGET: Shader Variable; HLSL's, not ours; shader symantic; tells that fragColor is returned; SV_TARGET implies color
				// return(color): ~out;

				// compile the pixel shader
	ID3DBlob *pID3DBlob_PixelShaderSourceCode = NULL;		// blob= binary large object(means data does not have any specific format)
	pID3DBlob_Error = NULL;

	hr = D3DCompile(
		pixelShaderSourceCode,					// buffer of PS source
		lstrlenA(pixelShaderSourceCode) + 1,	// length of 1st para; +1 for '\0'; long or literal version of strlen; A=ANSI(std) ASCII(code)
		"PS",									// code name; you can use this parameter for strings that specify error messages
		NULL,									// an array of NULL-terminated macro definitions (use if your shader has #define's)
		D3D_COMPILE_STANDARD_FILE_INCLUDE,		// use if your shader has #include's. D3D_COMPILE_STANDARD_FILE_INCLUDE: pointer to a default include handler
		"main",									// string; EPF name
		"ps_5_0",								// shader version/feature level
		0,										// how should the HLSL compiler compile your shader; 0= don't care (hence it takes ;debug' but does not actually debug since you didn't write'debug'
		0,										// no. of fx constants; 0= no effects (no seperate .fx file in which shader is written); In DX, all shaders are effects (fx)
		&pID3DBlob_PixelShaderSourceCode,		// give shader's compiled code in this; empty
		&pID3DBlob_Error						// give errors in this, if any
	);	// compiler: fxc.exe
	if (FAILED(hr))
	{
		if (pID3DBlob_Error != NULL)	// error present
		{
			fopen_s(&gpFile, gszLogFileName, "a+");
			fprintf_s(gpFile, "D3DCompile() Failed For Pixel Shader : %s\n", (char*)pID3DBlob_Error->GetBufferPointer());	// retrieves a pointer to the data in the buffer; returns void* string
			fclose(gpFile);
			pID3DBlob_Error->Release();		// not needed now; don't release pID3DBlob_VertexShaderSourceCode now since it is needed in input layout code
			pID3DBlob_Error = NULL;
			return(hr);
		}
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3DCompile() Succeeded For Pixel Shader.\n");
		fclose(gpFile);
	}

	// create the pixel shader
	gpID3D11Device->CreatePixelShader(		// usually, 'create' functions are called on device
		pID3DBlob_PixelShaderSourceCode->GetBufferPointer(),	// pointer to the buffer of compiled shader source
		pID3DBlob_PixelShaderSourceCode->GetBufferSize(),		// size of the buffer
		NULL,													// class linkage parameter (if you have variables to be shared across shaders)
		&gpID3D11PixelShader									// give final vertex sghader in this (empty)
	);	// ~glCreateShader()
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreatePixelShader() Succeeded.\n");
		fclose(gpFile);
	}

	// tell pipeline to set vertex shader (in OGL, shader program object used to do this; here you do it directly)
	gpID3D11DeviceContext->PSSetShader(gpID3D11PixelShader,	// whom to set
		NULL,	// ID3D11ClassLinkage* or ID3D11ClassInstance* (array of sharable parameters); actually it should be NULL, but #define NULL is 0
		NULL	// count of array
	);

	// create and set input layout
	D3D11_INPUT_ELEMENT_DESC inputElementDesc[2];
	ZeroMemory((void *)&inputElementDesc, sizeof(D3D11_INPUT_ELEMENT_DESC));	// zero out members of struct

	inputElementDesc[0].SemanticName = "POSITION";						// what you wrote in VS; ~vPosition
	inputElementDesc[0].SemanticIndex = 0;								// attach position at index 0 (we decide)
	inputElementDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;			// ~vec3; means 3 components(x,y,z)
	inputElementDesc[0].InputSlot = 0;									// ~layout in OGL or AMC_ATTRIBUTE_POSITION, where are P,C,N,T kept 
	inputElementDesc[0].AlignedByteOffset = 0;							// gap between sets of P,C,N,T; packed data, hence no gap
	inputElementDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;	// class of input slot (since P,C,N,T are per vertex always)
	inputElementDesc[0].InstanceDataStepRate = 0;						// no since we have per vertex and not per instance

	inputElementDesc[1].SemanticName = "COLOR";							// what you wrote in VS; ~vColor
	inputElementDesc[1].SemanticIndex = 0;								// attach color at index 0 (we decide)
	inputElementDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;			// ~vec3; means 3 components(x,y,z)
	inputElementDesc[1].InputSlot = 1;									// ~layout in OGL or AMC_ATTRIBUTE_COLOR, where are P,C,N,T kept
	inputElementDesc[1].AlignedByteOffset = 0;							// gap between sets of P,C,N,T; packed data, hence no gap
	inputElementDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;	// class of input slot (since P,C,N,T are per vertex always)
	inputElementDesc[1].InstanceDataStepRate = 0;						// no since we have per vertex and not per instance

	hr = gpID3D11Device->CreateInputLayout(
		inputElementDesc,										// address of array of input layout, but we have only 1 var., hence &
		_ARRAYSIZE(inputElementDesc),							// or can write 2; size of array
		pID3DBlob_VertexShaderSourceCode->GetBufferPointer(),	// where to send input layout; ~glBindAttribLocation()
		pID3DBlob_VertexShaderSourceCode->GetBufferSize(),		// size of the shader
		&gpID3D11InputLayout									// empty
	);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateInputLayout() Succeeded.\n");
		fclose(gpFile);
	}

	// set input layout
	gpID3D11DeviceContext->IASetInputLayout(gpID3D11InputLayout);	// IA= Input Assembler

																	// release compiled shader sources
	pID3DBlob_VertexShaderSourceCode->Release();
	pID3DBlob_VertexShaderSourceCode = NULL;
	pID3DBlob_PixelShaderSourceCode->Release();
	pID3DBlob_PixelShaderSourceCode = NULL;

	// fill triangle vertices in array
	float vertices_pyramid[] = {
		// front
		0.0f,1.0f,0.0f,		// apex
		-1.0f,-1.0f,1.0f,	// front left
		1.0f,-1.0f,1.0f,	// front right
		// right
		0.0f,1.0f,0.0f,
		1.0f,-1.0f,1.0f,
		1.0f,-1.0f,-1.0f,
		// back
		0.0f,1.0f,0.0f,
		1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		// left
		0.0f,1.0f,0.0f,
		-1.0f,-1.0f,-1.0f,
		-1.0f,-1.0f,1.0f,
	};	// last ',' OK!; const since array of values
								// since written in Initialize() only, coords will not get set everytime in Display(). Hence, fast speed.

	float color_pyramid[] = {
		// front
		1.0f,0.0f,0.0f,		// R
		0.0f,1.0f,0.0f,		// G; winding order clockwise in DX
		0.0f,0.0f,1.0f,		// B
		// right
		1.0f,0.0f,0.0f,	
		0.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,
		// back
		1.0f,0.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,0.0f,1.0f,
		// left
		1.0f,0.0f,0.0f,
		0.0f,0.0f,1.0f,
		0.0f,1.0f,0.0f,
	};

	float vertices_cube[] = {
		// top
		// triangle 1
		-1.0f,1.0f,1.0f,
		1.0f,1.0f,1.0f,
		-1.0f,1.0f,-1.0f,
		// triangle 2
		-1.0f,1.0f,-1.0f,
		1.0f,1.0f,1.0f,
		1.0f,1.0f,-1.0f,

		// bottom
		// triangle 1
		1.0f,-1.0f,-1.0f,
		1.0f,-1.0f,1.0f,
		-1.0f,-1.0f,-1.0f,
		// triangle 2
		-1.0f,-1.0f,-1.0f,
		1.0f,-1.0f,1.0f,
		-1.0f,-1.0f,1.0f,

		// front
		// triangle 1
		-1.0f,1.0f,-1.0f,
		1.0f,1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,
		// triangle 2
		-1.0f,-1.0f,-1.0f,
		1.0f,1.0f,-1.0f,
		1.0f,-1.0f,-1.0f,

		// back
		// triangle 1
		1.0f,-1.0f,1.0f,
		1.0f,1.0f,1.0f,
		-1.0f,-1.0f,1.0f,
		// triangle 2
		-1.0f,-1.0f,1.0f,
		1.0f,1.0f,1.0f,
		-1.0f,1.0f,1.0f,

		// left
		// triangle 1
		-1.0f,1.0f,1.0f,
		-1.0f,1.0f,-1.0f,
		-1.0f,-1.0f,1.0f,
		// triangle 2
		-1.0f,-1.0f,1.0f,
		-1.0f,1.0f,-1.0f,
		-1.0f,-1.0f,-1.0f,

		// right
		// triangle 1
		1.0f,-1.0f,-1.0f,
		1.0f,1.0f,-1.0f,
		1.0f,-1.0f,1.0f,
		// triangle 2
		1.0f,-1.0f,1.0f,
		1.0f,1.0f,-1.0f,
		1.0f,1.0f,1.0f,
	};	

	float color_cube[] = {
		// top (red)
		// triangle 1
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f,
		// triangle 2
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f,
		1.0f,0.0f,0.0f,

		// bottom (green)
		// triangle 1
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,
		// triangle 2
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,
		0.0f,1.0f,0.0f,

		// front (blue)
		// triangle 1
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		// triangle 2
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,
		0.0f,0.0f,1.0f,

		// back (cyan)
		// triangle 1
		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f,
		// triangle 2
		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f,
		0.0f,1.0f,1.0f,

		// left (magenta)
		// triangle 1
		1.0f,0.0f,1.0f,
		1.0f,0.0f,1.0f,
		1.0f,0.0f,1.0f,
		// triangle 2
		1.0f,0.0f,1.0f,
		1.0f,0.0f,1.0f,
		1.0f,0.0f,1.0f,

		// right (yellow)
		// triangle 1
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,
		// triangle 2
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,
		1.0f,1.0f,0.0f,
	};

	// pyramid
	// pyramid position
	// create vertex buffer(for GPU)
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Position_Pyramid;
	ZeroMemory((void *)&bufferDesc_VertexBuffer_Position_Pyramid, sizeof(D3D11_BUFFER_DESC));	// zero out members of struct

	bufferDesc_VertexBuffer_Position_Pyramid.Usage = D3D11_USAGE_DYNAMIC;	// how is our buffer
	bufferDesc_VertexBuffer_Position_Pyramid.ByteWidth = sizeof(float)*_ARRAYSIZE(vertices_pyramid);	// or ARRAYSIZE; macro; byte size of buffer
	bufferDesc_VertexBuffer_Position_Pyramid.BindFlags = D3D11_BIND_VERTEX_BUFFER;	// bind with vertex buffer; ~glBindBuffer() or 1st para. of glVertexAttribPointer()
	bufferDesc_VertexBuffer_Position_Pyramid.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;	// give write access to CPU so that it can give data to GPU

	hr = gpID3D11Device->CreateBuffer(
		&bufferDesc_VertexBuffer_Position_Pyramid,
		NULL,									// dynamic draw
		&gpID3D11Buffer_VertexBuffer_Position_Pyramid	// empty
	);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Vertex Buffer For Position For Pyramid.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Vertex Buffer Position For Pyramid.\n");
		fclose(gpFile);
	}

	// set gpID3D11Buffer_VertexBuffer_Position in Display() dynamically

	// copy vertices into above buffer
	D3D11_MAPPED_SUBRESOURCE mappedSubResource;	// on CPU
	ZeroMemory((void *)&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));	// zero out members of struct

	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Position_Pyramid,	// map to this buffer on GPU
		0,													// byte offset to fill data in buffer
		D3D11_MAP_WRITE_DISCARD,							// what to do with buffer- write and then discard
		0,													// what must CPU do while/after mapping; 0=don't care; depends on is multithreading present or not(not in our hands)
		&mappedSubResource									// map with this on CPU
	);	// mapping between mappedSubResource and gpID3D11Buffer_VertexBuffer

		// give data to gpID3D11Buffer_VertexBuffer
	memcpy(mappedSubResource.pData, vertices_pyramid, sizeof(vertices_pyramid));	// give vertices to mappedSubResource.pData, it will give to gpID3D11Buffer_VertexBuffer

																					// remove lock on Map()
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Position_Pyramid, 0);	// first two parameters of Map()

	// pyramid color
	// create vertex buffer(for GPU)
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Color_Pyramid;		// can use same buff desc for position and color too (just zero out memory before using)
	ZeroMemory((void *)&bufferDesc_VertexBuffer_Color_Pyramid, sizeof(D3D11_BUFFER_DESC));	// zero out members of struct

	bufferDesc_VertexBuffer_Color_Pyramid.Usage = D3D11_USAGE_DYNAMIC;	// how is our buffer
	bufferDesc_VertexBuffer_Color_Pyramid.ByteWidth = sizeof(float)*_ARRAYSIZE(color_pyramid);	// or ARRAYSIZE; macro; byte size of buffer
	bufferDesc_VertexBuffer_Color_Pyramid.BindFlags = D3D11_BIND_VERTEX_BUFFER;	// bind with vertex buffer; ~glBindBuffer() or 1st para. of glVertexAttribPointer()
	bufferDesc_VertexBuffer_Color_Pyramid.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;	// give write access to CPU so that it can give data to GPU

	hr = gpID3D11Device->CreateBuffer(
		&bufferDesc_VertexBuffer_Color_Pyramid,
		NULL,									// dynamic draw
		&gpID3D11Buffer_VertexBuffer_Color_Pyramid	// empty
	);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Vertex Buffer For Color For Pyramid.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Vertex Buffer For Color For Pyramid.\n");
		fclose(gpFile);
	}

	// set gpID3D11Buffer_VertexBuffer_Color in Display() dynamically

	// copy vertices into above buffer
	ZeroMemory((void *)&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));	// zero out members of struct

	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Color_Pyramid,	// map to this buffer on GPU
		0,													// byte offset to fill data in buffer
		D3D11_MAP_WRITE_DISCARD,							// what to do with buffer- write and then discard
		0,													// what must CPU do while/after mapping; 0=don't care; depends on is multithreading present or not(not in our hands)
		&mappedSubResource									// map with this on CPU
	);	// mapping between mappedSubResource and gpID3D11Buffer_VertexBuffer

	// give data to gpID3D11Buffer_VertexBuffer
	memcpy(mappedSubResource.pData, color_pyramid, sizeof(color_pyramid));	// give vertices to mappedSubResource.pData, it will give to gpID3D11Buffer_VertexBuffer

	// remove lock on Map()
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Color_Pyramid, 0);	// first two parameters of Map()

	// cube
	// cube position
	// create vertex buffer(for GPU)
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Position_Cube;
	ZeroMemory((void *)&bufferDesc_VertexBuffer_Position_Cube, sizeof(D3D11_BUFFER_DESC));	// zero out members of struct

	bufferDesc_VertexBuffer_Position_Cube.Usage = D3D11_USAGE_DYNAMIC;	// how is our buffer
	bufferDesc_VertexBuffer_Position_Cube.ByteWidth = sizeof(float)*_ARRAYSIZE(vertices_cube);	// or ARRAYSIZE; macro; byte size of buffer
	bufferDesc_VertexBuffer_Position_Cube.BindFlags = D3D11_BIND_VERTEX_BUFFER;	// bind with vertex buffer; ~glBindBuffer() or 1st para. of glVertexAttribPointer()
	bufferDesc_VertexBuffer_Position_Cube.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;	// give write access to CPU so that it can give data to GPU

	hr = gpID3D11Device->CreateBuffer(
		&bufferDesc_VertexBuffer_Position_Cube,
		NULL,									// dynamic draw
		&gpID3D11Buffer_VertexBuffer_Position_Cube	// empty
	);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Vertex Buffer For Position For Cube.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Vertex Buffer For Position For Cube.\n");
		fclose(gpFile);
	}

	// set gpID3D11Buffer_VertexBuffer_Position in Display() dynamically

	// copy vertices into above buffer
	ZeroMemory((void *)&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));	// zero out members of struct

	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Position_Cube,	// map to this buffer on GPU
		0,													// byte offset to fill data in buffer
		D3D11_MAP_WRITE_DISCARD,							// what to do with buffer- write and then discard
		0,													// what must CPU do while/after mapping; 0=don't care; depends on is multithreading present or not(not in our hands)
		&mappedSubResource									// map with this on CPU
	);	// mapping between mappedSubResource and gpID3D11Buffer_VertexBuffer

		// give data to gpID3D11Buffer_VertexBuffer
	memcpy(mappedSubResource.pData, vertices_cube, sizeof(vertices_cube));	// give vertices to mappedSubResource.pData, it will give to gpID3D11Buffer_VertexBuffer

																						// remove lock on Map()
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Position_Cube, 0);	// first two parameters of Map()

																						// rectangle color
																						// create vertex buffer(for GPU)
	D3D11_BUFFER_DESC bufferDesc_VertexBuffer_Color_Cube;		// can use same buff desc for position and color too (just zero out memory before using)
	ZeroMemory((void *)&bufferDesc_VertexBuffer_Color_Cube, sizeof(D3D11_BUFFER_DESC));	// zero out members of struct

	bufferDesc_VertexBuffer_Color_Cube.Usage = D3D11_USAGE_DYNAMIC;	// how is our buffer
	bufferDesc_VertexBuffer_Color_Cube.ByteWidth = sizeof(float)*_ARRAYSIZE(color_cube);	// or ARRAYSIZE; macro; byte size of buffer
	bufferDesc_VertexBuffer_Color_Cube.BindFlags = D3D11_BIND_VERTEX_BUFFER;	// bind with vertex buffer; ~glBindBuffer() or 1st para. of glVertexAttribPointer()
	bufferDesc_VertexBuffer_Color_Cube.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;	// give write access to CPU so that it can give data to GPU

	hr = gpID3D11Device->CreateBuffer(
		&bufferDesc_VertexBuffer_Color_Cube,
		NULL,									// dynamic draw
		&gpID3D11Buffer_VertexBuffer_Color_Cube	// empty
	);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Vertex Buffer For Color For Cube.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Vertex Buffer For Color For Cube.\n");
		fclose(gpFile);
	}

	// set gpID3D11Buffer_VertexBuffer_Color in Display() dynamically

	// copy vertices into above buffer
	ZeroMemory((void *)&mappedSubResource, sizeof(D3D11_MAPPED_SUBRESOURCE));	// zero out members of struct

	gpID3D11DeviceContext->Map(gpID3D11Buffer_VertexBuffer_Color_Cube,	// map to this buffer on GPU
		0,													// byte offset to fill data in buffer
		D3D11_MAP_WRITE_DISCARD,							// what to do with buffer- write and then discard
		0,													// what must CPU do while/after mapping; 0=don't care; depends on is multithreading present or not(not in our hands)
		&mappedSubResource									// map with this on CPU
	);	// mapping between mappedSubResource and gpID3D11Buffer_VertexBuffer

		// give data to gpID3D11Buffer_VertexBuffer
	memcpy(mappedSubResource.pData, color_cube, sizeof(color_cube));	// give vertices to mappedSubResource.pData, it will give to gpID3D11Buffer_VertexBuffer

																				// remove lock on Map()
	gpID3D11DeviceContext->Unmap(gpID3D11Buffer_VertexBuffer_Color_Cube, 0);	// first two parameters of Map()

	// define and set the constant buffer to hold uniforms	~glGetUniformLocation()
	D3D11_BUFFER_DESC bufferDesc_ConstantBuffer;
	ZeroMemory(&bufferDesc_ConstantBuffer, sizeof(D3D11_BUFFER_DESC));	// zero out members of struct

	bufferDesc_ConstantBuffer.Usage = D3D11_USAGE_DEFAULT;	// decided at runtime
	bufferDesc_ConstantBuffer.ByteWidth = sizeof(CBUFFER);
	bufferDesc_ConstantBuffer.BindFlags = D3D11_BIND_CONSTANT_BUFFER;	// bind with constant buffer;

	hr = gpID3D11Device->CreateBuffer(
		&bufferDesc_ConstantBuffer,
		0,							// dynamic
		&gpID3D11Buffer_ConstantBuffer	// empty
	);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Failed For Constant Buffer.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateBuffer() Succeeded For Constant Buffer.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->VSSetConstantBuffers(	// since cbuffer is in VS here
		0,								// slot in shader
		1,								// no. of constant buffers we are sending
		&gpID3D11Buffer_ConstantBuffer	// whom are we sending
	);

	// vao, vbo logic ended here

	// set rasterization state
	// in D3D, is by default on, means backface of geometry will not be visible.
	// this causes out geometry's backface to vanish on rotation
	// so make culling off
	D3D11_RASTERIZER_DESC rasterizerDesc;
	ZeroMemory((void*)&rasterizerDesc, sizeof(D3D11_RASTERIZER_DESC));	// zero out members of struct

	rasterizerDesc.AntialiasedLineEnable = FALSE;	// we don't want to do line drawing
	rasterizerDesc.CullMode = D3D11_CULL_NONE;		// do not cull; this allow both faces of geometry to be visible on screen
	rasterizerDesc.DepthBias = 0;					// to seperate depth of two objects (especially when you want them at the same depth e.g.: wall and its shadow); our geometry is 2D, hence no question of shadow
	rasterizerDesc.DepthBiasClamp = 0.0f;			// since no depth bias
	rasterizerDesc.DepthClipEnable = TRUE;			// clip if gone outside range
	rasterizerDesc.FillMode = D3D11_FILL_SOLID;		// default; no wireframe
	rasterizerDesc.FrontCounterClockwise = FALSE;	// we have backface and clockwise; by default, DX gives back buffer first (?)
	rasterizerDesc.MultisampleEnable = FALSE;		// since less speed
	rasterizerDesc.ScissorEnable = FALSE;			// one of the 8 tests
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;		// sice no depth bias

	hr = gpID3D11Device->CreateRasterizerState(
		&rasterizerDesc,
		&gpID3D11RasterizerState	// empty
	);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRasterizerState() Failed For Culling.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRasterizerState() Succeeded For Culling.\n");
		fclose(gpFile);
	}

	gpID3D11DeviceContext->RSSetState(gpID3D11RasterizerState);	// whom are we sending

	// d3d clear color
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 0.0;
	gClearColor[3] = 1.0f;

	// set projection matrix to identity matrix
	gPerspectiveProjectionMatrix = XMMatrixIdentity();

	// *** warmup call to resize ***		// imp. to get depth-stencil view
	hr = Resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Succeeded.\n");
		fclose(gpFile);
	}

	return(S_OK);	// S_OK: operation successful
}

HRESULT Resize(int width, int height)
{
	// code
	HRESULT hr = S_OK;

	// free any size-dependent resources since size is changed
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain buffers accordingly
	gpIDXGISwapChain->ResizeBuffers(
		1,							// how many buffers to resize
		width,						// width of buffer
		height, 					// height of buffer
		DXGI_FORMAT_R8G8B8A8_UNORM,	// format of dxgiSwapChainDesc.BufferDesc
		0							// swap chain flags
	);

	// again get the back buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;	// reasons to take texture buffer (as it is we want some):
													// 1. Texture memory is supposed to be the fastest memory in GPU
													// 2. Texture memory is supposed to be the fastest accessible memory in GPU

	gpIDXGISwapChain->GetBuffer(
		0,										// give for 0th index (since back render always gets rendered first, then front buffer)
		__uuidof(ID3D11Texture2D),				// interfaces are known by GUID in COM
		(LPVOID*)&pID3D11Texture2D_BackBuffer		// long pointer to void*; give buffer in this(empty)
	);

	// again get render target view from d3d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(
		pID3D11Texture2D_BackBuffer,		// for this buffer
		NULL,							// D3D11_RENDER_TARGET_VIEW_DESC
		&gpID3D11RenderTargetView		// give in this
	);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();		// not needed now
	pID3D11Texture2D_BackBuffer = NULL;

	// create depth stencil buffer (or zbuffer) (DSV needs to be changed in every resize)
	D3D11_TEXTURE2D_DESC textureDesc;			// swap chain gives you color buffer, but we want depth buffer. Hence take texture buffer and black it out
	ZeroMemory((void*)&textureDesc, sizeof(D3D11_TEXTURE2D_DESC));	// zero out members of struct

	textureDesc.Width = (UINT)width;
	textureDesc.Height = (UINT)height;
	textureDesc.ArraySize = 1;							// one 2D image
	textureDesc.MipLevels = 1;							// mipmap
	textureDesc.SampleDesc.Count = 1;					// in real world, this can be upto 4
	textureDesc.SampleDesc.Quality = 0;					// if aboev is 4, it is 1
	textureDesc.Format = DXGI_FORMAT_D32_FLOAT;			// 32-bit depth (24 bit in OGL)
	textureDesc.Usage = D3D11_USAGE_DEFAULT;			// how is usage
	textureDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;	// depth-stencil
	textureDesc.CPUAccessFlags = 0;						// CPU access flags
	textureDesc.MiscFlags = 0;							// miscellaneous flags

	ID3D11Texture2D *pID3D11Texture2D_DepthBuffer;
	gpID3D11Device->CreateTexture2D(&textureDesc,
		NULL,										// sub-resource data; means do map and unmap further
		&pID3D11Texture2D_DepthBuffer);		// actually create depth buffer

	// create depth stencil view from above depth stencil buffer
	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory((void*)&depthStencilViewDesc, sizeof(D3D11_DEPTH_STENCIL_VIEW_DESC));	// zero out members of struct

	depthStencilViewDesc.Format = DXGI_FORMAT_D32_FLOAT;					// same as above
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;	// MS=Multi Sampled DSV

	hr = gpID3D11Device->CreateDepthStencilView(
		pID3D11Texture2D_DepthBuffer,		// for this buffer
		&depthStencilViewDesc,							// D3D11_TEXTURE2D_DESC
		&gpID3D11DepthStencilView		// give in this
	);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateDepthStencilView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_DepthBuffer->Release();		// not needed now
	pID3D11Texture2D_DepthBuffer = NULL;

	// set render target view as render target(in pipeline; DC controls pipeline)
	gpID3D11DeviceContext->OMSetRenderTargets(	// OM= Output Merger
		1,							// only one RT currently; RTs are 2 minimum: depth stencil view and render target view
		&gpID3D11RenderTargetView,	// RTV
		gpID3D11DepthStencilView	// ID3D11DepthStencilView*
	);

	// set viewport
	D3D11_VIEWPORT d3dViewPort;		// 6 members
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;	// new ~depth test in OGL
	d3dViewPort.MaxDepth = 1.0f;	// new ~depth test in OGL
	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);		// RS= Rasterizer; 1= 1 viewport(can be multiple, like in 24 spheres)

																// set orthographic matrix
	if (height == 0)
		height = 1;

	gPerspectiveProjectionMatrix = XMMatrixPerspectiveFovLH(XMConvertToRadians(45), (float)width / (float)height, 0.1f, 100.0f);	// in OGL, OGL used to convert into radians
																																	// parameters:
																																	// fovy- The field of view angle, in degrees, in the y - direction.
																																	// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																																	// zNear- The distance from the viewer to the near clipping plane(always positive).
																																	// zFar- The distance from the viewer to the far clipping plane(always positive).

	return(hr);
}

void Display()
{
	// code
	// clear render target view to a chosen color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);	// 1st para: whom to clear; 2nd para: what color; ~glClearColor()

	// clear depth-stencil view to 1.0
	gpID3D11DeviceContext->ClearDepthStencilView(gpID3D11DepthStencilView,
		D3D11_CLEAR_DEPTH,		// enum (its second member is STENCIL)
		1.0,					// LEQUAL 1.0
		0						// no stencil
	);

	// 5 steps of Display()
	// select which vertex buffer to display
	// pyramid
	// pyramid position
	UINT stride = sizeof(float) * 3;		// since format is R32G32B32
	UINT offset = 0;						// for interleaved

	gpID3D11DeviceContext->IASetVertexBuffers(
		0,										// imp: input slot for binding (since 0 in input layout slot)
		1,										// how many buffers
		&gpID3D11Buffer_VertexBuffer_Position_Pyramid,	// what to set (on GPU); pointer to the array of vertex buffers
		&stride,								// pointer to an array of stride values, one stride value for each buffer in the vertex-buffer array
		&offset									// pointer to an array of offset values, one offset value for each buffer in the vertex-buffer array
	);	// ~glVertexAttribPointer() and glEnableVertexAttribArray()

	// pyramid color
	stride = sizeof(float) * 3;		// since format is R32G32B32
	offset = 0;						// for interleaved

	gpID3D11DeviceContext->IASetVertexBuffers(
		1,										// imp: input slot for binding (since 1 in input layout slot)
		1,										// how many buffers
		&gpID3D11Buffer_VertexBuffer_Color_Pyramid,		// what to set (on GPU); pointer to the array of vertex buffers
		&stride,								// pointer to an array of stride values, one stride value for each buffer in the vertex-buffer array
		&offset									// pointer to an array of offset values, one offset value for each buffer in the vertex-buffer array
	);	// ~glVertexAttribPointer() and glEnableVertexAttribArray()

	// declaration of matrices
	XMMATRIX worldMatrix = XMMatrixIdentity();
	XMMATRIX viewMatrix = XMMatrixIdentity();

	// do the necessary transformation
	XMMATRIX translationMatrix = XMMatrixTranslation(-1.5f, 0.0f, 6.0f);	// In DX, z is positive inward
	XMMATRIX rotationMatrix = XMMatrixRotationY(-angle_pyramid);		// like in OGL, - for left to write

	worldMatrix = rotationMatrix * translationMatrix;		// order is very very important
	XMMATRIX wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

	// push data in uniforms i.e. load data in constant buffer ~glUniformMatrix4fv()
	CBUFFER constantBuffer;		// declaring variable of our struct; on CPU
	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;

	gpID3D11DeviceContext->UpdateSubresource(	// we have one resource: CBUFFER
		gpID3D11Buffer_ConstantBuffer,		// pass to this constant buffer
		0,									// slot in shader
		NULL,								// D3D11_BOX (portion of the destination subresource to copy the resource data into)
		&constantBuffer,					// what to send
		0,									// byte width of one row (when you have box for 3rd para.); not needed by us
		0									// byte width of one height or depth (when you have box for 3rd para.); not needed by us
	);	// will Map(), memcpy() and Unmap() for constant buffer

		// select geometry primitive to draw (during IA stage)
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);	// triangle list but ours is only one triangle

																							// draw vertex buffer to render target
	gpID3D11DeviceContext->Draw(
		12,		// // how many vertices to draw
		0		// array position of your 9-member array to start with
	);

	// cube
	// cube position
	stride = sizeof(float) * 3;		// since format is R32G32B32
	offset = 0;						// for interleaved

	gpID3D11DeviceContext->IASetVertexBuffers(
		0,										// imp: input slot for binding (since 0 in input layout slot)
		1,										// how many buffers
		&gpID3D11Buffer_VertexBuffer_Position_Cube,	// what to set (on GPU); pointer to the array of vertex buffers
		&stride,								// pointer to an array of stride values, one stride value for each buffer in the vertex-buffer array
		&offset									// pointer to an array of offset values, one offset value for each buffer in the vertex-buffer array
	);	// ~glVertexAttribPointer() and glEnableVertexAttribArray()

	// cube color
	stride = sizeof(float) * 3;		// since format is R32G32B32
	offset = 0;						// for interleaved

	gpID3D11DeviceContext->IASetVertexBuffers(
		1,										// imp: input slot for binding (since 1 in input layout slot)
		1,										// how many buffers
		&gpID3D11Buffer_VertexBuffer_Color_Cube,		// what to set (on GPU); pointer to the array of vertex buffers
		&stride,								// pointer to an array of stride values, one stride value for each buffer in the vertex-buffer array
		&offset									// pointer to an array of offset values, one offset value for each buffer in the vertex-buffer array
	);	// ~glVertexAttribPointer() and glEnableVertexAttribArray()

		// initialize above matrices to identity
	worldMatrix = XMMatrixIdentity();
	viewMatrix = XMMatrixIdentity();

	// do the necessary transformation
	translationMatrix = XMMatrixTranslation(1.5f, 0.0f, 6.0f);	// In DX, z is positive inward
	XMMATRIX rotationMatrix1 = XMMatrixRotationX(angle_cube);
	XMMATRIX rotationMatrix2 = XMMatrixRotationY(angle_cube);
	XMMATRIX rotationMatrix3 = XMMatrixRotationZ(angle_cube);
	rotationMatrix = rotationMatrix1 * rotationMatrix2 * rotationMatrix3;

	XMMATRIX scaleMatrix = XMMatrixScaling(0.75f, 0.75f, 0.75f);

	worldMatrix = scaleMatrix * rotationMatrix * translationMatrix;		// order is very very important (SRT)
	wvpMatrix = worldMatrix * viewMatrix * gPerspectiveProjectionMatrix;

	// push data in uniforms i.e. load data in constant buffer ~glUniformMatrix4fv()
	constantBuffer.WorldViewProjectionMatrix = wvpMatrix;

	gpID3D11DeviceContext->UpdateSubresource(	// we have one resource: CBUFFER
		gpID3D11Buffer_ConstantBuffer,		// pass to this constant buffer
		0,									// slot in shader
		NULL,								// D3D11_BOX (portion of the destination subresource to copy the resource data into)
		&constantBuffer,					// what to send
		0,									// byte width of one row (when you have box for 3rd para.); not needed by us
		0									// byte width of one height or depth (when you have box for 3rd para.); not needed by us
	);	// will Map(), memcpy() and Unmap() for constant buffer

		// select geometry primitive to draw (during IA stage)
	gpID3D11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);	// triangle list but ours is only one triangle

	// draw vertex buffer to render target
	gpID3D11DeviceContext->Draw(
		6,		// // how many vertices to draw
		0		// array position of your 9-member array to start with
	);

	gpID3D11DeviceContext->Draw(
		6,		// // how many vertices to draw
		6		// array position of your 9-member array to start with
	);

	gpID3D11DeviceContext->Draw(
		6,		// // how many vertices to draw
		12		// array position of your 9-member array to start with
	);

	gpID3D11DeviceContext->Draw(
		6,		// // how many vertices to draw
		18		// array position of your 9-member array to start with
	);

	gpID3D11DeviceContext->Draw(
		6,		// // how many vertices to draw
		24		// array position of your 9-member array to start with
	);

	gpID3D11DeviceContext->Draw(
		6,		// // how many vertices to draw
		30		// array position of your 9-member array to start with
	);
	// switch between front and back buffers ~SwapBuffers()
	gpIDXGISwapChain->Present(0, 0);		// Present=show
											// 1st 0: whether to synchronize with monitor's vertical refresh rate; 0=don't care. This shows DX is multithreaded
											// 2nd 0: show all frames from all buffers
}

void Update(void)
{
	angle_pyramid = angle_pyramid + 0.01f;	// greater increment means greater speed of rotation
	if (angle_pyramid >= 360.0f)			// discipline; angle is never greater than 360 degrees
		angle_pyramid = 0.0f;

	angle_cube = angle_cube - 0.01f;
	if (angle_cube <= -360.0f)
		angle_cube = 0.0f;
}

void Uninitialize()
{
	// code
	// safe release	// LIFO
	if (gpID3D11RasterizerState)
	{
		gpID3D11RasterizerState->Release();
		gpID3D11RasterizerState = NULL;
	}

	if (gpID3D11Buffer_ConstantBuffer)
	{
		gpID3D11Buffer_ConstantBuffer->Release();
		gpID3D11Buffer_ConstantBuffer = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Color_Cube)
	{
		gpID3D11Buffer_VertexBuffer_Color_Cube->Release();
		gpID3D11Buffer_VertexBuffer_Color_Cube = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Position_Cube)
	{
		gpID3D11Buffer_VertexBuffer_Position_Cube->Release();
		gpID3D11Buffer_VertexBuffer_Position_Cube = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Color_Pyramid)
	{
		gpID3D11Buffer_VertexBuffer_Color_Pyramid->Release();
		gpID3D11Buffer_VertexBuffer_Color_Pyramid = NULL;
	}

	if (gpID3D11Buffer_VertexBuffer_Position_Pyramid)
	{
		gpID3D11Buffer_VertexBuffer_Position_Pyramid->Release();
		gpID3D11Buffer_VertexBuffer_Position_Pyramid = NULL;
	}

	if (gpID3D11InputLayout)
	{
		gpID3D11InputLayout->Release();
		gpID3D11InputLayout = NULL;
	}

	if (gpID3D11PixelShader)
	{
		gpID3D11PixelShader->Release();
		gpID3D11PixelShader = NULL;
	}

	if (gpID3D11VertexShader)
	{
		gpID3D11VertexShader->Release();
		gpID3D11VertexShader = NULL;
	}

	if (gpID3D11DepthStencilView)
	{
		gpID3D11DepthStencilView->Release();
		gpID3D11DepthStencilView = NULL;
	}

	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Uninitialize() Succeeded.\n");
		fprintf_s(gpFile, "Log File Closed Successfully.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void ToggleFullScreen(HWND hwnd)
{
	MONITORINFO mi;

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(hwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(hwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(hwnd, &wpPrev);

		SetWindowPos(hwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}
}
