#include<windows.h>
#include<stdio.h>		// for file i/o
#include<d3d11.h>		// For DirectX's Direct3D (already present)
#pragma comment(lib,"d3d11.lib")		// corresponding to d3d11.dll (i.e. server)  (already present)

#define WIN_WIDTH 800
#define WIN_HEIGHT 600

// global variable declarations
DWORD dwStyle;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) };
HWND ghwnd = NULL;
bool gbIsFullScreen = false;
bool gbIsActiveWindow = false;
FILE *gpFile = NULL;
char gszLogFileName[] = "SD_Log.txt";
float gClearColor[4];	// RGBA	// to hold color; ~glClearColor()
// interfaces given to client by server (all interfaces are internally struct). All interfaces have 3 functions: QueryInterface(), AddRef(), Release()
IDXGISwapChain *gpIDXGISwapChain = NULL;	// for buffer swapping; DXGI= DirectX Graphics Infrastructure(the DX layer in GC) (there is also a layer for OGL)
ID3D11Device *gpID3D11Device = NULL;
ID3D11DeviceContext *gpID3D11DeviceContext = NULL;
ID3D11RenderTargetView *gpID3D11RenderTargetView = NULL;

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);

// WinMain
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	HRESULT Initialize(void);
	void Display(void);

	// variable declarations
	HWND hwnd;
	TCHAR szAppName[] = TEXT("MyApp");
	MSG msg;
	WNDCLASSEX wndclass;
	bool bDone = false;
	int iWindow_x, iWindow_y;

	// code
	// create log file
	if (fopen_s(&gpFile, gszLogFileName, "w") != 0)
	{
		MessageBox(NULL, TEXT("Log File Cannot Be Created.\nExiting...\n"), TEXT("Error"), MB_OK | MB_TOPMOST | MB_ICONSTOP);
		exit(0);
	}
	else
	{
		fprintf_s(gpFile, "Log File Created Successfully.\n");
		fclose(gpFile);
	}

	// initialization of WNDCLASSEX
	wndclass.cbSize = sizeof(wndclass);
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(NULL, IDI_APPLICATION);
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(NULL, IDI_APPLICATION);

	// register above class
	RegisterClassEx(&wndclass);

	// centering the window
	iWindow_x = GetSystemMetrics(SM_CXSCREEN) / 2 - WIN_WIDTH / 2;
	iWindow_y = GetSystemMetrics(SM_CYSCREEN) / 2 - WIN_HEIGHT / 2;

	// create window
	hwnd = CreateWindowEx(WS_EX_APPWINDOW,
		szAppName,
		TEXT("Sameera_Application"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE,
		iWindow_x,
		iWindow_y,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	ShowWindow(hwnd, iCmdShow);
	SetForegroundWindow(hwnd);	// window should be at top
	SetFocus(hwnd);	// window's title bar,etc. should be highlighted

	// initialize D3D
	HRESULT hr;
	hr = Initialize();			// since we are in COM
	if (FAILED(hr))	// macro
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Failed. Exitting Now...\n");
		fclose(gpFile);			// to avoid file becoming blank on error
		DestroyWindow(hwnd);
		hwnd = NULL;
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Initialize() Succeeded.\n");
		fclose(gpFile);
	}

	// game loop			// same
	while (bDone == false)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))	// PM_REMOVE: Messages are removed from the queue after processing by PeekMessage()
		{
			if (msg.message == WM_QUIT)
			{
				bDone = true;
			}
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbIsActiveWindow == true)
			{
				// Update();
			}
			Display();
		}
	}
	return((int)msg.wParam);
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(HWND);
	HRESULT Resize(int, int);
	void Uninitialize(void);

	// variable declarations
	HRESULT hr;

	// code
	switch (iMsg)
	{
	case WM_CHAR:
		switch (wParam)
		{
		case 'F':
		case 'f':
			ToggleFullScreen(hwnd);
			break;
		}
		break;

	case WM_SETFOCUS:
		gbIsActiveWindow = true;
		break;

	case  WM_KILLFOCUS:
		gbIsActiveWindow = false;
		break;

	case WM_SIZE:
		if (gpID3D11DeviceContext)	// if device context, then only go to resize
		{
			hr=Resize(LOWORD(lParam), HIWORD(lParam));
			if (FAILED(hr))
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "Resize() Failed.\n");
				fclose(gpFile);
				return(hr);	// no exit; up-casting of HRESULT TO LRESULT, hence no problem
			}
			else
			{
				fopen_s(&gpFile, gszLogFileName, "a+");
				fprintf_s(gpFile, "Resize() Succeeded.\n");
				fclose(gpFile);
			}
		}
		break;

	case WM_ERASEBKGND:
		return(0);

	case WM_CLOSE:	// Sent as a signal that a window or an application should terminate.
		DestroyWindow(hwnd);
		break;

	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_ESCAPE:
			DestroyWindow(hwnd);
			break;
		}
		break;

	case WM_DESTROY:
		Uninitialize();
		PostQuitMessage(0);
		break;
	}
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

HRESULT Initialize()
{
	// function declarations
	HRESULT Resize(int, int);
	void Uninitialize(void);

	// variable declarations
	HRESULT hr;
	D3D_DRIVER_TYPE d3dDriverType;
	D3D_DRIVER_TYPE d3dDriverTypes[] = {	// enum has 3 driver types; decreasing order of priority
		D3D_DRIVER_TYPE_HARDWARE,		// h/w rc (more quality)
		D3D_DRIVER_TYPE_WARP,			// Windows Accelerated Rasterization Platform
		D3D_DRIVER_TYPE_REFERENCE		// s/w rc
	};

	// ~glew in OpenGL
	D3D_FEATURE_LEVEL d3dFeatureLevel_required = D3D_FEATURE_LEVEL_11_0;	// needed feature level=11.0 (DX11)
	D3D_FEATURE_LEVEL d3dFeatureLevel_acquired = D3D_FEATURE_LEVEL_10_0;	// default,lowest; just initializing here, will be overwritten later

	// initializing variables
	UINT createDeviceFlags = 0;
	UINT numDriverTypes = 0;
	UINT numFeatureLevels = 1;	// can be more than 1; DX11 supports 8 FLs

	// code
	numDriverTypes = sizeof(d3dDriverTypes) / sizeof(d3dDriverTypes[0]);	// calculating no. of array elements

	// ~PFD
	DXGI_SWAP_CHAIN_DESC dxgiSwapChainDesc;		// descriptor of swap chain
												// struct (has 8 members: 2 structures(BufferDesc & SampleDesc), 2 enums(BufferUsage & DXGI_SWAP_CHAIN_FLAG(we don't use)), 4 individuals(BufferCount, OutputWindow, Windowed, DXGI_SWAP_EFFECT(we don't use)); we use 6);
	ZeroMemory((void *)&dxgiSwapChainDesc, sizeof(DXGI_SWAP_CHAIN_DESC));	// zero out members of struct
	dxgiSwapChainDesc.BufferCount = 1;	// how many buffers to swap(one buffer already given by DX which is front buffer, so you ask for one more)
	dxgiSwapChainDesc.BufferDesc.Width = WIN_WIDTH;		// BufferDesc is a structure of type DXGI_MODE_DESC
	dxgiSwapChainDesc.BufferDesc.Height = WIN_HEIGHT;
	dxgiSwapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;  // want such a buffer of DXGI_FORMAT struct which is unsigned normalized
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Numerator = 60;	// FPS
	dxgiSwapChainDesc.BufferDesc.RefreshRate.Denominator = 1;	// RefreshRate is struct of type DXGI_RATIONAL
	dxgiSwapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;	// want such a buffer which is a rendering target (o/p of rendering in it)
	dxgiSwapChainDesc.OutputWindow = ghwnd;		// output on which window
	dxgiSwapChainDesc.SampleDesc.Count = 1;		// single level sampling; SampleDesc is struct of type DXGI_SAMPLE_RATE
	dxgiSwapChainDesc.SampleDesc.Quality = 0;	// don't care
	dxgiSwapChainDesc.Windowed = TRUE;		// want windowed and fullscreen mode

	for (UINT driverTypeIndex = 0; driverTypeIndex < numDriverTypes; driverTypeIndex++)
	{
		d3dDriverType = d3dDriverTypes[driverTypeIndex];

		// get hardware and buffers' chain
		hr = D3D11CreateDeviceAndSwapChain(		// will return 5 things
			NULL,						// which adapter; NULL=GC of primary monitor; DXGI identifies GC as adapter while D#D identifies GC as device
			d3dDriverType,				// driver type
			NULL,						// software; handle to your rasterizer library if you do s/w rendering; NULL=don't care
			createDeviceFlags,			// flags; 0=don't care
			&d3dFeatureLevel_required,	// give this FL(giving filled)
			numFeatureLevels,			// how many feature levels you have and you need
			D3D11_SDK_VERSION,			// use library of this SDK version (it's a macro(UINT))
			&dxgiSwapChainDesc,			// swap chain desc(giving filled)
			&gpIDXGISwapChain,			// swap chain(empty; ret. value)
			&gpID3D11Device,			// device(empty; ret. value); give DX's logical device
			&d3dFeatureLevel_acquired,	// feature level obtained
			&gpID3D11DeviceContext		// device context
		);	// 12 para.

		if (SUCCEEDED(hr))
			break;
	}
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "D3D11CreateDeviceAndSwapChain() Succeeded.\n");
		fprintf_s(gpFile, "The Chosen Driver Is Of ");
		if (d3dDriverType == D3D_DRIVER_TYPE_HARDWARE)
		{
			fprintf_s(gpFile, "Hardware Type.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_WARP)
		{
			fprintf_s(gpFile, "WARP Type.\n");
		}
		else if (d3dDriverType == D3D_DRIVER_TYPE_REFERENCE)
		{
			fprintf_s(gpFile, "Reference Type.\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown Type.\n");
		}

		fprintf_s(gpFile, "The Supported Highest Feature Level Is ");	// you decide how many to check(we want DX10+)
		if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_11_0)
		{
			fprintf_s(gpFile, "11.0\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_1)
		{
			fprintf_s(gpFile, "10.1\n");
		}
		else if (d3dFeatureLevel_acquired == D3D_FEATURE_LEVEL_10_0)
		{
			fprintf_s(gpFile, "10.0\n");
		}
		else
		{
			fprintf_s(gpFile, "Unknown\n");
		}

		fclose(gpFile);

		// code similar to glewInit() done
	}
	// shaders will be here

	// d3d clear color (blue)
	gClearColor[0] = 0.0f;
	gClearColor[1] = 0.0f;
	gClearColor[2] = 1.0;
	gClearColor[3] = 1.0f;

	// *** call resize for first time ***
	hr = Resize(WIN_WIDTH, WIN_HEIGHT);
	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Resize() Succeeded.\n");
		fclose(gpFile);
	}

	return(S_OK);	// S_OK: operation successful
}

HRESULT Resize(int width, int height)
{
	// code
	HRESULT hr = S_OK;

	// free any size-dependent resources since size is changed
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	// resize swap chain buffers accordingly
	gpIDXGISwapChain->ResizeBuffers(
		1,							// how many buffers to resize
		width,						// width of buffer
		height, 					// height of buffer
		DXGI_FORMAT_R8G8B8A8_UNORM,	// format of dxgiSwapChainDesc.BufferDesc
		0							// swap chain flags
	);

	// again get back buffer from swap chain
	ID3D11Texture2D *pID3D11Texture2D_BackBuffer;	// reasons to take texture buffer (as it is we want some):
													// 1. Texture memory is supposed to be the fastest memory in GPU
													// 2. Texture memory is supposed to be the fastest accessible memory in GPU

	gpIDXGISwapChain->GetBuffer(
		0,										// give for 0th index (since back render always gets rendered first, then front buffer)
		__uuidof(ID3D11Texture2D),				// interfaces are known by GUID in COM
		(LPVOID*)&pID3D11Texture2D_BackBuffer		// long pointer to void*; give buffer in this(empty)
	);

	// again get render target view from d3d11 device using above back buffer
	hr = gpID3D11Device->CreateRenderTargetView(
		pID3D11Texture2D_BackBuffer,		// for this buffer
		NULL,							// D3D11_RENDER_TARGET_VIEW_DESC
		&gpID3D11RenderTargetView		// give in this
	);

	if (FAILED(hr))
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Failed.\n");
		fclose(gpFile);
		return(hr);
	}
	else
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "ID3D11Device::CreateRenderTargetView() Succeeded.\n");
		fclose(gpFile);
	}

	pID3D11Texture2D_BackBuffer->Release();		// not needed now
	pID3D11Texture2D_BackBuffer = NULL;

	// set render target view as render target(in pipeline; DC controls pipeline)
	gpID3D11DeviceContext->OMSetRenderTargets(	// OM= Output Merger
		1,							// only one RT currently; RTs are 2 minimum: depth stencil view and render target view
		&gpID3D11RenderTargetView,	// RTV
		NULL						// ID3D11DepthStencilView* ;no 3D
	);

	// set viewport
	D3D11_VIEWPORT d3dViewPort;		// 6 members
	d3dViewPort.TopLeftX = 0;
	d3dViewPort.TopLeftY = 0;
	d3dViewPort.Width = (float)width;
	d3dViewPort.Height = (float)height;
	d3dViewPort.MinDepth = 0.0f;	// new ~depth test in OGL
	d3dViewPort.MaxDepth = 1.0f;	// new ~depth test in OGL

	gpID3D11DeviceContext->RSSetViewports(1, &d3dViewPort);		// RS= Rasterizer; 1= 1 viewport(can be multiple, like in 24 spheres)

	return(hr);
}

void Display()
{
	// code
	// clear render target view to a chosen color
	gpID3D11DeviceContext->ClearRenderTargetView(gpID3D11RenderTargetView, gClearColor);	// 1st para: whom to clear; 2nd para: what color; ~glClearColor()

	// switch between front and back buffers ~SwapBuffers()
	gpIDXGISwapChain->Present(0, 0);		// Present=show
											// 1st 0: whether to synchronize with monitor's vertical refresh rate; 0=don't care. This shows DX is multithreaded
											// 2nd 0: show all frames from all buffers
}

void Uninitialize()
{
	// code
	// safe release
	if (gpID3D11RenderTargetView)
	{
		gpID3D11RenderTargetView->Release();
		gpID3D11RenderTargetView = NULL;
	}

	if (gpIDXGISwapChain)
	{
		gpIDXGISwapChain->Release();
		gpIDXGISwapChain = NULL;
	}

	if (gpID3D11DeviceContext)
	{
		gpID3D11DeviceContext->Release();
		gpID3D11DeviceContext = NULL;
	}

	if (gpID3D11Device)
	{
		gpID3D11Device->Release();
		gpID3D11Device = NULL;
	}

	if (gpFile)
	{
		fopen_s(&gpFile, gszLogFileName, "a+");
		fprintf_s(gpFile, "Uninitialize() Succeeded.\n");
		fprintf_s(gpFile, "Log File Closed Successfully.\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void ToggleFullScreen(HWND hwnd)
{
	MONITORINFO mi;

	if (gbIsFullScreen == false)
	{
		dwStyle = GetWindowLong(hwnd, GWL_STYLE);

		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			mi = { sizeof(MONITORINFO) };

			if (GetWindowPlacement(hwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(hwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(hwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);

				SetWindowPos(hwnd,
					HWND_TOP,
					mi.rcMonitor.left,
					mi.rcMonitor.top,
					mi.rcMonitor.right - mi.rcMonitor.left,
					mi.rcMonitor.bottom - mi.rcMonitor.top,
					SWP_NOZORDER | SWP_FRAMECHANGED);
			}
		}
		ShowCursor(FALSE);

		gbIsFullScreen = true;
	}
	else // fullscreen present
	{
		SetWindowLong(hwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);

		SetWindowPlacement(hwnd, &wpPrev);

		SetWindowPos(hwnd,
			HWND_TOP,
			0,
			0,
			0,
			0,
			SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER);

		ShowCursor(TRUE);

		gbIsFullScreen = false;
	}
}
