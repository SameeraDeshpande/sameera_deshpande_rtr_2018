// onload function
function main() // our entry-point function
{
    // 9 steps:
    // get <canvas> element
    var canvas = document.getElementById("AMC");    // document not declared means it is built-in; document: HTML page(representative of DOM);  '.' means internally document is a class
                                                    // var: inferred type
    if (!canvas)    // lets you know whether or not there is support for HTML5
        console.log("Obtaining Canvas Failed\n");   // console: predefined
    else
        console.log("Obtaining Canvas Succeeded\n");

    // print canvas width and height on console
    console.log("Canvas Width: "+canvas.width+" And Canvas Height: "+canvas.height);    // +: overloaded

    // get 2D context
    var context=canvas.getContext("2d");    // get 2D context;  ~wglGetContext() or ~glxGetContext()
    if(!context)
        console.log("Obtaining 2D Context Failed\n");
    else
        console.log("Obtaining 2D Context Succeeded\n");

    // fill canvas with black color
    context.fillStyle="black";  //"#000000": can use this too (00: R, 00: G, 00:B)
    context.fillRect(0,0,canvas.width,canvas.height);

    // center the text
    context.textAlign="center"; // center horizontally
    context.textBaseline="middle"; // center vertically

    // text
    var str="Hello World !!!";

    // text font
    context.font="48px sans-serif"; // '-' since space stands for font property

    // text color
    context.fillStyle="white";  // "#FFFFFF"(256)   // white since diff browsers have diff color gamuts

    // display the text in center
    context.fillText(str,canvas.width/2,canvas.height/2);   // para: what to show, text's x, text's y
}
