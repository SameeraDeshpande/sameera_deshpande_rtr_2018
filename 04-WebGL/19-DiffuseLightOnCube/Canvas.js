// global variables
var canvas = null;
var gl = null;  // WebGL context (context named as gl to have 'gl' in our code)
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros=      // when whole 'WebGLMacros' is const, all inside it are automatically const
{
    AMC_ATTRIBUTE_POSITION: 0,  // KVC (key-value coding): values assigned at compile time
	AMC_ATTRIBUTE_COLOR: 1,
	AMC_ATTRIBUTE_NORMAL: 2,
	AMC_ATTRIBUTE_TEXCOORD0: 3, // (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// shader objects
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao;
var vbo_position, vbo_normal;
var mvUniform;
var pUniform;
var ldUniform;	// diffused lighting
var kdUniform;	// coefficient of material's diffused reflectivity
var lightPositionUniform;	
var isLKeyPressedUniform;
var perspectiveProjectionMatrix;
var angle_cube = 0.0;
var gbLighting = false;

// requestAnimationFrame and cancelAnimationFrame: ~glSwapBuffers() or glxSwapBuffers() or requestRender()
// to start animation : to have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =             // function pointer
window.requestAnimationFrame ||         // for browsers not having their own frame; // window: in-built variable
window.webkitRequestAnimationFrame ||   // Safari
window.mozRequestAnimationFrame ||      // Mozilla
window.oRequestAnimationFrame ||        // Opera
window.msRequestAnimationFrame;         // IE/Edge
// || null also ok

// to stop animation : to have cancelAnimationFrame() to be called "cross-browser" compatible
// not required in our code but required in offline rendering
var cancelAnimationFrame =              // function pointer 
window.cancelAnimationFrame ||         // for browsers not having their own frame
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||// Safari     // 2 for different versions of browser engines(?)
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || // Mozilla
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||   // Opera
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;   // IE/Edge

// onload function
function main()     // our entry-point function
{
    // get <canvas> element
    canvas = document.getElementById("AMC");    // document not declared means it is built-in; document: HTML page(representative of DOM), '.' means internally document is a class
                                                    // var: inferred type
    if (!canvas)    // lets you know whether or not there is support for HTML5
        console.log("Obtaining Canvas Failed\n");   // console: predefined
    else
        console.log("Obtaining Canvas Succeeded\n");

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);     // "keydown": window's event listener(not our name); keydown: our event listener; false: don't do event bubbling i.e. sending event to super and up(bubble capture)
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", Resize, false);

    // initialize WebGL
    Initialize();

    // start drawing here as warming-up
    Resize();
    Display();
}


// define your event handlers (callback functions)
function keyDown(event)
{
    // code
    switch (event.keyCode)
    {
        case 70:    // 70: ASCII for F; 'F' or 'f'
            ToggleFullScreen();
            break;

        case 76:    // for 'L' or 'l'
            if (gbLighting == false)
                gbLighting = true;
            else
                gbLighting = false;
            break;

        case 27:    // escape
            Uninitialize();      // don't call when using Firefox as it gives warnings when escape key is pressed
            // close our application's tab
            window.close();     // may not work in Firefox but works in Safari and Chrome
            break;
    }
}

function mouseDown()
{
    // code
}

function Initialize()
{
    // code
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");   // webgl1 for mobile browser    // not working in IE and Edge?
    if (gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    // set viewport height and width
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader
    // define vertex shader object
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);	// creates a shader that is given as parameter	// ~ GL_VERTEX_SHADER

	// write vertex shader source code
    var vertexShaderSourceCode=
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_mv_matrix;" +
    "uniform mat4 u_p_matrix;" +
    "uniform mediump int u_isLKeyPressed;" +
    "uniform vec3 u_ld;" +
    "uniform vec3 u_kd;" +
    "uniform vec4 u_light_position;" +
    "out vec3 diffuse_color;" +
    "void main(void)" +
    "{" +
    "if(u_isLKeyPressed==1)" +
    "{" +
    "vec4 eye_coordinates = u_mv_matrix * vPosition;" +
    "mat3 normal_matrix = mat3(u_mv_matrix);" +
    "vec3 t_norm = normalize(normal_matrix * vNormal);" +
    "vec3 light_direction = normalize(vec3(u_light_position - eye_coordinates));" +
    "diffuse_color = u_ld * u_kd * dot(light_direction,t_norm);" +
    "}" +
    "gl_Position = u_p_matrix * u_mv_matrix * vPosition;" +
    "}"         // written in Graphics Library Shading/Shader Language (GLSL)
                // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES is 3.0 based
                // '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
                // ****VERY IMPORTANT****: uniform mediump int u_isSingleTapMade; program does not run if int not made mediump. Precision is imp. in ES. VS by def has highp for all. But int does not require highp(precision is related to battery life)
				// eye coordinates are not related to any projection, hence only mv
				// uniform mat4 u_mv_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// mat3 normal_matrix = mat3(u_mv_matrix): typecasting into mat3 gives you the normal matrix i.e. mat3(transpose(inverse(u_mv_matrix))); It gives first upper 3*3 matrix of the 4*4 matrix. transpose(), inverse() are shader functions
				// t_norm: transformed normal. normalize() is a shader function
				// vec3(u_light_position - eye_coordinates.xyz)= typecasting vec4 into vec3; eye_coordinates.xyz: taking x,y,z from x,y,z,w
				// actually, henceforth, do diffuse_color = u_ld * u_kd * max(dot(light_direction,t_norm),0.0): dot(): function for dot product; max(): to avoid sending -ve values to shader
				// gl_Position: in-built variable of shader

    // give above source code to the vertex shader object
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

    // compile the vertex shader
	gl.compileShader(vertexShaderObject);
    // error checking
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Vertex Shader Compilation Log : \n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // vertex converted to fragment now

    // define fragment shader object
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);	// creates a shader that is given as parameter	// ~ GL_FRAGMENT_SHADER
    
    // write vertex shader source code
    var fragmentShaderSourceCode=
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec3 diffuse_color;" +
    "out vec4 frag_color;" +
    "uniform int u_isLKeyPressed;" +
    "void main(void)" +
    "{" +
    "if(u_isLKeyPressed==1)" +
    "{" +
    "frag_color = vec4(diffuse_color,1.0);" +
    "}" +
    "else" +
    "{" +
    "frag_color = vec4(1.0,1.0,1.0,1.0);" +
    "}" +	
    "}"         // written in Graphics Library Shading/Shader Language (GLSL)
                // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES 3.0 based
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
                // precision highp float: give high precision to float.(error if not specified) Use mediump for int(def). VS by def has highp for all. Max processing happens in FS than VS as FS gives ultimate color.
                // precision needed only in ES as there are many math units
                // vec4: function/macro/constructor (of vmath?)
                // frag_color = vec4(diffuse_color,1.0): 1.0 just for converting vec3 to vec4
				// actually, checking the state of u_isLKeyPressed again is not a good code here. Instead, we should pass a variable from VS to FS for that state. But we are checking it here to explain the concept that uniforms(u_isLKeyPressed) are global to shaders.
				// frag_color = vec4(1.0,1.0,1.0,1.0): will give white color to fragment; don't write 'f' in shaders (if class)

    // give above source code to the fragment shader object
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

    // compile the fragment shader
	gl.compileShader(fragmentShaderObject);
    // error checking
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Fragment Shader Compilation Log : \n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // create shader program object 
	shaderProgramObject = gl.createProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject, vertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject, fragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding of shaderProgramObject to vertex shader attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	gl.bindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
		WebGLMacros.AMC_ATTRIBUTE_POSITION,		// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
        // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition
       
    gl.bindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
        WebGLMacros.AMC_ATTRIBUTE_NORMAL,		// the index of the generic vertex attribute to be bound.
        "vNormal"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
    );	

    // link the shader program to your program
	gl.linkProgram(shaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
    // error checking (eg. of link error- version incompatibility of shaders)
    if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Shader Program Link Log : \n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // post-linking retrieving uniform locations (uniforms are global to shaders)
	mvUniform = gl.getUniformLocation(shaderProgramObject, "u_mv_matrix");	// preparation of data transfer from CPU to GPU (binding)
                                                                                // u_mvp_matrix: of GPU; mvpUniform: of CPU
                                                                                // telling it to take location of uniform u_mvp_matrix and give in mvpUniform

    pUniform = gl.getUniformLocation(shaderProgramObject, "u_p_matrix");
    isLKeyPressedUniform = gl.getUniformLocation(shaderProgramObject, "u_isLKeyPressed");
    ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
    lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");

    // fill triangle vertices in array (this was in Display() in FFP)
    var cubePosition = new Float32Array([ 
        // top
		1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
		// bottom
		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,
		// front
		1.0, 1.0, 1.0,
		-1.0, 1.0, 1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,
		// back
		-1.0, 1.0, -1.0,
		1.0, 1.0, -1.0,
		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		// right
		1.0, 1.0, -1.0,
		1.0, 1.0, 1.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,
		// left
		-1.0, 1.0, 1.0,
		-1.0, 1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0
    ]);	// Float32Array: their type(constructor) (of type ArrayBufferView)
        // Float32Array(i.e. ArrayBufferView) required in gl.bufferData()
        // since written in Initialize() only, coords will not get set everytime in Display(). Hence, fast speed.

    var cubeNormal = new Float32Array([ 
        // top
		0.0, 1.0, 0.0,			// normal coming outside the surface
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		0.0, 1.0, 0.0,
		// bottom
		0.0, -1.0, 0.0,
		0.0, -1.0, 0.0,
		0.0, -1.0, 0.0,
		0.0, -1.0, 0.0,
		// front
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		0.0, 0.0, 1.0,
		// back
		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,
		0.0, 0.0, -1.0,
		// right
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		1.0, 0.0, 0.0,
		// left
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0,
		-1.0, 0.0, 0.0
    ]);                                            

    // 9 lines
    // create vao(id) (vao is shape-wise)
    vao = gl.createVertexArray();		// generate vertex array object name; createVertexArray(): different function
                                        // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

    gl.bindVertexArray(vao);		// bind a vertex array object

    // cube position
    // create vbo (vbo is attribute-wise)
    vbo_position = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        cubePosition,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    // cube normals
    // create vbo (vbo is attribute-wise)
    vbo_normal = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_normal);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        cubeNormal,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vColor 
                                           
    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    gl.bindVertexArray(null);		// unbind vao

    gl.clearDepth(1.0);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	gl.enable(gl.DEPTH_TEST);	// to compare depth values of objects

	gl.depthFunc(gl.LEQUAL);	// specifies the value used for depth-buffer comparisons
                            // Passes if the incoming z value is less than or equal to the stored z value.
             
    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // initialize matrices
    perspectiveProjectionMatrix = mat4.create();	// creates the matrix in identity format(diagonals 1); only does identity if matrix already present (?)

    // no warm-up call
}

function Resize()
{
    // code
    // set viewport
    if (bFullscreen == true)    // need to write this explicitly since there is no repaint() calling resize() here
    {
        canvas.width = window.innerWidth;   // inner property of fullscreen
        canvas.height = window.innerHeight; // ~code of WM_SIZE in Windows
    }
    else
    {
        canvas.width = canvas_original_width;   // taken in main()
        canvas.height = canvas_original_height;
    }

    if(canvas.height==0)
    {
        canvas.height=1;
    }

    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);

    // perspective projection
    mat4.perspective(perspectiveProjectionMatrix, degToRad(45.0), parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);    // parameters:
                                                                                                                            // o/p matrix
                                                                                                                            // fovy- The field of view angle, in radians, in the y - direction.
                                                                                                                            // aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
                                                                                                                            // zNear- The distance from the viewer to the near clipping plane(always positive).
                                                                                                                            // zFar- The distance from the viewer to the far clipping plane(always positive).
}

function Display()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	gl.useProgram(shaderProgramObject);	// binding your OpenGL code with shader program object
                                        // Specifies the handle of the program object whose executables are to be used as part of current rendering state.

    // 4 CPU steps
    // declaration and initialization of matrices
    var modelViewMatrix=mat4.create();  // creates the matrix in identity format(diagonals 1)

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0,0.0,-6.0]);   // parameters: o/p matrix, the matrix to translate, vector to translate by
    mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));   // degToRad(): our function
    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));   // degToRad(): our function
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));   // degToRad(): our function.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

    gl.uniformMatrix4fv(pUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        perspectiveProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

    var lightPosition = [ 0.0,0.0,2.0,1.0 ];	// x,y,z,w; 1.0 is for non-directional light

	if (gbLighting == true)
	{
		gl.uniform1i(isLKeyPressedUniform, 1);	// send 1 if 'L' key is pressed; 1i: to send 1 int
		gl.uniform3f(ldUniform, 1.0, 1.0, 1.0);	// white light; to send vec3
		gl.uniform3f(kdUniform, 0.5, 0.5, 0.5);	// gray material; to send vec3
		gl.uniform4fv(lightPositionUniform, lightPosition);	// no 1 here; to send vec4; ~glLightfv(GL_LIGHT0, GL_POSITION, LightPosition) in FFP
	}
	else
	{
		gl.uniform1i(isLKeyPressedUniform, 0);	// send 0 if 'L' key is not pressed; 1i: to send 1 int
    }
    
    // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
    gl.bindVertexArray(vao);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

    // draw the necessary scene!
    gl.drawArrays(gl.TRIANGLE_FAN,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
        0,							// array position of your 9-member array to start with (imp in inter-leaved)
        4							// how many vertices to draw
    );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
            // Arrays since multiple primitives(P,C,N,T) can be drawn

    gl.drawArrays(gl.TRIANGLE_FAN,
        4,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        8,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        12,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        16,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        20,
        4
    );

    // unbind vao
    gl.bindVertexArray(null);

    // unuse program
    gl.useProgram(null);	// unbinding your OpenGL code with shader program object
                            // If program is null, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.                                   
    
    // animation loop
    Update();   // write here in Display() since we don't have an explicit game loop
    requestAnimationFrame(Display, canvas);     // game loop;  requests that the browser calls a specified function to update an animation before the next repaint
                                                // requestAnimationFrame: function pointer; canvas: what to draw
                                                // this is not recursion !
}

function Update()
{
    angle_cube = angle_cube - 1.0;
	if (angle_cube <= -360.0)
        angle_cube = 0.0;
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI/180.0);    // Math is a built-in object that has properties and methods for mathematical constants and functions.
                                        // Math.PI since all properties(e.g. PI) and methods(e.g. sin()) of Math are static
}

function Uninitialize()     // actually no need to call this function as there is garbage collection
{
    // code
    if (shaderProgramObject)
    {        
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }

        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }

        // delete the shader program
        gl.deleteProgram(shaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
        shaderProgramObject = null;
    }

    if (vbo_normal)
    {
        gl.deleteBuffer(vbo_normal);	// delete named buffer object                              
        vbo_normal = null;
    }

    if (vbo_position)
    {
        gl.deleteBuffer(vbo_position);	// delete named buffer object                              
        vbo_position = null;
    }

    if (vao)
    {
        gl.deleteVertexArray(vao);		// delete vertex array object
        vao = null;
    }
}

function ToggleFullScreen()     // handles browser-dependent code
{
    // code
    var fullscreen_element =
        document.fullscreenElement ||           // flags(pre-defined in browsers) will assign this value if it is present or will go further    // Opera
        document.webkitFullscreenElement ||     // Apple's Safari
        document.mozFullScreenElement ||        // Mozilla Firefox
        document.msFullscreenElement ||         // Microsoft's Internet Explorer or Edge
        null;                                   // if none of the above

    // if not fullscreen
    if (fullscreen_element == null)
    {
        if (canvas.requestFullscreen)       // requestFullscreen: function pointer      // Opera
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)       // Mozilla Firefox
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)    // Apple's Safari
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)        // Microsoft's Internet Explorer or Edge
            canvas.msRequestFullscreen();

        bFullscreen=true;
    }
    else    // if already fullscreen
    {
        if (document.exitFullscreen)        // calling on document, not on canvas
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();

        bFullscreen = false;
    }
}
