// global variables
var canvas=null;    // not needed in this program actually
var context=null;   // not needed in this program actually
                    // never keep uninitialized variables in scripting languages

// onload function
function main() // our entry-point function
{
    // 9 steps:
    // get <canvas> element
    canvas = document.getElementById("AMC");    // document not declared means it is built-in; document: HTML page(representative of DOM);  '.' means internally document is a class
                                                    // var: inferred type
    if (!canvas)    // lets you know whether or not there is support for HTML5
        console.log("Obtaining Canvas Failed\n");   // console: predefined
    else
        console.log("Obtaining Canvas Succeeded\n");

    // print canvas width and height on console
    console.log("Canvas Width: "+canvas.width+" And Canvas Height: "+canvas.height);    // +: overloaded

    // get 2D context
    context=canvas.getContext("2d");    // get 2D context;  ~wglGetContext() or ~glxGetContext()
    if(!context)
        console.log("Obtaining 2D Context Failed\n");
    else
        console.log("Obtaining 2D Context Succeeded\n");

    // fill canvas with black color
    context.fillStyle="black";  //"#000000": can use this too (00: R, 00: G, 00:B)
    context.fillRect(0,0,canvas.width,canvas.height);

    // draw text
    drawText("Hello World !!!");

    // register and map keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);     // window: predefined
                                                            // "keydown": window's event listener(not our name); keydown: our event listener; false: don't do event bubbling i.e. sending event to super and up(bubble capture)
    window.addEventListener("click", mouseDown, false);
}

function drawText(text)
{
    // code
    // center the text
    context.textAlign="center"; // center horizontally
    context.textBaseline="middle"; // center vertically

    // text font
    context.font="48px sans-serif"; // '-' since space stands for font property

    // text color
    context.fillStyle="white";  // "#FFFFFF"(256)   // white since diff browsers have diff color gamuts

    // display the text in center
    context.fillText(text,canvas.width/2,canvas.height/2);   // para: what to show, text's x, text's y
}

function toggleFullScreen()     // handles browser-dependent code
{
    // code
    var fullscreen_element =
        document.fullscreenElement ||           // flags(pre-defined in browsers) will assign this value if it is present or will go further    // Opera
        document.webkitFullscreenElement ||     // Apple's Safari
        document.mozFullScreenElement ||        // Mozilla Firefox
        document.msFullscreenElement ||         // Microsoft's Internet Explorer or Edge
        null;                                   // if none of the above

    // if not fullscreen
    if (fullscreen_element == null)
    {
        if (canvas.requestFullscreen)       // requestFullscreen: function pointer      // Opera
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)       // Mozilla Firefox
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)    // Apple's Safari
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)        // Microsoft's Internet Explorer or Edge
            canvas.msRequestFullscreen();
    }
    else    // if already fullscreen
    {
        if (document.exitFullscreen)        // calling on document, not on canvas
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();
    }
}

// define your event handlers (callback functions)
function keyDown(event)
{
     // code
     switch (event.keyCode)
     {
         case 70:    // 70: ASCII for F; 'F' or 'f'
             toggleFullScreen();

            // repaint (explicitly since there is no repainting by default in browser. But WebGL has taken care of it. But there is no WebGL yet in this program. Hence, calling drawText() again.)
            drawText("Hello World !!!");
            break;
     }
}

function mouseDown(event)
{
    // code
}
