// global variables
var canvas = null;
var gl = null;  // WebGL context (context named as gl to have 'gl' in our code)
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros=      // when whole 'WebGLMacros' is const, all inside it are automatically const
{
    AMC_ATTRIBUTE_POSITION: 0,  // KVC (key-value coding): values assigned at compile time
	AMC_ATTRIBUTE_COLOR: 1,
	AMC_ATTRIBUTE_NORMAL: 2,
	AMC_ATTRIBUTE_TEXCOORD0: 3, // (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// shader objects
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_inner_cube;
var vbo_position_inner_cube;
var vao_outer_cube;
var vbo_position_outer_cube;
var vbo_texture_outer_cube;
var mvpUniform;
var samplerUniform;
var perspectiveProjectionMatrix;
var outer_cube_texture=0;
var angle_cube = 0.0;
var targetTextureWidth = 256;
var targetTextureHeight = 256;
var defaultFramebuffer;

// requestAnimationFrame and cancelAnimationFrame: ~glSwapBuffers() or glxSwapBuffers() or requestRender()
// to start animation : to have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =             // function pointer
window.requestAnimationFrame ||         // for browsers not having their own frame; // window: in-built variable
window.webkitRequestAnimationFrame ||   // Safari
window.mozRequestAnimationFrame ||      // Mozilla
window.oRequestAnimationFrame ||        // Opera
window.msRequestAnimationFrame;         // IE/Edge
// || null also ok

// to stop animation : to have cancelAnimationFrame() to be called "cross-browser" compatible
// not required in our code but required in offline rendering
var cancelAnimationFrame =              // function pointer 
window.cancelAnimationFrame ||         // for browsers not having their own frame
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||// Safari     // 2 for different versions of browser engines(?)
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || // Mozilla
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||   // Opera
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;   // IE/Edge

// onload function
function main()     // our entry-point function
{
    // get <canvas> element
    canvas = document.getElementById("AMC");    // document not declared means it is built-in; document: HTML page(representative of DOM), '.' means internally document is a class
                                                    // var: inferred type
    if (!canvas)    // lets you know whether or not there is support for HTML5
        console.log("Obtaining Canvas Failed\n");   // console: predefined
    else
        console.log("Obtaining Canvas Succeeded\n");

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);     // "keydown": window's event listener(not our name); keydown: our event listener; false: don't do event bubbling i.e. sending event to super and up(bubble capture)
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", Resize, false);

    // initialize WebGL
    Initialize();

    // start drawing here as warming-up
    Resize();
    Display();
}


// define your event handlers (callback functions)
function keyDown(event)
{
    // code
    switch (event.keyCode)
    {
        case 27:    // escape
            Uninitialize();     // don't call when using Firefox as it gives warnings when escape key is pressed
            // close our application's tab
            window.close();     // may not work in Firefox but works in Safari and Chrome
            break;

        case 70:    // 70: ASCII for F; 'F' or 'f'
            ToggleFullScreen();
            break;
    }
}

function mouseDown()
{
    // code
}

function Initialize()
{
    // code
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");   // webgl1 for mobile browser    // not working in IE and Edge?
    if (gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    // set viewport height and width
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader
    // define vertex shader object
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);	// creates a shader that is given as parameter	// ~ GL_VERTEX_SHADER

	// write vertex shader source code
    var vertexShaderSourceCode=
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec2 vTexture0_Coord;" +
    "uniform mat4 u_mvp_matrix;" +
    "out vec2 out_texture0_coord;" +
    "void main(void)" +
    "{" +
    "gl_Position = u_mvp_matrix * vPosition;" +
    "out_texture0_coord = vTexture0_Coord;" +
    "}";     // written in Graphics Library Shading/Shader Language (GLSL)
            // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES is 3.0 based
            // '\n' important and compulsory since shader file would have an enter after the version stmt 
            // in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
            // uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
            // gl_Position: in-built variable of shader

    // give above source code to the vertex shader object
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

    // compile the vertex shader
	gl.compileShader(vertexShaderObject);
    // error checking
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Vertex Shader Compilation Log : \n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // vertex converted to fragment now

    // define fragment shader object
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);	// creates a shader that is given as parameter	// ~ GL_FRAGMENT_SHADER
    
    // write vertex shader source code
    var fragmentShaderSourceCode=
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec2 out_texture0_coord;" +
    "uniform highp sampler2D u_texture0_sampler;"+
    "out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "FragColor = texture(u_texture0_sampler,out_texture0_coord);" +	
    "}";         // written in Graphics Library Shading/Shader Language (GLSL)
                // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES is 3.0 based
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // out: output of shader;
                // precision highp float: give high precision to float. Use mediump for int(def). VS by def has highp for all. Max processing happens in FS than VS as it gives ultimate color.
                // precision needed only in ES as there are many math units
                // vec4: function/macro/constructor (of vmath?)
                // FragColor = texture(u_sampler,out_texCoord): this proves that texture is a type of color.
				// texture() is a function of GLSL. u_sampler: Specifies the sampler to which the texture is bound. Texture sampling is the act of retrieving data from a texture. out_texCoord: Specifies the texture coordinates at which texture will be sampled.
				// u_sampler colors out_texCoord and in-turn fragment gets colored

    // give above source code to the fragment shader object
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

    // compile the fragment shader
	gl.compileShader(fragmentShaderObject);
    // error checking
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Fragment Shader Compilation Log : \n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // create shader program object 
	shaderProgramObject = gl.createProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject, vertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject, fragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding of shaderProgramObject to vertex shader attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	gl.bindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
		WebGLMacros.AMC_ATTRIBUTE_POSITION,		// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
        // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition
       
    gl.bindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
        WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0,		// the index of the generic vertex attribute to be bound.
        "vTexture0_Coord"						// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
    );	

    // link the shader program to your program
	gl.linkProgram(shaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
    // error checking (eg. of link error- version incompatibility of shaders)
    if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Shader Program Link Log : \n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // post-linking retrieving uniform locations (uniforms are global to shaders)
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");	// preparation of data transfer from CPU to GPU (binding)
                                                                                // u_mvp_matrix: of GPU; mvpUniform: of CPU
                                                                                // telling it to take location of uniform u_mvp_matrix and give in mvpUniform

    samplerUniform = gl.getUniformLocation(shaderProgramObject, "u_texture0_sampler");
                                                                            
    // fill triangle vertices in array (this was in Display() in FFP)
    var cubePosition = new Float32Array([ 
        // top
		1.0, 1.0, -1.0,
		-1.0, 1.0, -1.0,
		-1.0, 1.0, 1.0,
		1.0, 1.0, 1.0,
		// bottom
		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,
		// front
		1.0, 1.0, 1.0,
		-1.0, 1.0, 1.0,
		-1.0, -1.0, 1.0,
		1.0, -1.0, 1.0,
		// back
		-1.0, 1.0, -1.0,
		1.0, 1.0, -1.0,
		1.0, -1.0, -1.0,
		-1.0, -1.0, -1.0,
		// right
		1.0, 1.0, -1.0,
		1.0, 1.0, 1.0,
		1.0, -1.0, 1.0,
		1.0, -1.0, -1.0,
		// left
		-1.0, 1.0, 1.0,
		-1.0, 1.0, -1.0,
		-1.0, -1.0, -1.0,
		-1.0, -1.0, 1.0
    ]); 

    var cubeTexcoords = new Float32Array([ 
        // top
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		// bottom
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		// front
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		// back
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		// right
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
		// left
		0.0, 0.0,
		1.0, 0.0,
		1.0, 1.0,
		0.0, 1.0,
    ]);

    // inner cube
    // 9 lines
    // create vao(id) (vao is shape-wise)
    vao_inner_cube = gl.createVertexArray();		// generate vertex array object name; createVertexArray(): different function
                                                // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

    gl.bindVertexArray(vao_inner_cube);		// bind a vertex array object

    // cube position
    // create vbo (vbo is attribute-wise)
    vbo_position_inner_cube = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_inner_cube);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        cubePosition,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    gl.bindVertexArray(null);		// unbind vao
    
    // outer cube
    // 9 lines
    // create vao(id) (vao is shape-wise)
    vao_outer_cube = gl.createVertexArray();		// generate vertex array object name; createVertexArray(): different function
                                                // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

    gl.bindVertexArray(vao_outer_cube);		// bind a vertex array object

    // cube position
    // create vbo (vbo is attribute-wise)
    vbo_position_outer_cube = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_outer_cube);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        cubePosition,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    // cube texture
    // create vbo (vbo is attribute-wise)
    vbo_texture_outer_cube = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_texture_outer_cube);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        cubeTexcoords,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        2,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    gl.bindVertexArray(null);		// unbind vao

    loadCubeTexture();

    gl.clearDepth(1.0);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	gl.enable(gl.DEPTH_TEST);	// to compare depth values of objects

	gl.depthFunc(gl.LEQUAL);	// specifies the value used for depth-buffer comparisons
                                // Passes if the incoming z value is less than or equal to the stored z value.

    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // initialize projection matrix
    perspectiveProjectionMatrix = mat4.create();	// creates the matrix in identity format(diagonals 1); only does identity if matrix already present (?)

    // no warm-up call
}

function loadCubeTexture()
{
    outer_cube_texture=gl.createTexture();
    // texture.image = new Image();      // Image: HTMLImageElement; browser specific class; '.' shows it is a DS
    // texture.image.src=checkImage;  // source of image

    // bind texture to a target texture (type of texture)
    gl.bindTexture(gl.TEXTURE_2D, outer_cube_texture);	// binding the first texture(value) to a 2D array type usage
                                            // Note: Texture targets become aliases for textures currently bound to them

    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);

    // fill the data and tell it how to use it
    gl.texImage2D(
        gl.TEXTURE_2D,          // where to fill data; the target texture
        0,                      // mipmap level
        gl.RGBA,                // internal image format
        targetTextureWidth,        // width of the texture image
        targetTextureHeight,       // ht of the texture image
        0,                      // border width (no border)
        gl.RGBA,                // texel format
        gl.UNSIGNED_BYTE,       // data type of the texel data(last para)
        null,            // actual data
    );

    //gl.generateMipmap(gl.TEXTURE_2D);	// texture data in this now

    // Create and bind the framebuffer
    defaultFramebuffer = gl.createFramebuffer();

    gl.bindFramebuffer(gl.FRAMEBUFFER, defaultFramebuffer);
    
    // attach the texture as the first color attachment
    gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, outer_cube_texture, 0);

    // Create and bind the depth buffer
    const depthRenderbuffer = gl.createRenderbuffer();
    
    gl.bindRenderbuffer(gl.RENDERBUFFER, depthRenderbuffer);

    gl.renderbufferStorage(
        gl.RENDERBUFFER,			// target
        gl.DEPTH_COMPONENT16,		// 16 bit depth
        targetTextureWidth,			// width of DB
        targetTextureHeight			// ht of DB
    );

    // attach DB to FB 
    gl.framebufferRenderbuffer(
        gl.FRAMEBUFFER,
        gl.DEPTH_ATTACHMENT,
        gl.RENDERBUFFER,
        depthRenderbuffer
    );

    // unbind framebuffer
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        
    // unbind texture explicitly
    gl.bindTexture(gl.TEXTURE_2D, null);
}

function Resize()
{
    // code
    // set viewport
    if (bFullscreen == true)    // need to write this explicitly since there is no repaint() calling resize() here
    {
        canvas.width = window.innerWidth;   // inner property of fullscreen
        canvas.height = window.innerHeight; // ~code of WM_SIZE in Windows
    }
    else
    {
        canvas.width = canvas_original_width;   // taken in main()
        canvas.height = canvas_original_height;
    }

    if(canvas.height==0)
    {
        canvas.height=1;
    }

    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);

    // perspective projection
    mat4.perspective(perspectiveProjectionMatrix, degToRad(45.0), parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);  // parameters:
                                                                                                                                    // o/p matrix
                                                                                                                                    // fovy- The field of view angle, in radians, in the y - direction.
                                                                                                                                    // aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
                                                                                                                                    // zNear- The distance from the viewer to the near clipping plane(always positive).
                                                                                                                                    // zFar- The distance from the viewer to the far clipping plane(always positive).
}

function Display()
{
    // code
    //gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	gl.useProgram(shaderProgramObject);	// binding your OpenGL code with shader program object
                                        // Specifies the handle of the program object whose executables are to be used as part of current rendering state.

     // 4 CPU steps
     // declaration and initialization of matrices
    var modelViewMatrix=mat4.create();  // creates the matrix in identity format(diagonals 1); only does identity if matrix already present (?)
    var modelViewProjectionMatrix=mat4.create();    // creates the matrix in identity format(diagonals 1); only does identity if matrix already present (?)

    // initialize above matrices to identity
    mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // inner cube
    // render to our targetTexture by binding the framebuffer
    gl.bindFramebuffer(gl.FRAMEBUFFER, defaultFramebuffer);
 
    // Tell WebGL how to convert from clip space to pixels
    gl.viewport(0, 0, targetTextureWidth, targetTextureHeight);
 
    // Clear the attachment(s).
    gl.clearColor(0.25, 0.25, 0.25, 1);   // clear to blue
    gl.clear(gl.COLOR_BUFFER_BIT| gl.DEPTH_BUFFER_BIT);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0,0.0,-6.0]);   // parameters: o/p matrix, the matrix to translate, vector to translate by
    mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));   // degToRad(): our function
    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));   // degToRad(): our function
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));   // degToRad(): our function

    mat4.perspective(perspectiveProjectionMatrix, degToRad(45.0), parseFloat(targetTextureWidth)/parseFloat(targetTextureHeight), 0.1, 100.0);

    // do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						    // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

    // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
    gl.bindVertexArray(vao_inner_cube);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

    gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, 1.0, 0.0, 0.0);	// specify the value of a generic vertex attribute(for single color to object)

    // draw the necessary scene!
    gl.drawArrays(gl.TRIANGLE_FAN,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
        0,							// array position of your 9-member array to start with (imp in inter-leaved)
        4							// how many vertices to draw
    );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
            // Arrays since multiple primitives(P,C,N,T) can be drawn

    gl.drawArrays(gl.TRIANGLE_FAN,
        4,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        8,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        12,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        16,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        20,
        4
    );
    
    // unbind vao
    gl.bindVertexArray(null);

    // unbind fbo
    gl.bindFramebuffer(gl.FRAMEBUFFER, null);
    
    // render to the screen
    // Tell WebGL how to convert from clip space to pixels
    gl.viewport(0, 0, canvas.width, canvas.height);
 
    // Clear the attachment(s).
    gl.clearColor(0, 0, 0, 1);  
    gl.clear(gl.COLOR_BUFFER_BIT| gl.DEPTH_BUFFER_BIT);

    mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0,0.0,-6.0]);   // parameters: o/p matrix, the matrix to translate, vector to translate by
    mat4.rotateX(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));   // degToRad(): our function
    mat4.rotateY(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));   // degToRad(): our function
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angle_cube));   // degToRad(): our function

    mat4.perspective(perspectiveProjectionMatrix, degToRad(45.0), parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);

    // do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						    // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

     // B: bind a named texture to a texturing target
	gl.bindTexture(gl.TEXTURE_2D, outer_cube_texture);

	// U: specify the value of a uniform variable for the current program object(sending uniform); glUniformMatrix4fv() is used to send matrices as uniforms
	gl.uniform1i(samplerUniform, 0);		// tell OGL that you are sending uniform as one integer and give that to FS
                                            // uniform_texture0_sampler: Specifies the location of the uniform variable to be modified.
                                            // 0: Specifies the new values to be used for the specified uniform variable; 0 means unit 0

    // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
    gl.bindVertexArray(vao_outer_cube);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

    // draw the necessary scene!
    gl.drawArrays(gl.TRIANGLE_FAN,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
        0,							// array position of your 9-member array to start with (imp in inter-leaved)
        4							// how many vertices to draw
    );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
            // Arrays since multiple primitives(P,C,N,T) can be drawn

    gl.drawArrays(gl.TRIANGLE_FAN,
        4,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        8,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        12,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        16,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        20,
        4
    );
    
    // unbind texture
    gl.bindTexture(gl.TEXTURE_2D, null);

    // unbind vao
    gl.bindVertexArray(null);

    // unuse program
    gl.useProgram(null);	// unbinding your OpenGL code with shader program object
                            // If program is null, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.                                   
    
    // animation loop
    Update();   // write here in Display() since we don't have an explicit game loop

    requestAnimationFrame(Display, canvas);     // game loop;  requests that the browser calls a specified function to update an animation before the next repaint
                                                // requestAnimationFrame: function pointer; canvas: what to draw
                                                // this is not recursion !
}

function Update()
{
	angle_cube = angle_cube - 1.0;
	if (angle_cube <= -360.0)
        angle_cube = 0.0;
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI/180.0);
}

function Uninitialize()     // actually no need to call this function as there is garbage collection
{
    // code
    if(outer_cube_texture)
    {
        gl.deleteTexture(outer_cube_texture);
        outer_cube_texture=null;
    }

    if (shaderProgramObject)
    {        
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }

        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }

        // delete the shader program
        gl.deleteProgram(shaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
        shaderProgramObject = null;
    }

    if (vbo_position_inner_cube)
    {
        gl.deleteBuffer(vbo_position_inner_cube);	// delete named buffer object                              
        vbo_position_inner_cube = null;
    }

    if (vao_inner_cube)
    {
        gl.deleteVertexArray(vao_inner_cube);		// delete vertex array object
        vao_inner_cube = null;
    }

    if (vbo_position_outer_cube)
    {
        gl.deleteBuffer(vbo_position_outer_cube);	// delete named buffer object                              
        vbo_position_outer_cube = null;
    }

    if (vbo_texture_outer_cube)
    {
        gl.deleteBuffer(vbo_texture_outer_cube);	// delete named buffer object                              
        vbo_texture_outer_cube = null;
    }

    if (vao_outer_cube)
    {
        gl.deleteVertexArray(vao_outer_cube);		// delete vertex array object
        vao_outer_cube = null;
    }
}

function ToggleFullScreen()     // handles browser-dependent code
{
    // code
    var fullscreen_element =
        document.fullscreenElement ||           // flags(pre-defined in browsers) will assign this value if it is present or will go further    // Opera
        document.webkitFullscreenElement ||     // Apple's Safari
        document.mozFullScreenElement ||        // Mozilla Firefox
        document.msFullscreenElement ||         // Microsoft's Internet Explorer or Edge
        null;                                   // if none of the above

    // if not fullscreen
    if (fullscreen_element == null)
    {
        if (canvas.requestFullscreen)       // requestFullscreen: function pointer      // Opera
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)       // Mozilla Firefox
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)    // Apple's Safari
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)        // Microsoft's Internet Explorer or Edge
            canvas.msRequestFullscreen();

        bFullscreen=true;
    }
    else    // if already fullscreen
    {
        if (document.exitFullscreen)        // calling on document, not on canvas
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();

        bFullscreen = false;
    }
}
