// global variables
var canvas = null;
var gl = null;  // WebGL context (context named as gl to have 'gl' in our code)
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros=      // when whole 'WebGLMacros' is const, all inside it are automatically const
{
    AMC_ATTRIBUTE_POSITION: 0,  // KVC (key-value coding): values assigned at compile time
	AMC_ATTRIBUTE_COLOR: 1,
	AMC_ATTRIBUTE_NORMAL: 2,
	AMC_ATTRIBUTE_TEXCOORD0: 3, // (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// shader objects
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_I, vao_N, vao_D, vao_A, vao_plane, vao_smoke, vao_A_band;		// vertex array object	(1 also ok since we unbind)
var vbo_position_I, vbo_color_I, vbo_position_N, vbo_color_N, vbo_position_D, vbo_color_D, vbo_position_A, vbo_color_A, vbo_position_plane, vbo_position_smoke, vbo_position_A_band, vbo_color_A_band;
var mvpUniform;		// mvp: model-view projection

var letter_x = 0.46;
var letter_start = -0.19, letter_end = 0.19;
var i1_x = -3.0;
var a_x = 2.50;
var n_y = 1.58;
var i2_y = -1.58;
var r_saffron = 0.0, g_saffron = 0.0, b_saffron = 0.0;
var r_green = 0.0, g_green = 0.0, b_green = 0.0;
var r = 1.42;
var r_saffron_smoke = 255.0, g_saffron_smoke = 153.0, b_saffron_smoke = 51.0;
var r_white_smoke = 1.0;
var r_green_smoke = 18.0, g_green_smoke = 136.0, b_green_smoke = 7.0;
var middle_plane_x = -letter_x * 5.4;
var angle_upper_plane = -90.0;
var angle_lower_plane = 90.0;
var angle = Math.PI;
var angle_lower = Math.PI;
var angle_right_upper = 3.0*Math.M_PI / 2.0 + 0.17*Math.PI;
var angle_right_lower = Math.PI / 2.0 - 0.17*Math.PI;

var bDoneI1 = false;
var bDoneA = false;
var bDoneN = false;
var bDoneI2 = false;
var bDoneR_saffron = false, bDoneG_saffron = false, bDoneB_saffron = false;
var bDoneR_green = false, bDoneG_green = false, bDoneB_green = false;
var bDoneD = false;
var bDoneMidddlePlane = false;
var bDoneUpperPlane = false;
var bDoneDetachPlanes = false;
var bDoneFadingSmokes = false;

// declaration of matrices
var modelViewMatrix;
var modelViewProjectionMatrix;
var translationMatrix;
var rotationMatrix;
var perspectiveProjectionMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array


// requestAnimationFrame and cancelAnimationFrame: ~glSwapBuffers() or glxSwapBuffers() or requestRender()
// to start animation : to have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =             // function pointer
window.requestAnimationFrame ||         // for browsers not having their own frame; // window: in-built variable
window.webkitRequestAnimationFrame ||   // Safari
window.mozRequestAnimationFrame ||      // Mozilla
window.oRequestAnimationFrame ||        // Opera
window.msRequestAnimationFrame;         // IE/Edge
// || null also ok

// to stop animation : to have cancelAnimationFrame() to be called "cross-browser" compatible
// not required in our code but required in offline rendering
var cancelAnimationFrame =              // function pointer 
window.cancelAnimationFrame ||         // for browsers not having their own frame
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||// Safari     // 2 for different versions of browser engines(?)
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || // Mozilla
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||   // Opera
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;   // IE/Edge

// onload function
function main()     // our entry-point function
{
    // get <canvas> element
    canvas = document.getElementById("AMC");    // document not declared means it is built-in; document: HTML page(representative of DOM), '.' means internally document is a class
                                                    // var: inferred type
    if (!canvas)    // lets you know whether or not there is support for HTML5
        console.log("Obtaining Canvas Failed\n");   // console: predefined
    else
        console.log("Obtaining Canvas Succeeded\n");

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);     // "keydown": window's event listener(not our name); keydown: our event listener; false: don't do event bubbling i.e. sending event to super and up(bubble capture)
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", Resize, false);

    // initialize WebGL
    Initialize();

    // start drawing here as warming-up
    Resize();
    Display();
}


// define your event handlers (callback functions)
function keyDown(event)
{
    // code
    switch (event.keyCode)
    {
        case 27:    // escape
            Uninitialize();      // don't call when using Firefox as it gives warnings when escape key is pressed
            // close our application's tab
            window.close();     // may not work in Firefox but works in Safari and Chrome
            break;

        case 70:    // 70: ASCII for F; 'F' or 'f'
            ToggleFullScreen();
            break;
    }
}

function mouseDown()
{
    // code
}

function Initialize()
{
    // code
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");   // webgl1 for mobile browser    // not working in IE and Edge?
    if (gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    // set viewport height and width
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader
    // define vertex shader object
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);	// creates a shader that is given as parameter	// ~ GL_VERTEX_SHADER

	// write vertex shader source code
    var vertexShaderSourceCode=
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec4 vColor;" +
    "uniform mat4 u_mvp_matrix;" +
    "out vec4 out_color;" +
    "void main(void)" +
    "{" +
    "gl_Position = u_mvp_matrix * vPosition;" +
    "out_color = vColor;" +
    "}";   // written in Graphics Library Shading/Shader Language (GLSL)
            // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES is 3.0 based
            // '\n' important and compulsory since shader file would have an enter after the version stmt 
            // in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
            // uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
            // gl_Position: in-built variable of shader

    // give above source code to the vertex shader object
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

    // compile the vertex shader
	gl.compileShader(vertexShaderObject);
    // error checking
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Vertex Shader Compilation Log : \n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // vertex converted to fragment now

    // define fragment shader object
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);	// creates a shader that is given as parameter	// ~ GL_FRAGMENT_SHADER
    
    // write vertex shader source code
    var fragmentShaderSourceCode=
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec4 out_color;" +
    "out vec4 FragColor;" +
    "void main(void)" +
    "{" +
    "FragColor = out_color;" +	
    "}";         // written in Graphics Library Shading/Shader Language (GLSL)
                // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES 3.0 based
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // out: output of shader;
                // precision highp float: give high precision to float. Use mediump for int(def). VS by def has highp for all. Max processing happens in FS than VS as it gives ultimate color.
                // precision needed only in ES as there are many math units
                // vec4: function/macro/constructor (of vmath?)
                // FragColor = vec4(1.0,1.0,0.0,1.0): will give yellow color to fragment; don't write 'f' in shaders (if class)

    // give above source code to the fragment shader object
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

    // compile the fragment shader
	gl.compileShader(fragmentShaderObject);
    // error checking
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Fragment Shader Compilation Log : \n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // create shader program object 
	shaderProgramObject = gl.createProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject, vertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject, fragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding of shaderProgramObject to vertex shader attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	gl.bindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
		WebGLMacros.AMC_ATTRIBUTE_POSITION,		// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
        // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition
       
    gl.bindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
        WebGLMacros.AMC_ATTRIBUTE_COLOR,		// the index of the generic vertex attribute to be bound.
        "vColor"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
    );	

    // link the shader program to your program
	gl.linkProgram(shaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
    // error checking (eg. of link error- version incompatibility of shaders)
    if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Shader Program Link Log : \n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // post-linking retrieving uniform locations (uniforms are global to shaders)
	mvpUniform = gl.getUniformLocation(shaderProgramObject, "u_mvp_matrix");	// preparation of data transfer from CPU to GPU (binding)
                                                                                // u_mvp_matrix: of GPU; mvpUniform: of CPU
                                                                                // telling it to take location of uniform u_mvp_matrix and give in mvpUniform

    // variable declarations
    var half_letter_ht = 0.5;
                                                                                
    // fill triangle vertices in array (this was in Display() in FFP)
    var I_position = new Float32Array([ 
        letter_start + 0.02, half_letter_ht, 0.0,
		letter_end - 0.02, half_letter_ht, 0.0,
		0.0, half_letter_ht, 0.0,
		0.0, -half_letter_ht, 0.0,
		letter_start + 0.02, -half_letter_ht, 0.0,
		letter_end - 0.02, -half_letter_ht, 0.0
    ]);	// Float32Array: their type(constructor) (of type ArrayBufferView)
        // Float32Array(i.e. ArrayBufferView) required in gl.bufferData()
        // since written in Initialize() only, coords will not get set everytime in Display(). Hence, fast speed.

    var I_color = new Float32Array([ 
        1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375,	 // green
		0.0703125, 0.53125, 0.02734375,	 // green
		0.0703125, 0.53125, 0.02734375   // green
    ]);                                            

    var N_position = new Float32Array([ 
        letter_start, half_letter_ht + 0.015, 0.0,
		letter_start, -half_letter_ht - 0.015, 0.0,
		letter_start, half_letter_ht + 0.015, 0.0,
		letter_end, -half_letter_ht - 0.015, 0.0,
		letter_end, -half_letter_ht - 0.015, 0.0,
		letter_end, half_letter_ht + 0.015, 0.0
    ]);	

    var N_color = new Float32Array([ 
        1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375 ,  // green
		1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375,   // green
		0.0703125, 0.53125, 0.02734375,   // green
		1.0, 0.59765625, 0.19921875,	 // saffron
    ]);                                            

    var D_position = new Float32Array([ 
        letter_start + 0.05, half_letter_ht, 0.0,
		letter_start + 0.05, -half_letter_ht, 0.0,
		letter_start, half_letter_ht, 0.0,
		letter_end + 0.0135, half_letter_ht, 0.0,
		letter_end, half_letter_ht, 0.0,
		letter_end, -half_letter_ht, 0.0,
		letter_end + 0.013, -half_letter_ht, 0.0,
		letter_start, -half_letter_ht, 0.0
    ]);	

    var A_position = new Float32Array([ 
        letter_start, -half_letter_ht - 0.015, 0.0,
		0.0, half_letter_ht + 0.015, 0.0,
		0.0, half_letter_ht + 0.015, 0.0,
		letter_end, -half_letter_ht - 0.015, 0.0
    ]);	
    
    var A_color = new Float32Array([ 
        0.0703125, 0.53125, 0.02734375,	 // green
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375	 // green
    ]);                                            

    var A_band_position = new Float32Array([ 
        letter_start / 2.0, 0.0166, 0.0,
		letter_end / 2.0, 0.0166, 0.0,
		letter_start / 2.0, 0.0, 0.0,
		letter_end / 2.0, 0.0, 0.0,
		letter_start / 2.0, -0.0173, 0.0,
		letter_end / 2.0, -0.0173, 0.0
    ]);	

    var A_band_color = new Float32Array([ 
        1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 1.0, 1.0,					// white
		1.0, 1.0, 1.0,					// white
		0.0703125, 0.53125, 0.02734375,	 // green
		0.0703125, 0.53125, 0.02734375,	 // green
    ]);                                            

    var plane_position = new Float32Array([ 
        // middle quad
		0.25 + 0.1, 0.05, 0.0,
		0.0 + 0.1, 0.05, 0.0,
		0.0 + 0.1, -0.05, 0.0,
		0.25 + 0.1, -0.05, 0.0,
		// upper wing
		0.0 + 0.1, 0.05, 0.0,
		0.1 + 0.1, 0.05, 0.0,
		0.05 + 0.1, 0.15, 0.0,
		0.0 + 0.1, 0.15, 0.0,
		// lower wing
		0.1 + 0.1, -0.05, 0.0,
		0.0 + 0.1, -0.05, 0.0,
		0.0 + 0.1, -0.15, 0.0,
		0.05 + 0.1, -0.15, 0.0,
		// back
		0.0 + 0.1, 0.05, 0.0,
		-0.1 + 0.1, 0.08, 0.0,
		-0.1 + 0.1, -0.08, 0.0,
		0.0 + 0.1, -0.05, 0.0,
		// front triangle
		0.25 + 0.1, 0.05, 0.0,
		0.25 + 0.1, -0.05, 0.0,
		0.30 + 0.1, 0.00, 0.0,
		// I
		-0.01 + 0.1, 0.03, 0.0,
		0.03 + 0.1, 0.03, 0.0,
		0.01 + 0.1, 0.03, 0.0,
		0.01 + 0.1, -0.03, 0.0,
		-0.01 + 0.1, -0.03, 0.0,
		0.03 + 0.1, -0.03, 0.0,
		// A
		0.05 + 0.1, -0.03, 0.0,
		0.07 + 0.1, 0.03, 0.0,
		0.07 + 0.1, 0.03, 0.0,
		0.09 + 0.1, -0.03, 0.0,
		(0.05 + 0.07) / 2.0 + 0.1, 0.0, 0.0,
		(0.07 + 0.09) / 2.0 + 0.1, 0.0, 0.0,
		// F
		0.12 + 0.1, -0.03, 0.0,
		0.12 + 0.1, 0.03, 0.0,
		0.12 + 0.1, 0.03, 0.0,
		0.16 + 0.1, 0.03, 0.0,
		0.12 + 0.1, 0.0, 0.0,
		0.15 + 0.1, 0.0, 0.0
    ]);	
               
    // 9 lines
    // I
    // create vao(id) (vao is shape-wise)
    vao_I = gl.createVertexArray();		// generate vertex array object name; createVertexArray(): different function
                                        // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

    gl.bindVertexArray(vao_I);		// bind a vertex array object

    // I position
    // create vbo (vbo is attribute-wise)
    vbo_position_I = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_I);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        I_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    // I color
    // create vbo (vbo is attribute-wise)
    vbo_color_I = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_I);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        I_color,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vColor 
                                           
    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    gl.bindVertexArray(null);		// unbind vao

    //=====================================================================================================================================================

    // N
    // create vao(id) (vao is shape-wise)
    vao_N = gl.createVertexArray();		// generate vertex array object name; createVertexArray(): different function
                                        // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

    gl.bindVertexArray(vao_N);		// bind a vertex array object

    // N position
    // create vbo (vbo is attribute-wise)
    vbo_position_N = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_N);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        N_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    // N color
    // create vbo (vbo is attribute-wise)
    vbo_color_N = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_N);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        N_color,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vColor 
                                           
    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    gl.bindVertexArray(null);		// unbind vao

    //=====================================================================================================================================================

    // D
    // create vao(id) (vao is shape-wise)
    vao_D = gl.createVertexArray();		// generate vertex array object name; createVertexArray(): different function
                                        // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

    gl.bindVertexArray(vao_D);		// bind a vertex array object

    // D position
    // create vbo (vbo is attribute-wise)
    vbo_position_D = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_D);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        D_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    // D color
    // create vbo (vbo is attribute-wise)
    vbo_color_D = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_D);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        0,				                // actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vColor 
                                           
    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    gl.bindVertexArray(null);		// unbind vao

    //=====================================================================================================================================================

    // A
    // create vao(id) (vao is shape-wise)
    vao_A = gl.createVertexArray();		// generate vertex array object name; createVertexArray(): different function
                                        // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

    gl.bindVertexArray(vao_A);		// bind a vertex array object

    // A position
    // create vbo (vbo is attribute-wise)
    vbo_position_A = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_A);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        A_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    // A color
    // create vbo (vbo is attribute-wise)
    vbo_color_A = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_A);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        A_color,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vColor 
                                           
    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    gl.bindVertexArray(null);		// unbind vao

    //=====================================================================================================================================================

    // A band
    // create vao(id) (vao is shape-wise)
    vao_A_band = gl.createVertexArray();		// generate vertex array object name; createVertexArray(): different function
                                        // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

    gl.bindVertexArray(vao_A_band);		// bind a vertex array object

    // A band  position
    // create vbo (vbo is attribute-wise)
    vbo_position_A_band = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_A_band);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        A_band_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    // A band color
    // create vbo (vbo is attribute-wise)
    vbo_color_A_band = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_A_band);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        vbo_color_A_band,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vColor 
                                           
    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    gl.bindVertexArray(null);		// unbind vao

    //=====================================================================================================================================================

    // plane
    // create vao(id) (vao is shape-wise)
    vao_plane = gl.createVertexArray();		// generate vertex array object name; createVertexArray(): different function
                                        // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

    gl.bindVertexArray(vao_plane);		// bind a vertex array object

    // plane position
    // create vbo (vbo is attribute-wise)
    vbo_position_plane = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_plane);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        plane_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    gl.bindVertexArray(null);		// unbind vao

    //=====================================================================================================================================================

    // smoke
    // create vao(id) (vao is shape-wise)
    vao_smoke = gl.createVertexArray();		// generate vertex array object name; createVertexArray(): different function
                                        // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

    gl.bindVertexArray(vao_smoke);		// bind a vertex array object

    // smoke position
    // create vbo (vbo is attribute-wise)
    vbo_position_smoke = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_smoke);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        0,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        0,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    gl.bindVertexArray(null);		// unbind vao

    gl.clearDepth(1.0);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	gl.enable(gl.DEPTH_TEST);	// to compare depth values of objects

	gl.depthFunc(gl.LEQUAL);	// specifies the value used for depth-buffer comparisons
                            // Passes if the incoming z value is less than or equal to the stored z value.
                            
    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // initialize projection matrix
    perspectiveProjectionMatrix = mat4.create();	// creates the matrix in identity format(diagonals 1); only does identity if matrix already present (?)
    modelViewMatrix=mat4.create();  // creates the matrix in identity format(diagonals 1); only does identity if matrix already present (?)
    modelViewProjectionMatrix=mat4.create();

    // no warm-up call
}

function Resize()
{
    // code
    // set viewport
    if (bFullscreen == true)    // need to write this explicitly since there is no repaint() calling resize() here
    {
        canvas.width = window.innerWidth;   // inner property of fullscreen
        canvas.height = window.innerHeight; // ~code of WM_SIZE in Windows
    }
    else
    {
        canvas.width = canvas_original_width;   // taken in main()
        canvas.height = canvas_original_height;
    }

    if(canvas.height==0)
    {
        canvas.height=1;
    }

    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);

    // perspective projection
    mat4.perspective(perspectiveProjectionMatrix, degToRad(45.0), parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);    // parameters:
                                                                                                                            // o/p matrix
                                                                                                                            // fovy- The field of view angle, in radians, in the y - direction.
                                                                                                                            // aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
                                                                                                                            // zNear- The distance from the viewer to the near clipping plane(always positive).
                                                                                                                            // zFar- The distance from the viewer to the far clipping plane(always positive).
}

function Display()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT);

    // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	gl.useProgram(shaderProgramObject);	// binding your OpenGL code with shader program object
                                        // Specifies the handle of the program object whose executables are to be used as part of current rendering state.

    //glLineWidth(15.0);

    // I1
    DrawI1();

    // A
    if (bDoneI1 == true)
        DrawA();

    // N
    if (bDoneA == true)
        DrawN();

    // I2
    if (bDoneN == true)
        DrawI2();

    // D
    if (bDoneI2 == true)
        DrawD();

    // Attach planes
    if (bDoneD == true)
    {
        // upper
        if (bDoneUpperPlane == false)
        {
            DrawLeftUpperPlane();
        }
        DrawLeftUpperSmoke();

        // lower
        if (bDoneUpperPlane == false)
        {
            DrawLeftLowerPlane();
        }
        DrawLeftLowerSmoke();

        // middle
        DrawMiddlePlane();
        DrawSmokeMiddle();
    }

    // Detach planes
    if (bDoneMidddlePlane == true)
    {
        // upper
        if (bDoneDetachPlanes == false)
        {
            DrawRightUpperPlane();
        }
        DrawRightUpperSmoke();

        // lower
        if (bDoneDetachPlanes == false)
        {
            DrawRightLowerPlane();
        }
        DrawRightLowerSmoke();

        // middle
        DrawMiddlePlane();
        DrawSmokeMiddle();
    }

    // A's band
    if (bDoneDetachPlanes == true)
    {
        DrawABand();
    }

    // unuse program
    gl.useProgram(null);	// unbinding your OpenGL code with shader program object
                            // If program is null, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.                                   
    
    // animation loop
    Update();   // write here in Display() since we don't have an explicit game loop
    requestAnimationFrame(Display, canvas);     // game loop;  requests that the browser calls a specified function to update an animation before the next repaint
                                                // requestAnimationFrame: function pointer; canvas: what to draw
                                                // this is not recursion !
}

function DrawI1()
{
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [i1_x, 0.0, -3.0]);   // parameters: o/p matrix, the matrix to translate, vector to translate by

    // do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_I);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

	// unbind vao
	gl.bindVertexArray(null);
}

function DrawA()
{
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [a_x, 0.0, -3.0]);   // parameters: o/p matrix, the matrix to translate, vector to translate by

    // do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_A);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	gl.bindVertexArray(null);
}

function DrawN()
{
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [-letter_x, n_y, -3.0]);   // parameters: o/p matrix, the matrix to translate, vector to translate by

    // do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_N);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	gl.bindVertexArray(null);
}

function DrawI2()
{
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [letter_x, i2_y, -3.0]);   // parameters: o/p matrix, the matrix to translate, vector to translate by

    // do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_I);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	gl.bindVertexArray(null);
}

function DrawD()
{
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0,0.0,-3.0]);   // parameters: o/p matrix, the matrix to translate, vector to translate by

    // do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_D);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

    var DColor = new Float32Array([ 
        r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0,
		r_green / 255.0, g_green / 255.0, b_green / 255.0,
		r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0,
		r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0,
		r_saffron / 255.0, g_saffron / 255.0, b_saffron / 255.0,
		r_green / 255.0, g_green / 255.0, b_green / 255.0,
		r_green / 255.0, g_green / 255.0, b_green / 255.0,
		r_green / 255.0, g_green / 255.0, b_green / 255.0
    ]); 

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_color_D);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

													// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

													// fill attributes
	gl.bufferData(gl.ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		DColor,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		gl.DYNAMIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

			// unbind (LIFO)
	gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
										// to bind with next buffer

										// draw the necessary scene!
	gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		8						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	gl.bindVertexArray(null);
}

function DrawLeftUpperPlane()
{
	// code
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [-2.0*letter_x + letter_end, 1.22, -3.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by
    mat4.translate(modelViewMatrix, modelViewMatrix, [r*Math.cos(degToRad(angle)), r*Math.sin(degToRad(angle)), 0.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angle_upper_plane));   // degToRad(): our function

	// do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

																								// draw the necessary scene!
	DrawPlane();

	// unbind vao
	gl.bindVertexArray(null);
}

function DrawMiddlePlane()
{
	// code
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [middle_plane_x, 0.0, -3.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by

	// do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

																								// draw the necessary scene!
	DrawPlane();

	// unbind vao
	gl.bindVertexArray(null);
}

function DrawLeftLowerPlane()
{
	// code
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [-2.0*letter_x + letter_end, -1.22, -3.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by
    mat4.translate(modelViewMatrix, modelViewMatrix, [r*Math.cos(degToRad(angle_lower)), r*Math.sin(degToRad(angle_lower)), 0.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angle_lower_plane));   // degToRad(): our function

	// do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

																								// draw the necessary scene!
	DrawPlane();

	// unbind vao
	gl.bindVertexArray(null);
}

function DrawLeftUpperSmoke()
{
	// code
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [-2.0*letter_x + letter_end, 1.22, -3.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by

	// do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	//glPointSize(5.0);

	// saffron
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0, g_saffron_smoke / 255.0, b_saffron_smoke / 255.0);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_smoke);

	for (angle_smoke = Math.PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005)
	{
        var point_saffron = new Float32Array([ (r - 0.018)*Math.cos(degToRad(angle_smoke)), (r - 0.018)*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = Math.PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005)
	{
        var point_white = new Float32Array([ r*Math.cos(degToRad(angle_smoke)), r*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
            point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0, g_green_smoke / 255.0, b_green_smoke / 255.0);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = Math.PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005)
	{
        var point_green = new Float32Array([ (r + 0.018)*Math.cos(degToRad(angle_smoke)), (r + 0.018)*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
            point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// unbind vao
	gl.bindVertexArray(null);
}

function DrawSmokeMiddle()
{
	// code
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [0.0, 0.0, -3.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by

	// do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	//glPointSize(5.0);

	// saffron
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0, g_saffron_smoke / 255.0, b_saffron_smoke / 255.0);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_smoke);

    var line_saffron = new Float32Array([ -letter_x * 5.4, 0.0166, 0.0,
        middle_plane_x, 0.0166, 0.0 ]);

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
        line_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
        0,						// array position of your array to start with (imp in inter-leaved)
        2						// how many vertices to draw
    );
	
	// white	
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	var line_white = new Float32Array([-letter_x * 5.4, 0.0, 0.0,
        middle_plane_x, 0.0, 0.0 ]);

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
        line_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
        0,						// array position of your array to start with (imp in inter-leaved)
        2						// how many vertices to draw
    );

	// green
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0, g_green_smoke / 255.0, b_green_smoke / 255.0);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	var line_green = new Float32Array([-letter_x * 5.4, -0.0173, 0.0,
        middle_plane_x, -0.0173, 0.0 ]);

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
        line_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
        0,						// array position of your array to start with (imp in inter-leaved)
        2						// how many vertices to draw
    );

	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// unbind vao
    gl.bindVertexArray(null);
    
    if (bDoneDetachPlanes == true)
	{
		//glLineWidth(15.0);

		DrawI1();
		DrawN();
		DrawD();
		DrawI2();
		DrawA();
	}
}

function DrawLeftLowerSmoke()
{
	// code
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [-2.0*letter_x + letter_end, -1.22, -3.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by

	// do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	//glPointSize(5.0);

	// saffron
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0, g_saffron_smoke / 255.0, b_saffron_smoke / 255.0);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_smoke);

	for (angle_smoke = Math.PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005)
	{
        var point_saffron = new Float32Array([ (r - 0.018)*Math.cos(degToRad(angle_smoke)), (r - 0.018)*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = Math.PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005)
	{
        var point_white = new Float32Array([ r*Math.cos(degToRad(angle_smoke)), r*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
            point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0, g_green_smoke / 255.0, b_green_smoke / 255.0);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = Math.PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005)
	{
        var point_green = new Float32Array([ (r + 0.018)*Math.cos(degToRad(angle_smoke)), (r + 0.018)*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
            point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// unbind vao
	gl.bindVertexArray(null);
}

function DrawABand()
{
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [letter_x*2.0, 0.0, -3.0]);   // parameters: o/p matrix, the matrix to translate, vector to translate by

    // do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_A_band);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	gl.bindVertexArray(null);
}

function DrawRightUpperPlane()
{
	// code
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [2.0*letter_x - letter_end, 1.22, -3.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by
    mat4.translate(modelViewMatrix, modelViewMatrix, [r*Math.cos(degToRad(angle_right_upper)), r*Math.sin(degToRad(angle_right_upper)), 0.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angle_upper_plane));   // degToRad(): our function

	// do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

																								// draw the necessary scene!
	DrawPlane();

	// unbind vao
	gl.bindVertexArray(null);
}

function DrawRightLowerPlane()
{
	// code
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [2.0*letter_x - letter_end, -1.22, -3.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by
    mat4.translate(modelViewMatrix, modelViewMatrix, [r*Math.cos(degToRad(angle_right_lower)), r*Math.sin(degToRad(angle_right_lower)), 0.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by
    mat4.rotateZ(modelViewMatrix, modelViewMatrix, degToRad(angle_lower_plane));   // degToRad(): our function

	// do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

																								// draw the necessary scene!
	DrawPlane();

	// unbind vao
	gl.bindVertexArray(null);
}

function DrawRightUpperSmoke()
{
	// code
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [2.0*letter_x - letter_end, 1.22, -3.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by

	// do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	//glPointSize(5.0);

	// saffron
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0, g_saffron_smoke / 255.0, b_saffron_smoke / 255.0);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_smoke);

	for (angle_smoke = 3.0*Math.PI / 2.0 + 0.17*Math.PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005)
	{
        var point_saffron = new Float32Array([ (r - 0.018)*Math.cos(degToRad(angle_smoke)), (r - 0.018)*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = 3.0*Math.PI / 2.0 + 0.17*Math.PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005)
	{
        var point_white = new Float32Array([ r*Math.cos(degToRad(angle_smoke)), r*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
            point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0, g_green_smoke / 255.0, b_green_smoke / 255.0);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = 3.0*Math.PI / 2.0 + 0.17*Math.PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005)
	{
        var point_green = new Float32Array([ (r + 0.018)*Math.cos(degToRad(angle_smoke)), (r + 0.018)*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
            point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// unbind vao
	gl.bindVertexArray(null);
}

function DrawRightLowerSmoke()
{
	// code
	mat4.identity(modelViewMatrix);  // creates the matrix in identity format(diagonals 1);
    mat4.identity(modelViewProjectionMatrix);

    // do necessary transformation (T,S,R)
    mat4.translate(modelViewMatrix, modelViewMatrix, [2.0*letter_x - letter_end, -1.22, -3.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by

	// do necessary matrix multiplication
    mat4.multiply(
        modelViewProjectionMatrix,        // output matrix
        perspectiveProjectionMatrix,
        modelViewMatrix               
    );  // sequence imp.

    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelViewProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	gl.bindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	//glPointSize(5.0);

	// saffron
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0, g_saffron_smoke / 255.0, b_saffron_smoke / 255.0);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	gl.bindBuffer(gl.ARRAY_BUFFER, vbo_position_smoke);

	for (angle_smoke = Math.PI / 2.0 - 0.17*Math.PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005)
	{
        var point_saffron = new Float32Array([ (r - 0.018)*Math.cos(degToRad(angle_smoke)), (r - 0.018)*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = Math.PI / 2.0 - 0.17*Math.PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005)
	{
        var point_white = new Float32Array([ r*Math.cos(degToRad(angle_smoke)), r*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
            point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0, g_green_smoke / 255.0, b_green_smoke / 255.0);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = Math.PI / 2.0 - 0.17*Math.PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005)
	{
        var point_green = new Float32Array([ (r + 0.018)*Math.cos(degToRad(angle_smoke)), (r + 0.018)*Math.sin(degToRad(angle_smoke)), 0.0 ]);

		// fill attributes
		gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
            point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			gl.DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		gl.drawArrays(gl.POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	gl.bindBuffer(gl.ARRAY_BUFFER, null);

	// unbind vao
	gl.bindVertexArray(null);
}

function DrawPlane()
{
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, 186.0 / 255.0, 226.0 / 255.0, 238.0 / 255.0);	// specify the value of a generic vertex attribute(for single color to object)

	// middle quad
	gl.drawArrays(gl.TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// upper wing
	gl.drawArrays(gl.TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		4,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// lower wing
	gl.drawArrays(gl.TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		8,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// back
	gl.drawArrays(gl.TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		12,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// front triangle
	gl.drawArrays(gl.TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		16,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// IAF
	//glLineWidth(3.0);
	gl.vertexAttrib3f(WebGLMacros.AMC_ATTRIBUTE_COLOR, 99.0 / 255.0, 99.0 / 255.0, 99.0 / 255.0);	// dark gray // specify the value of a generic vertex attribute(for single color to object)

	// I
	gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		19,						// array position of your array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);
	// A
	gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		25,						// array position of your array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);
	// F
	gl.drawArrays(gl.LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		31,						// array position of your array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);
}

function Update()
{
	if (bDoneI1 == false)
	{
		i1_x = i1_x + 0.0027;

		if (i1_x >= -letter_x * 2)
		{
			i1_x = -letter_x * 2.0;
			bDoneI1 = true;
		}
	}
	else if (bDoneA == false)
	{
		a_x = a_x - 0.0027;

		if (a_x <= letter_x * 2)
		{
			a_x = letter_x * 2.0;
			bDoneA = true;
		}
	}
	else if (bDoneN == false)
	{
		n_y = n_y - 0.0027;

		if (n_y <= 0.0)
		{
			bDoneN = true;
			n_y = 0.0;
		}
	}
	else if (bDoneI2 == false)
	{
		i2_y = i2_y + 0.0027;

		if (i2_y >= 0.0)
		{
			i2_y = 0.0;
			bDoneI2 = true;
		}
	}
	else if (bDoneD == false)
	{
		if (bDoneR_saffron == false)
		{
			r_saffron = r_saffron + 2.5;
			if (r_saffron >= 255.0)
			{
				r_saffron = 255.0;
				bDoneR_saffron = true;
			}
		}
		if (bDoneG_saffron == false)
		{
			g_saffron = g_saffron + 1.0;
			if (g_saffron >= 153.0)
			{
				g_saffron = 153.0;
				bDoneG_saffron = true;
			}
		}
		if (bDoneB_saffron == false)
		{
			b_saffron = b_saffron + 0.2;
			if (b_saffron >= 51.0)
			{
				b_saffron = 51.0;
				bDoneB_saffron = true;
			}
		}

		// green
		if (bDoneR_green == false)
		{
			r_green = r_green + 0.1;
			if (r_green >= 18.0)
			{
				r_green = 18.0;
				bDoneR_green = true;
			}
		}
		if (bDoneG_green == false)
		{
			g_green = g_green + 0.7;
			if (g_green >= 136.0)
			{
				g_green = 136.0;
				bDoneG_green = true;
			}
		}
		if (bDoneB_green == false)
		{
			b_green = b_green + 0.1;
			if (b_green >= 7.0)
			{
				b_green = 7.0;
				bDoneB_green = true;
			}
		}
		if (bDoneR_saffron == true && bDoneG_saffron == true && bDoneB_saffron == true && bDoneR_green == true && bDoneG_green == true && bDoneB_green == true)
			bDoneD = true;
	}
	else if (bDoneUpperPlane == false)
	{
		middle_plane_x = middle_plane_x + 0.0049;


		if (angle_upper_plane <= 0.0)
			angle_upper_plane = angle_upper_plane + 0.45;

		if (angle_lower_plane >= 0.0)
			angle_lower_plane = angle_lower_plane - 0.45;

		if (r*Math.sin(angle) <= -1.22)
		{
			bDoneUpperPlane = true;
			//fprintf_s(gpFile, "\nr*cos(angle)=%f", r*cos(angle));
			//fprintf_s(gpFile, "\nr*sin(angle)=%f", r*sin(angle));
			angle_upper_plane = 0.0;
			angle_lower_plane = 0.0;
		}

		angle = angle + 0.005;
		angle_lower = angle_lower - 0.005;
	}

	else if (bDoneMidddlePlane == false)
	{
		middle_plane_x = middle_plane_x + 0.0049;
		if (middle_plane_x >= 2.0*letter_x - letter_end + 0.724918)
			bDoneMidddlePlane = true;
	}

	else if (bDoneDetachPlanes == false)
	{
		middle_plane_x = middle_plane_x + 0.0049;

		angle_right_upper = angle_right_upper + 0.005;
		angle_right_lower = angle_right_lower - 0.005;

		if (angle_upper_plane <= 90.0)
			angle_upper_plane = angle_upper_plane + 0.47;

		if (angle_lower_plane >= -90.0)
			angle_lower_plane = angle_lower_plane - 0.47;

		if (middle_plane_x >= letter_x * 6.0)
			bDoneDetachPlanes = true;
	}
	else if (bDoneFadingSmokes == false)
	{
		if (r_saffron_smoke > 0.0)
			r_saffron_smoke = r_saffron_smoke - 2.5;
		if (g_saffron_smoke > 0.0)
			g_saffron_smoke = g_saffron_smoke - 1.0;
		if (b_saffron_smoke > 0.0)
			b_saffron_smoke = b_saffron_smoke - 0.2;

		if (r_white_smoke > 0.0)
			r_white_smoke = r_white_smoke - 0.03;

		if (r_green_smoke > 0.0)
			r_green_smoke = r_green_smoke - 0.1;
		if (g_green_smoke > 0.0)
			g_green_smoke = g_green_smoke - 0.7;
		if (b_green_smoke > 0.0)
			b_green_smoke = b_green_smoke - 0.1;

		if (r_saffron_smoke <= 0 && g_saffron_smoke <= 0 && b_saffron_smoke <= 0 && r_white_smoke && r_green_smoke <= 0 && g_green_smoke <= 0 && b_green_smoke <= 0)
			bDoneFadingSmokes = true;
	}
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI/180.0);    // Math is a built-in object that has properties and methods for mathematical constants and functions.
                                        // Math.PI since all properties(e.g. PI) and methods(e.g. sin()) of Math are static
}

function Uninitialize()     // actually no need to call this function as there is garbage collection
{
    // code
    if (shaderProgramObject)
    {        
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }

        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }

        // delete the shader program
        gl.deleteProgram(shaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
        shaderProgramObject = null;
    }

    if (vbo_color)
    {
        gl.deleteBuffer(vbo_color);	// delete named buffer object                              
        vbo_color = null;
    }

    if (vbo_position)
    {
        gl.deleteBuffer(vbo_position);	// delete named buffer object                              
        vbo_position = null;
    }

    if (vao)
    {
        gl.deleteVertexArray(vao);		// delete vertex array object
        vao = null;
    }
}

function ToggleFullScreen()     // handles browser-dependent code
{
    // code
    var fullscreen_element =
        document.fullscreenElement ||           // flags(pre-defined in browsers) will assign this value if it is present or will go further    // Opera
        document.webkitFullscreenElement ||     // Apple's Safari
        document.mozFullScreenElement ||        // Mozilla Firefox
        document.msFullscreenElement ||         // Microsoft's Internet Explorer or Edge
        null;                                   // if none of the above

    // if not fullscreen
    if (fullscreen_element == null)
    {
        if (canvas.requestFullscreen)       // requestFullscreen: function pointer      // Opera
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)       // Mozilla Firefox
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)    // Apple's Safari
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)        // Microsoft's Internet Explorer or Edge
            canvas.msRequestFullscreen();

        bFullscreen=true;
    }
    else    // if already fullscreen
    {
        if (document.exitFullscreen)        // calling on document, not on canvas
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();

        bFullscreen = false;
    }
}
