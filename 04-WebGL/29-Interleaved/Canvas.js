0   // global variables
var canvas = null;
var gl = null;  // WebGL context (context named as gl to have 'gl' in our code)
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros=      // when whole 'WebGLMacros' is const, all inside it are automatically const
{
    AMC_ATTRIBUTE_POSITION: 0,  // KVC (key-value coding): values assigned at compile time
	AMC_ATTRIBUTE_COLOR: 1,
	AMC_ATTRIBUTE_NORMAL: 2,
	AMC_ATTRIBUTE_TEXCOORD0: 3, // (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// shader objects
var vertexShaderObject;
var fragmentShaderObject;
var shaderProgramObject;

var vao_cube;
var vbo_pctn_cube;
var mUniform;
var vUniform;
var pUniform;
var laUniform;	// intensity of ambient light
var ldUniform;	// intensity of diffused light
var lsUniform;	// intensity of specular light
var lightPositionUniform;
var kaUniform;	// coefficient of material's ambient reflectivity
var kdUniform;	// coefficient of material's diffused reflectivity
var ksUniform;	// coefficient of material's specular reflectivity 
var materialShininessUniform; // material shininessvar samplerUniform;
var perspectiveProjectionMatrix;
var texture=0;
var angle_cube = 0.0;

var light_ambient=[0.0,0.0,0.0];
var light_diffuse=[1.0,1.0,1.0];
var light_specular=[1.0,1.0,1.0];
var light_position=[ 0.0,0.0,0.0,1.0 ];	// x,y,z,w; 1.0 is for non-directional light

var material_ambient=[0.0,0.0,0.0]; // error for 3fv if 4th member written
var material_diffuse=[1.0,1.0,1.0];	// albedo diffuse
var material_specular=[1.0,1.0,1.0];
var material_shininess=128.0;       // 128.0

// requestAnimationFrame and cancelAnimationFrame: ~glSwapBuffers() or glxSwapBuffers() or requestRender()
// to start animation : to have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =             // function pointer
window.requestAnimationFrame ||         // for browsers not having their own frame; // window: in-built variable
window.webkitRequestAnimationFrame ||   // Safari
window.mozRequestAnimationFrame ||      // Mozilla
window.oRequestAnimationFrame ||        // Opera
window.msRequestAnimationFrame;         // IE/Edge
// || null also ok

// to stop animation : to have cancelAnimationFrame() to be called "cross-browser" compatible
// not required in our code but required in offline rendering
var cancelAnimationFrame =              // function pointer 
window.cancelAnimationFrame ||         // for browsers not having their own frame
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||// Safari     // 2 for different versions of browser engines(?)
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || // Mozilla
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||   // Opera
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;   // IE/Edge

// onload function
function main()     // our entry-point function
{
    // get <canvas> element
    canvas = document.getElementById("AMC");    // document not declared means it is built-in; document: HTML page(representative of DOM), '.' means internally document is a class
                                                    // var: inferred type
    if (!canvas)    // lets you know whether or not there is support for HTML5
        console.log("Obtaining Canvas Failed\n");   // console: predefined
    else
        console.log("Obtaining Canvas Succeeded\n");

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);     // "keydown": window's event listener(not our name); keydown: our event listener; false: don't do event bubbling i.e. sending event to super and up(bubble capture)
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", Resize, false);

    // initialize WebGL
    Initialize();

    // start drawing here as warming-up
    Resize();
    Display();
}


// define your event handlers (callback functions)
function keyDown(event)
{
    // code
    switch (event.keyCode)
    {
        case 27:    // escape
            // Uninitialize();     // don't call when using Firefox as it gives warnings when escape key is pressed
            // close our application's tab
            window.close();     // may not work in Firefox but works in Safari and Chrome
            break;

        case 70:    // 70: ASCII for F; 'F' or 'f'
            ToggleFullScreen();
            break;
    }
}

function mouseDown()
{
    // code
}

function Initialize()
{
    // code
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");   // webgl1 for mobile browser    // not working in IE and Edge?
    if (gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    // set viewport height and width
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader
    // define vertex shader object
	vertexShaderObject = gl.createShader(gl.VERTEX_SHADER);	// creates a shader that is given as parameter	// ~ GL_VERTEX_SHADER

	// write vertex shader source code
    var vertexShaderSourceCode=
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec4 vColor;" +
    "in vec2 vTexCoord;" +
    "in vec3 vNormal;" +
    "out vec4 out_color;" +
    "out vec2 out_texCoord;" +
    "out vec3 t_norm;" +
    "out vec3 light_direction;" +
    "out vec3 viewer_vector;" +
    "uniform mat4 u_m_matrix;" +
    "uniform mat4 u_v_matrix;" +
    "uniform mat4 u_p_matrix;" +
    "uniform vec4 u_light_position;" +
    "void main(void)" +
    "{" +
    "vec4 eye_coordinates =  u_v_matrix * u_m_matrix * vPosition;" +
    "t_norm = mat3(u_v_matrix * u_m_matrix) * vNormal;" +
    "light_direction = vec3(u_light_position - eye_coordinates);" +
    "viewer_vector = vec3(-eye_coordinates.xyz);" +
    "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
    "out_color = vColor;" +
    "out_texCoord = vTexCoord;" +
    "}";     // written in Graphics Library Shading/Shader Language (GLSL)
            // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES is 3.0 based
            // '\n' important and compulsory since shader file would have an enter after the version stmt 
            // in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
            // uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
            // gl_Position: in-built variable of shader

    // give above source code to the vertex shader object
    gl.shaderSource(vertexShaderObject,vertexShaderSourceCode);

    // compile the vertex shader
	gl.compileShader(vertexShaderObject);
    // error checking
    if(gl.getShaderParameter(vertexShaderObject,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(vertexShaderObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Vertex Shader Compilation Log : +n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // vertex converted to fragment now

    // define fragment shader object
    fragmentShaderObject = gl.createShader(gl.FRAGMENT_SHADER);	// creates a shader that is given as parameter	// ~ GL_FRAGMENT_SHADER
    
    // write vertex shader source code
    var fragmentShaderSourceCode=
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec4 out_color;" +
    "in vec2 out_texCoord;" +
    "in vec3 t_norm;" +
    "in vec3 light_direction;" +
    "in vec3 viewer_vector;" +
    "out vec4 fragColor;" +
    "uniform vec3 u_la;" +
    "uniform vec3 u_ld;" +
    "uniform vec3 u_ls;" +
    "uniform vec3 u_ka;" +
    "uniform vec3 u_kd;" +
    "uniform vec3 u_ks;" +
    "uniform float u_material_shininess;" +
    "uniform sampler2D u_sampler;" +
    "void main(void)" +
    "{" +
    "vec3 normalized_t_norm = normalize(t_norm);" +
    "vec3 normalized_light_direction = normalize(light_direction);" +
    "vec3 normalized_viewer_vector = normalize(viewer_vector);" +
    "vec3 reflection_vector = reflect(-normalized_light_direction,normalized_t_norm);" +
    "float tn_dot_lightDirection = max(dot(normalized_light_direction,normalized_t_norm),0.0);" +
    "vec3 ambient = u_la * u_ka;" +
    "vec3 diffuse = u_ld * u_kd * tn_dot_lightDirection;" +
    "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_viewer_vector),0.0),u_material_shininess);" +
    "vec3 phong_ads_light = ambient + diffuse + specular;" +
    "vec4 tex = texture(u_sampler,out_texCoord);" +
    "fragColor = out_color * tex * vec4(phong_ads_light,1.0);" +
    "}";         // written in Graphics Library Shading/Shader Language (GLSL)
                // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES is 3.0 based
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // out: output of shader;
                // precision highp float: give high precision to float. Use mediump for int(def). VS by def has highp for all. Max processing happens in FS than VS as it gives ultimate color.
                // precision needed only in ES as there are many math units
                // vec4: function/macro/constructor (of vmath?)
                // FragColor = texture(u_sampler,out_texCoord): this proves that texture is a type of color.
				// texture() is a function of GLSL. u_sampler: Specifies the sampler to which the texture is bound. Texture sampling is the act of retrieving data from a texture. out_texCoord: Specifies the texture coordinates at which texture will be sampled.
				// u_sampler colors out_texCoord and in-turn fragment gets colored

    // give above source code to the fragment shader object
    gl.shaderSource(fragmentShaderObject,fragmentShaderSourceCode);

    // compile the fragment shader
	gl.compileShader(fragmentShaderObject);
    // error checking
    if(gl.getShaderParameter(fragmentShaderObject,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Fragment Shader Compilation Log : +n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // create shader program object 
	shaderProgramObject = gl.createProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject, vertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject, fragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding of shaderProgramObject to vertex shader attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	gl.bindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
		WebGLMacros.AMC_ATTRIBUTE_POSITION,		// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
        // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition
       
    gl.bindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
        WebGLMacros.AMC_ATTRIBUTE_COLOR,		// the index of the generic vertex attribute to be bound.
        "vColor"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
    );	

    gl.bindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
        WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0,		// the index of the generic vertex attribute to be bound.
        "vTexCoord"						// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
    );	

    gl.bindAttribLocation(shaderProgramObject,	// the handle of the program object in which the association is to be made.
        WebGLMacros.AMC_ATTRIBUTE_NORMAL,		// the index of the generic vertex attribute to be bound.
        "vNormal"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
    );	

    // link the shader program to your program
	gl.linkProgram(shaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
    // error checking (eg. of link error- version incompatibility of shaders)
    if(!gl.getProgramParameter(shaderProgramObject,gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Shader Program Link Log : +n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // post-linking retrieving uniform locations (uniforms are global to shaders)
	mUniform = gl.getUniformLocation(shaderProgramObject, "u_m_matrix");	// preparation of data transfer from CPU to GPU (binding)
                                                                                // u_mvp_matrix: of GPU; mvpUniform: of CPU
                                                                                // telling it to take location of uniform u_mvp_matrix and give in mvpUniform
    vUniform = gl.getUniformLocation(shaderProgramObject, "u_v_matrix");                                                                            
    pUniform = gl.getUniformLocation(shaderProgramObject, "u_p_matrix");
    laUniform = gl.getUniformLocation(shaderProgramObject, "u_la");
    ldUniform = gl.getUniformLocation(shaderProgramObject, "u_ld");
    lsUniform = gl.getUniformLocation(shaderProgramObject, "u_ls");
    lightPositionUniform = gl.getUniformLocation(shaderProgramObject, "u_light_position");
    kaUniform = gl.getUniformLocation(shaderProgramObject, "u_ka");
    kdUniform = gl.getUniformLocation(shaderProgramObject, "u_kd");
    ksUniform = gl.getUniformLocation(shaderProgramObject, "u_ks");
    materialShininessUniform = gl.getUniformLocation(shaderProgramObject, "u_material_shininess");
    samplerUniform = gl.getUniformLocation(shaderProgramObject, "u_sampler");
                                                                            
    // fill triangle vertices in array (this was in Display() in FFP)
    var cubePCTN = new Float32Array([ 
// top
1.0, 1.0, -1.0,		// P
1.0,0.0,0.0,		// C
1.0, 1.0,			// T
0.0, 1.0, 0.0,		// N

-1.0, 1.0, -1.0,	// P
1.0,0.0,0.0,		// C
0.0, 1.0,			// T
0.0, 1.0, 0.0,		// N

-1.0, 1.0, 1.0,
1.0,0.0,0.0,
0.0, 0.0,
0.0, 1.0, 0.0,

1.0, 1.0, 1.0,
1.0,0.0,0.0,
1.0, 0.0,
0.0, 1.0, 0.0,

// bottom
1.0, -1.0, -1.0,
0.0,1.0,0.0,
0.0, 0.0,
0.0, -1.0, 0.0,

-1.0, -1.0, -1.0,
0.0,1.0,0.0,
1.0, 0.0,
0.0, -1.0, 0.0,

-1.0, -1.0, 1.0,
0.0,1.0,0.0,
1.0, 1.0,
0.0, -1.0, 0.0,

1.0, -1.0, 1.0,
0.0,1.0,0.0,
0.0, 1.0,
0.0, -1.0, 0.0,

// front
1.0, 1.0, 1.0,
0.0,0.0,1.0,
1.0, 1.0,
0.0, 0.0, 1.0,

-1.0, 1.0, 1.0,
0.0,0.0,1.0,
0.0, 1.0,
0.0, 0.0, 1.0,

-1.0, -1.0, 1.0,
0.0,0.0,1.0,
0.0, 0.0,
0.0, 0.0, 1.0,

1.0, -1.0, 1.0,
0.0,0.0,1.0,
1.0, 0.0,
0.0, 0.0, 1.0,

// back
-1.0, 1.0, -1.0,
0.0,1.0,1.0,
1.0, 1.0,
0.0, 0.0, -1.0,

1.0, 1.0, -1.0,
0.0,1.0,1.0,
0.0, 1.0,
0.0, 0.0, -1.0,

1.0, -1.0, -1.0,
0.0,1.0,1.0,
0.0, 0.0,
0.0, 0.0, -1.0,

-1.0, -1.0, -1.0,
0.0,1.0,1.0,
0.0, 0.0,
0.0, 0.0, -1.0,

// right
1.0, 1.0, -1.0,
1.0,0.0,1.0,
1.0, 1.0,
1.0, 0.0, 0.0,

1.0, 1.0, 1.0,
1.0,0.0,1.0,
0.0, 1.0,
1.0, 0.0, 0.0,

1.0, -1.0, 1.0,
1.0,0.0,1.0,
0.0, 0.0,
1.0, 0.0, 0.0,

1.0, -1.0, -1.0,
1.0,0.0,1.0,
1.0, 0.0,
1.0, 0.0, 0.0,

// left
-1.0, 1.0, 1.0,
1.0, 1.0, 0.0,
1.0, 1.0,
-1.0, 0.0, 0.0,

-1.0, 1.0, -1.0,
1.0, 1.0, 0.0,
0.0, 1.0,
-1.0, 0.0, 0.0,

-1.0, -1.0, -1.0,
1.0, 1.0, 0.0,
0.0, 0.0,
-1.0, 0.0, 0.0,

-1.0, -1.0, 1.0,
1.0, 1.0, 0.0,
1.0, 0.0,
-1.0, 0.0, 0.0
    ]);	// Float32Array: their type(constructor)
        // since written in Initialize() only, coords will not get set everytime in Display(). Hence, fast speed.
        
    // 9 lines
    // create vao(id) (vao is shape-wise)
    vao_cube = gl.createVertexArray();		// generate vertex array object name; createVertexArray(): different function
                                                // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

    gl.bindVertexArray(vao_cube);		// bind a vertex array object

    // postion
    // create vbo (vbo is attribute-wise)
    vbo_pctn_cube = gl.createBuffer();		// generate buffer object name; createBuffer():  different function

    gl.bindBuffer(gl.ARRAY_BUFFER, vbo_pctn_cube);		// creates vbo <-> gl.ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
                                                // gl.ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
                                                // vbo: bind this (the name of a buffer object.)

    // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

    // fill attributes
    gl.bufferData(gl.ARRAY_BUFFER,		// target buffer object (give data to gl.ARRAY_BUFFER)
        cubePCTN,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
        gl.STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
    );		// creates a new data store for the buffer object currently bound to target(1st para.)

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        11*4,											        // the byte offset between consecutive generic vertex attributes; 0=no stride
        0										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vPosition

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; x,y,z for position
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        11*4,											        // the byte offset between consecutive generic vertex attributes; 0=no stride
        3*4										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                                                                                // enables vColor 

    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        2,											            // number of components per generic vertex attribute; s,t for texcoords
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        11*4,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        6*4										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_TEXCOORD0);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                    // enables vColor 
                                           
    gl.vertexAttribPointer(WebGLMacros.AMC_ATTRIBUTE_NORMAL,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
        3,											            // number of components per generic vertex attribute; s,t for texcoords
        gl.FLOAT,									            // data type of each component in the array
        false,									                // is data normalised
        11*4,											            // the byte offset between consecutive generic vertex attributes; 0=no stride
        8*4										                // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
    );		// define an array of generic vertex attribute data

    gl.enableVertexAttribArray(WebGLMacros.AMC_ATTRIBUTE_NORMAL);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
                                                                                                                                // enables vColor                                                                 
    // unbind (LIFO)
    gl.bindBuffer(gl.ARRAY_BUFFER, null);	// 0= unbind; unbind vbo
                                            // to bind with next buffer

    gl.bindVertexArray(null);		// unbind vao
    
    // load texture
    texture=gl.createTexture();     // functions starting with 'create' in WebGL internally create data structures
    texture.image = new Image();      // Image: HTMLImageElement; browser specific class; '.' shows it is a DS
    texture.image.src="marble.png";  // source of image
    texture.image.onload = function () // Lambda(closure in JS): define and assign function at the same time and function as a variable; onload: when image loads;  ~loadTexture()
    {
        // bind texture to a target texture (type of texture)
		gl.bindTexture(gl.TEXTURE_2D, texture);	// binding the first texture(value) to a 2D array type usage
                                                                // Note: Texture targets become aliases for textures currently bound to them

        // set pixel storage mode
        gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);   // tell WebGL to invert your Y (like SOIL)           
        
        // set the state/parameters of the texture
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);     // NEAREST works on all browsers
        gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);     // NEAREST works on all browsers

        // fill the data and tell it how to use it
        gl.texImage2D(
            gl.TEXTURE_2D,          // where to fill data; the target texture
            0,                      // mipmap level
            gl.RGBA,                // internal image format
            gl.RGBA,                // texel format
            gl.UNSIGNED_BYTE,       // data type of the texel data(last para)
            texture.image   // actual data
        );

        // If you use gl.NEAREST, WebGL creates mipmaps, otherwise you have to call gl.generateMipmap();

        // unbind texture explicitly
		gl.bindTexture(gl.TEXTURE_2D, null);
    }

    gl.clearDepth(1.0);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	gl.enable(gl.DEPTH_TEST);	// to compare depth values of objects

	gl.depthFunc(gl.LEQUAL);	// specifies the value used for depth-buffer comparisons
                                // Passes if the incoming z value is less than or equal to the stored z value.

    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // initialize projection matrix
    perspectiveProjectionMatrix = mat4.create();	// creates the matrix in identity format(diagonals 1); only does identity if matrix already present (?)

    // no warm-up call
}

function Resize()
{
    // code
    // set viewport
    if (bFullscreen == true)    // need to write this explicitly since there is no repaint() calling resize() here
    {
        canvas.width = window.innerWidth;   // inner property of fullscreen
        canvas.height = window.innerHeight; // ~code of WM_SIZE in Windows
    }
    else
    {
        canvas.width = canvas_original_width;   // taken in main()
        canvas.height = canvas_original_height;
    }

    if(canvas.height==0)
    {
        canvas.height=1;
    }

    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);

    // perspective projection
    mat4.perspective(perspectiveProjectionMatrix, degToRad(45.0), parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);  // parameters:
                                                                                                                                    // o/p matrix
                                                                                                                                    // fovy- The field of view angle, in radians, in the y - direction.
                                                                                                                                    // aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
                                                                                                                                    // zNear- The distance from the viewer to the near clipping plane(always positive).
                                                                                                                                    // zFar- The distance from the viewer to the far clipping plane(always positive).
}

function Display()
{
    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	gl.useProgram(shaderProgramObject);	// binding your OpenGL code with shader program object
                                        // Specifies the handle of the program object whose executables are to be used as part of current rendering state.                           
    // 4 CPU steps
    // declaration and initialization of matrices
    var modelMatrix=mat4.create();  // creates the matrix in identity format(diagonals 1)
    var viewMatrix=mat4.create();  // creates the matrix in identity format(diagonals 1)

    // do necessary transformation (T,S,R)
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-6.0]);   // parameters: target(o/p) matrix, source matrix, vector to translate by
    mat4.rotateX(modelMatrix, modelMatrix, degToRad(angle_cube));   // degToRad(): our function
    mat4.rotateY(modelMatrix, modelMatrix, degToRad(angle_cube));   // degToRad(): our function
    mat4.rotateZ(modelMatrix, modelMatrix, degToRad(angle_cube));   // degToRad(): our function.
    
    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    gl.uniformMatrix4fv(mUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        modelMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

    gl.uniformMatrix4fv(vUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        viewMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

    gl.uniformMatrix4fv(pUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
        false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
        perspectiveProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
    );

    // B: bind a named texture to a texturing target
	gl.bindTexture(gl.TEXTURE_2D, texture);

	// U: specify the value of a uniform variable for the current program object(sending uniform); glUniformMatrix4fv() is used to send matrices as uniforms
	gl.uniform1i(samplerUniform, 0);		// tell OGL that you are sending uniform as one integer and give that to FS
                                            // uniform_texture0_sampler: Specifies the location of the uniform variable to be modified.
                                            // 0: Specifies the new values to be used for the specified uniform variable; 0 means unit 0

    // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
    gl.bindVertexArray(vao_cube);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

    // setting light properties
    gl.uniform3fv(laUniform, light_ambient);	// white light; to send vec3
    gl.uniform3fv(ldUniform, light_diffuse);
    gl.uniform3fv(lsUniform, light_specular);
    gl.uniform4fv(lightPositionUniform, light_position);	// no 1 here; to send vec4; ~glLightfv(GL_LIGHT0, GL_POSITION, LightPosition) in FFP

    // setting material properties
    gl.uniform3fv(kaUniform, material_ambient);
    gl.uniform3fv(kdUniform, material_diffuse);
    gl.uniform3fv(ksUniform, material_specular);
    gl.uniform1f(materialShininessUniform, material_shininess);

    // draw the necessary scene!
    gl.drawArrays(gl.TRIANGLE_FAN,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
        0,							// array position of your 9-member array to start with (imp in inter-leaved)
        4							// how many vertices to draw
    );		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
            // Arrays since multiple primitives(P,C,N,T) can be drawn

    gl.drawArrays(gl.TRIANGLE_FAN,
        4,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        8,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        12,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        16,
        4
    );

    gl.drawArrays(gl.TRIANGLE_FAN,
        20,
        4
    );

    // unbind texture
    gl.bindTexture(gl.TEXTURE_2D, null);
        
    // unbind vao
    gl.bindVertexArray(null);

    // unuse program
    gl.useProgram(null);	// unbinding your OpenGL code with shader program object
                            // If program is null, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.                                   

    requestAnimationFrame(Display, canvas);     // game loop;  requests that the browser calls a specified function to update an animation before the next repaint
                                                // requestAnimationFrame: function pointer; canvas: what to draw
                                                // this is not recursion !

    Update();                                           
}

function Update()
{
    angle_cube = angle_cube - 1.0;
	if (angle_cube <= -360.0)
        angle_cube = 0.0;
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI/180.0);
}

function Uninitialize()     // actually no need to call this function as there is garbage collection
{
    // code
    if(texture)
    {
        gl.deleteTexture(texture);
        texture=null;
    }

    if (shaderProgramObject)
    {        
        if(fragmentShaderObject)
        {
            gl.detachShader(shaderProgramObject,fragmentShaderObject);
            gl.deleteShader(fragmentShaderObject);
            fragmentShaderObject=null;
        }

        if(vertexShaderObject)
        {
            gl.detachShader(shaderProgramObject,vertexShaderObject);
            gl.deleteShader(vertexShaderObject);
            vertexShaderObject=null;
        }

        // delete the shader program
        gl.deleteProgram(shaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
        shaderProgramObject = null;
    }

    if (vbo_pctn_cube)
    {
        gl.deleteBuffer(vbo_pctn_cube);	// delete named buffer object                              
        vbo_pctn_cube = null;
    }

    if (vao_cube)
    {
        gl.deleteVertexArray(vao_cube);		// delete vertex array object
        vao_cube = null;
    }
}

function ToggleFullScreen()     // handles browser-dependent code
{
    // code
    var fullscreen_element =
        document.fullscreenElement ||           // flags(pre-defined in browsers) will assign this value if it is present or will go further    // Opera
        document.webkitFullscreenElement ||     // Apple's Safari
        document.mozFullScreenElement ||        // Mozilla Firefox
        document.msFullscreenElement ||         // Microsoft's Internet Explorer or Edge
        null;                                   // if none of the above

    // if not fullscreen
    if (fullscreen_element == null)
    {
        if (canvas.requestFullscreen)       // requestFullscreen: function pointer      // Opera
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)       // Mozilla Firefox
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)    // Apple's Safari
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)        // Microsoft's Internet Explorer or Edge
            canvas.msRequestFullscreen();

        bFullscreen=true;
    }
    else    // if already fullscreen
    {
        if (document.exitFullscreen)        // calling on document, not on canvas
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();

        bFullscreen = false;
    }
}
