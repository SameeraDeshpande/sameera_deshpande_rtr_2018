// global variables
var canvas = null;
var gl = null;  // WebGL context (context named as gl to have 'gl' in our code)
var bFullscreen = false;
var canvas_original_width;
var canvas_original_height;

const WebGLMacros=      // when whole 'WebGLMacros' is const, all inside it are automatically const
{
    AMC_ATTRIBUTE_POSITION: 0,  // KVC (key-value coding): values assigned at compile time
	AMC_ATTRIBUTE_COLOR: 1,
	AMC_ATTRIBUTE_NORMAL: 2,
	AMC_ATTRIBUTE_TEXCOORD0: 3, // (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// shader objects
var shaderProgramObject_PV;
var shaderProgramObject_PF;

var mUniform_PV;
var vUniform_PV;
var pUniform_PV;
var laUniform_light0_PV;	// intensity of ambient light
var ldUniform_light0_PV;	// intensity of diffused light
var lsUniform_light0_PV;	// intensity of specular light
var lightPositionUniform_light0_PV;
var laUniform_light1_PV;	// intensity of ambient light
var ldUniform_light1_PV;	// intensity of diffused light
var lsUniform_light1_PV;	// intensity of specular light
var lightPositionUniform_light1_PV;
var laUniform_light2_PV;	// intensity of ambient light
var ldUniform_light2_PV;	// intensity of diffused light
var lsUniform_light2_PV;	// intensity of specular light
var lightPositionUniform_light2_PV;
var kaUniform_PV;
var kdUniform_PV;
var ksUniform_PV;
var materialShininessUniform_PV;
var isLKeyPressedUniform_PV;

var mUniform_PF;
var vUniform_PF;
var pUniform_PF;
var laUniform_light0_PF;	// intensity of ambient light
var ldUniform_light0_PF;	// intensity of diffused light
var lsUniform_light0_PF;	// intensity of specular light
var lightPositionUniform_light0_PF;
var laUniform_light1_PF;	// intensity of ambient light
var ldUniform_light1_PF;	// intensity of diffused light
var lsUniform_light1_PF;	// intensity of specular light
var lightPositionUniform_light1_PF;
var laUniform_light2_PF;	// intensity of ambient light
var ldUniform_light2_PF;	// intensity of diffused light
var lsUniform_light2_PF;	// intensity of specular light
var lightPositionUniform_light2_PF;
var kaUniform_PF;
var kdUniform_PF;
var ksUniform_PF;
var materialShininessUniform_PF;
var isLKeyPressedUniform_PF;
 
var perspectiveProjectionMatrix;
var gbLighting = false;
var gbVPressed=true;
var gbFPressed=false;
var light_angle = 0.0;

var lights = [
    {
        Ambient : [ 0.0,0.0,0.0 ],
        Diffuse : [ 1.0,0.0,0.0 ],
        Specular : [ 1.0,0.0,0.0 ],
        Position : [ 0.0,0.0,0.0,1.0 ]
    },
    {
        Ambient : [ 0.0,0.0,0.0 ],
        Diffuse : [ 0.0,1.0,0.0 ],
        Specular : [ 0.0,1.0,0.0 ],
        Position : [ 0.0,0.0,0.0,1.0 ]
    },
    {
        Ambient : [ 0.0,0.0,0.0 ],
        Diffuse : [ 0.0,0.0,1.0 ],
        Specular : [ 0.0,0.0,1.0 ],
        Position : [ 0.0,0.0,0.0,1.0 ]
    }
];

var material_ambient=[0.0,0.0,0.0];
var material_diffuse=[1.0,1.0,1.0];	
var material_specular=[1.0,1.0,1.0];
var material_shininess=128.0;     

var sphere=null;

// requestAnimationFrame and cancelAnimationFrame: ~glSwapBuffers() or glxSwapBuffers() or requestRender()
// to start animation : to have requestAnimationFrame() to be called "cross-browser" compatible
var requestAnimationFrame =             // function pointer
window.requestAnimationFrame ||         // for browsers not having their own frame; // window: in-built variable
window.webkitRequestAnimationFrame ||   // Safari
window.mozRequestAnimationFrame ||      // Mozilla
window.oRequestAnimationFrame ||        // Opera
window.msRequestAnimationFrame;         // IE/Edge
// || null also ok

// to stop animation : to have cancelAnimationFrame() to be called "cross-browser" compatible
// not required in our code but required in offline rendering
var cancelAnimationFrame =              // function pointer 
window.cancelAnimationFrame ||         // for browsers not having their own frame
window.webkitCancelRequestAnimationFrame || window.webkitCancelAnimationFrame ||// Safari     // 2 for different versions of browser engines(?)
window.mozCancelRequestAnimationFrame || window.mozCancelAnimationFrame || // Mozilla
window.oCancelRequestAnimationFrame || window.oCancelAnimationFrame ||   // Opera
window.msCancelRequestAnimationFrame || window.msCancelAnimationFrame;   // IE/Edge

// onload function
function main()     // our entry-point function
{
    // get <canvas> element
    canvas = document.getElementById("AMC");    // document not declared means it is built-in; document: HTML page(representative of DOM), '.' means internally document is a class
                                                    // var: inferred type
    if (!canvas)    // lets you know whether or not there is support for HTML5
        console.log("Obtaining Canvas Failed\n");   // console: predefined
    else
        console.log("Obtaining Canvas Succeeded\n");

    canvas_original_width = canvas.width;
    canvas_original_height = canvas.height;

    // register keyboard's keydown event handler
    window.addEventListener("keydown", keyDown, false);     // "keydown": window's event listener(not our name); keydown: our event listener; false: don't do event bubbling i.e. sending event to super and up(bubble capture)
    window.addEventListener("click", mouseDown, false);
    window.addEventListener("resize", Resize, false);

    // initialize WebGL
    Initialize();

    // start drawing here as warming-up
    Resize();
    Display();
}


// define your event handlers (callback functions)
function keyDown(event)
{
    // code
    switch (event.keyCode)
    {
        case 86:    // 'V' or 'v'
            if (gbLighting == true)
			{
				gbVPressed = true;
				gbFPressed = false;
			}
            break;
        
        case 70:    // 'F' or 'f'
            if (gbLighting == true)
			{
				gbFPressed = true;
				gbVPressed = false;
			}
            break;

        case 76:    // for 'L' or 'l'
            if (gbLighting == false)
                gbLighting = true;
            else
                gbLighting = false;
            break;

        case 32:    // space bar
            ToggleFullScreen();
            break;

        case 27:    // escape
            Uninitialize();      // don't call when using Firefox as it gives warnings when escape key is pressed
            // close our application's tab
            window.close();     // may not work in Firefox but works in Safari and Chrome
            break;
    }
}

function mouseDown()
{
    // code
}

function Initialize()
{
    // variable declarations
    var vertexShaderObject_PV;
    var fragmentShaderObject_PV;
    var vertexShaderObject_PF;
    var fragmentShaderObject_PF;

    // code
    // get WebGL 2.0 context 
    gl = canvas.getContext("webgl2");   // webgl1 for mobile browser    // not working in IE and Edge?
    if (gl == null) // failed to get context
    {
        console.log("Failed to get the rendering context for WebGL");
        return;
    }

    // set viewport height and width
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;

    // vertex shader PF
    // define vertex shader object
	vertexShaderObject_PF = gl.createShader(gl.VERTEX_SHADER);	// creates a shader that is given as parameter	// ~ GL_VERTEX_SHADER

	// write vertex shader source code
    var vertexShaderSourceCode_PF=
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "out vec3 t_norm;" +
    "out vec3 light_direction_light0;" +
    "out vec3 light_direction_light1;" +
    "out vec3 light_direction_light2;" +
    "out vec3 viewer_vector;" +
    "uniform mat4 u_m_matrix;" +
    "uniform mat4 u_v_matrix;" +
    "uniform mat4 u_p_matrix;" +
    "uniform vec4 u_light_position_light0;" +
    "uniform vec4 u_light_position_light1;" +
    "uniform vec4 u_light_position_light2;" +
    "uniform mediump int u_isLKeyPressed;" +
    "void main(void)" +
    "{" +
    "if(u_isLKeyPressed==1)" +
    "{" +
    "vec4 eye_coordinates =  u_v_matrix * u_m_matrix * vPosition;" +
    "t_norm = mat3(u_v_matrix * u_m_matrix) * vNormal;" +
    "light_direction_light0 = vec3(u_light_position_light0 - eye_coordinates);" +
    "light_direction_light1 = vec3(u_light_position_light1 - eye_coordinates);" +
    "light_direction_light2 = vec3(u_light_position_light2 - eye_coordinates);" +
    "viewer_vector = vec3(-eye_coordinates.xyz);" +
    "}" +
    "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
    "}";         // written in Graphics Library Shading/Shader Language (GLSL)
                // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES is 3.0 based
                // '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
                // ****VERY IMPORTANT****: uniform mediump int u_isSingleTapMade; program does not run if int not made mediump. Precision is imp. in ES. VS by def has highp for all. But int does not require highp(precision is related to battery life)
				// eye coordinates are not related to any projection, hence only mv
				// uniform mat4 u_mv_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// mat3 normal_matrix = mat3(u_mv_matrix): typecasting into mat3 gives you the normal matrix i.e. mat3(transpose(inverse(u_mv_matrix))); It gives first upper 3*3 matrix of the 4*4 matrix. transpose(), inverse() are shader functions
				// t_norm: transformed normal. normalize() is a shader function (our 'n')
				// vec3(u_light_position - eye_coordinates)= typecasting vec4(x,y,z,w) into vec3;
				// tn_dot_lightDirection (s.n): tn=transformed normal
				// reflect(-light_direction,t_norm): -light_direction since reflected ray is opposite to light direction
				// viewer_vector = normalize(vec3(-eye_coordinates.xyx)): -eye_coordinates since viewer's coordinates are opposite to object's eye coordinates
				// ambient = u_la * u_ka: Ia = La * Ka
				// diffuse = u_ld * u_kd * tn_dot_lightDirection: Id = Ld * Kd * (s.n)
				// specular = u_ls * u_ks * pow(max(dot(reflection_vector * viewer_vector),0.0),u_material_shininess): Is = Ls * Ks * (r.v)^f where r: reflection vector, v: viewer vector, f: material shininess (f= exponential factor)
				// phong_ads_light = ambient + diffuse + specular: I = Ia + Id + Is
				// gl_Position: in-built variable of shader				
				// u_p_matrix * u_v_matrix * u_m_matrix: opposite sequence of mvp

    // give above source code to the vertex shader object
    gl.shaderSource(vertexShaderObject_PF,vertexShaderSourceCode_PF);

    // compile the vertex shader
	gl.compileShader(vertexShaderObject_PF);
    // error checking
    if(gl.getShaderParameter(vertexShaderObject_PF,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(vertexShaderObject_PF);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Vertex Shader Compilation Log : +n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // vertex converted to fragment now

    // fragment shader PF-
    // define fragment shader object
    fragmentShaderObject_PF = gl.createShader(gl.FRAGMENT_SHADER);	// creates a shader that is given as parameter	// ~ GL_FRAGMENT_SHADER
    
    // write fragment shader source code
    var fragmentShaderSourceCode_PF=
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec3 t_norm;" +
    "in vec3 light_direction_light0;" +
    "in vec3 light_direction_light1;" +
    "in vec3 light_direction_light2;" +
    "in vec3 viewer_vector;" +
    "uniform vec3 u_la_light0;" +
    "uniform vec3 u_ld_light0;" +
    "uniform vec3 u_ls_light0;" +
    "uniform vec3 u_la_light1;" +
    "uniform vec3 u_ld_light1;" +
    "uniform vec3 u_ls_light1;" +
    "uniform vec3 u_la_light2;" +
    "uniform vec3 u_ld_light2;" +
    "uniform vec3 u_ls_light2;" +
    "uniform vec3 u_ka;" +
    "uniform vec3 u_kd;" +
    "uniform vec3 u_ks;" +
    "uniform float u_material_shininess;" +
    "uniform int u_isLKeyPressed;" +
    "out vec4 frag_color;" +
    "vec3 phong_ads_light;" +
    "void main(void)" +
    "{" +
    "if(u_isLKeyPressed==1)" +
    "{" +
    "vec3 normalized_t_norm = normalize(t_norm);" +
    "vec3 normalized_viewer_vector = normalize(viewer_vector);" +
    "																																			\n" +
    "vec3 normalized_light_direction_light0 = normalize(light_direction_light0);" +
    "vec3 reflection_vector_light0 = reflect(-normalized_light_direction_light0,normalized_t_norm);" +
    "float tn_dot_lightDirection_light0 = max(dot(normalized_light_direction_light0,normalized_t_norm),0.0);" +
    "vec3 ambient_light0 = u_la_light0 * u_ka;" +
    "vec3 diffuse_light0 = u_ld_light0 * u_kd * tn_dot_lightDirection_light0;" +
    "vec3 specular_light0 = u_ls_light0 * u_ks * pow(max(dot(reflection_vector_light0, normalized_viewer_vector),0.0),u_material_shininess);" +
    "																																			\n" +
    "vec3 normalized_light_direction_light1 = normalize(light_direction_light1);" +
    "vec3 reflection_vector_light1 = reflect(-normalized_light_direction_light1,normalized_t_norm);" +
    "float tn_dot_lightDirection_light1 = max(dot(normalized_light_direction_light1,normalized_t_norm),0.0);" +
    "vec3 ambient_light1 = u_la_light1 * u_ka;" +
    "vec3 diffuse_light1 = u_ld_light1 * u_kd * tn_dot_lightDirection_light1;" +
    "vec3 specular_light1 = u_ls_light1 * u_ks * pow(max(dot(reflection_vector_light1, normalized_viewer_vector),0.0),u_material_shininess);" +
    "																																			\n" +
    "vec3 normalized_light_direction_light2 = normalize(light_direction_light2);" +
    "vec3 reflection_vector_light2 = reflect(-normalized_light_direction_light2,normalized_t_norm);" +
    "float tn_dot_lightDirection_light2 = max(dot(normalized_light_direction_light2,normalized_t_norm),0.0);" +
    "vec3 ambient_light2 = u_la_light2 * u_ka;" +
    "vec3 diffuse_light2 = u_ld_light2 * u_kd * tn_dot_lightDirection_light2;" +
    "vec3 specular_light2 = u_ls_light2 * u_ks * pow(max(dot(reflection_vector_light2, normalized_viewer_vector),0.0),u_material_shininess);" +
    "																																			\n" +
    "phong_ads_light = ambient_light0 + diffuse_light0 + specular_light0 + ambient_light1 + diffuse_light1 + specular_light1 + ambient_light2 + diffuse_light2 + specular_light2;" +
    "}" +
    "else" +
    "{" +
    "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
    "}" +
    "frag_color = vec4(phong_ads_light, 1.0);" +
    "}";         // written in Graphics Library Shading/Shader Language (GLSL)
                // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES 3.0 based
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // precision highp float: give high precision to float.(error if not specified) Use mediump for int(def). VS by def has highp for all. Max processing happens in FS than VS as FS gives ultimate color.
                // precision needed only in ES as there are many math units
                /// in vec4 out_color: VS's out is FS's in. Hence, same name. (gl_Position is in-built hence need not be passed)
				// out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
				// vec4: function/macro/constructor (of vmath?)
				// frag_color = vec4(phong_ads_light,1.0): 1.0 just for converting vec3 to vec4

    // give above source code to the fragment shader object
    gl.shaderSource(fragmentShaderObject_PF,fragmentShaderSourceCode_PF);

    // compile the fragment shader
	gl.compileShader(fragmentShaderObject_PF);
    // error checking
    if(gl.getShaderParameter(fragmentShaderObject_PF,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject_PF);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Fragment Shader Compilation Log : +n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // create shader program object 
	shaderProgramObject_PF = gl.createProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject_PF, vertexShaderObject_PF);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject_PF, fragmentShaderObject_PF); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding of shaderProgramObject_PF to vertex shader attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	gl.bindAttribLocation(shaderProgramObject_PF,	// the handle of the program object in which the association is to be made.
		WebGLMacros.AMC_ATTRIBUTE_POSITION,		// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
        // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition
       
    gl.bindAttribLocation(shaderProgramObject_PF,	// the handle of the program object in which the association is to be made.
        WebGLMacros.AMC_ATTRIBUTE_NORMAL,		// the index of the generic vertex attribute to be bound.
        "vNormal"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
    );	

    // link the shader program to your program
	gl.linkProgram(shaderProgramObject_PF);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
    // error checking (eg. of link error- version incompatibility of shaders)
    if(!gl.getProgramParameter(shaderProgramObject_PF,gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject_PF);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Shader Program Link Log : +n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // post-linking retrieving uniform locations (uniforms are global to shaders)
	mUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_m_matrix");	// preparation of data transfer from CPU to GPU (binding)
                                                                                // u_mvp_matrix: of GPU; mvpUniform: of CPU
                                                                                // telling it to take location of uniform u_mvp_matrix and give in mvpUniform
    vUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_v_matrix");                                                                            
    pUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_p_matrix");

    laUniform_light0_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_la_light0");
	ldUniform_light0_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ld_light0");
	lsUniform_light0_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ls_light0");
	lightPositionUniform_light0_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_light_position_light0");

	laUniform_light1_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_la_light1");
	ldUniform_light1_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ld_light1");
	lsUniform_light1_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ls_light1");
	lightPositionUniform_light1_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_light_position_light1");

	laUniform_light2_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_la_light2");
	ldUniform_light2_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ld_light2");
	lsUniform_light2_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ls_light2");
    lightPositionUniform_light2_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_light_position_light2");
    
    kaUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ka");
    kdUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_kd");
    ksUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_ks");
    materialShininessUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_material_shininess");

    isLKeyPressedUniform_PF = gl.getUniformLocation(shaderProgramObject_PF, "u_isLKeyPressed");

    // =====================================================================================================================================================================================

    // vertex shader PV-
    // define vertex shader object
	vertexShaderObject_PV = gl.createShader(gl.VERTEX_SHADER);	// creates a shader that is given as parameter	// ~ GL_VERTEX_SHADER

	// write vertex shader source code
    var vertexShaderSourceCode_PV=
    "#version 300 es" +
    "\n" +
    "in vec4 vPosition;" +
    "in vec3 vNormal;" +
    "uniform mat4 u_m_matrix;" +
    "uniform mat4 u_v_matrix;" +
    "uniform mat4 u_p_matrix;" +
    "uniform vec3 u_la_light0;" +
    "uniform vec3 u_ld_light0;" +
    "uniform vec3 u_ls_light0;" +
    "uniform vec4 u_light_position_light0;" +
    "uniform vec3 u_la_light1;" +
    "uniform vec3 u_ld_light1;" +
    "uniform vec3 u_ls_light1;" +
    "uniform vec4 u_light_position_light1;" +
    "uniform vec3 u_la_light2;" +
    "uniform vec3 u_ld_light2;" +
    "uniform vec3 u_ls_light2;" +
    "uniform vec4 u_light_position_light2;" +
    "uniform vec3 u_ka;" +
    "uniform vec3 u_kd;" +
    "uniform vec3 u_ks;" +
    "uniform float u_material_shininess;" +
    "uniform mediump int u_isLKeyPressed;" +
    "out vec3 phong_ads_light;" +
    "void main(void)" +
    "{" +
    "if(u_isLKeyPressed==1)" +
    "{" +
    "vec4 eye_coordinates =  u_v_matrix * u_m_matrix * vPosition;" +
    "vec3 t_norm = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);" +
    "vec3 viewer_vector = normalize(vec3(-eye_coordinates.xyz));" +
    "																																			\n" +
    "vec3 light_direction_light0 = normalize(vec3(u_light_position_light0 - eye_coordinates));" +
    "float tn_dot_lightDirection_light0 = max(dot(light_direction_light0,t_norm),0.0);" +
    "vec3 reflection_vector_light0 = reflect(-light_direction_light0,t_norm);" +
    "vec3 ambient_light0 = u_la_light0 * u_ka;" +
    "vec3 diffuse_light0 = u_ld_light0 * u_kd * tn_dot_lightDirection_light0;" +
    "vec3 specular_light0 = u_ls_light0 * u_ks * pow(max(dot(reflection_vector_light0, viewer_vector),0.0),u_material_shininess);" +
    "																																			\n" +
    "vec3 light_direction_light1 = normalize(vec3(u_light_position_light1 - eye_coordinates));" +
    "float tn_dot_lightDirection_light1 = max(dot(light_direction_light1,t_norm),0.0);" +
    "vec3 reflection_vector_light1 = reflect(-light_direction_light1,t_norm);" +
    "vec3 ambient_light1 = u_la_light1 * u_ka;" +
    "vec3 diffuse_light1 = u_ld_light1 * u_kd * tn_dot_lightDirection_light1;" +
    "vec3 specular_light1 = u_ls_light1 * u_ks * pow(max(dot(reflection_vector_light1, viewer_vector),0.0),u_material_shininess);" +
    "																																			\n" +
    "vec3 light_direction_light2 = normalize(vec3(u_light_position_light2 - eye_coordinates));" +
    "float tn_dot_lightDirection_light2 = max(dot(light_direction_light2,t_norm),0.0);" +
    "vec3 reflection_vector_light2 = reflect(-light_direction_light2,t_norm);" +
    "vec3 ambient_light2 = u_la_light2 * u_ka;" +
    "vec3 diffuse_light2 = u_ld_light2 * u_kd * tn_dot_lightDirection_light2;" +
    "vec3 specular_light2 = u_ls_light2 * u_ks * pow(max(dot(reflection_vector_light2, viewer_vector),0.0),u_material_shininess);" +
    "																																			\n" +
    "phong_ads_light = ambient_light0 + diffuse_light0 + specular_light0 + ambient_light1 + diffuse_light1 + specular_light1 + ambient_light2 + diffuse_light2 + specular_light2;" +
    "}" +
    "else" +
    "{" +
    "phong_ads_light = vec3(1.0, 1.0, 1.0);" +
    "}" +
    "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" +
    "}";         // written in Graphics Library Shading/Shader Language (GLSL)
                // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES is 3.0 based
                // '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
                // ****VERY IMPORTANT****: uniform mediump int u_isSingleTapMade; program does not run if int not made mediump. Precision is imp. in ES. VS by def has highp for all. But int does not require highp(precision is related to battery life)
				// eye coordinates are not related to any projection, hence only mv
				// uniform mat4 u_mv_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// mat3 normal_matrix = mat3(u_mv_matrix): typecasting into mat3 gives you the normal matrix i.e. mat3(transpose(inverse(u_mv_matrix))); It gives first upper 3*3 matrix of the 4*4 matrix. transpose(), inverse() are shader functions
				// t_norm: transformed normal. normalize() is a shader function (our 'n')
				// vec3(u_light_position - eye_coordinates)= typecasting vec4(x,y,z,w) into vec3;
				// tn_dot_lightDirection (s.n): tn=transformed normal
				// reflect(-light_direction,t_norm): -light_direction since reflected ray is opposite to light direction
				// viewer_vector = normalize(vec3(-eye_coordinates.xyx)): -eye_coordinates since viewer's coordinates are opposite to object's eye coordinates
				// ambient = u_la * u_ka: Ia = La * Ka
				// diffuse = u_ld * u_kd * tn_dot_lightDirection: Id = Ld * Kd * (s.n)
				// specular = u_ls * u_ks * pow(max(dot(reflection_vector * viewer_vector),0.0),u_material_shininess): Is = Ls * Ks * (r.v)^f where r: reflection vector, v: viewer vector, f: material shininess (f= exponential factor)
				// phong_ads_light = ambient + diffuse + specular: I = Ia + Id + Is
				// gl_Position: in-built variable of shader				
				// u_p_matrix * u_v_matrix * u_m_matrix: opposite sequence of mvp

    // give above source code to the vertex shader object
    gl.shaderSource(vertexShaderObject_PV,vertexShaderSourceCode_PV);

    // compile the vertex shader
	gl.compileShader(vertexShaderObject_PV);
    // error checking
    if(gl.getShaderParameter(vertexShaderObject_PV,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(vertexShaderObject_PV);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Vertex Shader Compilation Log : +n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // vertex converted to fragment now

    // fragment shader PV-
    // define fragment shader object
    fragmentShaderObject_PV = gl.createShader(gl.FRAGMENT_SHADER);	// creates a shader that is given as parameter	// ~ GL_FRAGMENT_SHADER
    
    // write fragment shader source code
    var fragmentShaderSourceCode_PV=
    "#version 300 es" +
    "\n" +
    "precision highp float;" +
    "in vec3 phong_ads_light;" +
    "out vec4 frag_color;" +
    "void main(void)" +
    "{" +
    "frag_color = vec4(phong_ads_light,1.0);" +
    "}";         // written in Graphics Library Shading/Shader Language (GLSL)
                // 300: OpenGL version support*100 (3.0*100); es: embedded system; OpenGL ES 3.0 based
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // precision highp float: give high precision to float.(error if not specified) Use mediump for int(def). VS by def has highp for all. Max processing happens in FS than VS as FS gives ultimate color.
                // precision needed only in ES as there are many math units
                /// in vec4 out_color: VS's out is FS's in. Hence, same name. (gl_Position is in-built hence need not be passed)
				// out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
				// vec4: function/macro/constructor (of vmath?)
				// frag_color = vec4(phong_ads_light,1.0): 1.0 just for converting vec3 to vec4

    // give above source code to the fragment shader object
    gl.shaderSource(fragmentShaderObject_PV,fragmentShaderSourceCode_PV);

    // compile the fragment shader
	gl.compileShader(fragmentShaderObject_PV);
    // error checking
    if(gl.getShaderParameter(fragmentShaderObject_PV,gl.COMPILE_STATUS)==false)      // getShaderParameter(): different function; // gl.COMPILE_STATUS: what(object parameter) is to be queried	
    {
        var error=gl.getShaderInfoLog(fragmentShaderObject_PV);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Fragment Shader Compilation Log : +n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // create shader program object 
	shaderProgramObject_PV = gl.createProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject_PV, vertexShaderObject_PV);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	gl.attachShader(shaderProgramObject_PV, fragmentShaderObject_PV); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding of shaderProgramObject_PV to vertex shader attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	gl.bindAttribLocation(shaderProgramObject_PV,	// the handle of the program object in which the association is to be made.
		WebGLMacros.AMC_ATTRIBUTE_POSITION,		// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
        // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition
       
    gl.bindAttribLocation(shaderProgramObject_PV,	// the handle of the program object in which the association is to be made.
        WebGLMacros.AMC_ATTRIBUTE_NORMAL,		// the index of the generic vertex attribute to be bound.
        "vNormal"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
    );	

    // link the shader program to your program
	gl.linkProgram(shaderProgramObject_PV);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
    // error checking (eg. of link error- version incompatibility of shaders)
    if(!gl.getProgramParameter(shaderProgramObject_PV,gl.LINK_STATUS))
    {
        var error=gl.getProgramInfoLog(shaderProgramObject_PV);  // getShaderInfoLog(): different function
        if(error.length > 0)
        {
            alert("Shader Program Link Log : +n\n"+error);   // browser's message box
            Uninitialize();
        }
    }

    // post-linking retrieving uniform locations (uniforms are global to shaders)
	mUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_m_matrix");	// preparation of data transfer from CPU to GPU (binding)
                                                                                // u_mvp_matrix: of GPU; mvpUniform: of CPU
                                                                                // telling it to take location of uniform u_mvp_matrix and give in mvpUniform
    vUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_v_matrix");                                                                            
    pUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_p_matrix");

    laUniform_light0_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_la_light0");
	ldUniform_light0_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ld_light0");
	lsUniform_light0_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ls_light0");
	lightPositionUniform_light0_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_light_position_light0");

	laUniform_light1_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_la_light1");
	ldUniform_light1_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ld_light1");
	lsUniform_light1_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ls_light1");
	lightPositionUniform_light1_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_light_position_light1");

	laUniform_light2_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_la_light2");
	ldUniform_light2_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ld_light2");
	lsUniform_light2_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ls_light2");
    lightPositionUniform_light2_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_light_position_light2");
    
    kaUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ka");
    kdUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_kd");
    ksUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_ks");
    materialShininessUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_material_shininess");
    
    isLKeyPressedUniform_PV = gl.getUniformLocation(shaderProgramObject_PV, "u_isLKeyPressed");

    // =====================================================================================================================================================================================
    // sphere
    sphere=new Mesh();              // from Mesh.js; vao, vbo in it
    makeSphere(sphere,2.0,30,30);   // from Sphere.js; para: mesh, radius, latitudes, longitudes

    gl.clearDepth(1.0);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	gl.enable(gl.DEPTH_TEST);	// to compare depth values of objects

	gl.depthFunc(gl.LEQUAL);	// specifies the value used for depth-buffer comparisons
                            // Passes if the incoming z value is less than or equal to the stored z value.
    
    // we will always cull back faces for better performance
    gl.enable(gl.CULL_FACE);

    // set clear color
    gl.clearColor(0.0, 0.0, 0.0, 1.0);

    // initialize matrices
    perspectiveProjectionMatrix = mat4.create();	// creates the matrix in identity format(diagonals 1); only does identity if matrix already present (?)

    // no warm-up call
}

function Resize()
{
    // code
    // set viewport
    if (bFullscreen == true)    // need to write this explicitly since there is no repaint() calling resize() here
    {
        canvas.width = window.innerWidth;   // inner property of fullscreen
        canvas.height = window.innerHeight; // ~code of WM_SIZE in Windows
    }
    else
    {
        canvas.width = canvas_original_width;   // taken in main()
        canvas.height = canvas_original_height;
    }

    if(canvas.height==0)
    {
        canvas.height=1;
    }

    // set the viewport to match
    gl.viewport(0, 0, canvas.width, canvas.height);

    // perspective projection
    mat4.perspective(perspectiveProjectionMatrix, degToRad(45.0), parseFloat(canvas.width)/parseFloat(canvas.height), 0.1, 100.0);    // parameters:
                                                                                                                            // o/p matrix
                                                                                                                            // fovy- The field of view angle, in radians, in the y - direction.
                                                                                                                            // aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
                                                                                                                            // zNear- The distance from the viewer to the near clipping plane(always positive).
                                                                                                                            // zFar- The distance from the viewer to the far clipping plane(always positive).
}

function Display()
{
    // variable declarations
    var r=100.0;

    // code
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

    // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	gl.useProgram(shaderProgramObject_PF);	// binding your OpenGL code with shader program object
                                        // Specifies the handle of the program object whose executables are to be used as part of current rendering state.

    // 4 CPU steps
    // declaration and initialization of matrices
    var modelMatrix=mat4.create();  // creates the matrix in identity format(diagonals 1)
    var viewMatrix=mat4.create();  // creates the matrix in identity format(diagonals 1)

    // do necessary transformation (T,S,R)
    mat4.translate(modelMatrix, modelMatrix, [0.0,0.0,-8.0]);   // parameters: o/p matrix, the matrix to translate, vector to translate by

    // light 1 (rotating about x-axis hence y and z will change)
    lights[0].Position[0] = 0.0;
    lights[0].Position[1] = r * Math.sin(light_angle);
    lights[0].Position[2] = r * Math.cos(light_angle);
    lights[0].Position[3] = 1.0;

    // light 2 (rotating about y-axis hence x and z will change)
    lights[1].Position[0] = r * Math.cos(light_angle);
    lights[1].Position[1] = 0.0;
    lights[1].Position[2] = r * Math.sin(light_angle);
    lights[1].Position[3] = 1.0;

    // light 3 (rotating about z-axis hence x and y will change)
    lights[2].Position[0] = r * Math.cos(light_angle);
    lights[2].Position[1] = r * Math.sin(light_angle);
    lights[2].Position[2] = 0.0;
    lights[2].Position[3] = 1.0;

    if (gbVPressed == true)
	{
		gl.useProgram(shaderProgramObject_PV);	// binding your OpenGL code with shader program object
                                                // Specifies the handle of the program object whose executables are to be used as part of current rendering state.
                                                
        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        gl.uniformMatrix4fv(mUniform_PV,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
        );

        gl.uniformMatrix4fv(vUniform_PV,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            viewMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
        );

        gl.uniformMatrix4fv(pUniform_PV,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            perspectiveProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
        );

        if (gbLighting == true)
        {
            gl.uniform1i(isLKeyPressedUniform_PV, 1);	// send 1 if 'L' key is pressed; 1i: to send 1 int
            
            gl.uniform3fv(laUniform_light0_PV, lights[0].Ambient, 0);	
            gl.uniform3fv(ldUniform_light0_PV, lights[0].Diffuse, 0);	// white light; to send vec3
            gl.uniform3fv(lsUniform_light0_PV, lights[0].Specular, 0);	
            gl.uniform4fv(lightPositionUniform_light0_PV, lights[0].Position);

            gl.uniform3fv(laUniform_light1_PV, lights[1].Ambient, 0);	
            gl.uniform3fv(ldUniform_light1_PV, lights[1].Diffuse, 0);	// white light; to send vec3
            gl.uniform3fv(lsUniform_light1_PV, lights[1].Specular, 0);	
            gl.uniform4fv(lightPositionUniform_light1_PV, lights[1].Position);

            gl.uniform3fv(laUniform_light2_PV, lights[2].Ambient, 0);	
            gl.uniform3fv(ldUniform_light2_PV, lights[2].Diffuse, 0);	// white light; to send vec3
            gl.uniform3fv(lsUniform_light2_PV, lights[2].Specular, 0);	
            gl.uniform4fv(lightPositionUniform_light2_PV, lights[2].Position);

            // setting material properties
            gl.uniform3fv(kaUniform_PV, material_ambient);
            gl.uniform3fv(kdUniform_PV, material_diffuse);
            gl.uniform3fv(ksUniform_PV, material_specular);
            gl.uniform1f(materialShininessUniform_PV, material_shininess);
        }
        else
        {
            gl.uniform1i(isLKeyPressedUniform_PV, 0);	// send 0 if 'L' key is not pressed; 1i: to send 1 int
        }                                            
	}
	else
	{
        gl.useProgram(shaderProgramObject_PF);
        
        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        gl.uniformMatrix4fv(mUniform_PF,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            modelMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
        );

        gl.uniformMatrix4fv(vUniform_PF,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            viewMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
        );

        gl.uniformMatrix4fv(pUniform_PF,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
            false,						            // do transpose? ; no since OGL and GLSL are column-major; false: of java
            perspectiveProjectionMatrix,		        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
        );

        if (gbLighting == true)
        {
            gl.uniform1i(isLKeyPressedUniform_PF, 1);	// send 1 if 'L' key is pressed; 1i: to send 1 int
            
            gl.uniform3fv(laUniform_light0_PF, lights[0].Ambient, 0);	
            gl.uniform3fv(ldUniform_light0_PF, lights[0].Diffuse, 0);	// white light; to send vec3
            gl.uniform3fv(lsUniform_light0_PF, lights[0].Specular, 0);	
            gl.uniform4fv(lightPositionUniform_light0_PF, lights[0].Position);

            gl.uniform3fv(laUniform_light1_PF, lights[1].Ambient, 0);	
            gl.uniform3fv(ldUniform_light1_PF, lights[1].Diffuse, 0);	// white light; to send vec3
            gl.uniform3fv(lsUniform_light1_PF, lights[1].Specular, 0);	
            gl.uniform4fv(lightPositionUniform_light1_PF, lights[1].Position);

            gl.uniform3fv(laUniform_light2_PF, lights[2].Ambient, 0);	
            gl.uniform3fv(ldUniform_light2_PF, lights[2].Diffuse, 0);	// white light; to send vec3
            gl.uniform3fv(lsUniform_light2_PF, lights[2].Specular, 0);	
            gl.uniform4fv(lightPositionUniform_light2_PF, lights[2].Position);

            // setting material properties
            gl.uniform3fv(kaUniform_PF, material_ambient);
            gl.uniform3fv(kdUniform_PF, material_diffuse);
            gl.uniform3fv(ksUniform_PF, material_specular);
            gl.uniform1f(materialShininessUniform_PF, material_shininess);
        }
        else
        {
            gl.uniform1i(isLKeyPressedUniform_PF, 0);	// send 0 if 'L' key is not pressed; 1i: to send 1 int
        }      
    }
   
    // draw the necessary scene!
    sphere.draw();                  // in Mesh.js; vao in it

    // unuse program
    gl.useProgram(null);	// unbinding your OpenGL code with shader program object
                            // If program is null, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.                                   
    
    // animation loop
    Update();   // write here in Display() since we don't have an explicit game loop
    requestAnimationFrame(Display, canvas);     // game loop;  requests that the browser calls a specified function to update an animation before the next repaint
                                                // requestAnimationFrame: function pointer; canvas: what to draw
                                                // this is not recursion !
}

function Update()
{
    if (gbLighting == true)
	{
		light_angle = light_angle + degToRad(1.0);
		if (light_angle >= degToRad(360.0))
			light_angle = 0.0;
	}
}

function degToRad(degrees)
{
    // code
    return(degrees * Math.PI/180.0);    // Math is a built-in object that has properties and methods for mathematical constants and functions.
                                        // Math.PI since all properties(e.g. PI) and methods(e.g. sin()) of Math are static
}

function Uninitialize()     // actually no need to call this function as there is garbage collection
{
    // code
    if(sphere)
    {
        sphere.deallocate();    // in Mesh.js
        sphere=null;
    }

    if (shaderProgramObject_PF)
    {        
        if(fragmentShaderObject_PF)
        {
            gl.detachShader(shaderProgramObject_PF,fragmentShaderObject_PF);
            gl.deleteShader(fragmentShaderObject_PF);
            fragmentShaderObject_PF=null;
        }

        if(vertexShaderObject_PF)
        {
            gl.detachShader(shaderProgramObject_PF,vertexShaderObject_PF);
            gl.deleteShader(vertexShaderObject_PF);
            vertexShaderObject_PF=null;
        }

        // delete the shader program
        gl.deleteProgram(shaderProgramObject_PF);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
        shaderProgramObject_PF = null;
    }

    if (shaderProgramObject_PV)
    {        
        if(fragmentShaderObject_PV)
        {
            gl.detachShader(shaderProgramObject_PV,fragmentShaderObject_PV);
            gl.deleteShader(fragmentShaderObject_PV);
            fragmentShaderObject_PV=null;
        }

        if(vertexShaderObject_PV)
        {
            gl.detachShader(shaderProgramObject_PV,vertexShaderObject_PV);
            gl.deleteShader(vertexShaderObject_PV);
            vertexShaderObject_PV=null;
        }

        // delete the shader program
        gl.deleteProgram(shaderProgramObject_PV);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
        shaderProgramObject_PV = null;
    }
}

function ToggleFullScreen()     // handles browser-dependent code
{
    // code
    var fullscreen_element =
        document.fullscreenElement ||           // flags(pre-defined in browsers) will assign this value if it is present or will go further    // Opera
        document.webkitFullscreenElement ||     // Apple's Safari
        document.mozFullScreenElement ||        // Mozilla Firefox
        document.msFullscreenElement ||         // Microsoft's Internet Explorer or Edge
        null;                                   // if none of the above

    // if not fullscreen
    if (fullscreen_element == null)
    {
        if (canvas.requestFullscreen)       // requestFullscreen: function pointer      // Opera
            canvas.requestFullscreen();
        else if (canvas.mozRequestFullScreen)       // Mozilla Firefox
            canvas.mozRequestFullScreen();
        else if (canvas.webkitRequestFullscreen)    // Apple's Safari
            canvas.webkitRequestFullscreen();
        else if (canvas.msRequestFullscreen)        // Microsoft's Internet Explorer or Edge
            canvas.msRequestFullscreen();

        bFullscreen=true;
    }
    else    // if already fullscreen
    {
        if (document.exitFullscreen)        // calling on document, not on canvas
            document.exitFullscreen();
        else if (document.mozCancelFullScreen)
            document.mozCancelFullScreen();
        else if (document.webkitExitFullscreen)
            document.webkitExitFullscreen();
        else if (document.msExitFullscreen)
            document.msExitFullscreen();

        bFullscreen = false;
    }
}
