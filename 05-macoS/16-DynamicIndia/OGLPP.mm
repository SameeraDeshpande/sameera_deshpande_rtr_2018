// headers
// #import prevents recursive inclusion of header files
#import <Foundation/Foundation.h>	// for NSAutoreleasePool
#import <Cocoa/Cocoa.h>	// Cocoa.framework for GUI applications (~windows.h)	// Cocoa is SDK of Mac
#import <QuartzCore/CVDisplayLink.h>	// Core Video DisplayLink
#import <OpenGL/gl3.h>  // OGl 3.0 and onwards is PP
#import <OpenGL/gl3ext.h>   // with extensions
#import "vmath.h"

enum	// nameless since we are just concerned with its "named" indices
{
    AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,		// 1
	AMC_ATTRIBUTE_NORMAL,		// 2
	AMC_ATTRIBUTE_TEXCOORD0,	// 3 (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// 'C' style global fuction declarations
CVReturn MyDisplayLinkCallback(
    CVDisplayLinkRef,       // which display link
    const CVTimeStamp *,    // current time
    const CVTimeStamp *,    // at what time to give output frame
    CVOptionFlags,          // reserved
    CVOptionFlags *,        // reserved
    void *                  // object with which this callback is registered
);

// global variable declarations
FILE *gpFile=NULL;
GLuint vao_I, vao_N, vao_D, vao_A, vao_plane, vao_smoke, vao_A_band;        // vertex array object    (1 also ok since we unbind)
GLuint vbo_position_I, vbo_color_I, vbo_position_N, vbo_color_N, vbo_position_D, vbo_color_D, vbo_position_A, vbo_color_A, vbo_position_plane, vbo_position_smoke, vbo_position_A_band, vbo_color_A_band;
GLuint mvpUniform;        // mvp: model-view projection
vmath::mat4 modelViewMatrix;
vmath::mat4 modelViewProjectionMatrix;
vmath::mat4 translationMatrix;
vmath::mat4 rotationMatrix;
vmath::mat4 perspectiveProjectionMatrix;        // mat4: in vmath.h; it is typedef of a 4-member float array
GLfloat letter_x = 0.46f;
GLfloat letter_start = -0.19f, letter_end = 0.19f;
GLfloat i1_x = -3.0f;
GLfloat a_x = 2.50f;
GLfloat n_y = 1.58f;
GLfloat i2_y = -1.58f;
GLfloat r_saffron = 0.0f, g_saffron = 0.0f, b_saffron = 0.0f;
GLfloat r_green = 0.0f, g_green = 0.0f, b_green = 0.0f;
GLfloat r = 1.42f;
GLfloat r_saffron_smoke = 255.0f, g_saffron_smoke = 153.0f, b_saffron_smoke = 51.0f;
GLfloat r_white_smoke = 1.0f;
GLfloat r_green_smoke = 18.0f, g_green_smoke = 136.0f, b_green_smoke = 7.0f;
GLfloat middle_plane_x = -letter_x * 5.4f;
GLfloat angle_upper_plane = -90.0f;
GLfloat angle_lower_plane = 90.0f;
GLfloat angle = (GLfloat)M_PI;
GLfloat angle_lower = (GLfloat)M_PI;
GLfloat angle_right_upper = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI;
GLfloat angle_right_lower = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI;

bool bDoneI1 = false;
bool bDoneA = false;
bool bDoneN = false;
bool bDoneI2 = false;
bool bDoneR_saffron = false, bDoneG_saffron = false, bDoneB_saffron = false;
bool bDoneR_green = false, bDoneG_green = false, bDoneB_green = false;
bool bDoneD = false;
bool bDoneMidddlePlane = false;
bool bDoneUpperPlane = false;
bool bDoneDetachPlanes = false;
bool bDoneFadingSmokes = false;

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> // AppDelegate inherited from NSObject and will implement functions of NSApplicationDelegate and NSWindowDelegate
																			// AppDelegate: delegate of NSApplicationDelegate, NSWindowDelegate
@end

@interface GLView : NSOpenGLView		// this is delegate to none
@end

// entry-point function
int main(int argc, const char* argv[])
{
	// code
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];	// NSAutoreleasePool: interface(class); alloc increases ref. count by 1

	NSApp=[NSApplication sharedApplication];	// NSApp: object; NSApplication: interface; sharedApplication: static method

	[NSApp setDelegate:[[AppDelegate alloc]init]];		// setDelegate: instance method; alloc = allocate memory ~'new';  init: constructor(succeeding alloc)


	[NSApp run];		// run loop

	[pPool release];    // ref. count=0

	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private				// default; OK if not written
	NSWindow *window;	// their class
	GLView *glView;		// our class
}	// does not mean class ends here (@end means that)

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification		// callback function (OS will call this); ~WM_CREATE; from NSApplicationDelegate
{
	// code
    // log file (5 steps)
    // get the object of your application package (.app)
    NSBundle *mainBundle=[NSBundle mainBundle];     // 1st mainBundle: object; 2nd mainBundle: static method, alloc and init happen internally

    // using this NSBundle object, get the path of your .app directory
    NSString *appDirName=[mainBundle bundlePath];   

    // get the parent directory of .app directory in NSString form (e.g. /Desktop/ProjName)
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];     // we want log file not in .app directory (since write protected) but besides it
                                                                                
    // create log file path in NSString form
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];  // 1st @: literal; %@ can give you object's UUID
                                                                                            // e.g. of %@ : /Desktop/ProjName;     e.g. of %@/ : /Desktop/ProjName/

    // convert NSString into C string (i.e. const char *)
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];    // p: pointer; sz: C string
                                                                                                            // asking ASCII encoding for C string

    // now use this C string as 1st para. of fopen()
    gpFile=fopen(pszLogFileNameWithPath,"w");
    if(gpFile==NULL)
    {
        printf("Cannot Create Log File.\n Exiting..\n");      // Objective C's printf called NSLog() and it internally calls printf()
        [self release];
        [NSApp terminate:self];     // will go to applicationWillTerminate
    }

    fprintf(gpFile, "Program Started Successfully\n");

	// window
	NSRect win_rect;	// struct (no '*' after it!)   // C style
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);       // C style

	// create simple window
	window=[[NSWindow alloc] initWithContentRect:win_rect	// create window of this rect (named parameters of init)
							 styleMask:NSWindowStyleMaskTitled |NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable| NSWindowStyleMaskResizable		// window styles
							 backing:NSBackingStoreBuffered		// how to store window in memory
							 defer:NO];		// do immediate repainting? no

	[window setTitle:@"macOS OpenGL Window"];		// @: literal string= NSString
	[window center];

    // create view (canvas) by giving it frame
	glView=[[GLView alloc]initWithFrame:win_rect];      // creating nameless object of GLView (object chaining)
                                                        // initWithFrame's implementation in GLView

    // give this view to window
	[window setContentView:glView];

	[window setDelegate:self];      // self = AppDelegate
	[window makeKeyAndOrderFront:self];     // get key focus on window and put it topmost in z-order
                                            // window: will receive messages; self: will give messages
}

-(void)applicationWillTerminate:(NSNotification *)notification      // callback function (OS will call this); ~WM_DESTROY; from NSApplicationDelegate
{
	// code
	fprintf(gpFile, "Program Terminated Successfully\n");

    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification	// ~WM_CLOSE; of NSWindowDelegate; applicationWillTerminate will be called after this
{
	// code
	[NSApp terminate:self];     // ~DestroyWindow()
}

-(void)dealloc		// destructor	// release calls this
{
	// code
	[glView release];		// will call dealloc of MyView
	[window release];	// NSWindow's default dealloc will be called
	[super dealloc];	// tell parent
}
@end

@implementation GLView
{
@private				// default; OK if not written
    CVDisplayLinkRef displayLink;   // ~CVDisplayLink displayLink;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
}

-(id)initWithFrame:(NSRect)frame;   // generic return type
{
    // code
	self=[super initWithFrame:frame];   // get GLView's object from parent

	if(self)    // imp code inside this
	{
		[[self window]setContentView:self];     // get GLView's window whose view is GLView only; this step actually required for NextStep Interface Builder

        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // must specify the 4.1 core profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,     // PFA: Pixel Format Attribute  // LHS
            NSOpenGLProfileVersion4_1Core,  // Apple has locked OpenGL at 4.1 version   // RHS
            // specify the display id to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),     // map Core Graphics' display id to OpenGL id;
                                                                                            // in mac, variables starting with 'k' are kernel level #define's
            NSOpenGLPFAAccelerated,     // h/w accleration i.e. h/w rendering; give h/w supporting OpenGL
            NSOpenGLPFANoRecovery,      // if no h/w acceleration obtained, give error and exit
            NSOpenGLPFAColorSize,24,    // 32 also ok
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,     // A of RGBA
            NSOpenGLPFADoubleBuffer,
            0                           // last 0 is must; nil also ok
        };

        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];    // get pixel format from attributes
                                                                                                                // autorelease will give you explicit release call w/o expecting release call in main()
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available.\n Exiting..\n");      // Objective C's printf called NSLog() and it internally calls printf()
            [self release];
            [NSApp terminate:self];     // will go to applicationWillTerminate
        }

        // now context using pixelFormat
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext];      // automatically releases the older context if present and sets the newer one
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];    // pool signifies it(?) is thread

    [self drawView];    // our method

    [pool release];
    return(kCVReturnSuccess); 
}

-(void)prepareOpenGL    // NSOpenGLView's method which we implement; OS will call it; This method is called only once after the OpenGL context is made the current context.
{
    // code
    // OpenGL info
    fprintf(gpFile,"OpenGL Version: %s\n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];       // ~wglMakeCurrent, glxCurrent

    // decide double buffer's swap interval
    GLint swapInt=1;    // mac's default swapping interval is 1 i.e. 1 frame gap
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];       // forParameter: for whom to set; CP: Context Parameter
    
    // OpenGL code
    // vertex shader-
	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);	// creates a shader that is given as parameter	// 1st line of programmable pipeline !

	// write vertex shader source code
	const GLchar* vertexShaderSourceCode = 
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
				// uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// gl_Position: in-built variable of shader	
				// out_color = vColor: assigning in attribute to out attribute
				// here, color is going to be given to frag color directly			

	// give above source code to the vertex shader object
	glShaderSource(
		gVertexShaderObject,	// shader to which the source code is to be given
		1,	// the number of strings in the array
		(const GLchar **)&vertexShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);	// replace the source code in a shader object

	// compile the vertex shader
	glCompileShader(gVertexShaderObject);
	// error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gVertexShaderObject,	// the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gVertexShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
											// When a shader object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nVertex Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

	// vertex converted to fragment now

    // fragment shader-
	// define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);	// creates a shader that is given as parameter

	// write fragment shader source code
	const GLchar* fragmentShaderSourceCode = 
		"#version 410 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 frag_color;" \
		"void main(void)" \
		"{" \
		"frag_color = out_color;" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt
				// in vec4 out_color: VS's out is FS's in. Hence, same name. (gl_Position is in-built hence need not be passed)
				// out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
				// vec4: function/macro/constructor

	// give above source code to the fragment shader object
	glShaderSource(
		gFragmentShaderObject,	// shader to which the source code is to be given
		1,		// the number of strings in the array
		(const GLchar **)&fragmentShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);			// replace the source code in a shader object

				// compile the fragment shader
	glCompileShader(gFragmentShaderObject);
	// error checking
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gFragmentShaderObject,	//  the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gFragmentShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);						// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
										// When a shader object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nFragment Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

    // create shader program object 
	gShaderProgramObject = glCreateProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding of gShaderProgramObject to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_POSITION,					// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
		// give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_COLOR,					// the index of the generic vertex attribute to be bound.
		"vColor"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	// link the shader program to your program
	glLinkProgram(gShaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
	// error checking (eg. of link error- version incompatibility of shaders)
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(				// iv= integer vector
		gShaderProgramObject,	//  the program to be queried.
		GL_LINK_STATUS,			// what(object parameter) is to be queried	
		&iProgramLinkStatus		// empty	//  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
	);							// return a parameter from a program object
	if (iProgramLinkStatus == GL_FALSE)	// error present
	{
		// check if the linker has any info. about the error
		glGetProgramiv(gShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetProgramInfoLog(
					gShaderProgramObject,	// the program object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
											// When a program object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nShader Program Link Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

    // post-linking retrieving uniform locations (uniforms are global to shaders)
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");	// preparation of data transfer from CPU to GPU (binding)
																				// u_mvp_matrix: of GPU; mvpUniform: of CPU
																				// telling it to take location of uniform u_mvp_matrix and give in mvpUniform

	// variable declarations
	GLfloat half_letter_ht = 0.5f;

	// fill vertices in array (this was in Display() in FFP)
	const GLfloat I_position[] = {
		letter_start + 0.02f, half_letter_ht, 0.0f,
		letter_end - 0.02f, half_letter_ht, 0.0f,
		0.0f, half_letter_ht, 0.0f,
		0.0f, -half_letter_ht, 0.0f,
		letter_start + 0.02f, -half_letter_ht, 0.0f,
		letter_end - 0.02f, -half_letter_ht, 0.0f, };	// last ',' OK!; const since array of values
														// since written in Initialize() only, coords will not get set everytime in Display(). Hence, fast speed.

	const GLfloat I_color[] = { 
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375,	 // green
		0.0703125, 0.53125, 0.02734375,	 // green
		0.0703125, 0.53125, 0.02734375   // green
	};

	const GLfloat N_position[] = {
		letter_start, half_letter_ht + 0.015f, 0.0f,
		letter_start, -half_letter_ht - 0.015f, 0.0f,
		letter_start, half_letter_ht + 0.015f, 0.0f,
		letter_end, -half_letter_ht - 0.015f, 0.0f,
		letter_end, -half_letter_ht - 0.015f, 0.0f,
		letter_end, half_letter_ht + 0.015f, 0.0f
	};

	const GLfloat N_color[] = {
		1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375 ,  // green
		1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375,   // green
		0.0703125, 0.53125, 0.02734375,   // green
		1.0, 0.59765625, 0.19921875,	 // saffron
	};

	const GLfloat D_position[] = {
		letter_start + 0.05f, half_letter_ht, 0.0f,
		letter_start + 0.05f, -half_letter_ht, 0.0f,
		letter_start, half_letter_ht, 0.0f,
		letter_end + 0.0135f, half_letter_ht, 0.0f,
		letter_end, half_letter_ht, 0.0f,
		letter_end, -half_letter_ht, 0.0f,
		letter_end + 0.013f, -half_letter_ht, 0.0f,
		letter_start, -half_letter_ht, 0.0
	};

	const GLfloat A_position[] = {
		letter_start, -half_letter_ht - 0.015f, 0.0f,
		0.0f, half_letter_ht + 0.015f, 0.0f,
		0.0f, half_letter_ht + 0.015f, 0.0f,
		letter_end, -half_letter_ht - 0.015f, 0.0f
	};

	const GLfloat A_color[] = {
		0.0703125, 0.53125, 0.02734375,	 // green
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		0.0703125, 0.53125, 0.02734375	 // green
	};

	const GLfloat A_band_position[] = {
		letter_start / 2.0f, 0.0166f, 0.0f,
		letter_end / 2.0f, 0.0166f, 0.0f,
		letter_start / 2.0f, 0.0f, 0.0f,
		letter_end / 2.0f, 0.0f, 0.0f,
		letter_start / 2.0f, -0.0173f, 0.0f,
		letter_end / 2.0f, -0.0173f, 0.0f
	};

	const GLfloat A_band_color[] = {
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 0.59765625, 0.19921875,	 // saffron
		1.0, 1.0, 1.0,					// white
		1.0, 1.0, 1.0,					// white
		0.0703125, 0.53125, 0.02734375,	 // green
		0.0703125, 0.53125, 0.02734375,	 // green
	};

	const GLfloat plane_position[] = {
		// middle quad
		0.25f + 0.1f, 0.05f, 0.0f,
		0.0f + 0.1f, 0.05f, 0.0f,
		0.0f + 0.1f, -0.05f, 0.0f,
		0.25f + 0.1f, -0.05f, 0.0f,
		// upper wing
		0.0f + 0.1f, 0.05f, 0.0f,
		0.1f + 0.1f, 0.05f, 0.0f,
		0.05f + 0.1f, 0.15f, 0.0f,
		0.0f + 0.1f, 0.15f, 0.0f,
		// lower wing
		0.1f + 0.1f, -0.05f, 0.0f,
		0.0f + 0.1f, -0.05f, 0.0f,
		0.0f + 0.1f, -0.15f, 0.0f,
		0.05f + 0.1f, -0.15f, 0.0f,
		// back
		0.0f + 0.1f, 0.05f, 0.0f,
		-0.1f + 0.1f, 0.08f, 0.0f,
		-0.1f + 0.1f, -0.08f, 0.0f,
		0.0f + 0.1f, -0.05f, 0.0f,
		// front triangle
		0.25f + 0.1f, 0.05f, 0.0f,
		0.25f + 0.1f, -0.05f, 0.0f,
		0.30f + 0.1f, 0.00f, 0.0f,
		// I
		-0.01f + 0.1f, 0.03f, 0.0f,
		0.03f + 0.1f, 0.03f, 0.0f,
		0.01f + 0.1f, 0.03f, 0.0f,
		0.01f + 0.1f, -0.03f, 0.0f,
		-0.01f + 0.1f, -0.03f, 0.0f,
		0.03f + 0.1f, -0.03f, 0.0f,
		// A
		0.05f + 0.1f, -0.03f, 0.0f,
		0.07f + 0.1f, 0.03f, 0.0f,
		0.07f + 0.1f, 0.03f, 0.0f,
		0.09f + 0.1f, -0.03f, 0.0f,
		(0.05f + 0.07f) / 2.0f + 0.1f, 0.0f, 0.0f,
		(0.07f + 0.09f) / 2.0f + 0.1f, 0.0f, 0.0f,
		// F
		0.12f + 0.1f, -0.03f, 0.0f,
		0.12f + 0.1f, 0.03f, 0.0f,
		0.12f + 0.1f, 0.03f, 0.0f,
		0.16f + 0.1f, 0.03f, 0.0f,
		0.12f + 0.1f, 0.0f, 0.0f,
		0.15f + 0.1f, 0.0f, 0.0f
	};

	// 9 lines
	// I
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_I);		// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_I);		// bind a vertex array object

	// I position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_I);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_I);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

														// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

														// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(I_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		I_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	// same above steps for C,T,N if any

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer
	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// I color
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_color_I);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_I);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(I_color),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		I_color,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vColor

													// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// N
								// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_N);		// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_N);		// bind a vertex array object

	// N position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_N);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_N);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(N_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		N_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	// same above steps for C,T,N if any

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// N color
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_color_N);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_N);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(N_color),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		N_color,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vColor

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// D
								// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_D);		// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_D);		// bind a vertex array object

	// D position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_D);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_D);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(D_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		D_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	// same above steps for C,T,N if any

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// D color
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_color_D);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_D);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

													// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

													// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(8 * 3 * sizeof(GLfloat)),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			NULL,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vColor

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// A
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_A);		// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_A);		// bind a vertex array object

	// A position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_A);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_A);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

														// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

														// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(A_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		A_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

														// same above steps for C,T,N if any

														// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// A color
										// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_color_A);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_A);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(A_color),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		A_color,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vColor

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// A band
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_A_band);		// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_A_band);		// bind a vertex array object

	// A band position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_A_band);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_A_band);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

														// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

														// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(A_band_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		A_band_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

														// same above steps for C,T,N if any

														// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// A band color
										// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_color_A_band);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_A_band);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

													// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

													// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(A_band_color),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		A_band_color,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	    // at CPU side; the index of the generic vertex attribute to be modified (send to vColor)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vColor

													// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// plane
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_plane);	// generate vertex array object names
										// 1: Specifies the number of vertex array object names to generate
										// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
										// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_plane);		// bind a vertex array object

	// plane position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_plane);		// generate buffer object names
											// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
											// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_plane);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

														// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

														// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(plane_position),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		plane_position,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	// same above steps for C,T,N if any

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	//--------------------------------------------------------------------------------------------------------------------------------------------------------------

	// plane color
	// ? not working here:
	//glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 186.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

	// smoke
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_smoke);		// generate vertex array object names
											// 1: Specifies the number of vertex array object names to generate
											// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
											// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_smoke);		// bind a vertex array object

	// smoke position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position_smoke);		// generate buffer object names
												// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
												// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
															// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
															// vbo: bind this (the name of a buffer object.)

															// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

															// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		0,				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		NULL,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes(between 2 vertices); 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer								

	// same above steps for C,T,N if any

	glBindVertexArray(0);		// unbind vao

	//=====================================================================================================================================================

    glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

    // set background color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											// specifies clear values[0,1] used by glClear() for the color buffers

	perspectiveProjectionMatrix = vmath::mat4::identity();	// making orthographicProjectionMatrix an identity matrix(diagonals 1)
														// mat4: of vmath

    // game loop (6 steps)  (C functions concerned with CGL and CG)
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);  // create display link; creating display link= creating seperate thread

    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);

    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];    // openGLContext of type NSOpenGLContext; 2nd CGLContextObj is method

    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];  // self pixelFormat: obtain pixelFormat of self

    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

    CVDisplayLinkStart(displayLink);    // start display link; game loop starts here!
    
    [super prepareOpenGL];
}

-(void)reshape      // NSOpenGLView's method which we override;  Called by Cocoa when the view's visible rectangle or bounds change.
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);   // lock context so that it does not go to main thread but remains in this thread only
                                                                        // 2nd CGLContextObj is method

    NSRect rect=[self bounds];      // call bounds() on self (self's bounds)

    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if (height == 0)
	{
		height = 1;
	}

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    perspectiveProjectionMatrix=vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// ? other values of near & far not working
																											// parameters:
																											// fovy- The field of view angle, in degrees, in the y - direction.
																											// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																											// zNear- The distance from the viewer to the near clipping plane(always positive).
																											// zFar- The distance from the viewer to the far clipping plane(always positive).

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawRect:(NSRect)dirtyRect   // ~WM_PAINT
{
    // code
    [self drawView];    // for precaution, not compulsion (in case of thread switching i.e. if it goes in main thread during buffer swapping (to avoid flickering))
}

-(void)drawView     // ~Display()
{
	// function declarations
	void DrawI1();
	void DrawA();
	void DrawN();
	void DrawI2();
	void DrawD();
	void DrawLeftUpperPlane();
	void DrawMiddlePlane();
	void DrawLeftLowerPlane();
	void DrawIAF(void);
	void DrawLeftUpperSmoke(void);
	void DrawSmokeMiddle(void);
	void DrawLeftLowerSmoke(void);
	void DrawABand();
	void DrawRightUpperPlane();
	void DrawRightLowerPlane();
	void DrawRightUpperSmoke();
	void DrawRightLowerSmoke();
	void Update(void);

    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    // display code
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	glUseProgram(gShaderProgramObject);	// binding your OpenGL code with shader program object
										// Specifies the handle of the program object whose executables are to be used as part of current rendering state.

	glLineWidth(15.0f);

	// I1
	DrawI1();

	// A
	if (bDoneI1 == true)
		DrawA();

	// N
	if (bDoneA == true)
		DrawN();

	// I2
	if (bDoneN == true)
		DrawI2();

	// D
	if (bDoneI2 == true)
		DrawD();

	// Attach planes
	if (bDoneD == true)
	{
		// upper
		if (bDoneUpperPlane == false)
		{
			DrawLeftUpperPlane();
		}
		DrawLeftUpperSmoke();

		// lower
		if (bDoneUpperPlane == false)
		{
			DrawLeftLowerPlane();
		}
		DrawLeftLowerSmoke();

		// middle
		DrawMiddlePlane();
		DrawSmokeMiddle();
	}

	// Detach planes
	if (bDoneMidddlePlane == true)
	{
		// upper
		if (bDoneDetachPlanes == false)
		{
			DrawRightUpperPlane();
		}
		DrawRightUpperSmoke();

		// lower
		if (bDoneDetachPlanes == false)
		{
			DrawRightLowerPlane();
		}
		DrawRightLowerSmoke();

		// middle
		DrawMiddlePlane();
		DrawSmokeMiddle();
	}

	// A's band
	if (bDoneDetachPlanes == true)
	{
		DrawABand();
	}

	// unuse program
	glUseProgram(0);	// unbinding your OpenGL code with shader program object
						// If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);       // ~glSwapBuffers() ; for multi-threaded rendering
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	Update();
}

-(BOOL)acceptsFirstResponder		// telling window that give your event handling to your view
{
	// code
	[[self window]makeFirstResponder:self];
	return(YES);		// YES means method is implemented
}

-(void)keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:    // esc key
            [self release];
            [NSApp terminate:self];
            break;

        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];	// toggleFullScreen: its method; repainting occurs automatically
            break;

        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
	// code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	// code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	// code
}

-(void) dealloc
{
	// code
    // Uninitialize() code here
    // opposite sequence of vao and vbo also ok
	if (vbo_position_plane)
	{
		glDeleteBuffers(1, &vbo_position_plane);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_position_plane = 0;
	}

	if (vao_plane)
	{
		glDeleteVertexArrays(1, &vao_plane);		// delete vertex array objects
		vao_plane = 0;
	}

	if (vbo_position_smoke)
	{
		glDeleteBuffers(1, &vbo_position_smoke);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_position_smoke = 0;
	}

	if (vao_smoke)
	{
		glDeleteVertexArrays(1, &vao_smoke);		// delete vertex array objects
		vao_smoke = 0;
	}

	if (vbo_position_I)
	{
		glDeleteBuffers(1, &vbo_position_I);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_position_I = 0;
	}

	if (vbo_color_I)
	{
		glDeleteBuffers(1, &vbo_color_I);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_color_I = 0;
	}

	if (vao_I)
	{
		glDeleteVertexArrays(1, &vao_I);		// delete vertex array objects
		vao_I = 0;
	}

	if (vbo_position_N)
	{
		glDeleteBuffers(1, &vbo_position_N);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_position_N = 0;
	}

	if (vbo_color_N)
	{
		glDeleteBuffers(1, &vbo_color_N);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_color_N = 0;
	}

	if (vao_N)
	{
		glDeleteVertexArrays(1, &vao_N);		// delete vertex array objects
		vao_N = 0;
	}

	if (vbo_position_D)
	{
		glDeleteBuffers(1, &vbo_position_D);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_position_D = 0;
	}

	if (vbo_color_D)
	{
		glDeleteBuffers(1, &vbo_color_D);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_color_D = 0;
	}

	if (vao_D)
	{
		glDeleteVertexArrays(1, &vao_D);		// delete vertex array objects
		vao_D = 0;
	}

	if (vbo_position_A)
	{
		glDeleteBuffers(1, &vbo_position_A);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_position_A = 0;
	}

	if (vbo_color_A)
	{
		glDeleteBuffers(1, &vbo_color_A);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_color_A = 0;
	}

	if (vao_A)
	{
		glDeleteVertexArrays(1, &vao_A);		// delete vertex array objects
		vao_A = 0;
	}

	if (vbo_position_A_band)
	{
		glDeleteBuffers(1, &vbo_position_A_band);	// delete named buffer objects
													// 1: number of buffer objects to be deleted
													// &vbo: array of buffer objects to be deleted
		vbo_position_A_band = 0;
	}

	if (vbo_color_A_band)
	{
		glDeleteBuffers(1, &vbo_color_A_band);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_color_A_band = 0;
	}

	if (vao_A_band)
	{
		glDeleteVertexArrays(1, &vao_A_band);		// delete vertex array objects
		vao_A_band = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;	// typedef int
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);		// since unused in Display()
		
		// ask program that how many shaders are attached to it
		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
			&shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);		// dynamic array for shaders since we don't know how many present
		if (pShaders)	// mem allocated
		{
			// take attached shaders  into above array
			glGetAttachedShaders(gShaderProgramObject,		// the program object to be queried
				shaderCount,								// the size of the array for storing the returned object names
				&shaderCount,								// Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
				pShaders									// an array that is used to return the names of attached shader objects(empty now)
			);		// return the handles of the shader objects attached to a program object

			for (shaderNumber = 0;shaderNumber < shaderCount;shaderNumber++)
			{
				// detach each shader
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
																					// 2nd para: Specifies the shader object to be detached.

				// delete each detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		// delete the shader program
		glDeleteProgram(gShaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
		gShaderProgramObject = 0;
		glUseProgram(0);
	}
    
    CVDisplayLinkStop(displayLink);     // stop
    CVDisplayLinkRelease(displayLink);  // release

	[super dealloc];    // can call Uninitialize() here
}

@end

// C style functions
void DrawI1(void)
{
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(i1_x, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_I);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	glBindVertexArray(0);
}

void DrawA()
{
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(a_x, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_A);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	glBindVertexArray(0);
}

void DrawN()
{
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(-letter_x, n_y, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_N);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	glBindVertexArray(0);
}

void DrawI2()
{
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(letter_x, i2_y, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_I);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

									// draw the necessary scene!
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	glBindVertexArray(0);
}

void DrawD()
{
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_D);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

	GLfloat DColor[] = {
		r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f,
		r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_saffron / 255.0f, g_saffron / 255.0f, b_saffron / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f,
		r_green / 255.0f, g_green / 255.0f, b_green / 255.0f
	};

	glBindBuffer(GL_ARRAY_BUFFER, vbo_color_D);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

													// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

													// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(DColor),			// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		DColor,					// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

			// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

										// draw the necessary scene!
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your 9-member array to start with (imp in inter-leaved)
		8						// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

			// unbind vao
	glBindVertexArray(0);
}

void DrawLeftUpperPlane()
{
	// function declarations
	void DrawPlane(void);

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	translationMatrix = vmath::translate(-2.0f*letter_x + letter_end, 1.22f, -3.0f) * vmath::translate(r*(GLfloat)cos(angle), r*(GLfloat)sin(angle), 0.0f);
	rotationMatrix = vmath::rotate(angle_upper_plane, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

																								// draw the necessary scene!
	DrawPlane();

	// unbind vao
	glBindVertexArray(0);
}

void DrawMiddlePlane()
{
	// function declarations
	void DrawPlane(void);

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(middle_plane_x, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

																								// draw the necessary scene!
	DrawPlane();
	// unbind vao
	glBindVertexArray(0);
}

void DrawLeftLowerPlane()
{
	// function declarations
	void DrawPlane(void);

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	translationMatrix = vmath::translate(-2.0f*letter_x + letter_end, -1.22f, -3.0f) * vmath::translate(r*(GLfloat)cos(angle_lower), r*(GLfloat)sin(angle_lower), 0.0f);
	rotationMatrix = vmath::rotate(angle_lower_plane, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading
																					// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glBindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

																								// draw the necessary scene!
	DrawPlane();

	// unbind vao
	glBindVertexArray(0);
}

void DrawLeftUpperSmoke(void)
{
	// variable declarations
	static GLfloat angle_smoke;

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(-2.0f*letter_x + letter_end, 1.22f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	glPointSize(2.0f);

	// saffron
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_saffron[3] = { (r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_saffron),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_white[3] = { r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_white),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI; angle_smoke <= angle; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_green[3] = { (r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_green),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)


		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);
}

void DrawSmokeMiddle(void)
{
	// function declarations
	void DrawI1();
	void DrawN();
	void DrawD();
	void DrawI2();
	void DrawA();

	// code
	glLineWidth(5.0f);
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glPointSize(2.0f);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);

	// saffron
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	GLfloat line_saffron[6] = { -letter_x * 5.4f, 0.0166f, 0.0f,
								 middle_plane_x, 0.0166f, 0.0f };

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(line_saffron),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		line_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)


	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your array to start with (imp in inter-leaved)
		2						// how many vertices to draw
	);

	// white
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	GLfloat line_white[6] = { -letter_x * 5.4f, 0.0f, 0.0f,
								middle_plane_x, 0.0f, 0.0f };

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(line_white),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		line_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)


	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your array to start with (imp in inter-leaved)
		2						// how many vertices to draw
	);

	// green
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	GLfloat line_green[6] = { -letter_x * 5.4f, -0.0173f, 0.0f,
								middle_plane_x, -0.0173f, 0.0f };

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(line_green),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		line_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)


	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your array to start with (imp in inter-leaved)
		2						// how many vertices to draw
	);

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);

	if (bDoneDetachPlanes == true)
	{
		glLineWidth(15.0f);

		DrawI1();
		DrawN();
		DrawD();
		DrawI2();
		DrawA();
	}
}

void DrawLeftLowerSmoke()
{
	// variable declarations
	static GLfloat angle_smoke;

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(-2.0f*letter_x + letter_end, -1.22f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glPointSize(2.0f);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);

	// saffron
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_saffron[3] = { (r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_saffron),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_white[3] = { r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_white),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI; angle_smoke >= angle_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_green[3] = { (r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_green),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)


		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);
}

void DrawABand()
{
	glLineWidth(5.0f);

	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(letter_x*2.0f, 0.0f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_A_band);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);

	// unbind vao
	glBindVertexArray(0);
}

void DrawRightUpperPlane()
{
	// function declarations
	void DrawPlane(void);

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	translationMatrix = vmath::translate(2.0f*letter_x - letter_end, 1.22f, -3.0f) * vmath::translate(r*(GLfloat)cos(angle_right_upper), r*(GLfloat)sin(angle_right_upper), 0.0f);
	rotationMatrix = vmath::rotate(angle_upper_plane, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

	// draw the necessary scene!
	DrawPlane();

	// unbind vao
	glBindVertexArray(0);
}

void DrawRightLowerPlane()
{
	// function declarations
	void DrawPlane(void);

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	translationMatrix = vmath::translate(2.0f*letter_x - letter_end, -1.22f, -3.0f) * vmath::translate(r*(GLfloat)cos(angle_right_lower), r*(GLfloat)sin(angle_right_lower), 0.0f);
	rotationMatrix = vmath::rotate(angle_lower_plane, 0.0f, 0.0f, 1.0f);
	modelViewMatrix = translationMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading
																					// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glBindVertexArray(vao_plane);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

										// draw the necessary scene!
	DrawPlane();

	// unbind vao
	glBindVertexArray(0);
}

void DrawRightUpperSmoke()
{
	// variable declarations
	static GLfloat angle_smoke;

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(2.0f*letter_x - letter_end, 1.22f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	glPointSize(2.0f);

	// saffron
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_saffron[3] = { (r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_saffron),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_white[3] = { r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_white),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = 3.0f*(GLfloat)M_PI / 2.0f + 0.17f*(GLfloat)M_PI; angle_smoke <= angle_right_upper; angle_smoke = angle_smoke + 0.005f)
	{
		GLfloat point_green[3] = { (r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_green),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)


		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);
}

void DrawRightLowerSmoke()
{
	// variable declarations
	static GLfloat angle_smoke;

	// code
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	modelViewMatrix = vmath::translate(2.0f*letter_x - letter_end, -1.22f, -3.0f);
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glPointSize(2.0f);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_smoke);		// arrays are in vbo and vbo is in vao. Hence, bind to vao
	glBindBuffer(GL_ARRAY_BUFFER, vbo_position_smoke);

	// saffron
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_saffron_smoke / 255.0f, g_saffron_smoke / 255.0f, b_saffron_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_saffron[3] = { (r + 0.018f)*(GLfloat)cos(angle_smoke), (r + 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_saffron),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_saffron,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// white	
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_white_smoke, r_white_smoke, r_white_smoke);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_white[3] = { r*(GLfloat)cos(angle_smoke), r*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_white),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_white,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)

		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}

	// green
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, r_green_smoke / 255.0f, g_green_smoke / 255.0f, b_green_smoke / 255.0f);	// saffron // specify the value of a generic vertex attribute(for single color to object)

	for (angle_smoke = (GLfloat)M_PI / 2.0f - 0.17f*(GLfloat)M_PI; angle_smoke >= angle_right_lower; angle_smoke = angle_smoke - 0.005f)
	{
		GLfloat point_green[3] = { (r - 0.018f)*(GLfloat)cos(angle_smoke), (r - 0.018f)*(GLfloat)sin(angle_smoke), 0.0f };

		// fill attributes
		glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
			sizeof(point_green),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
			point_green,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
			GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
		);		// creates a new data store for the buffer object currently bound to target(1st para.)


		glDrawArrays(GL_POINTS,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
			0,						// array position of your array to start with (imp in inter-leaved)
			1						// how many vertices to draw
		);
	}
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);
}

void DrawPlane(void)
{
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 186.0f / 255.0f, 226.0f / 255.0f, 238.0f / 255.0f);	// specify the value of a generic vertex attribute(for single color to object)

	// middle quad
	glDrawArrays(GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// upper wing
	glDrawArrays(GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		4,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// lower wing
	glDrawArrays(GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		8,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// back
	glDrawArrays(GL_TRIANGLE_FAN,// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		12,						// array position of your array to start with (imp in inter-leaved)
		4						// how many vertices to draw
	);
	// front triangle
	glDrawArrays(GL_TRIANGLES,	// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		16,						// array position of your array to start with (imp in inter-leaved)
		3						// how many vertices to draw
	);
	// IAF
	glLineWidth(3.0f);
	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 99.0f / 255.0f, 99.0f / 255.0f, 99.0f / 255.0f);	// dark gray // specify the value of a generic vertex attribute(for single color to object)

																							// I
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		19,						// array position of your array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);
	// A
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		25,						// array position of your array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);
	// F
	glDrawArrays(GL_LINES,		// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		31,						// array position of your array to start with (imp in inter-leaved)
		6						// how many vertices to draw
	);
}

void Update(void)
{
	if (bDoneI1 == false)
	{
		i1_x = i1_x + 0.0027f;

		if (i1_x >= -letter_x * 2)
		{
			i1_x = -letter_x * 2.0f;
			bDoneI1 = true;
		}
	}
	else if (bDoneA == false)
	{
		a_x = a_x - 0.0027f;

		if (a_x <= letter_x * 2)
		{
			a_x = letter_x * 2.0f;
			bDoneA = true;
		}
	}
	else if (bDoneN == false)
	{
		n_y = n_y - 0.0027f;

		if (n_y <= 0.0f)
		{
			bDoneN = true;
			n_y = 0.0f;
		}
	}
	else if (bDoneI2 == false)
	{
		i2_y = i2_y + 0.0027f;

		if (i2_y >= 0.0f)
		{
			i2_y = 0.0f;
			bDoneI2 = true;
		}
	}
	else if (bDoneD == false)
	{
		if (bDoneR_saffron == false)
		{
			r_saffron = r_saffron + 2.5f;
			if (r_saffron >= 255.0f)
			{
				r_saffron = 255.0f;
				bDoneR_saffron = true;
			}
		}
		if (bDoneG_saffron == false)
		{
			g_saffron = g_saffron + 1.0f;
			if (g_saffron >= 153.0f)
			{
				g_saffron = 153.0f;
				bDoneG_saffron = true;
			}
		}
		if (bDoneB_saffron == false)
		{
			b_saffron = b_saffron + 0.2f;
			if (b_saffron >= 51.0f)
			{
				b_saffron = 51.0f;
				bDoneB_saffron = true;
			}
		}

		// green
		if (bDoneR_green == false)
		{
			r_green = r_green + 0.1f;
			if (r_green >= 18.0f)
			{
				r_green = 18.0f;
				bDoneR_green = true;
			}
		}
		if (bDoneG_green == false)
		{
			g_green = g_green + 0.7f;
			if (g_green >= 136.0f)
			{
				g_green = 136.0f;
				bDoneG_green = true;
			}
		}
		if (bDoneB_green == false)
		{
			b_green = b_green + 0.1f;
			if (b_green >= 7.0f)
			{
				b_green = 7.0f;
				bDoneB_green = true;
			}
		}
		if (bDoneR_saffron == true && bDoneG_saffron == true && bDoneB_saffron == true && bDoneR_green == true && bDoneG_green == true && bDoneB_green == true)
			bDoneD = true;
	}
	else if (bDoneUpperPlane == false)
	{
		middle_plane_x = middle_plane_x + 0.0049f;


		if (angle_upper_plane <= 0.0f)
			angle_upper_plane = angle_upper_plane + 0.45f;

		if (angle_lower_plane >= 0.0f)
			angle_lower_plane = angle_lower_plane - 0.45f;

		if (r*(GLfloat)sin(angle) <= -1.22f)
		{
			bDoneUpperPlane = true;
			//fprintf_s(gpFile, "\nr*cos(angle)=%f", r*cos(angle));
			//fprintf_s(gpFile, "\nr*sin(angle)=%f", r*sin(angle));
			angle_upper_plane = 0.0f;
			angle_lower_plane = 0.0f;
		}

		angle = angle + 0.005f;
		angle_lower = angle_lower - 0.005f;
	}

	else if (bDoneMidddlePlane == false)
	{
		middle_plane_x = middle_plane_x + 0.0049f;
		if (middle_plane_x >= 2.0f*letter_x - letter_end + 0.724918f)
			bDoneMidddlePlane = true;
	}

	else if (bDoneDetachPlanes == false)
	{
		middle_plane_x = middle_plane_x + 0.0049f;

		angle_right_upper = angle_right_upper + 0.005f;
		angle_right_lower = angle_right_lower - 0.005f;

		if (angle_upper_plane <= 90.0f)
			angle_upper_plane = angle_upper_plane + 0.47f;

		if (angle_lower_plane >= -90.0f)
			angle_lower_plane = angle_lower_plane - 0.47f;

		if (middle_plane_x >= letter_x * 6.0f)
			bDoneDetachPlanes = true;
	}
	else if (bDoneFadingSmokes == false)
	{
		if (r_saffron_smoke > 0.0f)
			r_saffron_smoke = r_saffron_smoke - 2.5f;
		if (g_saffron_smoke > 0.0f)
			g_saffron_smoke = g_saffron_smoke - 1.0f;
		if (b_saffron_smoke > 0.0f)
			b_saffron_smoke = b_saffron_smoke - 0.2f;

		if (r_white_smoke > 0.0f)
			r_white_smoke = r_white_smoke - 0.03f;

		if (r_green_smoke > 0.0f)
			r_green_smoke = r_green_smoke - 0.1f;
		if (g_green_smoke > 0.0f)
			g_green_smoke = g_green_smoke - 0.7f;
		if (b_green_smoke > 0.0f)
			b_green_smoke = b_green_smoke - 0.1f;

		if (r_saffron_smoke <= 0 && g_saffron_smoke <= 0 && b_saffron_smoke <= 0 && r_white_smoke && r_green_smoke <= 0 && g_green_smoke <= 0 && b_green_smoke <= 0)
			bDoneFadingSmokes = true;
	}
}

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *pDisplayLinkContext)
{
    // *pNow: current time
    // *pDisplayLinkContext: object actually holding display link

    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];    // (GLView *): type-casting
    return(result);
} 

// calls: CVDisplayLinkStart -> MyDisplayLinkCallback -> getFrameForTime -> drawView
