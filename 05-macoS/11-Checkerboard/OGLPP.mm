// headers
// #import prevents recursive inclusion of header files
#import <Foundation/Foundation.h>	// for NSAutoreleasePool
#import <Cocoa/Cocoa.h>	// Cocoa.framework for GUI applications (~windows.h)	// Cocoa is SDK of Mac
#import <QuartzCore/CVDisplayLink.h>	// Core Video DisplayLink
#import <OpenGL/gl3.h>  // OGl 3.0 and onwards is PP
#import <OpenGL/gl3ext.h>   // with extensions
#import "vmath.h"

#define CHECK_IMAGE_WIDTH 64
#define CHECK_IMAGE_HEIGHT 64	// ?

enum	// nameless since we are just concerned with its "named" indices
{
    AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,		// 1
	AMC_ATTRIBUTE_NORMAL,		// 2
	AMC_ATTRIBUTE_TEXCOORD0,	// 3 (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// 'C' style global fuction declarations
CVReturn MyDisplayLinkCallback(
    CVDisplayLinkRef,       // which display link
    const CVTimeStamp *,    // current time
    const CVTimeStamp *,    // at what time to give output frame
    CVOptionFlags,          // reserved
    CVOptionFlags *,        // reserved
    void *                  // object with which this callback is registered
);

// global variable declarations
FILE *gpFile=NULL;
GLubyte checkImage[CHECK_IMAGE_WIDTH][CHECK_IMAGE_HEIGHT][4];
GLuint texImage;

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> // AppDelegate inherited from NSObject and will implement functions of NSApplicationDelegate and NSWindowDelegate
																			// AppDelegate: delegate of NSApplicationDelegate, NSWindowDelegate
@end

@interface GLView : NSOpenGLView		// this is delegate to none
@end

// entry-point function
int main(int argc, const char* argv[])
{
	// code
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];	// NSAutoreleasePool: interface(class); alloc increases ref. count by 1

	NSApp=[NSApplication sharedApplication];	// NSApp: object; NSApplication: interface; sharedApplication: static method

	[NSApp setDelegate:[[AppDelegate alloc]init]];		// setDelegate: instance method; alloc = allocate memory ~'new';  init: constructor(succeeding alloc)


	[NSApp run];		// run loop

	[pPool release];    // ref. count=0

	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private				// default; OK if not written
	NSWindow *window;	// their class
	GLView *glView;		// our class
}	// does not mean class ends here (@end means that)

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification		// callback function (OS will call this); ~WM_CREATE; from NSApplicationDelegate
{
	// code
    // log file (5 steps)
    // get the object of your application package (.app)
    NSBundle *mainBundle=[NSBundle mainBundle];     // 1st mainBundle: object; 2nd mainBundle: static method, alloc and init happen internally

    // using this NSBundle object, get the path of your .app directory
    NSString *appDirName=[mainBundle bundlePath];   

    // get the parent directory of .app directory in NSString form (e.g. /Desktop/ProjName)
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];     // we want log file not in .app directory (since write protected) but besides it
                                                                                
    // create log file path in NSString form
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];  // 1st @: literal; %@ can give you object's UUID
                                                                                            // e.g. of %@ : /Desktop/ProjName;     e.g. of %@/ : /Desktop/ProjName/

    // convert NSString into C string (i.e. const char *)
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];    // p: pointer; sz: C string
                                                                                                            // asking ASCII encoding for C string

    // now use this C string as 1st para. of fopen()
    gpFile=fopen(pszLogFileNameWithPath,"w");
    if(gpFile==NULL)
    {
        printf("Cannot Create Log File.\n Exiting..\n");      // Objective C's printf called NSLog() and it internally calls printf()
        [self release];
        [NSApp terminate:self];     // will go to applicationWillTerminate
    }

    fprintf(gpFile, "Program Started Successfully\n");

	// window
	NSRect win_rect;	// struct (no '*' after it!)   // C style
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);       // C style

	// create simple window
	window=[[NSWindow alloc] initWithContentRect:win_rect	// create window of this rect (named parameters of init)
							 styleMask:NSWindowStyleMaskTitled |NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable| NSWindowStyleMaskResizable		// window styles
							 backing:NSBackingStoreBuffered		// how to store window in memory
							 defer:NO];		// do immediate repainting? no

	[window setTitle:@"macOS OpenGL Window"];		// @: literal string= NSString
	[window center];

    // create view (canvas) by giving it frame
	glView=[[GLView alloc]initWithFrame:win_rect];      // creating nameless object of GLView (object chaining)
                                                        // initWithFrame's implementation in GLView

    // give this view to window
	[window setContentView:glView];

	[window setDelegate:self];      // self = AppDelegate
	[window makeKeyAndOrderFront:self];     // get key focus on window and put it topmost in z-order
                                            // window: will receive messages; self: will give messages
}

-(void)applicationWillTerminate:(NSNotification *)notification      // callback function (OS will call this); ~WM_DESTROY; from NSApplicationDelegate
{
	// code
	fprintf(gpFile, "Program Terminated Successfully\n");

    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification	// ~WM_CLOSE; of NSWindowDelegate; applicationWillTerminate will be called after this
{
	// code
	[NSApp terminate:self];     // ~DestroyWindow()
}

-(void)dealloc		// destructor	// release calls this
{
	// code
	[glView release];		// will call dealloc of MyView
	[window release];	// NSWindow's default dealloc will be called
	[super dealloc];	// tell parent
}
@end

@implementation GLView
{
@private				// default; OK if not written
    CVDisplayLinkRef displayLink;   // ~CVDisplayLink displayLink;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vao;		// vertex array object
    GLuint vbo_position, vbo_texture;		// vertex buffer object
    GLuint mvpUniform;		// mvp: model-view projection
    vmath::mat4 perspectiveProjectionMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array
}

-(id)initWithFrame:(NSRect)frame;   // generic return type
{
    // code
	self=[super initWithFrame:frame];   // get GLView's object from parent

	if(self)    // imp code inside this
	{
		[[self window]setContentView:self];     // get GLView's window whose view is GLView only; this step actually required for NextStep Interface Builder

        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // must specify the 4.1 core profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,     // PFA: Pixel Format Attribute  // LHS
            NSOpenGLProfileVersion4_1Core,  // Apple has locked OpenGL at 4.1 version   // RHS
            // specify the display id to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),     // map Core Graphics' display id to OpenGL id;
                                                                                            // in mac, variables starting with 'k' are kernel level #define's
            NSOpenGLPFAAccelerated,     // h/w accleration i.e. h/w rendering; give h/w supporting OpenGL
            NSOpenGLPFANoRecovery,      // if no h/w acceleration obtained, give error and exit
            NSOpenGLPFAColorSize,24,    // 32 also ok
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,     // A of RGBA
            NSOpenGLPFADoubleBuffer,
            0                           // last 0 is must; nil also ok
        };

        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];    // get pixel format from attributes
                                                                                                                // autorelease will give you explicit release call w/o expecting release call in main()
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available.\n Exiting..\n");      // Objective C's printf called NSLog() and it internally calls printf()
            [self release];
            [NSApp terminate:self];     // will go to applicationWillTerminate
        }

        // now context using pixelFormat
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext];      // automatically releases the older context if present and sets the newer one
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];    // pool signifies it(?) is thread

    [self drawView];    // our method

    [pool release];
    return(kCVReturnSuccess); 
}

-(void)prepareOpenGL    // NSOpenGLView's method which we implement; OS will call it; This method is called only once after the OpenGL context is made the current context.
{
	// function declarations
	void loadTexture(void);

    // code
    // OpenGL info
    fprintf(gpFile,"OpenGL Version: %s\n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];       // ~wglMakeCurrent, glxCurrent

    // decide double buffer's swap interval
    GLint swapInt=1;    // mac's default swapping interval is 1 i.e. 1 frame gap
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];       // forParameter: for whom to set; CP: Context Parameter
    
    // OpenGL code
    // vertex shader-
	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);	// creates a shader that is given as parameter	// 1st line of programmable pipeline !

	// write vertex shader source code
	const GLchar* vertexShaderSourceCode = 
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec2 vTexCoord;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec2 out_texCoord;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_texCoord = vTexCoord;" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
				// uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// gl_Position: in-built variable of shader	
				// out_color = vColor: assigning in attribute to out attribute
				// here, color is going to be given to frag color directly			

	// give above source code to the vertex shader object
	glShaderSource(
		gVertexShaderObject,	// shader to which the source code is to be given
		1,	// the number of strings in the array
		(const GLchar **)&vertexShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);	// replace the source code in a shader object

	// compile the vertex shader
	glCompileShader(gVertexShaderObject);
	// error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gVertexShaderObject,	// the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gVertexShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
											// When a shader object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nVertex Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

	// vertex converted to fragment now

    // fragment shader-
	// define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);	// creates a shader that is given as parameter

	// write fragment shader source code
	const GLchar* fragmentShaderSourceCode = 
		"#version 410 core" \
		"\n" \
		"in vec2 out_texCoord;" \
		"uniform sampler2D u_sampler;" \
		"out vec4 fragColor;" \
		"void main(void)" \
		"{" \
		"fragColor = texture(u_sampler,out_texCoord);"
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt
				// in vec4 out_color: VS's out is FS's in. Hence, same name. (gl_Position is in-built hence need not be passed)
				// out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
				// vec4: function/macro/constructor

	// give above source code to the fragment shader object
	glShaderSource(
		gFragmentShaderObject,	// shader to which the source code is to be given
		1,		// the number of strings in the array
		(const GLchar **)&fragmentShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);			// replace the source code in a shader object

				// compile the fragment shader
	glCompileShader(gFragmentShaderObject);
	// error checking
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gFragmentShaderObject,	//  the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gFragmentShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);						// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
										// When a shader object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nFragment Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

    // create shader program object 
	gShaderProgramObject = glCreateProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding of gShaderProgramObject to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_POSITION,					// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
		// give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_TEXCOORD0,					// the index of the generic vertex attribute to be bound.
		"vTexCoord"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	// link the shader program to your program
	glLinkProgram(gShaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
	// error checking (eg. of link error- version incompatibility of shaders)
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(				// iv= integer vector
		gShaderProgramObject,	//  the program to be queried.
		GL_LINK_STATUS,			// what(object parameter) is to be queried	
		&iProgramLinkStatus		// empty	//  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
	);							// return a parameter from a program object
	if (iProgramLinkStatus == GL_FALSE)	// error present
	{
		// check if the linker has any info. about the error
		glGetProgramiv(gShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetProgramInfoLog(
					gShaderProgramObject,	// the program object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
											// When a program object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nShader Program Link Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

    // post-linking retrieving uniform locations (uniforms are global to shaders)
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");	// preparation of data transfer from CPU to GPU (binding)
																				// u_mvp_matrix: of GPU; mvpUniform: of CPU
																				// telling it to take location of uniform u_mvp_matrix and give in mvpUniform

	// fill triangle vertices in array (this was in Display() in FFP)
	// texture
	// clockwise starting from left bottom
	const GLfloat texCoords[] = {
	0.0f,0.0f,
	0.0f,1.0f,
	1.0f,1.0f,
	1.0f,0.0f
	};

	// 9 lines
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao);		// generate vertex array object names
									// 1: Specifies the number of vertex array object names to generate
									// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
									// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao);		// bind a vertex array object

	// position
	// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_position);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
														// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
														// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(4*3*sizeof(GLfloat)),	// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		NULL,							// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	// texture
	glGenBuffers(1, &vbo_texture);		// generate buffer object names
										// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
										// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_texture);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(texCoords),				// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		texCoords,						// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,	// at CPU side; the index of the generic vertex attribute to be modified (send to vTexCoord)
		2,											// number of components per generic vertex attribute; x,y for TexCoord
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		0,											// the byte offset between consecutive generic vertex attributes; 0=no stride
		NULL										// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vTexCoord

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

    glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

	glEnable(GL_TEXTURE_2D);	// If enabled, two-dimensional texturing is performed

	// load texture
	loadTexture();

    // set background color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											// specifies clear values[0,1] used by glClear() for the color buffers

	perspectiveProjectionMatrix = vmath::mat4::identity();	// making orthographicProjectionMatrix an identity matrix(diagonals 1)
														// mat4: of vmath

    // game loop (6 steps)  (C functions concerned with CGL and CG)
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);  // create display link; creating display link= creating seperate thread

    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);

    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];    // openGLContext of type NSOpenGLContext; 2nd CGLContextObj is method

    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];  // self pixelFormat: obtain pixelFormat of self

    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

    CVDisplayLinkStart(displayLink);    // start display link; game loop starts here!
    
    [super prepareOpenGL];
}

-(void)reshape      // NSOpenGLView's method which we override;  Called by Cocoa when the view's visible rectangle or bounds change.
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);   // lock context so that it does not go to main thread but remains in this thread only
                                                                        // 2nd CGLContextObj is method

    NSRect rect=[self bounds];      // call bounds() on self (self's bounds)

    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if (height == 0)
	{
		height = 1;
	}

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    perspectiveProjectionMatrix=vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// ? other values of near & far not working
																											// parameters:
																											// fovy- The field of view angle, in degrees, in the y - direction.
																											// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																											// zNear- The distance from the viewer to the near clipping plane(always positive).
																											// zFar- The distance from the viewer to the far clipping plane(always positive).

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawRect:(NSRect)dirtyRect   // ~WM_PAINT
{
    // code
    [self drawView];    // for precaution, not compulsion (in case of thread switching i.e. if it goes in main thread during buffer swapping (to avoid flickering))
}

-(void)drawView     // ~Display()
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    // display code
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	glUseProgram(gShaderProgramObject);	// binding your OpenGL code with shader program object
										// Specifies the handle of the program object whose executables are to be used as part of current rendering state.

	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;

	// 1st quad
	// initialize above matrices to identity (not initialised in above step for better visibility and understanding)
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	// do necessary transformation (T,S,R)
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.6f);

	// do necessary matrix multiplication (done by gluOrtho2d() in FFP)
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

	// fill and send uniforms
	// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// texture
	// B: bind a named texture to a texturing target
	glBindTexture(GL_TEXTURE_2D, texImage);

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

	// fill rectangle vertices in array
	// clockwise starting from left bottom
	GLfloat rectanglePosition[] = {
		-2.0f, -1.0f, 0.0f,
		-2.0f, 1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,
		0.0f, -1.0f, 0.0f };

	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);	// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(rectanglePosition),	// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		rectanglePosition,			// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	// draw the necessary scene!
	glDrawArrays(GL_TRIANGLE_FAN,	// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,							// array position of your 9-member array to start with (imp in inter-leaved)
		4							// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

	// 2nd quad
	// initialize above matrices to identity again
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();

	// do necessary transformation (T,S,R)
	modelViewMatrix = vmath::translate(0.0f, 0.0f, -3.6f);

	// do necessary matrix multiplication (done by gluOrtho2d() in FFP)
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

																					// fill and send uniforms
																					// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	// clockwise starting from left bottom
	rectanglePosition[0] = 1.0f;
	rectanglePosition[1] = -1.0f;
	rectanglePosition[2] = 0.0f;
	rectanglePosition[3] = 1.0f;
	rectanglePosition[4] = 1.0f;
	rectanglePosition[5] = 0.0f;
	rectanglePosition[6] = 2.41421f;
	rectanglePosition[7] = 1.0f;
	rectanglePosition[8] = -1.41421f;
	rectanglePosition[9] = 2.41421f;
	rectanglePosition[10] = -1.0f;
	rectanglePosition[11] = -1.41421f;


	glBindBuffer(GL_ARRAY_BUFFER, vbo_position);	// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
													// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
													// vbo: bind this (the name of a buffer object.)

	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,	// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(rectanglePosition),	// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		rectanglePosition,			// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_DYNAMIC_DRAW				// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	// draw the necessary scene!
	glDrawArrays(GL_TRIANGLE_FAN,	// what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,							// array position of your 9-member array to start with (imp in inter-leaved)
		4							// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn

	// unbind texture
	glBindTexture(GL_TEXTURE_2D, 0);

	// unbind vao
	glBindVertexArray(0);

	// unuse program
	glUseProgram(0);	// unbinding your OpenGL code with shader program object
						// If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);       // ~glSwapBuffers() ; for multi-threaded rendering
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder		// telling window that give your event handling to your view
{
	// code
	[[self window]makeFirstResponder:self];
	return(YES);		// YES means method is implemented
}

-(void)keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:    // esc key
            [self release];
            [NSApp terminate:self];
            break;

        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];	// toggleFullScreen: its method; repainting occurs automatically
            break;

        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
	// code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	// code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	// code
}

-(void) dealloc
{
	// code
    // Uninitialize() code here
	if (vbo_texture)
	{
		glDeleteBuffers(1, &vbo_texture);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_texture = 0;
	}

	if (vbo_position)
	{
		glDeleteBuffers(1, &vbo_position);	// delete named buffer objects
											// 1: number of buffer objects to be deleted
											// &vbo: array of buffer objects to be deleted
		vbo_position = 0;
	}

	if (vao)
	{
		glDeleteVertexArrays(1, &vao);		// delete vertex array objects
		vao = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;	// typedef int
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);		// since unused in Display()
		
		// ask program that how many shaders are attached to it
		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
			&shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);		// dynamic array for shaders since we don't know how many present
		if (pShaders)	// mem allocated
		{
			// take attached shaders  into above array
			glGetAttachedShaders(gShaderProgramObject,		// the program object to be queried
				shaderCount,								// the size of the array for storing the returned object names
				&shaderCount,								// Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
				pShaders									// an array that is used to return the names of attached shader objects(empty now)
			);		// return the handles of the shader objects attached to a program object

			for (shaderNumber = 0;shaderNumber < shaderCount;shaderNumber++)
			{
				// detach each shader
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
																					// 2nd para: Specifies the shader object to be detached.

				// delete each detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		// delete the shader program
		glDeleteProgram(gShaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
		gShaderProgramObject = 0;
		glUseProgram(0);
	}
    
    CVDisplayLinkStop(displayLink);     // stop
    CVDisplayLinkRelease(displayLink);  // release

	[super dealloc];    // can call Uninitialize() here
}

@end

// C style functions
void loadTexture(void)
{
	// function declaration
	void makeCheckImage(void);

	// code
	makeCheckImage();

	// OpenGL functions now
	// set pixel storage mode
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);	// GL_UNPACK_ALIGNMENT : Specifies the alignment requirements for the start of each pixel row in memory
											// 1 : 1 byte alignment between rows of image

	// allocate memory for textures on GPU and obtain its id in 'texture'
	glGenTextures(1, &texImage);	// 1 : how many textures to generate
									// &texImage : starting addr of the array of textures returned as an id

	// bind every element of the array to a target texture (type of texture)
	glBindTexture(GL_TEXTURE_2D, texImage);	// binding the first texture of array(value) to a 2D array type usage
											// Texture targets become aliases for textures currently bound to them

	// set the state/parameters of the texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// GL_TEXTURE_WRAP_S : wrap the tex horizontally. Sets the wrap parameter for texture coordinate s to either GL_CLAMP or GL_REPEAT
																	// GL_TEXTURE_WRAP_S : parameter to be tweaked
																	// GL_REPEAT : tell OGL to repeat pattern

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);	// GL_TEXTURE_WRAP_T : wrap the tex vertically

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);	// when model is near the viewer; pixel being textured maps to an area lesser than one texture element
																		// sets the texture magnification function to either GL_NEAREST(better performance) or GL_LINEAR(better quality due to interpolation)
																		// GL_TEXTURE_MAG_FILTER : parameter to be tweaked
																		// GL_NEAREST : since we have a pattern (no need of fine quality)

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);	// when model is away from the viewer; pixel being textured maps to an area greater than one texture element

																		// fill the data
	glTexImage2D(GL_TEXTURE_2D,	// target
		0,						// levels of mipmaps
		GL_RGBA,				// the number of color components in the texture (called internal image format)
		CHECK_IMAGE_WIDTH,		// width of the texture image
		CHECK_IMAGE_HEIGHT,		// height of the texture image
		0,						// border width (no border)
		GL_RGBA,				// format of the pixel data (last parameter)
		GL_UNSIGNED_BYTE,		// data type of the pixel data (last parameter)
		checkImage				// address of the image data
	);	// specifies a two-dimensional texture image.	// ?
}

// creating the texture through Maths
void makeCheckImage(void)
{
	// variable declarations
	int i, j, c;

	// code
	for (i = 0;i < CHECK_IMAGE_HEIGHT;i++)
	{
		for (j = 0;j < CHECK_IMAGE_WIDTH;j++)
		{
			c = (((i & 0x8) == 0) ^ ((j & 0x8) == 0)) * 255;	// gives color	// ?

			checkImage[i][j][0] = (GLubyte)c;	// R
			checkImage[i][j][1] = (GLubyte)c;	// G
			checkImage[i][j][2] = (GLubyte)c;	// B
			checkImage[i][j][3] = 255;			// A
		}
	}
}

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *pDisplayLinkContext)
{
    // *pNow: current time
    // *pDisplayLinkContext: object actually holding display link

    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];    // (GLView *): type-casting
    return(result);
} 

// calls: CVDisplayLinkStart -> MyDisplayLinkCallback -> getFrameForTime -> drawView
