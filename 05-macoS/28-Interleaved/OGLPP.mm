// headers
// #import prevents recursive inclusion of header files
#import <Foundation/Foundation.h>	// for NSAutoreleasePool
#import <Cocoa/Cocoa.h>	// Cocoa.framework for GUI applications (~windows.h)	// Cocoa is SDK of Mac
#import <QuartzCore/CVDisplayLink.h>	// Core Video DisplayLink
#import <OpenGL/gl3.h>  // OGl 3.0 and onwards is PP
#import <OpenGL/gl3ext.h>   // with extensions
#import "vmath.h"

// global variable declarations
FILE *gpFile=NULL;
GLfloat angle_cube = 0.0f;

enum	// nameless since we are just concerned with its "named" indices
{
    AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,		// 1
	AMC_ATTRIBUTE_NORMAL,		// 2
	AMC_ATTRIBUTE_TEXCOORD0,	// 3 (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

GLfloat light_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_diffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[4] = { 0.0f,0.0f,0.0f,1.0f };

GLfloat material_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 128.0f;

// 'C' style global fuction declarations
CVReturn MyDisplayLinkCallback(
    CVDisplayLinkRef,       // which display link
    const CVTimeStamp *,    // current time
    const CVTimeStamp *,    // at what time to give output frame
    CVOptionFlags,          // reserved
    CVOptionFlags *,        // reserved
    void *                  // object with which this callback is registered
);

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> // AppDelegate inherited from NSObject and will implement functions of NSApplicationDelegate and NSWindowDelegate
																			// AppDelegate: delegate of NSApplicationDelegate, NSWindowDelegate
@end

@interface GLView : NSOpenGLView		// this is delegate to none
@end

// entry-point function
int main(int argc, const char* argv[])
{
	// code
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];	// NSAutoreleasePool: interface(class); alloc increases ref. count by 1

	NSApp=[NSApplication sharedApplication];	// NSApp: object; NSApplication: interface; sharedApplication: static method

	[NSApp setDelegate:[[AppDelegate alloc]init]];		// setDelegate: instance method; alloc = allocate memory ~'new';  init: constructor(succeeding alloc)


	[NSApp run];		// run loop

	[pPool release];    // ref. count=0

	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private				// default; OK if not written
	NSWindow *window;	// their class
	GLView *glView;		// our class
}	// does not mean class ends here (@end means that)

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification		// callback function (OS will call this); ~WM_CREATE; from NSApplicationDelegate
{
	// code
    // log file (5 steps)
    // get the object of your application package (.app)
    NSBundle *mainBundle=[NSBundle mainBundle];     // 1st mainBundle: object; 2nd mainBundle: static method, alloc and init happen internally

    // using this NSBundle object, get the path of your .app directory
    NSString *appDirName=[mainBundle bundlePath];   

    // get the parent directory of .app directory in NSString form (e.g. /Desktop/ProjName)
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];     // we want log file not in .app directory (since write protected) but besides it
                                                                                
    // create log file path in NSString form
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];  // 1st @: literal; %@ can give you object's UUID
                                                                                            // e.g. of %@ : /Desktop/ProjName;     e.g. of %@/ : /Desktop/ProjName/

    // convert NSString into C string (i.e. const char *)
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];    // p: pointer; sz: C string
                                                                                                            // asking ASCII encoding for C string

    // now use this C string as 1st para. of fopen()
    gpFile=fopen(pszLogFileNameWithPath,"w");
    if(gpFile==NULL)
    {
        printf("Cannot Create Log File.\n Exiting..\n");      // Objective C's printf called NSLog() and it internally calls printf()
        [self release];
        [NSApp terminate:self];     // will go to applicationWillTerminate
    }

    fprintf(gpFile, "Program Started Successfully\n");

	// window
	NSRect win_rect;	// struct (no '*' after it!)   // C style
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);       // C style

	// create simple window
	window=[[NSWindow alloc] initWithContentRect:win_rect	// create window of this rect (named parameters of init)
							 styleMask:NSWindowStyleMaskTitled |NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable| NSWindowStyleMaskResizable		// window styles
							 backing:NSBackingStoreBuffered		// how to store window in memory
							 defer:NO];		// do immediate repainting? no

	[window setTitle:@"macOS OpenGL Window"];		// @: literal string= NSString
	[window center];

    // create view (canvas) by giving it frame
	glView=[[GLView alloc]initWithFrame:win_rect];      // creating nameless object of GLView (object chaining)
                                                        // initWithFrame's implementation in GLView

    // give this view to window
	[window setContentView:glView];

	[window setDelegate:self];      // self = AppDelegate
	[window makeKeyAndOrderFront:self];     // get key focus on window and put it topmost in z-order
                                            // window: will receive messages; self: will give messages
}

-(void)applicationWillTerminate:(NSNotification *)notification      // callback function (OS will call this); ~WM_DESTROY; from NSApplicationDelegate
{
	// code
	fprintf(gpFile, "Program Terminated Successfully\n");

    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification	// ~WM_CLOSE; of NSWindowDelegate; applicationWillTerminate will be called after this
{
	// code
	[NSApp terminate:self];     // ~DestroyWindow()
}

-(void)dealloc		// destructor	// release calls this
{
	// code
	[glView release];		// will call dealloc of MyView
	[window release];	// NSWindow's default dealloc will be called
	[super dealloc];	// tell parent
}
@end

@implementation GLView
{
@private				// default; OK if not written
    CVDisplayLinkRef displayLink;   // ~CVDisplayLink displayLink;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

	GLuint texture_marble;
    GLuint vao_cube;		// vertex array object	(1 also ok since we unbind)
	GLuint vbo_pctn_cube;		// vertex buffer object
	GLuint mUniform;		// model matrix
	GLuint vUniform;		// view matrix	// taken seperate for freedom
	GLuint pUniform;	// projection; seperate since calculation of mormal matrix needs mv matrix only
	GLuint samplerUniform;
	GLuint laUniform;	// intensity of ambient light
	GLuint ldUniform;	// intensity of diffused light
	GLuint lsUniform;	// intensity of specular light
	GLuint lightPositionUniform;
	GLuint kaUniform;	// coefficient of material's ambient reflectivity
	GLuint kdUniform;	// coefficient of material's diffused reflectivity
	GLuint ksUniform;	// coefficient of material's specular reflectivity 
	GLuint materialShininessUniform; // material shininess

	vmath::mat4 perspectiveProjectionMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array
}

-(id)initWithFrame:(NSRect)frame;   // generic return type
{
    // code
	self=[super initWithFrame:frame];   // get GLView's object from parent

	if(self)    // imp code inside this
	{
		[[self window]setContentView:self];     // get GLView's window whose view is GLView only; this step actually required for NextStep Interface Builder

        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // must specify the 4.1 core profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,     // PFA: Pixel Format Attribute  // LHS
            NSOpenGLProfileVersion4_1Core,  // Apple has locked OpenGL at 4.1 version   // RHS
            // specify the display id to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),     // map Core Graphics' display id to OpenGL id;
                                                                                            // in mac, variables starting with 'k' are kernel level #define's
            NSOpenGLPFAAccelerated,     // h/w accleration i.e. h/w rendering; give h/w supporting OpenGL
            NSOpenGLPFANoRecovery,      // if no h/w acceleration obtained, give error and exit
            NSOpenGLPFAColorSize,24,    // 32 also ok
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,     // A of RGBA
            NSOpenGLPFADoubleBuffer,
            0                           // last 0 is must; nil also ok
        };

        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];    // get pixel format from attributes
                                                                                                                // autorelease will give you explicit release call w/o expecting release call in main()
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available.\n Exiting..\n");      // Objective C's printf called NSLog() and it internally calls printf()
            [self release];
            [NSApp terminate:self];     // will go to applicationWillTerminate
        }

        // now context using pixelFormat
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext];      // automatically releases the older context if present and sets the newer one
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];    // pool signifies it(?) is thread

    [self drawView];    // our method

    [pool release];
    return(kCVReturnSuccess); 
}

-(void)prepareOpenGL    // NSOpenGLView's method which we implement; OS will call it; This method is called only once after the OpenGL context is made the current context.
{
	// function declarations
	void LoadMesh(void);

    // code
    // OpenGL info
    fprintf(gpFile,"OpenGL Version: %s\n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];       // ~wglMakeCurrent, glxCurrent

    // decide double buffer's swap interval
    GLint swapInt=1;    // mac's default swapping interval is 1 i.e. 1 frame gap
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];       // forParameter: for whom to set; CP: Context Parameter
    
    // OpenGL code
    // vertex shader-
	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);	// creates a shader that is given as parameter	// 1st line of programmable pipeline !

	// write vertex shader source code
	const GLchar* vertexShaderSourceCode = 
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"in vec2 vTexCoord;" \
		"in vec3 vNormal;" \
		"out vec4 out_color;" \
		"out vec2 out_texCoord;" \
		"out vec3 t_norm;" \
		"out vec3 light_direction;" \
		"out vec3 viewer_vector;" \
		"uniform mat4 u_m_matrix;" \
		"uniform mat4 u_v_matrix;" \
		"uniform mat4 u_p_matrix;" \
		"uniform vec4 u_light_position;" \
		"void main(void)" \
		"{" \
		"vec4 eye_coordinates =  u_v_matrix * u_m_matrix * vPosition;" \
		"t_norm = mat3(u_v_matrix * u_m_matrix) * vNormal;" \
		"light_direction = vec3(u_light_position - eye_coordinates);" \
		"viewer_vector = vec3(-eye_coordinates.xyz);" \
		"gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
		"out_color = vColor;" \
		"out_texCoord = vTexCoord;" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
				// uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// gl_Position: in-built variable of shader	
				// out_color = vColor: assigning in attribute to out attribute
				// here, color is going to be given to frag color directly			

	// give above source code to the vertex shader object
	glShaderSource(
		gVertexShaderObject,	// shader to which the source code is to be given
		1,	// the number of strings in the array
		(const GLchar **)&vertexShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);	// replace the source code in a shader object

	// compile the vertex shader
	glCompileShader(gVertexShaderObject);
	// error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gVertexShaderObject,	// the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gVertexShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
											// When a shader object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nVertex Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

	// vertex converted to fragment now

    // fragment shader-
	// define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);	// creates a shader that is given as parameter

	// write fragment shader source code
	const GLchar* fragmentShaderSourceCode = 
		"#version 410 core" \
		"\n" \
		"in vec4 out_color;" \
		"in vec2 out_texCoord;" \
		"in vec3 t_norm;" \
		"in vec3 light_direction;" \
		"in vec3 viewer_vector;" \
		"out vec4 fragColor;" \
		"uniform vec3 u_la;" \
		"uniform vec3 u_ld;" \
		"uniform vec3 u_ls;" \
		"uniform vec3 u_ka;" \
		"uniform vec3 u_kd;" \
		"uniform vec3 u_ks;" \
		"uniform float u_material_shininess;" \
		"uniform sampler2D u_sampler;" \
		"void main(void)" \
		"{" \
		"vec3 normalized_t_norm = normalize(t_norm);" \
		"vec3 normalized_light_direction = normalize(light_direction);" \
		"vec3 normalized_viewer_vector = normalize(viewer_vector);" \
		"vec3 reflection_vector = reflect(-normalized_light_direction,normalized_t_norm);" \
		"float tn_dot_lightDirection = max(dot(normalized_light_direction,normalized_t_norm),0.0);" \
		"vec3 ambient = u_la * u_ka;" \
		"vec3 diffuse = u_ld * u_kd * tn_dot_lightDirection;" \
		"vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_viewer_vector),0.0),u_material_shininess);" \
		"vec3 phong_ads_light = ambient + diffuse + specular;" \
		"vec4 tex = texture(u_sampler,out_texCoord);" \
		"fragColor = out_color * tex * vec4(phong_ads_light,1.0);"
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt
				// in vec4 out_color: VS's out is FS's in. Hence, same name. (gl_Position is in-built hence need not be passed)
				// out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
				// vec4: function/macro/constructor

	// give above source code to the fragment shader object
	glShaderSource(
		gFragmentShaderObject,	// shader to which the source code is to be given
		1,		// the number of strings in the array
		(const GLchar **)&fragmentShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);			// replace the source code in a shader object

				// compile the fragment shader
	glCompileShader(gFragmentShaderObject);
	// error checking
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gFragmentShaderObject,	//  the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gFragmentShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);						// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
										// When a shader object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nFragment Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

    // create shader program object 
	gShaderProgramObject = glCreateProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding of gShaderProgramObject to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_POSITION,					// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
		// give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_COLOR,					// the index of the generic vertex attribute to be bound.
		"vColor"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_TEXCOORD0,					// the index of the generic vertex attribute to be bound.
		"vTexCoord"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_NORMAL,					// the index of the generic vertex attribute to be bound.
		"vNormal"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	// link the shader program to your program
	glLinkProgram(gShaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
	// error checking (eg. of link error- version incompatibility of shaders)
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(				// iv= integer vector
		gShaderProgramObject,	//  the program to be queried.
		GL_LINK_STATUS,			// what(object parameter) is to be queried	
		&iProgramLinkStatus		// empty	//  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
	);							// return a parameter from a program object
	if (iProgramLinkStatus == GL_FALSE)	// error present
	{
		// check if the linker has any info. about the error
		glGetProgramiv(gShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetProgramInfoLog(
					gShaderProgramObject,	// the program object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
											// When a program object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nShader Program Link Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

    // post-linking retrieving uniform locations (uniforms are global to shaders)
	mUniform = glGetUniformLocation(gShaderProgramObject, "u_m_matrix");	// preparation of data transfer from CPU to GPU (binding)
																				// u_mvp_matrix: of GPU; mvpUniform: of CPU
																				// telling it to take location of uniform u_mvp_matrix and give in mvpUniform
	vUniform = glGetUniformLocation(gShaderProgramObject, "u_v_matrix");
	pUniform = glGetUniformLocation(gShaderProgramObject, "u_p_matrix");
	laUniform = glGetUniformLocation(gShaderProgramObject, "u_la");
	ldUniform = glGetUniformLocation(gShaderProgramObject, "u_ld");
	lsUniform = glGetUniformLocation(gShaderProgramObject, "u_ls");
	lightPositionUniform = glGetUniformLocation(gShaderProgramObject, "u_light_position");
	kaUniform = glGetUniformLocation(gShaderProgramObject, "u_ka");
	kdUniform = glGetUniformLocation(gShaderProgramObject, "u_kd");
	ksUniform = glGetUniformLocation(gShaderProgramObject, "u_ks");
	materialShininessUniform = glGetUniformLocation(gShaderProgramObject, "u_material_shininess");
	samplerUniform = glGetUniformLocation(gShaderProgramObject, "u_sampler");

	// actually it should be PCNT
	const GLfloat cubePCTN[] = {
		// top
		1.0f, 1.0f, -1.0f,		// P
		1.0f,0.0f,0.0f,			// C
		1.0f, 1.0f,				// T
		0.0f, 1.0f, 0.0f,		// N

		-1.0f, 1.0f, -1.0f,		// P
		1.0f,0.0f,0.0f,			// C
		0.0f, 1.0f,				// T
		0.0f, 1.0f, 0.0f,		// N

		-1.0f, 1.0f, 1.0f,
		1.0f,0.0f,0.0f,
		0.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		1.0f, 1.0f, 1.0f,
		1.0f,0.0f,0.0f,
		1.0f, 0.0f,
		0.0f, 1.0f, 0.0f,

		// bottom
		1.0f, -1.0f, -1.0f,
		0.0f,1.0f,0.0f,
		0.0f, 0.0f,
		0.0f, -1.0f, 0.0f,

		-1.0f, -1.0f, -1.0f,
		0.0f,1.0f,0.0f,
		1.0f, 0.0f,
		0.0f, -1.0f, 0.0f,

		-1.0f, -1.0f, 1.0f,
		0.0f,1.0f,0.0f,
		1.0f, 1.0f,
		0.0f, -1.0f, 0.0f,

		1.0f, -1.0f, 1.0f,
		0.0f,1.0f,0.0f,
		0.0f, 1.0f,
		0.0f, -1.0f, 0.0f,

		// front
		1.0f, 1.0f, 1.0f,
		0.0f,0.0f,1.0f,
		1.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		-1.0f, 1.0f, 1.0f,
		0.0f,0.0f,1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f, 1.0f,

		-1.0f, -1.0f, 1.0f,
		0.0f,0.0f,1.0f,
		0.0f, 0.0f,
		0.0f, 0.0f, 1.0f,

		1.0f, -1.0f, 1.0f,
		0.0f,0.0f,1.0f,
		1.0f, 0.0f,
		0.0f, 0.0f, 1.0f,

		// back
		-1.0f, 1.0f, -1.0f,
		0.0f,1.0f,1.0f,
		1.0f, 1.0f,
		0.0f, 0.0f, -1.0f,

		1.0f, 1.0f, -1.0f,
		0.0f,1.0f,1.0f,
		0.0f, 1.0f,
		0.0f, 0.0f, -1.0f,

		1.0f, -1.0f, -1.0f,
		0.0f,1.0f,1.0f,
		0.0f, 0.0f,
		0.0f, 0.0f, -1.0f,

		-1.0f, -1.0f, -1.0f,
		0.0f,1.0f,1.0f,
		0.0f, 0.0f,
		0.0f, 0.0f, -1.0f,

		// right
		1.0f, 1.0f, -1.0f,
		1.0f,0.0f,1.0f,
		1.0f, 1.0f,
		1.0f, 0.0f, 0.0f,

		1.0f, 1.0f, 1.0f,
		1.0f,0.0f,1.0f,
		0.0f, 1.0f,
		1.0f, 0.0f, 0.0f,

		1.0f, -1.0f, 1.0f,
		1.0f,0.0f,1.0f,
		0.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		1.0f, -1.0f, -1.0f,
		1.0f,0.0f,1.0f,
		1.0f, 0.0f,
		1.0f, 0.0f, 0.0f,

		// left
		-1.0f, 1.0f, 1.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 1.0f,
		-1.0f, 0.0f, 0.0f,

		-1.0f, 1.0f, -1.0f,
		1.0f, 1.0f, 0.0f,
		0.0f, 1.0f,
		-1.0f, 0.0f, 0.0f,

		-1.0f, -1.0f, -1.0f,
		1.0f, 1.0f, 0.0f,
		0.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,

		-1.0f, -1.0f, 1.0f,
		1.0f, 1.0f, 0.0f,
		1.0f, 0.0f,
		-1.0f, 0.0f, 0.0f,
	};

	// fill cube vertices in array (this was in Display() in FFP)
	// cube
	// create vao(id) (vao is shape-wise)
	glGenVertexArrays(1, &vao_cube);		// generate vertex array object names
											// 1: Specifies the number of vertex array object names to generate
											// &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
											// everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()

	glBindVertexArray(vao_cube);		// bind a vertex array object

										// cube position
										// create vbo (vbo is attribute-wise)
	glGenBuffers(1, &vbo_pctn_cube);		// generate buffer object names
												// 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
												// &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)

	glBindBuffer(GL_ARRAY_BUFFER, vbo_pctn_cube);		// creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
															// GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
															// vbo: bind this (the name of a buffer object.)

	// all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)
	// fill attributes
	glBufferData(GL_ARRAY_BUFFER,		// target buffer object (give data to GL_ARRAY_BUFFER)
		sizeof(cubePCTN),		// size of array in which data is to be provided; size in bytes of the buffer object's new data store
		cubePCTN,				// actual array in which data is present; pointer to data that will be copied into the data store for initialization
		GL_STATIC_DRAW					// when to give data (statically=now); static= The data store contents will be modified once and used many times.
	);		// creates a new data store for the buffer object currently bound to target(1st para.)

	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		11 * sizeof(GLfloat),						// the byte offset between consecutive generic vertex attributes; 0=no stride
		0					// pointer to the first component of the first generic vertex attribute in the array; offset if V,C,T,N stored in single array (inter-leaved). NULL: no offset since no stride;
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		11*sizeof(GLfloat),							// the byte offset between consecutive generic vertex attributes; 0=no stride
		(void*)(3 * sizeof(GLfloat))				// pointer to the first component of the first generic vertex attribute in the array; offset if V,C,T,N stored in single array (inter-leaved). NULL: no offset since no stride;
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
													// enables vPosition

	glVertexAttribPointer(AMC_ATTRIBUTE_TEXCOORD0,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		2,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		11*sizeof(GLfloat),							// the byte offset between consecutive generic vertex attributes; 0=no stride
		(void*)(6 * sizeof(GLfloat))				// offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_TEXCOORD0);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vPosition

	glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL,	// at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
		3,											// number of components per generic vertex attribute; x,y,z for position
		GL_FLOAT,									// data type of each component in the array
		GL_FALSE,									// is data normalised
		11 * sizeof(GLfloat),						// the byte offset between consecutive generic vertex attributes; 0=no stride
		(void*)(8 * sizeof(GLfloat))										// pointer to the first component of the first generic vertex attribute in the array; offset if V,C,T,N stored in single array (inter-leaved). NULL: no offset since no stride;
	);		// define an array of generic vertex attribute data

	glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);	// para: Specifies the index of the generic vertex attribute to be enabled or disabled.
														// enables vNormal

	// unbind (LIFO)
	glBindBuffer(GL_ARRAY_BUFFER, 0);	// 0= unbind; unbind vbo
										// to bind with next buffer

	glBindVertexArray(0);		// unbind vao

    glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

	glEnable(GL_TEXTURE_2D);	// If enabled, two-dimensional texturing is performed

	// load texture
	texture_marble=[self loadTextureFromBMPFile:"marble.bmp"];

    // set background color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											// specifies clear values[0,1] used by glClear() for the color buffers

	perspectiveProjectionMatrix = vmath::mat4::identity();	// making orthographicProjectionMatrix an identity matrix(diagonals 1)
														// mat4: of vmath

    // game loop (6 steps)  (C functions concerned with CGL and CG)
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);  // create display link; creating display link= creating seperate thread

    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);

    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];    // openGLContext of type NSOpenGLContext; 2nd CGLContextObj is method

    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];  // self pixelFormat: obtain pixelFormat of self

    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

    CVDisplayLinkStart(displayLink);    // start display link; game loop starts here!
    
    [super prepareOpenGL];
}

-(GLuint)loadTextureFromBMPFile:(const char*)texFileName
{
	// get the object of your application package (.app)
    NSBundle *mainBundle=[NSBundle mainBundle];     // 1st mainBundle: object; 2nd mainBundle: static method, alloc and init happen internally

    // using this NSBundle object, get the path of your .app directory
    NSString *appDirName=[mainBundle bundlePath];   

    // get the parent directory of .app directory in NSString form (e.g. /Desktop/ProjName)
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];     // texture file not in .app directory but besides it
                                                                                
    // create texture file path in NSString form
    NSString *textureFileNameWithPath=[NSString stringWithFormat:@"%@/%s",parentDirPath, texFileName];  // %@: parent directory path
                                                                                            			// %S: texFileName parameter
	// no need to convert to C string as no fopen() reqd.

	// 5 steps:
	NSImage *bmpImage=[[NSImage alloc]initWithContentsOfFile:textureFileNameWithPath];	// constructor

	if(!bmpImage)
	{
		NSLog(@"Can't find %@", textureFileNameWithPath);
		return(0);
	}

	CGImageRef cgImage = [bmpImage CGImageForProposedRect:nil 	// give for this rect; nil forces Cocoa to get w,h and origin of the view rectangle; CGImageForProposedRect is method name here and 1st is unnamed para.
								   context:nil 					// it wants CGContext (a graphics context). We have OGL context. Hence nil
								   hints:nil];					// info. about image; nil=telling it to take by itself

	// C style functions
	int w = (int)CGImageGetWidth(cgImage);		// gives size_t
	int h = (int)CGImageGetHeight(cgImage);

	CFDataRef imageData = CGDataProviderCopyData(CGImageGetDataProvider(cgImage));	

	void *pixels = (void*)CFDataGetBytePtr(imageData);

	GLuint texture;

	// set pixel storage mode
	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);	// GL_UNPACK_ALIGNMENT : Specifies the alignment requirements for the start of each pixel row in memory
											// set 1 rather than default 4 for better performance

	// allocate memory for textures on GPU and obtain its id in 'bmpTexture'
	glGenTextures(1, &texture);	// 1 : how many textures to generate
									// bmpTexture : starting addr of the array of textures returned as an id

	// bind every element of the array to a target texture (type of texture)
	glBindTexture(GL_TEXTURE_2D, texture);	// binding the first texture(value) to a 2D array type usage
											// *Texture targets become aliases for textures currently bound to them*

	// set the state/parameters of the texture
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);	// when model is near the viewer; pixel being textured maps to an area lesser than one texture element
																		// sets the texture magnification function to either GL_NEAREST(better performance) or GL_LINEAR(better quality due to interpolation)
																		// GL_TEXTURE_MAG_FILTER : parameter to be tweaked
																		// GL_LINEAR : value of the parameter(this is int : i)

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);	// when model is away from the viewer; pixel being textured maps to an area greater than one texture element
																					// GL_LINEAR_MIPMAP_LINEAR : Maintain GL_LINEAR at all mipmap levels; A mipmap is an ordered set of arrays representing the same image at progressively lower resolutions

																					// fill the data (instead of gluBuild2DMipmaps())
	glTexImage2D(
		GL_TEXTURE_2D,		// where to fill data; the target texture; following 8 para. go in this
		0,					// mipmap level(level of detail). Level 0 is the base image level
		GL_RGBA,			// internal image format
		w,					// width of the texture image
		h,					// height of the texture image
		0,					// border width. Must be 0
		GL_RGBA,				// external image data format
		GL_UNSIGNED_BYTE,	// the data type for data (last parameter)
		pixels			// actual data : Pointer to the location of the bit values for the bitmap
	);	// specify a two-dimensional texture image

	// create mipmaps for this texture for better image quality
	glGenerateMipmap(GL_TEXTURE_2D);	// texture data in this now

	// unbind texture explicitly
	glBindTexture(GL_TEXTURE_2D, 0);				

	CFRelease(imageData);

	return(texture);		   
}

-(void)reshape      // NSOpenGLView's method which we override;  Called by Cocoa when the view's visible rectangle or bounds change.
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);   // lock context so that it does not go to main thread but remains in this thread only
                                                                        // 2nd CGLContextObj is method

    NSRect rect=[self bounds];      // call bounds() on self (self's bounds)

    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if (height == 0)
	{
		height = 1;
	}

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    perspectiveProjectionMatrix=vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// ? other values of near & far not working
																											// parameters:
																											// fovy- The field of view angle, in degrees, in the y - direction.
																											// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																											// zNear- The distance from the viewer to the near clipping plane(always positive).
																											// zFar- The distance from the viewer to the far clipping plane(always positive).

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawRect:(NSRect)dirtyRect   // ~WM_PAINT
{
    // code
    [self drawView];    // for precaution, not compulsion (in case of thread switching i.e. if it goes in main thread during buffer swapping (to avoid flickering))
}

-(void)drawView     // ~Display()
{
	// function declarations
	void Update(void);

    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    // display code
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	glUseProgram(gShaderProgramObject);	// binding your OpenGL code with shader program object
										// Specifies the handle of the program object whose executables are to be used as part of current rendering state.

	// cube
	// 4 CPU steps
	// declaration of matrices
	vmath::mat4 modelMatrix;
	vmath::mat4 viewMatrix;
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;

	// initialize above matrices to identity (not initialised in above step for better visibility and understanding)
	modelMatrix = vmath::mat4::identity();
	viewMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();

	// do necessary transformation (T,S,R)
	translationMatrix = vmath::translate(0.0f, 0.0f, -6.0f);
	rotationMatrix = vmath::rotate(angle_cube, angle_cube, angle_cube);

	// do necessary matrix multiplication 
	modelMatrix = translationMatrix * rotationMatrix;

	// fill and send uniforms
	// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mUniform,		// uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelMatrix		// actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
	);

	glUniformMatrix4fv(vUniform,		// uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		viewMatrix					// actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
	);

	glUniformMatrix4fv(pUniform,		// uniform in which perspectiveProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		perspectiveProjectionMatrix		// actual matrix; this will bind to pUniform which is bound to u_p_matrix
	);

	// 3 texture lines before vao (ABU)
	// A: select active texture unit (there are usually total 80 texture units for a single geometry)
	glActiveTexture(GL_TEXTURE0);	// GL_TEXTURE0: of OGL; matches with our AMC_ATTRIBUTE_TEXCOORD0

	// B: bind a named texture to a texturing target
	glBindTexture(GL_TEXTURE_2D, texture_marble);

	// U: specify the value of a uniform variable for the current program object(sending uniform); glUniformMatrix4fv() is used to send matrices as uniforms
	glUniform1i(samplerUniform, 0);		// tell OGL that you are sending uniform as one integer and give that to FS
										// samplerUniform: Specifies the location of the uniform variable to be modified.
										// 0: Specifies the new values to be used for the specified uniform variable; 0 means unit 0

	glUniform3fv(laUniform, 1, light_ambient);	// to send vec4
	glUniform3fv(ldUniform, 1, light_diffuse);	// to send vec4
	glUniform3fv(lsUniform, 1, light_specular);	// to send vec4
	glUniform4fv(lightPositionUniform, 1, light_position);	// to send vec4; ~glLightfv(GL_LIGHT0, GL_POSITION, LightPosition) in FFP

	glUniform3fv(kaUniform, 1, material_ambient);	// gray material; to send vec3
	glUniform3fv(kdUniform, 1, material_diffuse);	// gray material; to send vec3
	glUniform3fv(ksUniform, 1, material_specular);	// gray material; to send vec3
	glUniform1f(materialShininessUniform, material_shininess);	// gray material; to send vec3

	// bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
	glBindVertexArray(vao_cube);		// arrays are in vbo and vbo is in vao. Hence, bind to vao

										// similarly, bind with textures if any (call glBindTexture() here)

										// draw the necessary scene!
	glDrawArrays(GL_TRIANGLE_FAN,		// PP does not have GL_QUADS! ;	what kind of primitives to render; same as glBegin(GL_TRIANGLES)
		0,								// array position of your array to start with (imp in inter-leaved)
		4								// how many vertices to draw
	);		// render primitives from array data;  If GL_VERTEX_ARRAY is not enabled, no geometric primitives are generated
			// Arrays since multiple primitives(P,C,N,T) can be drawn
	glDrawArrays(GL_TRIANGLE_FAN,
		4,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		8,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		12,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		16,
		4
	);
	glDrawArrays(GL_TRIANGLE_FAN,
		20,
		4
	);

	// unbind texture
	glBindTexture(GL_TEXTURE_2D, 0);

	// unbind vao
	glBindVertexArray(0);

	// unuse program
	glUseProgram(0);	// unbinding your OpenGL code with shader program object
						// If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);       // ~glSwapBuffers() ; for multi-threaded rendering
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

	Update();
}

-(BOOL)acceptsFirstResponder		// telling window that give your event handling to your view
{
	// code
	[[self window]makeFirstResponder:self];
	return(YES);		// YES means method is implemented
}

-(void)keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:    // esc key
            [self release];
            [NSApp terminate:self];
            break;

        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];	// toggleFullScreen: its method; repainting occurs automatically
            break;

        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
	// code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	// code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	// code
}

-(void) dealloc
{
	// code
    // Uninitialize() code here
   glDeleteTextures(1, &texture_marble);

	if (vbo_pctn_cube)
	{
		glDeleteBuffers(1, &vbo_pctn_cube);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_pctn_cube = 0;
	}

	if (vao_cube)
	{
		glDeleteVertexArrays(1, &vao_cube);		// delete vertex array objects
		vao_cube = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;	// typedef int
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);		// since unused in Display()
		
		// ask program that how many shaders are attached to it
		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
			&shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);		// dynamic array for shaders since we don't know how many present
		if (pShaders)	// mem allocated
		{
			// take attached shaders  into above array
			glGetAttachedShaders(gShaderProgramObject,		// the program object to be queried
				shaderCount,								// the size of the array for storing the returned object names
				&shaderCount,								// Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
				pShaders									// an array that is used to return the names of attached shader objects(empty now)
			);		// return the handles of the shader objects attached to a program object

			for (shaderNumber = 0;shaderNumber < shaderCount;shaderNumber++)
			{
				// detach each shader
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
																					// 2nd para: Specifies the shader object to be detached.

				// delete each detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		// delete the shader program
		glDeleteProgram(gShaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
		gShaderProgramObject = 0;
		glUseProgram(0);
	}
    
    CVDisplayLinkStop(displayLink);     // stop
    CVDisplayLinkRelease(displayLink);  // release

	[super dealloc];    // can call Uninitialize() here
}

@end

// C style functions
void Update(void)
{
	angle_cube = angle_cube - 1.0f;
	if (angle_cube <= -360.0f)
		angle_cube = 0.0f;
}

CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *pDisplayLinkContext)
{
    // *pNow: current time
    // *pDisplayLinkContext: object actually holding display link

    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];    // (GLView *): type-casting
    return(result);
} 

// calls: CVDisplayLinkStart -> MyDisplayLinkCallback -> getFrameForTime -> drawView
