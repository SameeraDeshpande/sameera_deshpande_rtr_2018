// headers
// #import prevents recursive inclusion of header files
#import <Foundation/Foundation.h>	// for NSAutoreleasePool
#import <Cocoa/Cocoa.h>	// Cocoa.framework for GUI applications (~windows.h)	// Cocoa is SDK of Mac
#import <QuartzCore/CVDisplayLink.h>	// Core Video DisplayLink
#import <OpenGL/gl3.h>  // OGl 3.0 and onwards is PP
#import <OpenGL/gl3ext.h>   // with extensions
#import "vmath.h"
#import "Sphere.h"

enum	// nameless since we are just concerned with its "named" indices
{
    AMC_ATTRIBUTE_POSITION=0,
	AMC_ATTRIBUTE_COLOR,		// 1
	AMC_ATTRIBUTE_NORMAL,		// 2
	AMC_ATTRIBUTE_TEXCOORD0,	// 3 (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// 'C' style global fuction declarations
CVReturn MyDisplayLinkCallback(
    CVDisplayLinkRef,       // which display link
    const CVTimeStamp *,    // current time
    const CVTimeStamp *,    // at what time to give output frame
    CVOptionFlags,          // reserved
    CVOptionFlags *,        // reserved
    void *                  // object with which this callback is registered
);

// global variable declarations
FILE *gpFile=NULL;
int year = 0, day = 0, month = 0, moon_day=0;

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> // AppDelegate inherited from NSObject and will implement functions of NSApplicationDelegate and NSWindowDelegate
																			// AppDelegate: delegate of NSApplicationDelegate, NSWindowDelegate
@end

@interface GLView : NSOpenGLView		// this is delegate to none
@end

// entry-point function
int main(int argc, const char* argv[])
{
	// code
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];	// NSAutoreleasePool: interface(class); alloc increases ref. count by 1

	NSApp=[NSApplication sharedApplication];	// NSApp: object; NSApplication: interface; sharedApplication: static method

	[NSApp setDelegate:[[AppDelegate alloc]init]];		// setDelegate: instance method; alloc = allocate memory ~'new';  init: constructor(succeeding alloc)


	[NSApp run];		// run loop

	[pPool release];    // ref. count=0

	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private				// default; OK if not written
	NSWindow *window;	// their class
	GLView *glView;		// our class
}	// does not mean class ends here (@end means that)

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification		// callback function (OS will call this); ~WM_CREATE; from NSApplicationDelegate
{
	// code
    // log file (5 steps)
    // get the object of your application package (.app)
    NSBundle *mainBundle=[NSBundle mainBundle];     // 1st mainBundle: object; 2nd mainBundle: static method, alloc and init happen internally

    // using this NSBundle object, get the path of your .app directory
    NSString *appDirName=[mainBundle bundlePath];   

    // get the parent directory of .app directory in NSString form (e.g. /Desktop/ProjName)
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];     // we want log file not in .app directory (since write protected) but besides it
                                                                                
    // create log file path in NSString form
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];  // 1st @: literal; %@ can give you object's UUID
                                                                                            // e.g. of %@ : /Desktop/ProjName;     e.g. of %@/ : /Desktop/ProjName/

    // convert NSString into C string (i.e. const char *)
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];    // p: pointer; sz: C string
                                                                                                            // asking ASCII encoding for C string

    // now use this C string as 1st para. of fopen()
    gpFile=fopen(pszLogFileNameWithPath,"w");
    if(gpFile==NULL)
    {
        printf("Cannot Create Log File.\n Exiting..\n");      // Objective C's printf called NSLog() and it internally calls printf()
        [self release];
        [NSApp terminate:self];     // will go to applicationWillTerminate
    }

    fprintf(gpFile, "Program Started Successfully\n");

	// window
	NSRect win_rect;	// struct (no '*' after it!)   // C style
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);       // C style

	// create simple window
	window=[[NSWindow alloc] initWithContentRect:win_rect	// create window of this rect (named parameters of init)
							 styleMask:NSWindowStyleMaskTitled |NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable| NSWindowStyleMaskResizable		// window styles
							 backing:NSBackingStoreBuffered		// how to store window in memory
							 defer:NO];		// do immediate repainting? no

	[window setTitle:@"macOS OpenGL Window"];		// @: literal string= NSString
	[window center];

    // create view (canvas) by giving it frame
	glView=[[GLView alloc]initWithFrame:win_rect];      // creating nameless object of GLView (object chaining)
                                                        // initWithFrame's implementation in GLView

    // give this view to window
	[window setContentView:glView];

	[window setDelegate:self];      // self = AppDelegate
	[window makeKeyAndOrderFront:self];     // get key focus on window and put it topmost in z-order
                                            // window: will receive messages; self: will give messages
}

-(void)applicationWillTerminate:(NSNotification *)notification      // callback function (OS will call this); ~WM_DESTROY; from NSApplicationDelegate
{
	// code
	fprintf(gpFile, "Program Terminated Successfully\n");

    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification	// ~WM_CLOSE; of NSWindowDelegate; applicationWillTerminate will be called after this
{
	// code
	[NSApp terminate:self];     // ~DestroyWindow()
}

-(void)dealloc		// destructor	// release calls this
{
	// code
	[glView release];		// will call dealloc of MyView
	[window release];	// NSWindow's default dealloc will be called
	[super dealloc];	// tell parent
}
@end

@implementation GLView
{
@private				// default; OK if not written
    CVDisplayLinkRef displayLink;   // ~CVDisplayLink displayLink;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;

    GLuint vao_sphere;		// vertex array object	(1 also ok since we unbind)
	GLuint vbo_sphere_position, vbo_sphere_element;		// vertex buffer object

	GLuint mvpUniform;		// model matrix
	vmath::mat4 perspectiveProjectionMatrix;		// mat4: in vmath.h; it is typedef of a 4-member float array

	GLsizei gNumVertices;
	GLsizei gNumElements;
}

-(id)initWithFrame:(NSRect)frame;   // generic return type
{
    // code
	self=[super initWithFrame:frame];   // get GLView's object from parent

	if(self)    // imp code inside this
	{
		[[self window]setContentView:self];     // get GLView's window whose view is GLView only; this step actually required for NextStep Interface Builder

        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // must specify the 4.1 core profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,     // PFA: Pixel Format Attribute  // LHS
            NSOpenGLProfileVersion4_1Core,  // Apple has locked OpenGL at 4.1 version   // RHS
            // specify the display id to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),     // map Core Graphics' display id to OpenGL id;
                                                                                            // in mac, variables starting with 'k' are kernel level #define's
            NSOpenGLPFAAccelerated,     // h/w accleration i.e. h/w rendering; give h/w supporting OpenGL
            NSOpenGLPFANoRecovery,      // if no h/w acceleration obtained, give error and exit
            NSOpenGLPFAColorSize,24,    // 32 also ok
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,     // A of RGBA
            NSOpenGLPFADoubleBuffer,
            0                           // last 0 is must; nil also ok
        };

        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];    // get pixel format from attributes
                                                                                                                // autorelease will give you explicit release call w/o expecting release call in main()
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available.\n Exiting..\n");      // Objective C's printf called NSLog() and it internally calls printf()
            [self release];
            [NSApp terminate:self];     // will go to applicationWillTerminate
        }

        // now context using pixelFormat
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext];      // automatically releases the older context if present and sets the newer one
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];    // pool signifies it(?) is thread

    [self drawView];    // our method

    [pool release];
    return(kCVReturnSuccess); 
}

-(void)prepareOpenGL    // NSOpenGLView's method which we implement; OS will call it; This method is called only once after the OpenGL context is made the current context.
{
    // code
    // OpenGL info
    fprintf(gpFile,"OpenGL Version: %s\n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];       // ~wglMakeCurrent, glxCurrent

    // decide double buffer's swap interval
    GLint swapInt=1;    // mac's default swapping interval is 1 i.e. 1 frame gap
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];       // forParameter: for whom to set; CP: Context Parameter
    
    // OpenGL code
    // vertex shader-
	// define vertex shader object
	gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);	// creates a shader that is given as parameter	// 1st line of programmable pipeline !

	// write vertex shader source code
	const GLchar* vertexShaderSourceCode = 
		"#version 410 core" \
		"\n" \
		"in vec4 vPosition;" \
		"in vec4 vColor;" \
		"uniform mat4 u_mvp_matrix;" \
		"out vec4 out_color;" \
		"void main(void)" \
		"{" \
		"gl_Position = u_mvp_matrix * vPosition;" \
		"out_color = vColor;" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt 
				// in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
				// uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
				// gl_Position: in-built variable of shader	
				// out_color = vColor: assigning in attribute to out attribute
				// here, color is going to be given to frag color directly			

	// give above source code to the vertex shader object
	glShaderSource(
		gVertexShaderObject,	// shader to which the source code is to be given
		1,	// the number of strings in the array
		(const GLchar **)&vertexShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);	// replace the source code in a shader object

	// compile the vertex shader
	glCompileShader(gVertexShaderObject);
	// error checking
	GLint iShaderCompileStatus = 0;
	GLint iInfoLogLength = 0;
	GLchar *szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gVertexShaderObject,	// the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gVertexShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gVertexShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
											// When a shader object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nVertex Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

	// vertex converted to fragment now

    // fragment shader-
	// define fragment shader object
	gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);	// creates a shader that is given as parameter

	// write fragment shader source code
	const GLchar* fragmentShaderSourceCode = 
		"#version 410 core" \
		"\n" \
		"in vec4 out_color;" \
		"out vec4 frag_color;" \
		"void main(void)" \
		"{" \
		"frag_color = out_color;" \
		"}";	// written in Graphics Library Shading/Shader Language (GLSL)
				// 450: OpenGL version support*100 (4.5*100); core: core profile
				// '\n' important and compulsory since shader file would have an enter after the version stmt
				// in vec4 out_color: VS's out is FS's in. Hence, same name. (gl_Position is in-built hence need not be passed)
				// out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
				// vec4: function/macro/constructor

	// give above source code to the fragment shader object
	glShaderSource(
		gFragmentShaderObject,	// shader to which the source code is to be given
		1,		// the number of strings in the array
		(const GLchar **)&fragmentShaderSourceCode,
		NULL	// length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
				// If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string 
	);			// replace the source code in a shader object

				// compile the fragment shader
	glCompileShader(gFragmentShaderObject);
	// error checking
	iShaderCompileStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetShaderiv(	// iv= integer vector
		gFragmentShaderObject,	//  the shader object to be queried.
		GL_COMPILE_STATUS,		// what(object parameter) is to be queried	
		&iShaderCompileStatus	// empty	//  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
	);	// return a parameter from a shader object
	if (iShaderCompileStatus == GL_FALSE)	// error present
	{
		// check if the compiler has any info. about the error
		glGetShaderiv(gFragmentShaderObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetShaderInfoLog(
					gFragmentShaderObject,	// the shader object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);						// The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation. 
										// When a shader object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nFragment Shader Compilation Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

    // create shader program object 
	gShaderProgramObject = glCreateProgram();	// same program for all shaders

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gVertexShaderObject);	// 1st para: the program object to which a shader object will be attached
																// 2nd para: the shader object that is to be attached.

	// attach vertex shader to the shader program
	glAttachShader(gShaderProgramObject, gFragmentShaderObject); // 1st para: the program object to which a shader object will be attached
																 // 2nd para: the shader object that is to be attached.

	// pre-linking binding of gShaderProgramObject to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_POSITION,					// the index of the generic vertex attribute to be bound.
		"vPosition"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);	// bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
		// give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition

	glBindAttribLocation(gShaderProgramObject,	// the handle of the program object in which the association is to be made.
		AMC_ATTRIBUTE_COLOR,					// the index of the generic vertex attribute to be bound.
		"vColor"								// a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
	);

	// link the shader program to your program
	glLinkProgram(gShaderProgramObject);	// If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
	// error checking (eg. of link error- version incompatibility of shaders)
	GLint iProgramLinkStatus = 0;
	iInfoLogLength = 0;
	szInfoLog = NULL;

	glGetProgramiv(				// iv= integer vector
		gShaderProgramObject,	//  the program to be queried.
		GL_LINK_STATUS,			// what(object parameter) is to be queried	
		&iProgramLinkStatus		// empty	//  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
	);							// return a parameter from a program object
	if (iProgramLinkStatus == GL_FALSE)	// error present
	{
		// check if the linker has any info. about the error
		glGetProgramiv(gShaderProgramObject,
			GL_INFO_LOG_LENGTH,
			&iInfoLogLength);	// returns the number of characters in the information log for shader
		if (iInfoLogLength > 0)
		{
			// *before writing into any pointer, allocate it memory (else exception)
			szInfoLog = (GLchar*)malloc(iInfoLogLength);	// allocate it memory equal to iInfoLogLength
			if (szInfoLog != NULL)	// memory allocated
			{
				GLsizei written;	// temporary var. (GLsizei: typedef of int)
				glGetProgramInfoLog(
					gShaderProgramObject,	// the program object whose information log is to be queried.
					iInfoLogLength,			// the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
					&written,				// returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
					szInfoLog				// an array of characters that is used to return the information log
				);							// The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
											// When a program object is created, its information log will be a string of length 0.
				fprintf(gpFile, "\nShader Program Link Log : %s", szInfoLog);
				free(szInfoLog);	// not needed now
				[self release];
                [NSApp terminate:self];     // will go to applicationWillTerminate
			}
		}
	}

    // post-linking retrieving uniform locations (uniforms are global to shaders)
	mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");	// preparation of data transfer from CPU to GPU (binding)
																				// u_mvp_matrix: of GPU; mvpUniform: of CPU
																				// telling it to take location of uniform u_mvp_matrix and give in mvpUniform

	// fill sphere data in arrays
    float sphere_vertices[1146];
    float sphere_normals[1146];
    float sphere_textures[764];
    short sphere_elements[2280];

    getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
    gNumVertices = getNumberOfSphereVertices();
    gNumElements = getNumberOfSphereElements();

	// 9 lines
	// sphere
	// create vao(id) (vao is shape-wise)
	// vao
	glGenVertexArrays(1, &vao_sphere);
	glBindVertexArray(vao_sphere);

	// position vbo
	glGenBuffers(1, &vbo_sphere_position);
	glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
	glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
	glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// element vbo
	glGenBuffers(1, &vbo_sphere_element);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// unbind vao
	glBindVertexArray(0);

    glClearDepth(1.0f);		// bringing depth buffer into existance
							// filling the depth buffer with max value

	glEnable(GL_DEPTH_TEST);	// to compare depth values of objects

	glDepthFunc(GL_LEQUAL);	// specifies the value used for depth-buffer comparisons
							// Passes if the incoming z value is less than or equal to the stored z value. 
							// GL_LEQUAL : GLenum

    // set background color
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);	// bringing color buffer into existance
											// specifies clear values[0,1] used by glClear() for the color buffers

	perspectiveProjectionMatrix = vmath::mat4::identity();	// making orthographicProjectionMatrix an identity matrix(diagonals 1)
														// mat4: of vmath

    // game loop (6 steps)  (C functions concerned with CGL and CG)
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);  // create display link; creating display link= creating seperate thread

    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);

    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];    // openGLContext of type NSOpenGLContext; 2nd CGLContextObj is method

    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];  // self pixelFormat: obtain pixelFormat of self

    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

    CVDisplayLinkStart(displayLink);    // start display link; game loop starts here!
    
    [super prepareOpenGL];
}

-(void)reshape      // NSOpenGLView's method which we override;  Called by Cocoa when the view's visible rectangle or bounds change.
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);   // lock context so that it does not go to main thread but remains in this thread only
                                                                        // 2nd CGLContextObj is method

    NSRect rect=[self bounds];      // call bounds() on self (self's bounds)

    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    if (height == 0)
	{
		height = 1;
	}

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    perspectiveProjectionMatrix=vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);	// ? other values of near & far not working
																											// parameters:
																											// fovy- The field of view angle, in degrees, in the y - direction.
																											// aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
																											// zNear- The distance from the viewer to the near clipping plane(always positive).
																											// zFar- The distance from the viewer to the far clipping plane(always positive).

    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawRect:(NSRect)dirtyRect   // ~WM_PAINT
{
    // code
    [self drawView];    // for precaution, not compulsion (in case of thread switching i.e. if it goes in main thread during buffer swapping (to avoid flickering))
}

-(void)drawView     // ~Display()
{
	// variable declarations
	vmath::mat4 mv_stack[3];
	int ptr = -1;

    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    // display code
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
	glUseProgram(gShaderProgramObject);	// binding your OpenGL code with shader program object
										// Specifies the handle of the program object whose executables are to be used as part of current rendering state.

	// declaration of matrices
	vmath::mat4 translationMatrix;
	vmath::mat4 rotationMatrix;
	vmath::mat4 rotationMatrix1;
	vmath::mat4 scaleMatrix;
	vmath::mat4 modelViewMatrix;
	vmath::mat4 modelViewProjectionMatrix;

	// initialize above matrices to identity (not initialised in above step for better visibility and understanding)
	modelViewMatrix = vmath::mat4::identity();
	modelViewProjectionMatrix = vmath::mat4::identity();
	translationMatrix = vmath::mat4::identity();
	rotationMatrix = vmath::mat4::identity();
	rotationMatrix1 = vmath::mat4::identity();
	scaleMatrix = vmath::mat4::identity();

	// sun
	// do necessary transformation (T,S,R)
	translationMatrix = vmath::translate(0.0f, 0.0f, -3.0f);
	modelViewMatrix = translationMatrix;

	// push on stack
	ptr++;
	mv_stack[ptr] = modelViewMatrix;

	// do necessary matrix multiplication 
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

	// fill and send uniforms
	// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix					// actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
	);

	// *** bind vao ***
	glBindVertexArray(vao_sphere);

	// *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);

	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 1.0f, 1.0f, 0.0f);	// specify the value of a generic vertex attribute(for single color to object)

	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);	// When glDrawElements is called, it uses 'count' sequential indices from 'indices' to lookup elements in enabled arrays to construct a sequence of geometric primitives. 
		// 'mode' specifies what kind of primitives are constructed, and how the array elements construct these primitives. 
		// If GL_VERTEX_ARRAY is not enabled, no geometric primitives are constructed.

	// earth
	// pop from stack
	modelViewMatrix = mv_stack[ptr];
	ptr--;

	// do necessary transformation (T,S,R)
	rotationMatrix = vmath::rotate((GLfloat)year, 0.0f, 1.0f, 0.0f);		// for revolution of Earth
	translationMatrix = vmath::translate(1.0f, 0.0f, 0.0f);
	modelViewMatrix = modelViewMatrix * rotationMatrix * translationMatrix;

	// push on stack
	ptr++;
	mv_stack[ptr] = modelViewMatrix;

	scaleMatrix = vmath::scale(0.3f, 0.3f, 0.3f);
	rotationMatrix = vmath::rotate((GLfloat)day, 0.0f, 1.0f, 0.0f);		// for rotation of Earth

	// do necessary matrix multiplication 
	modelViewMatrix = modelViewMatrix * scaleMatrix * rotationMatrix;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

	// fill and send uniforms
	// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix						// actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
	);

	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f);	// specify the value of a generic vertex attribute(for single color to object)

	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);	

	// moon
	// pop from stack
	modelViewMatrix = mv_stack[ptr];
	ptr--;

	rotationMatrix = vmath::rotate((GLfloat)month, 0.0f, 1.0f, 0.0f);		// for revolution of moon
	translationMatrix = vmath::translate(0.4f, 0.0f, 0.0f);
	scaleMatrix = vmath::scale(0.13f, 0.13f, 0.13f);
	rotationMatrix1 = vmath::rotate((GLfloat)moon_day, 0.0f, 0.0f, 1.0f);		// for rotation of moon

	// do necessary matrix multiplication 
	modelViewMatrix = modelViewMatrix * rotationMatrix * translationMatrix * scaleMatrix * rotationMatrix1;
	modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;		// multiplication of matrices: operator overloading

	// fill and send uniforms
	// send necessary matrices to shader in respective uniforms (on GPU)
	glUniformMatrix4fv(mvpUniform,		// uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
		1,								// how many matrices to send?
		GL_FALSE,						// do transpose? ; no since OGL and GLSL are column-major
		modelViewProjectionMatrix						// actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
	);

	glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.90f, 0.90f, 0.90f);	// specify the value of a generic vertex attribute(for single color to object)

	glDrawElements(
		GL_TRIANGLES, // what kind of primitives to render (mode)
		gNumElements, // the number of elements to be rendered (count)
		GL_UNSIGNED_SHORT,	//  type of the values in last parameter
		0	// Specifies a pointer to the location where the indices are stored (indices)
	);	

	// unbind vbo
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	// *** unbind vao ***
	glBindVertexArray(0);

	// unbind vao
	glBindVertexArray(0);

	// unuse program
	glUseProgram(0);	// unbinding your OpenGL code with shader program object
						// If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.
    
    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);       // ~glSwapBuffers() ; for multi-threaded rendering
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder		// telling window that give your event handling to your view
{
	// code
	[[self window]makeFirstResponder:self];
	return(YES);		// YES means method is implemented
}

-(void)keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:    // esc key
            [self release];
            [NSApp terminate:self];
            break;

        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];	// toggleFullScreen: its method; repainting occurs automatically
            break;

		case 'Y':
			year = (year + 3) % 360;
			month = (month + 5) % 360;
			day = (day + 6) % 360;
			break;

		case 'y':
			year = (year - 3) % 360;
			month = (month - 5) % 360;
			day = (day - 6) % 360;
			break;

		case 'D':
			day = (day + 6) % 360;
			break;

		case 'd':
			day = (day - 6) % 360;
			break;

		case 'M':
			moon_day = (moon_day + 6) % 360;
			month = (month + 5) % 360;
			break;

		case 'm':
			moon_day = (moon_day - 6) % 360;
			month = (month - 5) % 360;
			break;

        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
	// code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	// code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	// code
}

-(void) dealloc
{
	// code
    // Uninitialize() code here
    // opposite sequence of vao and vbo also ok
	if (vbo_sphere_element)
	{
		glDeleteBuffers(1, &vbo_sphere_element);	// delete named buffer objects
												// 1: number of buffer objects to be deleted
												// &vbo: array of buffer objects to be deleted
		vbo_sphere_element = 0;
	}

	if (vbo_sphere_position)
	{
		glDeleteBuffers(1, &vbo_sphere_position);	// delete named buffer objects
													// 1: number of buffer objects to be deleted
													// &vbo: array of buffer objects to be deleted
		vbo_sphere_position = 0;
	}

	if (vao_sphere)
	{
		glDeleteVertexArrays(1, &vao_sphere);		// delete vertex array objects
		vao_sphere = 0;
	}

	if (gShaderProgramObject)
	{
		GLsizei shaderCount;	// typedef int
		GLsizei shaderNumber;

		glUseProgram(gShaderProgramObject);		// since unused in Display()
		
		// ask program that how many shaders are attached to it
		glGetProgramiv(gShaderProgramObject,
			GL_ATTACHED_SHADERS,		// returns the number of shader objects attached to shader program
			&shaderCount);

		GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);		// dynamic array for shaders since we don't know how many present
		if (pShaders)	// mem allocated
		{
			// take attached shaders  into above array
			glGetAttachedShaders(gShaderProgramObject,		// the program object to be queried
				shaderCount,								// the size of the array for storing the returned object names
				&shaderCount,								// Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
				pShaders									// an array that is used to return the names of attached shader objects(empty now)
			);		// return the handles of the shader objects attached to a program object

			for (shaderNumber = 0;shaderNumber < shaderCount;shaderNumber++)
			{
				// detach each shader
				glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);		// 1st para: Specifies the program object from which to detach the shader object.
																					// 2nd para: Specifies the shader object to be detached.

				// delete each detached shader
				glDeleteShader(pShaders[shaderNumber]);
				pShaders[shaderNumber] = 0;
			}
			free(pShaders);
		}
		// delete the shader program
		glDeleteProgram(gShaderProgramObject);	// frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
		gShaderProgramObject = 0;
		glUseProgram(0);
	}
    
    CVDisplayLinkStop(displayLink);     // stop
    CVDisplayLinkRelease(displayLink);  // release

	[super dealloc];    // can call Uninitialize() here
}

@end

// C style function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *pDisplayLinkContext)
{
    // *pNow: current time
    // *pDisplayLinkContext: object actually holding display link

    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];    // (GLView *): type-casting
    return(result);
} 

// calls: CVDisplayLinkStart -> MyDisplayLinkCallback -> getFrameForTime -> drawView
