// headers
// #import prevents recursive inclusion of header files
#import <Foundation/Foundation.h>	// for NSAutoreleasePool
#import <Cocoa/Cocoa.h>	// Cocoa.framework for GUI applications (~windows.h)	// Cocoa is SDK of Mac
#import <QuartzCore/CVDisplayLink.h>	// Core Video DisplayLink
#import <OpenGL/gl3.h>  // OGl 3.0 and onwards is PP
#import <OpenGL/gl3ext.h>   // with extensions

// 'C' style global fuction declarations
CVReturn MyDisplayLinkCallback(
    CVDisplayLinkRef,       // which display link
    const CVTimeStamp *,    // current time
    const CVTimeStamp *,    // at what time to give output frame
    CVOptionFlags,          // reserved
    CVOptionFlags *,        // reserved
    void *                  // object with which this callback is registered
);

// global variable declarations
FILE *gpFile=NULL;

// interface declarations
@interface AppDelegate : NSObject <NSApplicationDelegate, NSWindowDelegate> // AppDelegate inherited from NSObject and will implement functions of NSApplicationDelegate and NSWindowDelegate
																			// AppDelegate: delegate of NSApplicationDelegate, NSWindowDelegate
@end

@interface GLView : NSOpenGLView		// this is delegate to none
@end

// entry-point function
int main(int argc, const char* argv[])
{
	// code
	NSAutoreleasePool *pPool=[[NSAutoreleasePool alloc]init];	// NSAutoreleasePool: interface(class); alloc increases ref. count by 1

	NSApp=[NSApplication sharedApplication];	// NSApp: object; NSApplication: interface; sharedApplication: static method

	[NSApp setDelegate:[[AppDelegate alloc]init]];		// setDelegate: instance method; alloc = allocate memory ~'new';  init: constructor(succeeding alloc)


	[NSApp run];		// run loop

	[pPool release];    // ref. count=0

	return(0);
}

// interface implementations
@implementation AppDelegate
{
@private				// default; OK if not written
	NSWindow *window;	// their class
	GLView *glView;		// our class
}	// does not mean class ends here (@end means that)

-(void)applicationDidFinishLaunching:(NSNotification *)aNotification		// callback function (OS will call this); ~WM_CREATE; from NSApplicationDelegate
{
	// code
    // log file (5 steps)
    // get the object of your application package (.app)
    NSBundle *mainBundle=[NSBundle mainBundle];     // 1st mainBundle: object; 2nd mainBundle: static method, alloc and init happen internally

    // using this NSBundle object, get the path of your .app directory
    NSString *appDirName=[mainBundle bundlePath];   

    // get the parent directory of .app directory in NSString form (e.g. /Desktop/ProjName)
    NSString *parentDirPath=[appDirName stringByDeletingLastPathComponent];     // we want log file not in .app directory (since write protected) but besides it
                                                                                
    // create log file path in NSString form
    NSString *logFileNameWithPath=[NSString stringWithFormat:@"%@/Log.txt",parentDirPath];  // 1st @: literal; %@ can give you object's UUID
                                                                                            // e.g. of %@ : /Desktop/ProjName;     e.g. of %@/ : /Desktop/ProjName/

    // convert NSString into C string (i.e. const char *)
    const char *pszLogFileNameWithPath=[logFileNameWithPath cStringUsingEncoding:NSASCIIStringEncoding];    // p: pointer; sz: C string
                                                                                                            // asking ASCII encoding for C string

    // now use this C string as 1st para. of fopen()
    gpFile=fopen(pszLogFileNameWithPath,"w");
    if(gpFile==NULL)
    {
        printf("Cannot Create Log File.\n Exiting..\n");      // Objective C's printf called NSLog() and it internally calls printf()
        [self release];
        [NSApp terminate:self];     // will go to applicationWillTerminate
    }

    fprintf(gpFile, "Program Started Successfully\n");

	// window
	NSRect win_rect;	// struct (no '*' after it!)   // C style
	win_rect=NSMakeRect(0.0,0.0,800.0,600.0);       // C style

	// create simple window
	window=[[NSWindow alloc] initWithContentRect:win_rect	// create window of this rect (named parameters of init)
							 styleMask:NSWindowStyleMaskTitled |NSWindowStyleMaskClosable | NSWindowStyleMaskMiniaturizable| NSWindowStyleMaskResizable		// window styles
							 backing:NSBackingStoreBuffered		// how to store window in memory
							 defer:NO];		// do immediate repainting? no

	[window setTitle:@"macOS OpenGL Window"];		// @: literal string= NSString
	[window center];

    // create view (canvas) by giving it frame
	glView=[[GLView alloc]initWithFrame:win_rect];      // creating nameless object of GLView (object chaining)
                                                        // initWithFrame's implementation in GLView

    // give this view to window
	[window setContentView:glView];

	[window setDelegate:self];      // self = AppDelegate
	[window makeKeyAndOrderFront:self];     // get key focus on window and put it topmost in z-order
                                            // window: will receive messages; self: will give messages
}

-(void)applicationWillTerminate:(NSNotification *)notification      // callback function (OS will call this); ~WM_DESTROY; from NSApplicationDelegate
{
	// code
	fprintf(gpFile, "Program Terminated Successfully\n");

    if(gpFile)
    {
        fclose(gpFile);
        gpFile=NULL;
    }
}

-(void)windowWillClose:(NSNotification *)notification	// ~WM_CLOSE; of NSWindowDelegate; applicationWillTerminate will be called after this
{
	// code
	[NSApp terminate:self];     // ~DestroyWindow()
}

-(void)dealloc		// destructor	// release calls this
{
	// code
	[glView release];		// will call dealloc of MyView
	[window release];	// NSWindow's default dealloc will be called
	[super dealloc];	// tell parent
}
@end

@implementation GLView
{
@private				// default; OK if not written
    CVDisplayLinkRef displayLink;   // ~CVDisplayLink displayLink;
}

-(id)initWithFrame:(NSRect)frame;   // generic return type
{
    // code
	self=[super initWithFrame:frame];   // get GLView's object from parent

	if(self)    // imp code inside this
	{
		[[self window]setContentView:self];     // get GLView's window whose view is GLView only; this step actually required for NextStep Interface Builder

        NSOpenGLPixelFormatAttribute attrs[]=
        {
            // must specify the 4.1 core profile to use OpenGL 4.1
            NSOpenGLPFAOpenGLProfile,     // PFA: Pixel Format Attribute  // LHS
            NSOpenGLProfileVersion4_1Core,  // Apple has locked OpenGL at 4.1 version   // RHS
            // specify the display id to associate the GL context with (main display for now)
            NSOpenGLPFAScreenMask,CGDisplayIDToOpenGLDisplayMask(kCGDirectMainDisplay),     // map Core Graphics' display id to OpenGL id;
                                                                                            // in mac, variables starting with 'k' are kernel level #define's
            NSOpenGLPFAAccelerated,     // h/w accleration i.e. h/w rendering; give h/w supporting OpenGL
            NSOpenGLPFANoRecovery,      // if no h/w acceleration obtained, give error and exit
            NSOpenGLPFAColorSize,24,    // 32 also ok
            NSOpenGLPFADepthSize,24,
            NSOpenGLPFAAlphaSize,8,     // A of RGBA
            NSOpenGLPFADoubleBuffer,
            0                           // last 0 is must; nil also ok
        };

        NSOpenGLPixelFormat *pixelFormat=[[[NSOpenGLPixelFormat alloc]initWithAttributes:attrs]autorelease];    // get pixel format from attributes
                                                                                                                // autorelease will give you explicit release call w/o expecting release call in main()
        if(pixelFormat==nil)
        {
            fprintf(gpFile, "No Valid OpenGL Pixel Format Is Available.\n Exiting..\n");      // Objective C's printf called NSLog() and it internally calls printf()
            [self release];
            [NSApp terminate:self];     // will go to applicationWillTerminate
        }

        // now context using pixelFormat
        NSOpenGLContext *glContext=[[[NSOpenGLContext alloc]initWithFormat:pixelFormat shareContext:nil]autorelease];

        [self setPixelFormat:pixelFormat];

        [self setOpenGLContext:glContext];      // automatically releases the older context if present and sets the newer one
    }
    return(self);
}

-(CVReturn)getFrameForTime:(const CVTimeStamp *)pOutputTime
{
    // code
    NSAutoreleasePool *pool=[[NSAutoreleasePool alloc]init];    // pool signifies it(?) is thread

    [self drawView];    // our method

    [pool release];
    return(kCVReturnSuccess); 
}

-(void)prepareOpenGL    // NSOpenGLView's method which we implement; OS will call it; This method is called only once after the OpenGL context is made the current context.
{
    // code
    // OpenGL info
    fprintf(gpFile,"OpenGL Version: %s\n",glGetString(GL_VERSION));
    fprintf(gpFile,"GLSL Version: %s\n",glGetString(GL_SHADING_LANGUAGE_VERSION));

    [[self openGLContext]makeCurrentContext];       // ~wglMakeCurrent, glxCurrent

    // decide double buffer's swap interval
    GLint swapInt=1;    // mac's default swapping interval is 1 i.e. 1 frame gap
    [[self openGLContext]setValues:&swapInt forParameter:NSOpenGLCPSwapInterval];       // forParameter: for whom to set; CP: Context Parameter

    // all OpenGL code here
    // set background color
    glClearColor(0.0f,0.0f,1.0f,0.0f);  // blue

    // game loop (6 steps)  (C functions concerned with CGL and CG)
    CVDisplayLinkCreateWithActiveCGDisplays(&displayLink);  // create display link; creating display link= creating seperate thread

    CVDisplayLinkSetOutputCallback(displayLink,&MyDisplayLinkCallback,self);

    CGLContextObj cglContext=(CGLContextObj)[[self openGLContext]CGLContextObj];    // openGLContext of type NSOpenGLContext; 2nd CGLContextObj is method

    CGLPixelFormatObj cglPixelFormat=(CGLPixelFormatObj)[[self pixelFormat]CGLPixelFormatObj];  // self pixelFormat: obtain pixelFormat of self

    CVDisplayLinkSetCurrentCGDisplayFromOpenGLContext(displayLink,cglContext,cglPixelFormat);

    CVDisplayLinkStart(displayLink);    // start display link; game loop starts here!
    
    [super prepareOpenGL];
}

-(void)reshape      // NSOpenGLView's method which we override;  Called by Cocoa when the view's visible rectangle or bounds change.
{
    // code
    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);   // lock context so that it does not go to main thread but remains in this thread only
                                                                        // 2nd CGLContextObj is method

    NSRect rect=[self bounds];      // call bounds() on self (self's bounds)

    GLfloat width=rect.size.width;
    GLfloat height=rect.size.height;

    glViewport(0,0,(GLsizei)width,(GLsizei)height);

    // projection code here
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
    
    [super reshape];
}

-(void)drawRect:(NSRect)dirtyRect   // ~WM_PAINT
{
    // code
    [self drawView];    // for precaution, not compulsion (in case of thread switching i.e. if it goes in main thread during buffer swapping (to avoid flickering))
}

-(void)drawView     // ~Display()
{
    // code
    [[self openGLContext]makeCurrentContext];

    CGLLockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);

    // display code here
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

    CGLFlushDrawable((CGLContextObj)[[self openGLContext]CGLContextObj]);       // ~glSwapBuffers() ; for multi-threaded rendering
    CGLUnlockContext((CGLContextObj)[[self openGLContext]CGLContextObj]);
}

-(BOOL)acceptsFirstResponder
{
    // code
    [[self window]makeFirstResponder:self];
    return(YES);
}

-(void)keyDown:(NSEvent *)theEvent
{
    // code
    int key=(int)[[theEvent characters]characterAtIndex:0];
    switch(key)
    {
        case 27:    // esc key
            [self release];
            [NSApp terminate:self];
            break;

        case 'F':
        case 'f':
            [[self window]toggleFullScreen:self];	// toggleFullScreen: its method; repainting occurs automatically
            break;

        default:
            break;
    }
}

-(void)mouseDown:(NSEvent *)theEvent
{
	// code
}

-(void)mouseDragged:(NSEvent *)theEvent
{
	// code
}

-(void)rightMouseDown:(NSEvent *)theEvent
{
	// code
}

-(void) dealloc
{
	// code
    CVDisplayLinkStop(displayLink);     // stop
    CVDisplayLinkRelease(displayLink);  // release

	[super dealloc];    // can call Uninitialize() here
}

@end

// C style function
CVReturn MyDisplayLinkCallback(CVDisplayLinkRef displayLink, const CVTimeStamp *pNow, const CVTimeStamp *pOutputTime, CVOptionFlags flagsIn, CVOptionFlags *flagsOut, void *pDisplayLinkContext)
{
    // *pNow: current time
    // *pDisplayLinkContext: object actually holding display link

    CVReturn result=[(GLView *)pDisplayLinkContext getFrameForTime:pOutputTime];    // (GLView *): type-casting
    return(result);
} 

// calls: CVDisplayLinkStart -> MyDisplayLinkCallback -> getFrameForTime -> drawView
