// headers
// #import prevents recursive inclusion of header files
#import <Foundation/Foundation.h>	// for NSAutoreleasePool
#import <Cocoa/Cocoa.h>	// Cocoa.framework for GUI applications (~windows.h)
#import <QuartzCore/CVDisplayLink.h>	// Core Video DisplayLink
#import <OpenGL/gl3.h>
#import <OpenGL/gl3ext.h>

// 'C' style global fuction declarations
CVReturn 
