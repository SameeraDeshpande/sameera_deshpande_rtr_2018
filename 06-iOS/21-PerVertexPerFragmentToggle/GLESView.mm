#import <OpenGLES/ES3/gl.h>     // adding OpenGLES.framework; locked on version 3.0
#import <OpenGLES/ES3/glext.h>  // ~glew.h (extension wrangler)
#import "vmath.h"
#import "GLESView.h"
#import "Sphere.h"

enum    // nameless since we are just concerned with its "named" indices
{
    AMC_ATTRIBUTE_POSITION=0,
    AMC_ATTRIBUTE_COLOR,        // 1
    AMC_ATTRIBUTE_NORMAL,        // 2
    AMC_ATTRIBUTE_TEXCOORD0,    // 3 (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// global variable declarations
bool gbLighting = false;
bool gbVPressed = true;
bool gbFPressed = false;

GLfloat light_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat light_diffuse[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat light_position[4] = { 100.0f,100.0f,100.0f,1.0f };

GLfloat material_ambient[4] = { 0.0f,0.0f,0.0f,0.0f };
GLfloat material_diffuse[4] = { 0.5f,0.2f,0.7f,1.0f };    // albedo diffuse
GLfloat material_specular[4] = { 1.0f,1.0f,1.0f,1.0f };
GLfloat material_shininess = 128.0f;

@implementation GLESView
{
    EAGLContext *eaglContext;   // EAGL= Embedded Apple Graphics Layer
    
    GLuint defaultFrameBuffer;  // storage for all buffers
    GLuint colorRenderBuffer;   // color buffer, depth buffer & stencil buffer are togetherly called render buffers
    GLuint depthRenderBuffer;
    
    id displayLink;     // id= generic
    NSInteger animationFrameInterval;   // mac's integer
    BOOL isAnimating;
    
    GLuint gShaderProgramObject_PV;
    GLuint gShaderProgramObject_PF;
    
    GLuint vao_sphere;
    GLuint vbo_sphere_position;
    GLuint vbo_sphere_normal;
    GLuint vbo_sphere_element;
    int numVertices;
    int numElements;
    
    GLuint mUniform_PV;
    GLuint vUniform_PV;
    GLuint pUniform_PV;
    GLuint laUniform_PV;
    GLuint ldUniform_PV;
    GLuint lsUniform_PV;
    GLuint lightPositionUniform_PV;
    GLuint kaUniform_PV;
    GLuint kdUniform_PV;
    GLuint ksUniform_PV;
    GLuint materialShininessUniform_PV;
    GLuint isDoubleTapMadeUniform_PV;
    
    GLuint mUniform_PF;
    GLuint vUniform_PF;
    GLuint pUniform_PF;
    GLuint laUniform_PF;
    GLuint ldUniform_PF;
    GLuint lsUniform_PF;
    GLuint lightPositionUniform_PF;
    GLuint kaUniform_PF;
    GLuint kdUniform_PF;
    GLuint ksUniform_PF;
    GLuint materialShininessUniform_PF;
    GLuint isDoubleTapMadeUniform_PF;
    
    vmath::mat4 perspectiveProjectionMatrix;        // mat4: in vmath.h; it is typedef of a 4-member float array
    vmath::mat4 modelMatrix;        // mat4: in vmath.h; it is typedef of a 4-member float array
    vmath::mat4 viewMatrix;        // mat4: in vmath.h; it is typedef of a 4-member float array
}

-(id)initWithFrame:(CGRect)frame;
{
    // variable declarations
    GLuint gVertexShaderObject_PF;
    GLuint gFragmentShaderObject_PF;
    GLuint gVertexShaderObject_PV;
    GLuint gFragmentShaderObject_PV;
    
    // code
    self=[super initWithFrame:frame];   // self= this
    if(self)
    {
        // get layer from super (i.e. from UIView)
        CAEAGLLayer *eaglLayer=(CAEAGLLayer*)super.layer;   // CA: Core Animation; layer: method; can also write [super layer]
                                                            // layer will be CALayer and not OGL layer so we typecast it to OGL layer
        
        // set its properties
        eaglLayer.opaque=YES;   // setting its alpha to 1.0 or [eaglLayer setOpaque]
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:    // dictionary when multiple properties (object-key array)
                                      [NSNumber numberWithBool:FALSE],              // creating no. from BOOL
                                      kEAGLDrawablePropertyRetainedBacking,         // drawable in iOS= Android's surface (constant i.e. #define)
                                      kEAGLColorFormatRGBA8,                        // 8 bits each for R,G,B,A; it is a live object(struct) and not a constant
                                      kEAGLDrawablePropertyColorFormat,             // this is key of the preceding object
                                      nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];    // constant
        if(eaglContext==nil)
        {
            printf("Couldn't Achieve EAGLContext");
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];    // ~wglMakeCurrent() in Windows or glxMakeCurrent() in Linux or makeCurrentContext() in macOS
        
        // create frame buffer (setup similar to vao, vbo creation and use in Display())
        glGenFramebuffers(1,&defaultFrameBuffer);   // 1: one FB
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);  // GL_FRAMEBUFFER: target(GPU addr); defaultFrameBuffer: bind this

        // color buffer
        glGenRenderbuffers(1,&colorRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];   // GL_RENDERBUFFER: colorRenderBuffer
                                                                                    // eaglLayer(our drawable) has data(eg. h & w of window) to fill in CB
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,       // attach to this
                                  GL_COLOR_ATTACHMENT0, // attach here
                                  GL_RENDERBUFFER,      // attach this
                                  colorRenderBuffer     // colorRenderBuffer: this is bound to third para.
                                  );
        
        // depth buffer
        GLint backingWidth;    // we want DB for these w & h
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,       // take from this (GL_RENDERBUFFER= CB)
                                     GL_RENDERBUFFER_WIDTH, // take this
                                     &backingWidth          // take into this
                                     );
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        
        // establish data storage, format and dimensions of a renderbuffer object's image not from context but from OGL
        glRenderbufferStorage(GL_RENDERBUFFER,      // give to this
                              GL_DEPTH_COMPONENT16, // data format(16 bit depth component)(def.)
                              backingWidth,         // width of DB
                              backingHeight         // ht of DB
                              );
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,       // attach to this
                                  GL_DEPTH_ATTACHMENT,  // attach here
                                  GL_RENDERBUFFER,      // attach this
                                  depthRenderBuffer     // depthRenderBuffer: this is bound to third para.
                                  );
        
        // FB test
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return(nil);
        }
        
        // FB is ready now
        printf("Renderer: %s | GL Version: %s | GLSL Version: %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating=NO;     // since we are still in constructor
        animationFrameInterval=60;  // default since iOS 8.2 (current iOS: 13.3); ours runs max on iOS 11.4, after that it gives warnings
        
        // OpenGL code
        // vertex shader PF-
        // define vertex shader object
        gVertexShaderObject_PF = glCreateShader(GL_VERTEX_SHADER);    // creates a shader that is given as parameter    // 1st line of programmable pipeline !
        
        // write vertex shader source code
        const GLchar* vertexShaderSourceCode_PF =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "out vec3 t_norm;" \
        "out vec3 light_direction;" \
        "out vec3 viewer_vector;" \
        "uniform mat4 u_m_matrix;" \
        "uniform mat4 u_v_matrix;" \
        "uniform mat4 u_p_matrix;" \
        "uniform vec4 u_light_position;" \
        "uniform mediump int u_isDoubleTapMade;" \
        "void main(void)" \
        "{" \
        "if(u_isDoubleTapMade==1)" \
        "{" \
        "vec4 eye_coordinates =  u_v_matrix * u_m_matrix * vPosition;" \
        "t_norm = mat3(u_v_matrix * u_m_matrix) * vNormal;" \
        "light_direction = vec3(u_light_position - eye_coordinates);" \
        "viewer_vector = vec3(-eye_coordinates.xyz);" \
        "}" \
        "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
        "}";    // written in Graphics Library Shading/Shader Language (GLSL)
                // 300: OpenGL version support*100 (3.0*100); es: embedded system
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
                // ****VERY IMPORTANT****: uniform mediump int u_isDoubleTapMade; program does not run if int not made mediump. Precision is imp. in ES. VS by def has highp for all. But int does not require highp(precision is related to battery life)
                // eye coordinates are not related to any projection, hence only mv
                // uniform mat4 u_mv_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
                // mat3 normal_matrix = mat3(u_mv_matrix): typecasting into mat3 gives you the normal matrix i.e. mat3(transpose(inverse(u_mv_matrix))); It gives first upper 3*3 matrix of the 4*4 matrix. transpose(), inverse() are shader functions
                // t_norm: transformed normal. normalize() is a shader function (our 'n')
                // vec3(u_light_position - eye_coordinates)= typecasting vec4(x,y,z,w) into vec3;
                // tn_dot_lightDirection (s.n): tn=transformed normal
                // reflect(-light_direction,t_norm): -light_direction since reflected ray is opposite to light direction
                // viewer_vector = normalize(vec3(-eye_coordinates.xyx)): -eye_coordinates since viewer's coordinates are opposite to object's eye coordinates
                // ambient = u_la * u_ka: Ia = La * Ka
                // diffuse = u_ld * u_kd * tn_dot_lightDirection: Id = Ld * Kd * (s.n)
                // specular = u_ls * u_ks * pow(max(dot(reflection_vector * viewer_vector),0.0),u_material_shininess): Is = Ls * Ks * (r.v)^f where r: reflection vector, v: viewer vector, f: material shininess (f= exponential factor)
                // phong_ads_light = ambient + diffuse + specular: I = Ia + Id + Is
                // gl_Position: in-built variable of shader
                // u_p_matrix * u_v_matrix * u_m_matrix: opposite sequence of mvp
        
        // give above source code to the vertex shader object
        glShaderSource(
                       gVertexShaderObject_PF,    // shader to which the source code is to be given
                       1,    // the number of strings in the array
                       (const GLchar **)&vertexShaderSourceCode_PF,
                       NULL    // length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
                       // If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string
                       );    // replace the source code in a shader object
        
        // compile the vertex shader
        glCompileShader(gVertexShaderObject_PF);
        // error checking
        GLint iShaderCompileStatus = 0;
        GLint iInfoLogLength = 0;
        GLchar *szInfoLog = NULL;
        
        glGetShaderiv(    // iv= integer vector
                      gVertexShaderObject_PF,    // the shader object to be queried.
                      GL_COMPILE_STATUS,        // what(object parameter) is to be queried
                      &iShaderCompileStatus    // empty    //  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
                      );    // return a parameter from a shader object
        if (iShaderCompileStatus == GL_FALSE)    // error present
        {
            // check if the compiler has any info. about the error
            glGetShaderiv(gVertexShaderObject_PF,
                          GL_INFO_LOG_LENGTH,
                          &iInfoLogLength);    // returns the number of characters in the information log for shader
            if (iInfoLogLength > 0)
            {
                // *before writing into any pointer, allocate it memory (else exception)
                szInfoLog = (GLchar*)malloc(iInfoLogLength);    // allocate it memory equal to iInfoLogLength
                if (szInfoLog != NULL)    // memory allocated
                {
                    GLsizei written;    // temporary var. (GLsizei: typedef of int)
                    glGetShaderInfoLog(
                                       gVertexShaderObject_PF,    // the shader object whose information log is to be queried.
                                       iInfoLogLength,            // the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
                                       &written,                // returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
                                       szInfoLog                // an array of characters that is used to return the information log
                                       );                            // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation.
                    // When a shader object is created, its information log will be a string of length 0.
                    printf("\nVertex Shader Compilation Log : %s", szInfoLog);
                    free(szInfoLog);    // not needed now
                    [self release];
                }
            }
        }
        
        // vertex converted to fragment now
        
        // fragment shader PF-
        // define fragment shader object
        gFragmentShaderObject_PF = glCreateShader(GL_FRAGMENT_SHADER);    // creates a shader that is given as parameter
        
        // write fragment shader source code
        const GLchar* fragmentShaderSourceCode_PF =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 t_norm;" \
        "in vec3 light_direction;" \
        "in vec3 viewer_vector;" \
        "uniform vec3 u_la;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_ls;" \
        "uniform vec3 u_ka;" \
        "uniform vec3 u_kd;" \
        "uniform vec3 u_ks;" \
        "uniform float u_material_shininess;" \
        "uniform int u_isDoubleTapMade;" \
        "out vec4 frag_color;" \
        "vec3 phong_ads_light;" \
        "void main(void)" \
        "{" \
        "if(u_isDoubleTapMade==1)" \
        "{" \
        "vec3 normalized_t_norm = normalize(t_norm);" \
        "vec3 normalized_light_direction = normalize(light_direction);" \
        "vec3 normalized_viewer_vector = normalize(viewer_vector);" \
        "vec3 reflection_vector = reflect(-normalized_light_direction,normalized_t_norm);" \
        "float tn_dot_lightDirection = max(dot(normalized_light_direction,normalized_t_norm),0.0);" \
        "vec3 ambient = u_la * u_ka;" \
        "vec3 diffuse = u_ld * u_kd * tn_dot_lightDirection;" \
        "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, normalized_viewer_vector),0.0),u_material_shininess);" \
        "phong_ads_light = ambient + diffuse + specular;" \
        "}" \
        "else" \
        "{" \
        "phong_ads_light = vec3(1.0, 1.0, 1.0);" \
        "}" \
        "frag_color = vec4(phong_ads_light, 1.0);" \
        "}";    // written in Graphics Library Shading/Shader Language (GLSL)
                // 450: OpenGL version support*100 (4.5*100); core: core profile
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // out: output of shader; vPosition: 'v' for attribute (v for vertex and attributes are for vertex)
                // precision highp float: give high precision to float.(error if not specified) Use mediump for int(def). VS by def has highp for all. Max processing happens in FS than VS as FS gives ultimate color.
                // precision needed only in ES as there are many math units
                // vec4: function/macro/constructor (of vmath?)
                // frag_color = vec4(phong_ads_light,1.0): 1.0 just for converting vec3 to vec4
        
        // give above source code to the fragment shader object
        glShaderSource(
                       gFragmentShaderObject_PF,    // shader to which the source code is to be given
                       1,        // the number of strings in the array
                       (const GLchar **)&fragmentShaderSourceCode_PF,
                       NULL    // length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
                       // If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string
                       );            // replace the source code in a shader object
        
        // compile the fragment shader
        glCompileShader(gFragmentShaderObject_PF);
        // error checking
        iShaderCompileStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(    // iv= integer vector
                      gFragmentShaderObject_PF,    //  the shader object to be queried.
                      GL_COMPILE_STATUS,        // what(object parameter) is to be queried
                      &iShaderCompileStatus    // empty    //  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
                      );    // return a parameter from a shader object
        if (iShaderCompileStatus == GL_FALSE)    // error present
        {
            // check if the compiler has any info. about the error
            glGetShaderiv(gFragmentShaderObject_PF,
                          GL_INFO_LOG_LENGTH,
                          &iInfoLogLength);    // returns the number of characters in the information log for shader
            if (iInfoLogLength > 0)
            {
                // *before writing into any pointer, allocate it memory (else exception)
                szInfoLog = (GLchar*)malloc(iInfoLogLength);    // allocate it memory equal to iInfoLogLength
                if (szInfoLog != NULL)    // memory allocated
                {
                    GLsizei written;    // temporary var. (GLsizei: typedef of int)
                    glGetShaderInfoLog(
                                       gFragmentShaderObject_PF,    // the shader object whose information log is to be queried.
                                       iInfoLogLength,            // the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
                                       &written,                // returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
                                       szInfoLog                // an array of characters that is used to return the information log
                                       );                        // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation.
                    // When a shader object is created, its information log will be a string of length 0.
                    printf("\nFragment Shader Compilation Log : %s", szInfoLog);
                    free(szInfoLog);    // not needed now
                    [self release];
                }
            }
        }
        
        // create shader program object
        gShaderProgramObject_PF = glCreateProgram();    // same program for all shaders
        
        // attach vertex shader to the shader program
        glAttachShader(gShaderProgramObject_PF, gVertexShaderObject_PF);    // 1st para: the program object to which a shader object will be attached
        // 2nd para: the shader object that is to be attached.
        
        // attach vertex shader to the shader program
        glAttachShader(gShaderProgramObject_PF, gFragmentShaderObject_PF); // 1st para: the program object to which a shader object will be attached
        // 2nd para: the shader object that is to be attached.
        
        // pre-linking binding of gShaderProgramObject_PF to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
        glBindAttribLocation(gShaderProgramObject_PF,    // the handle of the program object in which the association is to be made.
                             AMC_ATTRIBUTE_POSITION,                    // the index of the generic vertex attribute to be bound.
                             "vPosition"                                // a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
                             );    // bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
                                  // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition
        
        glBindAttribLocation(gShaderProgramObject_PF,    // the handle of the program object in which the association is to be made.
                             AMC_ATTRIBUTE_NORMAL,                    // the index of the generic vertex attribute to be bound.
                             "vNormal"                                // a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
                             );
        
        // link the shader program to your program
        glLinkProgram(gShaderProgramObject_PF);    // If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
        // error checking (eg. of link error- version incompatibility of shaders)
        GLint iProgramLinkStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetProgramiv(                // iv= integer vector
                       gShaderProgramObject_PF,    //  the program to be queried.
                       GL_LINK_STATUS,            // what(object parameter) is to be queried
                       &iProgramLinkStatus        // empty    //  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
                       );                            // return a parameter from a program object
        if (iProgramLinkStatus == GL_FALSE)    // error present
        {
            // check if the linker has any info. about the error
            glGetProgramiv(gShaderProgramObject_PF,
                           GL_INFO_LOG_LENGTH,
                           &iInfoLogLength);    // returns the number of characters in the information log for shader
            if (iInfoLogLength > 0)
            {
                // *before writing into any pointer, allocate it memory (else exception)
                szInfoLog = (GLchar*)malloc(iInfoLogLength);    // allocate it memory equal to iInfoLogLength
                if (szInfoLog != NULL)    // memory allocated
                {
                    GLsizei written;    // temporary var. (GLsizei: typedef of int)
                    glGetProgramInfoLog(
                                        gShaderProgramObject_PF,    // the program object whose information log is to be queried.
                                        iInfoLogLength,            // the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
                                        &written,                // returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
                                        szInfoLog                // an array of characters that is used to return the information log
                                        );                            // The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
                    // When a program object is created, its information log will be a string of length 0.
                    printf("\nShader Program Link Log : %s", szInfoLog);
                    free(szInfoLog);    // not needed now
                    [self release];
                }
            }
        }
        
        // post-linking retrieving uniform locations (uniforms are global to shaders)
        mUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_m_matrix");    // preparation of data transfer from CPU to GPU (binding)
        // u_mvp_matrix: of GPU; mvpUniform: of CPU
        // telling it to take location of uniform u_mvp_matrix and give in mvpUniform
        vUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_v_matrix");
        pUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_p_matrix");
        laUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_la");
        ldUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_ld");
        lsUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_ls");
        lightPositionUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_light_position");
        kaUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_ka");
        kdUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_kd");
        ksUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_ks");
        materialShininessUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_material_shininess");
        isDoubleTapMadeUniform_PF = glGetUniformLocation(gShaderProgramObject_PF, "u_isDoubleTapMade");
        
        // ============================================================================================================================================================================
        
        // vertex shader PV-
        // define vertex shader object
        gVertexShaderObject_PV = glCreateShader(GL_VERTEX_SHADER);    // creates a shader that is given as parameter    // 1st line of programmable pipeline !
        
        // write vertex shader source code
        const GLchar* vertexShaderSourceCode_PV =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec3 vNormal;" \
        "uniform mat4 u_m_matrix;" \
        "uniform mat4 u_v_matrix;" \
        "uniform mat4 u_p_matrix;" \
        "uniform vec3 u_la;" \
        "uniform vec3 u_ld;" \
        "uniform vec3 u_ls;" \
        "uniform vec4 u_light_position;" \
        "uniform vec3 u_ka;" \
        "uniform vec3 u_kd;" \
        "uniform vec3 u_ks;" \
        "uniform float u_material_shininess;" \
        "uniform mediump int u_isDoubleTapMade;" \
        "out vec3 phong_ads_light;" \
        "void main(void)" \
        "{" \
        "if(u_isDoubleTapMade == 1)" \
        "{" \
        "vec4 eye_coordinates = u_v_matrix * u_m_matrix * vPosition;" \
        "vec3 t_norm = normalize(mat3(u_v_matrix * u_m_matrix) * vNormal);" \
        "vec3 light_direction = normalize(vec3(u_light_position - eye_coordinates));" \
        "float tn_dot_lightDirection = max(dot(light_direction,t_norm),0.0);" \
        "vec3 reflection_vector = reflect(-light_direction,t_norm);" \
        "vec3 viewer_vector = normalize(vec3(-eye_coordinates));" \
        "vec3 ambient = u_la * u_ka;" \
        "vec3 diffuse = u_ld * u_kd * tn_dot_lightDirection;" \
        "vec3 specular = u_ls * u_ks * pow(max(dot(reflection_vector, viewer_vector),0.0),u_material_shininess);" \
        "phong_ads_light = ambient + diffuse + specular;" \
        "}" \
        "else" \
        "{" \
        "phong_ads_light = vec3(1.0, 1.0, 1.0);" \
        "}" \
        "gl_Position = u_p_matrix * u_v_matrix * u_m_matrix * vPosition;" \
        "}";    // written in Graphics Library Shading/Shader Language (GLSL)
        // 300: OpenGL version support*100 (3.0*100); es: embedded system
        // '\n' important and compulsory since shader file would have an enter after the version stmt
        // in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
        // uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
        // gl_Position: in-built variable of shader
        
        // give above source code to the vertex shader object
        glShaderSource(
                       gVertexShaderObject_PV,    // shader to which the source code is to be given
                       1,    // the number of strings in the array
                       (const GLchar **)&vertexShaderSourceCode_PV,
                       NULL    // length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
                       // If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string
                       );    // replace the source code in a shader object
        
        // compile the vertex shader
        glCompileShader(gVertexShaderObject_PV);
        // error checking
        iShaderCompileStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(    // iv= integer vector
                      gVertexShaderObject_PV,    // the shader object to be queried.
                      GL_COMPILE_STATUS,        // what(object parameter) is to be queried
                      &iShaderCompileStatus    // empty    //  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
                      );    // return a parameter from a shader object
        if (iShaderCompileStatus == GL_FALSE)    // error present
        {
            // check if the compiler has any info. about the error
            glGetShaderiv(gVertexShaderObject_PV,
                          GL_INFO_LOG_LENGTH,
                          &iInfoLogLength);    // returns the number of characters in the information log for shader
            if (iInfoLogLength > 0)
            {
                // *before writing into any pointer, allocate it memory (else exception)
                szInfoLog = (GLchar*)malloc(iInfoLogLength);    // allocate it memory equal to iInfoLogLength
                if (szInfoLog != NULL)    // memory allocated
                {
                    GLsizei written;    // temporary var. (GLsizei: typedef of int)
                    glGetShaderInfoLog(
                                       gVertexShaderObject_PV,    // the shader object whose information log is to be queried.
                                       iInfoLogLength,            // the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
                                       &written,                // returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
                                       szInfoLog                // an array of characters that is used to return the information log
                                       );                            // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation.
                    // When a shader object is created, its information log will be a string of length 0.
                    printf("\nVertex Shader Compilation Log : %s", szInfoLog);
                    free(szInfoLog);    // not needed now
                    [self release];
                }
            }
        }
        
        // vertex converted to fragment now
        
        // fragment shader-
        // define fragment shader object
        gFragmentShaderObject_PV = glCreateShader(GL_FRAGMENT_SHADER);    // creates a shader that is given as parameter
        
        // write fragment shader source code
        const GLchar* fragmentShaderSourceCode_PV =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec3 phong_ads_light;" \
        "out vec4 frag_color;" \
        "void main(void)" \
        "{" \
        "frag_color = vec4(phong_ads_light,1.0);" \
        "}";    // written in Graphics Library Shading/Shader Language (GLSL)
        // 450: OpenGL version support*100 (4.5*100); core: core profile
        // '\n' important and compulsory since shader file would have an enter after the version stmt
        // out: output of shader
        // vec4: function/macro/constructor (of vmath?)
        // FragColor = vec4(1.0,1.0,0.0,0.0): will give yellow color to fragment; don't write 'f' in shaders (if class)
        
        // give above source code to the fragment shader object
        glShaderSource(
                       gFragmentShaderObject_PV,    // shader to which the source code is to be given
                       1,        // the number of strings in the array
                       (const GLchar **)&fragmentShaderSourceCode_PV,
                       NULL    // length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
                       // If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string
                       );            // replace the source code in a shader object
        
        // compile the fragment shader
        glCompileShader(gFragmentShaderObject_PV);
        // error checking
        iShaderCompileStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(    // iv= integer vector
                      gFragmentShaderObject_PV,    //  the shader object to be queried.
                      GL_COMPILE_STATUS,        // what(object parameter) is to be queried
                      &iShaderCompileStatus    // empty    //  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
                      );    // return a parameter from a shader object
        if (iShaderCompileStatus == GL_FALSE)    // error present
        {
            // check if the compiler has any info. about the error
            glGetShaderiv(gFragmentShaderObject_PV,
                          GL_INFO_LOG_LENGTH,
                          &iInfoLogLength);    // returns the number of characters in the information log for shader
            if (iInfoLogLength > 0)
            {
                // *before writing into any pointer, allocate it memory (else exception)
                szInfoLog = (GLchar*)malloc(iInfoLogLength);    // allocate it memory equal to iInfoLogLength
                if (szInfoLog != NULL)    // memory allocated
                {
                    GLsizei written;    // temporary var. (GLsizei: typedef of int)
                    glGetShaderInfoLog(
                                       gFragmentShaderObject_PV,    // the shader object whose information log is to be queried.
                                       iInfoLogLength,            // the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
                                       &written,                // returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
                                       szInfoLog                // an array of characters that is used to return the information log
                                       );                        // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation.
                    // When a shader object is created, its information log will be a string of length 0.
                    printf("\nFragment Shader Compilation Log : %s", szInfoLog);
                    free(szInfoLog);    // not needed now
                    [self release];
                }
            }
        }
        
        // create shader program object
        gShaderProgramObject_PV = glCreateProgram();    // same program for all shaders
        
        // attach vertex shader to the shader program
        glAttachShader(gShaderProgramObject_PV, gVertexShaderObject_PV);    // 1st para: the program object to which a shader object will be attached
        // 2nd para: the shader object that is to be attached.
        
        // attach vertex shader to the shader program
        glAttachShader(gShaderProgramObject_PV, gFragmentShaderObject_PV); // 1st para: the program object to which a shader object will be attached
        // 2nd para: the shader object that is to be attached.
        
        // pre-linking binding of gShaderProgramObject_PV to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
        glBindAttribLocation(gShaderProgramObject_PV,    // the handle of the program object in which the association is to be made.
                             AMC_ATTRIBUTE_POSITION,                    // the index of the generic vertex attribute to be bound.
                             "vPosition"                                // a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
                             );    // bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
        // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition
        
        glBindAttribLocation(gShaderProgramObject_PV,    // the handle of the program object in which the association is to be made.
                             AMC_ATTRIBUTE_NORMAL,                    // the index of the generic vertex attribute to be bound.
                             "vNormal"                                // a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
                             );
        
        // link the shader program to your program
        glLinkProgram(gShaderProgramObject_PV);    // If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
        // error checking (eg. of link error- version incompatibility of shaders)
        iProgramLinkStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetProgramiv(                // iv= integer vector
                       gShaderProgramObject_PV,    //  the program to be queried.
                       GL_LINK_STATUS,            // what(object parameter) is to be queried
                       &iProgramLinkStatus        // empty    //  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
                       );                            // return a parameter from a program object
        if (iProgramLinkStatus == GL_FALSE)    // error present
        {
            // check if the linker has any info. about the error
            glGetProgramiv(gShaderProgramObject_PV,
                           GL_INFO_LOG_LENGTH,
                           &iInfoLogLength);    // returns the number of characters in the information log for shader
            if (iInfoLogLength > 0)
            {
                // *before writing into any pointer, allocate it memory (else exception)
                szInfoLog = (GLchar*)malloc(iInfoLogLength);    // allocate it memory equal to iInfoLogLength
                if (szInfoLog != NULL)    // memory allocated
                {
                    GLsizei written;    // temporary var. (GLsizei: typedef of int)
                    glGetProgramInfoLog(
                                        gShaderProgramObject_PV,    // the program object whose information log is to be queried.
                                        iInfoLogLength,            // the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
                                        &written,                // returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
                                        szInfoLog                // an array of characters that is used to return the information log
                                        );                            // The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
                    // When a program object is created, its information log will be a string of length 0.
                    printf("\nShader Program Link Log : %s", szInfoLog);
                    free(szInfoLog);    // not needed now
                    [self release];
                }
            }
        }
        
        // post-linking retrieving uniform locations (uniforms are global to shaders)
        mUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_m_matrix");    // preparation of data transfer from CPU to GPU (binding)
        // u_mvp_matrix: of GPU; mvpUniform: of CPU
        // telling it to take location of uniform u_mvp_matrix and give in mvpUniform
        vUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_v_matrix");
        pUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_p_matrix");
        laUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_la");
        ldUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_ld");
        lsUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_ls");
        lightPositionUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_light_position");
        kaUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_ka");
        kdUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_kd");
        ksUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_ks");
        materialShininessUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_material_shininess");
        isDoubleTapMadeUniform_PV = glGetUniformLocation(gShaderProgramObject_PV, "u_isDoubleTapMade");
        
        // ============================================================================================================================================================================
        
        // fill sphere data in arrays
        float sphere_vertices[1146];
        float sphere_normals[1146];
        float sphere_textures[764];
        short sphere_elements[2280];
        
        // fill sphere vertices in array (this was in Display() in FFP)
        getSphereVertexData(sphere_vertices, sphere_normals, sphere_textures, sphere_elements);
        numVertices = getNumberOfSphereVertices();
        numElements = getNumberOfSphereElements();
        
        // 9 lines
        // create vao(id) (vao is shape-wise)
        // vao
        glGenVertexArrays(1, &vao_sphere);
        glBindVertexArray(vao_sphere);
        
        // position vbo
        glGenBuffers(1, &vbo_sphere_position);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_position);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_vertices), sphere_vertices, GL_STATIC_DRAW);
        
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // normal vbo
        glGenBuffers(1, &vbo_sphere_normal);
        glBindBuffer(GL_ARRAY_BUFFER, vbo_sphere_normal);
        glBufferData(GL_ARRAY_BUFFER, sizeof(sphere_normals), sphere_normals, GL_STATIC_DRAW);
        
        glVertexAttribPointer(AMC_ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, NULL);
        
        glEnableVertexAttribArray(AMC_ATTRIBUTE_NORMAL);
        
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        // element vbo
        glGenBuffers(1, &vbo_sphere_element);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(sphere_elements), sphere_elements, GL_STATIC_DRAW);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        
        // unbind vao
        glBindVertexArray(0);
        
        
        //glClearDepth(1.0f);        // not working here? bringing depth buffer into existance
        // filling the depth buffer with max value
        
        glEnable(GL_DEPTH_TEST);    // to compare depth values of objects
        
        glDepthFunc(GL_LEQUAL);    // specifies the value used for depth-buffer comparisons
        // Passes if the incoming z value is less than or equal to the stored z value.
        // GL_LEQUAL : GLenum
        
        // set background color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);    // bringing color buffer into existance
        // specifies clear values[0,1] used by glClear() for the color buffers
        
        modelMatrix = vmath::mat4::identity();
        viewMatrix = vmath::mat4::identity();
        perspectiveProjectionMatrix = vmath::mat4::identity();    // making orthographicProjectionMatrix an identity matrix(diagonals 1)
        // mat4: of vmath
        
        // GESTURE RECOGNITION (register and define your gestures)
        // tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        // initWithTarget:self: self is going to respond to the single tap (called target-action pattern)
        // 'onSingleTap:' is our method (':' tells it's a fn, not a string)
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger (touches=fingers)
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];     // register gesture recognizer with self
        
        // similar code for double tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger (touches=fingers)
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];     // register gesture recognizer with self
        
        // * this will allow to diffrentiate between single tap and double tap *
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; // to avoid taking 1st tap of double tap as single tap, fail that 1st tap as single tap
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        // no properties to be set
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}   // constructor ends

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
  We have drawView()
*/

+(Class)layerClass  // static method(start with +);     must be implemented
{
    // code
    // obtain class of CAEAGLLayer
    return([CAEAGLLayer class]);        // class: method
}

-(void)layoutSubviews   // inherited from UIView    ~Resize()
{
    // variable declarations
    GLint width;
    GLint height;
    
    // code
    // window size changed so again fill CRB and DRB
    // color buffer
    
    // no call to glGenRenderbuffers() as it is done by layer
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];   // eaglLayer not available here; GL_RENDERBUFFER: colorRenderBuffer
    // self.layer(our drawable) has data(eg. h & w of window) to fill in CB and returns object of CAEAGLLayer
    // hence we implemented layerClass()
    
    // depth buffer
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,       // take from this (GL_RENDERBUFFER= CB)
                                 GL_RENDERBUFFER_WIDTH, // take this
                                 &width          // take into this
                                 );
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);  // depth buffer according to color buffer
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    
    // establish data storage, format and dimensions of a renderbuffer object's image not from context but from OGL
    glRenderbufferStorage(GL_RENDERBUFFER,      // give to this
                          GL_DEPTH_COMPONENT16, // data format(16 bit depth component)(def.)
                          width,         // width of DB
                          height         // ht of DB
                          );
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,       // attach to this
                              GL_DEPTH_ATTACHMENT,  // attach here
                              GL_RENDERBUFFER,      // attach this
                              depthRenderBuffer     // depthRenderBuffer: this is bound to third para.
                              );    // replaces previous depth attachment
    
    if (height == 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, width, height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);    // ? other values of near & far not working
    // parameters:
    // fovy- The field of view angle, in degrees, in the y - direction.
    // aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
    // zNear- The distance from the viewer to the near clipping plane(always positive).
    // zFar- The distance from the viewer to the far clipping plane(always positive).
    
    // FB test
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
        // don't return since this does not usually occur
    }
    
    // projection code here
    
    [self drawView:nil];    // warm-up call to repaint
}

-(void)drawView:(id)sender      // instance method (starts with -)
{
    // code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);  // since you are going to render from this
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // sphere
    // 4 CPU steps
    // declaration of matrices
    vmath::mat4 translationMatrix;
    vmath::mat4 rotationMatrix;
    
    // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
    modelMatrix = vmath::mat4::identity();
    viewMatrix = vmath::mat4::identity();
    translationMatrix = vmath::mat4::identity();
    
    // do necessary transformation (T,S,R)
    translationMatrix = vmath::translate(0.0f, 0.0f, -2.0f);
    
    // do necessary matrix multiplication
    modelMatrix = translationMatrix;
    
    // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
    if (gbVPressed == true)
    {
        glUseProgram(gShaderProgramObject_PV);    // binding your OpenGL code with shader program object
        // Specifies the handle of the program object whose executables are to be used as part of current rendering state.
        
        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        glUniformMatrix4fv(mUniform_PV,        // uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
                           1,                                // how many matrices to send?
                           GL_FALSE,                        // do transpose? ; no since OGL and GLSL are column-major
                           modelMatrix                    // actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
                           );
        
        glUniformMatrix4fv(vUniform_PV,        // uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
                           1,                                // how many matrices to send?
                           GL_FALSE,                        // do transpose? ; no since OGL and GLSL are column-major
                           viewMatrix                    // actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
                           );
        
        glUniformMatrix4fv(pUniform_PV,        // uniform in which perspectiveProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
                           1,                                // how many matrices to send?
                           GL_FALSE,                        // do transpose? ; no since OGL and GLSL are column-major
                           perspectiveProjectionMatrix        // actual matrix; this will bind to pUniform which is bound to u_p_matrix
                           );
        
        if (gbLighting == true)
        {
            glUniform1i(isDoubleTapMadeUniform_PV, 1);    // send 1 if 'L' key is pressed; 1i: to send 1 int
            
            // setting light properties
            glUniform3fv(laUniform_PV, 1, light_ambient);    // white light; to send vec3
            glUniform3fv(ldUniform_PV, 1, light_diffuse);
            glUniform3fv(lsUniform_PV, 1, light_specular);
            glUniform4fv(lightPositionUniform_PV, 1, light_position);    // no 1 here; to send vec4; ~glLightfv(GL_LIGHT0, GL_POSITION, LightPosition) in FFP
            
            // setting material properties
            glUniform3fv(kaUniform_PV, 1, material_ambient);
            glUniform3fv(kdUniform_PV, 1, material_diffuse);
            glUniform3fv(ksUniform_PV, 1, material_specular);
            glUniform1f(materialShininessUniform_PV, material_shininess);
        }
        else
        {
            glUniform1i(isDoubleTapMadeUniform_PV, 0);    // send 0 if 'L' key is not pressed; 1i: to send 1 int
        }
    }
    
    else
    {
        glUseProgram(gShaderProgramObject_PF);
        
        // fill and send uniforms
        // send necessary matrices to shader in respective uniforms (on GPU)
        glUniformMatrix4fv(mUniform_PF,        // uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
                           1,                                // how many matrices to send?
                           GL_FALSE,                        // do transpose? ; no since OGL and GLSL are column-major
                           modelMatrix                    // actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
                           );
        
        glUniformMatrix4fv(vUniform_PF,        // uniform in which modelViewMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
                           1,                                // how many matrices to send?
                           GL_FALSE,                        // do transpose? ; no since OGL and GLSL are column-major
                           viewMatrix                    // actual matrix; this will bind to mvUniform which is bound to u_mv_matrix
                           );
        
        glUniformMatrix4fv(pUniform_PF,        // uniform in which perspectiveProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
                           1,                                // how many matrices to send?
                           GL_FALSE,                        // do transpose? ; no since OGL and GLSL are column-major
                           perspectiveProjectionMatrix        // actual matrix; this will bind to pUniform which is bound to u_p_matrix
                           );
        
        if (gbLighting == true)
        {
            glUniform1i(isDoubleTapMadeUniform_PF, 1);    // send 1 if 'L' key is pressed; 1i: to send 1 int
            
            // setting light properties
            glUniform3fv(laUniform_PF, 1, light_ambient);    // white light; to send vec3
            glUniform3fv(ldUniform_PF, 1, light_diffuse);
            glUniform3fv(lsUniform_PF, 1, light_specular);
            glUniform4fv(lightPositionUniform_PF, 1, light_position);    // no 1 here; to send vec4; ~glLightfv(GL_LIGHT0, GL_POSITION, LightPosition) in FFP
            
            // setting material properties
            glUniform3fv(kaUniform_PF, 1, material_ambient);
            glUniform3fv(kdUniform_PF, 1, material_diffuse);
            glUniform3fv(ksUniform_PF, 1, material_specular);
            glUniform1f(materialShininessUniform_PF, material_shininess);
        }
        else
        {
            glUniform1i(isDoubleTapMadeUniform_PF, 0);    // send 0 if 'L' key is not pressed; 1i: to send 1 int
        }
    }
    
    // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
    // *** bind vao ***
    glBindVertexArray(vao_sphere);
    
    // *** draw, either by glDrawTriangles() or glDrawArrays() or glDrawElements()
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_sphere_element);
    
    glDrawElements(
                   GL_TRIANGLES, // what kind of primitives to render (mode)
                   numElements, // the number of elements to be rendered (count)
                   GL_UNSIGNED_SHORT,    //  type of the values in last parameter
                   0    // Specifies a pointer to the location where the indices are stored (indices)
                   );    // When glDrawElements is called, it uses 'count' sequential indices from 'indices' to lookup elements in enabled arrays to construct a sequence of geometric primitives.
    // 'mode' specifies what kind of primitives are constructed, and how the array elements construct these primitives.
    // If GL_VERTEX_ARRAY is not enabled, no geometric primitives are constructed.
    
    // unbind vbo
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    
    // *** unbind vao ***
    glBindVertexArray(0);
    
    
    // unuse program
    glUseProgram(0);    // unbinding your OpenGL code with shader program object
    // If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);     // since CB renders
    // no binding to DB as CBs could be many but DB is single
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];  // present render buffer to drawable ~glSwapBuffers()
}

-(void)startAnimation   // our method, not UIView's
{
    if(!isAnimating)    // or if(isAnimating==NO); animate if not animating
    {
        // create displayLink
        displayLink=[NSClassFromString(@"CADisplayLink")    // get CADisplayLink class
                     displayLinkWithTarget:self             // defined in CADisplayLink class
                     selector:@selector(drawView:)];        // selector: method
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        // add our displayLink function(drawView) to run loop (animation start)
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop]
                     forMode:NSDefaultRunLoopMode      // purpose to add
        ];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation    // our method, not UIView's
{
    if(isAnimating)     // r if(isAnimating==YES); stop animation if animating
    {
        [displayLink invalidate];   // removes displayLink from run loop
                                    // unregisters and removes it
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);        // YES means method is implemented
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event    // ~ Android's onTouchEvent()
{
    
}

// our 4 methods
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    if (gbLighting == true)
    {
        if(gbVPressed == true)
        {
            gbVPressed = false;
            gbFPressed = true;
        }
        else if(gbFPressed == true)
        {
            gbFPressed = false;
            gbVPressed = true;
        }
    }
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    if (gbLighting == false)
        gbLighting = true;
    else
        gbLighting = false;
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // exit code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

-(void) dealloc
{
    // code
    if (vbo_sphere_element)
    {
        glDeleteBuffers(1, &vbo_sphere_element);    // delete named buffer objects
        // 1: number of buffer objects to be deleted
        // &vbo: array of buffer objects to be deleted
        vbo_sphere_element = 0;
    }
    
    if (vbo_sphere_normal)
    {
        glDeleteBuffers(1, &vbo_sphere_normal);    // delete named buffer objects
        // 1: number of buffer objects to be deleted
        // &vbo: array of buffer objects to be deleted
        vbo_sphere_normal = 0;
    }
    
    if (vbo_sphere_position)
    {
        glDeleteBuffers(1, &vbo_sphere_position);    // delete named buffer objects
        // 1: number of buffer objects to be deleted
        // &vbo: array of buffer objects to be deleted
        vbo_sphere_position = 0;
    }
    
    if (vao_sphere)
    {
        glDeleteVertexArrays(1, &vao_sphere);        // delete vertex array objects
        vao_sphere = 0;
    }
    
    if (gShaderProgramObject_PF)
    {
        GLsizei shaderCount;    // typedef int
        GLsizei shaderNumber;
        
        glUseProgram(gShaderProgramObject_PF);        // since unused in Display()
        
        // ask program that how many shaders are attached to it
        glGetProgramiv(gShaderProgramObject_PF,
                       GL_ATTACHED_SHADERS,        // returns the number of shader objects attached to shader program
                       &shaderCount);
        
        GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);        // dynamic array for shaders since we don't know how many present
        if (pShaders)    // mem allocated
        {
            // take attached shaders  into above array
            glGetAttachedShaders(gShaderProgramObject_PF,        // the program object to be queried
                                 shaderCount,                                // the size of the array for storing the returned object names
                                 &shaderCount,                                // Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
                                 pShaders                                    // an array that is used to return the names of attached shader objects(empty now)
                                 );        // return the handles of the shader objects attached to a program object
            
            for (shaderNumber = 0;shaderNumber < shaderCount;shaderNumber++)
            {
                // detach each shader
                glDetachShader(gShaderProgramObject_PF, pShaders[shaderNumber]);        // 1st para: Specifies the program object from which to detach the shader object.
                // 2nd para: Specifies the shader object to be detached.
                
                // delete each detached shader
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        // delete the shader program
        glDeleteProgram(gShaderProgramObject_PF);    // frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
        gShaderProgramObject_PF = 0;
        glUseProgram(0);
    }
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
