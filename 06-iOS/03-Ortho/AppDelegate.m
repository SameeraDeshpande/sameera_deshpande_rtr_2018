#import "AppDelegate.h"
#import "ViewController.h"
#import "GLESView.h"

@implementation AppDelegate
{
@private
    UIWindow *mainWindow;
    ViewController *mainViewController;
    GLESView *glesView;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Override point for customization after application launch.
    
    // get screen bounds for fullscreen
    CGRect screenBounds=[[UIScreen mainScreen]bounds];  // UIScreen: class; mainScreen: method of UIScreen; UIScreen mainScreen: object for bounds()
    
    // initialize window variable corresponding to screen bounds
    mainWindow=[[UIWindow alloc]initWithFrame:screenBounds];
    
    // create object of ViewController
    mainViewController=[[ViewController alloc]init];
    
    [mainWindow setRootViewController:mainViewController];  // since there can be multiple-view heirarchy
    
    // initialize view variable corresponding to screen bounds
    glesView=[[GLESView alloc]initWithFrame:screenBounds];  // ref. count increased by 1
    
    [mainViewController setView:glesView];
    
    [glesView release];  // ref. count decreased by 1
    
    // add the ViewController's view as subview to the window
    [mainWindow addSubview:[mainViewController view]];  // actually redundant as view already given to window
    
    // make window key window and visible
    [mainWindow makeKeyAndVisible];
    
    [glesView startAnimation];
    
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    // code
    [glesView stopAnimation];
}


- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // calls applicationWillResignActive, hence no code here
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // calls applicationDidBecomeActive, hence no code here
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // code
    [glesView startAnimation];
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // code
    [glesView stopAnimation];
}

-(void) dealloc
{
    // code
    [glesView release];
    
    [mainViewController release];
    
    [mainWindow release];
    
    [super dealloc];
}

@end
