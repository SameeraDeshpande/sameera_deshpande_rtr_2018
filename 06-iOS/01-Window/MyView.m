//
//  MyView.m
//  Window
//
//  Created by user161124 on 3/26/20.
//

#import "MyView.h"

@implementation MyView
{
    NSString *centralText;
}

- (id)initWithFrame:(CGRect)frameRect
{
    self=[super initWithFrame:frameRect];
    if(self)
    {
        // initialization code here
        
        // set scene's background color
        [self setBackgroundColor:[UIColor whiteColor]];     // in mac, this fn not for view but for window
        
        centralText=@"Hello World !!!";
        
        // GESTURE RECOGNITION (register and define your gestures)
        // tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        // initWithTarget:self: self is going to respond to the single tap (called target-action pattern)
        // 'onSingleTap:' is our method (':' tells it's a fn, not a string)

        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger (touches=fingers)
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];     // register gesture recognizer with self
        
        // similar code for double tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger (touches=fingers)
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];     // register gesture recognizer with self
        
        // * this will allow to diffrentiate between single tap and double tap *
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; // to avoid taking 1st tap of double tap as single tap, fail that 1st tap as single tap
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        // no properties to be set
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // black background
    UIColor *fillColor=[UIColor blackColor];    // hence our window appears black (otherwise we have set white color)
    [fillColor set];
    UIRectFill(rect);
    
    // dictionary with kvc
    NSDictionary *dictionaryForTextAttributes=[NSDictionary dictionaryWithObjectsAndKeys: [UIFont fontWithName:@"Helvetica" size:24],
                                               NSFontAttributeName,
                                               [UIColor greenColor],    // thus greenColor is an object (NS in mac, UI in iOS)
                                               NSForegroundColorAttributeName,
                                               nil];    // last element of array as nil
    
    CGSize textSize=[centralText sizeWithAttributes:dictionaryForTextAttributes];   // CG= Core Graphics
    
    CGPoint point;        // struct
    point.x=(rect.size.width/2)-(textSize.width/2);
    point.y=(rect.size.height/2)-(textSize.height/2)+12;
    
    [centralText drawAtPoint:point withAttributes:dictionaryForTextAttributes];
}

// to become first responder
- (BOOL)acceptsFirstResponder
{
    // code
    return(YES);        // YES means method is implemented
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event    // ~ Android's onTouchEvent()
{
    // not setting centralText here as single tap is taken as a touch
    
    [self setNeedsDisplay];     // repainting; in mac, you wrote 'YES' after this
}

// our 4 methods
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    // code
    centralText=@"'onSingleTap' Event Occured";
    [self setNeedsDisplay];     // repainting
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    // code
    centralText=@"'onDoubleTap' Event Occured";
    [self setNeedsDisplay];     // repainting
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // exit code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    // code
    centralText=@"'onLongPress' Event Occured";
    [self setNeedsDisplay];     // repainting
}

- (void)dealloc
{
    [super dealloc];
}

@end
