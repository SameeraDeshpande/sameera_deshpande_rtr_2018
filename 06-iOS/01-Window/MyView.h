//
//  MyView.h
//  Window
//
//  Created by user161124 on 3/26/20.
//

#import <UIKit/UIKit.h>

@interface MyView : UIView <UIGestureRecognizerDelegate>    // MyView : UIView : did while project creation; <UIGestureRecognizerDelegate> : you write

@end
