//
//  AppDelegate.m
//  Window
//
//  Created by user161124 on 3/26/20.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "MyView.h"

@implementation AppDelegate
{
@private
    UIWindow *mainWindow;
    ViewController *mainViewController;
    MyView *myView;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions    // ?: 1st application: fn name; 2nd application: unnamed para.
                                                                                                                // didFinishLaunchingWithOptions: para. key; launchOptions: para. value(named para)
                                                                                                                // UIApplication *: datatype; NSDictionary *: datatype
                                                                                                                // there are various ways(options) to launch an app)
{
    // Override point for customization after application launch.
    
    // get screen bounds for fullscreen
    CGRect screenBounds=[[UIScreen mainScreen]bounds];  // UIScreen: class; mainScreen: method of UIScreen; UIScreen mainScreen: object for bounds()
    
    // initialize window variable corresponding to screen bounds
    mainWindow=[[UIWindow alloc]initWithFrame:screenBounds];
    
    // create object of ViewController
    mainViewController=[[ViewController alloc]init];
    
    [mainWindow setRootViewController:mainViewController];  // since there can be multiple-view heirarchy
    
    // initialize view variable corresponding to screen bounds
    myView=[[MyView alloc]initWithFrame:screenBounds];  // ref. count increased by 1
    
    [mainViewController setView:myView];
    
    [myView release];  // ref. count decreased by 1
    
    // add the ViewController's view as subview to the window
    [mainWindow addSubview:[mainViewController view]];  // actually redundant as view already given to window
    
    // make window key window and visible
    [mainWindow makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void)dealloc
{
    [myView release];
    
    [mainViewController release];
    
    [mainWindow release];
    
    [super dealloc];
}

@end
