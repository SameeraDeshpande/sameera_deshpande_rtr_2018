#import <OpenGLES/ES3/gl.h>     // adding OpenGLES.framework; locked on version 3.0
#import <OpenGLES/ES3/glext.h>  // ~glew.h (extension wrangler)
#import "vmath.h"
#import "GLESView.h"

enum    // nameless since we are just concerned with its "named" indices
{
    AMC_ATTRIBUTE_POSITION=0,
    AMC_ATTRIBUTE_COLOR,        // 1
    AMC_ATTRIBUTE_NORMAL,        // 2
    AMC_ATTRIBUTE_TEXCOORD0,    // 3 (if multiple textures, TEXCOORD0,TEXCOORD1,TEXCOORD2,...)
};

// global variable declarations
GLfloat angle_triangle = 0.0f;
GLfloat angle_rectangle = 0.0f;

@implementation GLESView
{
    EAGLContext *eaglContext;   // EAGL= Embedded Apple Graphics Layer
    
    GLuint defaultFrameBuffer;  // storage for all buffers
    GLuint colorRenderBuffer;   // color buffer, depth buffer & stencil buffer are togetherly called render buffers
    GLuint depthRenderBuffer;
    
    id displayLink;     // id= generic
    NSInteger animationFrameInterval;   // mac's integer
    BOOL isAnimating;
    
    GLuint gVertexShaderObject;
    GLuint gFragmentShaderObject;
    GLuint gShaderProgramObject;
    
    GLuint vao_triangle, vao_rectangle;        // vertex array object    (1 also ok since we unbind)
    GLuint vbo_position_triangle, vbo_color_triangle;
    GLuint vbo_position_rectangle;        // vertex buffer object
    GLuint mvpUniform;        // mvp: model-view projection
    vmath::mat4 perspectiveProjectionMatrix;        // mat4: in vmath.h; it is typedef of a 4-member float array
}

-(id)initWithFrame:(CGRect)frame;
{
    // code
    self=[super initWithFrame:frame];   // self= this
    if(self)
    {
        // get layer from super (i.e. from UIView)
        CAEAGLLayer *eaglLayer=(CAEAGLLayer*)super.layer;   // CA: Core Animation; layer: method; can also write [super layer]
                                                            // layer will be CALayer and not OGL layer so we typecast it to OGL layer
        
        // set its properties
        eaglLayer.opaque=YES;   // setting its alpha to 1.0 or [eaglLayer setOpaque]
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:    // dictionary when multiple properties (object-key array)
                                      [NSNumber numberWithBool:FALSE],              // creating no. from BOOL
                                      kEAGLDrawablePropertyRetainedBacking,         // drawable in iOS= Android's surface (constant i.e. #define)
                                      kEAGLColorFormatRGBA8,                        // 8 bits each for R,G,B,A; it is a live object(struct) and not a constant
                                      kEAGLDrawablePropertyColorFormat,             // this is key of the preceding object
                                      nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];    // constant
        if(eaglContext==nil)
        {
            printf("Couldn't Achieve EAGLContext");
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];    // ~wglMakeCurrent() in Windows or glxMakeCurrent() in Linux or makeCurrentContext() in macOS
        
        // create frame buffer (setup similar to vao, vbo creation and use in Display())
        glGenFramebuffers(1,&defaultFrameBuffer);   // 1: one FB
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);  // GL_FRAMEBUFFER: target(GPU addr); defaultFrameBuffer: bind this

        // color buffer
        glGenRenderbuffers(1,&colorRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];   // GL_RENDERBUFFER: colorRenderBuffer
                                                                                    // eaglLayer(our drawable) has data(eg. h & w of window) to fill in CB
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,       // attach to this
                                  GL_COLOR_ATTACHMENT0, // attach here
                                  GL_RENDERBUFFER,      // attach this
                                  colorRenderBuffer     // colorRenderBuffer: this is bound to third para.
                                  );
        
        // depth buffer
        GLint backingWidth;    // we want DB for these w & h
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,       // take from this (GL_RENDERBUFFER= CB)
                                     GL_RENDERBUFFER_WIDTH, // take this
                                     &backingWidth          // take into this
                                     );
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        
        // establish data storage, format and dimensions of a renderbuffer object's image not from context but from OGL
        glRenderbufferStorage(GL_RENDERBUFFER,      // give to this
                              GL_DEPTH_COMPONENT16, // data format(16 bit depth component)(def.)
                              backingWidth,         // width of DB
                              backingHeight         // ht of DB
                              );
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,       // attach to this
                                  GL_DEPTH_ATTACHMENT,  // attach here
                                  GL_RENDERBUFFER,      // attach this
                                  depthRenderBuffer     // depthRenderBuffer: this is bound to third para.
                                  );
        
        // FB test
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return(nil);
        }
        
        // FB is ready now
        printf("Renderer: %s | GL Version: %s | GLSL Version: %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating=NO;     // since we are still in constructor
        animationFrameInterval=60;  // default since iOS 8.2 (current iOS: 13.3); ours runs max on iOS 11.4, after that it gives warnings
        
        // OpenGL code
        // vertex shader-
        // define vertex shader object
        gVertexShaderObject = glCreateShader(GL_VERTEX_SHADER);    // creates a shader that is given as parameter    // 1st line of programmable pipeline !
        
        // write vertex shader source code
        const GLchar* vertexShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "in vec4 vPosition;" \
        "in vec4 vColor;" \
        "uniform mat4 u_mvp_matrix;" \
        "out vec4 out_color;" \
        "void main(void)" \
        "{" \
        "gl_Position = u_mvp_matrix * vPosition;" \
        "out_color = vColor;" \
        "}";    // written in Graphics Library Shading/Shader Language (GLSL)
                // 300: OpenGL version support*100 (3.0*100); es: embedded system
                // '\n' important and compulsory since shader file would have an enter after the version stmt
                // in: input to shader(attribute); vPosition: 'v' for attribute (v for vertex and attributes are for vertex) (global); vec4 since x,y,z,w
                // uniform mat4 u_mvp_matrix: this mat4 is in-built datatype of GLSL(but equivalent to mat4 of vmath); 'u' for uniform (global)
                // gl_Position: in-built variable of shader
        
        // give above source code to the vertex shader object
        glShaderSource(
                       gVertexShaderObject,    // shader to which the source code is to be given
                       1,    // the number of strings in the array
                       (const GLchar **)&vertexShaderSourceCode,
                       NULL    // length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
                       // If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string
                       );    // replace the source code in a shader object
        
        // compile the vertex shader
        glCompileShader(gVertexShaderObject);
        // error checking
        GLint iShaderCompileStatus = 0;
        GLint iInfoLogLength = 0;
        GLchar *szInfoLog = NULL;
        
        glGetShaderiv(    // iv= integer vector
                      gVertexShaderObject,    // the shader object to be queried.
                      GL_COMPILE_STATUS,        // what(object parameter) is to be queried
                      &iShaderCompileStatus    // empty    //  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
                      );    // return a parameter from a shader object
        if (iShaderCompileStatus == GL_FALSE)    // error present
        {
            // check if the compiler has any info. about the error
            glGetShaderiv(gVertexShaderObject,
                          GL_INFO_LOG_LENGTH,
                          &iInfoLogLength);    // returns the number of characters in the information log for shader
            if (iInfoLogLength > 0)
            {
                // *before writing into any pointer, allocate it memory (else exception)
                szInfoLog = (GLchar*)malloc(iInfoLogLength);    // allocate it memory equal to iInfoLogLength
                if (szInfoLog != NULL)    // memory allocated
                {
                    GLsizei written;    // temporary var. (GLsizei: typedef of int)
                    glGetShaderInfoLog(
                                       gVertexShaderObject,    // the shader object whose information log is to be queried.
                                       iInfoLogLength,            // the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
                                       &written,                // returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
                                       szInfoLog                // an array of characters that is used to return the information log
                                       );                            // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation.
                    // When a shader object is created, its information log will be a string of length 0.
                    printf("\nVertex Shader Compilation Log : %s", szInfoLog);
                    free(szInfoLog);    // not needed now
                    [self release];
                }
            }
        }
        
        // vertex converted to fragment now
        
        // fragment shader-
        // define fragment shader object
        gFragmentShaderObject = glCreateShader(GL_FRAGMENT_SHADER);    // creates a shader that is given as parameter
        
        // write fragment shader source code
        const GLchar* fragmentShaderSourceCode =
        "#version 300 es" \
        "\n" \
        "precision highp float;" \
        "in vec4 out_color;" \
        "out vec4 frag_color;" \
        "void main(void)" \
        "{" \
        "frag_color = out_color;" \
        "}";    // written in Graphics Library Shading/Shader Language (GLSL)
        // 450: OpenGL version support*100 (4.5*100); core: core profile
        // '\n' important and compulsory since shader file would have an enter after the version stmt
        // out: output of shader
        // vec4: function/macro/constructor (of vmath?)
        // FragColor = vec4(1.0,1.0,0.0,0.0): will give yellow color to fragment; don't write 'f' in shaders (if class)
        
        // give above source code to the fragment shader object
        glShaderSource(
                       gFragmentShaderObject,    // shader to which the source code is to be given
                       1,        // the number of strings in the array
                       (const GLchar **)&fragmentShaderSourceCode,
                       NULL    // length of string; telling that you have only 1 string (ending in '\0') and asking OGL to calculate its length till '\0'
                       // If length is NULL, each string is assumed to be null terminated. If length is a value other than NULL, it points to an array containing a string length for each of the corresponding elements of string. Each element in the length array may contain the length of the corresponding string
                       );            // replace the source code in a shader object
        
        // compile the fragment shader
        glCompileShader(gFragmentShaderObject);
        // error checking
        iShaderCompileStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetShaderiv(    // iv= integer vector
                      gFragmentShaderObject,    //  the shader object to be queried.
                      GL_COMPILE_STATUS,        // what(object parameter) is to be queried
                      &iShaderCompileStatus    // empty    //  returns GL_TRUE if the last compile operation on shader was successful, and GL_FALSE otherwise.
                      );    // return a parameter from a shader object
        if (iShaderCompileStatus == GL_FALSE)    // error present
        {
            // check if the compiler has any info. about the error
            glGetShaderiv(gFragmentShaderObject,
                          GL_INFO_LOG_LENGTH,
                          &iInfoLogLength);    // returns the number of characters in the information log for shader
            if (iInfoLogLength > 0)
            {
                // *before writing into any pointer, allocate it memory (else exception)
                szInfoLog = (GLchar*)malloc(iInfoLogLength);    // allocate it memory equal to iInfoLogLength
                if (szInfoLog != NULL)    // memory allocated
                {
                    GLsizei written;    // temporary var. (GLsizei: typedef of int)
                    glGetShaderInfoLog(
                                       gFragmentShaderObject,    // the shader object whose information log is to be queried.
                                       iInfoLogLength,            // the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
                                       &written,                // returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
                                       szInfoLog                // an array of characters that is used to return the information log
                                       );                        // The information log for a shader object is a string that may contain diagnostic messages, warning messages, and other information about the last compile operation.
                    // When a shader object is created, its information log will be a string of length 0.
                    printf("\nFragment Shader Compilation Log : %s", szInfoLog);
                    free(szInfoLog);    // not needed now
                    [self release];
                }
            }
        }
        
        // create shader program object
        gShaderProgramObject = glCreateProgram();    // same program for all shaders
        
        // attach vertex shader to the shader program
        glAttachShader(gShaderProgramObject, gVertexShaderObject);    // 1st para: the program object to which a shader object will be attached
        // 2nd para: the shader object that is to be attached.
        
        // attach vertex shader to the shader program
        glAttachShader(gShaderProgramObject, gFragmentShaderObject); // 1st para: the program object to which a shader object will be attached
        // 2nd para: the shader object that is to be attached.
        
        // pre-linking binding of gShaderProgramObject to vertex attributes (telling linker that link with considering the following attributes) (attributes are private to each shader)
        glBindAttribLocation(gShaderProgramObject,    // the handle of the program object in which the association is to be made.
                             AMC_ATTRIBUTE_POSITION,                    // the index of the generic vertex attribute to be bound.
                             "vPosition"                                // a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
                             );    // bind shader's(i.e. GPU's) vPosition variable to CPU's variable (enum's index)
                                  // give whatever is to be given to vPosition to AMC_ATTRIBUTE_POSITION. AMC_ATTRIBUTE_POSITION will give to vPosition
        
        glBindAttribLocation(gShaderProgramObject,    // the handle of the program object in which the association is to be made.
                             AMC_ATTRIBUTE_COLOR,                    // the index of the generic vertex attribute to be bound.
                             "vColor"                                // a null terminated string containing the name of the vertex shader attribute variable to which the index is to be bound
                             );
        
        // link the shader program to your program
        glLinkProgram(gShaderProgramObject);    // If any shader objects of type GL_VERTEX_SHADER are attached to program, they will be used to create an executable that will run on the programmable vertex processor and so on
        // error checking (eg. of link error- version incompatibility of shaders)
        GLint iProgramLinkStatus = 0;
        iInfoLogLength = 0;
        szInfoLog = NULL;
        
        glGetProgramiv(                // iv= integer vector
                       gShaderProgramObject,    //  the program to be queried.
                       GL_LINK_STATUS,            // what(object parameter) is to be queried
                       &iProgramLinkStatus        // empty    //  returns GL_TRUE if the last link operation on program was successful, and GL_FALSE otherwise.
                       );                            // return a parameter from a program object
        if (iProgramLinkStatus == GL_FALSE)    // error present
        {
            // check if the linker has any info. about the error
            glGetProgramiv(gShaderProgramObject,
                           GL_INFO_LOG_LENGTH,
                           &iInfoLogLength);    // returns the number of characters in the information log for shader
            if (iInfoLogLength > 0)
            {
                // *before writing into any pointer, allocate it memory (else exception)
                szInfoLog = (GLchar*)malloc(iInfoLogLength);    // allocate it memory equal to iInfoLogLength
                if (szInfoLog != NULL)    // memory allocated
                {
                    GLsizei written;    // temporary var. (GLsizei: typedef of int)
                    glGetProgramInfoLog(
                                        gShaderProgramObject,    // the program object whose information log is to be queried.
                                        iInfoLogLength,            // the size of the character buffer for storing the returned information log.(maximum characters that can be returned)
                                        &written,                // returns the length of the string returned in last parameter (the number of characters actually returned, excluding the null termination character)
                                        szInfoLog                // an array of characters that is used to return the information log
                                        );                            // The information log for a program object is either an empty string, or a string containing information about the last link operation, or a string containing information about the last validation operation.It may contain diagnostic messages, warning messages, and other information.
                    // When a program object is created, its information log will be a string of length 0.
                    printf("\nShader Program Link Log : %s", szInfoLog);
                    free(szInfoLog);    // not needed now
                    [self release];
                }
            }
        }
        
        // post-linking retrieving uniform locations (uniforms are global to shaders)
        mvpUniform = glGetUniformLocation(gShaderProgramObject, "u_mvp_matrix");    // preparation of data transfer from CPU to GPU (binding)
        // u_mvp_matrix: of GPU; mvpUniform: of CPU
        // telling it to take location of uniform u_mvp_matrix and give in mvpUniform
        
        // fill triangle vertices in array (this was in Display() in FFP)
        const GLfloat trianglePosition[] = { 0.0f,1.0f,0.0f,
            -1.0f,-1.0f,0.0f,
            1.0f,-1.0f,0.0f, };    // last ',' OK!; const since array of values
        // since written in Initialize() only, coords will not get set everytime in Display(). Hence, fast speed.
        
        const GLfloat triangleColor[] = { 1.0f, 0.0f, 0.0f,    // r
            0.0f,1.0f,0.0f,    // g
            0.0f,0.0f,1.0f    // b
        };
        
        const GLfloat rectanglePosition[] = { 1.0f, 1.0f, 0.0f,
            -1.0f, 1.0f, 0.0f,
            -1.0f, -1.0f, 0.0f,
            1.0f, -1.0f, 0.0f };
        
        // 9 lines
        // create vao(id) (vao is shape-wise)
        glGenVertexArrays(1, &vao_triangle);        // generate vertex array object names
        // 1: Specifies the number of vertex array object names to generate
        // &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
        // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()
        
        glBindVertexArray(vao_triangle);        // bind a vertex array object
        
        // triangle position
        // create vbo (vbo is attribute-wise)
        glGenBuffers(1, &vbo_position_triangle);        // generate buffer object names
        // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
        // &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_triangle);        // creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
        // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
        // vbo: bind this (the name of a buffer object.)
        
        // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)
        
        // fill attributes
        glBufferData(GL_ARRAY_BUFFER,        // target buffer object (give data to GL_ARRAY_BUFFER)
                     sizeof(trianglePosition),        // size of array in which data is to be provided; size in bytes of the buffer object's new data store
                     trianglePosition,                // actual array in which data is present; pointer to data that will be copied into the data store for initialization
                     GL_STATIC_DRAW                    // when to give data (statically=now); static= The data store contents will be modified once and used many times.
                     );        // creates a new data store for the buffer object currently bound to target(1st para.)
        
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
                              3,                                            // number of components per generic vertex attribute; x,y,z for position
                              GL_FLOAT,                                    // data type of each component in the array
                              GL_FALSE,                                    // is data normalised
                              0,                                            // the byte offset between consecutive generic vertex attributes; 0=no stride
                              NULL                                        // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                              );        // define an array of generic vertex attribute data
        
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);    // para: Specifies the index of the generic vertex attribute to be enabled or disabled.
        // enables vPosition
        
        // same above steps for C,T,N if any
        
        // unbind (LIFO)
        glBindBuffer(GL_ARRAY_BUFFER, 0);    // 0= unbind; unbind vbo
        // to bind with next buffer
        
        // triangle color
        // create vbo (vbo is attribute-wise)
        glGenBuffers(1, &vbo_color_triangle);        // generate buffer object names
        // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
        // &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo_color_triangle);        // creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
        // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
        // vbo: bind this (the name of a buffer object.)
        
        // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)
        
        // fill attributes
        glBufferData(GL_ARRAY_BUFFER,    // target buffer object (give data to GL_ARRAY_BUFFER)
                     sizeof(triangleColor),        // size of array in which data is to be provided; size in bytes of the buffer object's new data store
                     triangleColor,                // actual array in which data is present; pointer to data that will be copied into the data store for initialization
                     GL_STATIC_DRAW                // when to give data (statically=now); static= The data store contents will be modified once and used many times.
                     );        // creates a new data store for the buffer object currently bound to target(1st para.)
        
        glVertexAttribPointer(AMC_ATTRIBUTE_COLOR,    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
                              3,                                            // number of components per generic vertex attribute; x,y,z for position
                              GL_FLOAT,                                    // data type of each component in the array
                              GL_FALSE,                                    // is data normalised
                              0,                                            // the byte offset between consecutive generic vertex attributes; 0=no stride
                              NULL                                        // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                              );        // define an array of generic vertex attribute data
        
        glEnableVertexAttribArray(AMC_ATTRIBUTE_COLOR);    // para: Specifies the index of the generic vertex attribute to be enabled or disabled.
        // enables vPosition
        
        // unbind (LIFO)
        glBindBuffer(GL_ARRAY_BUFFER, 0);    // 0= unbind; unbind vbo
        // to bind with next buffer
        glBindVertexArray(0);        // unbind vao
        
        // rectangle
        // create vao(id) (vao is shape-wise)
        glGenVertexArrays(1, &vao_rectangle);        // generate vertex array object names
        // 1: Specifies the number of vertex array object names to generate
        // &vao: Specifies an array in which the generated vertex array object names are stored (but vao is variable hence no '&')
        // everything below vao is recorded and stored in vao and then just vao is played in Display(). vao saves your lines in Display()
        
        glBindVertexArray(vao_rectangle);        // bind a vertex array object
        
        // rectangle position
        // create vbo (vbo is attribute-wise)
        glGenBuffers(1, &vbo_position_rectangle);        // generate buffer object names
        // 1: no. of buffers to create (Specifies the number of buffer object names to be generated.)
        // &vbo: address of buffer (Specifies an array in which the generated buffer object names are stored.)
        
        glBindBuffer(GL_ARRAY_BUFFER, vbo_position_rectangle);        // creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
        // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
        // vbo: bind this (the name of a buffer object.)
        
        // all lines between bind and unbind are related to vbo only since you are currently bound to vbo (state m/c)
        
        // fill attributes
        glBufferData(GL_ARRAY_BUFFER,        // target buffer object (give data to GL_ARRAY_BUFFER)
                     sizeof(rectanglePosition),        // size of array in which data is to be provided; size in bytes of the buffer object's new data store
                     rectanglePosition,                // actual array in which data is present; pointer to data that will be copied into the data store for initialization
                     GL_STATIC_DRAW                    // when to give data (statically=now); static= The data store contents will be modified once and used many times.
                     );        // creates a new data store for the buffer object currently bound to target(1st para.)
        
        glVertexAttribPointer(AMC_ATTRIBUTE_POSITION,    // at CPU side; the index of the generic vertex attribute to be modified (send to vPosition)
                              3,                                            // number of components per generic vertex attribute; x,y,z for position
                              GL_FLOAT,                                    // data type of each component in the array
                              GL_FALSE,                                    // is data normalised
                              0,                                            // the byte offset between consecutive generic vertex attributes; 0=no stride
                              NULL                                        // offset if V,C,T,N stored in single array (inter-leaved); NULL: no offset since no stride; pointer to the first component of the first generic vertex attribute in the array
                              );        // define an array of generic vertex attribute data
        
        glEnableVertexAttribArray(AMC_ATTRIBUTE_POSITION);    // para: Specifies the index of the generic vertex attribute to be enabled or disabled.
        // enables vPosition
        
        // same above steps for C,T,N if any
        
        // unbind (LIFO)
        glBindBuffer(GL_ARRAY_BUFFER, 0);    // 0= unbind; unbind vbo
        // to bind with next buffer
        
        // rectangle color
        glVertexAttrib3f(AMC_ATTRIBUTE_COLOR, 0.0f, 0.0f, 1.0f);    // specify the value of a generic vertex attribute(for single color to object)
        
        glBindVertexArray(0);        // unbind vao
        
        //glClearDepth(1.0f);        // bringing depth buffer into existance
        // filling the depth buffer with max value
        
        glEnable(GL_DEPTH_TEST);    // to compare depth values of objects
        
        glDepthFunc(GL_LEQUAL);    // specifies the value used for depth-buffer comparisons
        // Passes if the incoming z value is less than or equal to the stored z value.
        // GL_LEQUAL : GLenum
        
        // set background color
        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);    // bringing color buffer into existance
        // specifies clear values[0,1] used by glClear() for the color buffers
        
        perspectiveProjectionMatrix = vmath::mat4::identity();    // making orthographicProjectionMatrix an identity matrix(diagonals 1)
        // mat4: of vmath
        
        // GESTURE RECOGNITION (register and define your gestures)
        // tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        // initWithTarget:self: self is going to respond to the single tap (called target-action pattern)
        // 'onSingleTap:' is our method (':' tells it's a fn, not a string)
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger (touches=fingers)
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];     // register gesture recognizer with self
        
        // similar code for double tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger (touches=fingers)
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];     // register gesture recognizer with self
        
        // * this will allow to diffrentiate between single tap and double tap *
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; // to avoid taking 1st tap of double tap as single tap, fail that 1st tap as single tap
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        // no properties to be set
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}   // constructor ends

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
  We have drawView()
*/

+(Class)layerClass  // static method(start with +);     must be implemented
{
    // code
    // obtain class of CAEAGLLayer
    return([CAEAGLLayer class]);        // class: method
}

-(void)layoutSubviews   // inherited from UIView    ~Resize()
{
    // variable declarations
    GLint width;
    GLint height;
    
    // code
    // window size changed so again fill CRB and DRB
    // color buffer
    
    // no call to glGenRenderbuffers() as it is done by layer
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];   // eaglLayer not available here; GL_RENDERBUFFER: colorRenderBuffer
    // self.layer(our drawable) has data(eg. h & w of window) to fill in CB and returns object of CAEAGLLayer
    // hence we implemented layerClass()
    
    // depth buffer
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,       // take from this (GL_RENDERBUFFER= CB)
                                 GL_RENDERBUFFER_WIDTH, // take this
                                 &width          // take into this
                                 );
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);  // depth buffer according to color buffer
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    
    // establish data storage, format and dimensions of a renderbuffer object's image not from context but from OGL
    glRenderbufferStorage(GL_RENDERBUFFER,      // give to this
                          GL_DEPTH_COMPONENT16, // data format(16 bit depth component)(def.)
                          width,         // width of DB
                          height         // ht of DB
                          );
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,       // attach to this
                              GL_DEPTH_ATTACHMENT,  // attach here
                              GL_RENDERBUFFER,      // attach this
                              depthRenderBuffer     // depthRenderBuffer: this is bound to third para.
                              );    // replaces previous depth attachment
    
    if (height == 0)
    {
        height = 1;
    }
    
    glViewport(0, 0, width, height);
    
    perspectiveProjectionMatrix = vmath::perspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 100.0f);    // ? other values of near & far not working
    // parameters:
    // fovy- The field of view angle, in degrees, in the y - direction.
    // aspect- The aspect ratio that determines the field of view in the x - direction.The aspect ratio is the ratio of x(width) to y(height).
    // zNear- The distance from the viewer to the near clipping plane(always positive).
    // zFar- The distance from the viewer to the far clipping plane(always positive).
    
    // FB test
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
        // don't return since this does not usually occur
    }
    
    // projection code here
    
    [self drawView:nil];    // warm-up call to repaint
}

-(void)drawView:(id)sender      // instance method (starts with -)
{
    // function declarations
    void Update(void);
    
    // code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);  // since you are going to render from this
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    // One or more executables are created in a program object by successfully attaching shader objects to it with glAttachShader, successfully compiling the shader objects with glCompileShader, and successfully linking the program object with glLinkProgram. These executables are made part of current state when glUseProgram is called. Program objects can be deleted by calling glDeleteProgram.
    glUseProgram(gShaderProgramObject);    // binding your OpenGL code with shader program object
    // Specifies the handle of the program object whose executables are to be used as part of current rendering state.
    
    // triangle
    // 4 CPU steps
    // declaration of matrices
    vmath::mat4 modelViewMatrix;
    vmath::mat4 modelViewProjectionMatrix;
    vmath::mat4 translationMatrix;
    vmath::mat4 rotationMatrix;
    
    // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    
    // do necessary transformation (T,S,R)
    translationMatrix = vmath::translate(-1.5f, 0.0f, -6.0f);
    rotationMatrix = vmath::rotate(angle_triangle, 0.0f, 1.0f, 0.0f);
    
    // do necessary matrix multiplication (done by gluOrtho2d() in FFP)
    modelViewMatrix = translationMatrix * rotationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;        // multiplication of matrices: operator overloading
    
    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    glUniformMatrix4fv(mvpUniform,        // uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
                       1,                                // how many matrices to send?
                       GL_FALSE,                        // do transpose? ; no since OGL and GLSL are column-major
                       modelViewProjectionMatrix        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
                       );
    
    // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
    glBindVertexArray(vao_triangle);        // arrays are in vbo and vbo is in vao. Hence, bind to vao
    
    glBindBuffer(GL_ARRAY_BUFFER, vbo_color_triangle);    // creates vbo <-> GL_ARRAY_BUFFER and lets you create(?) or "use" a named buffer object
    // GL_ARRAY_BUFFER: the target to which the buffer object is bound. (in GPU)
    // vbo: bind this (the name of a buffer object.)
    
    // similarly, bind with textures if any (call glBindTexture() here)
    
    // draw the necessary scene!
    glDrawArrays(GL_TRIANGLES,        // what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                 0,                            // array position of your 9-member array to start with (imp in inter-leaved)
                 3                            // how many vertices to draw
                 );        // render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
    // Arrays since multiple primitives(P,C,N,T) can be drawn
    
    // unbind vao
    glBindVertexArray(0);
    
    // rectangle
    // 4 CPU steps
    // initialize above matrices to identity (not initialised in above step for better visibility and understanding)
    modelViewMatrix = vmath::mat4::identity();
    modelViewProjectionMatrix = vmath::mat4::identity();
    translationMatrix = vmath::mat4::identity();
    rotationMatrix = vmath::mat4::identity();
    
    // do necessary transformation (T,S,R)
    translationMatrix = vmath::translate(1.5f, 0.0f, -6.0f);
    rotationMatrix = vmath::rotate(angle_rectangle, 1.0f, 0.0f, 0.0f);
    
    // do necessary matrix multiplication (done by gluOrtho2d() in FFP)
    modelViewMatrix = translationMatrix * rotationMatrix;
    modelViewProjectionMatrix = perspectiveProjectionMatrix * modelViewMatrix;        // multiplication of matrices: operator overloading
    
    // fill and send uniforms
    // send necessary matrices to shader in respective uniforms (on GPU)
    glUniformMatrix4fv(mvpUniform,        // uniform in which modelViewProjectionMatrix is to be sent; Specifies the location of the uniform value to be modified.(send to u_mvp_matrix)
                       1,                                // how many matrices to send?
                       GL_FALSE,                        // do transpose? ; no since OGL and GLSL are column-major
                       modelViewProjectionMatrix        // actual matrix; this will bind to mvpUniform which is bound to u_mvp_matrix
                       );
    
    // bind with vao; this will avoid many repetitive bindings with vbo (needed since unbound in Initialize())
    glBindVertexArray(vao_rectangle);        // arrays are in vbo and vbo is in vao. Hence, bind to vao
    
    // similarly, bind with textures if any (call glBindTexture() here)
    
    // draw the necessary scene!
    glDrawArrays(GL_TRIANGLE_FAN,        // PP does not have GL_QUADS! ;    what kind of primitives to render; same as glBegin(GL_TRIANGLES)
                 0,                                // array position of your 9-member array to start with (imp in inter-leaved)
                 4                                // how many vertices to draw
                 );        // render primitives from array data;  If GL_VERTEX_ARRAY is not enabled(how?), no geometric primitives are generated
    // Arrays since multiple primitives(P,C,N,T) can be drawn
    
    // unbind vao
    glBindVertexArray(0);
    
    // unuse program
    glUseProgram(0);    // unbinding your OpenGL code with shader program object
    // If program is 0, then the current rendering state refers to an invalid program object, and the results of vertex and fragment shader execution due to any glDrawArrays or glDrawElements commands are undefined.
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);     // since CB renders
    // no binding to DB as CBs could be many but DB is single
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];  // present render buffer to drawable ~glSwapBuffers()
    
    Update();
}

-(void)startAnimation   // our method, not UIView's
{
    if(!isAnimating)    // or if(isAnimating==NO); animate if not animating
    {
        // create displayLink
        displayLink=[NSClassFromString(@"CADisplayLink")    // get CADisplayLink class
                     displayLinkWithTarget:self             // defined in CADisplayLink class
                     selector:@selector(drawView:)];        // selector: method
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        // add our displayLink function(drawView) to run loop (animation start)
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop]
                     forMode:NSDefaultRunLoopMode      // purpose to add
        ];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation    // our method, not UIView's
{
    if(isAnimating)     // r if(isAnimating==YES); stop animation if animating
    {
        [displayLink invalidate];   // removes displayLink from run loop
                                    // unregisters and removes it
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);        // YES means method is implemented
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event    // ~ Android's onTouchEvent()
{
    
}

// our 4 methods
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // exit code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

-(void) dealloc
{
    // code
    if (vbo_position_rectangle)
    {
        glDeleteBuffers(1, &vbo_position_rectangle);    // delete named buffer objects
        // 1: number of buffer objects to be deleted
        // &vbo: array of buffer objects to be deleted
        vbo_position_rectangle = 0;
    }

    if (vao_rectangle)
    {
        glDeleteVertexArrays(1, &vao_rectangle);        // delete vertex array objects
        vao_rectangle = 0;
    }
    
    if (vbo_position_triangle)
    {
        glDeleteBuffers(1, &vbo_position_triangle);    // delete named buffer objects
        // 1: number of buffer objects to be deleted
        // &vbo: array of buffer objects to be deleted
        vbo_position_triangle = 0;
    }
    
    if (vbo_color_triangle)
    {
        glDeleteBuffers(1, &vbo_color_triangle);    // delete named buffer objects
        // 1: number of buffer objects to be deleted
        // &vbo: array of buffer objects to be deleted
        vbo_color_triangle = 0;
    }
    
    if (vao_triangle)
    {
        glDeleteVertexArrays(1, &vao_triangle);        // delete vertex array objects
        vao_triangle = 0;
    }
    
    if (gShaderProgramObject)
    {
        GLsizei shaderCount;    // typedef int
        GLsizei shaderNumber;
        
        glUseProgram(gShaderProgramObject);        // since unused in Display()
        
        // ask program that how many shaders are attached to it
        glGetProgramiv(gShaderProgramObject,
                       GL_ATTACHED_SHADERS,        // returns the number of shader objects attached to shader program
                       &shaderCount);
        
        GLuint *pShaders = (GLuint*)malloc(sizeof(GLuint) * shaderCount);        // dynamic array for shaders since we don't know how many present
        if (pShaders)    // mem allocated
        {
            // take attached shaders  into above array
            glGetAttachedShaders(gShaderProgramObject,        // the program object to be queried
                                 shaderCount,                                // the size of the array for storing the returned object names
                                 &shaderCount,                                // Returns the number of names actually returned in last parameter(empty now) (diff var also ok)
                                 pShaders                                    // an array that is used to return the names of attached shader objects(empty now)
                                 );        // return the handles of the shader objects attached to a program object
            
            for (shaderNumber = 0;shaderNumber < shaderCount;shaderNumber++)
            {
                // detach each shader
                glDetachShader(gShaderProgramObject, pShaders[shaderNumber]);        // 1st para: Specifies the program object from which to detach the shader object.
                // 2nd para: Specifies the shader object to be detached.
                
                // delete each detached shader
                glDeleteShader(pShaders[shaderNumber]);
                pShaders[shaderNumber] = 0;
            }
            free(pShaders);
        }
        // delete the shader program
        glDeleteProgram(gShaderProgramObject);    // frees the memory and invalidates the name associated with the program object specified by the parameter. This command effectively undoes the effects of a call to glCreateProgram().
        gShaderProgramObject = 0;
        glUseProgram(0);
    }
    
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end

void Update(void)
{
    angle_triangle = angle_triangle + 1.0f;    // greater increment means greater speed of rotation
    if (angle_triangle >= 360.0f)            // discipline; angle is never greater than 360 degrees
        angle_triangle = 0.0f;
    
    angle_rectangle = angle_rectangle - 1.0f;
    if (angle_rectangle <= -360.0f)
        angle_rectangle = 0.0f;
}

