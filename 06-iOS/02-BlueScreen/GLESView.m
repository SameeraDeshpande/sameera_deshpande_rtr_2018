#import <OpenGLES/ES3/gl.h>     // adding OpenGLES.framework; locked on version 3.0
#import <OpenGLES/ES3/glext.h>  // ~glew.h (extension wrangler)
#import "GLESView.h"


@implementation GLESView
{
    EAGLContext *eaglContext;   // EAGL= Embedded Apple Graphics Layer
    
    GLuint defaultFrameBuffer;  // storage for all buffers
    GLuint colorRenderBuffer;   // color buffer, depth buffer & stencil buffer are togetherly called render buffers
    GLuint depthRenderBuffer;
    
    id displayLink;     // id= generic
    NSInteger animationFrameInterval;   // mac's integer
    BOOL isAnimating;
}

-(id)initWithFrame:(CGRect)frame;
{
    // code
    self=[super initWithFrame:frame];   // self= this
    if(self)
    {
        // get layer from super (i.e. from UIView)
        CAEAGLLayer *eaglLayer=(CAEAGLLayer*)super.layer;   // CA: Core Animation; layer: method; can also write [super layer]
                                                            // layer will be CALayer and not OGL layer so we typecast it to OGL layer
        
        // set its properties
        eaglLayer.opaque=YES;   // setting its alpha to 1.0 or [eaglLayer setOpaque]
        eaglLayer.drawableProperties=[NSDictionary dictionaryWithObjectsAndKeys:    // dictionary when multiple properties (object-key array)
                                      [NSNumber numberWithBool:FALSE],              // creating no. from BOOL
                                      kEAGLDrawablePropertyRetainedBacking,         // drawable in iOS= Android's surface (constant i.e. #define)
                                      kEAGLColorFormatRGBA8,                        // 8 bits each for R,G,B,A; it is a live object(struct) and not a constant
                                      kEAGLDrawablePropertyColorFormat,             // this is key of the preceding object
                                      nil];
        
        eaglContext=[[EAGLContext alloc]initWithAPI:kEAGLRenderingAPIOpenGLES3];    // constant
        if(eaglContext==nil)
        {
            printf("Couldn't Achieve EAGLContext");
            [self release];
            return(nil);
        }
        
        [EAGLContext setCurrentContext:eaglContext];    // ~wglMakeCurrent() in Windows or glxMakeCurrent() in Linux or makeCurrentContext() in macOS
        
        // create frame buffer (setup similar to vao, vbo creation and use in Display())
        glGenFramebuffers(1,&defaultFrameBuffer);   // 1: one FB
        glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);  // GL_FRAMEBUFFER: target(GPU addr); defaultFrameBuffer: bind this

        // color buffer
        glGenRenderbuffers(1,&colorRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
        
        [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:eaglLayer];   // GL_RENDERBUFFER: colorRenderBuffer
                                                                                    // eaglLayer(our drawable) has data(eg. h & w of window) to fill in CB
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,       // attach to this
                                  GL_COLOR_ATTACHMENT0, // attach here
                                  GL_RENDERBUFFER,      // attach this
                                  colorRenderBuffer     // colorRenderBuffer: this is bound to third para.
                                  );
        
        // depth buffer
        GLint backingWidth;    // we want DB for these w & h
        GLint backingHeight;
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER,       // take from this (GL_RENDERBUFFER= CB)
                                     GL_RENDERBUFFER_WIDTH, // take this
                                     &backingWidth          // take into this
                                     );
        
        glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &backingHeight);
        
        glGenRenderbuffers(1, &depthRenderBuffer);
        glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
        
        // establish data storage, format and dimensions of a renderbuffer object's image not from context but from OGL
        glRenderbufferStorage(GL_RENDERBUFFER,      // give to this
                              GL_DEPTH_COMPONENT16, // data format(16 bit depth component)(def.)
                              backingWidth,         // width of DB
                              backingHeight         // ht of DB
                              );
        
        glFramebufferRenderbuffer(GL_FRAMEBUFFER,       // attach to this
                                  GL_DEPTH_ATTACHMENT,  // attach here
                                  GL_RENDERBUFFER,      // attach this
                                  depthRenderBuffer     // depthRenderBuffer: this is bound to third para.
                                  );
        
        // FB test
        if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
        {
            printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
            glDeleteFramebuffers(1, &defaultFrameBuffer);
            glDeleteRenderbuffers(1, &colorRenderBuffer);
            glDeleteRenderbuffers(1, &depthRenderBuffer);
            
            return(nil);
        }
        
        // FB is ready now
        printf("Renderer: %s | GL Version: %s | GLSL Version: %s\n",glGetString(GL_RENDERER),glGetString(GL_VERSION),glGetString(GL_SHADING_LANGUAGE_VERSION));
        
        // hard coded initializations
        isAnimating=NO;     // since we are still in constructor
        animationFrameInterval=60;  // default since iOS 8.2 (current iOS: 13.3); ours runs max on iOS 11.4, after that it gives warnings
        
        // OpenGL code
        // set background color
        glClearColor(0.0f, 0.0f, 1.0f, 1.0f);    // bringing color buffer into existance
        // specifies clear values[0,1] used by glClear() for the color buffers
       
        // GESTURE RECOGNITION (register and define your gestures)
        // tap gesture code
        UITapGestureRecognizer *singleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onSingleTap:)];
        // initWithTarget:self: self is going to respond to the single tap (called target-action pattern)
        // 'onSingleTap:' is our method (':' tells it's a fn, not a string)
        
        [singleTapGestureRecognizer setNumberOfTapsRequired:1];
        [singleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger (touches=fingers)
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:singleTapGestureRecognizer];     // register gesture recognizer with self
        
        // similar code for double tap
        UITapGestureRecognizer *doubleTapGestureRecognizer=[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onDoubleTap:)];
        [doubleTapGestureRecognizer setNumberOfTapsRequired:2];
        [doubleTapGestureRecognizer setNumberOfTouchesRequired:1];  // touch of 1 finger (touches=fingers)
        [singleTapGestureRecognizer setDelegate:self];
        [self addGestureRecognizer:doubleTapGestureRecognizer];     // register gesture recognizer with self
        
        // * this will allow to diffrentiate between single tap and double tap *
        [singleTapGestureRecognizer requireGestureRecognizerToFail:doubleTapGestureRecognizer]; // to avoid taking 1st tap of double tap as single tap, fail that 1st tap as single tap
        
        // swipe gesture
        UISwipeGestureRecognizer *swipeGestureRecognizer=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(onSwipe:)];
        // no properties to be set
        [self addGestureRecognizer:swipeGestureRecognizer];
        
        // long-press gesture
        UILongPressGestureRecognizer *longPressGestureRecognizer=[[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(onLongPress:)];
        [self addGestureRecognizer:longPressGestureRecognizer];
    }
    return(self);
}   // constructor ends

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
  We have drawView()
*/

+(Class)layerClass  // static method(start with +);     must be implemented
{
    // code
    // obtain class of CAEAGLLayer
    return([CAEAGLLayer class]);        // class: method
}

-(void)layoutSubviews   // inherited from UIView    ~Resize()
{
    // variable declarations
    GLint width;
    GLint height;
    
    // code
    // window size changed so again fill CRB and DRB
    // color buffer
    
    // no call to glGenRenderbuffers() as it is done by layer
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);
    
    [eaglContext renderbufferStorage:GL_RENDERBUFFER fromDrawable:(CAEAGLLayer*)self.layer];   // eaglLayer not available here; GL_RENDERBUFFER: colorRenderBuffer
    // self.layer(our drawable) has data(eg. h & w of window) to fill in CB and returns object of CAEAGLLayer
    // hence we implemented layerClass()
    
    // depth buffer
    glGetRenderbufferParameteriv(GL_RENDERBUFFER,       // take from this (GL_RENDERBUFFER= CB)
                                 GL_RENDERBUFFER_WIDTH, // take this
                                 &width          // take into this
                                 );
    
    glGetRenderbufferParameteriv(GL_RENDERBUFFER, GL_RENDERBUFFER_HEIGHT, &height);
    
    glGenRenderbuffers(1, &depthRenderBuffer);  // depth buffer according to color buffer
    glBindRenderbuffer(GL_RENDERBUFFER, depthRenderBuffer);
    
    // establish data storage, format and dimensions of a renderbuffer object's image not from context but from OGL
    glRenderbufferStorage(GL_RENDERBUFFER,      // give to this
                          GL_DEPTH_COMPONENT16, // data format(16 bit depth component)(def.)
                          width,         // width of DB
                          height         // ht of DB
                          );
    
    glFramebufferRenderbuffer(GL_FRAMEBUFFER,       // attach to this
                              GL_DEPTH_ATTACHMENT,  // attach here
                              GL_RENDERBUFFER,      // attach this
                              depthRenderBuffer     // depthRenderBuffer: this is bound to third para.
                              );    // replaces previous depth attachment
    
    glViewport(0, 0, width, height);
    
    // FB test
    if(glCheckFramebufferStatus(GL_FRAMEBUFFER)!=GL_FRAMEBUFFER_COMPLETE)
    {
        printf("Failed To Create Complete Framebuffer Object %x\n",glCheckFramebufferStatus(GL_FRAMEBUFFER));
        // don't return since this does not usually occur
    }
    
    // projection code here
    
    [self drawView:nil];    // warm-up call to repaint
}

-(void)drawView:(id)sender      // instance method (starts with -)
{
    // code
    [EAGLContext setCurrentContext:eaglContext];
    
    glBindFramebuffer(GL_FRAMEBUFFER, defaultFrameBuffer);  // since you are going to render from this
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    
    glBindRenderbuffer(GL_RENDERBUFFER, colorRenderBuffer);     // since CB renders
    // no binding to DB as CBs could be many but DB is single
    
    [eaglContext presentRenderbuffer:GL_RENDERBUFFER];  // present render buffer to drawable ~glSwapBuffers()
}

-(void)startAnimation   // our method, not UIView's
{
    if(!isAnimating)    // or if(isAnimating==NO); animate if not animating
    {
        // create displayLink
        displayLink=[NSClassFromString(@"CADisplayLink")    // get CADisplayLink class
                     displayLinkWithTarget:self             // defined in CADisplayLink class
                     selector:@selector(drawView:)];        // selector: method
        
        [displayLink setPreferredFramesPerSecond:animationFrameInterval];
        
        // add our displayLink function(drawView) to run loop (animation start)
        [displayLink addToRunLoop:[NSRunLoop currentRunLoop]
                     forMode:NSDefaultRunLoopMode      // purpose to add
        ];
        
        isAnimating=YES;
    }
}

-(void)stopAnimation    // our method, not UIView's
{
    if(isAnimating)     // r if(isAnimating==YES); stop animation if animating
    {
        [displayLink invalidate];   // removes displayLink from run loop
                                    // unregisters and removes it
        displayLink=nil;
        
        isAnimating=NO;
    }
}

// to become first responder
-(BOOL)acceptsFirstResponder
{
    // code
    return(YES);        // YES means method is implemented
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event    // ~ Android's onTouchEvent()
{
    
}

// our 4 methods
-(void)onSingleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onDoubleTap:(UITapGestureRecognizer *)gr
{
    
}

-(void)onSwipe:(UISwipeGestureRecognizer *)gr
{
    // exit code
    [self release];
    exit(0);
}

-(void)onLongPress:(UILongPressGestureRecognizer *)gr
{
    
}

-(void) dealloc
{
    // code
    if(depthRenderBuffer)
    {
        glDeleteRenderbuffers(1, &depthRenderBuffer);
        depthRenderBuffer=0;
    }
    
    if(colorRenderBuffer)
    {
        glDeleteRenderbuffers(1, &colorRenderBuffer);
        colorRenderBuffer=0;
    }
    
    if(defaultFrameBuffer)
    {
        glDeleteFramebuffers(1, &defaultFrameBuffer);
        defaultFrameBuffer=0;
    }
    
    if([EAGLContext currentContext]==eaglContext)
    {
        [EAGLContext setCurrentContext:nil];
    }
    [eaglContext release];
    eaglContext=nil;
    
    [super dealloc];
}

@end
